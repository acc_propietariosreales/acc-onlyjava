/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * GestorLlavesUtil.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour               By                  Company     Description
 * -------  -------------------     ----------------    --------    ---------
 * 1.0		12/02/2015-11:26:47 PM	 Stefanini			Stefanini	Creacion
 * 1.1      15/11/2017-10:00:00 AM   Isban 				Isban		Modificación por proyecto M/023732 Iniciativa Automotriz-Norkom
 */
package mx.isban.acc.lh.filter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import org.apache.log4j.Logger;

import altec.infra.StringEncrypter;
import altec.infra.StringEncrypter.EncryptionException;
import mx.isban.norkom.cuestionario.exception.GestorClavesException;
import mx.isban.norkom.properties.ConfigApp;

/**
 * @author J Ulises Garcia Martinez
 * Objetivo: Manejar las operaciones relacionadas con la lectura de las llaves Privada y Publica de un Keystore
 * Justificacion: Obtiene la informacion de un keystore, ya sea Base de Datos o sistema de archivos, para posteriormente obtener la llave publica o privada. La informacion se encuentra almacenada en un archivo de propiedades.  
 */
public class GestorLlavesUtil {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(GestorLlavesUtil.class);
	
	/**
	 * Llave publica cargada
	 */
	private static boolean llavePublicaCargada = false;
	
	/**
	 * Llave publica
	 */
	private static PublicKey llavePublica = null;
	
	/**
	 * Llave private cargada
	 */
	private static boolean llavePrivadaCargada = false;
	
	/**
	 * Llave privada
	 */
	private static PrivateKey llavePrivada = null;
	
	/** Mensaje usado cuando no es posible leer el keystore */
	private static final String ERROR_LEER_KEYSTORE = "Error de E/S al tratar de leer el keystore";
	
	/**
	 * Obtiene la llave privada de manera sincronizada
	 * @return La llave privada del keystore configurado
	 * @throws GestorClavesException En caso de un problema al obtener la llave o el keystore
	 */
	public PrivateKey obtenerLlavePrivada() throws GestorClavesException {
		LOGGER.debug("Obteniendo keystore de FS para llave privada");
		if(!llavePrivadaCargada) {
			synchronized(GestorLlavesUtil.class){
				if(!llavePrivadaCargada){ 
					llavePrivada = obtenerLlavePrivadaKS(ConfigApp.getRutaKeystoreLlavePublica());
					llavePrivadaCargada = true;
				}
			}
		}
		
		return llavePrivada;
	}

	/**
	 * Metodo que permite obtener la llave privada del KS
	 * @param rutaKeystoreLlavePublica Ruta keystore llave publica
	 * @throws GestorClavesException Error lanzado cuando ocurre un problema al momento de obtener la llave privada
	 * @return regresa la llave privada
	 */
	private PrivateKey obtenerLlavePrivadaKS(String rutaKeystoreLlavePublica) throws GestorClavesException {
		FileInputStream readStream = obtenerKeyStore(rutaKeystoreLlavePublica);
		PrivateKey privateKey = null;
		try {
			String tipoKeystore = ConfigApp.getTipoKeyStore();
			String passwordKeystoreEncrip = ConfigApp.getPwdKeystore();
			String aliasEncriptado = ConfigApp.getAliasKeystore();
			String pwdLlaveEncrip = ConfigApp.getPwdKeystore();
			
			String llaveDescifra = null;
			try {
				llaveDescifra = ConfigApp.getLlaveDescifrado();
			} catch(IllegalArgumentException iae){
				LOGGER.error("No existe una llave de descifrado, se va a realizar la operacion sin ella", iae);
			}
			
			String passwordKeystore = this.getPwdOriginal(passwordKeystoreEncrip, llaveDescifra);
			String pwdLlave = this.getPwdOriginal(pwdLlaveEncrip, llaveDescifra);
			String alias = this.getPwdOriginal(aliasEncriptado, llaveDescifra);
			KeyStore ks = KeyStore.getInstance(tipoKeystore);
			ks.load(readStream, passwordKeystore.toCharArray());
			Key tmpKey = ks.getKey(alias, pwdLlave.toCharArray());
			if(tmpKey instanceof PrivateKey){
				privateKey = (PrivateKey)tmpKey;
			}
		} catch (UnrecoverableKeyException e) {
			throw new GestorClavesException("No es posible recuperar el key del KS", e);
		} catch (KeyStoreException e) {
			throw new GestorClavesException("Error leyendo el keystore", e);
		} catch (NoSuchAlgorithmException e) {
			throw new GestorClavesException("Algoritmo no soportado", e);
		} catch (CertificateException e) {
			throw new GestorClavesException("Error al leer el certificado", e);
		} catch (IOException e) {
			throw new GestorClavesException(ERROR_LEER_KEYSTORE, e);
		} catch (EncryptionException e) {
			throw new GestorClavesException("Error al descifrar el password", e);
		}
		
		return privateKey;
	}

	/**
	 * Obtiene la llave publica
	 * @return Llave publica
	 * @throws GestorClavesException En caso de un problema al obtener la llave o el keystore
	 */
	public PublicKey obtenerLlavePublica(String aliasKeystore) throws GestorClavesException {
		if(!llavePublicaCargada) {
			synchronized(GestorLlavesUtil.class){
				if(!llavePublicaCargada){ 
					llavePublica = obtenerLlavePublicaKS(ConfigApp.getRutaKeystoreLlavePublica(), aliasKeystore);
					LOGGER.info(":::Se lee la llave publica con el alias " + aliasKeystore);
					//llavePublicaCargada = true;
				}
			}
		}
		
		return llavePublica;
	}

	/**
	 * Metodo que permite obtener la llave publica del KS
	 * @param rutaKeystoreLlavePublica ruta del keystore de la llave publica
	 * @throws GestorClavesException Error lanzado cuando ocurre un problema al momento de obtener la llave publica
	 * @return regresa la llave publica
	 */
	private PublicKey obtenerLlavePublicaKS(String rutaKeystoreLlavePublica, String aliasKeystore) throws GestorClavesException {
		FileInputStream readStream = obtenerKeyStore(rutaKeystoreLlavePublica);
		
		try {
		
			String tipoKeystore = ConfigApp.getTipoKeyStore();
			String passwordKeystoreEncrip = ConfigApp.getPwdKeystore();
			
			String llaveDescifra = null;
			try {
				llaveDescifra = ConfigApp.getLlaveDescifrado();
			} catch(IllegalArgumentException iae) {
				LOGGER.error("No existe una llave de descifrado, se va a realizar la operacion sin ella", iae);
			}
			
			String passwordKeystore = this.getPwdOriginal(passwordKeystoreEncrip, llaveDescifra);
			KeyStore ks = KeyStore.getInstance(tipoKeystore);
			ks.load(readStream, passwordKeystore.toCharArray());
			Certificate cert = ks.getCertificate(aliasKeystore);
			
			return cert.getPublicKey();
		} catch (KeyStoreException e) {
			throw new GestorClavesException("Error leyendo el keystore", e);
		} catch (NoSuchAlgorithmException e) {
			throw new GestorClavesException("Algoritmo no soportado", e);
		} catch (CertificateException e) {
			throw new GestorClavesException("Error al leer el certificado", e);
		} catch (IOException e) {
			throw new GestorClavesException(ERROR_LEER_KEYSTORE, e);
		} catch (EncryptionException e) {
			throw new GestorClavesException("Fue imposible obtener el password original para el keystore o llave privada", e);
		} finally {
			if(readStream!=null){
				try {
					readStream.close();
				} catch (IOException e) {
					throw new GestorClavesException(ERROR_LEER_KEYSTORE,e);
				}
			}
		}
	}
	
	/**
	 * Metodo que permite obtener el password original para poder accesar al keystore y a la llave privada
	 * @param pwd Cadena encriptada del password original
	 * @param llaveDescifra Llave usada para descifrar la cadena y poder obtener el password original
	 * @throws EncryptionException Error lanzado en caso de existir algun tipo de error al recuperar el password original
	 * @return regresa el password original
	 */
	private String getPwdOriginal(final String pwd, final String llaveDescifra) throws EncryptionException {
		StringEncrypter deencrypter = null;
		if (llaveDescifra == null || llaveDescifra.isEmpty()) {
			deencrypter = new StringEncrypter(StringEncrypter.DES_ENCRYPTION_SCHEME);
		} else {
			deencrypter = new StringEncrypter(StringEncrypter.DES_ENCRYPTION_SCHEME, llaveDescifra);	
		}
		return deencrypter.decrypt(pwd);
	}
	
	/**
	 * Obtiene el Keystore de la ruta especificada
	 * @param rutaFileSystem Ruta en donde se encuentra el file system
	 * @return InputStream del keystore
	 * @throws GestorClavesException En caso de no encontrar el archivo
	 */
	private FileInputStream obtenerKeyStore(String rutaFileSystem) throws GestorClavesException {
		LOGGER.debug(String.format(":::Obteniendo llave publica de keystore", rutaFileSystem));
		try {
			return new FileInputStream(rutaFileSystem);
		} catch (FileNotFoundException e) {
			LOGGER.error("No se encontro el Keystore", e);
		}
		
		return null;
	}
}
