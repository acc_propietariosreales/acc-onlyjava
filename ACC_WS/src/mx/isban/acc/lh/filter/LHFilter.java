/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * LHFilter.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour               By                  Company     Description
 * -------  -------------------     ----------------    --------    ---------
 * 1.0		12/02/2015-11:26:47 PM	 Stefanini			Stefanini	Creacion
 * 1.1      17/10/2017-10:00:00 AM   Isban 				Isban		Modificación por proyecto M/023732 Iniciativa Automotriz-Norkom
 */
package mx.isban.acc.lh.filter;

import java.io.IOException;
import java.security.PublicKey;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import mx.isban.norkom.cuestionario.exception.GestorClavesException;
import mx.isban.norkom.properties.ConfigApp;
import mx.isban.norkom.util.ConvertidorB64;

/**
 * The Class LHFilter.
 * @author J ULISES GARCIA MARTINEZ
 * Objetivo: Interceptar todas las peticiones a los servicios web REST
 * Justificacion: Validar a el JWT a traves del metodo doFilter. Se obtiene la llave publica y se valida que la Firma, la Audiencia y el Emisor
 * sean validos de acuerdo a la parametría definida en el archivo de Configuracion y al almacen de claves. En caso de cumplir con el filtro permite
 * continuar con el flujo, en caso contrario devuelve un mensaje error y deniega el acceso al servicio.
 * Actualizacion: Isban 17/10/2017
 */
public class LHFilter implements Filter {
	/**
	 * Objeto - Gestor de llaves
	 */
	private final transient GestorLlavesUtil gestor = new GestorLlavesUtil();

	/**
	 * Variable utilizada para escribir el log de la aplicacion
	 */
	private static final Logger LOGGER = Logger.getLogger(LHFilter.class);

	/** The config. */
	private FilterConfig config = null;
	
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() { 
	// Override abstract method destroy() in Filter class
		LOGGER.info( "LHFilter destroyed" );
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
		String mensajeError = StringUtils.EMPTY;
		String issuerConf = "";
		try {
			final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
			//  Se obtiene el Header desde el httpRequest
			final String authHeader = httpRequest.getHeader("Authorization");
			//  Valida si el header  viene vacio y si el mismo no inicia con la palabra clave Bearer
	        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
				//  Al cumplirse alguna de las condiciones, devuelve el mensaje de error correspondiente
				LOGGER.info(":::Error: Unauthorized access - JWT is required");
	        	((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized access - JWT is required");
				return;
	        }

			//String jwtAudience = ConfigApp.getJwtAudience();

			//  Se obtiene el Token (base64) contenido en el header
			final String compactJwt = authHeader.substring(7); // The part after "Bearer "
			LOGGER.info(":::JWT enviado: " + compactJwt);
			//  Obtenemos la propiedad Issuer desde el Token que se envia en el Payload utilizando el metodo obtenerIssuer() para decodificarlo
			String jwtIssuer = obtenerIssuer(compactJwt);
			PublicKey llavePublica = gestor.obtenerLlavePublica(jwtIssuer);
			
			//  Obtenemos la propiedad Issuer desde el archivo de configuracion y lo comparamos con el Issuer enviado en el Token
			issuerConf = ConfigApp.getJwtIssuer();
			String[] parts = issuerConf.trim().split("\\s*\\|\\s*");

			//  Si el Issuer existe en el archivo de propiedades se da paso a la autenticacion
			if (ArrayUtils.contains(parts, jwtIssuer)){
				LOGGER.info(":::El issuer enviado en el Token concuerda con el del properties." );
				// Se valida la firma en base a la llave Publica
				Jwts.parser().setSigningKey(llavePublica).parseClaimsJws(compactJwt);
				//Jwts.parser().requireAudience(jwtAudience).setSigningKey(llavePublica).parseClaimsJws(compactJwt);
				Jwts.parser().requireIssuer(jwtIssuer).setSigningKey(llavePublica).parseClaimsJws(compactJwt);
				LOGGER.info(":::Comienza llamado a la autenticacion de la aplicacion.");

				// Se realiza el filtro de manera correcta
				chain.doFilter(servletRequest, servletResponse);
				LOGGER.info(":::Autenticacion correcta para: " + jwtIssuer);
				return;
			} else{
			//  En caso contrario, se deniega el acceso
				LOGGER.info(":::Autenticacion incorrecta para: " + jwtIssuer);
				((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized access - Unsupported JWT");
			}

			//  Manejo de errores
		} catch (GestorClavesException e) {
			mensajeError = "Unauthorized access - Incorrect Key Info";
			LOGGER.error(String.format("Unauthorized access - Incorrect Key Info", e.getMessage()), e);
		} catch (ExpiredJwtException e) {
			mensajeError = "Unauthorized access - Expired JWT";
			LOGGER.error(String.format("Unauthorized access - Expired JWT", e.getMessage()), e);
		} catch (UnsupportedJwtException e) {
			mensajeError = "Unauthorized access - Unsupported JWT";
			LOGGER.error(String.format("Unauthorized access - Unsupported JWT", e.getMessage()), e);
		} catch (MalformedJwtException e) {
			mensajeError = "Unauthorized access - Malformed JWT";
			LOGGER.error(String.format("Unauthorized access - Malformed JWT", e.getMessage()), e);
		} catch (SignatureException e) {
			mensajeError = "Unauthorized access - Incorrect Signature";
			LOGGER.error(String.format("Unauthorized access - Incorrect Signature", e.getMessage()), e);
		}

		((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, mensajeError);
	}

	/**
	 * Obtiene la propiedad issuer, en base a la cadena JWT base64
	 * @param jwtBase64, Objeto con la informacion del JWT en base64
	 * @return String, con el valor de la propiedad issuer
	 * @return null, en caso de no encontrar la propiedad
	 */
	private String obtenerIssuer(String jwtBase64) {
		try {
			if(StringUtils.isNotBlank(jwtBase64)) {
				// Separamos la cadena base64 del token
				String[] jwtSeparado = jwtBase64.split("\\.");
				if(jwtSeparado.length >= 3) {
					// Obtenemos la propiedad iss desde el cuerpo del token
					String headerJson = new String(ConvertidorB64.decodificaBase64Bytes(jwtSeparado[1]));
					ObjectMapper objectMapper = new ObjectMapper();
					JsonNode nodo = objectMapper.readTree(headerJson.getBytes());
					JsonNode issuer = nodo.get("iss");
					return issuer.asText();
				}
			}
		} catch (JsonProcessingException e) {
			LOGGER.error(String.format("Error al leer la cadena JSON ProcessingExc - Mensaje Error[%s]", e.getMessage()), e);
		} catch (IOException e) {
			LOGGER.error(String.format("Error al leer la cadena JSON IOExc - Mensaje Error[%s]", e.getMessage()), e);
		}

		return null;
	}
	
	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	//  Override abstract method init(FilterConfig) in Filter class
		LOGGER.info( "LHFilter initialized" );
	}

	/**
	 * @return the config
	 */
	public FilterConfig getConfig() {
		return config;
	}

	/**
	 * @param config
	 * el config a agregar
	 */
	public void setConfig(FilterConfig config) {
		this.config = config;
	}

}