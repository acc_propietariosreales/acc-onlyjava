/**
 * NorkomC ComponenteCentralFacade.java
 */
package mx.isban.norkom.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.cuestionario.bo.AdministradorComponenteCentralBO;
import mx.isban.norkom.cuestionario.bo.AdministradorRelacionadosBO;
import mx.isban.norkom.cuestionario.bo.ObtenerCuestionarioWSBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.dto.CuestionarioFinalizadoDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.util.ConvertidorB64;
import mx.isban.norkom.util.GeneraReportesByte;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;
import mx.isban.norkom.ws.dto.CuestionariosWebServiceDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosWebServiceDTO;
import mx.isban.norkom.ws.dto.ReporteBase64WebServiceDTO;
import mx.isban.norkom.ws.dto.RespuestaWebService;

/**
 * @author Leopoldo F Espinosa R 20/04/2015
 *
 */
public class ComponenteCentralFacade {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(ComponenteCentralFacade.class);
	
	/** Define el nombre del BO para ACC */
	private final static String TXT_NOMBRE_BO_ACC = "BOAdministradorComponenteCentral";
	
	/**
	 * String Parametro URL que se obtendra de la base de datos del ACC
	 */
	private static final String PARAMETRO_URL = "urlComponeneteCentral";
	/**constante para Mensajes*/
	private static final String MENSAJE="Mensaje";
	/**contexto para leer la configuracion de Spring**/
	private final transient ServletContext servletContext;
	/**
	 * Constructor para ComponenteCentralFacade
	 * @param servletContext contexto para spring 
	 **/
	public ComponenteCentralFacade(ServletContext servletContext) {
		this.servletContext=servletContext;
	}

	/**
	 * Generar un identificador de cuestionario
	 * @param idAplicacion Identificador de la aplicacion
	 * @param buc Codigo de Cliente
	 * @return CuestionariosWebServiceDTO Objecto con la informacion de la operacion
	 */
	public CuestionariosWebServiceDTO obtenerIdCuestionario( String idAplicacion,  String buc) {
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final AdministradorComponenteCentralBO administradorIdCuestionarioBO =
				(AdministradorComponenteCentralBO)springContext.getBean(TXT_NOMBRE_BO_ACC);
		
		CuestionariosWebServiceDTO cuestionarioDto = new CuestionariosWebServiceDTO();
		String idCuestionario = null;
		try {
			if(buc == null || buc.isEmpty()){
				cuestionarioDto.setIdCuestionario(StringUtils.EMPTY);
				cuestionarioDto.setCodigoOperacion(AdministradorComponenteCentralBO.COD_ER_BUC);
				cuestionarioDto.setDescOperacion( AdministradorComponenteCentralBO.DESC_ER_BUC);
				return cuestionarioDto;
			}
			
			idCuestionario = administradorIdCuestionarioBO.obtenerIdCuestionario(idAplicacion, new ArchitechSessionBean());
			
			cuestionarioDto.setIdCuestionario(idCuestionario);
			cuestionarioDto.setCodigoOperacion(Mensajes.EXOBID00.getCodigo());
			cuestionarioDto.setDescOperacion(Mensajes.EXOBID00.getDescripcion());
		} catch (BusinessException e) {
			LOGGER.debug(String.format("No fue posible obtener ID de Cuestionario Codigo[%s] Mensaje[%s]", e.getCode(), e.getMessage()));
			LOGGER.error("Mensajes:",e);
			cuestionarioDto.setIdCuestionario(StringUtils.EMPTY);
			cuestionarioDto.setCodigoOperacion(e.getCode());
			cuestionarioDto.setDescOperacion(e.getMessage());
		}
		
		return cuestionarioDto;
	}
	
	/**
	 * Obtiene la URL a la cual se deben solicitar los cuestionario
	 * @return URL de los cuestionarios en el Componente Central, si no se encuentra se regresa un acadena vacia
	 */
	public String obtenerUrlCuestionario() {
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ParametrosBO parametrosBO = (ParametrosBO)springContext.getBean("BOParametros");
		
		try {
			ParametroDTO parametroDTO = parametrosBO.obtenerParametroPorNombre(PARAMETRO_URL, new ArchitechSessionBean());
			
			if(parametroDTO != null && parametroDTO.getValor() != null) {
				LOGGER.info(String.format("URL en BD Parametro[%s] Valor[%s]", parametroDTO.getNombre(), parametroDTO.getValor()));
				return parametroDTO.getValor();
			} else {
				LOGGER.info(String.format("No fue posible obtener el valor de la URL de BD - Parametro[%s]", PARAMETRO_URL));
			}
		} catch (BusinessException e) {
			LOGGER.error(MENSAJE,e);
			LOGGER.debug(String.format("Error al obtener la URL de Cuestionarios Code[%s] Message[%s] Cause[%s]", e.getCode(), e.getMessage(), e.getCause()));
		}
		
		return StringUtils.EMPTY;
	}
	
	/**
	 * Obtiene un cuestionario en formato PDF.<BR>
	 * El primer filtro sera el id de formulario, <br>
	 * como segundo filtro en caso de que no venga el id de formulario y seran obligatorios: 
	 * <br>-Codigo de sucursal
     * <br>-Numero de cuenta o contrato
     * <br>-Codigo de producto
     * <br>-Codigo de subproducto
     * @param idFormulario el id de formulario
     * @param codigoSucursal Codigo de sucursal
     * @param numeroContrato Numero de contrato del cual se desea obtener el cuestionario
     * @param codigoProducto Codigo de producto
     * @param codigoSubProducto Codigo de subproducto
     * 
	 * @return String Cadena en Base64 con la informacion de los cuestionarios
	 */
	public ReporteBase64WebServiceDTO obtenerCuestionarioPdf(
			String idFormulario, String codigoSucursal,String numeroContrato, String codigoProducto,String codigoSubProducto) 
	{
		LOGGER.info("Inicia obtenerCuestionarioPdf idFromulario: "+idFormulario+" numCntr: "+numeroContrato);
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ObtenerCuestionarioWSBO pdfCCBO =
				(ObtenerCuestionarioWSBO)springContext.getBean("BOObtenerCuestionario");
		ReporteBase64WebServiceDTO respuestaWs = new ReporteBase64WebServiceDTO();
		
		try {
			RequestObtenerCuestionario peticion=
					llenardtoPeticion(idFormulario,codigoSucursal,numeroContrato,codigoProducto,codigoSubProducto);
			
			CuestionarioWSDTO cuestionarioBD = 
						pdfCCBO.obtenerDatosGeneralesCuestionarioPorIdCntrNoNulo(peticion, new ArchitechSessionBean());
				
			if(cuestionarioBD == null || cuestionarioBD.getClaveCuestionarioIP() == 0){
				respuestaWs.setCodigoOperacion(ReporteBase64WebServiceDTO.COD_ER_DATOS);
				respuestaWs.setDescOperacion(ReporteBase64WebServiceDTO.MSJ_ER_DATOS);
				return respuestaWs;
			}
			
			LOGGER.debug("Inicia obtenerCuestionarioPdf->ObtenerPdfCCBO.obtenerPDF ");
			ResponseObtenerCuestionario respuesta = pdfCCBO.obtenerPdfIP(peticion, new ArchitechSessionBean());
			
			if(Mensajes.OPERACION_EXITOSA.getCodigo().equals(respuesta.getCodError())){
				//vamos a llenar el reporte
				LOGGER.debug("enviamos a obtener Reporte IP");
				ResponseObtenerCuestionario respuestadatos= new ResponseObtenerCuestionario();
				respuestadatos.setDatosReporte((HashMap<String, Object> )((HashMap<String, Object>)respuesta.getDatosReporte()).clone());
				GeneraReportesByte reportebyte=null;
				if(respuesta!=null && respuesta.getTipo()!=null){
					reportebyte= new GeneraReportesByte(respuesta,true);
				}else{
					asignarCodigoMensajeOperacion(respuestaWs, 
							ReporteBase64WebServiceDTO.COD_ER_GENERACION, ReporteBase64WebServiceDTO.MSJ_ER_GENERACION);
					return respuestaWs;
				}
				
				LOGGER.debug("Termina obtener Reporte IP. Inicia obtencion de IC");
				
				ResponseObtenerCuestionario respuestaIC=null;
				try{
					respuestaIC = pdfCCBO.obtenerPdfIC(peticion, cuestionarioBD, respuestadatos,new ArchitechSessionBean());
				}catch(BusinessException e){
					if(!Mensajes.ERROR_NO_DATOS.getCodigo().equals(e.getCode())){
						asignarCodigoMensajeOperacion(respuestaWs, e.getCode(), e.getMessage());
						return respuestaWs;
					}
					respuestaIC=new ResponseObtenerCuestionario();
					respuestaIC.setCodError(e.getCode());
				}
				if(Mensajes.OPERACION_EXITOSA.getCodigo().equals(respuestaIC.getCodError())){
					LOGGER.debug("Generamos reporte IC");
					GeneraReportesByte reportebyteIC= new GeneraReportesByte(respuestaIC,false);					
					ConvertidorB64.convierteBase64(
							respuestaWs,reportebyte.concatenarPDFs(Arrays.asList(reportebyte.getReporte(),reportebyteIC.getReporte())));
				}else{
					//obtenemos el arreglo de bytes y lo mandamos codificar
					LOGGER.debug("Inicia conversion Reporte IP base64");
					ConvertidorB64.convierteBase64(respuestaWs,reportebyte.concatenarPDFs(Arrays.asList(reportebyte.getReporte())));
				}
			} else {
				LOGGER.debug("No se pudo obtener el PDF:"+ respuesta.getCodError());
				asignarCodigoMensajeOperacion(respuestaWs, 
						ReporteBase64WebServiceDTO.COD_ER_GENERACION, ReporteBase64WebServiceDTO.MSJ_ER_GENERACION);
			}
		} catch (BusinessException e) {
			LOGGER.error("Mensaje:",e);
			asignarCodigoMensajeOperacion(respuestaWs, e.getCode(), e.getMessage());
		}
		
		return respuestaWs;
	}
	
	/**
	 * Define el codigo y el mensaje de operacion
	 * @param respuestaWs Objeto al que seran asignados los valores
	 * @param codigoOp Codigo de operacion que sera asignado
	 * @param descripcionOp Descripcion de la operacion que sera asignada
	 */
	private void asignarCodigoMensajeOperacion(ReporteBase64WebServiceDTO respuestaWs, String codigoOp, String descripcionOp){
		respuestaWs.setCodigoOperacion(codigoOp);
		respuestaWs.setDescOperacion(descripcionOp);
	}

	/**
	 * Llena un dto con la informacion de la peticion
     * @param idFormulario el id de formulario
     * @param codigoSucursal Codigo de sucursal
     * @param numeroContrato Numero de contrato del cual se desea obtener el cuestionario
     * @param codigoProducto Codigo de producto
     * @param codigoSubProducto Codigo de subproducto
	 * @return RequestObtenerCuestionario con datos de entrada
	 */
	private RequestObtenerCuestionario llenardtoPeticion(String idFormulario,
			String codigoSucursal, String numeroContrato,
			String codigoProducto, String codigoSubProducto) {
		RequestObtenerCuestionario peticion=new RequestObtenerCuestionario();
		peticion.setIdCuestionario(idFormulario);
		peticion.setContrato(numeroContrato);
		peticion.setCodSucursal(codigoSucursal);
		peticion.setCodProducto(codigoProducto);
		peticion.setCodSubProducto(codigoSubProducto);
		return peticion;
	}

	/** 
	 * Agrega relacionados a un Cuestionario
	 * @param relacionadosWS Lista de relacionados que se desean agregar
	 * @return RelacionadosWebServiceDTO Respuesta de la operacion de agregar relacionados
	 */
	public RelacionadosWebServiceDTO agregarRelacionados(RelacionadosWebServiceDTO relacionadosWS){
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final AdministradorRelacionadosBO administradorRelacionadosBO =
				(AdministradorRelacionadosBO)springContext.getBean("BOAdministradorRelacionados");
		
		final RelacionadosWebServiceDTO respuestaWS = new RelacionadosWebServiceDTO();
		try {
			ArrayList<RelacionadoDTO> relacionadosErroneos = (ArrayList<RelacionadoDTO>)
					administradorRelacionadosBO.agregarRelacionados(relacionadosWS, new ArchitechSessionBean());
			
			respuestaWS.setRelacionados(relacionadosErroneos);
			
			if(relacionadosErroneos.isEmpty()){
				respuestaWS.setCodigoOperacion(Mensajes.EXRLGD00.getCodigo());
				respuestaWS.setDescOperacion(Mensajes.EXRLGD00.getDescripcion());
			} else {
				respuestaWS.setCodigoOperacion(Mensajes.WNRLGD00.getCodigo());
				respuestaWS.setDescOperacion(Mensajes.WNRLGD00.getDescripcion());
			}
			
		} catch (BusinessException e) {
			LOGGER.error(MENSAJE,e);
			respuestaWS.setCodigoOperacion(e.getCode());
			respuestaWS.setDescOperacion(e.getMessage());
		}
		
		return respuestaWS;
	}
	
	/**
	 * Consulta los relacionados asignados a un cuestionario
	 * @param numeroContrato Numero de contrato del cual se desean obtener los relacionados
	 * @return RelacionadosWebServiceDTO Lista con los relacionados asignados
	 */
	public RelacionadosWebServiceDTO consultarRelacionados( String numeroContrato){
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final AdministradorRelacionadosBO administradorRelacionadosBO =
				(AdministradorRelacionadosBO)springContext.getBean("BOAdministradorRelacionados");
		
		final RelacionadosWebServiceDTO respuestaWS = new RelacionadosWebServiceDTO();
		try {
			ArrayList<RelacionadoDTO> relacionados = (ArrayList<RelacionadoDTO>)
					administradorRelacionadosBO.obtenerRelacionados(numeroContrato, new ArchitechSessionBean());
			
			respuestaWS.setRelacionados(relacionados);
			respuestaWS.setCodigoOperacion(Mensajes.EXGDRL00.getCodigo());
			respuestaWS.setDescOperacion(Mensajes.EXGDRL00.getDescripcion());
		} catch (BusinessException e) {
			LOGGER.error(MENSAJE,e);
			respuestaWS.setCodigoOperacion(Mensajes.ERGDRL01.getCodigo());
			respuestaWS.setDescOperacion(
					String.format(Mensajes.ERGDRL01.getDescripcion(), e.getLocalizedMessage()));
		}
		
		return respuestaWS;
	}
	
	/**
	 * Asigna un numero de contrato a un cuestionario
	 * @param idCuestionario Identificador de cuestionario al cual se va a agregar el contrato
	 * @param numeroContrato Numero de contato que se va a asignar
	 * @return RelacionadosWebServiceDTO Resultado de la operacion
	 */
	public RespuestaWebService asignarNumeroContrato( String idCuestionario,  String numeroContrato){
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final AdministradorComponenteCentralBO administradorComponenteCentralBO =
				(AdministradorComponenteCentralBO)springContext.getBean(TXT_NOMBRE_BO_ACC);
		
		final RelacionadosWebServiceDTO respuestaWS = new RelacionadosWebServiceDTO();
		try {
			administradorComponenteCentralBO.asignarNumeroContrato(idCuestionario, numeroContrato, new ArchitechSessionBean());
			respuestaWS.setCodigoOperacion(Mensajes.EXASCT00.getCodigo());
			respuestaWS.setDescOperacion(Mensajes.EXASCT00.getDescripcion());
		} catch (BusinessException e) {
			LOGGER.error(MENSAJE,e);
			respuestaWS.setCodigoOperacion(e.getCode());
			respuestaWS.setDescOperacion(e.getMessage());
		}
		
		return respuestaWS;
	}
	
	/**
	 * Obtiene el Nivel de Riesgo e Indicador UPLD que se calculo al terminar los formularios IP e IC
	 * @param idCuestionario Identificador de cuestionario para obtener el Nivel de riesgo e indicador UPLD
	 * @return CuestionariosWebServiceDTO Objecto con la informacion de la operacion
	 */
	public CuestionarioFinalizadoDTO obtenerNivRIndU( String idCuestionario) {
			final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final AdministradorComponenteCentralBO administradorIdCuestionarioBO =
					(AdministradorComponenteCentralBO)springContext.getBean(TXT_NOMBRE_BO_ACC);
			
			CuestionarioFinalizadoDTO cuestionarioFinalizadoDto = new CuestionarioFinalizadoDTO();
			List<HashMap<String, Object>> nivelIndicador = null;
			try {
				nivelIndicador = administradorIdCuestionarioBO.obtenerNivRIndU(idCuestionario, new ArchitechSessionBean());
								
				if(nivelIndicador == null || nivelIndicador.isEmpty()){
					throw new BusinessException(Mensajes.ERAGCT07.getCodigo(), Mensajes.ERAGCT07.getDescripcion());	
				}
				
				HashMap<String, Object> registro = nivelIndicador.get(0);
				
				
				LOGGER.info(" ComponenteCentralNorkomWS -NivelR_IndicadorU: "+registro);
				cuestionarioFinalizadoDto.setIndNor(registro.get("IND_NOR").toString());
				cuestionarioFinalizadoDto.setIndPld(registro.get("IND_PLD").toString());
				cuestionarioFinalizadoDto.setIndUpld(registro.get("IND_UPLD").toString());
				cuestionarioFinalizadoDto.setNivelRgo(registro.get("NIVEL_RGO").toString());
				cuestionarioFinalizadoDto.setCodigoOperacion(Mensajes.EXOBID00.getCodigo());
				cuestionarioFinalizadoDto.setDescOperacion(Mensajes.EXOBID00.getDescripcion());
			} catch (BusinessException e) {
				LOGGER.debug(String.format("No fue posible obtener el Nivel de Riesgo y el Indicador UPLD, Codigo[%s] Mensaje[%s]", e.getCode(), e.getMessage()));
				LOGGER.error("Mensajes:",e);
				cuestionarioFinalizadoDto.setCodigoOperacion(e.getCode());
				cuestionarioFinalizadoDto.setDescOperacion(e.getMessage());
			}
			
			return cuestionarioFinalizadoDto;
		}
}
