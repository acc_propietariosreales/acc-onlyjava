/**
 * Clase de Fachada para la actualiacion de
 * los relacionados a ok
 * por bloque
 * NorkomC_WS CruzeRelacionadosFacade
 */
package mx.isban.norkom.facade;

import javax.servlet.ServletContext;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.EnvioRelacionadosBO;
import mx.isban.norkom.cuestionario.util.EstatusRelacionados;
import mx.isban.norkom.ws.dto.ActualizaEstatusRelacionados;
import mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO;
import mx.isban.norkom.ws.dto.RespuestaWebService;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Fachada para el WS
 * de Relacionados por bloques
 * para exponer lo metodos de actualizacion
 * de estatus a ok por norkom
 * y hacer uso del contexto de Spring
 * @author Leopoldo F Espinosa R 13/07/2017
 *
 */
public class CruzeRelacionadosFacade {
	/** 
	 * Variable utilizada para escribir
	 *  el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(CruzeRelacionadosFacade.class);
	/**
	 * constante para Mensajes
	 * */
	private static final String MENSAJE="Mensaje";
	/**
	 * contexto para leer la configuracion
	 *  de Spring
	 ***/
	private final ServletContext servletContext;
	/**
	 * Constructor para ComponenteCentralFacade
	 * @param servletContext contexto para spring 
	 **/
	public CruzeRelacionadosFacade(ServletContext servletContext) {
		this.servletContext=servletContext;
	}
	/** 
	 * Actualiza el estatus de los relacionados 
	 * a un Cuestionario segun el id de cuestionario 
	 * y bloque ingresados
	 * @param relacionadosWS datos para la actualizacion
	 * @return RespuestaWebService Respuesta de la operacion de estatus de  relacionados
	 */
	public RespuestaWebService actualizaEstatusOK(ActualizaEstatusRelacionados relacionadosWS){
		RespuestaWebService respuesta= new RespuestaWebService();
		boolean validacionNoOK=false;
		if(relacionadosWS.getIdformulario()==null || relacionadosWS.getIdformulario().isEmpty() ){
			respuesta.setCodigoOperacion("ERVAL01");
			respuesta.setDescOperacion("No se ha informado el id de formulario");
			validacionNoOK=true;
		}
		if(!validacionNoOK && (relacionadosWS.getNumeroBloque()==null || relacionadosWS.getNumeroBloque().isEmpty()) ){
			respuesta.setCodigoOperacion("ERVAL02");
			respuesta.setDescOperacion("No se ha informado el Numero de Bloque");
			validacionNoOK=true;
		}
		if(!validacionNoOK){
			final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final EnvioRelacionadosBO envioRelacionadosBo =(EnvioRelacionadosBO)springContext.getBean("BOEnvioRelacionados");
			
			final RequestDatosRelacionadosWSDTO peticionactualizacion = new RequestDatosRelacionadosWSDTO();
			peticionactualizacion.setIdCuestionario(relacionadosWS.getIdformulario());
			peticionactualizacion.setNumeroBloque(relacionadosWS.getNumeroBloque());
			
			try {
				envioRelacionadosBo.actualizarEstatusRel(peticionactualizacion,EstatusRelacionados.OK.toString(), new ArchitechSessionBean());
				respuesta.setCodigoOperacion("OKACT01");
				respuesta.setDescOperacion("Se realizo la actualizacion de Estatus");
			} catch (BusinessException e) {
				LOGGER.error(MENSAJE,e);
				respuesta.setCodigoOperacion("ERACT01");
				respuesta.setDescOperacion("No se realizo la actualizacion de Estatus:"+e.getMessage());
			}
		}
		return respuesta;
	}
}
