/**
 * NorkomC NivelRiesgoFacade.java
 */
package mx.isban.norkom.facade;

import javax.servlet.ServletContext;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.PersonasBO;
import mx.isban.norkom.cuestionario.dto.IndicadorOPD2WS;
import mx.isban.norkom.ws.dto.RespuestaWebService;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author Leopoldo F Espinosa R 20/04/2015
 *
 */
public class NivelRiesgoFacade {

	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOG = Logger.getLogger(NivelRiesgoFacade.class);
	
	/**
	 * Codigo de operacion exitosa
	 */
	private static final String COD_OP_EXITOSA = "EX0001";
	
	/**
	 * Codigo de operacion erronea
	 */
	private static final String COD_ERROR_COMUNICACION = "ER0000";
	
	/** 
	 * Variable utilizada para determinar operacion exitosa 
	 */
	private static final String OP_EXITOSA = "Operacion exitosa";
	
	/**contexto para leer la configuracion de Spring**/
	private final transient ServletContext servletContext;
	/**
	 * constructor NivelRiesgoFacade
	 * @param servletContext contexto para spring
	 */
	public NivelRiesgoFacade(ServletContext servletContext) {
		this.servletContext=servletContext;
	}
	/**
	 * Realiza la actualizacion del Nivel de Riesgo y del Indicador UPLD en personas
	 * @param buc Cliente al que se le va a actualizar la informacion
	 * @param nivelRiesgo Nivel de Riesgo que se va a asignar
	 * @param indicadorUpld Indicador UPLD que se va a asignar
	 * @param claveEmpleado Clave del empleado que esta realizando el cambio
	 * @param fechaActualizacion Fecha en que se realizo el cambio
	 * @param idNorkom Identificador de Norkom
	 * @return Codigo y mensaje de la operacion
	 */
	public RespuestaWebService actualizarNivelRiesgoManual( String buc, String nivelRiesgo, 
			 String indicadorUpld, String claveEmpleado, String fechaActualizacion, String idNorkom){
		
		LOG.info(String.format("actualizarNivelRiesgoManual NKM ::: Parametros buc[%s] nivelRiesgo[%s] indicadorUpld[%s] idNorkom[%s] fechaActualizacion[%s]", 
				buc, nivelRiesgo, indicadorUpld, idNorkom, fechaActualizacion));

		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final PersonasBO personasBO = (PersonasBO)springContext.getBean("BOPersonas");
		RespuestaWebService respuesta = new RespuestaWebService();
		
		try {
			IndicadorOPD2WS indicadorODP2 = new IndicadorOPD2WS();
			indicadorODP2.setBuc(buc);
			indicadorODP2.setNivelRiesgo(nivelRiesgo);
			indicadorODP2.setIndicadorUpld(indicadorUpld);
			indicadorODP2.setIdNorkom(idNorkom);
			indicadorODP2.setFechaActualizacion(fechaActualizacion);
			
			personasBO.ejecutarActualizacionCalificacionManual(indicadorODP2, new ArchitechSessionBean());
			
			if(PersonasBO.ERR_DATOS_ID_NORKOM.equals(respuesta.getCodigoOperacion()) || PersonasBO.ERR_DATOS_BUC.equals(respuesta.getCodigoOperacion()) ||
				PersonasBO.ERR_DATOS_NIVEL_RIESGO.equals(respuesta.getCodigoOperacion()) || PersonasBO.ERR_DATOS_IND_UPLD.equals(respuesta.getCodigoOperacion())){
				respuesta.setCodigoOperacion(respuesta.getCodigoOperacion());
				respuesta.setDescOperacion(respuesta.getDescOperacion());
			}else{
				respuesta.setCodigoOperacion(COD_OP_EXITOSA);
				respuesta.setDescOperacion(OP_EXITOSA);
			}
		} catch (BusinessException e) {
			LOG.error("Error:",e);
			
			if(PersonasBO.ERR_DATOS_BUC.equals(e.getCode()) || PersonasBO.ERR_DATOS_ID_NORKOM.equals(e.getCode()) || 
					PersonasBO.ERR_DATOS_NIVEL_RIESGO.equals(e.getCode()) || PersonasBO.ERR_DATOS_IND_UPLD.equals(e.getCode()))
			{
				respuesta.setCodigoOperacion(e.getCode());
				respuesta.setDescOperacion(e.getMessage());
			} else if(PersonasBO.ERR_DATOS_NIVEL_RIESGO_INCORRECTO.equals(e.getCode()) || PersonasBO.ERR_DATOS_IND_UPLD_INCORRECTO.equals(e.getCode())) {
				respuesta.setCodigoOperacion(e.getCode());
				respuesta.setDescOperacion(e.getMessage());
			} else {
				respuesta.setCodigoOperacion(COD_ERROR_COMUNICACION);
				respuesta.setDescOperacion("Error de comunicacion durante la actualizacion del Nivel de Riesgo");
			}
			
		}
		return respuesta;
	}
}
