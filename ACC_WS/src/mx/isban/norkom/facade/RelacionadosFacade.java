/**
 * Clase de Fachada para relacionados
 * por bloque
 * NorkomC_WS RelacionadosFacade.java
 */
package mx.isban.norkom.facade;

import java.util.ArrayList;

import javax.servlet.ServletContext;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.RelacionadosBloqueBO;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosRequestDTO;
import mx.isban.norkom.ws.dto.RelacionadosResponseDTO;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Fachada para el WS
 * de Relacionados por bloques
 * para exponer lo metodos de Agregar relacionados
 * y hacer uso del contexto de Spring
 * @author Leopoldo F Espinosa R 13/06/2017
 *
 */
public class RelacionadosFacade {
	/** 
	 * Variable utilizada para escribir
	 *  el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(RelacionadosFacade.class);
	/**
	 * constante para Mensajes
	 * */
	private static final String MENSAJE="Mensaje";
	/**
	 * contexto para leer la configuracion
	 *  de Spring**/
	private final ServletContext servletContext;
	/**
	 * Constructor para ComponenteCentralFacade
	 * @param servletContext contexto para spring 
	 **/
	public RelacionadosFacade(ServletContext servletContext) {
		this.servletContext=servletContext;
	}
	/** 
	 * Agrega relacionados a un Cuestionario
	 * segun el id de cuestionario y bloque ingresados
	 * @param relacionadosWS Lista de relacionados que se desean agregar
	 * @return RelacionadosWebServiceDTO Respuesta de la operacion de agregar relacionados
	 */
	public RelacionadosResponseDTO agregarRelacionados(RelacionadosRequestDTO relacionadosWS){
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RelacionadosBloqueBO administradorRelacionadosBO =
				(RelacionadosBloqueBO)springContext.getBean("BORelacionadosBloque");
		
		final RelacionadosResponseDTO respuestaRelacionadosWS = new RelacionadosResponseDTO();
		respuestaRelacionadosWS.setBloque(relacionadosWS.getBloque());
		respuestaRelacionadosWS.setIdCuestionario(relacionadosWS.getIdCuestionario());
		try {
			ArrayList<RelacionadoDTO> relacionados = (ArrayList<RelacionadoDTO>)
					administradorRelacionadosBO.agregarRelacionados(relacionadosWS, new ArchitechSessionBean());
			
			respuestaRelacionadosWS.setRelacionados(relacionados);
			
			if(relacionados.isEmpty()){
				respuestaRelacionadosWS.setCodigoOperacion(Mensajes.EXRLGD00.getCodigo());
				respuestaRelacionadosWS.setDescOperacion(Mensajes.EXRLGD00.getDescripcion());
			} else {
				respuestaRelacionadosWS.setCodigoOperacion(Mensajes.WNRLGD00.getCodigo());
				respuestaRelacionadosWS.setDescOperacion(Mensajes.WNRLGD00.getDescripcion());
			}
			
		} catch (BusinessException e) {
			LOGGER.error(MENSAJE,e);
			respuestaRelacionadosWS.setCodigoOperacion(e.getCode());
			respuestaRelacionadosWS.setDescOperacion(e.getMessage());
		}
		
		return respuestaRelacionadosWS;
	}
}
