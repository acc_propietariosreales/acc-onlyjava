package mx.isban.norkom.properties;

/**
 * @author J ULISES GARCIA MARTINEZ
 * Objetivo: Obtener las propiedades para la obtencion de llaves privadas y publicas asi como de las validaciones de JWT
 * Justificacion: Se almacenara de manera estatica todos los valores necesarios para obtener las llaves privada y publicas de un Keystores asi como los valores 'Audience' y 'Issuer' utilizados en la validacion de un JWT 
 */
public final class ConfigApp {
	/** Ruta del Keystore que contiene la llave publica */
	private static String rutaKeystoreLlavePublica;
	/** Password del keystore */
	private static String pwdKeystore;
	/** Alias del keystore */
	private static String aliasKeystore;
	/** Llave de descifradod del password */
	private static String llaveDescifrado;
	/** Tipo de Keystore en el que se encuentra la llave */
	private static String tipoKeyStore;
	/** Audiencia a la que va dirido el JWT */
	private static String jwtAudience;
	/** Emisor del JWT */
	private static String jwtIssuer;
	
	/**
	 * Obtiene el valor de rutaKeystoreLlavePublica
	 * @return El valor de rutaKeystoreLlavePublica
	 */
	public static String getRutaKeystoreLlavePublica() {
		return rutaKeystoreLlavePublica;
	}
	/**
	 * Define el nuevo valor para rutaKeystoreLlavePublica
	 * @param rutaKeystoreLlavePublica El nuevo valor de rutaKeystoreLlavePublica
	 */
	public static void setRutaKeystoreLlavePublica(String rutaKeystoreLlavePublica) {
		ConfigApp.rutaKeystoreLlavePublica = rutaKeystoreLlavePublica;
	}
	/**
	 * Obtiene el valor de pwdKeystore
	 * @return El valor de pwdKeystore
	 */
	public static String getPwdKeystore() {
		return pwdKeystore;
	}
	/**
	 * Define el nuevo valor para pwdKeystore
	 * @param pwdKeystore El nuevo valor de pwdKeystore
	 */
	public static void setPwdKeystore(String pwdKeystore) {
		ConfigApp.pwdKeystore = pwdKeystore;
	}
	/**
	 * Obtiene el valor de aliasKeystore
	 * @return El valor de aliasKeystore
	 */
	public static String getAliasKeystore() {
		return aliasKeystore;
	}
	/**
	 * Define el nuevo valor para aliasKeystore
	 * @param aliasKeystore El nuevo valor de aliasKeystore
	 */
	public static void setAliasKeystore(String aliasKeystore) {
		ConfigApp.aliasKeystore = aliasKeystore;
	}
	/**
	 * Obtiene el valor de llaveDescifrado
	 * @return El valor de llaveDescifrado
	 */
	public static String getLlaveDescifrado() {
		return llaveDescifrado;
	}
	/**
	 * Define el nuevo valor para llaveDescifrado
	 * @param llaveDescifrado El nuevo valor de llaveDescifrado
	 */
	public static void setLlaveDescifrado(String llaveDescifrado) {
		ConfigApp.llaveDescifrado = llaveDescifrado;
	}
	/**
	 * Obtiene el valor de tipoKeyStore
	 * @return El valor de tipoKeyStore
	 */
	public static String getTipoKeyStore() {
		return tipoKeyStore;
	}
	/**
	 * Define el nuevo valor para tipoKeyStore
	 * @param tipoKeyStore El nuevo valor de tipoKeyStore
	 */
	public static void setTipoKeyStore(String tipoKeyStore) {
		ConfigApp.tipoKeyStore = tipoKeyStore;
	}
	/**
	 * Obtiene el valor de jwtAudience
	 * @return El valor de jwtAudience
	 */
	public static String getJwtAudience() {
		return jwtAudience;
	}
	/**
	 * Define el nuevo valor para jwtAudience
	 * @param jwtAudience El nuevo valor de jwtAudience
	 */
	public static void setJwtAudience(String jwtAudience) {
		ConfigApp.jwtAudience = jwtAudience;
	}
	/**
	 * Obtiene el valor de jwtIssuer
	 * @return El valor de jwtIssuer
	 */
	public static String getJwtIssuer() {
		return jwtIssuer;
	}
	/**
	 * Define el nuevo valor para jwtIssuer
	 * @param jwtIssuer El nuevo valor de jwtIssuer
	 */
	public static void setJwtIssuer(String jwtIssuer) {
		ConfigApp.jwtIssuer = jwtIssuer;
	}
}
