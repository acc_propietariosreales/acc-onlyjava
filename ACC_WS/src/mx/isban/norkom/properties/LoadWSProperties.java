package mx.isban.norkom.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * Listener del ServletContext para la carga de las configuraciones
 * @author J ULISES GARCIA MARTINEZ
 * Objetivo: Obtener las propiedades para la obtencion de llaves privadas y publicas asi como de las validaciones de JWT
 * Justificacion: Es necesario cargar en una sola ocasion a traves de ServletContextListener todas las propiedades necesarias para trabajar con las llaves y el JWT
 */
public class LoadWSProperties implements ServletContextListener {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(LoadWSProperties.class);
	/** Constante para la ruta del keystore que contiene la llave publica */
	private static final String RUTA_KS_LLAVE_PUBLICA = "RUTA_KS_LLAVE_PUBLICA";
	/** Constante para el password del keystore */
	private static final String PWD_KEYSTORE = "PWD_KEYSTORE";
	/** Constante para el alias del keystore */
	private static final String ALIAS_KEYSTORE = "ALIAS_KEYSTORE";
	/** Constante para la llave de descifrado del password */
	private static final String LLAVE_DESCIFRADO_PWD = "LLAVE_DESCIFRADO_PWD";
	/** Constante para el tipo de Keystore */
	private static final String SK_TIPO_KEYSTORE = "SK_TIPO_KEYSTORE";
	/** Constante para cargar la Audiencia del JWT */
	private static final String JWT_PROP_AUDIENCE = "JWT_PROP_AUDIENCE";
	/** Constante para cargar el Emisor del JWT */
	private static final String JWT_PROP_ISSUER = "JWT_PROP_ISSUER";
	/** Constante para el texto "classpath:" que se utilizara para definir tipo de lectura del archiv */
	private static final String PREFIJO_PROPERTIES = "classpath:";

	/**
	 * N/A
     * @param arg0 Parametro no utilizado en el metodo 
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         //No es necesario realizar operaciones
    }

	/**
	 * Cargara las porpiedades de configuraci\u00f3n para el aplicativo
     * @param arg0 ServletContextEvent Utilizado para obtener la ruta del archivo de propiedades
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
    	Properties prop =  new Properties();
    	try {
            String path = arg0.getServletContext().getInitParameter("ComponenteCentralWSConfig");
            
            final InputStream is;
            if(path.startsWith(PREFIJO_PROPERTIES)){
            	is = arg0.getServletContext().getResourceAsStream(path.substring(PREFIJO_PROPERTIES.length()));
            }else{
            	is = new FileInputStream(path);
            }
            try {
                prop.load(is);
            } finally {
                is.close();
            }
        } catch (IOException asd) {
        	LOGGER.error("Ha ocurrido un error al cargar el archivo de propiedades.", asd);
            throw new IllegalArgumentException("Ha ocurrido un error al cargar el archivo de propiedades.",asd);
        }

    	if(prop.containsKey(RUTA_KS_LLAVE_PUBLICA)){
    		ConfigApp.setRutaKeystoreLlavePublica(prop.getProperty(RUTA_KS_LLAVE_PUBLICA, StringUtils.EMPTY));
    	}

        if(prop.containsKey(PWD_KEYSTORE)){
    		ConfigApp.setPwdKeystore(prop.getProperty(PWD_KEYSTORE, StringUtils.EMPTY));
    	}

        if(prop.containsKey(ALIAS_KEYSTORE)){
    		ConfigApp.setAliasKeystore(prop.getProperty(ALIAS_KEYSTORE, StringUtils.EMPTY));
    	}
        
        if(prop.containsKey(LLAVE_DESCIFRADO_PWD)){
        	ConfigApp.setLlaveDescifrado(prop.getProperty(LLAVE_DESCIFRADO_PWD, StringUtils.EMPTY));
        }
        
        if(prop.containsKey(SK_TIPO_KEYSTORE)){
        	ConfigApp.setTipoKeyStore(prop.getProperty(SK_TIPO_KEYSTORE, StringUtils.EMPTY));
        }
        
        if(prop.containsKey(JWT_PROP_AUDIENCE)){
        	ConfigApp.setJwtAudience(prop.getProperty(JWT_PROP_AUDIENCE, StringUtils.EMPTY));
        }
        
        if(prop.containsKey(JWT_PROP_ISSUER)){
        	ConfigApp.setJwtIssuer(prop.getProperty(JWT_PROP_ISSUER, StringUtils.EMPTY));
        }
    	
    }
}
