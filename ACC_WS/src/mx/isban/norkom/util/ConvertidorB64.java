/**
 * Isban Mexico
 *   Clase: ConvertidorB64.java
 *   Descripcion: Componente para convertir a base 64.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.norkom.ws.dto.ReporteBase64WebServiceDTO;


/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
public final class ConvertidorB64 {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(ConvertidorB64.class);
	
	/**
	 * Constructor predeterminado
	 */
	private ConvertidorB64(){
		LOGGER.info("No es necesario crear instancias debido a que sus metodos son estaticos");
	}
	
	/**
	 * Convierte un elemento de bytes a Base64 y lo regresa como una cadena
	 * @param elemento a convertir en Base 64
	 * @return string de cadena en base 64
	 */
	private static String convierteB64(byte[] elemento){
		if(elemento!=null){
			return new String(Base64.encodeBase64(elemento));
		}else{
			return "";
		}
		
	}
	
	/**
	 * Convierte a Base64 dejando la respuesta
	 * @param respuestaWs dto a agregar datos de base 64
	 * @param bytes reportes a agregar
	 */
	public static void convierteBase64(ReporteBase64WebServiceDTO respuestaWs, byte[] bytes) {
		//obtenemos el arreglo de bytes y lo mandamos codificar
		LOGGER.info("Inicia conversion Reportes IP e IC base64");
		final String base64pdf = convierteB64(bytes);
		respuestaWs.setPdfBase64(base64pdf);
		respuestaWs.setCodigoOperacion(ReporteBase64WebServiceDTO.COD_EXITO);
		respuestaWs.setDescOperacion(ReporteBase64WebServiceDTO.MSJ_EXITO);
		LOGGER.info("Termina conversion Reportes IP e IC base64");
	}
	
	public static byte[] decodificaBase64Bytes(String data) {
		byte [] b = null;
		if (StringUtils.isNotBlank(data)) {
			b = Base64.decodeBase64(data.getBytes());
		}
		return b;
	}
}
