/**
 * Isban Mexico
 *   Clase: GeneraReportesByte.java
 *   Descripcion: Componente para generar los cuestionarios en formato pdf.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.cuestionario.util.Cuestionarios;
import mx.isban.norkom.cuestionario.util.Mensajes;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.log4j.Logger;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Clase principal par ala generacion de los reportes en formato PDF 
 * @author STEFANINI (Leopoldo F Espinosa R) 05/02/2015
 * 
 */
public class GeneraReportesByte {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(GeneraReportesByte.class);
	/**ruta base de los reportes*/
	private static final String RUTA_BASE_REPORTES="/mx/isban/norkom/reportes";
	/**SUBREPORT_DIR*/
	private static final String SUBREPORT_DIR="SUBREPORT_DIR";
	/**constante para Mensaje*/
	private static final String MESAJE_ERR="Mensaje";
	/**Arreglo de bytes para guardar el reporte*/
	private byte[] reporte; 
	
	/**
	 * Genera el reporte en formato PDF
	 * @param datos datos para el reporte
	 * @param ip identificador para saber si es el cuestionario ip o no
	 * @throws BusinessException con mensaje de error
	 */
	public GeneraReportesByte(ResponseObtenerCuestionario datos,boolean ip) throws BusinessException {
		
		this.reporte = null;
		LOGGER.info("Reporte a generar: "+datos.getTipo()!=null?datos.getTipo().getValor():"-No se proporciono el tipo-");
		if(ip){
			generarPreliminar(datos);			
		}else {
			if(Cuestionarios.TIPO_IC_A1.equalsObject(datos.getTipo())){
				datos.getDatosReporte().put(SUBREPORT_DIR, RUTA_BASE_REPORTES + "/a1/");
				generarIC("A1",datos);
			}else if(Cuestionarios.TIPO_IC_A2.equalsObject(datos.getTipo())){
				datos.getDatosReporte().put(SUBREPORT_DIR, RUTA_BASE_REPORTES + "/a2/");
				generarIC("A2",datos);
			}else if(Cuestionarios.TIPO_IC_A3.equalsObject(datos.getTipo())){
				datos.getDatosReporte().put(SUBREPORT_DIR, RUTA_BASE_REPORTES + "/a3/");
				generarIC("A3",datos);
			}
		}
		
	}
	/**
	 * genera el reporte de informacion preliminar
	 * @param datos Mapa con los datos para generar el reporte
	 * @throws BusinessException con mensaje de error
	 */
	private void generarPreliminar(ResponseObtenerCuestionario datos) throws BusinessException {
		LOGGER.info("Entramos a generar el reporte");
		datos.getDatosReporte().put("strLogoSantander", RUTA_BASE_REPORTES+"/Santander.png");
		datos.getDatosReporte().put(SUBREPORT_DIR, RUTA_BASE_REPORTES + "\\ip\\");
		datos.getDatosReporte().put("strResultadoFinal", datos.getCuestionarioDTO().getNivelRiesgo());//COD_NIVEL_RIESG
		datos.getDatosReporte().put("StrBcomCode", "BCOM-498 (082012)");
        String nombreReporte = obtenNombreReporte("IP_personas_",datos,"v1.jrxml");
        InputStream is=null;
        LOGGER.debug("Reporte a generar:"+nombreReporte);
    	try {
    		JRBeanCollectionDataSource dataSource = new 
            JRBeanCollectionDataSource(Arrays.asList(datos.getListasBeanIC()));
    		is=this.getClass().getResourceAsStream(RUTA_BASE_REPORTES+"/ip/"+nombreReporte);
            JasperReport jasperReport = JasperCompileManager.compileReport(is);
            validaOpciones(datos);
            byte[] datosbytes = JasperRunManager.runReportToPdf(jasperReport,datos.getDatosReporte() ,dataSource);
            
            if(datosbytes!=null && datosbytes.length>0){
            	this.reporte = datosbytes;
            }
            
        } catch (JRException err) {
        	LOGGER.error(MESAJE_ERR,err);
        	LOGGER.error("Reporte no generado: JRException"+err.getMessage());
        	throw new mx.isban.norkom.cuestionario.exception.BusinessException(Mensajes.ERR_EXP_PDF_IP.getCodigo(),Mensajes.ERR_EXP_PDF_IP.getDescripcion(),err);
        }finally{
        	if(is!=null){
        		try{
        			is.close();
        		} catch (IOException ioe) {
                	LOGGER.error(MESAJE_ERR,ioe);
                }
        	}
        }
    	
        LOGGER.info("Termina generacion de Reporte...");
	}
	/**
	 * 
	 * @param datos ResponseObtenerCuestionario para la validacion de las opciones
	 */
	private void validaOpciones(ResponseObtenerCuestionario datos) {
		if(datos.getDatosReporte()!=null){
			rellenaValorDefault(datos, "strNroTransInterEnviadas", "0");
			rellenaValorDefault(datos,"strMtoTransInterEnviadas", "0");
			rellenaValorDefault(datos,"strNroTransInterRecibidas", "0");
			rellenaValorDefault(datos,"strMtoTransInterRecibidas", "0");
			rellenaValorDefault(datos,"strNroTransNacEnviadas", "0");
			rellenaValorDefault(datos,"strMtoTransNacEnviadas", "0");
			rellenaValorDefault(datos,"strNroTransNacRecibidas", "0");
			rellenaValorDefault(datos,"strMtoTransNacRecibidas", "0");
			rellenaValorDefault(datos,"strNroDepositoEfectivo", "0");
			rellenaValorDefault(datos,"strMtoDepositoEfectivo", "0");
			rellenaValorDefault(datos,"strNroDepositoNoEfectivo", "0");
			rellenaValorDefault(datos,"strMtoDepositoNoEfectivo", "0");
			rellenaValorDefault(datos,"strNroRetiroEfectivo", "0");
			rellenaValorDefault(datos,"strMtoRetiroEfectivo", "0");
			rellenaValorDefault(datos,"strNroRetiroNoEfectivo", "0");
			rellenaValorDefault(datos,"strMtoRetiroNoEfectivo", "0");
			rellenaValorDefault(datos,"strNroCompraDivisas", "0");
			rellenaValorDefault(datos,"strMtoCompraDivisas", "0");
			rellenaValorDefault(datos,"strNroVentaDivisas", "0");
			rellenaValorDefault(datos,"strMtoVentaDivisas", "0");
		}
		
	}
	/**
	 * rellena el valor default de una llave si no esta
	 * @param llave llave a buscar y rellenar
	 * @param datos a donde agregar
	 * @param valor valor de dafault
	 */
	private void rellenaValorDefault(ResponseObtenerCuestionario datos,String llave,String valor){
		if(!datos.getDatosReporte().containsKey(llave)){
			datos.getDatosReporte().put(llave, valor);
		}else{
			datos.getDatosReporte().put(llave, quitaCero((String)datos.getDatosReporte().get(llave),true));
		}
	}
	/**
	 * 
	 * @param object cadena a validar si tiene el cero incialmente y lo reducimos en uno si se agrega  la opcion reducir
	 * @param reducir variable para indicar si se reduce en uno o no
	 * @return cadena sin cero inicial
	 */
	private String quitaCero(String object,boolean reducir) {
		if(object!=null && object.startsWith("0")){
			object=object.replaceFirst("0", "");
		}
		if(reducir){
			try{
			object= String.valueOf((Integer.parseInt(object)-1));
			}catch(NumberFormatException e){
				LOGGER.debug("noes un numero valido.."+e.getMessage());
			}
		}
		return object;
	}
	/***
	 * Generamos la informacion del cuestionario de tipo Conmplementario A1
	 * @param reporte el reporte
	 * @param datos datos para generar el reporte
	 * @throws BusinessException con mensaje de error
	 */
	private void generarIC(String reporte,ResponseObtenerCuestionario datos) throws BusinessException {
        JRBeanCollectionDataSource dataSource = new 
        JRBeanCollectionDataSource(Arrays.asList(datos.getListasBeanIC()));
        datos.getDatosReporte().put("strLogoSantander", RUTA_BASE_REPORTES+"/Santander.png");
        String nombreReporte = obtenNombreReporte("IC"+reporte+"_personas_",datos,"v1.jrxml");
        datos.getDatosReporte().put("StrBcomCode", "BCOM-498 (082012)");
        LOGGER.info("Reporte a generar: "+ nombreReporte);
        rellenaValorDefault(datos, "strVentaAnual", "");
		rellenaValorDefault(datos,"strNroEmpleados", "");
        InputStream is=null;
    	if(!nombreReporte.isEmpty()){
	    	try {
	            is=this.getClass().getResourceAsStream(RUTA_BASE_REPORTES+"/"+reporte.toLowerCase()+"/"+nombreReporte);
	            JasperReport jasperReport = JasperCompileManager.compileReport(is);
	            LOGGER.debug("Reporte a generado: ");        
	            byte[] datosbyte = JasperRunManager.runReportToPdf(jasperReport,datos.getDatosReporte() , dataSource);
	            
	            if(datosbyte!=null && datosbyte.length>0){
	            	LOGGER.debug("agregando reporte generado: ");
	            	this.reporte = datosbyte;
	            }
	        } catch (JRException err) {
	        	LOGGER.error(MESAJE_ERR,err);
	        	throw new mx.isban.norkom.cuestionario.exception.BusinessException(Mensajes.ERR_EXP_PDF_IC.getCodigo(),Mensajes.ERR_EXP_PDF_IC.getDescripcion(),err);
	        }finally{
	        	if(is!=null){
	        		try{
	        			is.close();
	        		} catch (IOException ioe) {
	                	LOGGER.error(MESAJE_ERR,ioe);
	                }
	        	}
	        }
		}
	}
	/***
	 * obtenemos el tipo de reporte segun el tipo de persona y moneda
	 * @param tipoRep inicio de reporte 
	 * @param datos ResponseObtenerCuestionario
	 * @param finReporte de cadena de reporte ejemplo v1.jrxml
	 * @return tipo de reporte
	 */
	private String obtenNombreReporte(String tipoRep,ResponseObtenerCuestionario datos,String finReporte) {
		StringBuilder nombreReporte = new StringBuilder();
		nombreReporte.append(tipoRep);
		
		if(Cuestionarios.PERSONA_FISICA.equalsObject(datos.getTipoPersona())){
			nombreReporte.append("fisicas_");
	    }else if(Cuestionarios.PERSONA_MORAL.equalsObject(datos.getTipoPersona())){
	    	nombreReporte.append("moral_");
	    }else if(Cuestionarios.PERSONA_FISICA_AE.equalsObject(datos.getTipoPersona())){
	    	nombreReporte.append("fisicasAE_");
	    }else{
	    	nombreReporte.append("fisicas_");
	    }
	    if(Cuestionarios.TIPO_MONEDA_PESOS.equalsObject(datos.getTipoMoneda())){
	    	nombreReporte.append("pes_");
	    }else if(Cuestionarios.TIPO_MONEDA_DOLARES.equalsObject(datos.getTipoMoneda())){
	    	nombreReporte.append("dol_");
	    }else{
	    	nombreReporte.append("pes_");
	    }
	    return nombreReporte+finReporte;
	}

	/**
	 * Obtenemos el resultado del reporte en un arreglo de bytes
	 * @return byte[]
	 */
	public byte[] getReporte() {
		return reporte.clone();
	}

	
	 /**
	 * Retorna  un array de bytes de los pdfs unidos
	 * @param pdfFiles pdf
	 * @return byte con todos loes reportes generados
	 * @throws BusinessException mensaje de erro
	 */
     
    public byte[] concatenarPDFs(List<byte[]> pdfFiles) throws BusinessException {
    	
    	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    	boolean paginate = true;
    	
        Document document = new Document();
        try {
            List<PdfReader> readers = new ArrayList<PdfReader>();
            
            for(int i=0; i<pdfFiles.size(); i++){
            	if(pdfFiles.get(i)!=null){
            		PdfReader pdfReader = new PdfReader(pdfFiles.get(i));
	                readers.add(pdfReader);
            	}
            }
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
 
            document.open();
            PdfContentByte cb = writer.getDirectContent();
 
            PdfImportedPage page;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
 
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                LOGGER.debug("paginas::::pdfReader-> " +pdfReader.getNumberOfPages());
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
                    document.setPageSize(rectangle);
                    document.newPage();
                    pageOfCurrentReaderPDF++;
                    page = writer.getImportedPage(pdfReader,pageOfCurrentReaderPDF);

                    agregarTemplate(cb, rectangle, page, pdfReader);
                    
                    if (paginate) {
                        cb.beginText();
                        cb.getPdfDocument().getPageSize();
                        cb.endText();
                    }
                }
                pageOfCurrentReaderPDF = 0;
            }
            outputStream.flush();
            
        } catch (IOException e) {
         	LOGGER.error(MESAJE_ERR,e);
         	throw new mx.isban.norkom.cuestionario.exception.BusinessException(this.getClass().getName(),
 					"ERGR002", "Problemas al generar el documento",e);
		} catch (DocumentException e) {
			LOGGER.error(MESAJE_ERR,e);
         	throw new mx.isban.norkom.cuestionario.exception.BusinessException(this.getClass().getName(),
 					"ERGR002", "Problemas al generar el documento",e);
		} finally {
            if (document.isOpen()){
            	document.close();
            }
            
            try {
                if (outputStream != null){
                	outputStream.close();
                }
            } catch (IOException ioe) {
            	LOGGER.error(MESAJE_ERR,ioe);
            }
        }
        return outputStream.toByteArray();
    }
    
    /**
     * Define un nuevo template para el archivo PDF
     * @param cb Objeto al cual se le define el template
     * @param rectangle Objeto del cual se obtiene la rotacion que tiene el documento
     * @param page Pagina con el tempalte
     * @param pdfReader Objeto para obtener la altura o el ancho del documento
     */
    private void agregarTemplate(PdfContentByte cb, Rectangle rectangle, PdfImportedPage page, PdfReader pdfReader) {
    	switch (rectangle.getRotation()) {
        case 0:
            cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
            break;
        case 90:
            cb.addTemplate(page, 0, -1f, 1f, 0, 0, pdfReader.getPageSizeWithRotation(1).getHeight());
            break;
        case 180:
            cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
            break;
        case 270:
            cb.addTemplate(page, 0, 1.0F, -1.0F, 0, pdfReader.getPageSizeWithRotation(1).getWidth(), 0);
            break;
        default:
            break;
        }
    }
	/**
	 * @param reporte 
	 * el reporte a agregar
	 */
	public void setReporte(byte[] reporte) {
		this.reporte = reporte;
	}
}
