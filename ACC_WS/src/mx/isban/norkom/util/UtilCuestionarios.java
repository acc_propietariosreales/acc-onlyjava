/**
 * Isban Mexico
 *   Clase: UtilCuestionarios.java
 *   Descripcion: Componente de utileria para los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 *   
 * Objetivo: Validar los datos de entrada
 * Justificacion: Validar en un unico objeto los datos de entrada al realizar la peticion de almacenamiento de cuestionario
 */
package mx.isban.norkom.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.UtileriasHelper;

public final class UtilCuestionarios {
	/**
	 * Codigo de pais para Mexico
	 */
	private static final String COD_PAIS_MX = "052";
	/**
	 * Elemento utilizado para la bitacorizacion de las operaciones
	 */
	private static final Logger LOG = Logger.getLogger(UtilCuestionarios.class);
	/**Constructor**/
	private UtilCuestionarios(){
		
	}
	/**
	 * Valida los datos que fueron enviados en la peticion
	 * @param cuestionario Datos a validar
	 * @return CuestionarioDTO obejeto con datos de cuestionario
	 * @throws BusinessException con mensaje de error
	 */
	public static CuestionarioDTO validarDatosPeticionConsulta(CuestionarioDTO cuestionario) throws BusinessException{
		ActividadEconomicaDTO actividadGenerica = cuestionario.getActividadGenerica();
		ActividadEconomicaDTO actividadEspecifica = cuestionario.getActividadEspecifica();
		
		if(validaParametro(cuestionario.getIdCuestionario())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Identificador de Cuestionario");
		}else if(validaformatoIdCuest(cuestionario.getIdCuestionario())){
			throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, sin este dato no es posible realizar la operaci\u00F3n ");
		}
			
		obtenerDatosTipoCuestionario(cuestionario);
		obtenerDatosCliente(cuestionario);
		obtenerDatosSucursal(cuestionario);
		
		if(validaParametro(cuestionario.getCodigoSegmento())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Segmento");
		}
		
		if(validaParametro(cuestionario.getDescSegmento())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Descripci\u00F3n del Segmento");
		}
		
		if(validaParametro(actividadGenerica.getCodigo())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Actividad Generica");
		}
		
		if(validaParametro(actividadEspecifica.getCodigo())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Actividad Especifica");
		}
		
		obtenerProductoSubproducto(cuestionario);
		
		if(validaParametro(cuestionario.getCodigoPais())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Pa\u00EDs");
		}
		
		if(validaParametro(cuestionario.getCodigoEntidad()) && COD_PAIS_MX.equals(cuestionario.getCodigoPais())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Entidad");
		}
		
		if(validaParametro(cuestionario.getCodigoMunicipio())  && COD_PAIS_MX.equals(cuestionario.getCodigoPais())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Municipio");
		}
		
		if(validaParametro(cuestionario.getCodigoNacionalidad())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Nacionalidad");
		}
		
		return cuestionario;
	}
	
	/**
	 * metodo para validar el formato del identificador que debe contener:<br>
	 * Formato XX000000YYYYMMDD
	 *  las dos primeras letras, deben ser OP,TF,FC<br>
	 *  los siguientes 6 son numericos<br>
	 *  y los ultimos 8 son en formato fecha YYYYMMDD<br>
	 * 
	 * @param id id del cuestionario a validar
	 * @return true si es invalido el formato
	 * @throws BusinessException con mensaje de error
	 */
	private static boolean validaformatoIdCuest(String id) throws BusinessException {
		if(id!=null){
			//validamos que contenga LH - LightHouse
			if(id.startsWith("LH") && id.length()>2){
				boolean segunda=false;
				//validamos que sean numeros
				try{
				Integer.parseInt(id.substring(2, 8));
				segunda=true;
				}catch(NumberFormatException nfe){
					segunda=false;
				}
				if(segunda && id.length()>=8){
					try{
						//validamos que sean numeros el formato de fecha
						Integer.parseInt(id.substring(8));
						segunda=true;
					}catch(NumberFormatException nfe){
						LOG.error("Es necesario proporcionar un Identificador de Cuestionario Valido. El formato de la fecha es Incorrecto", nfe);
						segunda=false;
					}
					if(segunda){
						if(UtileriasHelper.isValidDate(id.substring(8))){
							if(!(UtileriasHelper.diasDiferenciaAldia(id.substring(8))==0)){
								throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, La fecha no es valida. ");
							}else {
								return false;
							}
						}else{
							throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, La fecha no es valida.");
							//return true;
						}
					}else {
						return true;
					}
						
				}else {
					return true;
				}
			}else{
				return true;
			}
		}
		return true;
	}
	
	/**
	 * Obtiene los datos correspondientes al cliente
	 * @param cuestionario Request en donde se encuentra la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerDatosCliente(CuestionarioDTO cuestionario) throws BusinessException {
		ClienteDTO cliente = cuestionario.getCliente();
		
		if(validaParametro(cliente.getCodigoCliente()) ){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el BUC");
		}
		
		if(validaParametro(cliente.getIndClienteNuevo())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Indicador de Cliente Nuevo");
		}
		
		if(validaParametro(cliente.getNombreCliente()) ){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Nombre del Cliente");
		}
		
		if(!validaParametro(cuestionario.getTipoPersona()) && "F".equalsIgnoreCase(cuestionario.getTipoPersona()) && validaParametro(cliente.getApellidoPaterno())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Apellido Paterno del Cliente");
		}
		
		if(StringUtils.isBlank(cliente.getApellidoMaterno())) {
			cliente.setApellidoMaterno(StringUtils.EMPTY);
		}
		
		
		if(validaParametro(cliente.getFechaNacimiento())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Fecha de Nacimiento / Fecha de Constituci&oacute;n del Cliente");
		}
	}
	
	/**
	 * Obtiene los datos con los cuales se define el tipo de cuestionario que se va a mostrar
	 * @param cuestionario En donde se encuentra la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerDatosTipoCuestionario(CuestionarioDTO cuestionario) throws BusinessException {
		if(validaParametro(cuestionario.getTipoPersona())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Tipo de Persona del Cliente");
		}
		if(!"F".equalsIgnoreCase(cuestionario.getTipoPersona()) && !"J".equalsIgnoreCase(cuestionario.getTipoPersona())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"El Tipo de Persona es incorrecto");
		}
		
		if(validaParametro(cuestionario.getSubtipoPersona())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Subtipo de Persona del Cliente");
		}
		
		if(validaParametro(cuestionario.getDivisa())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Divisa del Producto");
		}
	}
	
	/**
	 * Obtiene los datos correspondientes a una sucursal
	 * @param cuestionario Cuestionario al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerDatosSucursal(CuestionarioDTO cuestionario) throws BusinessException {
		if(validaParametro(cuestionario.getCodigoSucursal())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de Sucursal");
		}
		
		if(validaParametro(cuestionario.getNombreSucursal())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Nombre de la Sucursal");
		}
		
		if(StringUtils.isBlank(cuestionario.getCodigoPlaza())) {
			cuestionario.setCodigoPlaza(StringUtils.EMPTY);
		}
		
		if(StringUtils.isBlank(cuestionario.getNombrePlaza())) {
			cuestionario.setNombrePlaza(StringUtils.EMPTY);
		}
		
		if(StringUtils.isBlank(cuestionario.getCodigoZona())) {
			cuestionario.setCodigoZona(StringUtils.EMPTY);
		}
		
		if(StringUtils.isBlank(cuestionario.getNombreZona())) {
			cuestionario.setNombreZona(StringUtils.EMPTY);
		}
		
		if(validaParametro(cuestionario.getCodigoRegion())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Regi\u00F3n");
		}
		
		if(StringUtils.isBlank(cuestionario.getNombreRegion())) {
			cuestionario.setNombreRegion(StringUtils.EMPTY);
		}
	}
	
	/**
	 * Obtiene los datos correspondiente al producto y subproducto
	 * @param cuestionario Cuestionario al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerProductoSubproducto(CuestionarioDTO cuestionario) throws BusinessException {
		if(validaParametro(cuestionario.getCodigoProducto())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Producto");
		}
		
		if(validaParametro(cuestionario.getDescProducto())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Descripci\u00F3n del Producto");
		}
		
		if(validaParametro(cuestionario.getCodigoSubproducto())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Subproducto");
		}
	}
	
	/**
	 * Validacion que el parametro no se encuentre vacio
	 * @param valorParametro parametro a buscar
	 * @return false si el parametro existe y no es vacio
	 */
	private static boolean validaParametro(String valorParametro) {
		if(StringUtils.isNotBlank(valorParametro)){
			return false;
		}
		return true;
	}
}
