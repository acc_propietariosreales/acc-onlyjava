/**
 * Isban Mexico
 *   Clase: AdministradorCuestionarioService.java
 *   Descripcion: Componente WS para obtener los datos generales del cliente y las respuestas del cuestionario.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.norkom.cuestionario.dto.RespuestasRes;
import mx.isban.norkom.facade.AdministradorCuestionariosFacade;
import mx.isban.norkom.ws.dto.CuestionariosWebServiceDTO;

import org.apache.log4j.Logger;

@WebService(targetNamespace="http://ws.norkom.isban.mx/", serviceName="AdministradorCuestionarioService", portName="ObtenerRespuestaWSPort")
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class AdministradorCuestionarioService {

	/** 
	 * Variable utilizada para el loger 
	 */
	private static final Logger LOGGER = Logger.getLogger(AdministradorCuestionarioService.class);
	/** 
	 * Contexto de Spring
	 */
	@Resource
	private transient WebServiceContext context;
	/**
	 * Metodo para consultar las respuestas de los cuestionarios
	 * @param identificadorCuestionario Numero de contrato del que se desea obtener la informacion
	 * @return RespuestasRes Informacion del cuestionario con la informacion
	 */
	@WebMethod(operationName="obtenerRespuestasCuestionario")
	public RespuestasRes obtenerRespuestas(@WebParam(name="identificadorCuestionario") String identificadorCuestionario){
		LOGGER.info("Ingresamos a AdministradorCuestionarioService.obtenerRespuestas");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		AdministradorCuestionariosFacade facade= new AdministradorCuestionariosFacade(servletContext);
		
		return facade.obtenerPreguntasRespuestas(identificadorCuestionario);
	}
	
	/**
	 * Obtiene la informacion general de un cuestionario
	 * @param buc Codigo de cliente
	 * @param numeroContrato Numero de contrato
	 * @param fechaInicio Fecha inicial de creacion del contrato
	 * @param fechaFin Fecha final de creacio del contrato
	 * @return CuestionariosWebServiceDTO Informacion del Cuestionario
	 */
	@WebMethod
	public CuestionariosWebServiceDTO obtenerDatosGeneralesCuestionario(
			@WebParam(name="buc") String buc, @WebParam(name="numeroContrato") String numeroContrato, 
			@WebParam(name="fechaInicio") String fechaInicio, @WebParam(name="fechaFin") String fechaFin)
	{
		LOGGER.info("Ingresamos a AdministradorCuestionarioService.obtenerDatosGeneralesCuestionario");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		AdministradorCuestionariosFacade facade= new AdministradorCuestionariosFacade(servletContext);
		
		return facade.obtenerDatosGeneralesCuestionario(buc, numeroContrato, fechaInicio, fechaFin);
	}
}
