/**
 *   Isban Mexico
 *   Clase: RecepcionCruzeRelacionadosWS.java
 *   Descripcion: Componente WS para actualizar
 *   el estatus de los relacionados por norkom
 *
 *   Control de Cambios:
 *   1.0 Jul 13, 2017 LFER - Creacion
 */
package mx.isban.norkom.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.norkom.facade.CruzeRelacionadosFacade;
import mx.isban.norkom.ws.dto.ActualizaEstatusRelacionados;
import mx.isban.norkom.ws.dto.RespuestaWebService;

import org.apache.log4j.Logger;
/**
 * RecepcionCruzeRelacionadosWS
 * WebService principal para la actualizacion de relacionados
 * por bloque
 * @author lespinosa
 *
 */
@WebService(targetNamespace="http://ws.ccc.norkom.isban.mx/", serviceName="recepcionCruzeRelacionadosService", portName="recepcionCruzeRelacionadosPort")
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class RecepcionCruzeRelacionadosWS {
	
	/** 
	 * Variable utilizada para escribir 
	 * el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(RecepcionCruzeRelacionadosWS.class);
	/**
	 * WebServiceContext contexto para 
	 * la utilizacion de los BO's
	 */
	@Resource
	private WebServiceContext context;
	
	/** 
	 * Agrega relacionados a un Cuestionario
	 * @param actualizadto Lista de relacionados que se desean agregar
	 * @return RelacionadosWebServiceDTO Respuesta de la operacion de agregar relacionados
	 */
	@WebMethod(operationName="recepcionCruzeRelacionadosOK")
	public RespuestaWebService recepcionCruzeRelacionadosOK(@WebParam(name="beanActualiza") ActualizaEstatusRelacionados actualizadto){
		LOGGER.info("Ingresamos a RecepcionCruzeRelacionadosWS.recepcionCruzeRelacionadosOK");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		CruzeRelacionadosFacade facade= new CruzeRelacionadosFacade(servletContext);
		return facade.actualizaEstatusOK(actualizadto);
	}
	
}
