/**
 * Isban Mexico
 *   Clase: ComponenteCentralNorkomWS.java
 *   Descripcion: Componente WS para generar los 
 *   cuestionarios con sus respuestas.
 *
 *   Control de Cambios:
 *   1.0 Jun 12, 2017 LFER - Creacion
 */
package mx.isban.norkom.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.norkom.facade.RelacionadosFacade;
import mx.isban.norkom.ws.dto.RelacionadosRequestDTO;
import mx.isban.norkom.ws.dto.RelacionadosResponseDTO;

import org.apache.log4j.Logger;
/**
 * RelacionadosACCWS
 * WebService principal para la carga de relacionados
 * por bloque
 * @author lespinosa
 *
 */
@WebService(targetNamespace="http://ws.ccc.norkom.isban.mx/", serviceName="RelacionadosACCWSServices", portName="RelacionadosACCWSServicePort")
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class RelacionadosACCWS {
	
	/** 
	 * Variable utilizada para escribir 
	 * el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(RelacionadosACCWS.class);
	/**
	 * WebServiceContext contexto para 
	 * la utilizacion de los BO's
	 */
	@Resource
	private WebServiceContext context;
	
	/** 
	 * Agrega relacionados a un Cuestionario
	 * @param relacionadosWS Lista de relacionados que se desean agregar
	 * @return RelacionadosWebServiceDTO Respuesta de la operacion de agregar relacionados
	 */
	@WebMethod(operationName="agregarRelacionadosBloque")
	public RelacionadosResponseDTO agregarRelacionadosXBloque(@WebParam(name="relacionados") RelacionadosRequestDTO relacionados){
		LOGGER.info("Ingresamos a RelacionadosACCWS.agregarRelacionadosXBloque");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		RelacionadosFacade facade= new RelacionadosFacade(servletContext);
		return facade.agregarRelacionados(relacionados);
	}
	
}
