/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * AccRestApplication.java
 *
 * Control de versiones:
 *
 * Version  Date                    By                  Company     Description
 * -------  -------------------     ----------------    --------    ---------
 * 1.0		24/05/2016			    Stefanini			Stefanini	Creacion
 */
package mx.isban.norkom.ws.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * The Class AccRestApplication.
 */
@ApplicationPath("/services")
public class AccRestApplication extends Application {

	/** The singletons. */
	private transient final Set<Object> singletons = new HashSet<Object>();
	
	/* (non-Javadoc)
	 * @see javax.ws.rs.core.Application#getClasses()
	 */
	@Override
	public Set<Class<?>> getClasses() {
		// Resources
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(mx.isban.norkom.ws.jaxrs.CuestionarioLightHouseService.class);
		
		// Providers
		classes.add(mx.isban.norkom.cuestionario.util.JsonTokenProvider.class);
		classes.add(mx.isban.norkom.cuestionario.exception.JsonProcessingExceptionMapper.class);
		
		return classes;
	}
	
	/* (non-Javadoc)
	 * @see javax.ws.rs.core.Application#getSingletons()
	 */
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
