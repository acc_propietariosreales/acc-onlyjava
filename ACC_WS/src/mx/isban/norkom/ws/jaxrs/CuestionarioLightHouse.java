package mx.isban.norkom.ws.jaxrs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseRequest;
import mx.isban.norkom.cuestionario.exception.CuestionarioLightHouseException;

@Path("/acc/ProcesarCuestionarioLightHouse")
public interface CuestionarioLightHouse {
	/**
	 * Procesa la informacion de almacenamiento y calificacion
	 * @param request Informacion de la peticion
	 * @return Codigo y mensaje de la operacion
	 * @throws CuestionarioLightHouseException En caso de un problema inesperado
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces( MediaType.APPLICATION_JSON )
	public Response procesarCuestionario(CuestionarioLightHouseRequest request) throws CuestionarioLightHouseException;
}
