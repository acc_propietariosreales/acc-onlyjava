package mx.isban.norkom.ws.jaxrs;

import javax.ejb.Stateless;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.ibm.json.java.JSONObject;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.CuestionarioLightHouseBO;
import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseRequest;
import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseResponse;
import mx.isban.norkom.cuestionario.exception.CuestionarioLightHouseException;

/**
 * 
 * @author jugarcia
 * Objetivo: Recibir las peticiones de LightHouse
 * Justificacion: Administrar las peticiones que se realizan para el almacenamiento de un cuestionario de clientes en LightHouse y la obtencion de la calificacion con base a esa informacion
 */

@Stateless
public class CuestionarioLightHouseService implements CuestionarioLightHouse {
	/**
	 * ServletContext contexto para la utilizacion de los BO's
	 */
	@Context
	private transient ServletContext context;
	
	@Override
	public Response procesarCuestionario(CuestionarioLightHouseRequest request) throws CuestionarioLightHouseException {
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(context);
		final CuestionarioLightHouseBO cuestionarioLightHouseBO =
				(CuestionarioLightHouseBO)springContext.getBean("BOCuestionarioLightHouse");
		
		try {
			CuestionarioLightHouseResponse response = cuestionarioLightHouseBO.procesarCuestionario(request);
			return Response.ok(response).type(MediaType.APPLICATION_JSON).build();
		} catch (BusinessException e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("errors", e);
			return Response.status(Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON).entity(jsonObj)
					.build();
		}
	}
}
