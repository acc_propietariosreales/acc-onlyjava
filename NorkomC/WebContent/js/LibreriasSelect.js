(function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );

        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },

      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
          this.input = $( "<input id='" + this.element.attr('id') + "_AU'>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .css( "width", "300px" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });

        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },

          autocompletechange: "_removeIfInvalid"
        });
      },

      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;

        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Mostrar Todos" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();

            // Close if already visible
            if ( wasOpen ) {
              return;
            }

            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },

      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) ) {
            return {
              label: text,
              value: text,
              option: this
            };
          }
        }) );
      },

      _removeIfInvalid: function( event, ui ) {

        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }

        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });

        // Found a match, nothing to do
        if ( valid ) {
          return;
        }

        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", "El valor " + value + " no coincide con algun elemento en la lista" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },

      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );

function moverOpcionesSelect(fromID, toID) {
    var i_ind;
    var i_obj;

    var i=0;
    var total_opciones = document.getElementById(fromID).options.length;
    var cont_selected = 0;
    for(i=total_opciones-1;i>=0;i--){
        i_ind = i;
        i_obj = document.getElementById( fromID ).options[ i_ind ];
        if(i_obj.selected  && i_obj.value!==''){
            cont_selected++;
            if ($('#'+toID+' option[value='+i_obj.value+']').length > 0 &&  i_obj.value!=='') {
                jAlert('' , 'Información Incompleta', 'ERRSL001', 'Debe seleccionar al menos un registro para Agregar o Eliminar');
                break;
            } else {
                var theOpt = new Option( i_obj.text, i_obj.value, false, false );
                document.getElementById( toID ).options[document.getElementById( toID ).options.length] = theOpt;
                //eliminar item de lista origen:
                if (i_ind!==-1){
                    document.getElementById( fromID ).options[ i_ind ] = null;
                }
            }
        }
    }

    if(cont_selected===0){
        jError('Seleccione un registro en el Origen o el Destino', 'Informaci\u00F3n Incompleta', 'ERRSL001', 'Debe seleccionar al menos un registro de la lista de pa\u00EDses para Agregar o Eliminar');
    } else {
        // ordenar
        ordenarSelect(fromID);
        ordenarSelect(toID);
        eliminarDuplicados(toID);
    }
}

function ordenarSelect(id) {
    var $list = $('#'+id);
    // .get() solo es necesario si es jQuery <= 1.3.1
    var elemHijos=$('option',$list).get();

    elemHijos.sort(function(a, b){
        return (a.innerHTML > b.innerHTML) ? 1 : -1;
    });

    $.each(elemHijos, function(index, row){
        $list.append(row);
    });
}

function eliminarDuplicados(idrev){
    var a = [];
    $('#'+idrev).children("option").each(function(x){
        test = false;
        b = a[x] = $(this).text();
        for (i=0;i<a.length-1;i++){
            if (b === a[i]) {
                test =true;
            }
        }
        if (test){
            $(this).remove();
        }
    });
}