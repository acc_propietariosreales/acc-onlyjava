/*********FUNCIONES COMUNES**********/
var identificadorFuentes = 1;
var contadorFuentes = 0;

$(function() {
    var image = $("#contextPath").val() + "/img/loading.gif";
	var divfondo = "<div id='ninja' style='display:none;'><img src='" + image +"' id='ninjaImg' name='ninjaImg'/></div>";
	$("#ninja").hide();
	$("body").prepend(divfondo); //es importante que quede al principio del body

    validarConcubina();
    validarParticipacionesAccionarias();

    var selectUnico = $('.selectPregAutocompletar');
    if(selectUnico) {
        for(var contadorSelect = 0; contadorSelect < selectUnico.length; contadorSelect++) {
            $(selectUnico[contadorSelect]).combobox();
        }
    }
});

function validarCampoObligatorios(){
	var inputUsernames = document.getElementsByTagName('input');
	var total = inputUsernames.length;
	var realizarPeticion = true;

	for(var contadorInput = 0; contadorInput < total; contadorInput++) {
		if(inputUsernames[contadorInput].type === 'text' && inputUsernames[contadorInput].disabled === false && inputUsernames[contadorInput].value.length === 0){
			$("#"+inputUsernames[contadorInput].id).addClass("CamposCompletar");
			realizarPeticion = false;
		}
	}

	return realizarPeticion;
}

function validarNumerosTelefonicos(){
    var inputTelefonos = $("input[type='text'].CampoTelefonico");
    var total = inputTelefonos.length;
    var realizarPeticion = true;

    for(var contadorInput = 0; contadorInput < total; contadorInput++) {
        if(inputTelefonos[contadorInput].disabled === false && inputTelefonos[contadorInput].value.length !== 10){
            $("#"+inputTelefonos[contadorInput].id).addClass("CamposCompletar");
            realizarPeticion = false;
        }
    }

    return realizarPeticion;
}

function validarAlMenosUnFamiliar() {
	var listaParentesco = document.getElementById('selectPregUnicoIAPR');
	var inputUsernames = document.getElementsByTagName('input');
	var total = inputUsernames.length;
	var existeUnCampo = false;

	for(var contadorInput = 0; contadorInput < total; contadorInput++) {
		if(inputUsernames[contadorInput].type === 'text' && inputUsernames[contadorInput].name.indexOf("txtPregIAF") >= 0){
			existeUnCampo = true;
		}
	}

	if(listaParentesco && existeUnCampo === false) {
		return false;
	}

	return true;
}

function validarRadioButtons() {
    if($('input[name="radioPregFBPR"]').length > 0 && $('input[name="radioPregFBPR"]:checked').length === 0) {
        jError('Por favor capture toda la Informaci\u00F3n.',
             'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0004', 'Es necesario capturar un valor para la Periodicidad.');

        return false;
    }

    if($('input[name="radioPregEBPR"]').length > 0 && $('input[name="radioPregEBPR"]:checked').length === 0) {
        jError('Por favor capture toda la Informaci\u00F3n.',
            'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0004', 'Es necesario capturar un valor para la Periodicidad.');

        return false;
	}

    return validarRadioButtonsComplemento();
}

function validarRadioButtonsComplemento() {
    if($('input[name="radioPregITCO"]').length > 0 && $('input[name="radioPregITCO"]:checked').length === 0) {
         jError('Por favor capture toda la Informaci\u00F3n.',
             'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0005', 'Es necesario seleccionar una opci&oacute;n para Concubina/Concubinario.');

        return false;
	}

    if($('input[name="radioPregNSC0"]').length > 0 && $('input[name="radioPregNSC0"]:checked').length === 0) {
        jError('Por favor capture toda la Informaci\u00F3n.',
            'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0006', 'Es necesario seleccionar una opci&oacute;n para Participaci&oacute;n Accionaria.');

        return false;
	}

    return true;
}

function validaNumerosEnterosEvent(event){

    var key = (event.charCode)?event.charCode:
        ((event.keyCode)?event.keyCode:((event.which)?event.which:0));

    //48-57 -
    return (key >= 48 && key <= 57);
}

function validaNumerosDecimalesEvent(event){

    var key = (event.charCode)?event.charCode:
        ((event.keyCode)?event.keyCode:((event.which)?event.which:0));

   //48-57 -
   return ((key >= 48 && key <= 57) || orKey8946(key));
}
function orKey8946(key){
  return key===8 || key===9 || key===46;
} 

function validaLetrasNumerosEspEvent(event){
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		//(key >= 48 && key <= 57) numeros
		//(key >= 97 && key <= 122) a-z
		//(key >= 65 && key <= 90) A - Z
		//241 209
		//32 Espacio
		//180
		//193, 201, 205, 211, 218 Vocales mayusculas acentuadas
		//225, 233, 237, 243, 250 Vocales minusculas acentuadas
		//44, 46, 64, 95 punto, coma, arroba, guion bajo
		return ( (key >= 48 && key <= 57)
        || orKeys971226590(key)
        || unionkeys(key));
}
function orKeys971226590(){
  return (key >= 97 && key <= 122)
  || (key >= 65 && key <= 90);
}
function unionkeys(key){
   return keycode2418(key) ||keycode193218(key)
    || keycode225250(key)
    || keycode4495(key);
}
function keycode2418(key){
  return key===241 || key===209 || key===32 || key===8;
}

function keycode193218(key){
  return key===193 || key===201 || complementaria218(key);
}
function complementaria218(){
  return key===205 || key===211 || key===218;
}
function keycode225250(key){
  return key===225 || key===233 || complementaria5250();
}
function complementaria5250(){
  return  key===237 || key===243 || key===250;
}

function keycode4495(key){
  return key===44 || key===46 || key===64 || key===95;
}

function guardarCuestionarioIc(){
	$("input:text").removeClass("CamposCompletar");
	$(".custom-combobox-input").each(function(){
		$(this).css('background-color', '#FFFFFF');
	});
	
	var validacionAutocompletableValida = true;
	$(".custom-combobox-input").each(function(){
		if($(this).val().length === 0) {
			jError('Por favor capture toda la Informaci\u00F3n.',
					'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0007', 'Es necesario capturar un valor en las listas autocompletables.');
			$(this).css('background-color', '#C6F3FF');
			validacionAutocompletableValida = false;
		}
	});
	
	if(validacionAutocompletableValida === false) {
		return false;
	}
	
	var realizarPeticion = validarCampoObligatorios();
    var validacionTelefonos = validarNumerosTelefonicos();
	var validacionFamiliar = validarAlMenosUnFamiliar();
    var validacionPeriodicidad = validarRadioButtons();
    if(realizarPeticion === true && validacionFamiliar === true && validacionTelefonos === true && validacionPeriodicidad === true) {
		verNinja();
		$('.parentescos').removeAttr('disabled');

		document.getElementById("btnGuardarCuestionarioIC").disabled = true;
		$('#btnGuardarCuestionarioIC').hide();

		document.getElementById(ID_FORMA).action = URL_GUARDAR_IC;
		document.getElementById(ID_FORMA).submit();
	} else if(validacionFamiliar === false) {
		jError('Por favor capture toda la Informaci\u00F3n.',
				'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0001', 'Es necesario agregar al menos un Familiar en la secci\u00F3n de INFORMACI\u00D3N ADICIONAL.');
	} else if(realizarPeticion === false) {
		jError('Por favor capture toda la Informaci\u00F3n. ',
				'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0002', 'Es necesario capturar todos los valores, en caso de que no aplique capturar NA para texto o 0 para valores num\u00E9ricos.');
	} else if(validacionTelefonos === false) {
		jError('Por favor capture toda la Informaci\u00F3n.',
				'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0003', 'Es necesario capturar todos los valores, por favor revise la informaci\u00F3n de los N\u00FAmeros Telef\u00F3nicos.');
	}
}

function agregarFamiliarPep(){
	var opcionSeleccionda = document.getElementById("selectPregUnicoIAPR").selectedIndex;
	var textoOpcion = document.getElementById("selectPregUnicoIAPR")[opcionSeleccionda].text;
	var tipoParentesco = 'Parentesco';
	var campoDeshabilitado = ' disabled ';

	if(textoOpcion.indexOf('OTROS') >= 0) {
		textoOpcion = '';
		tipoParentesco = 'Especifique';
		campoDeshabilitado = '';
	}

      var divContenedor = document.getElementById('seccionPreguntasIAPR');
      var nuevoDiv = document.createElement('div');
      nuevoDiv.id = "seccionPreguntasIAPR" + identificadorFuentes;

	  var opcionAgregar = '';
	  opcionAgregar =  opcionAgregar + "<BR>" + tipoParentesco + ": <input type='text' value='" + textoOpcion + "' id='txtPregIAP" + identificadorFuentes + "' name='txtPregIAP" + identificadorFuentes + "' onkeypress='return validaLetrasNumerosEspEvent(event)' maxlength='25' " + campoDeshabilitado + " class='parentescos'>";
      opcionAgregar =  opcionAgregar + "<BR>Nombre completo del familiar: <input type='text' id='txtPregIAN" + identificadorFuentes + "' name='txtPregIAN" + identificadorFuentes + "' onkeypress='return validaLetrasNumerosEspEvent(event)' maxlength='80'>";
      opcionAgregar =  opcionAgregar + "Fecha de Nacimiento (dd/mm/aaaa): <input type='text' id='txtPregIAF" + identificadorFuentes + "' name='txtPregIAF" + identificadorFuentes + "' maxlength='15' maxlength='10' onblur='validarFecha(this)'>";
      opcionAgregar =  opcionAgregar + "<input type='button' id='calendarioIAF" + identificadorFuentes + "' value='Calendario'>";
      opcionAgregar =  opcionAgregar + "<BR>Domicilio del familiar: <input type='text' value='' id='txtPregIAD" + identificadorFuentes + "' name='txtPregIAD" + identificadorFuentes + "' onkeypress='return validaLetrasNumerosEspEvent(event)' maxlength='150'>&nbsp;";
	  opcionAgregar =  opcionAgregar + "<input type='button' value='Eliminar Familiar' id='btnAgregarFamiliar' name='btnAgregarFamiliar' onclick=' eliminarFamiliarPep(this);'>";

	  nuevoDiv.innerHTML = opcionAgregar;
	  divContenedor.appendChild(nuevoDiv);

	  createCalendar("txtPregIAF" + identificadorFuentes, "calendarioIAF" + identificadorFuentes);

	  if(textoOpcion.indexOf('OTROS') >= 0) {
		  document.getElementById('txtPregIAP' + identificadorFuentes).disabled = false;
	  }

	  identificadorFuentes++;
}

function eliminarFamiliarPep(objetoEliminar){
	$('#' + objetoEliminar.parentElement.id).remove();
}




function crearCalendarios(){
	var inputUsernames = document.getElementsByTagName('input');
	var total = inputUsernames.length;

	for(var contadorInput = 0; contadorInput < total; contadorInput++) {
		if(inputUsernames[contadorInput].id.indexOf('calendario') >= 0){
			var txtAbrev = inputUsernames[contadorInput].id.substr('calendario'.length);
			createCalendar("txtPreg" + txtAbrev, inputUsernames[contadorInput].id);
		}
	}
}

function mostrarMensajeInicial() {
	var mensajeInicial = $('#msjOperacion').val();
	var codigoOperacion = $('#codigoOperacion').val();

    //Se para escapar el backSpace. adds:MEG -03102013
    escaparBackSpace();

	if (mensajeInicial && mensajeInicial.length > 0) {
		if (mensajeInicial === 'INF001') {
			jInfo(mensajes['msjINF001Sugerencia'], mensajes['msjINF001Titulo'],
					mensajes['msjINF001Codigo'],
					mensajes['msjINF001Observacion']);
		} else if(codigoOperacion && codigoOperacion === 'ERCLCT04'){
			jError(mensajeInicial,'Error en la respuesta de Norkom', codigoOperacion, 'Por favor vuelva a intentarlo m\u00E1s tarde');
		} else if(mensajeInicial !== 'OK0001'){
			jError(mensajeInicial,'Error al accesar al cuestionario'
					,"Error Inesperado",'Por favor vuelva a intentarlo m\u00E1s tarde');
		}
	}

	mostrarSugerenciaInicial();
	seleccionarPais();
}

function mostrarSugerenciaInicial() {
	var nivelRiesgoNkm = $('#nivelRiesgoNkm').val();
	var indicadorUpldNkm = $('#indicadorUpldNkm').val();
	var indicadoresVisita = $('#indicadorVisitaNkm').val();
	var tipoPersonaAcc = $('#tipoPersonaAcc').val();
	var sugerenciaNivel = '';
	
	if(indicadorUpldNkm && indicadorUpldNkm === 'KYC-DR') {
		sugerenciaNivel = sugerenciaNivel + '- Requiere autorizaci&oacute;n de su Director Regional / Ejecutivo<br>';
	}
	
	if(indicadoresVisita && indicadoresVisita.length > 0) {
		var indicadoresSeparados = indicadoresVisita.split(",");
		for(var contIndicador = 0; contIndicador < indicadoresSeparados.length; contIndicador++) {
			if(indicadoresSeparados[contIndicador] == indicadorUpldNkm) {
				sugerenciaNivel = sugerenciaNivel + '- EL CLIENTE REQUIERE LLEVAR A CABO VISITA DOMICILIARIA<br>';
			}
    }
		}
  validaMensajesResultadoInicial(nivelRiesgoNkm, sugerenciaNivel);
	}

function validaMensajesResultadoInicial(nivelRiesgoNkm, sugerenciaNivel) {
  var rel=$("#RELA1").val();
  var intentos=$("#INT_RELA").val();
  var intentosIC=$("#INT_RELA_IC").val();
  if(rel!=='' && rel==='REL_A1' && parseInt(intentosIC)>0 &&  parseInt(intentosIC)<3){
    funcionEsperaRelacionados('guardarCuestionarioIc.do');
  }else if(rel!=='' && rel==='REL_A1' && parseInt(intentos)<=3){
    
    funcionEsperaRelacionados('guardarCuestionarioIp.do');
    
  }else{
    enviaMensajeresultadoPR(nivelRiesgoNkm,sugerenciaNivel,intentosIC,rel);
  }
}

function enviaMensajeresultadoPR(nivelRiesgoNkm,sugerenciaNivel,intentosIC,rel){
  if(nivelRiesgoNkm && nivelRiesgoNkm.length > 0) {
    if(rel==='' && parseInt(intentosIC)>=1){
      jInfo('', 'El cliente ha sido clasificado como ' + nivelRiesgoNkm +" \n faltan preguntas por contestar.", "Nivel de Riesgo", sugerenciaNivel);
      validaPreguntasA1PR();
    }else if(rel!=='' && rel==='REL_A1' && parseInt(intentosIC)>=3){
      jInfo('', 'El cliente ha sido clasificado como ' + nivelRiesgoNkm, "Nivel de Riesgo", sugerenciaNivel);
    }else if(rel===''){
      jInfo('', 'El cliente ha sido clasificado como ' + nivelRiesgoNkm, "Nivel de Riesgo", sugerenciaNivel);
    }
  }
}

function funcionEsperaRelacionados(url){
  $(".frameTablaEstandar").each(function(){
    $(this).hide();
  });
  var i = 10;
  var txt1='Espere ';
  $("#descripcion").text(txt1+ i + ' Segundos.');
     jAlert('','Aun no se han terminado de procesar los Relacionados en Norkom','','', function(r){
       if(r){
      $('#frmTiposPreguntas').attr('action',url);
      $('#frmTiposPreguntas').submit();
       }
    });
     $("#popup_ok").hide();
  
  var timer = setInterval(function() {
    $("#descripcion").text(txt1+ i + ' Segundos.');
    if (i === 0) {
     $("#popup_ok").show();
     clearInterval(timer);
    }
    i--;
  }, 1000);
}
function seleccionarPais(){
	//StrCodPaisSel
	if($("#selectPregUnicoREPS").length){
		var paiscompara=$("#StrCodPaisSel").val();
		var existe=0;
		$("#selectPregUnicoREPS option").each(function(){
			  //var idPais=$(this).val();
			  var texto=$(this).text();
			  if(texto===paiscompara){
				  $(this).attr("selected", true);
				  existe=1;
			  }
		});
		if(existe===0){
			$("#selectPregUnicoREPS").append('<option value=999 selected>'+paiscompara+'</option>');
		}
		$("#selectPregUnicoREPS").attr('disabled', 'disabled');
	}
}
/**
 * Funcion que escapa el backSpace.... BY MEG
 * @return
 */

function escaparBackSpace() {
$(document).unbind('keydown').bind('keydown', function (event) {
   var doPrevent = false;
   if (event.keyCode === 8) {
   var d = event.srcElement || event.target;
   var upperD=d.type.toUpperCase();
      if ((d.tagName.toUpperCase() === 'INPUT' &&
      valUpperD(upperD))
          || d.tagName.toUpperCase() === 'TEXTAREA') {
         doPrevent = d.readOnly || d.disabled;
      }
      else {
         doPrevent = true;
      }
   }
   if (doPrevent) {
      event.preventDefault();
   }
});

}

function valUpperD(upperD){
  return ( upperD=== 'TEXT' ||
       upperD === 'PASSWORD' ||
       upperD === 'FILE');
}

function validarReferenciasBancarias(){
	var opciones = document.getElementsByName('radioPregRFBC');
	var opcionSeleccionada = '';
	for(var contadorRadios = 0; contadorRadios < opciones.length; contadorRadios++){
	    if(opciones[contadorRadios].checked){
	    	opcionSeleccionada = opciones[contadorRadios].getAttribute("texto");
	    }
	}

	if(opcionSeleccionada && opcionSeleccionada.indexOf('SI') >= 0) {
		mostrarReferenciasBancarias();
	} else if(opcionSeleccionada && opcionSeleccionada.indexOf('NO') >= 0) {
		ocultarReferenciasBancarias();
	}
}

function mostrarReferenciasBancarias(){
	document.getElementById('txtPregBNC1').disabled = false;
	document.getElementById('txtPregNCD1').disabled = false;
	document.getElementById('txtPregBNC2').disabled = false;
	document.getElementById('txtPregNCD2').disabled = false;
	document.getElementById('txtPregJUST').disabled = true;

	document.getElementById('PreguntasDivBNC1').style.display = 'block';
	document.getElementById('PreguntasDivNCD1').style.display = 'block';
	document.getElementById('PreguntasDivBNC2').style.display = 'block';
	document.getElementById('PreguntasDivNCD2').style.display = 'block';
	document.getElementById('PreguntasDivJUST').style.display = 'none';
}

function ocultarReferenciasBancarias(){
	document.getElementById('txtPregBNC1').disabled = true;
	document.getElementById('txtPregNCD1').disabled = true;
	document.getElementById('txtPregBNC2').disabled = true;
	document.getElementById('txtPregNCD2').disabled = true;
	document.getElementById('txtPregJUST').disabled = false;

	document.getElementById('PreguntasDivBNC1').style.display = 'none';
	document.getElementById('PreguntasDivNCD1').style.display = 'none';
	document.getElementById('PreguntasDivBNC2').style.display = 'none';
	document.getElementById('PreguntasDivNCD2').style.display = 'none';
	document.getElementById('PreguntasDivJUST').style.display = 'block';
}

function validarOtrasFuentesIngresos(){
	var opciones = document.getElementsByName('radioPregOTFI');
	var opcionSeleccionada = '';
	for(var contadorRadios = 0; contadorRadios < opciones.length; contadorRadios++){
        opciones[contadorRadios].checked = false;
	    if(opciones[contadorRadios].checked){
	    	opcionSeleccionada = opciones[contadorRadios].getAttribute("texto");
	    }
	}

	if(opcionSeleccionada && opcionSeleccionada.indexOf('SI') >= 0) {
		mostrarOtrasFuentesIngresos();
	} else if(opcionSeleccionada && opcionSeleccionada.indexOf('NO') >= 0) {
		ocultarOtrasFuentesIngresos();
	}
}

function mostrarOtrasFuentesIngresos(){
	document.getElementById('txtPregOTF1').disabled = false;
	if(document.getElementById('txtPregOMP1')) {
		document.getElementById('txtPregOMP1').disabled = false;
	}
	if(document.getElementById('txtPregOMD1')) {
		document.getElementById('txtPregOMD1').disabled = false;
	}

	document.getElementById('txtPregOTF2').disabled = false;
	if(document.getElementById('txtPregOMP2')) {
		document.getElementById('txtPregOMP2').disabled = false;
	}
	if(document.getElementById('txtPregOMD2')) {
		document.getElementById('txtPregOMD2').disabled = false;
	}


	document.getElementById('PreguntasDivOTF1').style.display = 'block';
	if(document.getElementById('PreguntasDivOMP1')) {
		document.getElementById('PreguntasDivOMP1').style.display = 'block';
	}
	if(document.getElementById('PreguntasDivOMD1')) {
		document.getElementById('PreguntasDivOMD1').style.display = 'block';
	}

	document.getElementById('PreguntasDivOTF2').style.display = 'block';
	if(document.getElementById('PreguntasDivOMP2')) {
		document.getElementById('PreguntasDivOMP2').style.display = 'block';
	}
	if(document.getElementById('PreguntasDivOMD2')) {
		document.getElementById('PreguntasDivOMD2').style.display = 'block';
	}
}

function ocultarOtrasFuentesIngresos(){
	document.getElementById('txtPregOTF1').disabled = true;
	if(document.getElementById('txtPregOMP1')) {
		document.getElementById('txtPregOMP1').disabled = true;
	}
	if(document.getElementById('txtPregOMD1')) {
		document.getElementById('txtPregOMD1').disabled = true;
	}

	document.getElementById('txtPregOTF2').disabled = true;
	if(document.getElementById('txtPregOMP2')) {
		document.getElementById('txtPregOMP2').disabled = true;
	}
	if(document.getElementById('txtPregOMD2')) {
		document.getElementById('txtPregOMD2').disabled = true;
	}

	document.getElementById('PreguntasDivOTF1').style.display = 'none';
	if(document.getElementById('PreguntasDivOMP1')) {
		document.getElementById('PreguntasDivOMP1').style.display = 'none';
	}
	if(document.getElementById('PreguntasDivOMD1')) {
		document.getElementById('PreguntasDivOMD1').style.display = 'none';
	}

	document.getElementById('PreguntasDivOTF2').style.display = 'none';
	if(document.getElementById('PreguntasDivOMP2')) {
		document.getElementById('PreguntasDivOMP2').style.display = 'none';
	}
	if(document.getElementById('PreguntasDivOMD2')) {
		document.getElementById('PreguntasDivOMD2').style.display = 'none';
	}
}

function validaPuntos(idcampo){
  var valor=$("#"+idcampo).val();
  if(valor.indexOf(".")!==valor.lastIndexOf(".")){
    var tmp = valor.split(".");
    var valor1 = tmp[0];
    var valor2 = "." + valor.slice(valor.indexOf(".")).replace(/\./g,"");
    valor2 = (valor2==".")?"":valor2;
    valor = valor1 + valor2;
    $("#"+idcampo).val(valor);
  }else{
    if(isNaN(valor)){
      $("#"+idcampo).val("");
    }
  }
}

//Muestra la pantalla de espera de proceso.
function verNinja() {
	//gris (ocupar todo)
	$("#ninja").show();
	acomodarNinja();
	setTimeout("document.images['ninjaImg'].src=document.images['ninjaImg'].src", 10);
}

//Acomoda el espacio cubierto por la pantalla de espera.
function acomodarNinja(){
	//gris (ocupar todo)
	$("#ninja").css("height", $(document).height()+"px");
	$("#ninja").css("width", $(document).width()+"px");
}

function validarFecha(inputFecha) {
	var f=inputFecha.value;

	var re = /^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$/;
	if(f.length===0 || !re.exec(f)) {
		inputFecha.value = '';
		return false;
	}

	var d = new Date();
	d.setFullYear(parseInt(f.substring(6,10), 10));
	d.setMonth(parseInt(f.substring(3,5), 10) - 1);
	d.setDate(parseInt(f.substring(0,2), 10));

	if(d.getMonth() !== (parseInt(f.substring(3,5), 10) - 1)  || d.getDate() !== parseInt(f.substring(0,2), 10)) {
		inputFecha.value = '';
		return false;
	}

	return true;
}

function validaFormatoCorreo(inputCorreo){
    var strCorrecta;
    var correo = $(inputCorreo).val();
    strCorrecta = correo.toLowerCase();
    strCorrecta = trim(strCorrecta);
    $(inputCorreo).val(strCorrecta);
    
    if(strCorrecta === 'na') {
    	$(inputCorreo).val('NA');
    	return true;
    }
    
    if(strCorrecta !== ""){
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var validacion = filter.test(strCorrecta);
        if (!validacion) {
            $(inputCorreo).val('');
            jError('Por favor valide la Informaci\u00F3n capturada.',
                'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0003', 'Se ha capturado un correo electr&oacute;nico con un formato incorrecto.');
            } else if(strCorrecta.split('@')[0].length > 50) {
            $(inputCorreo).val('');
            jError('Por favor valide la Informaci\u00F3n capturada.',
                'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0003', 'Se ha capturado un correo electr&oacute;nico con una longitud de Buz&oacute;n incorrecta.');
        } else if(strCorrecta.split('@')[1].length > 30) {
            $(inputCorreo).val('');
            jError('Por favor valide la Informaci\u00F3n capturada.',
                'Error en la Informaci\u00F3n del Cuestionario', 'ERIC0003', 'Se ha capturado un correo electr&oacute;nico con una longitud de Dominio incorrecto.');
        }
    }
}

function trim(texto) {
    return (texto || "").replace( /^\s+|\s+$/g, "" );
}

function validarConcubina(){
    var opciones = document.getElementsByName('radioPregITCO');
    for(var contadorRadios = 0; contadorRadios < opciones.length; contadorRadios++){
        opciones[contadorRadios].checked = false;
    }

    ocultarConcubina();
}

function mostrarConcubina(){
    if(document.getElementById('txtPregINCO')) {
        document.getElementById('txtPregINCO').disabled = false;
        document.getElementById('PreguntasDivINCO').style.display = 'block';
    }
}

function ocultarConcubina(){
    if(document.getElementById('txtPregINCO')) {
        document.getElementById('txtPregINCO').disabled = true;
        document.getElementById('PreguntasDivINCO').style.display = 'none';
    }
}

function validarParticipacionesAccionarias(){
    var opciones = document.getElementsByName('radioPregNSC0');
    for(var contadorRadios = 0; contadorRadios < opciones.length; contadorRadios++){
        opciones[contadorRadios].checked = false;
    }

    ocultarParticipacionesAccionarias();
}

function mostrarParticipacionesAccionarias(){
    if(document.getElementById('txtPregNSC1')) {
        document.getElementById('txtPregNSC1').disabled = false;
        document.getElementById('PreguntasDivNSC1').style.display = 'block';
    }

    if(document.getElementById('txtPregNSC2')) {
        document.getElementById('txtPregNSC2').disabled = false;
        document.getElementById('PreguntasDivNSC2').style.display = 'block';
    }

    if(document.getElementById('txtPregNSC3')) {
        document.getElementById('txtPregNSC3').disabled = false;
        document.getElementById('PreguntasDivNSC3').style.display = 'block';
    }

    if(document.getElementById('txtPregNSC4')) {
        document.getElementById('txtPregNSC4').disabled = false;
        document.getElementById('PreguntasDivNSC4').style.display = 'block';
    }
}

function ocultarParticipacionesAccionarias(){
   if(document.getElementById('txtPregNSC1')) {
      document.getElementById('txtPregNSC1').disabled = true;
      document.getElementById('PreguntasDivNSC1').style.display = 'none';
   }
   if(document.getElementById('txtPregNSC2')) {
      document.getElementById('txtPregNSC2').disabled = true;
      document.getElementById('PreguntasDivNSC2').style.display = 'none';
   }
   if(document.getElementById('txtPregNSC3')) {
      document.getElementById('txtPregNSC3').disabled = true;
      document.getElementById('PreguntasDivNSC3').style.display = 'none';
   }
   if(document.getElementById('txtPregNSC4')) {
        document.getElementById('txtPregNSC4').disabled = true;
        document.getElementById('PreguntasDivNSC4').style.display = 'none';
   }

}
/**
 * Funcion para agregar las preguntas constestadas en el A1
 * por el rezago de Norkom con los relacionados
 */
function validaPreguntasA1PR(){
  var preguntas=$("#preguntasContestadas").val();
  console.log(preguntas);
  if(preguntas!=='' && preguntas.length>0 ){
    var allPreg=preguntas.split(':>>:');
    console.log(allPreg);
    for(var y=0;y<allPreg.length;y++){
      var tmp1=allPreg[y].split("==");
      console.log(tmp1);
      if(tmp1[0].indexOf('txt')===0 || tmp1[0].indexOf('select')===0){
        $("#"+tmp1[0]).val(tmp1[1]);
      }
      if(tmp1.indexOf('radio')===0){
        //seleccionar por nombre y id
        seleccionaSelectValor(tmp1);
      }
    }
  }
  
}

function seleccionaSelectValor(tmp1) {
  if($("#"+tmp1[0]+tmp1[1]).length){
    $("#"+tmp1[0]+tmp1[1]).attr('checked','checked');
    $("#"+tmp1[0]+tmp1[1]).click();
  }
}