/**
 * js principal del modulo de cuestionarios
 * 26-11-2014   creacion -LFER-
 */
var ID_FORMA = "frmTiposPreguntas";
var URL_GUARDAR_IC = "guardarCuestionarioIc.do";

$(function() {
	mostrarMensajeInicial();
	validarReferenciasBancarias();
	crearCalendarios();
});

function guardarCuestionarioIc(){
	document.getElementById('frmTiposPreguntas').action = 'guardarCuestionarioIp.do';
	document.getElementById('frmTiposPreguntas').submit();
}

function agregarFuenteIngresos(){
    var divContenedor = document.getElementById('PreguntasDivOTFIAux');
    divContenedor.innerHTML = divContenedor.innerHTML + "<BR>Fuente: <input type='text' id='txtPregOTFT" + contadorFuentes + "' name='txtPregOTFT" + contadorFuentes + "' onkeypress='return validaLetrasNumerosEspEvent(event)'>&nbsp;";
    divContenedor.innerHTML = divContenedor.innerHTML + "Monto: &dollar;<input type='text' id='txtPregOFMP" + contadorFuentes + "' name='txtPregOFMP" + contadorFuentes + "' onkeypress='return validaNumerosDecimalesEvent(event)'>";
    contadorFuentes++;
}

function mostrarOtrasFuentesIngresos(){
	document.getElementById('txtPregOTF1').disabled = false;
	document.getElementById('txtPregOTF2').disabled = false;
	
	if(document.getElementById('txtPregOMP1')){
		document.getElementById('txtPregOMP1').disabled = false;
	} else if(document.getElementById('txtPregOMD1')) {
		document.getElementById('txtPregOMD1').disabled = false;
	}
	
	if(document.getElementById('txtPregOMP2')){
		document.getElementById('txtPregOMP2').disabled = false;
	} else if(document.getElementById('txtPregOMD2')) {
		document.getElementById('txtPregOMD2').disabled = false;
	}
	
	document.getElementById('PreguntasDivOTF1').style.display = 'block';
	document.getElementById('PreguntasDivOTF2').style.display = 'block';
	
	if(document.getElementById('PreguntasDivOMP1')) {
		document.getElementById('PreguntasDivOMP1').style.display = 'block';
	} else if(document.getElementById('PreguntasDivOMD1')) {
		document.getElementById('PreguntasDivOMD1').style.display = 'block';
	}
	
	if(document.getElementById('PreguntasDivOMP2')) {
		document.getElementById('PreguntasDivOMP2').style.display = 'block';
	} else if(document.getElementById('PreguntasDivOMD2')) {
		document.getElementById('PreguntasDivOMD2').style.display = 'block';
	}
}

function ocultarOtrasFuentesIngresos(){
	document.getElementById('txtPregOTF1').disabled = true;
	document.getElementById('txtPregOTF2').disabled = true;
	
	if(document.getElementById('txtPregOMP1')){
		document.getElementById('txtPregOMP1').disabled = true;
	} else if(document.getElementById('txtPregOMD1')) {
		document.getElementById('txtPregOMD1').disabled = true;
	}
	
	if(document.getElementById('txtPregOMP2')){
		document.getElementById('txtPregOMP2').disabled = true;
	} else if(document.getElementById('txtPregOMD2')) {
		document.getElementById('txtPregOMD2').disabled = true;
	}
	
	document.getElementById('PreguntasDivOTF1').style.display = 'none';
	document.getElementById('PreguntasDivOTF2').style.display = 'none';
	
	if(document.getElementById('PreguntasDivOMP1')) {
		document.getElementById('PreguntasDivOMP1').style.display = 'none';
	} else if(document.getElementById('PreguntasDivOMD1')) {
		document.getElementById('PreguntasDivOMD1').style.display = 'none';
	}
	
	if(document.getElementById('PreguntasDivOMP2')) {
		document.getElementById('PreguntasDivOMP2').style.display = 'none';
	} else if(document.getElementById('PreguntasDivOMD2')) {
		document.getElementById('PreguntasDivOMD2').style.display = 'none';
	}
}

function mostrarReferenciasBancarias(){
	document.getElementById('txtPregBNC1').disabled = false;
	document.getElementById('txtPregNCD1').disabled = false;
	document.getElementById('txtPregBNC2').disabled = false;
	document.getElementById('txtPregNCD2').disabled = false;
	document.getElementById('txtPregJUST').disabled = true;
	
	document.getElementById('PreguntasDivBNC1').style.display = 'block';
	document.getElementById('PreguntasDivNCD1').style.display = 'block';
	document.getElementById('PreguntasDivBNC2').style.display = 'block';
	document.getElementById('PreguntasDivNCD2').style.display = 'block';
	document.getElementById('PreguntasDivJUST').style.display = 'none';
}

function ocultarReferenciasBancarias(){
	document.getElementById('txtPregBNC1').disabled = true;
	document.getElementById('txtPregNCD1').disabled = true;
	document.getElementById('txtPregBNC2').disabled = true;
	document.getElementById('txtPregNCD2').disabled = true;
	document.getElementById('txtPregJUST').disabled = false;
	
	document.getElementById('PreguntasDivBNC1').style.display = 'none';
	document.getElementById('PreguntasDivNCD1').style.display = 'none';
	document.getElementById('PreguntasDivBNC2').style.display = 'none';
	document.getElementById('PreguntasDivNCD2').style.display = 'none';
	document.getElementById('PreguntasDivJUST').style.display = 'block';
}