/**
 * js principal del modulo de cuestionarios
 * 26-11-2014   creacion -LFER-
 */
var ID_FORMA = "frmTiposPreguntas";
var URL_GUARDAR_IC = "guardarCuestionarioIc.do";

$(function() {
	mostrarMensajeInicial();
	crearCalendarios();
});

document.onload = function(){
	validarReferenciasBancarias();
	mostrarMensajeInicial();
};

function mostrarReferenciasBancarias(){
	document.getElementById('txtPregBNC1').disabled = false;
	document.getElementById('txtPregNCD1').disabled = false;
	document.getElementById('txtPregBNC2').disabled = false;
	document.getElementById('txtPregNCD2').disabled = false;
	document.getElementById('txtPregJUST').disabled = true;
	
	document.getElementById('PreguntasDivBNC1').style.display = 'block';
	document.getElementById('PreguntasDivNCD1').style.display = 'block';
	document.getElementById('PreguntasDivBNC2').style.display = 'block';
	document.getElementById('PreguntasDivNCD2').style.display = 'block';
	document.getElementById('PreguntasDivJUST').style.display = 'none';
}

function ocultarReferenciasBancarias(){
	document.getElementById('txtPregBNC1').disabled = true;
	document.getElementById('txtPregNCD1').disabled = true;
	document.getElementById('txtPregBNC2').disabled = true;
	document.getElementById('txtPregNCD2').disabled = true;
	document.getElementById('txtPregJUST').disabled = false;
	
	document.getElementById('PreguntasDivBNC1').style.display = 'none';
	document.getElementById('PreguntasDivNCD1').style.display = 'none';
	document.getElementById('PreguntasDivBNC2').style.display = 'none';
	document.getElementById('PreguntasDivNCD2').style.display = 'none';
	document.getElementById('PreguntasDivJUST').style.display = 'block';
}
