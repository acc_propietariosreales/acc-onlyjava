/**
 * js principal del modulo de cuestionarios
 * 26-11-2014   creacion -LFER-
 * version 1.2
 * fecha ultima Actualizacion: 11/05/2017 by LFER
 */
var ID_FORMA="frmTiposPreguntas";
var URL_ENVIAR="incio.do";

$(function() {
  mostrarMensajeInicialIP();
  mostrarCuestionarioEstatico();

  var image = $("#contextPath").val() + "/img/loading.gif";
  var divfondo = "<div id='ninja' style='display:none;'><img src='" + image +"' id='ninjaImg' name='ninjaImg'/></div>";
  $("#ninja").hide();
    //es importante que quede al principio del body
    $("body").prepend(divfondo);

    var selectUnico = $('.selectPregAutocompletar');
    if(selectUnico) {
        for(var contadorSelect = 0; contadorSelect < selectUnico.length; contadorSelect++) {
            $(selectUnico[contadorSelect]).combobox();
        }
    }
});

//Muestra la pantalla de espera de proceso.
function verNinja() {
  //gris (ocupar )
  $("#ninja").show();
  acomodarNinja();
  setTimeout("document.images['ninjaImg'].src=document.images['ninjaImg'].src", 10);
}

//Acomoda el espacio cubierto por la pantalla de espera.
function acomodarNinja(){
  //gris (ocupar )
  $("#ninja").css("height", $(document).height()+"px");
  $("#ninja").css("width", $(document).width()+"px");
}

/**
 * funciona para agregar funcionalidad de las preguntas dependientes
 */
function generarfuncionesMO(){
  //buscamos los elementos de clase DEPENDIENTES
  $('.dependencias').each(function(){
    var idpreg=$(this).attr('dep');
    var resp=$(this).attr('res');
    var trid=$(this).attr('idPreg');
    if($("#sino"+idpreg).length){
      //es de opcion si no
      $("input[id=sino"+idpreg+"]").each(function(){
        $(this).change(function(){
          if(resp===$(this).val() ){
            $("#TRpreg1_"+trid).attr("style","visibility:visible");
            $("#TRpreg2_"+trid).attr("style","visibility:visible");
          }else{
            $("#TRpreg1_"+trid).attr("style","visibility:hidden");
            $("#TRpreg2_"+trid).attr("style","visibility:hidden");
          }
        });
      });
    }
  });

}

function addPais(idtr){
  var html=$("#selectPais_"+idtr).html();
   $("#tdPreg_"+idtr).append("<select id='selectPais_"+idtr+"' class='paisesAgregados'>"+ html+"</select>");
}

function removePais(){
  //eliminamos el ultimo elemento
  $(".paisesAgregados:last").remove();
}

function habilitaDeshabilita(trid,resp,valor){
 
  if(resp===valor){
    $("#"+trid).attr("style","visibility:visible");
  }else{
    $("#"+trid).attr("style","visibility:hidden");
  }
}

function mostrarMensajeInicialIP() {
  var codigoOperacion = $('#codigoOperacion').val();
  var mensajeInicial = $('#msjOperacion').val();

  if(mensajeInicial === "") {
    mensajeInicial = "Error Inesperado";
  }

  if (codigoOperacion && codigoOperacion.length > 0) {
    if(codigoOperacion.indexOf("DBEX")===0){
      if(mensajeInicial === "" || mensajeInicial === "default") {
        mensajeInicial = "Error al realizar la conexi\u00F3n a Base de Datos";
      }
      jError(mensajeInicial, 'Error en Datos' , codigoOperacion,
      'Por favor revise la informaci\u00F3n proporcionada');
    }else{
      jError(mensajeInicial, 'Error al accesar al Cuestionario IP' , codigoOperacion,
          'Por favor revise la informaci\u00F3n proporcionada y vuelva a intentarlo');
    }
  }
  if("RS"===$("#regimenSimplificado").val()){
    var radios = document.getElementsByName('radioPregENFI');
    for(var contadorRadios = 0; contadorRadios < radios.length; contadorRadios++) {
      if(radios[contadorRadios].attributes['texto'].value==="SI" ){
        radios[contadorRadios].checked=true;
      }else{
        radios[contadorRadios].checked=false;
      }
      radios[contadorRadios].disabled = true;
    }
    jInfo('Su calificaci\u00F3n es diferente a A1, favor de llenar la informaci\u00F3n faltante', 'El Usuario es de Regimen Simplificado' , 'RS001',
    '');

  }
}

function habilitarPreguntasTransferencias(){
  document.getElementById('PreguntasDivTFPS').style.display = 'block';
  document.getElementById('PreguntasDivTIEN').style.display = 'block';
  document.getElementById('PreguntasDivTIRN').style.display = 'block';

  document.getElementById('selectPregMultipleTFPS').disabled = false;
}

function deshabilitarPreguntasTransferencias(){
  document.getElementById('PreguntasDivTFPS').style.display = 'none';
  document.getElementById('PreguntasDivTIEN').style.display = 'none';
  document.getElementById('PreguntasDivTIRN').style.display = 'none';

  document.getElementById('selectPregUnicoTIEN').selectedIndex = "0";
  document.getElementById('selectPregUnicoTIEM').selectedIndex = "0";
  document.getElementById('selectPregUnicoTIRN').selectedIndex = "0";
  document.getElementById('selectPregUnicoTIRM').selectedIndex = "0";

  document.getElementById('selectPregMultipleTFPS').disabled = true;
  $("#selectPregMultipleTFPS option").attr("selected", "selected");
  if($('#selectPregMultipleTFPS :selected').size() > 0) {
    moverOpcionesSelect('selectPregMultipleTFPS', 'selectPregMultipleOrigenTFPS');
  }
}

function guardarCuestionarioIp() {
  $("input:text").removeClass("CamposCompletar");
  $('#selectPregUnicoSALP').removeClass("CamposCompletar");
  $(".custom-combobox-input").each(function(){
    $(this).css('background-color', '#FFFFFF');
  });
  
  
  var validacionAutocompletableValida = true;
  $(".custom-combobox-input").each(function(){
    if($(this).val().length === 0) {
      var id=$(this).attr('id');
      jError('Por favor capture toda la Informaci\u00F3n.',
          'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0008', 'Es necesario capturar un valor en las listas autocompletables.');
      cambiaBackGround(id,'#C6F3FF');
      validacionAutocompletableValida = false;
    }
  });
  
  if(validacionAutocompletableValida === false) {
    return false;
  }
  
  if(validacionesGuardar()) {
    if($('#PreguntasDivSALP').length && $('#selectPregUnicoSALP').prop('selectedIndex')===0){
        $('#selectPregUnicoSALP').addClass("CamposCompletar");
        jError('Por favor capture toda la Informaci\u00F3n.',
            'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0007',
            'Es necesario capturar todos los valores, debe capturar valores mayores a cero para Saldo Promedio.');
        return false;
      
    }
    
    guardarCuestionarioIpHelper();
  }
}

function cambiaBackGround(id,color){
  $('#'+id).css('background-color', color);
}

function guardarCuestionarioIpHelper(){
  verNinja();
  var radios = document.getElementsByName('radioPregENFI');
  for(var contadorRadios = 0; contadorRadios < radios.length; contadorRadios++) {
    radios[contadorRadios].disabled = false;
  }
  document.getElementById("btnGuardarCuestionarioIP").disabled = true;
  $('#btnGuardarCuestionarioIP').hide();
  document.getElementById('frmTiposPreguntas').action = 'guardarCuestionarioIp.do';
  document.getElementById('frmTiposPreguntas').submit();
}
function validacionesGuardar(){
  return capturoPaises() === true  
  && validaTransferenciasInternacionales()===true 
  && validaCapturaDivisas() === true 
  && validaOperaciones();
}
function capturoPaises(){
    $("#selectPregMultipleTFPS option").attr("selected", "selected");
  if($("#PreguntasDivTFPS").css('display') !== 'none' && $('#selectPregMultipleTFPS :selected').size() > 0 || $("#PreguntasDivTFPS").css('display') === 'none') {
    $('#selectPregMultipleTFPS').removeClass("CamposCompletar");
    return true;
  } else {

    $('#selectPregMultipleTFPS').addClass("CamposCompletar");
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0002', 'Es necesario capturar todos los valores, debe seleccionar al menos un pa\u00EDs.');
    return false;
  }
}

function validaCapturaDivisas(){
  var capturoCompraVentaDivisas=false;
  if (($("#PreguntasDivCPDN").css('display') === 'block' && valida2combos("selectPregUnicoCPDN","selectPregUnicoCPDM"))
      || ($("#PreguntasDivVTDN").css('display') === 'block' && valida2combos("selectPregUnicoVTDN","selectPregUnicoVTDM") )
      ){
    capturoCompraVentaDivisas = true;
    remuevecamposCompletar('PreguntasDivCPDN', 'selectPregUnicoCPDN', 'selectPregUnicoCPDM');
    remuevecamposCompletar('PreguntasDivVTDN', 'selectPregUnicoVTDM', 'selectPregUnicoVTDN');

    }else{
      agregacamposCompletar('PreguntasDivCPDN','selectPregUnicoCPDN','selectPregUnicoCPDM');
      agregacamposCompletar('PreguntasDivVTDN','selectPregUnicoVTDN','selectPregUnicoVTDM');

      if($("#PreguntasDivCPDN").css('display') === 'block' || $("#PreguntasDivVTDN").css('display') === 'block' ){
        jError('Por favor capture toda la Informaci\u00F3n.',
            'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0005',
            'Es necesario capturar valores mayores a cero para Compra de Divisas o Venta de Divisas.');
        return false;
      }else{
        capturoCompraVentaDivisas = true;
      }
    }
  return capturoCompraVentaDivisas;
}
function validaTransferenciasInternacionales(){
  var transfInternacionales=false;
  if (($("#PreguntasDivTIEN").css('display') === 'block' && valida2combos("selectPregUnicoTIEN","selectPregUnicoTIEM"))
      || ($("#PreguntasDivTIRN").css('display') === 'block' && valida2combos("selectPregUnicoTIRN","selectPregUnicoTIRM") )
      ){
      transfInternacionales = true;
      remuevecamposCompletar('PreguntasDivTIEN', 'selectPregUnicoTIEN', 'selectPregUnicoTIEM');

      remuevecamposCompletar('PreguntasDivTIRN', 'selectPregUnicoTIRM', 'selectPregUnicoTIRN');

    }else{
      agregacamposCompletar('PreguntasDivTIEN','selectPregUnicoTIEN','selectPregUnicoTIEM');
      agregacamposCompletar('PreguntasDivTIRN','selectPregUnicoTIRN','selectPregUnicoTIRM');
      if($("#PreguntasDivTIEN").css('display') === 'block' || $("#PreguntasDivTIRN").css('display') === 'block' ){
        jError('Por favor capture toda la Informaci\u00F3n.',
              'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0004',
              'Es necesario capturar valores mayores a cero para Transferencias Internacionales RECIBIDAS o ENVIADAS.');
        return false;
      }else{
        transfInternacionales = true;
      }
    }
  return transfInternacionales;
}
function remuevecamposCompletar(div,campo1,campo2){
  if($("#"+div).css('display') === 'block' ){
    $('#'+campo1).removeClass("CamposCompletar");
    $('#'+campo2).removeClass("CamposCompletar");
  }
}

function agregacamposCompletar(div,campo1,campo2){
  if($("#"+div).css('display') === 'block' ){
    $('#'+campo1).addClass("CamposCompletar");
    $('#'+campo2).addClass("CamposCompletar");
  }
}

function valida2combos(combo1,combo2){
  return ($('#'+combo1).prop('selectedIndex') > 0 && $('#'+combo2).prop('selectedIndex') > 0);
}

function deshabilitarRegimenSimplificado() {
  document.getElementById('PreguntasDivORRC').style.display = 'block';
  document.getElementById('PreguntasDivDTRC').style.display = 'block';
  document.getElementById('PreguntasDivSTFI').style.display = 'block';
  document.getElementById('PreguntasDivTFPS').style.display = 'block';
  document.getElementById('PreguntasDivCVDV').style.display = 'block';

  document.getElementById('selectPregUnicoORRC').disabled = false;
  document.getElementById('selectPregUnicoDTRC').disabled = false;

  var radios = document.getElementsByName('radioPregSTFI');
  for(var contadorRadios = 0; contadorRadios < radios.length; contadorRadios++) {
    radios[contadorRadios].disabled = false;
  }

  document.getElementById('selectPregMultipleTFPS').disabled = false;
  var radiosPreg = document.getElementsByName('radioPregCVDV');
  for(var intradios = 0; intradios < radiosPreg.length; intradios++) {
    radiosPreg[intradios].disabled = false;
  }

  document.getElementById('PreguntasDivTIEN').style.display = 'block';
  document.getElementById('PreguntasDivTIRN').style.display = 'block';
  document.getElementById('PreguntasDivTNEN').style.display = 'block';
  document.getElementById('PreguntasDivTNRN').style.display = 'block';
  document.getElementById('PreguntasDivDPEN').style.display = 'block';
  document.getElementById('PreguntasDivDNEN').style.display = 'block';
  document.getElementById('PreguntasDivRTEN').style.display = 'block';
  document.getElementById('PreguntasDivRNEN').style.display = 'block';
  if($('#PreguntasDivCPDN').length){
  document.getElementById('PreguntasDivCPDN').style.display = 'block';
  }
  if($('#PreguntasDivVTDN').length){
  document.getElementById('PreguntasDivVTDN').style.display = 'block';
  }
}

function habilitarRegimenSimplificado(){
  if(document.getElementById('claveActividadFinanciera') && document.getElementById('claveActividadFinanciera').value === 'S' &&
      document.getElementById('clavePaisResidenciaBajoRiesgo') && document.getElementById('clavePaisResidenciaBajoRiesgo').value === 'S')
  {
    document.getElementById('PreguntasDivORRC').style.display = 'none';
    document.getElementById('PreguntasDivDTRC').style.display = 'none';
    document.getElementById('PreguntasDivSTFI').style.display = 'none';
    document.getElementById('PreguntasDivTFPS').style.display = 'none';
    document.getElementById('PreguntasDivCVDV').style.display = 'none';

    document.getElementById('selectPregUnicoORRC').disabled = true;
    document.getElementById('selectPregUnicoDTRC').disabled = true;

    var radios = document.getElementsByName('radioPregSTFI');
    for(var contadorRadios = 0; contadorRadios < radios.length; contadorRadios++) {
      radios[contadorRadios].disabled = true;
    }

    document.getElementById('selectPregMultipleTFPS').disabled = true;
    $("#selectPregMultipleTFPS option").attr("selected", "selected");
    if($('#selectPregMultipleTFPS :selected').size() > 0) {
      moverOpcionesSelect('selectPregMultipleTFPS', 'selectPregMultipleOrigenTFPS');
    }
    
    var radiospregcvdv = document.getElementsByName('radioPregCVDV');
    for(var intradiospreg = 0; intradiospreg < radiospregcvdv.length; intradiospreg++) {
      radiospregcvdv[intradiospreg].disabled = true;
    }

    document.getElementById('PreguntasDivTIEN').style.display = 'none';
    document.getElementById('PreguntasDivTIRN').style.display = 'none';
    document.getElementById('PreguntasDivTNEN').style.display = 'none';
    document.getElementById('PreguntasDivTNRN').style.display = 'none';
    document.getElementById('PreguntasDivDPEN').style.display = 'none';
    document.getElementById('PreguntasDivDNEN').style.display = 'none';
    document.getElementById('PreguntasDivRTEN').style.display = 'none';
    document.getElementById('PreguntasDivRNEN').style.display = 'none';
    if($('#PreguntasDivVTDN').length){
    document.getElementById('PreguntasDivCPDN').style.display = 'none';
    }
    if($('#PreguntasDivVTDN').length){
    document.getElementById('PreguntasDivVTDN').style.display = 'none';
    }

    document.getElementById('selectPregUnicoTIEN').selectedIndex = "0";
    document.getElementById('selectPregUnicoTIEM').selectedIndex = "0";
    document.getElementById('selectPregUnicoTIRN').selectedIndex = "0";
    document.getElementById('selectPregUnicoTIRM').selectedIndex = "0";
    document.getElementById('selectPregUnicoTNEN').selectedIndex = "0";
    document.getElementById('selectPregUnicoTNEM').selectedIndex = "0";
    document.getElementById('selectPregUnicoTNRN').selectedIndex = "0";
    document.getElementById('selectPregUnicoTNRM').selectedIndex = "0";
    document.getElementById('selectPregUnicoDPEN').selectedIndex = "0";
    document.getElementById('selectPregUnicoDPEM').selectedIndex = "0";
    document.getElementById('selectPregUnicoDNEN').selectedIndex = "0";
    document.getElementById('selectPregUnicoDNEM').selectedIndex = "0";
    document.getElementById('selectPregUnicoRTEN').selectedIndex = "0";
    document.getElementById('selectPregUnicoRTEM').selectedIndex = "0";
    document.getElementById('selectPregUnicoRNEN').selectedIndex = "0";
    document.getElementById('selectPregUnicoRNEM').selectedIndex = "0";
    document.getElementById('selectPregUnicoCPDN').selectedIndex = "0";
    document.getElementById('selectPregUnicoCPDM').selectedIndex = "0";
    document.getElementById('selectPregUnicoVTDN').selectedIndex = "0";
    document.getElementById('selectPregUnicoVTDM').selectedIndex = "0";
  }
}

function habilitarCompraVentaDivisas(){
  if($('#PreguntasDivCPDN').length){
  document.getElementById('PreguntasDivCPDN').style.display = 'block';
  }
  if($('#PreguntasDivVTDN').length){
  document.getElementById('PreguntasDivVTDN').style.display = 'block';
  }
}

function deshabilitarCompraVentaDivisas(){
  if($('#PreguntasDivCPDN').length){
    document.getElementById('PreguntasDivCPDN').style.display = 'none';
    document.getElementById('selectPregUnicoCPDN').selectedIndex = "0";
    document.getElementById('selectPregUnicoCPDM').selectedIndex = "0";
  }
  if($('#PreguntasDivVTDN').length){
  document.getElementById('PreguntasDivVTDN').style.display = 'none';
  document.getElementById('selectPregUnicoVTDN').selectedIndex = "0";
  document.getElementById('selectPregUnicoVTDM').selectedIndex = "0";
  }
}

function mostrarCuestionarioEstatico() {
  if(document.getElementById('tipoCuestionarioOpics').value === 'E') {
    document.getElementById('PreguntasDivORRC').style.display = 'none';
    document.getElementById('PreguntasDivDTRC').style.display = 'none';
    document.getElementById('PreguntasDivSTFI').style.display = 'none';
    document.getElementById('PreguntasDivTFPS').style.display = 'none';
    document.getElementById('PreguntasDivCVDV').style.display = 'none';

    document.getElementById('selectPregUnicoORRC').disabled = true;
    document.getElementById('selectPregUnicoDTRC').disabled = true;

    var radios = document.getElementsByName('radioPregSTFI');
    for(var contadorRadios = 0; contadorRadios < radios.length; contadorRadios++) {
      radios[contadorRadios].disabled = true;
    }

    document.getElementById('selectPregMultipleTFPS').disabled = true;
    $("#selectPregMultipleTFPS option").attr("selected", "selected");
    if($('#selectPregMultipleTFPS :selected').size() > 0) {
      moverOpcionesSelect('selectPregMultipleTFPS', 'selectPregMultipleOrigenTFPS');
    }
    
    document.getElementsByName('radioPregCVDV').disabled = true;
    var radioPregCvDv = document.getElementsByName('radioPregCVDV');
    for(var intcontadorradios = 0; intcontadorradios < radioPregCvDv.length; intcontadorradios++) {
      radioPregCvDv[intcontadorradios].disabled = true;
    }

    document.getElementById('PreguntasDivTIEN').style.display = 'none';
    document.getElementById('PreguntasDivTIRN').style.display = 'none';
    document.getElementById('PreguntasDivTNEN').style.display = 'none';
    document.getElementById('PreguntasDivTNRN').style.display = 'none';
    document.getElementById('PreguntasDivDPEN').style.display = 'none';
    document.getElementById('PreguntasDivDNEN').style.display = 'none';
    document.getElementById('PreguntasDivRTEN').style.display = 'none';
    document.getElementById('PreguntasDivRNEN').style.display = 'none';
    document.getElementById('PreguntasDivCPDN').style.display = 'none';
    document.getElementById('PreguntasDivVTDN').style.display = 'none';
  }
}
