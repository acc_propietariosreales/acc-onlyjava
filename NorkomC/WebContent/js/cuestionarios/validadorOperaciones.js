/**
 * JS para las validaciones de la seccion de operaciones por No y monto
 * del cuestionario IP al momento de guardar el cuestionario
 * version 1.2
 * Fecha ultima Actualizacion: 11/05/2017 by LFER
 * 
 */


function validaOperaciones(){
  if(esAppValida()){
    var idOperGen=$("#OPER_GEN").val();
    var tipo=$("#tipoCuestionarioOpics").val();
    var actFinanciera = $("#claveActividadFinanciera").val();
    var rs = $("#radioPregENFI54").is(':checked');
    
    if(idOperGen!==''){
    	if(tipo==='E' || actFinanciera==='S'){
    		if(rs){
    			if((validaNoSeleccionesTransferencias() || validaNoSeleccionesDivisas())){
    				return true;
    			}
    		} else{
    			return true;
    		}
    	} else if((validaNoSeleccionesTransferencias() || validaNoSeleccionesDivisas())){
    		return true; 
    	}
      return validaOperacionesHelper();
    }else{
      return false;
    }
  
  }
  return true;
}

function validaOperacionesHelper() {
  if(!mensajesNumeros()){
    return false;
  }
  if(!mensajesMonto()){
    return false;
  }
 return validaOperacioneYMonto();
}

function validaOperacioneYMonto(){
  var idOperGen=$("#OPER_GEN").val();
  var ids=idOperGen.split(',');
  
  var combos=[];
  var combosOp=[];
  $('select.selectPregUnico').each(function(){
    var idSelF=$(this).attr('id');
    var idSel=idSelF.replace('selectPregUnico','');
    var i=0;
    for(;i<ids.length;i++){
      if(idSel===ids[i]){
        combos[i]=idSelF;
        combosOp[i]=idSelF.substring(0,idSelF.length-1)+'M';
      }
    }
  });
  var valida=0;
  for(;valida<combos.length;valida++){
    $("#"+combos[valida]).removeClass("CamposCompletar");
  }
  valida=0;
  var error=true;
  for(;valida<combos.length;valida++){
    var combo=$("#"+combos[valida]+" option:selected" ).text().replace(/^\s+|\s+$/g, '');
    var comboOpuesto=$("#"+combosOp[valida]+" option:selected" ).text().replace(/^\s+|\s+$/g, '');
    if( (combo==='0' && comboOpuesto!=='0') || (combo!=='0' && comboOpuesto==='0') ){
      $("#"+combos[valida]).addClass("CamposCompletar");
      $("#"+combosOp[valida]).addClass("CamposCompletar");
      error=false;
    }
  }
  if(!error){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0005',
        'Es necesario capturar valores mayores a cero para No. y Monto de cada Transacci�n.');
  }
  return error;
} 
function validaNoSeleccionesTransferencias(){
  var tmp=($("#PreguntasDivTIEN").css('display') === 'block' && $("#PreguntasDivTIRN").css('display')==='block');
  return tmp;
}
function validaNoSeleccionesDivisas(){
  var tp= ($("#PreguntasDivCPDN").css('display') === 'block' && $("#PreguntasDivVTDN").css('display') === 'block');
  return tp;
}
function mensajesNumeros(){
  var idOperGen=$("#OPER_GEN").val();
  var idOperDep=$("#OPER_DEP").val();
  var idOperRet=$("#OPER_RET").val();
  if(validaOperacionesValida(idOperGen)){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0011',
        //'Para todos los tipos de operaci�n del cuestionario se tiene un valor declarado de "cero" en el N�MERO DE OPERACIONES, debe existir un valor diferente a "cero" al menos para un tipo de operaci�n de dep�sito y para un tipo de operaci�n de retiro'
        'Es necesario capturar valores mayores a cero para No. de Transacciones de Dep�sito y Retiro.'
        );
    return false;
  }
  if(validaOperacionesValida(idOperDep)){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0012',
        'Es necesario capturar valores mayores a cero para No. de Transacciones de Dep�sito.');
    return false;
  }
  if(validaOperacionesValida(idOperRet)){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0013',
        'Es necesario capturar valores mayores a cero para No. de Transacciones de Retiro.');
    return false;
  }
  return true;
}
function mensajesMonto(){
  var idMontGen=$("#MONT_GEN").val();
  var idMontDep=$("#MONT_DEP").val();
  var idMontRet=$("#MONT_RET").val();
  if(validaOperacionesValida(idMontGen)){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0014',
        'Es necesario capturar valores mayores a cero para Monto de Transacciones de Dep�sito y Retiro.');
    return false;
  }
  if(validaOperacionesValida(idMontDep)){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0015',
        'Es necesario capturar valores mayores a cero para Monto de Transacciones de Dep�sito.');
    return false;
  }
  if(validaOperacionesValida(idMontRet)){
    jError('Por favor capture toda la Informaci\u00F3n.',
        'Error en la Informaci\u00F3n del Cuestionario', 'ERIP0016',
        'Es necesario capturar valores mayores a cero para Monto de Transacciones de Retiro.');
    return false;
  }
  return true;
}

function esAppValida(){
  var tipoAplicativo=$("#tipoApp").val();
  var tiposAppValida=$("#AppsValidar").val();

  if(tiposAppValida.length>0){
    if(tiposAppValida.indexOf(',')>0){
      if(esAppValidaHelper(tiposAppValida,tipoAplicativo)){
        return true;
      }
    }else{
      if(tipoAplicativo===tiposAppValida){
        return true;
      }
    }
  }
  return false;
}
function esAppValidaHelper(tiposAppValida,tipoAplicativo){
  var apparray=tiposAppValida.split(',');
  for(var t=0;t<apparray.length;t++){
    if(tipoAplicativo===apparray[t]){
      return true;
    }
  }
  return false;
}
function validaOperacionesValida(idsAvalidar){
  var ids=idsAvalidar.split(',');
  var invalidos=0;
  var combos=[];
  $('select.selectPregUnico').each(function(){
    var idSelF=$(this).attr('id');
    var idSel=idSelF.replace('selectPregUnico','');
    var i=0;
    for(;i<ids.length;i++){
      if(idSel===ids[i]){
        combos[i]=idSelF;
      }
    }
  });
  var valida=0;
  for(;valida<combos.length;valida++){
    $("#"+combos[valida]).removeClass("CamposCompletar");
  }
  valida=0;
  for(;valida<combos.length;valida++){
    var combo=$("#"+combos[valida]+" option:selected" ).text().replace(/^\s+|\s+$/g, '');
    if(combo==='0'){
      invalidos++;
    }
  }
  if(invalidos===ids.length){
    valida=0;
    for(;valida<combos.length;valida++){
      $("#"+combos[valida]).addClass("CamposCompletar");
    }
  }
  return (invalidos===ids.length);
}