<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../encabezadoInternoIP.jsp" flush="true"/>

<spring:message code="general.cuestionario.preliminar" var="titPreliminar"></spring:message>
<spring:message code="cuestionario.titulo"            var="moduloTipoPreg"></spring:message>
<spring:message code="cuestionario.si"          var="si"></spring:message>
<spring:message code="cuestionario.no"          var="no"></spring:message>
<spring:message code="cuestionario.seleccione"  var="seleccione"></spring:message>
<!-- encabezado de pagina-->
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/muestraCuestionario.js?version=1.0" type="text/javascript"></script>

<div class="pageTitleContainer">
<span class="pageTitle">No es posible mostrar el cuestionario debido a que el Token no es correcto</span></div>

<!-- Detalle -->
<form action="" name="frmTiposPreguntas" id="frmTiposPreguntas" method="POST">
	<input type="hidden" name="msjOperacion" id="msjOperacion" value="ERTK0000">
	<input type="hidden" name="idCuestionarioCC" id="idCuestionarioCC" value="">
	<input type="hidden" name="nivelRiesgoCC" id="nivelRiesgoCC" value="">
	<input type="hidden" name="indicadorUpldCC" id="indicadorUpldCC" value="">
	
	<div class="frameTablaEstandar" id="contenedorPaginadorResultados"> 
	        <div class="titleBuscadorSimple">
	             <span>ERROR</span>
	        </div>
			<div id="divPrincipalDatos" >
	        <div id="DivDatos1"  class="contentTablaVariasColumnas">
	            <table id="TablaDatos1">
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                       <td>(${fechaCues})</td>
	                       <td>Ha finalizado el proceso de llenado de cuestionario IP e IC</td>
	                    </tr>
	                </thead>
	                <tbody>
	                </tbody>
	           </table>           
	       </div>
	      </div>
	</div>
</form>
<br>
<br>

<jsp:include page="../private/myFooter.jsp" flush="true"/>
