<%--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* encabezadoInterno.jsp
*
* Control de versiones:
*
* Version Date/Hour        By                   Company             Description
* ------- ---------------- -------------------- ------------------- ---------------------------
* 1.0     23/10/2012 09:48 LFEspinosa            Stefanini           Encabezado de pagina con Datos de encabezado
***********************************************************************************************
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<spring:message code="general.encabezado.sucursal"              var="enc_sucursal"/>
<spring:message code="general.encabezado.nombre"                var="enc_nombre"/>
<spring:message code="general.encabezado.zona"                  var="enc_zona"/>
<spring:message code="general.encabezado.plaza"                 var="enc_plaza"/>
<spring:message code="general.encabezado.numCliente"            var="enc_numcli"/>
<spring:message code="general.encabezado.cuentaCntr"            var="enc_cta"/>
<spring:message code="general.encabezado.segmento"              var="enc_segmento"/>
<spring:message code="general.encabezado.producto"              var="enc_prod"/>
<spring:message code="general.encabezado.pais"                  var="enc_pais"/>
<spring:message code="general.encabezado.encaso"                var="enc_encaso"/> 
<spring:message code="general.encabezado.edo"                   var="enc_edo"/>
<spring:message code="general.encabezado.mun"                   var="enc_municipio"/>
<spring:message code="general.encabezado.nac"                   var="enc_nacionalidad"/>
<spring:message code="general.encabezado.act.gen"               var="enc_acti_gen"/>
<spring:message code="general.encabezado.act.esp"               var="enc_act_esp"/>
<spring:message code="general.encabezado.tit.seg"               var="enc_tit_segmento"/>
<spring:message code="general.encabezado.tit.prod"              var="enc_tit_prod"/>
<spring:message code="general.encabezado.tit.zona"              var="enc_tit_zona"/>
<spring:message code="general.encabezado.tit.nac"               var="enc_tit_nac"/>
<spring:message code="general.encabezado.tit.act"               var="enc_tit_actividad"/>

<spring:message code="general.cuestionario.producto"               var="cuest_producto"/>
<spring:message code="general.cuestionario.pais.res"               var="cuest_pais_res"/>
<spring:message code="general.cuestionario.estado"               var="cuest_estado"/>
<spring:message code="general.cuestionario.municipio"               var="cuest_municipio"/>
<spring:message code="general.cuestionario.nacionalidad"               var="cuest_nacionalidad"/>
<spring:message code="general.cuestionario.actividad.gen"               var="cuest_actividad_gen"/>
<spring:message code="general.cuestionario.actividad.esp"               var="cuest_actividad_esp"/>

<spring:message code="general.salir"                            var="salir"/>

<link href="${pageContext.servletContext.contextPath}/lf/default/css/dialogBox/jquery.alerts.css"      type="text/css" rel="stylesheet" />
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js"        type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery.ui.draggable.js" type="text/javascript"></script>		
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery.alerts.js"       type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery.bgiframe.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"                        type="text/javascript"></script>

<script src="${pageContext.servletContext.contextPath}/lf/default/js/popup/popup.js"                   type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"                 type="text/css"   rel="stylesheet" >
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css"      type="text/css"   rel="stylesheet" >
<link href="${pageContext.servletContext.contextPath}/lf/default/css/popup.css"                        type="text/css"   rel="stylesheet" >
<script src="${pageContext.servletContext.contextPath}/lf/default/js/buttonBack.js"                     type="text/javascript"></script>
<script type="text/javascript">
			var jqueryAlerta 		= 'Alerta';
			var jqueryError 		= 'Error';
			var jqueryInfo 			= 'Info';
			var jqueryAyuda 		= 'Ayuda';
			var jqueryConfirmar 	= 'Confirmar';
			var jqueryAviso 		= 'Aviso';
			var jquerySelect 		= 'Seleccionar';
</script>
</head>
<div id="header">
	<input id="contextPath" type="hidden" value="${pageContext.servletContext.contextPath}/lf/default" />
  	<img id="logoHeader" src="${pageContext.servletContext.contextPath}/lf/default/img/menu/logoCorporativo.jpg" >
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<br>
	<div id="globalMenu">
		<a href="${LyFBean.linkHeader5}" class="linkGlobal" target="_blank" title="Link hacia ${labelLinkHeader5}">${labelLinkHeader5}</a>
				<a href="${pageContext.servletContext.contextPath}${LyFBean.linkSalirApp}" class="button" id="buttonSalir" target="_top">${salir}</a>
	</div>
	<br><br>
</div>
<div class="contentBuscadorSimple" align="center" id="encabezado"> 
	<table>
	    <tbody>
	 	<tr>
	         <td class="text_derecha" colspan="2">${enc_sucursal}:</td>
	         <td class="text_izquierda" colspan="4"> <input type="text" id="txtSucursal" class="" readonly="readonly" size="70" value="${datosCuestionario.codigoSucursal} - ${datosCuestionario.nombreSucursal}"> </td> 
	         <td class="text_derecha" colspan="2"></td>
	         <td class="text_derecha" ></td>                 
	    </tr>
	    <tr>
	    	<td class="text_derecha" colspan="2">${enc_nombre}:</td>
	    	<td class="text_izquierda" colspan="4"> <input type="text" id="txtNombre" class="" readonly="readonly" size="70" value="${datosCuestionario.nombreCompletoCliente}"> </td>
	    	<td class="text_derecha" colspan="2"></td>
	        <td class="text_derecha" ></td> 
	    </tr>
	    <tr>
	    	<td class="text_derecha" colspan="2">${enc_zona}:</td>
	    	<td class="text_izquierda" colspan="4"> <input type="text" id="txtZona" class="" readonly="readonly" size="60" value="${datosCuestionario.nombreRegion}"> </td>
	    	<td class="text_derecha" colspan="2"></td>
	        <td class="text_derecha" ></td> 
	    </tr>
	    <tr>
	    	<td class="text_derecha" colspan="2">${enc_plaza}:</td>
	    	<td class="text_izquierda" colspan="4"> <input type="text" id="txtPlaza" class="" readonly="readonly" size="60" value="${datosCuestionario.nombrePlaza}"> </td>
	    	<td class="text_derecha" colspan="2"></td>
	        <td class="text_derecha" ></td> 
	    </tr>
	    <tr>
	    	<td class="text_derecha" colspan="2">${enc_numcli}:</td>
	    	<td class="text_izquierda" colspan="2"> <input type="text" id="txtNumCli" class="" readonly="readonly" size="20" value="${datosCuestionario.codigoCliente}"> </td>
	    	<td class="text_derecha" >${enc_cta}:</td>
	        <td class="text_izquierda" ><input type="text" id="txtCta" class="" readonly="readonly" size="20"></td> 
	    </tr>
	    <tr>
	    	<td class="text_derecha" colspan="2"></td>
	    	<td class="text_derecha" colspan="2"></td>
	    	<td class="text_derecha" >${enc_segmento}:</td>
	        <td class="text_izquierda" ><input type="text" id="txtSegmento" class="" readonly="readonly" size="20" value="${datosCuestionario.descSegmento}"></td> 
	    </tr>
	    </tbody>	
	</table>
</div>

<div class="contentBuscadorSimple" align="center" id="encabezadoProducto"> 
	<table>
	    <tbody>
	 	<tr>
	         <td class="text_derecha">${cuest_producto}:</td>
	         <td class="text_izquierda"> 
	         	<input type="text" id="txtProducto" class="" readonly="readonly" size="70" 
	         	value="${datosCuestionario.codigoProducto} - ${datosCuestionario.codigoSubproducto} ${datosCuestionario.descProducto}">
	         </td> 
	    </tr>
	    </tbody>	
	</table>
</div>
<div class="contentBuscadorSimple" align="center" id="encabezadoZonaGeografica"> 
	<table>
	    <tbody>
	 	<tr>
	         <td class="text_derecha">${cuest_pais_res}:</td>
	         <td class="text_izquierda"> 
	         	<input type="text" id="txtPaisRes" class="" readonly="readonly" size="70" 
	         	value="${datosCuestionario.nombrePais}">
	         </td>
	    </tr>
	    	<tr>
		    	<td class="text_derecha">${cuest_estado}:</td>
		         <td class="text_izquierda"> 
		         	<input type="text" id="txtEstado" class="" readonly="readonly" size="70" 
		         	value="${datosCuestionario.nombreEntidad}">
		         </td>
			</tr>
	    	<tr>
		    	<td class="text_derecha">${cuest_municipio}:</td>
		         <td class="text_izquierda"> 
		         	<input type="text" id="txtMunicipio" class="" readonly="readonly" size="70" 
		         	value="${datosCuestionario.nombreMunicipio}">
		         </td>
			</tr>
	    </tbody>	
	</table>
</div>
<div class="contentBuscadorSimple" align="center" id="encabezadoNacionalidad"> 
	<table>
	    <tbody>
	 	<tr>
	         <td class="text_derecha">${cuest_nacionalidad}:</td>
	         <td class="text_izquierda"> 
	         	<input type="text" id="txtNacionalidad" class="" readonly="readonly" size="70" 
	         	value="${datosCuestionario.nombreNacionalidad}">
	         </td> 
	    </tr>
	    </tbody>	
	</table>
</div>
<div class="contentBuscadorSimple" align="center" id="encabezadoActividades"> 
	<table>
	    <tbody>
	 	<tr>
	         <td class="text_derecha">${cuest_actividad_gen}:</td>
	         <td class="text_izquierda"> 
	         	<input type="text" id="txtActGen" class="" readonly="readonly" size="70" 
	         	value="${datosCuestionario.descripcionActGenerica}">
	         </td>
	    </tr>
	    <tr>
	    	<td class="text_derecha">${cuest_actividad_esp}:</td>
	         <td class="text_izquierda"> 
	         	<input type="text" id="txtActEsp" class="" readonly="readonly" size="70" 
	         	value="${datosCuestionario.descripcionActEspecifica}">
	         </td>
	 	</tr>
	    </tbody>	
	</table>
</div>