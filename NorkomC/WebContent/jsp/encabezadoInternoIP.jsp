<%--
**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* encabezadoInterno.jsp
*
* Control de versiones:
*
* Version Date/Hour        By                   Company             Description
* ------- ---------------- -------------------- ------------------- ---------------------------
* 1.0     23/10/2012 09:48 LFEspinosa            Stefanini           Encabezado de pagina con Datos de encabezado
***********************************************************************************************
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="-1" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<spring:message code="general.encabezado.sucursal"              var="enc_sucursal"/>
<spring:message code="general.encabezado.nombre"                var="enc_nombre"/>
<spring:message code="general.encabezado.zona"                  var="enc_zona"/>
<spring:message code="general.encabezado.plaza"                 var="enc_plaza"/>
<spring:message code="general.encabezado.numCliente"            var="enc_numcli"/>
<spring:message code="general.encabezado.cuentaCntr"            var="enc_cta"/>
<spring:message code="general.encabezado.segmento"              var="enc_segmento"/>
<spring:message code="general.encabezado.producto"              var="enc_prod"/>
<spring:message code="general.encabezado.pais"                  var="enc_pais"/>
<spring:message code="general.encabezado.encaso"                var="enc_encaso"/> 
<spring:message code="general.encabezado.edo"                   var="enc_edo"/>
<spring:message code="general.encabezado.mun"                   var="enc_municipio"/>
<spring:message code="general.encabezado.nac"                   var="enc_nacionalidad"/>
<spring:message code="general.encabezado.act.gen"               var="enc_acti_gen"/>
<spring:message code="general.encabezado.act.esp"               var="enc_act_esp"/>
<spring:message code="general.encabezado.tit.seg"               var="enc_tit_segmento"/>
<spring:message code="general.encabezado.tit.prod"              var="enc_tit_prod"/>
<spring:message code="general.encabezado.tit.zona"              var="enc_tit_zona"/>
<spring:message code="general.encabezado.tit.nac"               var="enc_tit_nac"/>
<spring:message code="general.encabezado.tit.act"               var="enc_tit_actividad"/>

<spring:message code="general.cuestionario.producto"               var="cuest_producto"/>
<spring:message code="general.cuestionario.pais.res"               var="cuest_pais_res"/>
<spring:message code="general.cuestionario.estado"               var="cuest_estado"/>
<spring:message code="general.cuestionario.municipio"               var="cuest_municipio"/>
<spring:message code="general.cuestionario.nacionalidad"               var="cuest_nacionalidad"/>
<spring:message code="general.cuestionario.actividad.gen"               var="cuest_actividad_gen"/>
<spring:message code="general.cuestionario.actividad.esp"               var="cuest_actividad_esp"/>

<spring:message code="general.salir"                            var="salir"/>

<link href="${pageContext.servletContext.contextPath}/lf/default/css/calendar/calendar.css"            type="text/css" rel="stylesheet"  />
<link href="${pageContext.servletContext.contextPath}/lf/default/css/dialogBox/jquery.alerts.css"      type="text/css" rel="stylesheet" />
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery-1.2.6.js"        type="text/javascript"></script>
<script type="text/javascript">
var jQuery_1_2_6 = $.noConflict(true);
</script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-1.10.2.js"        type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery.ui.draggable.js" type="text/javascript"></script>		
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery.alerts.js"       type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/dialogBox/jquery.bgiframe.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/global.js"                        type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/calendar/calendar.js"             type="text/javascript" ></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/calendar/calendarSetup.js"        type="text/javascript" ></script>

<script src="${pageContext.servletContext.contextPath}/lf/default/js/calendar/calendarES.js"           type="text/javascript" ></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/popup/popup.js"                   type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estilos.css"                 type="text/css"   rel="stylesheet" >
<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/elementos_interfaz.css"      type="text/css"   rel="stylesheet" >
<link href="${pageContext.servletContext.contextPath}/lf/default/css/popup.css"                        type="text/css"   rel="stylesheet" >
<script src="${pageContext.servletContext.contextPath}/lf/default/js/buttonBack.js"                     type="text/javascript"></script>
<script type="text/javascript">
			var jqueryAlerta 		= 'Alerta';
			var jqueryError 		= 'Error';
			var jqueryInfo 			= 'Info';
			var jqueryAyuda 		= 'Ayuda';
			var jqueryConfirmar 	= 'Confirmar';
			var jqueryAviso 		= 'Aviso';
			var jquerySelect 		= 'Seleccionar';
</script>
</head>
<body oncontextmenu="return false">
<input id="contextPath" type="hidden" value="${pageContext.servletContext.contextPath}/lf/default" />
