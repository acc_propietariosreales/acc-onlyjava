<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="${pageContext.servletContext.contextPath}/lf/default/css/menu/estiloErrorGral.css"            rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ERROR</title>
</head>
<body> 
	<input type="hidden" name="END_PROCESS_GC" id="END_PROCESS_GC" value="true"/>
	<div id="wrapper"> 
	  <div class="header" id="header" > 
	    <div class="headerlogo" id="headerlogo"> 
	     </div> 
	  </div> 
	  <div id="body"> 
	    <div id="content"> 
	      <h2 align="center">ERROR</h2>
	      <P>ERROR 404</P>
	      <p>No se ha encontrado el recurso solicitado.</p>
	    </div> 
	  </div> 
	</div> 
</body> 
</html>