<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../encabezadoInternoIP.jsp" flush="true"/>

<spring:message code="general.cuestionario.preliminar" var="titPreliminar"></spring:message>
<spring:message code="cuestionario.titulo"            var="moduloTipoPreg"></spring:message>
<spring:message code="cuestionario.si"          var="si"></spring:message>
<spring:message code="cuestionario.no"          var="no"></spring:message>
<spring:message code="cuestionario.seleccione"  var="seleccione"></spring:message>
<%-- encabezado de pagina--%>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-1.10.2.js" type="text/javascript">
</script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-ui.js" type="text/javascript">
</script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/jquery-ui.css" type="text/css"   rel="stylesheet"/>
<script src="${pageContext.servletContext.contextPath}/js/LibreriasSelect.js" type="text/javascript"></script>

<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }
</style>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/ComunesIC.js?version=1.2" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/CuestionarioIC.js" type="text/javascript"></script>

<div class="pageTitleContainer">
<span class="pageTitle">Finalizaci&oacute;n del Cuestionario</span></div>

<%-- Detalle --%>
<form action="" name="frmTiposPreguntas" id="frmTiposPreguntas" method="POST">

	<input type="hidden" name="RELA1" id="RELA1" value="${RELA1}"/>
	<input type="hidden" name="INT_RELA" id="INT_RELA" value="${INT_RELA}"/>
	<input type="hidden" name="INT_RELA_IC" id="INT_RELA_IC" value="${INT_RELA_IC}"/>
	<input type="hidden" name="preguntasContestadas" id="preguntasContestadas" value="${preguntasContestadas}"/>
    <input type="hidden" name="codigoOperacion" id="codigoOperacion" value="${COD_ERR}">
    <input type="hidden" name="msjOperacion" id="msjOperacion" value="${MSJ_ERR}">
    <input type="hidden" name="StrCodPaisSel" id="StrCodPaisSel" value="${StrCodPaisSel}">
    <input type="hidden" name="END_PROCESS_GC" id="END_PROCESS_GC" value="${END_PROCESS_GC}"/>
    <input type="hidden" name="idCuestHdn" id="idCuestHdn" value="${idCuestHdn}"/>
    <div class="frameTablaEstandar" id="contenedorPaginadorResultados">
            <div class="titleBuscadorSimple">
                 <span>${moduloTipoPreg}</span>
            </div>
            <div id="divPrincipalDatos" >
            <div id="DivDatos1"  class="contentTablaVariasColumnas">
                <table id="TablaDatos1">
                	<caption></caption>
                    <thead>
                        <tr id="encabezadoEsqComProd">
                           <td>(${fechaCues})</td>
                           <td>Ha ocurrido un error inesperado en el proceso de llenado de los Cuestionarios IP e IC</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
               </table>
           </div>
          </div>
    </div>
</form>
<br>
<br>

<jsp:include page="../private/myFooter.jsp" flush="true"/>
