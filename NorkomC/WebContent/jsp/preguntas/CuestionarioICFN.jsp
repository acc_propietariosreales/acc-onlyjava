<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../encabezadoInternoIP.jsp" flush="true"/>

<spring:message code="general.cuestionario.complementaria" var="titComplementaria"></spring:message>
<spring:message code="general.cuestionario.persona.fisica" var="txtPersonaFisica"></spring:message>
<spring:message code="cuestionario.titulo"            var="moduloTipoPreg"></spring:message>
<spring:message code="cuestionario.si"          var="si"></spring:message>
<spring:message code="cuestionario.no"          var="no"></spring:message>
<spring:message code="cuestionario.seleccione"  var="seleccione"></spring:message>
<%-- encabezado de pagina--%>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-1.10.2.js" type="text/javascript">
</script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-ui.js" type="text/javascript">
</script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/jquery-ui.css" type="text/css" rel="stylesheet">
</link>
<script src="${pageContext.servletContext.contextPath}/js/LibreriasSelect.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/CuestionarioICFN.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/ComunesIC.js?version=1.2" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/LibreriasSelect.js" type="text/javascript"></script>

<div class="pageTitleContainer">
<span class="pageTitle">${titComplementaria} - ${txtPersonaFisica}</span></div>

<%-- Detalle --%>
<form action="" name="frmTiposPreguntas" id="frmTiposPreguntas" method="POST">
	<input type="hidden" name="RELA1" id="RELA1" value="${RELA1}"/>
	<input type="hidden" name="INT_RELA" id="INT_RELA" value="${INT_RELA}"/>
	<input type="hidden" name="INT_RELA_IC" id="INT_RELA_IC" value="${INT_RELA_IC}"/>
	<input type="hidden" name="idCuestHdn" id="idCuestHdn" value="${idCuestHdn}"/>
	<input type="hidden" name="preguntasContestadas" id="preguntasContestadas" value="${preguntasContestadas}"/>
    <input type="hidden" name="codigoOperacion" id="codigoOperacion" value="${COD_ERR}">
    <input type="hidden" name="msjOperacion" id="msjOperacion" value="${MSJ_ERR}">
    <input type="hidden" name="claveClienteCuestionario" id="claveClienteCuestionario" value="${datosCuestionario.claveClienteCuestionario}">
    <input type="hidden" name="claveCuestionario" id="claveCuestionario" value="${datosCuestionario.claveCuestionario}">
    <input type="hidden" name="claveActividadFinanciera" id="claveActividadFinanciera" value="${datosCuestionario.entidadFinancieraActEspecifica}">
    <input type="hidden" name="clavePaisResidenciaBajoRiesgo" id="clavePaisResidenciaBajoRiesgo" value="${datosCuestionario.paisBajoRiesgo}">
    <input type="hidden" name="nivelRiesgoNkm" id="nivelRiesgoNkm" value="${datosCuestionario.nivelRiesgo}">
    <input type="hidden" name="indicadorUpldNkm" id="indicadorUpldNkm" value="${datosCuestionario.indicadorUpld}">
    <input type="hidden" name="indicadorVisitaNkm" id="indicadorVisitaNkm" value="${indicadorVisitaNkm}">
    <input type="hidden" name="tipoPersonaAcc" id="tipoPersonaAcc" value="${tipoPersonaAcc}">
    <input type="hidden" name="subtipoPersonaAcc" id="subtipoPersonaAcc" value="${subtipoPersonaAcc}">
    <input type="hidden" name="StrCodPaisSel" id="StrCodPaisSel" value="${StrCodPaisSel}">
    <input type="hidden" name="END_PROCESS_GC" id="END_PROCESS_GC" value="${END_PROCESS_GC}"/>
    <div class="frameTablaEstandar" id="contenedorPaginadorResultados">
            <div class="titleBuscadorSimple">
                 <span>${moduloTipoPreg}</span>
            </div>
            <div id="divPrincipalDatos" >
            <div id="DivDatos1"  class="contentTablaVariasColumnas">
                <table id="TablaDatos1">
                <caption></caption>
                    <thead>
                        <tr id="encabezadoEsqComProd">
                           <td>${descripcionCues} - (${fechaCues})</td>
                           <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
               </table>
           </div>
          </div>
    </div>

    <c:if test="${not empty htmlCuestionario}" >
        ${htmlCuestionario}
        <div class="frameTablaEstandar" id="contenedorPaginadorResultados">
            <div class="titleBuscadorSimple">
                <span><input id="btnGuardarCuestionarioIC" type="button" onclick="javascript: guardarCuestionarioIc();" value="Guardar IC"></span>
            </div>
        </div>
     </c:if>
</form>
<br>
<br>

<jsp:include page="../private/myFooter.jsp" flush="true"/>
