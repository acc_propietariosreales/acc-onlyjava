<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="../encabezadoInternoIP.jsp" flush="true"/>

<spring:message code="general.cuestionario.preliminar" var="titPreliminar"></spring:message>
<spring:message code="cuestionario.titulo"            var="moduloTipoPreg"></spring:message>
<spring:message code="cuestionario.si"          var="si"></spring:message>
<spring:message code="cuestionario.no"          var="no"></spring:message>
<spring:message code="cuestionario.seleccione"  var="seleccione"></spring:message>
<%---- encabezado de pagina--%>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-1.10.2.js"        type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/lf/default/js/jquery/jquery-ui.js"              type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/LibreriasSelect.js" type="text/javascript"></script>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/jquery-ui.css"    type="text/css"   rel="stylesheet"/>
<link href="${pageContext.servletContext.contextPath}/lf/default/css/cuestionarioIP.css"  type="text/css"   rel="stylesheet"/>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/muestraCuestionarioIP.js?version=1.1" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/validadorOperaciones.js?version=1.4" type="text/javascript"  ></script><div class="pageTitleContainer">
<span class="pageTitle">${titApp} - ${titPreliminar}</span></div>

<%-- Detalle --%>
<form action="" name="frmTiposPreguntas" id="frmTiposPreguntas" method="POST">
	<input type="hidden" name="codigoOperacion" id="codigoOperacion" value="${COD_ERR}"/>
	<input type="hidden" name="msjOperacion" id="msjOperacion" value="${MSJ_ERR}"/>
	<input type="hidden" name="claveClienteCuestionario" id="claveClienteCuestionario" value="${datosCuestionario.claveClienteCuestionario}"/>
	<input type="hidden" name="claveCuestionario" id="claveCuestionario" value="${datosCuestionario.claveCuestionario}"/>
	<input type="hidden" name="claveActividadFinanciera" id="claveActividadFinanciera" value="${datosCuestionario.entidadFinancieraActEspecifica}"/>
	<input type="hidden" name="clavePaisResidenciaBajoRiesgo" id="clavePaisResidenciaBajoRiesgo" value="${datosCuestionario.paisBajoRiesgo}"/>
	<input type="hidden" name="tipoCuestionarioOpics" id="tipoCuestionarioOpics" value="${datosCuestionario.codigoTipoFormulario}"/>
	<input type="hidden" name="regimenSimplificado" id="regimenSimplificado" value="${regimenSimplificado}"/>
	<input type="hidden" name="END_PROCESS_GC" id="END_PROCESS_GC" value="${END_PROCESS_GC}"/>
	<input type="hidden" name="idCuestHdn" id="idCuestHdn" value="${idCuestHdn}"/>
	<input type="hidden" name="OPER_GEN" id="OPER_GEN" value="${OPER_GEN}"/>
    <input type="hidden" name="OPER_DEP" id="OPER_DEP" value="${OPER_DEP}"/>
    <input type="hidden" name="OPER_RET" id="OPER_RET" value="${OPER_RET}"/>
    <input type="hidden" name="MONT_GEN" id="MONT_GEN" value="${MONT_GEN}"/>
    <input type="hidden" name="MONT_DEP" id="MONT_DEP" value="${MONT_DEP}"/>
    <input type="hidden" name="MONT_RET" id="MONT_RET" value="${MONT_RET}"/>
	<input type="hidden" name="tipoApp" id="tipoApp" value="${tipoApp}"/>
	<input type="hidden" name="AppsValidar" id="AppsValidar" value="${AppsValidar}"/>
	<div class="frameTablaEstandar" id="contenedorPaginadorResultados"> 
	        <div class="titleBuscadorSimple">
	             <span>${moduloTipoPreg}</span>
	        </div>
			<div id="divPrincipalDatos" >
	        <div id="DivDatos1"  class="contentTablaVariasColumnas">
	            <table id="TablaDatos1">
	            <caption></caption>
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                       <td>${descripcionCues} - (${fechaCues})</td>
	                       <td></td>
	                    </tr>
	                </thead>
	                <tbody>
	                </tbody>
	           </table>           
	       </div>
	      </div>
	</div>
	
	<c:if test="${not empty htmlCuestionario}" >
		${htmlCuestionario}
		<div class="frameTablaEstandar" id="contenedorPaginadorResultados">
			<div class="titleBuscadorSimple">
				<span><input id="btnGuardarCuestionarioIP" type="button" onclick="javascript: guardarCuestionarioIp();" value="Guardar IP"/></span>
			</div>
		</div>
	 </c:if>
</form>
<br/>
<br/>

<jsp:include page="../private/myFooter.jsp" flush="true"/>
