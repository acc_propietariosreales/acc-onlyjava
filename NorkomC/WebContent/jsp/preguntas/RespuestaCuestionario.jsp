<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../encabezadoInternoIP.jsp" flush="true"/>

<spring:message code="general.cuestionario.preliminar" var="titPreliminar"></spring:message>
<spring:message code="cuestionario.titulo"            var="moduloTipoPreg"></spring:message>
<spring:message code="cuestionario.si"          var="si"></spring:message>
<spring:message code="cuestionario.no"          var="no"></spring:message>
<spring:message code="cuestionario.seleccione"  var="seleccione"></spring:message>

<div class="pageTitleContainer">
<span class="pageTitle">Finalizaci&oacute;n del Cuestionario</span></div>
<script src="${pageContext.servletContext.contextPath}/js/cuestionarios/RespuestaCuestionario.js" type="text/javascript"></script>
<!-- Detalle -->
<form action="" name="frmTiposPreguntas" id="frmTiposPreguntas" method="POST" >
	<input type="hidden" name="codigoOperacion" id="codigoOperacion" value="${COD_ERR}">
	<input type="hidden" name="msjOperacion" id="msjOperacion" value="${MSJ_ERR}">
	<input type="hidden" name="idCuestionarioCC" id="idCuestionarioCC" value="${datosCuestionario.idCuestionario}">
	<input type="hidden" name="nivelRiesgoCC" id="nivelRiesgoCC" value="${datosCuestionario.nivelRiesgo}">
	<input type="hidden" name="indicadorUpldCC" id="indicadorUpldCC" value="${datosCuestionario.indicadorUpld}">
	<input type="hidden" name="END_PROCESS_GC" id="END_PROCESS_GC" value="${END_PROCESS_GC}"/>
	<div class="frameTablaEstandar" id="contenedorPaginadorResultados"> 
	        <div class="titleBuscadorSimple">
	             <span>${moduloTipoPreg}</span>
	        </div>
			<div id="divPrincipalDatos" >
	        <div id="DivDatos1"  class="contentTablaVariasColumnas">
	            <table id="TablaDatos1">
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                       <td>(${fechaCues})</td>
	                       <td>Ha finalizado el proceso de llenado de cuestionario IP e IC</td>
	                    </tr>
	                </thead>
	                <tbody>
	                </tbody>
	           </table>           
	       </div>
	      </div>
	</div>
</form>
<br>
<br>

<jsp:include page="../private/myFooter.jsp" flush="true"/>
