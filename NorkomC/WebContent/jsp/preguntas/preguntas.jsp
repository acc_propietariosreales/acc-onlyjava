<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../private/myHeader.jsp" flush="true"/>
<jsp:include page="../private/myMenu.jsp" flush="true">
	<jsp:param name="menuItem"    value="moduloPrincipal" />
<jsp:param name="submenuItem" value="tipoPregunta" />
</jsp:include>

<spring:message code="principal.titApp"              var="titApp"/>
<spring:message code="modulo.TitPreguntas"              var="moduloTipoPreg"/>
<spring:message code="preguntas.tit.tipo" var="tipo"/>
<spring:message code="preguntas.tit.desc" var="desc"/>
<spring:message code="preguntas.tit.codigo" var ="codigo"/>
<spring:message code="preguntas.tit.nombre" var ="nombre"/>

<!-- encabezado de pagina-->
<script src="${pageContext.servletContext.contextPath}/lf/default/js/cuestionarios/preguntas.js" type="text/javascript"></script>

<div class="pageTitleContainer"><span class="pageTitle">${titApp} - ${moduloTipoPreg}</span></div>

<!-- Detalle -->
<form action="" name="frmTiposPreguntas" id="frmTiposPreguntas" method="POST">
	<div class="frameTablaEstandar" id="contenedorPaginadorResultados"> 
	        <div class="titleBuscadorSimple">
	             <span>${moduloTipoPreg} </span>
	        </div>
			<div id="divPrincipalDatos" >
	        <div id="DivDatos1"  class="contentTablaVariasColumnas">
	            <table id="TablaDatos1">
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                        <th width="100px" class="text_centro_wrap_cntr">${tipo}</th>
	                        <th width="100px" class="text_centro">${desc}</th>
	                    </tr>
	                </thead>
	                <tbody>	
	                	<c:forEach items="${lstTipopPreguntas}" var="tipos" varStatus="rowCounter">
	                		 <tr class="odd2" id="producto${ rowCounter.count }">
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                			${tipos.tipoPregunta}
	                			</td>
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                				${tipos.descPregunta}
	                			</td>
	                		</tr>
	                	</c:forEach>
	                </tbody>
	           </table>           
	           
	           <table id="TablaDatos2">
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                        <th width="100px" class="text_centro_wrap_cntr">${codigo}</th>
	                        <th width="100px" class="text_centro">${desc}</th>	             
	                    </tr>
	                </thead>
	                <tbody>
	                	 <c:forEach items="${lstActvEcon}" var="actividades" varStatus="rowCounter">
	                		 <tr class="odd2" id="producto${ rowCounter.count }">
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                			${actividades.codigo}
	                			</td>
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                				${actividades.descripcion}
	                			</td>
	                		</tr>
	           			</c:forEach>	                	
	                </tbody>
	           </table>	  
	           
	           <table id="TablaDatos3">
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                        <th width="100px" class="text_centro_wrap_cntr">${codigo}</th>
	                        <th width="100px" class="text_centro">${nombre}</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	 <c:forEach items="${lstPais}" var="paises" varStatus="rowCounter">
	                		 <tr class="odd2" id="producto${ rowCounter.count }">
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                			${paises.codigo}
	                			</td>
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                				${paises.nombre}
	                			</td>
	                		</tr>
	           			</c:forEach>	                	
	                </tbody>
	           </table>
	           
	           <table id="TablaDatos4">
	                <thead>
	                    <tr id="encabezadoEsqComProd">         
	                        <th width="100px" class="text_centro_wrap_cntr">${codigo}</th>
	                        <th width="100px" class="text_centro">${desc}</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	 <c:forEach items="${lstSegmento}" var="segmentos" varStatus="rowCounter">
	                		 <tr class="odd2" id="producto${ rowCounter.count }">
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                			${segmentos.codigo}
	                			</td>
	                			<td width="100px" class="text_centro_wrap_cntr ">
	                				${segmentos.descripcion}
	                			</td>
	                		</tr>
	           			</c:forEach>	                	
	                </tbody>
	           </table>
	                   	  
	       </div>
	      </div>
	</div>
</form>

<jsp:include page="../private/myFooter.jsp" flush="true"/>
