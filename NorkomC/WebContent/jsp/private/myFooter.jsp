<%--
***************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* myFooter.jsp
*
* Control de versiones:
*
* Version Company             Description
* ------- ---------------- --------------------
* 1.0     Stefanini           Imprime Pie de la pagina web
***************************************************************
--%>
		</div>
		<div id="footer">
			<img border="0" src="${pageContext.servletContext.contextPath}/lf/img/menu/footer_logo_santander.gif" width="142" height="34" align="left" alt="imagen esquina izq">
		<img border="0" src="${pageContext.servletContext.contextPath}/lf/img/menu/footer_esquina_der.gif" width="10" height="34" align="right" alt="imagen esquina der">
		</div>
	</div>
	<div id='popupContainer'>
        <div id='popupClose'></div>
        <div id='popupTitle'></div>
        <div id='content'></div>
    </div>
    <div id='backgroundPopup'></div>


</body>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
</html>