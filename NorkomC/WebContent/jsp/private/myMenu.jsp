<%--
***************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* myMenu.jsp
*
* Control de versiones:
*
* Version Company             Description
* ------- ---------------- --------------------
* 1.0     Stefanini           Imprime Menu de la pagina web
***************************************************************
--%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

	<script src="../lf/default/js/global.js"           type="text/javascript"></script>
	<script src="../lf/default/js/menu/dynamicMenu.js" type="text/javascript"></script>	
	<link href="../lf/default/css/menu/estilos.css"            rel="stylesheet" type="text/css">
	<link href="../lf/default/css/menu/elementos_interfaz.css" rel="stylesheet" type="text/css">

<spring:message code="menu.principal"            var="titlePrincipal"/>
<spring:message code="menu.submenu.preguntas"    var="titlesubMenuPreguntas"/>
<spring:message code="menu.inicio"              var="Mnuinicio"/>

	<body>
	<script type="text/javascript">
		$(document).ready(function() {
			initialize('${param.menuItem}', '${param.menuSubitem}');
		});
		
	</script>

	<div id="top04">
		

			<div class="frameMenuContainer">
				<ul id="mainMenu">
					<li id="inicio"         class="startMenuGroup">
						<a href="../principal/inicio.do"><span>${Mnuinicio}</span></a>
					</li>					
									
					<li id="moduloPrincipal" class="withSubMenus startMenuGroup" >
						<a href="javascript:selectMenuItem('moduloPrincipal')"><span>${titlePrincipal}</span></a>
						<ul>									
					       	<li id="tipoPregunta">
                                  	<a href="#">
                                  		<span class="subMenuText">${titlesubMenuPreguntas}</span></a>
                                  </li>
                    	</ul>
					</li>							 	

										
				</ul>
				<div id="menuFooter">
				<div></div>
				</div>
			</div>
	</div>
<div id="content_container">