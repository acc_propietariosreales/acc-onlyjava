﻿// ** I18N

// Calendar EN (English) language
// Author: Mihai Bazon, <mihai_bazon@yahoo.com>, textos modificados por LFER
// Updater: Servilio Afre Puentes <servilios@yahoo.com>,textos modificados por LFER
// Updated: 2004-06-03, textos modificados por LFER 2013-03-25
// Encoding: utf-8
// Distributed under the same terms as the calendar itself.

// For translators: please use UTF-8 if possible.  We strongly believe that
// Unicode is the answer to a real internationalized world.  Also please
// include your contact information in the header, as can be seen above.

// full day names
//'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
Calendar._DN = new Array
("Sunday",
 "Monday",
 "Tuesday",
 "Wednesday",
 "Thursday",
 "Friday",
 "Saturday",
 "Sunday");

// Please note that the following array of short day names (and the same goes
// for short month names, _SMN) isn't absolutely necessary.  We give it here
// for exemplification on how one can customize the short day names, but if
// they are simply the first N letters of the full name you can simply say:
//
//   Calendar._SDN_len = N; // short day name length
//   Calendar._SMN_len = N; // short month name length
//
// If N = 3 then this is not needed either since we assume a value of 3 if not
// present, to be compatible with translation files that were written before
// this feature.

// short day names
//Su	Mo	Tu	We	Th	Fr	Sa
Calendar._SDN = new Array
("Su",
 "Mo",
 "Tu",
 "We",
 "Th",
 "Fr",
 "Sa",
 "Su");

// First day of the week. "0" means display Sunday first, "1" means display
// Monday first, etc.
Calendar._FD = 1;

// full month names
//'January','February','March','April','May','June','July','August','September','October','November','December'
Calendar._MN = new Array
("January",
 "February",
 "March",
 "April",
 "May",
 "June",
 "July",
 "August",
 "October",
 "Octubre",
 "November",
 "December");

// short month names
//'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',	'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
Calendar._SMN = new Array
("Jan",
 "Feb",
 "Mar",
 "Apr",
 "May",
 "Jun",
 "Jul",
 "Aug",
 "Sep",
 "Oct",
 "Nov",
 "Dec");

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "About Calendar";

Calendar._TT["ABOUT"] =
"Date Selection:\n" +
"- Use Buttons \xab, \xbb to select year\n" +
"- Use Buttons " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " to saelect Month \n" +
"- To quick selection use Mause when push Button.";
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Hours Selection:\n" +
"- Push in both directions so change values\n" +
"- or push capitals when clic to decrement\n" +
"- or clic and pull mause for quick selection .";

Calendar._TT["PREV_YEAR"] = "Previus year (push for view Menu)";
Calendar._TT["PREV_MONTH"] = "Previus Month anterior (push for view Menu)";
Calendar._TT["GO_TODAY"] = "go today";
Calendar._TT["NEXT_MONTH"] = "Next Month (push for view Menu)";
Calendar._TT["NEXT_YEAR"] = "Next Year (push for view Menu)";
Calendar._TT["SEL_DATE"] = "Select Date";
Calendar._TT["DRAG_TO_MOVE"] = "Pull to move";
Calendar._TT["PART_TODAY"] = " (today)";

// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
Calendar._TT["DAY_FIRST"] = "make %s first day of the week";

// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Close";
Calendar._TT["TODAY"] = "Today";
Calendar._TT["TIME_PART"] = "(Capitals-)Click or pull to change Value";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%d/%m/%Y";
Calendar._TT["TT_DATE_FORMAT"] = "%A, %e  %B of %Y";

Calendar._TT["WK"] = "sem";
Calendar._TT["TIME"] = "Hour:";
