function initialize(menuItem, menuSubitem) {
	try{
		if(!esVacio(menuItem)) {
			selectMenuItem(menuItem);
			if(!esVacio(menuSubitem)) {
				selectSubMenuItem(menuSubitem);
			}
		}
		ini();
	}catch(errorInfo){
		
	}
}

function estableceAyuda(lnkAyuda) {
	try{
		if(!esVacio(lnkAyuda)) {
			linkAyuda = lnkAyuda;
		}
		else{
			linkAyuda = "";
		}
	}
	catch(errorInfo){
		
	}
}


function changeSelectedTab(idTab, idDiv) {
	hideDivsTabs();
	document.getElementById(idDiv).style.display = "block";
	var ulElement = document.getElementById("tabs");
	for(var i=0; i < ulElement.getElementsByTagName("li").length; i++) {
		var liElement = ulElement.getElementsByTagName("li")[i];
		if(liElement.id == idTab) {
			liElement.className  = "active";
		} else {
			liElement.className = "";
		}
	}
}

function setTabsWidth(containerId){
	var fontWidth = 6.5;
	var borderWidth = 28;
	
	var container = document.getElementById(containerId);
	var totalTabs = container.getElementsByTagName("li");
	var containerWidth = 0;			
	var tabsWidth = 0;
	var tabsByLine = new Array();
	var subArray = new Array();
	var lineIndex = 0;
	var tabInLineIndex = 0;
	
	if(container != null){
		containerWidth = container.scrollWidth - 15;
		for(var tabIndex = 0; tabIndex < totalTabs.length; tabIndex++){
			var currentTab = totalTabs[tabIndex];
			var currentHref = totalTabs[tabIndex].getElementsByTagName("a");
			if(tabsWidth + borderWidth + currentHref[0].innerHTML.length * fontWidth <= containerWidth){
				tabsWidth += borderWidth + currentHref[0].innerHTML.length * fontWidth;
				subArray[tabInLineIndex] = currentTab;
				tabInLineIndex++;
			} else {
				tabsByLine[lineIndex] = subArray;
				tabInLineIndex = 1;
				lineIndex++;
				subArray = new Array();
				tabsWidth = borderWidth + currentHref[0].innerHTML.length * fontWidth;
				subArray[0] = currentTab;  
			}
		}
		tabsByLine[lineIndex] = subArray;
		
		for(var tabIndex = 0; tabIndex < tabsByLine.length; tabIndex++){
			var tabs = tabsByLine[tabIndex];
			var totalWidth = 0;
			for(var subtabIndex = 0; subtabIndex < tabs.length; subtabIndex++){
				var currentHref = tabs[subtabIndex].getElementsByTagName("a");
				totalWidth += borderWidth + currentHref[0].innerHTML.length * fontWidth;
			}
			
			var remainningWidth = containerWidth - totalWidth;
			var remainningByTab = remainningWidth > 10 ? Math.round((remainningWidth / tabs.length)*10)/10 : 0;
			
			for(var subtabIndex = 0; subtabIndex < tabs.length; subtabIndex++){
				var hrefInTab = tabs[subtabIndex].getElementsByTagName("a");
				tabs[subtabIndex].style.width = (borderWidth + hrefInTab[0].innerHTML.length * fontWidth + remainningByTab) + "px";
			}
		}
		
		container.style.height = (tabsByLine.length * 19) + "px";
	}
}

function createCalendar(dateField,shooter){
	Calendar.setup({
	    inputField: dateField,
	    ifFormat:   '%d/%m/%Y',
	    button:     shooter,
	    weekNumbers: false,
	    firstDay: 0,
	    electric: false
	  });
}

function esVacio(valor){
	try{
		if (valor == null){
			return true;
		}else{
			if(valor.length == 0 || valor == '' || valor == -1){
				return true;
			}else{
				return false;
			}
		}
	}catch(errorInfo){}
	return false;
}

getDimensions = function(oElement) {
    var x, y, w, h;
    x = y = w = h = 0;
    if (document.getBoxObjectFor) { // Mozilla
      var oBox = document.getBoxObjectFor(oElement);
      x = oBox.x-1;
      w = oBox.width;
      y = oBox.y-1;
      h = oBox.height;
    }
    else if (oElement.getBoundingClientRect) { // IE
      var oRect = oElement.getBoundingClientRect();
      x = oRect.left-2;
      w = oElement.clientWidth;
      y = oRect.top-2;
      h = oElement.clientHeight;
    }
    return {x: x, y: y, w: w, h: h};
  };

//<!-- FUNCION JAVASCRIPT QUE FILTRA SIEMPRE POR PRINCIPIO-->
function filtra(txt,num) {
	  var colum=num-1;// num=columna por la que se filtrar&aacute;			 
	  t = document.getElementById('tab');
	  filas = t.getElementsByTagName('tr');
	  for (var i=1; ele=filas[i]; i++) {
		texto = ele.getElementsByTagName('td')[colum].innerHTML.toUpperCase();				
		posi = (texto.indexOf(txt.toUpperCase()) == 0);
		ele.style.display = (posi) ? '' : 'none';
	  } 
}


/**
 * Realiza el submit de la forma con el id indicado, al destino indicado.
 **/
function ir_a(idForma, urlDestino) {
	var jIdForm = "#" + idForma;
	$(jIdForm).attr('action', urlDestino);
	StopTheClock();
	verNinja();
	$(jIdForm).submit();
}

/**
 * Funcion para limpiar los elementos indicados de la forma.
 * @param nombreElementos una array con el nombre de los elementos que
 * 	se desean limpiar.
 */
function limpiar(nombreElementos) {

	if (!nombreElementos) {
		return -1;
	}
	if (typeof nombreElementos === 'string') {
		nombreElementos = [nombreElementos];
	}
	for (var i = 0; i < nombreElementos.length; i++) {
		var field = document.getElementById(nombreElementos[i]);
		if ((!field) || (field.disabled)) {
			continue;
		}
		if (field.disabled) {
			continue;
		}
	    var field_type = field.type.toLowerCase();
	    switch (field_type) {
	    	case "text":
	    	case "password":
	    	case "textarea":
	    	case "hidden":
	    		field.value = "";
	    		break;
	    	case "radio":
	    	case "checkbox":
	    		if (field.checked) {
	    			field.checked = false;
	    		}
	    		break;
	    	case "select-one":
	    	case "select-multi":
	    		field.selectedIndex = 0;
	    		break;
		    default:
		        break;
	    }
	}
}

/**
 * Valida que todos los campos en la forma con la clase
 * 'CamposCompletar' tengan un valor asignado.
 * @return true si son validos los campos, false de no se asi.
 **/
function validarRequeridos() {
	var requeridos = getElementsByClassName("CamposCompletar");
	for (var i = 0; i < requeridos.length; i++) {
		if (trim(requeridos[i].value) === "") {
			return false;
		}
	}

	return true;
}
////
//Carga los elementos de pantalla.
$(function () {

	//si mueven la ventana, reacomodar lo gris
	$(window).resize(function(){
		acomodar();
	});
	//pintar los divs que vamos a usar
	var image = $("#contextPath").val() + "/img/loading.gif";
	var divfondo = "<div id='ninja' style='display:none;'><img src='" + image +"' id='ninjaImg' name='ninjaImg'/></div>";
	$("#ninja").hide();
	$("body").prepend(divfondo); //es importante que quede al principio del body
	$(".validaCampoDigital").each(function() {
		agregaMascaraDigital($(this));
	});
	$(".validaCampoEntero").each(function() {
		agregaMascaraEntero($(this));
	});
	//Funcionalidad comun de seleccion multiple.
	$(".seleccionarTodo").each(function() {
		$(this).click(function() {
			seleccionaDeseleccionaTodos($(this).attr("checked"),
					$(this).attr("id") + "_el");
		});
	});
	$(".readOnly").each(function() {
		removeUnselectedOptions(document.getElementById($(this).attr('id')));
	});
	
	bajb_backdetect.OnBack = function()
	 {
		var location = window.location;
	 };
	 
	 $('form').submit(function(){
	    $('a[href="#"]').click(function() {
	        return false;
	    });
	});
	 
});

/**
 * Valida que todos los campos en la forma con la clase 'mask-pint' se pueda hacer ctrl+v y si trae alguna letra o 
 * caracter especial los borra
 * 
 * @return true si son validos los campos, false de no se asi.
 */
function validarNumeros() {
	var requeridos = getElementsByClassName("mask-pint");
	for (var i = 0; i < requeridos.length; i++) {
		$(requeridos[i]).keyup(function() {
			this.value = this.value.replace(/[\D]/g,'');
		});		
	}
}

/**
 * Valida que todos los campos en la forma con la clase 'mask-alphaesp' se pueda hacer ctrl+v y si trae algun caracter especial lo borra
 * 
 * @return true si son validos los campos, false de no se asi.
 */
function validarLetrasEspacio() {
	var requeridos = getElementsByClassName("mask-alphaesp");
	for (var i = 0; i < requeridos.length; i++) {
		$(requeridos[i]).keyup(function() {
			this.value = this.value.replace(/[^A-Za-z�� \u00C1\u00C9\u00CD\u00D3\u00DA\u00E1\u00E9\u00ED\u00F3\u00FA\u0001\u0002\u0011\u0012\u0013\u0014]/g,'');
		});		
	}
}

/**
 * Obtiene todos los elementos con el nombre de clase indicados.
 * @param className el nombre de la clase.
 * @return todos los elementos con el nombre de clase indicados.
 **/
function getElementsByClassName(className) {
	if (!className) {
		return new Array();
	}

	var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
	var allElements = document.getElementsByTagName("*");
	var results = [];

	var element;
	for (var i = 0; (element = allElements[i]) != null; i++) {
		var elementClass = element.className;
		if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
			results.push(element);
	}

	return results;
}

/**
 * Funcion que limpia un texto de los espacios en blanco
 * @param texto Texto a limpiar
 * @return el texto sin espacios 
 */
function trim(texto) {
	return (texto || "").replace( /^\s+|\s+$/g, "" );
}	


/**
 * Indica si el string es vacio.
 * @param string la cadena a verificar.
 * @return indica si el string es vacio. 
 **/
function isStringBlank(string) {
	return(!string || $.trim(string) === ""); 
}

/**
 * Indica si la fecha1 es menor a la fecha2.
 **/
function validarFechaMayor(strFecha1, strFecha2) {
	var dateFecha1 = null;
	var dateFecha2 = null;
	if (esVacio(strFecha1) || esVacio(strFecha2)) {
		return true;
	}
	try {
		dateFecha1 = Date.parseDate(strFecha1, "%d/%m/%Y");
		dateFecha2 = Date.parseDate(strFecha2, "%d/%m/%Y");
	} catch (e) {
		return false;
	}
	return dateFecha1 <= dateFecha2;
}

function eliminarCaracteresInvalidos(objetoParaValidar){
	try {
		var valorinicial = $(objetoParaValidar).val();
		var validar = $(objetoParaValidar).attr("validar");
		opciones = validar.split(" ");
		//console.log("Todo el validar: "+validar);
		if(opciones.length>1){
			validar = opciones[0];
			//console.log("validar mide "+opciones.length);
		}
		//console.log("validar: "+validar);
		var valor = valorinicial;
		var regex = "";
		//paso 0:  para valores exactos
		//Paso 1: expresion regular, para atrapar los mas "obvios"
		switch(validar){
		case 'numero':
			regex = /[^0-9]/gi;
			break;
		case 'numerocoma':
			regex = /[^0-9]/gi;
				break;
			case 'monedaNeg':
			case 'numerodecimal':
			case 'numerodecpov': 
				regex = /[^0-9\.\-]/gi;
				break;
			case 'numerodecpovmeg': 
				regex = /[^0-9\.\-]/gi;
				break;
			case 'sololetras':
				regex = /[^A-Z�]/gi;
				break;
			case 'letrasynumeros':
				regex = /[^A-Z0-9�\.\-]/gi;
				break;
			case 'letrasespacionumeros':
				regex = /[^A-Z�0-9 ]/gi;
				break;
			case 'inpvCodigoKeypress':
				regex = /[^A-Z0-9��\.,\-\(\)\/=+:?!%&*@><;{#]/gi;
				break;
			case 'moneda':
			case 'inpvDecimalKeypress':
				regex = /[^0-9\.]/gi;
				break;
			case 'numerona':
				//regex = /[^0-9\-\.Nn\/Aa]/gi;
				regex = /[^0-9\-\.]/gi;
				break;
			case 'alfanumerico':
				regex = /[^A-Z0-9��]/gi;
				break;
			default:
				break;
		}
		if(regex!=""){
			valor = valor.replace(regex,""); //quitarle lo que no sea permitido
		}
		
		//Paso 2: validaciones mas personalizadas
		switch(validar){
			case 'alfanumerico':
			case 'inpvCodigoKeypress':
			case 'inpvMayusculasKeypress':
				valor = valor.toUpperCase();
				break;
			case 'numerona':
			case 'monedaNeg':
				valor = valor.toUpperCase(); //para N/A
				//permitir N, N/, N/A y ya.
				if(isNaN(valor)&&valor!="-"){ //puede pasar por poner .'s o -'s extras
					//si tiene N, / o A:
					//borrar todos los -'s que no esten donde deban ir
					var c1 = valor.substring(0,1);
					var resto = valor.substring(1);
					//console.log(c1,",",resto);
					valor = c1 + resto.replace(/\-/g,"");
					//console.log("tras borrar negativos: ",valor);
					//borrar todos los puntos despues del primer punto
					if(valor.indexOf(".")!=valor.lastIndexOf(".")){
						var tmp = valor.split(".");
						var valor1 = tmp[0];
						var valor2 = "." + valor.slice(valor.indexOf(".")).replace(/\./g,"");
						valor2 = (valor2==".")?"":valor2;
						valor = valor1 + valor2;
					}
				}
				break;
			case 'moneda':
			case 'inpvDecimalKeypress':
			case 'numerodecpov':
				if(isNaN(valor)){ //puede pasar por poner .'s o -'s extras
					//borrar todos los puntos despues del primer punto
					valor = valor.substring(0,valor.indexOf("."))
					+ "."
					+ valor.slice(valor.indexOf(".")).replace(/\./g,"");
				}
				break;
			case 'numerodecpovmeg':
				if(isNaN(valor)){ //puede pasar por poner .'s o -'s extras
					//borrar todos los puntos despues del primer punto
					valor = valor.substring(0,valor.indexOf("."))
					+ "."
					+ valor.slice(valor.indexOf(".")).replace(/\./g,"");
				}
				if(valor.indexOf(".")>0){
					var tmp = valor.split(".");
					var valor1 = tmp[0];
					var valor2 = tmp[1];	
					if(tmp[1].length>2){
						valor2=tmp[1].substring(0,2);
					}
					
					valor = valor1 +"."+ valor2;
				}
				break;
			default:
				break;
		}
		if(valor!=valorinicial){
			$(objetoParaValidar).val(valor); //solo si hay cambios
		}
	} catch(e){
		//console.error("Error al validar: "+e); //para debugear con firebug
	}
}

/**
 * Funcion para validacion de campos 
 */
function validaLetrasEspacio(event,obj){
	/**Valida que solo se acepten letras y espacio*/
				
		//var browserName=navigator.appName; 
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		//alert(key);
		//(key >= 97 && key <= 122) a-z 
		//(key >= 65 && key <= 90)
		//241 209 � y �
		//32 Espacio
		return ( (key >= 97 && key <= 122) 
				|| (key >= 65 && key <= 90)
				|| key==241 || key ==209 || key==32 || key==8 );
	
}

/**
 * Funcion para validacion de campos 
 */
function validaLetrasNumerosEsp(event,obj){
	/**Valida que solo se acepten letras, numeros y espacio*/
				
		//var browserName=navigator.appName; 
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		//alert(key);
		//(key >= 48 && key <= 57) numeros
		//(key >= 97 && key <= 122) a-z 
		//(key >= 65 && key <= 90)
		//241 209 � y �
		//32 Espacio
		return ( (key >= 48 && key <= 57) 
				|| (key >= 97 && key <= 122) 
				|| (key >= 65 && key <= 90)
				|| key==241 || key ==209 || key==32 || key==8 );
	
}

/**
 * Funcion para validacion letras y numeros sin espacios
 */
function validaLetrasNumeros(event,obj){
				
		//var browserName=navigator.appName; 
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		//alert(key);
		//(key >= 48 && key <= 57) numeros
		//(key >= 97 && key <= 122) a-z 
		//(key >= 65 && key <= 90)
		//241 209 y 
		//32 Espacio
		return ( (key >= 48 && key <= 57) 
				|| (key >= 97 && key <= 122) 
				|| (key >= 65 && key <= 90)
				|| key==241 || key ==209 ||  key==8 );
	
}

/**
 * Funcion para validacion de campos 
 */
function validaCorreoCarac(event,obj){
	/**Valida que solo se acepten letras, numeros, (_), (.)y (@)*/
				
		//var browserName=navigator.appName; 
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		//alert(key);
		//(key >= 48 && key <= 57) numeros
		//(key >= 97 && key <= 122) a-z 
		//(key >= 65 && key <= 90)
		//241 209 � y �
		//95,64,46  _ , @ y . 

		return ( (key >= 48 && key <= 57) 
				|| (key >= 97 && key <= 122) 
				|| (key >= 65 && key <= 90)
				|| key==241 || key ==209 
				|| key==95 ||key==64  ||key==46 ||key ==8);
	
}
/**
 * Funcion para validacion de campos 
 */
function validaNumeros(event,obj){
	/**Valida que solo se acepten Numeros en el Campo*/
				
		//var browserName=navigator.appName; 
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		
		//48-57 - 
		return ((key >= 48 && key <= 57) || key==8 || key==9 );
	
}

function borrarFilasDeTabla(nombreTabla){
	var tabla = document.getElementById(nombreTabla);

	if(tabla){
		var filas = tabla.rows;
		while(filas.length > 1)
			tabla.deleteRow(filas.length - 1);
	}
}

/**Valida que solo se acepte numero letras y punto*/
function acceptNumeroLetraPunto(evt,obj){
	//var browserName=navigator.appName; 
	var key = (event.charCode)?event.charCode:
		((event.keyCode)?event.keyCode:((event.which)?event.which:0));
	
	return (key <= 13 ||(key >= 48 && key <= 57) 
			|| (key >= 97 && key <= 122) 
			|| (key >= 65 && key <= 90)
			|| key==241 || key ==209 || key==39 || key==46  || key==8);
}

/**Valida que solo se acepte numero letras y punto*/
function cambiaMayusculas(obj){
	var  valor= document.getElementById(obj);
	valor.value=valor.value.toUpperCase();
	
}

/**
 * Valida el formato del correo
 * LFER
 * @param campo
 * @return
 */
function validaFormatoCorreo(campo){
	var strCorrecta;
	var  correo= document.getElementById(campo);
	strCorrecta=correo.value.toLowerCase();
	
	//Si el campo no es vacio entonces se realiza la validaci�n
	if(correo.value != "" && correo.value != null  && correo.value.length>0){
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var validacion=filter.test(strCorrecta);
		if (!validacion) {
			alert("Formato de correo inv\u00e1lido");
		}
	}
}

/**
 * Valida el formato del correo
 * LFER
 * @param campo
 * @return
 */
function validaFormatoCorreoNoVacio(campo){
	var strCorrecta;
	var  correo= document.getElementById(campo);
	strCorrecta=correo.value.toLowerCase();
	
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var validacion=filter.test(strCorrecta);
	if (!validacion) {
		alert("Formato de correo inv\u00e1lido");
	}
}

function getBaseURL () {
	return location.protocol + "//" + location.hostname + 
	      (location.port && ":" + location.port);
}


/**
 *  Funcion para pausar ejecucion en generacion de reporte
 */ 
function pausarCondicionado(id) {
	  for (var i = 0; i < 1e7; i++) {
	    if (document.getElementById(id)!=null){
	    	break;
	    }
	  }
}

/**
 *  Funcion para pausar ejecucion en generacion de reporte
 */ 
function pausar(milliseconds) {
	  var ini_tiempo = new Date().getTime();
	  for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - ini_tiempo) > milliseconds){
	      break;
	    }
	  }
	  disablePopup();
}

function ir_externo(url, reqParam) {

	if (url.indexOf("?") == -1) {
		url += "?";
	} else {
		url += "&";
	}
	var image = getBaseURL() + $('#contextPath').val() + "/img/loading.gif";
	var generator = window.open(url + reqParam + "&esExportaRequest=true",
			"CC" + Math.floor(Math.random() * 1001), "width=500,height=400,scrollbars=1,resizable=1");
	pausar(3000);
	
	$(generator).ready(function()
		    {
		      pausar(3500);
		    });
	
	$(generator.document.body).html('<div id="ninja" style="display:block; ' +
			'height:430px; width:500px; background-color:#111; opacity: 0.65; ' +
			'filter: alpha(opacity = 65); position:absolute; z-index: 9009; ' +
			'top:0px; left:0px; width:100%;"><img src="' + image + '" ' +
			'style="position: absolute; top: 50%; left: 50%; width: 32px; ' +
			'height: 32px; margin-top: -16px; margin-left: -16px;"/></div>');

}

/**
 * Funcion para validacion de campos 
 */
function validaNumerosTab(event,obj){
	/**Valida que solo se acepten Numeros en el Campo*/
				
		//var browserName=navigator.appName; 
		var key = (event.charCode)?event.charCode:
			((event.keyCode)?event.keyCode:((event.which)?event.which:0));
		
		//48-57 
		return (key == 9 || (key >= 48 && key <= 57));
	
}

function rellenarIzquierda(texto, longitud, caracterRelleno){
	caracterRelleno = (caracterRelleno) ? caracterRelleno : " ";
	if(texto.length < longitud){
		while(texto.length < longitud){
			texto = caracterRelleno + texto;
		}
	}
	
	if(texto.length > longitud){
		texto = texto.substring((texto.length - longitud), longitud);
	}
	
	return texto;
}

function rellenarDerecha(texto, longitud, caracterRelleno){
	caracterRelleno = (caracterRelleno) ? caracterRelleno : " ";
	if(texto.length < longitud){
		while(texto.length < longitud){
			texto = texto + caracterRelleno;
		}
	}
	
	if(texto.length > longitud){
		texto = texto.substring(0, longitud);
	}
	
	return texto;
}
//Muestra la pantalla de espera de proceso.
function verNinja() {
	//gris (ocupar todo)
	$("#ninja").show();
	$("#ninja").bgiframe();
	acomodar();
	setTimeout("document.images['ninjaImg'].src=document.images['ninjaImg'].src", 10);
}

//Oculta la pantalla de espera
function ocultarNinja() {
	$("#ninja").hide();
}
//Acomoda el espacio cubierto por la pantalla de espera.
function acomodar(){
	//gris (ocupar todo)
	$("#ninja").css("height", $(document).height()+"px");
	$("#ninja").css("width", $(document).width()+"px");
}


function InitializeTimer(url){
    StopTheClock();
    StartTheTimer(url);
}

function StopTheClock(){
    if(timerRunning){
        clearTimeout(timerID);
    	timerRunning = false;
    }
}

function StartTheTimer(url){
    if(segundosRestantes==0){
		StopTheClock();
		jAlert("Su sesi\363n ha terminado por inactividad","Sesi\363n", "", "", 
				function(r){
					window.location = url;
				}
		);
	} else {
    	segundosRestantes = segundosRestantes - 1;
        timerRunning = true;
        timerID = self.setTimeout("StartTheTimer('" + url + "')", 1000);
    }
}

function formatNumber(num,prefix){  
    num = Math.round(parseFloat(num)*Math.pow(10,2))/Math.pow(10,2);  
    prefix = prefix || '';  
    num += '';  
    var splitStr = num.split('.');  
    var splitLeft = splitStr[0];  
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';  
    splitRight = splitRight + '00';  
    splitRight = splitRight.substr(0,3);  
    var regx = /(\d+)(\d{3})/;  
    while (regx.test(splitLeft)) {  
        splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');  
    }
    return prefix + splitLeft + splitRight;
    //alert( prefix + splitLeft + splitRight);  
}
/**
 * Funcion para inhabilitar los campos y links de la pantalla cuando el contrato
 * este cancelado
 * @param cancelado estatus del contrato
 * @return nada de nada
 */
function inhabilitarCuandoCancelado(cancelado){
	if( cancelado.indexOf("CANCEL") != -1 ){
		var inhabilitar = getElementsByClassName("inhabilitarPorCancelado");
		for (var i = 0; i < inhabilitar.length; i++) {			
			$("#"+inhabilitar[i].id).attr("disabled","disabled");
			if( $("#"+inhabilitar[i].id).attr("nodeName") === 'DIV'){
				$("#"+inhabilitar[i].id + " :input").attr("disabled","disabled");
			}
		}
		var inhabilitarLinks = getElementsByClassName("inhabilitarLinks");
		for (var j = 0; j < inhabilitarLinks.length; j++) {	
			$("#"+inhabilitarLinks[j].id).unbind('click');
		}
	}	
}
/**
 * Funcion para inhabilitar los links de la pantalla cuando hay errorres al cargar la pantalla
 * @return nada de nada
 */
function inhabilitarLinksError(){	
	var inhabilitarLinks = getElementsByClassName("inhabilitarLinks");
	for (var j = 0; j < inhabilitarLinks.length; j++) {	
		$("#"+inhabilitarLinks[j].id).unbind('click');
	}
}
 
 /**
  * LFER:01/04/2013
  * Funcion para eliminar los caracteres especiales en 
  * la expresion regular
  * 
  * @param idObjeto
  * @return la cadena sin los valores especiales
  */
 function validaCaracteres(idObjeto){
		var valor=$("#"+idObjeto).val();
		regex = /[\-\(\)\/=+:?\!\�\}\{%&*@$><;#~\^\[\]\�\"\�]/gi;
		valor=valor.replace(regex,"");
		$("#"+idObjeto).val(valor);
	}
 
 /**
  * LFER:01/04/2013
  * Funcion para eliminar los caracteres especiales y letras en 
  * la expresion regular
  * 
  * @param idObjeto
  * @return la cadena sin los valores especiales
  */
 function validaCaracteresNumeros(idObjeto){
		var valor=$("#"+idObjeto).val();
		regex = /[\-\(\)\/=+:?\!\�\}\{%&*@$><;#~\^\[\]\�\"\�]/gi;
		regex2=/[^0-9]/gi;
		valor=valor.replace(regex,"");
		valor=valor.replace(regex2,"");
		$("#"+idObjeto).val(valor);
	}
 
 /**
  * LFER:01/04/2013
  * Funcion para eliminar los caracteres especiales y numeros en 
  * la expresion regular
  * 
  * @param idObjeto
  * @return la cadena sin los valores especiales
  */
 function validaCaracteresLetras(idObjeto){
		var valor=$("#"+idObjeto).val();
		regex = /[\-\(\)\/=+:?\!\�\}\{%&*@$><;#~\^\[\]\�\"\�]/gi;
		regex2=/[^A-Za-z]/gi;
		valor=valor.replace(regex,"");
		valor=valor.replace(regex2,"");
		$("#"+idObjeto).val(valor);
	}
 