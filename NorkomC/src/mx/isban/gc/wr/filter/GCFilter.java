/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * GCFilter.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour               By                  Company     Description
 * -------  -------------------     ----------------    --------    ---------
 * 1.0		12/02/2015-11:26:47 PM			Stefanini			Stefanini	Creacion
 */
package mx.isban.gc.wr.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.gc.validador.Validador;
import mx.isban.gc.validador.exceptions.ValidationTokenException;
import mx.isban.gc.validador.factory.ValidadorFactory;
import mx.isban.gc.validador.factory.ValidadorType;

/**
 * The Class GCFilter.
 */
public class GCFilter extends Architech implements Filter {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5429354859701210230L;

	/** The Constant TOKEN_PARAMETER. */
	private static final String TOKEN_PARAMETER = "tokenCC";

	/** The Constant DEFAULT_VALIDATION_TYPE. */
	private static final String DEFAULT_VALIDATION_TYPE = "REMOTO";

	/** The Constant LOGIN_PAGE. */
	private static final String LOGIN_PAGE = "login_page";

	/** The Constant ERROR_PAGE. */
	private static final String ERROR_PAGE = "error_page";

	/** The Constant TOKEN_TYPE. */
	private static final String TOKEN_TYPE = "BKS"; // default
	
	/** The validador. */
	private Validador validador;

	/** The config. */
	private FilterConfig config = null;

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() { }

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		boolean authorized = false;
		String token = null;
		HttpSession session = null;

		if (request instanceof HttpServletRequest) {
			session = ((HttpServletRequest) request).getSession(false);
			if (session != null) {
				Object obj = session.getAttribute(TOKEN_PARAMETER);
				if (obj != null) {
					token = (String) obj;
					debug("Token already validated: " + token);
					//  check if token should be validated again
					authorized = true;
				} else {
					// first time, validating token
					authorized = validarTokenPrimeraVez(request, response, session);
				}
			} else {
				// first time, validating token
				session = ((HttpServletRequest) request).getSession(true);
				authorized = validarTokenPrimeraVez(request, response, session);
			}
		} // invalid request object

		if (authorized) {
			chain.doFilter(request, response);
			return;
		} else if (config != null) {
			String login_page = config.getInitParameter(LOGIN_PAGE);
			forwadUnexpectedPage(request, response, login_page);
			return;
		}
		throw new ServletException(
				"Unauthorized access, unable to forward to login page");
	}
	
	/**
	 * Valida el token cuando es la primera peticion
	 * @param request Objeto con la informacion de la peticion
	 * @param response Objeto con la informacion de la respuesta
	 * @param session Objeto de sesion
	 * @return True si la validacion es correcta, false en caso contrario
	 * @throws ServletException En caso de un error al realizar el forward
	 * @throws IOException En caso de un error al realizar el forward
	 */
	private boolean validarTokenPrimeraVez(ServletRequest request, ServletResponse response, HttpSession session) throws ServletException, IOException {
		// first time, validating token
		String token = ((HttpServletRequest) request).getParameter(TOKEN_PARAMETER);
		
		boolean authorized = true;
		/**if (token != null) {
			// check if token value is valid
			String tokenType = config.getInitParameter(TOKEN_TYPE);
			try {
				authorized = validador.validaToken(session.getId(), tokenType, token,getArchitechBean());
			} catch (ValidationTokenException e) {
				// validation error, authorized=false
				error("Validation Exception: " + e.toString());
				error("Validation Error, redirecting to error page...");
				String error_page = config.getInitParameter(ERROR_PAGE);
				forwadUnexpectedPage(request, response, error_page);
				return authorized;
			}
			// add token to session object
			if (authorized) {
				session.setAttribute(TOKEN_PARAMETER, token);
			}
		} else {
			//authorized = false;
			// session null, authorized=false
			error("Authorized=false, redirecting to error page...");
			String error_page = config.getInitParameter(ERROR_PAGE);
			forwadUnexpectedPage(request, response, error_page);
			return authorized;
		}*/
		
		return authorized;
	}

	/**
	 * Forwad unexpected page.
	 *
	 * @param request the request
	 * @param response the response
	 * @param page the page
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void forwadUnexpectedPage(ServletRequest request,
			ServletResponse response, String page) throws ServletException,
			IOException {
		if ((page != null) & (!("".equals((page.trim()))))) {
			config.getServletContext().getRequestDispatcher(page)
					.forward(request, response);
			return;
		} else {
			throw new ServletException(
					"Unauthorized access - Invalid Session Object, unable to forward to error page");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.config = filterConfig;
		// initialization config for validation
		String validationType = filterConfig.getInitParameter("ValidationType");
		// check configuration value
		if (validationType == null) {
			error("Invalid Validation Type: " + validationType
					+ ", Using default Validation Type - REMOTO");
			validationType = DEFAULT_VALIDATION_TYPE;
		}
		// create Validator
		try {
			ValidadorType type = ValidadorType.valueOf(validationType);
			validador = ValidadorFactory.getValidador(type);
		} catch (IllegalArgumentException e) {
			// Invalid ValidationType, default REMOTO
			error("Invalid Validation Type: " + validationType
					+ ", Using default Validation Type - REMOTO");
			ValidadorType type = ValidadorType.valueOf(DEFAULT_VALIDATION_TYPE);
			validador = ValidadorFactory.getValidador(type);
		}
	}

	/**
	 * @return the validador
	 */
	public Validador getValidador() {
		return validador;
	}

	/**
	 * @param validador 
	 * el validador a agregar
	 */
	public void setValidador(Validador validador) {
		this.validador = validador;
	}

	/**
	 * @return the config
	 */
	public FilterConfig getConfig() {
		return config;
	}

	/**
	 * @param config 
	 * el config a agregar
	 */
	public void setConfig(FilterConfig config) {
		this.config = config;
	}

}
