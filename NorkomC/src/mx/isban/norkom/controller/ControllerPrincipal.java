/**
 * Isban Mexico
 *   Clase: ControllerPrincipal.java
 *   Descripcion: Componente controller para mostrar cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.controller;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.architech.Architech;

import org.springframework.ui.ModelMap;

/**
 * @author Leopoldo F Espinosa R
 * clase padre de controller se almacenaran los JSP y variables globales a utilizar
 */
public class ControllerPrincipal extends Architech {

	/**
	 * SimpleDateFormat FORMATO_FECHA_DDMMYYYY
	 */
	public static final SimpleDateFormat FORMATO_FECHA_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

	/**
	 * constante para la leyenda DESCRIPCION_CUESTIONARIO_IP
	 */
	public static final String DESCRIPCION_CUESTIONARIO_IP = "Informaci&oacute;n Preliminar";
	/**
	 * constante para la leyenda  DESCRIPCION_CUESTIONARIO_IC
	 */
	public static final String DESCRIPCION_CUESTIONARIO_IC = "Informaci&oacute;n Complementaria";
	
	/**JSP para mostrar cuestionarios**/
	public static final String JSP_CUESTIONARIOS = "MuestraCuestionario";
	/**varible apara enviar los mensajes de error***/
	protected final static String MSJ_ERROR="msjError";
	/**
	 * serialVersionUID Id de version utilizado para la serializacion del objeto
	 */
	private static final long serialVersionUID = 1L;
	
	/** Codigo de error para valores mayores al permitido */
	private static final String ORA_CODE_12899 = "ORA-12899";
	
	/** Mensaje de error para valores mayores al permitido */
	private static final String ORA_DESC_12899 = "Existen valores cuya longitud excede el valor permitido";
	
	/** Codigo de error para un mes invalido */
	private static final String ORA_CODE_01843 = "ORA-01843";
	
	/** Mensaje de error para un mes invalido */
	private static final String ORA_DESC_01843 = "Proporcion&oacute; una fecha con un mes inv&aacute;lido";
	
	/** Codigo de error para un dia invalido */
	private static final String ORA_CODE_01847 = "ORA-01847";
	
	/** Mensaje de error para un dia invalido */
	private static final String ORA_DESC_01847 = "Proporcion&oacute; una fecha con un d&iacute;a inv&aacute;lido";
	
	/** Codigo de error para llave unica repetida */
	private static final String ORA_CODE_00001 = "ORA-00001";
	
	/** Mensaje de error para llave unica repetida */
	private static final String ORA_DESC_00001 = "Ya existe informaci&oacute;n previa con el Identificador de Cuestionario";
	
	/**
	 * aregamos un parametro a modelo
	 * @param req HttpServletRequest para obtener el parametro
	 * @param parametro a buscar
	 * @param modelo a gregar el parametro
	 */
	protected void agregaParamAModelo(HttpServletRequest req, String parametro,ModelMap modelo){
		if(parametro!=null && parametro.length()>0 && existeValorReqParam(req,parametro)){
				modelo.put(parametro, obtenValorReqParam(req,parametro));
		}
	}
	/**
	 * btiene el valor del request
	 * @param req HttpServletRequest para obtener el valor de request
	 * @param parametro a buscar en request
	 * @return cadena de valor de parametro si es encontrado
	 */
	protected String obtenValorReqAttr(HttpServletRequest req, String parametro) {
		if(req.getAttribute(parametro)!=null){
			return (String)req.getAttribute(parametro);
		}else{
			return "";
		}
	}
	
	/**
	 * btiene el valor del request
	 * @param req HttpServletRequest de donde se obtendra el valor
	 * @param parametro a buscar
	 * @return cadena de valor de parametro si es encontrado
	 */
	private String obtenValorReqParam(HttpServletRequest req, String parametro) {
		if(existeValorReqParam(req, parametro)){
			return req.getParameter(parametro);
		}else{
			return "";
		}
	}
	/**
	 * metodo para validar si existe el parametro en el request
	 * @param req httpservletRequest donde se buscara el parametro
	 * @param parametro a buscar si existe
	 * @return boolean con resultado
	 */
	private boolean existeValorReqParam(HttpServletRequest req, String parametro) {
		return req.getParameter(parametro)!=null && req.getParameter(parametro).length()>0;
	}
	
	/**
	 * Genera el mensaje de error con base al mensaje de BD recibido
	 * @param mensajeError Mensaje de error en BD
	 * @return El mensaje de error mostrado en el Front de ACC
	 */
	protected String obtenerMensajeDBEX008(String mensajeError){
		if(mensajeError.contains(ORA_CODE_00001)) {
			return ORA_DESC_00001;
		} else if(mensajeError.contains(ORA_CODE_01843)) {
			return ORA_DESC_01843;
		} else if(mensajeError.contains(ORA_CODE_01847)) {
			return ORA_DESC_01847;
		} else if(mensajeError.contains(ORA_CODE_12899)) {
			return ORA_DESC_12899;
		}
		
		return null;
	}
}
