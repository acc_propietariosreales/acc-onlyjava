/**
 * Isban Mexico
 *   Clase: ValidadoresIP.java
 *   Descripcion: Componente controller para Validadores del IP.
 *
 *   Control de Cambios:
 *   1.0 05 May, 2017 Stefanini - Creacion
 *   Fecha ultima Actualizacion: 11/05/2017 by LFER
 */
package mx.isban.norkom.controller.cuestionario;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.AdministradorOrigenDestinoRecursosBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioIcBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioIpBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIcBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIpBO;
import mx.isban.norkom.cuestionario.bo.RegimenSimplificadoBO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.FamiliarDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.manejador.ManejadorRadio;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;
import mx.isban.norkom.util.UtilCuestionarios;

import org.apache.log4j.Logger;
/**
 * @author Leopoldo F Espinosa R
 * Controller para Validaciones de cuestionario IP
 * creado para evitar mas complejidad en el controller principal
 * y agregando metodos del controller princiopal por observaciones
 * de sonar en su nueva version
 */
public class CuestionarioIC extends CuestionarioIPBase {

    /**
	 * UID serial
	 * de tipo Long
	 */
	private static final long serialVersionUID = 4792095905548056541L;
	/**
	 * instancia del
	 * LOG de la aplicacion
	 * **/
	private static final Logger LOG=Logger.getLogger(CuestionarioIC.class);
	/**
     * Validacion de Riesgo para
     *  Regimen Simplificado
     *  **/
    protected transient RegimenSimplificadoBO regimenSimplificadoBO; 
    /**
     * AdministradorOrigenDestinoRecursosBO 
     * bo para obtener el origen y 
     * destino de los recursos
     */
    protected transient AdministradorOrigenDestinoRecursosBO adminOrigenDestRecursosBO;
    /**
     * CuestionarioIcBO bo para 
     * obtener los datos del cuestionario IC
     */
    protected transient CuestionarioIcBO cuestionariosIcBO;
	/** 
	 * cuestionariosIpBO bo 
	 * de cuestionarios**/
    protected transient CuestionarioIpBO cuestionariosIpBO;
    /**
     * PreguntasIcBO bo para obtener los 
     * datos del cuestionario IC
     */
    protected transient PreguntasIcBO preguntasIcBO;
    
	/**
	 * Metodo para obetener el
	 *  BO de parametros
	 * @return the parametrosBO
	 */
	public ParametrosBO getParametrosBO() {
		return parametrosBO;
	}

	/**
	 * Obtiene las respuestas de las preguntas 
	 * del Cuestionario IC a aguardar
	 * en la Db de ACC
	 * 
	 * @param req Objeto HttpServletRequest con los datos de la peticion
	 * @param cuestionarioDTO Objto CuestionarioDTO con la informacion de los datos pertenecientes a la peticion
	 * @return List<CuestionarioRespuestaDTO> Lista con las respuestas obtenidas de la peticion
	 * @throws BusinessException BusinessException En caso de un error al obtener los datos
	 */
	protected List<CuestionarioRespuestaDTO> obtenerDatosPeticionGuardarIC(HttpServletRequest req, CuestionarioDTO cuestionarioDTO)
	throws BusinessException {
		Enumeration<String> nombreParametros = req.getParameterNames();
		List<CuestionarioRespuestaDTO> respuestas = new ArrayList<CuestionarioRespuestaDTO>();
		int claveClienteCuestionario = cuestionarioDTO.getClaveClienteCuestionario();
		int claveCuestionario = cuestionarioDTO.getClaveCuestionario();
		
		List<PreguntaDTO> preguntas = cuestionariosIcBO.obtenerPreguntasCuestionarioIc(claveCuestionario, getArchitechBean());
		Map<String, PreguntaDTO> preguntasPorAbrev = new HashMap<String, PreguntaDTO>();
		for(PreguntaDTO pregunta : preguntas){
			preguntasPorAbrev.put(pregunta.getAbreviatura(), pregunta);
		}
		
		CuestionarioRespuestaDTO preguntaOrigenRecurso = 
				preguntasIcBO.obtenerPreguntaPorSeccionAbreviatura(cuestionarioDTO.getIdCuestionario(), "ODRC", "ORRC", getArchitechBean());
		
		if(preguntaOrigenRecurso != null) {
			List<PreguntaDTO> preguntasOrigenRecursos = adminOrigenDestRecursosBO.obtenerPreguntasPorRespuesta(
					cuestionarioDTO.getTipoPersona(), cuestionarioDTO.getSubtipoPersona(), preguntaOrigenRecurso.getValorOpcion(), getArchitechBean());
			for(PreguntaDTO pregunta : preguntasOrigenRecursos){
				preguntasPorAbrev.put(pregunta.getAbreviatura(), pregunta);
			}
		}
		
		agregaPreguntas(req, nombreParametros, respuestas,
				claveClienteCuestionario, claveCuestionario, preguntasPorAbrev);
		
		return respuestas;
	}

	/**
	 * Metodo creado por demasiada complejidad 
	 * del metodo principal 
	 * @param req HttpServletRequest
	 * @param nombreParametros Enumeration<String>
	 * @param respuestas List<CuestionarioRespuestaDTO>
	 * @param claveClienteCuestionario int
	 * @param claveCuestionario int 
	 * @param preguntasPorAbrev Map<String, PreguntaDTO>
	 * @throws BusinessException con menaje de error
	 */
	private void agregaPreguntas(HttpServletRequest req,
			Enumeration<String> nombreParametros,
			List<CuestionarioRespuestaDTO> respuestas,
			int claveClienteCuestionario, int claveCuestionario,
			Map<String, PreguntaDTO> preguntasPorAbrev)
			throws BusinessException {
		while(nombreParametros.hasMoreElements()){
			String nombreParametro = nombreParametros.nextElement();
			CuestionarioRespuestaDTO cuestionarioRespDTO = null;
			
			LOG.info(String.format("JUGM ::: [%s, %s]", nombreParametro, req.getParameter(nombreParametro)));
			
			PreguntaDTO pregunta = null;
			/*
			 * Son las preguntas sobre familiares PEP
			 * Estas tienen el formato IAPX donde X es un numero que se les asigna en el Front debido a que se van agregando
			 * de manera dinamica. 
			 */
			pregunta = familiaresPEP(preguntasPorAbrev, nombreParametro,
					pregunta);
			
			if(nombreParametro.startsWith(ManejadorInput.ID_INPUT_TEXT)) {
				cuestionarioRespDTO = ManejadorInput.obtenerValorCampoLibre(
						pregunta.getIdPregunta(), req.getParameter(nombreParametro), claveClienteCuestionario, claveCuestionario);
				
				UtilCuestionarios.asignarValoresPreguntaParentesco(cuestionarioRespDTO, nombreParametro);
			} else if(nombreParametro.startsWith(ManejadorRadio.ID_INPUT_RADIO)) {
				cuestionarioRespDTO = ManejadorRadio.obtenerValorRadio(
						pregunta.getIdPregunta(), req.getParameter(nombreParametro), claveClienteCuestionario, claveCuestionario);
			} else if(nombreParametro.startsWith(ManejadorSelect.ID_SELECT_UNICO)) {
				cuestionarioRespDTO = ManejadorSelect.obtenerValorSelectUnico(
						pregunta.getIdPregunta(), req.getParameter(nombreParametro), claveClienteCuestionario, claveCuestionario);
			} else if(nombreParametro.startsWith(ManejadorSelect.ID_SELECT_MULTIPLE)) {
				respuestas.addAll(ManejadorSelect.obtenerValorSelectMultiple(
						pregunta.getIdPregunta(), req.getParameterValues(nombreParametro), claveClienteCuestionario, claveCuestionario));
			}
			
			LOG.info(cuestionarioRespDTO);
			if(cuestionarioRespDTO != null){
				cuestionarioRespDTO.setClaveGrupo(Integer.parseInt(pregunta.getClaveGrupoPregunta()));
				respuestas.add(cuestionarioRespDTO);
			}
		}
	}

	/**
	 * Son las preguntas sobre familiares PEP
	 * Estas tienen el formato IAPX donde X es un numero que se les asigna en el Front debido a que se van agregando
	 * de manera dinamica. 	
	 * @param preguntasPorAbrev Map<String, PreguntaDTO> 
	 * @param nombreParametro String
	 * @param pregunta PreguntaDTO
	 * @return PreguntaDTO
	 * @throws BusinessException
	 */
	private PreguntaDTO familiaresPEP(
			Map<String, PreguntaDTO> preguntasPorAbrev, String nombreParametro,
			PreguntaDTO pregunta) throws BusinessException {
		PreguntaDTO preguntain=new PreguntaDTO();
		preguntain=pregunta;
		if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_PARENTESCO, ManejadorInput.ID_INPUT_TEXT.length()) || 
				nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_NOMBRE, ManejadorInput.ID_INPUT_TEXT.length()) || 
				nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_DOMICILIO, ManejadorInput.ID_INPUT_TEXT.length()) || 
				nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_FECHA_NAC, ManejadorInput.ID_INPUT_TEXT.length())) {
			String abreviatura = nombreParametro.substring(ManejadorInput.ID_INPUT_TEXT.length());
			preguntain = preguntasIcBO.obtenerPreguntaPorAbreviatura(String.format("'%s'", abreviatura.substring(0, 3)), getArchitechBean());
		} else {
			if(nombreParametro.length()>4){
				String abreviatura = nombreParametro.substring(nombreParametro.length() - 4);
				preguntain = preguntasPorAbrev.get(abreviatura);
			}
		}
		return preguntain;
	}
	
	/**
	 * Define el valor para el objeto CuestionarioIcBO
	 * @param cuestionariosIcBO Nuevo valor del objeto
	 */
	public void setCuestionariosIcBO(CuestionarioIcBO cuestionariosIcBO) {
		this.cuestionariosIcBO = cuestionariosIcBO;
	}

	/**
	 * Define el valor para el objeto AdministradorOrigenDestinoRecursosBO
	 * @param adminOrigenDestRecursosBO El nuevo valor del objeto
	 */
	public void setAdminOrigenDestRecursosBO(AdministradorOrigenDestinoRecursosBO adminOrigenDestRecursosBO) {
		this.adminOrigenDestRecursosBO = adminOrigenDestRecursosBO;
	}

	/**
	 * Define el valor para el objeto PreguntasIcBO
	 * @param preguntasIcBO El nuevo valor del objeto
	 */
	public void setPreguntasIcBO(PreguntasIcBO preguntasIcBO) {
		this.preguntasIcBO = preguntasIcBO;
	}

	/**
	 * Define el valor para el objeto PreguntasIpBO
	 * @param preguntasIpBO El nuevo valor del objeto
	 */
	public void setPreguntasIpBO(PreguntasIpBO preguntasIpBO) {
		this.preguntasIpBO = preguntasIpBO;
	}

	/**
	 * @param cuestionariosIpBO 
	 * el cuestionariosIpBO a agregar
	 */
	public void setCuestionariosIpBO(CuestionarioIpBO cuestionariosIpBO) {
		this.cuestionariosIpBO = cuestionariosIpBO;
	}

	/**
	 * @param regimenSimplificadoBO 
	 * el regimenSimplificadoBO a agregar
	 */
	public void setRegimenSimplificadoBO(
			RegimenSimplificadoBO regimenSimplificadoBO) {
		this.regimenSimplificadoBO = regimenSimplificadoBO;
	}

}