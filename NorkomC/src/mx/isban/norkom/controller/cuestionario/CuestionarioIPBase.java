package mx.isban.norkom.controller.cuestionario;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.controller.ControllerPrincipal;
import mx.isban.norkom.cuestionario.bo.EnvioRelacionadosBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioIcPRBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIpBO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
/**
 * @author Leopoldo F Espinosa R
 * clase base para Validaciones de cuestionario IP
 * creado para evitar mas complejidad en el controller principal
 * y agregando metodos del controller princiopal por observaciones
 * de sonar en su nueva version
 */
public class CuestionarioIPBase extends ControllerPrincipal {

	/**
	 * Serial UID
	 * de tipo Long
	 */
	private static final long serialVersionUID = 6727908927839371460L;
	/**
	 * instancia del
	 * LOG de la aplicacion
	 * **/
	private static final Logger LOG=Logger.getLogger(CuestionarioIPBase.class);
	/**constante para el 
	 * campo fechaCues****/
	protected static final String FECHA_CUES = "fechaCues";
	/**constante para el 
	 * campo datosCuestionario****/
	protected static final String DATOS_CUESTIONARIO = "datosCuestionario";
	/**
	 * identificador para el id del cuestionario
	 */
	protected static final String ID_CUEST_HDN="idCuestHdn";
	/**
	 * identificador de relacionA1 si es que aplica
	 */
	protected static final String RELA1="RELA1";
	/**
	 * Indicador para saber si aplica el flujo de relacionados
	 */
	protected static final String REL_A1_APLICA="REL_A1";
	/**
	 * constante para el envio a el jsp de IC
	 */
	protected static final String ENVIO_CUESTIONARIO_IC="CuestionarioIC%s";
	/**
	 * constante para almacenar el valor de las preguntas contestadas
	 */
	protected static final String CONS_PREG_CONTESTADAS="preguntasContestadas";
	/**
	 * identificador de INT_RELA_IC para almacenar el estado de los 
	 * relacionados
	 */
	protected static final String INT_RELA_IC="INT_RELA_IC";
	/** 
	 * Objeto de negocio 
	 * para los Parametros */
	protected ParametrosBO parametrosBO;
	/**
	 * PreguntasIpBO bo 
	 * para obtener las pregutas IP
	 */
	protected transient PreguntasIpBO preguntasIpBO;

	/**
	 *BO para obtener las preguntas de IC A1 para 
	 *propietarios reales 
	 */
	protected transient CuestionarioIcPRBO cuestionariosIcPRBO;
	/**
	 * BO para la obtencion del estatus de los
	 * Relacionados
	 */
	protected transient EnvioRelacionadosBO envioRelacionadosBO; 
	/**
	 * Metodo para agreara el BO 
	 * de  Parametros
	 * @param parametrosBO the parametrosBO to set
	 */
	public void setParametrosBO(ParametrosBO parametrosBO) {
		this.parametrosBO = parametrosBO;
	}

	/**
	 * Metodo para obtener las cadenas 
	 * de validcion 
	 * para operaciones y montos 
	 * de TF FC y opics
	 * @param modelo para agregar datos a pantalla
	 * @param idCuestionario id del cuestionario para validar el tipo a obtener
	 * @throws BusinessException con mensaje de error
	 */
	protected void obtenerValidadoresOperaciones(ModelMap modelo, String idCuestionario)
			throws BusinessException {
				if(idCuestionario!=null && idCuestionario.length()>2){
					String opc=validatipoCuest(idCuestionario,modelo);
					if("ALL".equals(opc)){
						modelo.put("OPER_GEN",obtenerParametroValor("VALIDA_OPER_GEN_ALL"));
						modelo.put("OPER_DEP",obtenerParametroValor("VALIDA_OPER_DEP_ALL"));
						modelo.put("OPER_RET",obtenerParametroValor("VALIDA_OPER_RET_ALL"));
						modelo.put("MONT_GEN",obtenerParametroValor("VALIDA_MONT_GEN_ALL"));
						modelo.put("MONT_DEP",obtenerParametroValor("VALIDA_MONT_DEP_ALL"));
						modelo.put("MONT_RET",obtenerParametroValor("VALIDA_MONT_RET_ALL"));
						
					}else if("NDE".equals(opc)){
						modelo.put("OPER_GEN",obtenerParametroValor("VALIDA_OPER_GEN_NDE"));
						modelo.put("OPER_DEP",obtenerParametroValor("VALIDA_OPER_DEP_NDE"));
						modelo.put("OPER_RET",obtenerParametroValor("VALIDA_OPER_RET_NDE"));
						modelo.put("MONT_GEN",obtenerParametroValor("VALIDA_MONT_GEN_NDE"));
						modelo.put("MONT_DEP",obtenerParametroValor("VALIDA_MONT_DEP_NDE"));
						modelo.put("MONT_RET",obtenerParametroValor("VALIDA_MONT_RET_NDE"));
			
					}else{
						LOG.info("No hay validador para este aplicativo:"+idCuestionario.substring(0,2));
					}
				}
			}

	/***
	 * Metodo para validar el cuestionario 
	 * si es viable para
	 * agregar las validaciones del cuestionario
	 * actual
	 * @param idCuestionario id del cuestionario
	 * @param modelo modelMap
	 * @return String con tipo de validacion a ejecutar
	 * @throws BusinessException con mensaje de error
	 */
	protected String validatipoCuest(String idCuestionario, ModelMap modelo)
			throws BusinessException {
				String valida="";
				String app=idCuestionario.substring(0,2);
				modelo.put("tipoApp", app);
				String all=obtenerParametroValor("APP_VAL_M_O_ALL");
				if(all.contains(",")){
					String[] apps=all.split(",");
					for (String appin : apps) {
						if(all.equals(appin)){
							valida="ALL";
						}
					}
				}else{
					if(all.equals(app)){
						valida="ALL";
					}
				}
				if(valida.length()>0){
					modelo.put("AppsValidar",all);
					return valida;
				}
				String others=obtenerParametroValor("APP_VAL_M_O_NDE");
				valida = validaOthers(valida, app, others);
				modelo.put("AppsValidar",others);
				return valida;
			}

	/**metodo para validar la variable 
	 * validaOthers
	 * @param valida String
	 * @param app String
	 * @param others String
	 * @return Stringcon la validacion
	 */ 
	private String validaOthers(String valida, String app, String others) {
		String invalida=null;
		invalida=valida;
		if(others.contains(",")){
			String[] apps=others.split(",");
			for (String appin : apps) {
				if(app.equals(appin)){
					invalida="NDE";
				}
			}
		}else{
			if(others.equals(app)){
				invalida="NDE";
			}
		}
		return invalida;
	}

	/**
	 * Metodo para invocar al BO de parametros y 
	 * traer la constante de Validacion de 
	 * la tabla de parametros
	 * @param llave  buscar
	 * @return cadena de validaciones
	 * @throws BusinessException con mensaje de error
	 */
	protected String obtenerParametroValor(String llave)
			throws BusinessException {
				LOG.debug("validacion a abuscar:" +llave);
				ParametroDTO param=parametrosBO.obtenerParametroPorNombre(llave, getArchitechBean());
				if(param!=null && param.getValor()!=null){
					return param.getValor();
				}
				return null;
			}

	/**
	 * @return the cuestionariosIcPRBO
	 */
	public CuestionarioIcPRBO getCuestionariosIcPRBO() {
		return cuestionariosIcPRBO;
	}

	/**
	 * @param cuestionariosIcPRBO the cuestionariosIcPRBO to set
	 */
	public void setCuestionariosIcPRBO(CuestionarioIcPRBO cuestionariosIcPRBO) {
		this.cuestionariosIcPRBO = cuestionariosIcPRBO;
	}

	/**
	 * @return the envioRelacionadosBO
	 */
	public EnvioRelacionadosBO getEnvioRelacionadosBO() {
		return envioRelacionadosBO;
	}

	/**
	 * @param envioRelacionadosBO the envioRelacionadosBO to set
	 */
	public void setEnvioRelacionadosBO(EnvioRelacionadosBO envioRelacionadosBO) {
		this.envioRelacionadosBO = envioRelacionadosBO;
	}

}