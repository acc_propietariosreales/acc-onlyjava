/**
 * Isban Mexico
 *   Clase: CuestionarioIpController.java
 *   Descripcion: Componente controller para cuestionario IP.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 *   Fecha ultima Actualizacion: 11/05/2017 by LFER
 *   Fecha ultima Actualizacion: 17/07/2017 by LFER
 */
package mx.isban.norkom.controller.cuestionario;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.NivelRiesgoDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.manejador.ManejadorRadio;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;
import mx.isban.norkom.cuestionario.util.ConstantesEncabezado;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.util.UtilCuestionarioIPCont;
import mx.isban.norkom.util.UtilCuestionarios;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * @author Leopoldo F Espinosa R
 * Controller principal para mostrar los cuestionarios.
 * Modificado por revision de sonar
 * 17/05/2017 by LFER 
 * modificado por el cambio de relacionados
 * 11/07/2017
 */

@Controller
public class CuestionarioIpController extends CuestionarioPrincipalController {
	/**
	 * serialVersionUID de
	 * tipo Long para 
	 * la serializacion de objetos
	 * 
	 */
	private static final long serialVersionUID = 5695908661137273565L;
	
	/**
	 * LOG de la 
	 * aplicacion para la clase
	 * CuestionarioIpController
	 * **/
	private static final Logger LOG=Logger.getLogger(CuestionarioIpController.class);
	
	/**
	 * constante de tipo 
	 * String ERROR
	 */
	private static final String ERROR = "Mensaje:";
	/***
	 * constante para la JSp 
	 * de RespuestaCuestionario
	 * 
	 * **/
	private static final String JSP_RESPUESTACUESTIONARIO = "RespuestaCuestionario";
	/***
	 * constante para la JSp 
	 * de regimenSimplificado
	 * 
	 * **/
	private static final String REGIMEN_SIMPLIFICADO = "regimenSimplificado";

	/**
	 * Recibe la peticion inicial de Cuestionario IP 
	 * y muestra el contenido del cuestionario IP
	 * si es que se tienen todos los datos para mostrarlo
	 * @param req Objeto HttpServletRequest con los datos de la peticion
	 * @param res Objeto HttpServletResponse con los datos de la respuesta
	 * @param modelo Modelo de datos de Spring
	 * @return Codigo HTML con las preguntas del Cuestionario IP 
	 */
	@RequestMapping(value="/obtenerCuestionarioIp.do")
	public String obtenerCuestionarioIp(HttpServletRequest req, HttpServletResponse res,ModelMap modelo) {
		try{
			LOG.info("Entramos a mostrarCuestionario...");
			String idCuestionario = req.getParameter(ConstantesEncabezado.ENC_ID_CUESTIONARIO);
			modelo.put(ID_CUEST_HDN, idCuestionario);
			validaDatosEntradaCuestionario(req, idCuestionario);
			String idCuestionarioRecortado = idCuestionario.substring(0, idCuestionario.length() - 14);
			ParametroDTO paramIdAplicacionBD = parametrosBO.obtenerParametroPorNombre(idCuestionarioRecortado, getArchitechBean());
			if(paramIdAplicacionBD == null || paramIdAplicacionBD.getValor() == null || paramIdAplicacionBD.getValor().isEmpty()) {
				throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, sin este dato no es posible realizar la operaci\u00F3n ");
			}
			
			CuestionarioDTO cuestionarioDTO = UtilCuestionarios.obtenerDatosPeticionConsulta(req);
			LOG.debug( String.format("CuestionarioDTO_idCuest:%s codZona:[%s],zona:[%s],requestZona:[%s]",cuestionarioDTO.getIdCuestionario(),cuestionarioDTO.getCodigoZona(),cuestionarioDTO.getNombreZona(),req.getParameter(ConstantesEncabezado.ENC_CODIGO_ZONA)));
			
			
			CuestionarioDTO cuestionarioBD = cuestionariosIpBO.complementarInformacionCuestionario(cuestionarioDTO, getArchitechBean());
			
			
			StringBuilder cuestionarioHtml = cuestionariosIpBO.obtenerCuestionarioIpHtml(cuestionarioBD, getArchitechBean());
			
			
			modelo.put(DESCRIPCION_CUES, DESCRIPCION_CUESTIONARIO_IP);
			modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
			
			modelo.put(DATOS_CUESTIONARIO, cuestionarioBD);
			
			modelo.put(HTML_CUESTIONARIO, cuestionarioHtml);
			modelo.put(TXT_END_PROCESS_GC, false);
			obtenerValidadoresOperaciones(modelo,idCuestionario);
		} catch(BusinessException be) {
			LOG.error(ERROR,be);
			LOG.debug(String.format("Error en consulta: codigo[%s] mensaje[%s] causa[%s]", be.getCode(), be.getMessage(), be.getCause()));
			modelo.put(Mensajes.COD_ERROR.getCodigo(), be.getCode());
			
			putMensajeErrprObtCues(modelo, be);
			modelo.put(TXT_END_PROCESS_GC, true);
		} finally{
			LOG.info("Terminamos operacion...");
		}
		LOG.info("Finaliza Obtener Cuestionario...");
		return JSP_CUESTIONARIOS;
	}

	/**
	 * Recibe la peticion de almacenamiento de un Cuestionario IP
	 * y valida que los relacionados esten ok
	 * en otro caso manda un mensaje de espera
	 * @param req Objeto HttpServletRequest con los datos de la peticion
	 * @param res Objeto HttpServletResponse con los datos de la respuesta
	 * @param modelo Modelo de datos de Spring
	 * @return Codigo HTML con las preguntas del Cuestionario IC
	 */
	@RequestMapping(value="/guardarCuestionarioIp.do")
	public String guardarCuestionarioIp(HttpServletRequest req, HttpServletResponse res, ModelMap modelo) {
		String complementoPaginaRetorno = StringUtils.EMPTY;
		modelo.put(INT_RELA_IC, "0");
		String idCuest=req.getParameter(ID_CUEST_HDN);
		modelo.put(ID_CUEST_HDN, idCuest);
		UtilCuestionarioIPCont util= new UtilCuestionarioIPCont();
		try {
			LOG.info("Entramos a guardarCuestionario...");
			
			int intentos= util.getValorIntentos(req,"INT_RELA");
			int claveClienteCuestionario = Integer.parseInt(req.getParameter("claveClienteCuestionario"));
			int claveCuestionario = Integer.parseInt(req.getParameter("claveCuestionario"));
			boolean rsyavalidado=false;
			rsyavalidado = rsYaValidadoReq(req, rsyavalidado);
			List<CuestionarioRespuestaDTO> respuestas = obtenerRespuestaIP(req,claveClienteCuestionario,claveCuestionario);
			
			CuestionarioDTO cuestionarioIcBD=validaRelacionadosIP(respuestas,rsyavalidado,claveCuestionario,intentos,idCuest,claveClienteCuestionario,modelo,req);
			ParametroDTO indicadoresVisitaDom = parametrosBO.obtenerParametroPorNombre("indUpldVisitaDom", getArchitechBean());
			modelo.put("indicadorVisitaNkm", StringUtils.trimToEmpty(indicadoresVisitaDom.getValor()));
			
			RequestObtenerCuestionario cuestionario= new RequestObtenerCuestionario();
			cuestionario.setIdCuestionario(cuestionarioIcBD.getIdCuestionario());
			NivelRiesgoDTO regimenSimplificado =regimenSimplificadoBO.esRegimenSimplificado(cuestionario , getArchitechBean());
			LOG.debug("Es Regimen Simplificado:-->"+regimenSimplificado.isSimplificadoAx());
			if(!rsyavalidado){
				String retorno= validaregresoRSYaValidado(modelo, claveCuestionario,cuestionarioIcBD, regimenSimplificado);
				if(retorno!=null){
					return retorno;
				}
			}else if(regimenSimplificado.isSimplificadoA1()){
				//terminamos el proceso de Cuestionario sin pedir la IC
				modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
				modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
				modelo.put(TXT_END_PROCESS_GC, true);
				
				return JSP_RESPUESTACUESTIONARIO;
			}
			agregaHtmlPorIntentos(modelo, cuestionarioIcBD.getIntentos(), cuestionarioIcBD);
			complementoPaginaRetorno = String.format("%s%s", cuestionarioIcBD.getTipoPersona(), cuestionarioIcBD.getSubtipoPersona());
			
			modelo.put("tipoPersonaAcc", cuestionarioIcBD.getTipoPersona());
			modelo.put("subtipoPersonaAcc", cuestionarioIcBD.getSubtipoPersona());
			
			validaDummyIs(cuestionarioIcBD);
			
			modelo.put(DESCRIPCION_CUES, DESCRIPCION_CUESTIONARIO_IC);
			modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
			modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);			
			modelo.put("StrCodPaisSel",cuestionarioIcBD.getNombrePaisResidencia());
			modelo.put(TXT_END_PROCESS_GC, false);
			
		} catch(BusinessException be){
			LOG.error(ERROR,be);
			util.validaMensajeErrorGuardaCuestionario(modelo, be);
			modelo.put(TXT_END_PROCESS_GC, true);
		}
		
		LOG.info("Finaliza mostrarCuestionario...");
		
		return String.format(ENVIO_CUESTIONARIO_IC, complementoPaginaRetorno);
	}

	/**
	 * metodo para validar el parametro de 
	 * REGIMEN_SIMPLIFICADO
	 * @param req HttpServletRequest
	 * @param rsyavalidado boolean
	 * @return boolean si ya se valido
	 */
	private boolean rsYaValidadoReq(HttpServletRequest req, boolean rsyavalidado) {
		boolean rsyavalidadoin=rsyavalidado;
		if(req.getParameter(REGIMEN_SIMPLIFICADO)!=null && "RS".equals(req.getParameter(REGIMEN_SIMPLIFICADO).toString()) ){
			//ya se valido el regimen simplificado
			rsyavalidadoin=true;
			req.getSession().setAttribute("RS_Simplificado","1");
		}
		return rsyavalidadoin;
	}
	/**
	 * Recibe la peticion de almacenamiento de un Cuestionario IC
	 * y valida que los relacionados esten ok
	 * en otro caso manda un mensaje de espera
	 * @param req Objeto HttpServletRequest con los datos de la peticion
	 * @param res Objeto HttpServletResponse con los datos de la respuesta
	 * @param modelo Modelo de datos de Spring
	 * @return Codigo HTML con la informacion del resultado de la operacion
	 */
	@RequestMapping(value="/guardarCuestionarioIc.do")
	public String guardarCuestionarioIc(HttpServletRequest req, HttpServletResponse res, ModelMap modelo) {
		UtilCuestionarioIPCont util= new UtilCuestionarioIPCont();
		try {
			LOG.info("Entramos a guardarCuestionario IC...");
			int claveClienteCuestionario = Integer.parseInt(req.getParameter("claveClienteCuestionario"));
			int claveCuestionario = Integer.parseInt(req.getParameter("claveCuestionario"));
			String rela= req.getParameter(RELA1);
			String idCuest=req.getParameter(ID_CUEST_HDN);
			modelo.put(ID_CUEST_HDN, idCuest);
			
			int intentos= util.getValorIntentos(req,INT_RELA_IC);
			
			if(rela!=null && rela.equals(REL_A1_APLICA) && intentos<3){
				//seguimos preguntando el estatus
				
				List<CuestionarioRespuestaDTO> respuestas = obtenerRespuestaIP(req,claveClienteCuestionario,claveCuestionario);
				boolean rsyavalidado=util.getRsValidadoDesession(req.getSession());
				intentos++;
				modelo.put(INT_RELA_IC, intentos);
				modelo.put(TXT_END_PROCESS_GC, false);
				String preg=req.getParameter(CONS_PREG_CONTESTADAS);
				validaPregParam(req, modelo, preg);
				//si faltaron relacionados ok y el estatus ya es OK enviamos a la pantalla para que vuelva aguardar
				String retorno=guardaIPCvalidaPR(req, modelo, claveClienteCuestionario,
						claveCuestionario, idCuest, intentos, respuestas,
						rsyavalidado);
				if(retorno!=null){
					return retorno;
				}
			}else{
				//flujo normal
				flujoNormalIC(req, modelo, claveClienteCuestionario,claveCuestionario);
			}
		} catch(BusinessException be){
			LOG.error(ERROR,be);
			util.validaMensajeErrorGuardaCuestionario(modelo, be);
			modelo.put(TXT_END_PROCESS_GC, true);
		} finally{
			LOG.info("Terminamos operacion...");
		}
		LOG.info("Finaliza mostrarCuestionario...");
		
		return JSP_RESPUESTACUESTIONARIO;
	}

	/**
	 * metodp ara validar el parametro de 
	 * CONS_PREG_CONTESTADAS
	 * @param req HttpServletRequest
	 * @param modelo ModelMap
	 * @param preg String
	 */
	private void validaPregParam(HttpServletRequest req, ModelMap modelo,
			String preg) {
		if(preg==null || preg.isEmpty()){
			UtilCuestionarioIPCont utilip= new UtilCuestionarioIPCont();
			modelo.put(CONS_PREG_CONTESTADAS, utilip.obtenerPreguntasDeRequest(req));
		}else{
			modelo.put(CONS_PREG_CONTESTADAS, preg);
		}
	}



	/**
	 * Metodo auxiliar para reducir la complejidad
	 * del metodo principal de guardarCuestionarioIp
	 * regresara dependiendo de las validaciones
	 * si regresa null
	 * seguira el flujo del metodo 
	 * en caso contrario regresara a la vista que
	 * corresponde
	 * @param modelo  ModelMap
	 * @param claveCuestionario int
	 * @param cuestionarioIcBD CuestionarioDTO
	 * @param regimenSimplificado NivelRiesgoDTO
	 * @throws BusinessException con mensaje de error
	 */
	private String validaregresoRSYaValidado(ModelMap modelo,
			int claveCuestionario, CuestionarioDTO cuestionarioIcBD,
			NivelRiesgoDTO regimenSimplificado) throws BusinessException {
		if(regimenSimplificado.isSimplificadoAx()){
			//regresamos a IP
			cuestionarioIcBD.setRegSimp(true);
			cuestionarioIcBD.setClaveCuestionario(claveCuestionario);
			asignaValoresRegimenSimplificado(cuestionarioIcBD,modelo);
			modelo.put(TXT_END_PROCESS_GC, false);
			return JSP_CUESTIONARIOS;
		}
		if(regimenSimplificado.isSimplificadoA1()){
			//terminamos el proceso de Cuestionario sin pedir la IC
			modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
			modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
			modelo.put(Mensajes.COD_ERROR.getCodigo(),"RS0002");
			modelo.put(Mensajes.MSJ_ERROR.getCodigo(), "El Usuario es de Regimen Simplificado Nivel A1");
			modelo.put(TXT_END_PROCESS_GC, true);
			return JSP_RESPUESTACUESTIONARIO;
		}
		return null;
	}
	
	/**
	 * metodo auxiliar para reducir la 
	 * complejidad del controller principal 
	 * en el metodo de guradar cuestionario ip
	 * @param modelo
	 * @param be
	 */
	private void putMensajeErrprObtCues(ModelMap modelo, BusinessException be) {
		if("DBEX008".equals(be.getCode())) {
			modelo.put(Mensajes.MSJ_ERROR.getCodigo(), obtenerMensajeDBEX008(be.getMessage()));
		} else {
			modelo.put(Mensajes.MSJ_ERROR.getCodigo(), be.getMessage());
		}
	}
	/**
	 * Obtiene las respuestas de las preguntas 
	 * del Cuestionario IP
	 * @param req Objeto HttpServletRequest con los datos de la peticion 
	 * @param claveClienteCuestionario Clave del Cliente-Cuestionario al que van a ser asignadas las preguntas
	 * @param claveCuestionario Clave del cuestionario al que pertenecen las preguntas
	 * @return List<CuestionarioRespuestaDTO> Lista con las respuestas obtenidas de la peticion
	 * @throws BusinessException En caso de un error al obtener los datos
	 */
	protected List<CuestionarioRespuestaDTO> obtenerDatosPeticionGuardarIp(HttpServletRequest req, int claveClienteCuestionario, int claveCuestionario)
	throws BusinessException {
		Enumeration<String> nombreParametros = req.getParameterNames();
		List<CuestionarioRespuestaDTO> respuestas = new ArrayList<CuestionarioRespuestaDTO>();
		
		List<PreguntaDTO> preguntas = preguntasIpBO.obtenerPreguntasCuestionarioIp(claveCuestionario, getArchitechBean());
		Map<String, PreguntaDTO> preguntasPorAbrev = new HashMap<String, PreguntaDTO>();
		for(PreguntaDTO pregunta : preguntas){
			preguntasPorAbrev.put(pregunta.getAbreviatura(), pregunta);
		}
		
		while(nombreParametros.hasMoreElements()){
			String nombreParametro = nombreParametros.nextElement();
			CuestionarioRespuestaDTO cuestionarioRespDTO = null;
			
			LOG.info(String.format("JUGM ::: [%s, %s]", nombreParametro, req.getParameter(nombreParametro)));
			
			String abreviatura = nombreParametro.substring(nombreParametro.length() - 4);
			PreguntaDTO pregunta = preguntasPorAbrev.get(abreviatura);
			
			if(nombreParametro.startsWith(ManejadorInput.ID_INPUT_TEXT)) {
				cuestionarioRespDTO = ManejadorInput.obtenerValorCampoLibre(
						pregunta.getIdPregunta(), req.getParameter(nombreParametro), claveClienteCuestionario, claveCuestionario);
			} else if(nombreParametro.startsWith(ManejadorRadio.ID_INPUT_RADIO)) {
				cuestionarioRespDTO = ManejadorRadio.obtenerValorRadio(
						pregunta.getIdPregunta(), req.getParameter(nombreParametro), claveClienteCuestionario, claveCuestionario);
			} else if(nombreParametro.startsWith(ManejadorSelect.ID_SELECT_UNICO)) {
				cuestionarioRespDTO = ManejadorSelect.obtenerValorSelectUnico(
						pregunta.getIdPregunta(), req.getParameter(nombreParametro), claveClienteCuestionario, claveCuestionario);
			} else if(nombreParametro.startsWith(ManejadorSelect.ID_SELECT_MULTIPLE)) {
				respuestas.addAll(ManejadorSelect.obtenerValorSelectMultiple(
						pregunta.getIdPregunta(), req.getParameterValues(nombreParametro), claveClienteCuestionario, claveCuestionario));
			}
			
			LOG.info(cuestionarioRespDTO);
			if(cuestionarioRespDTO != null){
				respuestas.add(cuestionarioRespDTO);
			}
		}
		
		return respuestas;
	}
	/**
	 * obtencion de la lista de respuestas de DB 
	 * o de Session segun sea el caso
	 * @param req HttpServletRequest para obtener los valores
	 * @param claveClienteCuestionario clave cte cuestionario
	 * @param claveCuestionario id de cuestionario
	 * @return Lista de CuestionarioRespuestaDTO
	 * @throws BusinessException con mensaje de error
	 */
	protected List<CuestionarioRespuestaDTO> obtenerRespuestaIP(HttpServletRequest req, int claveClienteCuestionario, int claveCuestionario)
			throws BusinessException {
				List<CuestionarioRespuestaDTO> listapreg= null;
				Object listases=req.getSession().getAttribute("lstPregIP");
				if(listases!=null){
					listapreg=(List<CuestionarioRespuestaDTO>) listases;
				}else{
					listapreg= obtenerDatosPeticionGuardarIp(req, claveClienteCuestionario, claveCuestionario);
				}
				return listapreg;
			}

}
