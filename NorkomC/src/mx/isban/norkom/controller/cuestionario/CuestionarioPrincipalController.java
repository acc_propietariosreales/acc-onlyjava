/**
 * Isban Mexico
 *   Clase: CuestionarioIpController.java
 *   Descripcion: Componente controller para cuestionario IP
 *   creado para evitar demasiados metodos en la clase principal.
 *
 *   Control de Cambios:
 *   1.0 Jul 11, 2017 Stefanini - Creacion
 *   Fecha ultima Actualizacion: 17/07/2017 by LFER
 */
package mx.isban.norkom.controller.cuestionario;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerRelacionados;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerRelacionados;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.util.ConstantesEncabezado;
import mx.isban.norkom.cuestionario.util.EstatusRelacionados;
import mx.isban.norkom.cuestionario.util.Mensajes;

import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;
/**
 * CuestionarioPrincipalController
 * @author lespinosa
 *
 */
public class CuestionarioPrincipalController extends CuestionarioIC {
	/**
	 * serialVersionUID
	 * de tipo long
	 * para versionado
	 */
	private static final long serialVersionUID = 6605818659687227161L;
	/**
	 * LOG de la 
	 * aplicacion
	 * **/
	private static final Logger LOG=Logger.getLogger(CuestionarioPrincipalController.class);

	/***
	 * constante para la JSp 
	 * de descripcionCues
	 * 
	 * **/
	protected static final String DESCRIPCION_CUES = "descripcionCues";
	/***
	 * constante para la JSp 
	 * de htmlCuestionario
	 * 
	 * **/
	protected static final String HTML_CUESTIONARIO = "htmlCuestionario";

	
	/** 
	 * Constante para el texto 
	 * END_PROCESS_GC */
	protected static final String TXT_END_PROCESS_GC = "END_PROCESS_GC";

	/**
	 * Metodo auxiliar para las validaciones
	 * de los datos minimos de entrada
	 * para poder obtener los datos del 
	 * cuestionario IP
	 * @param req HttpServletRequest
	 * @param idCuestionario String
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	protected void validaDatosEntradaCuestionario(HttpServletRequest req, String idCuestionario)
			throws BusinessException {
				if(req.getParameter(ConstantesEncabezado.ENC_ID_CUESTIONARIO) == null || req.getParameter(ConstantesEncabezado.ENC_ID_CUESTIONARIO).trim().isEmpty()){
					throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Identificador de Cuestionario");
				}
				if(idCuestionario.length() <= 14){
					throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, debe tener una longitud mayor a 14");
				}
			}

	/**
	 * metodo auxiliar para validar el numero de intentos
	 * y mostrar el html del cuestionario si esta
	 * en flujo de relacionados no ok
	 * @param modelo ModelMap
	 * @param intentos int intentos actuales
	 * @param cuestionarioIcBD CuestionarioDTO
	 * 
	 * @throws BusinessException cone mensaje de error
	 */
	protected void agregaHtmlPorIntentos(ModelMap modelo, int intentos,
			CuestionarioDTO cuestionarioIcBD) throws BusinessException {
				if(intentos>3 || cuestionarioIcBD.isRelacionadosOk()){
					StringBuilder cuestionarioHtml = cuestionariosIcBO.
						obtenerCuestionarioIcHtml(cuestionarioIcBD, getArchitechBean());
					modelo.put(HTML_CUESTIONARIO, cuestionarioHtml);
				}
			}

	/**
	 * validaRelacionadosIP 
	 * Metodo auxiliar del metodo de guardado IP
	 * @param cuestionarioIcBD CuestionarioDTO
	 * @param respuestas List<CuestionarioRespuestaDTO>
	 * @param rsyavalidado boolean
	 * @param claveCuestionario int clave cuest
	 * @param intentos numero de intentos
	 * @param idCuest id del cuestionario
	 * @param claveClienteCuestionario clave cliente cuestionario
	 * @param modelo modelo Map
	 * @param req HttpServletRequest
	 * @return boolean
	 * @throws BusinessException
	 */
	protected CuestionarioDTO validaRelacionadosIP( List<CuestionarioRespuestaDTO> respuestas,
			boolean rsyavalidado, int claveCuestionario, int intentos, String idCuest, int claveClienteCuestionario,
			ModelMap modelo, HttpServletRequest req) throws BusinessException {
				CuestionarioDTO cuestionarioIcBDIn=new CuestionarioDTO();
				cuestionarioIcBDIn.setRelacionadosOk(false);
				int intentosIn=intentos;
				if(!validaEstatusRelacionados(idCuest)){
					//mandamos IC de tipo A1 para que mientras llene los datos y posteriormente llenar los demas datos faltantes
					cuestionarioIcBDIn = llamadoICPR(respuestas,claveClienteCuestionario,claveCuestionario,rsyavalidado,intentos,false,req.getSession());
					modelo.put(RELA1, REL_A1_APLICA);
					req.getSession().setAttribute("lstPregIP", respuestas);
					intentosIn++;
					modelo.put("INT_RELA",intentosIn);
					cuestionarioIcBDIn.setIntentos(intentosIn);
				}else{
					//Obtengo la informacion del Cuestionario IC para despues empezar a generar sus preguntas
					if(intentosIn>0){
						cuestionarioIcBDIn = llamadoICPR(respuestas,claveClienteCuestionario,claveCuestionario,rsyavalidado,intentos,true,req.getSession());
					}else{
						cuestionarioIcBDIn = cuestionariosIcBO.
							obtenerTipoCuestionario(respuestas, claveClienteCuestionario, claveCuestionario,rsyavalidado ,getArchitechBean());
					}
					
					modelo.put(RELA1, "");
					cuestionarioIcBDIn.setRelacionadosOk(true);
					
				}

				return cuestionarioIcBDIn;
			}

	/**
	 * Metodo para validar si el objeto de 
	 * CuestionarioDTO se envuentra en session o no se encuentra
	 * y mandar llamar el BO para llenarlo y realizar 
	 * las operaciones correspondientes
	 * @param respuestas List<CuestionarioRespuestaDTO> lista de respuestas
	 * @param claveClienteCuestionario clave del cte cuestionario
	 * @param claveCuestionario clave de cuestionario
	 * @param rsyavalidado boolean con rs de indicador
	 * @param intentos numero de intentos actual
	 * @param b indica si se realiza guardado o no
	 * @param session session actual
	 * @return dto de tipo CuestionarioDTO
	 * @throws BusinessException con mensaje de error
	 */
	private CuestionarioDTO llamadoICPR(List<CuestionarioRespuestaDTO> respuestas, int claveClienteCuestionario, int claveCuestionario,
			boolean rsyavalidado, int intentos, boolean b, HttpSession session)
			throws BusinessException {
				
				CuestionarioDTO dto=null;
				Object dtoSession= session.getAttribute("cuestionarioICprboDTO");
				if(dtoSession==null || !dtoSession.getClass().isInstance(CuestionarioDTO.class)){
					dto=cuestionariosIcPRBO.
						obtenerTipoCuestionarioA1(respuestas, claveClienteCuestionario, claveCuestionario,rsyavalidado ,intentos,b,getArchitechBean());
				}else{
					if(b){
						dto=cuestionariosIcPRBO.
						obtenerTipoCuestionarioA1(respuestas, claveClienteCuestionario, claveCuestionario,rsyavalidado ,intentos,b,getArchitechBean());
					}else{
						dto=(CuestionarioDTO)dtoSession;
					}
				}
				return dto;
			}

	/**
	 * Metodo auxiliar para evitar complejidad 
	 * por demasiados if
	 * @param req HttpServletRequest
	 * @param modelo ModelMap
	 * @param claveClienteCuestionario int 
	 * @param claveCuestionario int
	 * @param idCuest id del cuestionarip
	 * @param intentos numero de intentos
	 * @param respuestas respuestas obtenidas
	 * @param rsyavalidado rs ya validado
	 * @throws BusinessException con mensaje de error
	 */
	protected String guardaIPCvalidaPR(HttpServletRequest req, ModelMap modelo, int claveClienteCuestionario,
			int claveCuestionario, String idCuest, int intentos, List<CuestionarioRespuestaDTO> respuestas, boolean rsyavalidado)
			throws BusinessException {
				if(validaEstatusRelacionados(idCuest)){
					//reenviar con preguntas faltantes si es A2 o A3
					CuestionarioDTO cuestionarioIcBD = llamadoICPR(respuestas,claveClienteCuestionario,claveCuestionario,rsyavalidado,intentos+1,true,req.getSession());
					modelo.put(RELA1,"");
					if(!"A1".equalsIgnoreCase(cuestionarioIcBD.getNivelRiesgo()) ){
						//obtenemos las respuestas respondidas y generamos el nuevo HTML con las faltantes
						
						StringBuilder cuestionarioHtml = cuestionariosIcBO.
						obtenerCuestionarioIcHtml(cuestionarioIcBD, getArchitechBean());
						modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
						modelo.put(HTML_CUESTIONARIO, cuestionarioHtml);
						String complementoPaginaRetorno = String.format("%s%s", cuestionarioIcBD.getTipoPersona(), cuestionarioIcBD.getSubtipoPersona());
						
						modelo.put("tipoPersonaAcc", cuestionarioIcBD.getTipoPersona());
						modelo.put("subtipoPersonaAcc", cuestionarioIcBD.getSubtipoPersona());
						
						validaDummyIs(cuestionarioIcBD);
						
						modelo.put(DESCRIPCION_CUES, DESCRIPCION_CUESTIONARIO_IC);
						modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
						modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
						
						modelo.put("StrCodPaisSel",cuestionarioIcBD.getNombrePaisResidencia());
						modelo.put(TXT_END_PROCESS_GC, false);
						debug("Se envia a:"+String.format(ENVIO_CUESTIONARIO_IC, complementoPaginaRetorno));
						return String.format(ENVIO_CUESTIONARIO_IC, complementoPaginaRetorno); 
					}else{
						//si es A1 sigue el flujo normal
						flujoNormalIC(req, modelo, claveClienteCuestionario,claveCuestionario);
					}
				}else{
					//reenviar para tiempo de espera
					modelo.put(RELA1,REL_A1_APLICA);
					if(intentos>=3){
						
						llamadoICPR(respuestas,claveClienteCuestionario,claveCuestionario,rsyavalidado,intentos+1,true,req.getSession());
						
						flujoNormalIC(req, modelo, claveClienteCuestionario,claveCuestionario);
						//actualizar estatus a Err
						RequestObtenerRelacionados request= new RequestObtenerRelacionados();
						request.setIdCuestionario(idCuest);
						envioRelacionadosBO.actualizaEstatusRelacionadosErr(request, getArchitechBean());
					}else{
						CuestionarioDTO cuestionarioIcBD =
						llamadoICPR(respuestas,claveClienteCuestionario,claveCuestionario,rsyavalidado,intentos,false,req.getSession());
						modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
						String complementoPaginaRetorno = String.format("%s%s", cuestionarioIcBD.getTipoPersona(), cuestionarioIcBD.getSubtipoPersona());
						return String.format(ENVIO_CUESTIONARIO_IC, complementoPaginaRetorno);
					}
				}
				return null;
			}

	/**
	 * metodo para ccalidar si es dummy y agregar un mensaje
	 * si es este dummy en DB
	 * @param cuestionarioIcBD
	 */
	protected void validaDummyIs(CuestionarioDTO cuestionarioIcBD) {
		if(cuestionarioIcBD.isDummy()) {
			cuestionarioIcBD.setNivelRiesgo(String.format("%s(DUMMY)", cuestionarioIcBD.getNivelRiesgo()));
		}
	}

	/**
	 * Metodo para el flujo normal del ACC
	 * si es que aplica o se va por el flujo de
	 * relacionados
	 * @param req  HttpServletRequest
	 * @param modelo ModelMap
	 * @param claveClienteCuestionario clave del Cliente y Cuestionario
	 * @param claveCuestionario id ce cuestionario
	 * @throws BusinessException con mensaje de error
	 */
	protected void flujoNormalIC(HttpServletRequest req, ModelMap modelo, int claveClienteCuestionario,
			int claveCuestionario) throws BusinessException {
				CuestionarioDTO clienteCuestionario = 
						cuestionariosIcBO.obtenerDatosGeneralesCuestionarioPorClaveCC(claveClienteCuestionario, getArchitechBean());
				
				CuestionarioDTO cuestionarioIcBD = cuestionariosIcBO.obtenerTipoCuestionarioPorClave(claveCuestionario, claveClienteCuestionario, getArchitechBean());
				cuestionarioIcBD.setClaveClienteCuestionario(claveClienteCuestionario);
				cuestionarioIcBD.setIdCuestionario(clienteCuestionario.getIdCuestionario());
				cuestionarioIcBD.setCodigoPais(clienteCuestionario.getCodigoPais());
				cuestionarioIcBD.setCodigoNacionalidad(clienteCuestionario.getCodigoNacionalidad());
				cuestionarioIcBD.setActividadEspecifica(clienteCuestionario.getActividadEspecifica());		
				cuestionarioIcBD.setNivelRiesgo(clienteCuestionario.getNivelRiesgo());
				cuestionarioIcBD.setIndicadorUpld(clienteCuestionario.getIndicadorUpld());
				
				List<CuestionarioRespuestaDTO> respuestas = obtenerDatosPeticionGuardarIC(req, cuestionarioIcBD);
				
				cuestionariosIcBO.asignarClaveCuestionarioIC(claveClienteCuestionario, claveCuestionario, getArchitechBean());
				cuestionarioIcBD = preguntasIcBO.guardarRespuestasCuestionarioIC(cuestionarioIcBD, respuestas, getArchitechBean());
				
				modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
				modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
				modelo.put(TXT_END_PROCESS_GC, true);
			}



	/**
	 * Metodo para saber si ya estan ok los relacionados
	 * @param claveCuestionario id del cuestionario 
	 * para buscar su estatus de relacionados
	 * @return boolean si esta ok o false si no estan ok
	 */
	private boolean validaEstatusRelacionados(String claveCuestionario) {
		RequestObtenerRelacionados req= new RequestObtenerRelacionados();
		req.setIdCuestionario(claveCuestionario);
		try {
			ResponseObtenerRelacionados respuesta= 
				envioRelacionadosBO.obtenerEstatusRelacionados(req, getArchitechBean());
			if(respuesta!=null && EstatusRelacionados.OK.toString().equals(respuesta.getEstatusRelacion())){
				return true;
			}
		} catch (BusinessException e) {
			LOG.error("Error en la obtencion de relacionados;"+e.getMessage(), e);
		}
		return false;
	}

	/***
	 * asigna los Valores  para la vista 
	 * en RegimenSimplificado de el 
	 * cuestionario IP
	 * @param cuestionarioIcBD dto con datos para obtener el cuestionario
	 * @param modelo mapa que se envia a la vista
	 * @throws BusinessException con mensaje de error
	 */
	protected void asignaValoresRegimenSimplificado(CuestionarioDTO cuestionarioIcBD, ModelMap modelo)
			throws BusinessException {
				
				StringBuilder cuestionarioHtml = cuestionariosIpBO.obtenerCuestionarioIpHtml(cuestionarioIcBD, getArchitechBean());
				
				modelo.put(DESCRIPCION_CUES, DESCRIPCION_CUESTIONARIO_IP);
				modelo.put(FECHA_CUES, FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
				
				modelo.put(DATOS_CUESTIONARIO, cuestionarioIcBD);
				modelo.put(HTML_CUESTIONARIO, cuestionarioHtml);
				LOG.debug("html: "+cuestionarioHtml.toString());
				modelo.put("regimenSimplificado","RS" );
				
			}

}