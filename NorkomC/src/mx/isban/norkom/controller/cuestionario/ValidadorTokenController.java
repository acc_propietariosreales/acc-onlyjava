/**
 * Isban Mexico
 *   Clase: ValidadorTokenController.java
 *   Descripcion: Componente para validar Token.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.controller.cuestionario;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.norkom.controller.ControllerPrincipal;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * @author Leopoldo F Espinosa R
 * Controller principal para mostrar los cuestionarios.
 */

@Controller
public class ValidadorTokenController extends ControllerPrincipal {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 5695908661137273565L;
	
	/**LOG de la aplicacion**/
	private static final Logger LOG=Logger.getLogger(ValidadorTokenController.class);
	
	/**
	 * Maneja la peticion de error de Token
	 * @param req HttpServletRequest con objetos de request
	 * @param res HttpServletResponse para validaciones de salida
	 * @param modelo Map con datos para las validaciones nesesarias
	 * @return String url de validacion de token
	 */
	@RequestMapping(value="/errorToken.do")
	public String obtenerCuestionarioIp(HttpServletRequest req, HttpServletResponse res,ModelMap modelo) {
		LOG.info("No se informo el Token, se redirige a la pagina de error");
		
		modelo.put("fechaCues", FORMATO_FECHA_DDMMYYYY.format(Calendar.getInstance().getTime()));
		
		return "../cuestionario/ErrorToken";
	}
}
