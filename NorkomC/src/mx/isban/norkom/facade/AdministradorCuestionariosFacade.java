/**
 * NorkomC DatosGeneralesFacade.java
 */
package mx.isban.norkom.facade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletContext;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bo.AdministradorComponenteCentralBO;
import mx.isban.norkom.cuestionario.bo.ConsultaCuestionarioPRBO;
import mx.isban.norkom.cuestionario.dto.RespuestasRes;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;
import mx.isban.norkom.ws.dto.CuestionariosWebServiceDTO;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author Leopoldo F Espinosa R 20/04/2015
 *
 */
public class AdministradorCuestionariosFacade {
	/** 
	 * Variable utilizada para el loger 
	 */
	private static final Logger LOGGER = Logger.getLogger(AdministradorCuestionariosFacade.class);
	/** 
	 * Variable utilizada para determinar operacion exitosa 
	 */
	private static final String OP_EXITOSA = "Operacion exitosa";

	/**contexto para leer la configuracion de Spring**/
	private final transient ServletContext servletContext;
	/**
	 * constructor
	 * @param servletContext contexto para spring
	 */
	public AdministradorCuestionariosFacade(ServletContext servletContext) {
		this.servletContext=servletContext;
	}
	
	/**
	 * Obtiene la informacion general de un cuestionario
	 * @param buc Codigo de cliente
	 * @param numeroContrato Numero de contrato
	 * @param fechaInicio Fecha inicial de creacion del contrato
	 * @param fechaFin Fecha final de creacio del contrato
	 * @return CuestionariosWebServiceDTO Informacion del Cuestionario
	 */
	public CuestionariosWebServiceDTO obtenerDatosGeneralesCuestionario( String buc, String numeroContrato, String fechaInicio,String fechaFin){
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final AdministradorComponenteCentralBO administradorComponenteCentralBO =
				(AdministradorComponenteCentralBO)springContext.getBean("BOAdministradorComponenteCentral");
		final CuestionariosWebServiceDTO cuestionarioWS = new CuestionariosWebServiceDTO();
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.US);
			Date fechaInicialFormato = null;
			Date fechaFinalFormato = null;
			
			if(fechaInicio != null && !fechaInicio.isEmpty()) {
				fechaInicialFormato = formatter.parse(fechaInicio);
			}
			
			if(fechaFin != null && !fechaFin.isEmpty()) {
				fechaFinalFormato = formatter.parse(fechaFin);
			}
			
			ArrayList<CuestionarioWSDTO> cuestionarios = (ArrayList<CuestionarioWSDTO>)administradorComponenteCentralBO.obtenerDatosGeneralesCuestionario(
														buc, numeroContrato, fechaInicialFormato, fechaFinalFormato, new ArchitechSessionBean());
			
			if(cuestionarios == null || cuestionarios.isEmpty()) {
				cuestionarioWS.setCodigoOperacion(Mensajes.WNOBCU00.getCodigo());
				cuestionarioWS.setDescOperacion(Mensajes.WNOBCU00.getDescripcion());
			} else {
				cuestionarioWS.setCuestionarios(cuestionarios);
				cuestionarioWS.setCodigoOperacion(Mensajes.EXOBCU00.getCodigo());
				cuestionarioWS.setDescOperacion(Mensajes.EXOBCU00.getDescripcion());
			}
		} catch (BusinessException e) {
			cuestionarioWS.setCodigoOperacion(Mensajes.EROBCU01.getCodigo());
			cuestionarioWS.setDescOperacion(e.getMessage());
		} catch (ParseException e) {
			cuestionarioWS.setCodigoOperacion(Mensajes.EROBCU02.getCodigo());
			cuestionarioWS.setDescOperacion(Mensajes.EROBCU02.getDescripcion());
		}
		
		return cuestionarioWS;
	}
	

	/**
	 * Metodo para consultar las respuestas de los cuestionarios
	 * @param identificadorCuestionario Numero de contrato del que se desea obtener la informacion
	 * @return RespuestasRes Informacion del cuestionario con la informacion
	 */
	public RespuestasRes obtenerPreguntasRespuestas(String identificadorCuestionario){
		
		final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ConsultaCuestionarioPRBO consultarRespuestas=
				(ConsultaCuestionarioPRBO)springContext.getBean("ConsultaRespuestasBO");
		
		RequestObtenerCuestionario request = new RequestObtenerCuestionario();
		request.setIdCuestionario(identificadorCuestionario);
		
		RespuestasRes response = new RespuestasRes();
		try {
			response = consultarRespuestas.consultaPreguntasRespuestas(request, new ArchitechSessionBean());
			if(!"WNCTCN01".equals(response.getCodOperacion()) && !"ERCTCN01".equals(response.getCodOperacion()) && !"ERCTCN02".equals(response.getCodOperacion())){
					response.setCodOperacion("EXOBCU00");
					response.setDescOperacion(OP_EXITOSA);
			}
			
		} catch (BusinessException e) {
			LOGGER.error("Mensaje",e);
			
			response.setCodOperacion("EROBCU01");
			response.setDescOperacion(e.getMessage());
		}
		return response;
	}
}
