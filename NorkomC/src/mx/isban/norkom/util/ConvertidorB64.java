/**
 * Isban Mexico
 *   Clase: ConvertidorB64.java
 *   Descripcion: Componente para convertir a base 64.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.util;

import org.apache.commons.codec.binary.Base64;



/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
public class ConvertidorB64 {

	/**
	 * Convierte un elemento de bytes a Base64 y lo regresa como una cadena
	 * @param elemento a convertir en Base 64
	 * @return string de cadena en base 64
	 */
	public String convierteB64(byte[] elemento){
		if(elemento!=null){
			return new String(Base64.encodeBase64(elemento));
		}else{
			return "";
		}
		
	}
}
