/**
 * 
 */
package mx.isban.norkom.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.ModelMap;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.util.Mensajes;

/**
 * @author lespinosa
 *
 */
public class UtilCuestionarioIPCont extends Architech{
	/**
	 * serialVersionUID de tipo
	 * long para su identificacion
	 */
	private static final long serialVersionUID = 8640615512857781019L;

	/**
	 * metodo para gregar las preguntas
	 * constestadas en caso de que sea diferente de A1
	 * @param req HttpServletRequest para obtener las preguntas
	 * @return String
	 */
	public String obtenerPreguntasDeRequest(HttpServletRequest req) {
		
		Enumeration<String> enumParam= req.getParameterNames();
		StringBuilder preguntas= new StringBuilder();
		while(enumParam.hasMoreElements()){
			String param=enumParam.nextElement();
			if(iniciaPregunta(param)){
				if(preguntas.length()>0){
					preguntas.append(":>>:");
				}
				preguntas.append(param).append("==").append(req.getParameter(param));
			}
		}
		
		return preguntas.toString();
	}
	/**
	 * Metodo para saber si el parametro
	 * inicia con el prefijo de pregunta oara ser enviado
	 * al front y 
	 * @param paramagregar las preguntas respondidas
	 * @return boolean
	 */
	private boolean iniciaPregunta(String param) {
		
		return param.startsWith("txt") || param.startsWith("select") || param.startsWith("radio") ;
	}
	
	/**
	 * metodo para obtener el rs de valiacion 
	 * de regimensimplificado
	 * @param session HttpSession
	 * @return boolean convalidacion
	 */
	public boolean getRsValidadoDesession(HttpSession session) {
		String rs=(String)session.getAttribute("RS_Simplificado");
		if(rs!=null && "".equals(rs)){
			return true;
		}
		return false;
	}
	
	/**
	 * Retornara el numero de intentos actual
	 * si no existe se regresara 0
	 * @param req HttpServletRequest
	 * @param param nombre de parametro a
	 * buscar
	 * @return intentos actual
	 */
	public int getValorIntentos(HttpServletRequest req,String param) {
		String valor=req.getParameter(param);
		if(valor!=null && !valor.isEmpty()){
			try{
				return Integer.parseInt(valor);
			}catch(NumberFormatException nfe){
				showException(nfe);
				return 0;
			}
		}else{
			return 0;
		}
		
	}
	
	/**
	 * MEtodo auxiliar para evitar 
	 * complejidad mayor a 12
	 * 
	 * @param modelo ModelMap
	 * @param be BusinessException
	 */
	public void validaMensajeErrorGuardaCuestionario(ModelMap modelo,	BusinessException be) {
		if(be.getCode()!=null && be.getCode().startsWith("JMSE00")){
			modelo.put(Mensajes.COD_ERROR.getCodigo(), Mensajes.JMSE00X.getCodigo());
			modelo.put(Mensajes.MSJ_ERROR.getCodigo(), Mensajes.JMSE00X.getDescripcion());
		}else{
			modelo.put(Mensajes.COD_ERROR.getCodigo(), be.getCode());
			modelo.put(Mensajes.MSJ_ERROR.getCodigo(), be.getMessage());
		}
	}
}
