/**
 * Isban Mexico
 *   Clase: UtilCuestionarios.java
 *   Descripcion: Componente de utileria para los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.FamiliarDTO;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.util.ConstantesEncabezado;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.UtileriasHelper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public final class UtilCuestionarios {
	/**
	 * String FORMATO_VALOR_PARENTESCO
	 */
	private static final String FORMATO_VALOR_PARENTESCO = "%s%s%s%s";
	
	/**
	 * Codigo de pais para Mexico
	 */
	private static final String COD_PAIS_MX = "052";
	/**LOG de la aplicacion**/
	private static final Logger LOG=Logger.getLogger(UtilCuestionarios.class);
	/**Constructor**/
	private UtilCuestionarios(){
		
	}
	/**
	 * @param req HttpServletRequest con datos de request
	 * @return CuestionarioDTO obejeto con datos de cuestionario
	 * @throws BusinessException con mensaje de error
	 */
	public static CuestionarioDTO obtenerDatosPeticionConsulta(HttpServletRequest req) throws BusinessException{
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		
		ClienteDTO cliente = new ClienteDTO();
		cuestionario.setCliente(cliente);
		mostrarParametrosURL(req);
		ActividadEconomicaDTO actividadGenerica = new ActividadEconomicaDTO();
		cuestionario.setActividadGenerica(actividadGenerica);
		
		ActividadEconomicaDTO actividadEspecifica = new ActividadEconomicaDTO();
		cuestionario.setActividadEspecifica(actividadEspecifica);
		
		if(validaParametro(req,ConstantesEncabezado.ENC_ID_CUESTIONARIO)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Identificador de Cuestionario");
		}else if(validaformatoIdCuest(req.getParameter(ConstantesEncabezado.ENC_ID_CUESTIONARIO))){
			throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, sin este dato no es posible realizar la operaci\u00F3n ");
		}
			
		cuestionario.setIdCuestionario(req.getParameter(ConstantesEncabezado.ENC_ID_CUESTIONARIO));
		
		obtenerDatosCliente(req, cliente);
		
		if(StringUtils.isNotBlank(req.getParameter(ConstantesEncabezado.ENC_NOMBRE_FUNC))) {
			cuestionario.setNombreFuncionario(req.getParameter(ConstantesEncabezado.ENC_NOMBRE_FUNC));
		} else {
			cuestionario.setNombreFuncionario(StringUtils.EMPTY);
		}
		
		obtenerDatosTipoCuestionario(req, cuestionario);
		
		obtenerDatosSucursal(req, cuestionario);
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_SEGMENTO)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Segmento");
		}
		cuestionario.setCodigoSegmento(req.getParameter(ConstantesEncabezado.ENC_COD_SEGMENTO));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_DESC_SEGMENTO)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Descripci\u00F3n del Segmento");
		}
		cuestionario.setDescSegmento(req.getParameter(ConstantesEncabezado.ENC_DESC_SEGMENTO));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_ACT_GENERICA)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Actividad Generica");
		}
		actividadGenerica.setCodigo(req.getParameter(ConstantesEncabezado.ENC_COD_ACT_GENERICA));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_ACT_ESPECIFICA)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Actividad Especifica");
		}
		actividadEspecifica.setCodigo(req.getParameter(ConstantesEncabezado.ENC_COD_ACT_ESPECIFICA));
		
		obtenerProductoSubproducto(req, cuestionario);
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_PAIS)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Pa\u00EDs");
		}
		cuestionario.setCodigoPais(req.getParameter(ConstantesEncabezado.ENC_COD_PAIS));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_ENTIDAD_DOM) && COD_PAIS_MX.equals(cuestionario.getCodigoPais())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Entidad");
		}
		cuestionario.setCodigoEntidad(req.getParameter(ConstantesEncabezado.ENC_COD_ENTIDAD_DOM));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_MUNICIPIO)  && COD_PAIS_MX.equals(cuestionario.getCodigoPais())){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Municipio");
		}
		cuestionario.setCodigoMunicipio(req.getParameter(ConstantesEncabezado.ENC_COD_MUNICIPIO));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_NACIONALIDAD)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Nacionalidad");
		}
		cuestionario.setCodigoNacionalidad(req.getParameter(ConstantesEncabezado.ENC_COD_NACIONALIDAD));
		
		obtenerSeccionOpics(req, cuestionario);
		
		return cuestionario;
	}
	/**
	 * parametro para mostrar los parametros de la url que se envian el cuestionario, este solo se mostrara 
	 * cuando el log este en modo de debug
	 * @param req HttpServletRequest de donde obtener los parametros
	 */
	private static void mostrarParametrosURL(HttpServletRequest req) {
		if(LOG.isDebugEnabled()){
			LOG.debug("IP_peticion:"+req.getLocalName());
			LOG.debug("RequestURL:["+req.getRequestURL()+"]");
			Enumeration<String> params = req.getParameterNames(); 
			while(params.hasMoreElements()){
			 String paramName = (String)params.nextElement();
			 LOG.debug(String.format("Nombre:[%s], Valor:[%s]",paramName,req.getParameter(paramName)));
			}
		}
	}
	/**
	 * metodo para validar el formato del identificador que debe contener:<br>
	 * Formato XX000000YYYYMMDD
	 *  las dos primeras letras, deben ser OP,TF,FC<br>
	 *  los siguientes 6 son numericos<br>
	 *  y los ultimos 8 son en formato fecha YYYYMMDD<br>
	 * 
	 * @param id id del cuestionario a validar
	 * @return true si es invalido el formato
	 * @throws BusinessException con mensaje de error
	 */
	private static boolean validaformatoIdCuest(String idCuestionario) throws BusinessException {
		if(idCuestionario != null){
			String id = idCuestionario.substring(idCuestionario.length() - 14);
			//validamos que contenga OP,TF o FC
				boolean segunda=false;
				//validamos que sean numeros
				try{
				Integer.parseInt(id.substring(0, 6));
				segunda=true;
				}catch(NumberFormatException nfe){
					segunda=false;
				}
				if(segunda && id.length()>=6){
					try{
						//validamos que sean numeros el formato de fecha
						Integer.parseInt(id.substring(6));
						segunda=true;
					}catch(NumberFormatException nfe){
						segunda=false;
						throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, El formato de la fecha es Incorrecto. ");
					}
					if(segunda){
						if(UtileriasHelper.isValidDate(id.substring(6))){
							if(!(UtileriasHelper.diasDiferenciaAldia(id.substring(6))==0)){
								throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, La fecha no es valida. ");
							}else {
								return false;
							}
						}else{

							LOG.info("FECHA:"+id.substring(6));

							throw new BusinessException(Mensajes.ERCCIP13.getCodigo(),"Es necesario proporcionar un Identificador de Cuestionario Valido, La fecha no es valida.");
							//return true;
						}
					}else {
						return true;
					}
						
				}else {
					return true;
				}
		}
		return true;
	}
	
	/**
	 * Asigna el valor para las preguntas de Parentesco
	 * @param cuestionarioRespDTO Pregunta a la que se le asigna el valor
	 * @param nombreParametro Nombre del parametro del que se va a obtener la informacion
	 */
	public static void asignarValoresPreguntaParentesco(CuestionarioRespuestaDTO cuestionarioRespDTO, String nombreParametro){
		if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_PARENTESCO, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_PARENTESCO, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		} else if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_NOMBRE, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_NOMBRE, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		} else if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_DOMICILIO, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_DOMICILIO, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		} else if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_FECHA_NAC, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_FECHA_NAC, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		}
	}
	
	/**
	 * Obtiene los datos correspondientes al cliente
	 * @param req Request en donde se encuentra la informacion
	 * @param cliente Cliente al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerDatosCliente(HttpServletRequest req, ClienteDTO cliente) throws BusinessException {
		if(validaParametro(req,ConstantesEncabezado.ENC_BUC) ){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el BUC");
		}
		cliente.setCodigoCliente(req.getParameter(ConstantesEncabezado.ENC_BUC));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_IND_CLIENTE_NVO)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Indicador de Cliente Nuevo");
		}
		cliente.setIndClienteNuevo(req.getParameter(ConstantesEncabezado.ENC_IND_CLIENTE_NVO));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_NOMBRE_CLI) ){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Nombre del Cliente");
		}
		cliente.setNombreCliente(req.getParameter(ConstantesEncabezado.ENC_NOMBRE_CLI));
		
		if(!validaParametro(req,ConstantesEncabezado.ENC_TIPO_PERS) && "F".equalsIgnoreCase(req.getParameter(ConstantesEncabezado.ENC_TIPO_PERS))){
			if(validaParametro(req,ConstantesEncabezado.ENC_AP_PATERNO)){
				throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Apellido Paterno del Cliente");
			}
			cliente.setApellidoPaterno(req.getParameter(ConstantesEncabezado.ENC_AP_PATERNO));
		}
		
		String apellidoMaterno = req.getParameter(ConstantesEncabezado.ENC_AP_MATERNO);
		if(StringUtils.isNotBlank(apellidoMaterno) && "F".equalsIgnoreCase(req.getParameter(ConstantesEncabezado.ENC_TIPO_PERS))) {
			cliente.setApellidoMaterno(apellidoMaterno);
		} else {
			cliente.setApellidoMaterno(StringUtils.EMPTY);
		}
		
		
		if(validaParametro(req,ConstantesEncabezado.ENC_FECHA_NAC)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Fecha de Nacimiento / Fecha de Constituci&oacute;n del Cliente");
		}
		cliente.setFechaNacimiento(req.getParameter(ConstantesEncabezado.ENC_FECHA_NAC));
	}
	
	/**
	 * Obtiene los datos con los cuales se define el tipo de cuestionario que se va a mostrar
	 * @param req Request en donde se encuentra la informacion
	 * @param cuestionario Cuestionario al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerDatosTipoCuestionario(HttpServletRequest req, CuestionarioDTO cuestionario) throws BusinessException {
		if(validaParametro(req,ConstantesEncabezado.ENC_TIPO_PERS)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Tipo de Persona del Cliente");
		}
		if("F".equalsIgnoreCase(req.getParameter(ConstantesEncabezado.ENC_TIPO_PERS)) || "J".equalsIgnoreCase(req.getParameter(ConstantesEncabezado.ENC_TIPO_PERS))){
			cuestionario.setTipoPersona(req.getParameter(ConstantesEncabezado.ENC_TIPO_PERS));
		}else{
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"El Tipo de Persona es incorrecto");
		}
		
		
		if(validaParametro(req,ConstantesEncabezado.ENC_SUBTIPO_PERS) ){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Subtipo de Persona del Cliente");
		}
		cuestionario.setSubtipoPersona(req.getParameter(ConstantesEncabezado.ENC_SUBTIPO_PERS));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_DIVISA_PROD)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Divisa del Producto");
		}
		cuestionario.setDivisa(req.getParameter(ConstantesEncabezado.ENC_DIVISA_PROD));
	}
	
	/**
	 * Obtiene los datos correspondientes a una sucursal
	 * @param req Request en donde se encuentra la informacion
	 * @param cuestionario Cuestionario al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerDatosSucursal(HttpServletRequest req, CuestionarioDTO cuestionario) throws BusinessException {
		if(validaParametro(req,ConstantesEncabezado.ENC_CODIGO_SUC)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de Sucursal");
		}
		cuestionario.setCodigoSucursal(req.getParameter(ConstantesEncabezado.ENC_CODIGO_SUC));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_NOMBRE_SUC)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el Nombre de la Sucursal");
		}
		cuestionario.setNombreSucursal(req.getParameter(ConstantesEncabezado.ENC_NOMBRE_SUC));
		
		String codigoPlaza = req.getParameter(ConstantesEncabezado.ENC_CODIGO_PLAZA);
		if(StringUtils.isNotBlank(codigoPlaza)){
			cuestionario.setCodigoPlaza(codigoPlaza);
		} else {
			cuestionario.setCodigoPlaza(StringUtils.EMPTY);
		}
		
		String nombrePlaza = req.getParameter(ConstantesEncabezado.ENC_NOMBRE_PLAZA);
		if(StringUtils.isNotBlank(nombrePlaza)){
			cuestionario.setNombrePlaza(nombrePlaza);
		} else {
			cuestionario.setNombrePlaza(StringUtils.EMPTY);
		}
		
		String codigoZona = req.getParameter(ConstantesEncabezado.ENC_CODIGO_ZONA);
		if(StringUtils.isNotBlank(codigoZona)){
			cuestionario.setCodigoZona(codigoZona);
		} else {
			cuestionario.setCodigoZona(StringUtils.EMPTY);
		}
		
		String nombreZona = req.getParameter(ConstantesEncabezado.ENC_NOMBRE_ZONA);
		if(StringUtils.isNotBlank(nombreZona)){
			cuestionario.setNombreZona(nombreZona);
		} else {
			cuestionario.setNombreZona(StringUtils.EMPTY);
		}
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_REGION)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo de la Regi\u00F3n");
		}
		cuestionario.setCodigoRegion(req.getParameter(ConstantesEncabezado.ENC_COD_REGION));
		
		String nombreRegion = req.getParameter(ConstantesEncabezado.ENC_NOM_REGION);
		if(StringUtils.isNotBlank(nombreRegion)){
			cuestionario.setNombreRegion(nombreRegion);
		} else {
			cuestionario.setNombreRegion(StringUtils.EMPTY);
		}
	}
	
	/**
	 * Obtiene los datos correspondiente al producto y subproducto
	 * @param req Request en donde se encuentra la informacion
	 * @param cuestionario Cuestionario al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerProductoSubproducto(HttpServletRequest req, CuestionarioDTO cuestionario) throws BusinessException {
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_PROD)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Producto");
		}
		cuestionario.setCodigoProducto(req.getParameter(ConstantesEncabezado.ENC_COD_PROD));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_DESC_PROD)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta la Descripci\u00F3n del Producto");
		}
		cuestionario.setDescProducto(req.getParameter(ConstantesEncabezado.ENC_DESC_PROD));
		
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_SUB_PROD)){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Subproducto");
		}
		cuestionario.setCodigoSubproducto(req.getParameter(ConstantesEncabezado.ENC_COD_SUB_PROD));
	}
	
	/**
	 * Obtiene los datos cuando se trata de Opics
	 * @param req Request en donde se encuentra la informacion
	 * @param cuestionario Cuestionario al que se agrega la informacion
	 * @throws BusinessException En caso de que falte algun dato
	 */
	private static void obtenerSeccionOpics(HttpServletRequest req, CuestionarioDTO cuestionario) throws BusinessException {
		//Es obligatorio para Opics y FC
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_BRANCH) && (cuestionario.getIdCuestionario().startsWith("OP") || cuestionario.getIdCuestionario().startsWith("FC"))){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Branch");
		}
		cuestionario.setCodigoBranch(req.getParameter(ConstantesEncabezado.ENC_COD_BRANCH));
		
		//Es obligatorio para Opics y FC
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_CENTRO_COSTO) && (cuestionario.getIdCuestionario().startsWith("OP") || cuestionario.getIdCuestionario().startsWith("FC"))){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Centro de Costos");
		}
		cuestionario.setCodigoCentroCostos(req.getParameter(ConstantesEncabezado.ENC_COD_CENTRO_COSTO));
		
		//Es obligatorio para Opics y FC
		if(validaParametro(req,ConstantesEncabezado.ENC_COD_TIPO_FORMULARIO)  && (cuestionario.getIdCuestionario().startsWith("OP") || cuestionario.getIdCuestionario().startsWith("FC"))){
			throw new BusinessException(Mensajes.ERROR_DATOS_INICIO.getCodigo(),"Falta el C\u00F3digo del Tipo de Formulario");
		}
		cuestionario.setCodigoTipoFormulario(req.getParameter(ConstantesEncabezado.ENC_COD_TIPO_FORMULARIO));
	}
	/**
	 * Validacion de parametros de request
	 * @param req httpserletRequest donde buscar los parametros
	 * @param parametro parametro a buscar
	 * @return false si el parametro existe y no es vacio
	 */
	private static boolean validaParametro(HttpServletRequest req,String parametro) {
		if(req.getParameter(parametro)!=null && req.getParameter(parametro).trim().length()>0){
			return false;
		}
		return true;
	}
}
