/**
 * Isban Mexico
 *   Clase: AdministradorNivelRiesgoWS.java
 *   Descripcion: Componente WS para regresar el nivel de riesgo obtenido.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.norkom.facade.NivelRiesgoFacade;
import mx.isban.norkom.ws.dto.RespuestaWebService;

import org.apache.log4j.Logger;

@WebService(targetNamespace="http://ws.ccc.norkom.isban.mx/", serviceName="AdministradorNivelRiesgoServices", portName="AdministradorNivelRiesgoServicesPort")
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class AdministradorNivelRiesgoWS {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOG = Logger.getLogger(AdministradorNivelRiesgoWS.class);
	/**
	 * WebServiceContext Contexto de Spring
	 */
	@Resource
	private transient WebServiceContext context;
	/**
	 * Realiza la actualizacion del Nivel de Riesgo y del Indicador UPLD en personas
	 * @param buc Cliente al que se le va a actualizar la informacion
	 * @param nivelRiesgo Nivel de Riesgo que se va a asignar
	 * @param indicadorUpld Indicador UPLD que se va a asignar
	 * @param claveEmpleado Clave del empleado que esta realizando el cambio
	 * @param fechaActualizacion Fecha en que se realizo el cambio
	 * @param idNorkom Identificador de Norkom
	 * @return Codigo y mensaje de la operacion
	 */
	@WebMethod
	public RespuestaWebService actualizarNivelRiesgoManual(@WebParam(name="BUC") String buc, @WebParam(name="NivelRiesgo") String nivelRiesgo, 
			@WebParam(name="IndUPLD") String indicadorUpld, @WebParam(name="ClaveEmpleado") String claveEmpleado,
			@WebParam(name="FechaAct") String fechaActualizacion, @WebParam(name="IdNorkom") String idNorkom)
	{
		LOG.info("Service NKM ::: Se recibe peticion para Actualizacion Manual del Riesgo");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		NivelRiesgoFacade facade = new NivelRiesgoFacade(servletContext);
		return facade.actualizarNivelRiesgoManual(buc, nivelRiesgo, indicadorUpld, claveEmpleado, fechaActualizacion, idNorkom);
	}
}
