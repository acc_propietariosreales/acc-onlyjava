/**
 * Isban Mexico
 *   Clase: ComponenteCentralNorkomWS.java
 *   Descripcion: Componente WS para generar los cuestionarios con sus respuestas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import mx.isban.norkom.cuestionario.dto.CuestionarioFinalizadoDTO;
import mx.isban.norkom.facade.ComponenteCentralFacade;
import mx.isban.norkom.ws.dto.CuestionariosWebServiceDTO;
import mx.isban.norkom.ws.dto.RelacionadosWebServiceDTO;
import mx.isban.norkom.ws.dto.ReporteBase64WebServiceDTO;
import mx.isban.norkom.ws.dto.RespuestaWebService;

import org.apache.log4j.Logger;

@WebService(targetNamespace="http://ws.ccc.norkom.isban.mx/", serviceName="ComponenteCentralCuestionariosServices", portName="ComponenteCentralCuestionariosServicesPort")
@SOAPBinding(style = Style.DOCUMENT, use=Use.LITERAL)
public class ComponenteCentralNorkomWS {
	
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOGGER = Logger.getLogger(ComponenteCentralNorkomWS.class);
	/**
	 * WebServiceContext contexto para la utilizacion de los BO's
	 */
	@Resource
	private transient WebServiceContext context;
	/**
	 * Generar un identificador de cuestionario
	 * @param idAplicacion Identificador de la aplicacion
	 * @param buc Codigo de Cliente
	 * @return CuestionariosWebServiceDTO Objecto con la informacion de la operacion
	 */
	@WebMethod
	public CuestionariosWebServiceDTO obtenerIdCuestionario(@WebParam(name="idAplicacion") String idAplicacion, @WebParam(name="buc") String buc) {
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.obtenerIdCuestionario");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		
		ComponenteCentralFacade facade = new ComponenteCentralFacade(servletContext);
		return facade.obtenerIdCuestionario(idAplicacion, buc);
	}
	
	/**
	 * Obtiene la URL a la cual se deben solicitar los cuestionario
	 * @return URL de los cuestionarios en el Componente Central, si no se encuentra se regresa un acadena vacia
	 */
	@WebMethod
	public String obtenerUrlCuestionario() {
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.obtenerUrlCuestionario");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		ComponenteCentralFacade facade= new ComponenteCentralFacade(servletContext);
		return facade.obtenerUrlCuestionario();
	}
	
	/**
	 * Obtiene un cuestionario en formato PDF.<BR>
	 * El primer filtro sera el id de formulario, <br>
	 * como segundo filtro en caso de que no venga el id de formulario y seran obligatorios: 
	 * <br>-Codigo de sucursal
     * <br>-Numero de cuenta o contrato
     * <br>-Codigo de producto
     * <br>-Codigo de subproducto
     * @param idFormulario el id de formulario
     * @param codigoSucursal Codigo de sucursal
     * @param numeroContrato Numero de contrato del cual se desea obtener el cuestionario
     * @param codigoProducto Codigo de producto
     * @param codigoSubProducto Codigo de subproducto
     * 
	 * @return String Cadena en Base64 con la informacion de los cuestionarios
	 */
	@WebMethod
	public ReporteBase64WebServiceDTO obtenerCuestionarioPdf(@WebParam(name="idFromulario") String idFormulario,
			@WebParam(name="codSucursal") String codigoSucursal,
			@WebParam(name="numeroContrato") String numeroContrato,
			@WebParam(name="codProducto") String codigoProducto,
			@WebParam(name="codSubProd") String codigoSubProducto
			
	) {
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.obtenerCuestionarioPdf");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		ComponenteCentralFacade facade= new ComponenteCentralFacade(servletContext);
		return facade.obtenerCuestionarioPdf(idFormulario, codigoSucursal, numeroContrato, codigoProducto, codigoSubProducto);
	}
	
	/** 
	 * Agrega relacionados a un Cuestionario
	 * @param relacionadosWS Lista de relacionados que se desean agregar
	 * @return RelacionadosWebServiceDTO Respuesta de la operacion de agregar relacionados
	 */
	@WebMethod
	public RelacionadosWebServiceDTO agregarRelacionados(@WebParam(name="relacionadosWS") RelacionadosWebServiceDTO relacionadosWS){
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.agregarRelacionados");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		ComponenteCentralFacade facade= new ComponenteCentralFacade(servletContext);
		return facade.agregarRelacionados(relacionadosWS);
	}
	

	/** 
	 * Agrega relacionados a un Cuestionario
	 * @param relacionadosWS Lista de relacionados que se desean agregar
	 * @return RelacionadosWebServiceDTO Respuesta de la operacion de agregar relacionados
	 */
	@WebMethod
	public RelacionadosWebServiceDTO agregarRelacionadosEnviaNorkom(@WebParam(name="relacionadosWS") RelacionadosWebServiceDTO relacionadosWS){
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.agregarRelacionados");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		ComponenteCentralFacade facade= new ComponenteCentralFacade(servletContext);
		return facade.agregarRelacionados(relacionadosWS);
	}
	

	/**
	 * Consulta los relacionados asignados a un cuestionario
	 * @param numeroContrato Numero de contrato del cual se desean obtener los relacionados
	 * @return RelacionadosWebServiceDTO Lista con los relacionados asignados
	 */
	@WebMethod
	public RelacionadosWebServiceDTO consultarRelacionados(@WebParam(name="numeroContrato") String numeroContrato){
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.consultarRelacionados");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		ComponenteCentralFacade facade= new ComponenteCentralFacade(servletContext);
		return facade.consultarRelacionados(numeroContrato);
	}
	
	/**
	 * Asigna un numero de contrato a un cuestionario
	 * @param idCuestionario Identificador de cuestionario al cual se va a agregar el contrato
	 * @param numeroContrato Numero de contato que se va a asignar
	 * @return RelacionadosWebServiceDTO Resultado de la operacion
	 */
	@WebMethod
	public RespuestaWebService asignarNumeroContrato(@WebParam(name="idCuestionario") String idCuestionario, @WebParam(name="numeroContrato") String numeroContrato){
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.asignarNumeroContrato");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		ComponenteCentralFacade facade= new ComponenteCentralFacade(servletContext);
		return facade.asignarNumeroContrato(idCuestionario, numeroContrato);
	}
	
	/**
	 * Obtiene el Nivel de Riesgo e Indicaor UPLD separador por pipe |
	 * @param idCuestionario Identificador del cuestionario guardado
	 * @return CuestionariosWebServiceDTO Objecto con la informacion de la operacion
	 */
	@WebMethod(operationName="obtenerNivelR_IndicadorU")
	public CuestionarioFinalizadoDTO obtenerNivelRIndicadorU(@WebParam(name="idCuestionario") String idCuestionario) {
		LOGGER.info("Ingresamos a ComponenteCentralNorkomWS.obtenerNivelR_IndicadorU");
		final ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		
		ComponenteCentralFacade facade = new ComponenteCentralFacade(servletContext);
		return facade.obtenerNivRIndU(idCuestionario);
	}
	
	
}
