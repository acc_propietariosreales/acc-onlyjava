/**
 * Isban Mexico
 *   Clase: PaisesBO.java
 *   Descripcion: Interfaz para el BO PaisesBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;

/** 
 * 
 * @author Stefanini (jgarcia) 11/02/2015
 *
 */
@Local
public interface ClientesBO {
	/**
	 * Obtiene la informacion del cliente con base a su codigo
	 * @param buc Codigo del cliente
	 * @param psession Objeto de sesion de Agave
	 * @return Informacion del cliente
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public ClienteDTO obtenerClientePorBuc(String buc, ArchitechSessionBean psession)throws BusinessException;
	/**
	 * Obtiene el cliente asignado a un cuestionario
	 * @param claveClienteCuestionario Clave del Cliente-Cuestionario al que esta asignado el cliente
	 * @param psession Objeto de sesion de Agave
	 * @return Informacion del cliente
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public ClienteDTO obtenerClientePorClaveClteCuest(int claveClienteCuestionario, ArchitechSessionBean psession)throws BusinessException;
}
