/**
 * Isban Mexico
 *   Clase: RelacionadosEnvioWSBO.java
 *   Descripcion: Interfaz para el BO RelacionadosEnvioWSBO
 *   
 *   Control de Cambios:
 *   1.0 Jul 14, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosRequestDTO;

/** 
 * 
 * @author Stefanini (lespinosa) 14/07/2015
 * interfaz de relacionados para el envio
 * a Norkom via WS
 */
@Local
public interface RelacionadosEnvioWSBO {
	/**
	 * Definicion de metodo para el envio de
	 * los relacionados ingresados previamente a DB
	 * para su envio a Norkom
	 * @param relacionados lista de relacionados a enviar
	 * @param entradaDto dto con datos para el envio
	 * @param psession bean de session
	 * @return String con respuesta de envio
	 */
	String enviaRelacionadosWS(List<RelacionadoDTO> relacionados, RelacionadosRequestDTO entradaDto, ArchitechSessionBean psession);
}
