/**
 * Isban Mexico
 *   Clase: PaisesBO.java
 *   Descripcion: Interfaz para el BO PaisesBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Local
public interface ValidadorEntidadFinancieraBO {
	/**
	 * Valor para la seccion de Actividad Economica
	 */
	public static final String SECCION_ACTIVIDAD_ECON = "ACTE";
	/**
	 * Valor para la pregunta "Entidad establecida fisicamente"
	 */
	public static final String ABREV_ESTABLECIDA_MEXICO = "ENFI";
	
	/**
	 * Calcula si la pregunta "Establecida fisicamente en algun lugar" fue contestada como verdadero o falso
	 * @param idCuestionario Identificador de cuestionario al cual se le va a hacer la validacion
	 * @param sesion Objeto de la sesion de Agave
	 * @return True en caso de que se haya contestado que la entidad se encuentra establecida fisicamente en algun lugar
	 * @throws BusinessException En caso de error en la informacion o en las consultas de BD
	 */
	public boolean entidadEstablecidaFisicamente(String idCuestionario, ArchitechSessionBean sesion) throws BusinessException;
}
