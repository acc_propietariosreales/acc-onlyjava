/**
 * Isban Mexico
 *   Clase: ValidadorRiesgoA1BO.java
 *   Descripcion: interfaz para las operaciones de validacion de riesgo de mismo dia.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.NivelRiesgoDTO;

/**
 * @author jmquillo
 *
 */
@Local
public interface ValidadorRiesgoA1BO {

	/**
	 * Define si un cliente es del tipo regimen simplificado
	 * @param peticion Contiene la informacion que se va a validar
	 * @param sesion Objeto de sesion de Agave
	 * @return boolean True en caso de que aplique para el regimen simplificado, False en caso contrario
	 * @throws BusinessException En caso de error en la informacion o en las consultas de BD
	 */
	public boolean esRegimenSimplificado(RequestObtenerCuestionario peticion, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Define si un cliente es del tipo regimen simplificado y es diferente tipo A1 la calificacion
	 * @param peticion Contiene la informacion que se va a validar
	 * @param sesion Objeto de sesion de Agave
	 * @return NivelRiesgoDTO con datos de respuesta de la validacion
	 * @throws BusinessException En caso de error en la informacion o en las consultas de BD
	 */
	public NivelRiesgoDTO esRegimenSimplificadoAx(RequestObtenerCuestionario peticion, ArchitechSessionBean sesion) throws BusinessException;
	
}
