/**
 * Isban Mexico
 *   Clase: ActividadEconomicaBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de las actividades economicas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.ActividadEconomicaBO;
import mx.isban.norkom.cuestionario.dao.ActividadEconomicaDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;

import org.apache.log4j.Logger;

/** 
 *  Session Bean implementation class ActividadEconomicaBOImpl
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ActividadEconomicaBOImpl extends Architech implements ActividadEconomicaBO {

	/**
	 * Identificador utilizado para la serialiacion del objeto
	 */
	private static final long serialVersionUID = 4874312890217339124L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(ActividadEconomicaBOImpl.class);
	/**
	 * Objeto para realizar las operaciones sobre las Actividades Economicas
	 */
	@EJB
	private transient ActividadEconomicaDAO actividadEconomicaDAO;
	
	@Override
	public ActividadEconomicaDTO obtenerActividadEconomica(String codigoActividad, ArchitechSessionBean psession) throws BusinessException {
		LOG.info(String.format("Inicia obtencion de la actividad con codigo[%s]", codigoActividad));
		return actividadEconomicaDAO.obtenerActividadEconomicaPorID(codigoActividad);
	}
	
}