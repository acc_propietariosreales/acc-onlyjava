/**
 * Isban Mexico
 *   Clase: AdministradorComponenteCentralBOImpl.java
 *   Objetivo: Componente para realizar la logica de negocio de los cuestionarios IP y IC.
 *   Justificacion: A traves de este componente se realizaran tareas comunes como los son la obtencion del Identificador de Cuestionario y la Asignacion de un Contrato a un Cuestionario
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.norkom.schemas.wsdl.realtimelogicservices.RealTimeLogicEjbEndpoint;
import com.norkom.schemas.wsdl.realtimelogicservices.RealTimeLogicService;
import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.AdministradorComponenteCentralBO;
import mx.isban.norkom.cuestionario.dao.AdministradorComponenteCentralDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.NivelRiesgoDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.RequestODP1DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP1DTO;
import mx.isban.norkom.cuestionario.mq.PersonasMQ;
import mx.isban.norkom.cuestionario.util.GeneradorXmlNuevosDatosNkm;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AdministradorComponenteCentralBOImpl extends Architech implements AdministradorComponenteCentralBO {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -9008664095495840647L;

	/**
	 * Variable utilizada para instanciar FORMATO_NUMERO
	 */
	private static final DecimalFormat FORMATO_NUMERO = new DecimalFormat("000000");
	
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOG = Logger.getLogger(AdministradorComponenteCentralBOImpl.class);
	
	/** Numero de milisegundos en un segundo */
	private static final int MILI_POR_SEGUNDO = 1000;
	
	/**
	 * Timeout para la ejecucion del servicio web de Norkom
	 */
	private static final int DEFAULT_TIMEOUT_WS = 10000;

	/**
	 * Variable utilizada para declarar admonIdDAO
	 */
	@EJB
	private transient AdministradorComponenteCentralDAO admonIdDAO;
	/**
	 * Variable utilizada para declarar cuestionarioDAO
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	/**
	 * Variable utilizada para declarar preguntasDAO
	 */
	@EJB
	private transient PreguntasDAO preguntasDAO;
	
	/**
	 * PersonasMQ Acceso a las transacciones de MQ
	 */
	@EJB
	private transient PersonasMQ personasMQ;
	
	/**
	 * Dao para obtener los Parametros 
	 */
	@EJB
	private transient ParametrosDAO parametrosDAO;
	
	/**
	 * Dao para obtener las equivalencias de Nivel de Riesgo e Indicador UPLD para Personas y Norkom
	 */
	@EJB
	private transient NivelRiesgoDAO nivelRiesgoDAO;
	
	@Override
	public String obtenerIdCuestionario(String idAplicacion, ArchitechSessionBean psession) throws BusinessException {
		LOG.info(String.format("BO ::: obtenerIdCuestionario :: Id Aplicacion[%s]", idAplicacion));
		
		if(idAplicacion == null || idAplicacion.isEmpty()){
			throw new BusinessException(COD_ER_ID_APLICACION, DESC_ER_ID_APLICACION);
		}
		
		ParametroDTO paramIdAplicacionBD = parametrosDAO.obtenerParametroPorNombre(idAplicacion);
		if(paramIdAplicacionBD == null || paramIdAplicacionBD.getValor() == null || paramIdAplicacionBD.getValor().isEmpty()) {
			throw new BusinessException(COD_ER_ID_APLICACION, DESC_ER_ID_APLICACION_VAL);
		}
		
		SimpleDateFormat FORMATO_FECHA = new SimpleDateFormat("yyyyMMdd", Locale.US);
		String fechaActual = FORMATO_FECHA.format(new Date());
		int valorActualSecuencia = admonIdDAO.obtenerSecuenciaCuestionario(psession);
		
		int secuenciaBD = obtenerValorFinalSecuencia(valorActualSecuencia);
		
		String secuenciaCuestionario = FORMATO_NUMERO.format(secuenciaBD);
		
		LOG.info(String.format("BO ::: obtenerIdCuestionario :: SecuenciaBD[%s]", secuenciaBD));
		
		String idCuestionario = String.format("%s%s%s", idAplicacion, secuenciaCuestionario, fechaActual);
		
		LOG.info(String.format("Se formo el Identificador de Cuestionario[%s]", idCuestionario));
		
		return idCuestionario;
	}
	/**
	 * obtenerValorFinalSecuencia
	 * @param valorActualSecuencia valor de secuencia
	 * @return int de valor de secuencia
	 */
	private static int obtenerValorFinalSecuencia(int valorActualSecuencia) {
		String secuencia = String.valueOf(valorActualSecuencia);
		
		if(secuencia.length() > 6){
			return Integer.parseInt(secuencia.substring(secuencia.length() - 6));
		}
		
		return Integer.parseInt(secuencia);
	}
	
	/**
	 * @param idCuestionario de asignarNumeroContrato
	 * @param numeroContrato de asignarNumeroContrato
	 * @param psession de asignarNumeroContrato
	 * @throws BusinessException de asignarNumeroContrato
	 */
	public void asignarNumeroContrato(String idCuestionario, String numeroContrato, ArchitechSessionBean psession) throws BusinessException {
		if(idCuestionario == null || StringUtils.isBlank(idCuestionario)){
			throw new BusinessException(COD_ER_ID_CUESTIONARIO, DESC_ER_ID_CUESTIONARIO);
		}
		
		if(numeroContrato == null || StringUtils.isBlank(numeroContrato)){
			throw new BusinessException(COD_ER_NUMERO_CONTRATO, DESC_ER_NUMERO_CONTRATO);
		}
		
		int numeroPreguntasEnCuestionario = preguntasDAO.numeroPreguntasEnCuestionario(idCuestionario);
		if(numeroPreguntasEnCuestionario == 0){
			throw new BusinessException(COD_ER_CUESTIONARIO_INCOMPLETO, DESC_ER_CUESTIONARIO_INCOMPLETO);
		}
		
		admonIdDAO.asignarNumeroContrato(idCuestionario, numeroContrato, psession);
		actualizarDatosEnNorkom(idCuestionario, psession);
	}
	
	/**
	 * @param buc numero de buc para la busqueda
	 * @param numeroContrato  para la busqueda
	 * @param fechaInicio  para la busqueda
	 * @param fechaFin para  para la busqueda
	 * @param psession bean de session
	 * @return List<CuestionarioWSDTO> con el resultado de la busqueda
	 * @throws BusinessException con mensaje de error
	 */
	public List<CuestionarioWSDTO> obtenerDatosGeneralesCuestionario(
			String buc, String numeroContrato, Date fechaInicio, Date fechaFin, ArchitechSessionBean psession) 
	throws BusinessException {
		return cuestionarioDAO.obtenerDatosGeneralesCuestionario(null, buc, numeroContrato, fechaInicio, fechaFin, psession);
	}

	@Override
	public List<HashMap<String, Object>> obtenerNivRIndU(String idCuestionario,
			ArchitechSessionBean psession) throws BusinessException {
		
		if(idCuestionario == null || idCuestionario.isEmpty()){
			throw new BusinessException(COD_ER_ID_CUESTIONARIO, DESC_ER_ID_CUESTIONARIO);
		}
		
		return  admonIdDAO.obtenerNivRIndU(idCuestionario, psession);
	}
	
	@Override
	public void actualizarDatosEnNorkom(String idCuestionario, ArchitechSessionBean psession) throws BusinessException {
		if(StringUtils.isBlank(idCuestionario)) {
			throw new BusinessException(COD_ER_NUMERO_CNTR_VACIO, DESC_ER_NUMERO_CNTR_VACIO);
		}
		
		List<CuestionarioWSDTO> cuestionarios = cuestionarioDAO.obtenerDatosGeneralesCuestionario(idCuestionario, null, null, null, null, psession);
		if(cuestionarios == null || cuestionarios.isEmpty()) { 
			throw new BusinessException(COD_ER_INFO_BD_INEXISTENTE, DESC_ER_INFO_BD_INEXISTENTE);
		}
		CuestionarioWSDTO cuestionarioWS = cuestionarios.get(0);
		

		String buc = cuestionarioWS.getCodigoCliente();
		String numCntrWS = cuestionarioWS.getNumeroContrato();
		if(StringUtils.isBlank(buc)) {
			throw new BusinessException(COD_ER_BUC_VACIO, DESC_ER_BUC_VACIO);
		}
		
		if(StringUtils.isBlank(numCntrWS)) {
			throw new BusinessException(COD_ER_NUMERO_CNTR_VACIO, DESC_ER_NUMERO_CNTR_VACIO);
		}
		
		RequestODP1DTO request = new RequestODP1DTO();
		request.setNumeroPersona(buc);
		ResponseODP1DTO response = personasMQ.obtenerCalificacionesODP1(request, psession);
		if(!ResponseODP1DTO.OPERACION_EXITOSA.equals(response.getCodigoOperacion())) {
			throw new BusinessException(COD_ER_LLAMADO_390, DESC_ER_LLAMADO_390);
		}
		
		if(StringUtils.isBlank(response.getIndicadorNivelRiesgo()) && !StringUtils.isBlank(response.getIndicadorUpld())) {
			throw new BusinessException(COD_ER_DATOS_390, DESC_ER_DATOS_390);
		}
		
		if(!StringUtils.isBlank(response.getIndicadorNivelRiesgo()) && StringUtils.isBlank(response.getIndicadorUpld())) {
			throw new BusinessException(COD_ER_DATOS_390, DESC_ER_DATOS_390);
		}
		
		info(String.format("Respuesta de ODP1 NivelRiesgo[%s] IndicadorUpld[%s]", response.getIndicadorNivelRiesgo(), response.getIndicadorUpld()));
		if(StringUtils.isBlank(response.getIndicadorNivelRiesgo()) && StringUtils.isBlank(response.getIndicadorUpld()) && obtenerIndicadoresDeBaseDatos(cuestionarioWS)) {
			info(String.format(
					"La informacion de la ODP1 no es valida, se va a utilizar la que se encuentra en BD NivelRiesgo[%s] IndicadorUPLD[%s]", 
					cuestionarioWS.getNivelRiesgo(), cuestionarioWS.getIndicadorUpld()));
		} else {
			CalificacionDTO nivelRiesgoNkm = nivelRiesgoDAO.obtenerValoresPorIndicadorPersonas(response.getIndicadorNivelRiesgo());
			CalificacionDTO indicadorUpldNkm = nivelRiesgoDAO.obtenerValoresPorIndicadorPersonas(response.getIndicadorUpld());
			
			cuestionarioWS.setNivelRiesgo(nivelRiesgoNkm.getValorIndicadorNorkom());
			cuestionarioWS.setIndicadorUpld(indicadorUpldNkm.getValorIndicadorNorkom());
		}
		
		if(StringUtils.isBlank(response.getIndicadorNivelRiesgo()) && StringUtils.isBlank(response.getIndicadorUpld())) {
			throw new BusinessException(COD_ER_DATOS_390, DESC_ER_DATOS_390);
		}
		
		GeneradorXmlNuevosDatosNkm generadorXml = new GeneradorXmlNuevosDatosNkm();
		String xmlNkm = generadorXml.creaFormatoXML(cuestionarioWS);
		
		if(StringUtils.isBlank(xmlNkm)) {
			throw new BusinessException(COD_ER_GENERAR_XML, DESC_ER_GENERAR_XML);
		}
		
		enviarInformacionParaNorkom(cuestionarioWS, xmlNkm);
	}
	
	/**
	 * Valida si es posible tomar el Nivel de Riesgo y el Indicador UPLD de la base de datos del ACC
	 * @param cuestionarioWS Informacion del Cuestionario
	 * @return Verdadero en caso de que sea posible tomar la informacion de BD, Falso en caso contrario
	 */
	private boolean obtenerIndicadoresDeBaseDatos(CuestionarioWSDTO cuestionarioWS){
		if("A1".equals(cuestionarioWS.getNivelRiesgo()) || "A2".equals(cuestionarioWS.getNivelRiesgo())){
			return true;
		}
		
		if("A3".equals(cuestionarioWS.getNivelRiesgo()) && "KYC-DR".equals(cuestionarioWS.getIndicadorUpld())){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Llamado al cliente de Norkom para actualizar la informacion
	 * @param cuestionario Informacion del Cuestionario
	 * @param xmlNkm XML con la informacion para Norkom
	 * @throws BusinessException En caso de un problema al obtener los parametros de la BD
	 */
	private void enviarInformacionParaNorkom(CuestionarioWSDTO cuestionario, String xmlNkm) throws BusinessException {
		info("Inicia Proceso de calificacion a traves del WS de Norkom");
		
		ParametroDTO paramWsdlLocation = parametrosDAO.obtenerParametroPorNombre("wsdlLocationRealTime");
		ParametroDTO paramMessageType = parametrosDAO.obtenerParametroPorNombre("wsMsgTypeRealTimeUpd");
		ParametroDTO paramMessageFunctionSequence = parametrosDAO.obtenerParametroPorNombre("wsMsgFunctionSequenceRealTimeUpd");
		ParametroDTO paramTimeOut = parametrosDAO.obtenerParametroPorNombre("TimeOutWebService");

		int timeoutMiliseconds = DEFAULT_TIMEOUT_WS;
		try {
			timeoutMiliseconds = Integer.parseInt(paramTimeOut.getValor());
		} catch(NumberFormatException nfe){
			error("No existe un valor valido para el parametro TimeOutWebService, se maneja valor por default");
		}
		
		try {
			URL wsdlURL = new URL(paramWsdlLocation.getValor());
			RealTimeLogicService ss = new RealTimeLogicService(wsdlURL);
			RealTimeLogicEjbEndpoint port = ss.getServicesManagerPort();
			
			Map<String, Object> requestContext = ((BindingProvider)port).getRequestContext();
			requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, timeoutMiliseconds);
			requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, timeoutMiliseconds);
			
			String timeoutSeconds = String.valueOf(timeoutMiliseconds / MILI_POR_SEGUNDO);        
			requestContext.put(com.ibm.wsspi.webservices.Constants.CONNECTION_TIMEOUT_PROPERTY , timeoutSeconds);
			requestContext.put(com.ibm.wsspi.webservices.Constants.WRITE_TIMEOUT_PROPERTY , timeoutSeconds);
			requestContext.put(com.ibm.wsspi.webservices.Constants.RESPONSE_TIMEOUT_PROPERTY , timeoutSeconds);
			
			info(String.format("Datos procesamiento IdCuestionario[%s] MessageType[%s] MessageFunctionSequence[%s] messageData:[%s]", 
					cuestionario.getIdCuestionario(), paramMessageType.getValor(), paramMessageFunctionSequence.getValor(), xmlNkm));
			
			String resultadoCalificacion = port.processMessage(
					cuestionario.getIdCuestionario(), paramMessageType.getValor(), paramMessageFunctionSequence.getValor(), xmlNkm);
			
			info(String.format("Resultado de la actualizacion de datos [%s]", resultadoCalificacion));
		} catch (MalformedURLException e) {
			error(String.format("No fue posible formar la URL con el valor paramWsdlLocation[%s], no se realiza actualizacion de datos", paramWsdlLocation));
			showException(e);
		}
	}
}
