/**
 * Isban Mexico
 *   Clase: AdministradorOrigenDestinoRecursosBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del origen y destino de los recursos.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.AdministradorOrigenDestinoRecursosBO;
import mx.isban.norkom.cuestionario.dao.OrigenDestinoRecursosDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;



/**
 *
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AdministradorOrigenDestinoRecursosBOImpl extends Architech implements AdministradorOrigenDestinoRecursosBO {
	/**
	 * long serial version UID
	 */
	private static final long serialVersionUID = -7859544563008704909L;

	/**
	 * Prefijo utilizado para obtener las preguntas de la tabla de parametros
	 */
	private static final String PREFIJO_ID_PREGUNTAS = "ID_PREGUNTAS_";

	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(AdministradorOrigenDestinoRecursosBOImpl.class);

	/**
	 * Variable utilizada para declarar preguntasDAO
	 */
	@EJB
	private transient PreguntasDAO preguntasDAO;

	/**
	 * ParametrosDAO para obtener los parametros
	 */
	@EJB
	private transient ParametrosDAO parametrosDAO;

	/**
	 * OrigenDestinoRecursosDAO para obtener el destino y origen de los recursos
	 */
	@EJB
	private transient OrigenDestinoRecursosDAO origenDestinoRecursosDAO;

	@Override
	public List<PreguntaDTO> obtenerPreguntasPorRespuesta(String tipoPersona, String subtipoPersona, int opcionRespuesta, ArchitechSessionBean psession)
	throws BusinessException {
		String abreviaturas = "''";
		LOG.debug("inicia obtenerPreguntasPorRespuesta.");
		RecursoDTO recursoDTO = origenDestinoRecursosDAO.obtenerRecursoPorClave(opcionRespuesta, psession);
		String idPreguntasParametro = StringUtils.EMPTY;

		if(recursoDTO != null && "F".equals(tipoPersona) && "S".equals(subtipoPersona)) {
			idPreguntasParametro = String.format("%s%02d_%s", PREFIJO_ID_PREGUNTAS, Integer.parseInt(recursoDTO.getCodigoRecurso()), "E");
		} else if(recursoDTO != null && "F".equals(tipoPersona)) {
			idPreguntasParametro = String.format("%s%02d_%s", PREFIJO_ID_PREGUNTAS, Integer.parseInt(recursoDTO.getCodigoRecurso()), tipoPersona);
		} else if(recursoDTO != null && "J".equals(tipoPersona)) {
			idPreguntasParametro = String.format("%s%02d_%s", PREFIJO_ID_PREGUNTAS, Integer.parseInt(recursoDTO.getCodigoRecurso()), tipoPersona);
		}

		ParametroDTO parametro = parametrosDAO.obtenerParametroPorNombre(idPreguntasParametro);

		if(parametro != null && !StringUtils.isBlank(parametro.getValor())) {
			abreviaturas = parametro.getValor().replaceAll("\"", "'");
		}

		List<PreguntaDTO> preguntasBD = preguntasDAO.obtenerPreguntasPorAbreviaturas(abreviaturas, psession);
		List<PreguntaDTO> preguntasOrdenadas = new ArrayList<PreguntaDTO>();

		String[] abreviaturasSeparadas = null;
		if(parametro != null && !StringUtils.isBlank(parametro.getValor())) {
			abreviaturasSeparadas = parametro.getValor().replaceAll("\"", "").split(",");
		}

		if(preguntasBD != null && !preguntasBD.isEmpty() && abreviaturasSeparadas.length > 0) {
			for(String abreviatura : abreviaturasSeparadas) {
				for(PreguntaDTO preguntaBD : preguntasBD){
					if(abreviatura.equals(preguntaBD.getAbreviatura())) {
						preguntasOrdenadas.add(preguntaBD);
					}
				}
			}
		}

		return preguntasOrdenadas;
	}
}