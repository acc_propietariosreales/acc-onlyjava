/**
 * Isban Mexico
 *   Clase: AdministradorRelacionadosBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de los accionistas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.AdministradorRelacionadosBO;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.dao.AdministradorRelacionadosDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosWebServiceDTO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AdministradorRelacionadosBOImpl extends Architech implements AdministradorRelacionadosBO {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 7391340431606963173L;
	
	/**
	 * Variable utilizada para declarar relacionadosDAO
	 */
	@EJB
	private transient AdministradorRelacionadosDAO relacionadosDAO;
	/**
	 * Variable utilizada para declarar cuestionarioDAO
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	/**
	 * Objeto para la obtencion de claves de BD
	 */
	@EJB
	private transient AdministradorClavesDAO administradorClavesDAO;
	
	/**
	 * Obtencion de los parametros de BD
	 */
	@EJB
	private transient ParametrosDAO parametrosDAO;
	
	@Override
	public List<RelacionadoDTO> agregarRelacionados(RelacionadosWebServiceDTO relacionadosWS, ArchitechSessionBean psession) 
	throws BusinessException {
		List<RelacionadoDTO> relacionadosErroneos = new ArrayList<RelacionadoDTO>();
		List<RelacionadoDTO> relacionadosInsercion = new ArrayList<RelacionadoDTO>();
		int claveClienteCuestionario = 0;
		ParametroDTO paramTipoPersona = parametrosDAO.obtenerParametroPorNombre("tipoPersonaPred");
		ParametroDTO paramSubtipoPersona = parametrosDAO.obtenerParametroPorNombre("subtipoPersonaPred");
		ParametroDTO paramDivisa = parametrosDAO.obtenerParametroPorNombre("divisaPred");
		
		String idCuestionario = relacionadosWS.getIdCuestionario();
		
		if(StringUtils.isBlank(idCuestionario)) {
			throw new BusinessException(Mensajes.ERRLGD01.getCodigo(), Mensajes.ERRLGD01.getDescripcion());
		}
		
		
		CuestionarioWSDTO cuestionarioWS = cuestionarioDAO.obtenerDatosGeneralesCuestionarioPorId(idCuestionario, psession);
		
		/*
		 * Si ya existe el cuestionario se obtiene la clave Cliente-Cuestionario para asignarle los Relacionados
		 * En caso de que no exista se agrega un nuevo registro.
		 */
		if(cuestionarioWS != null && cuestionarioWS.getClaveCuestionario() > 0) {
			claveClienteCuestionario = cuestionarioWS.getClaveCuestionario();
		} else {
			claveClienteCuestionario = administradorClavesDAO.obtenerNuevaClaveClienteCuestionario();
			info(String.format("Se obtiene la clave Cliente - Cuestionario [%s] para la insercion de los relacionados", claveClienteCuestionario));
			
			CuestionarioDTO cuestionarioDTO = creCuestionarioDTO(claveClienteCuestionario, paramTipoPersona, paramSubtipoPersona, paramDivisa, idCuestionario);
			
			CuestionarioDTO tipoCuestionarioBD = cuestionarioDAO.obtenerTipoCuestionario(cuestionarioDTO, "IP", psession);
			cuestionarioDTO.setClaveCuestionario(tipoCuestionarioBD.getClaveCuestionario());
			
			relacionadosDAO.agregarCuestionarioBasico(cuestionarioDTO, psession);
		}
		
		final SimpleDateFormat FORMATO_FECHA_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		
		for(RelacionadoDTO relacionado : relacionadosWS.getRelacionados()){
			try {
				//Trata de hacer la conversion para validar que la fecha es correcta
				Date fecha = FORMATO_FECHA_YYYY_MM_DD.parse(relacionado.getFechaNacimiento());
				
				if(fecha == null) {
					relacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
					relacionado.setDescOperacion("Fecha incorrecta");
					relacionadosErroneos.add(relacionado);
				} else if(!TIPO_RELACION_ACCIONISTA.equals(relacionado.getTipoRelacion()) && !TIPO_RELACION_BENEFICIARIO.equals(relacionado.getTipoRelacion()) &&
						!TIPO_RELACION_INTERVINIENTE.equals(relacionado.getTipoRelacion()) && !TIPO_RELACION_PROVEEDOR.equals(relacionado.getTipoRelacion()))
				{
					relacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
					relacionado.setDescOperacion("Tipo de Relaci\u00F3n incorrecta");
					relacionadosErroneos.add(relacionado);
				} if(!GeneradorCuestionarioGenerico.IND_PERSONA_FISICA.equals(relacionado.getTipoPersona()) && 
						!GeneradorCuestionarioGenerico.IND_PERSONA_MORAL.equals(relacionado.getTipoPersona())) 
				{
					relacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
					relacionado.setDescOperacion("Tipo de Persona incorrecta");
					relacionadosErroneos.add(relacionado);
				} else {
					relacionadosInsercion.add(relacionado);
				}
			} catch (ParseException e) {
				showException(e);
				error(String.format("Error al convertir fecha[%s] Causa[%s] Mensaje[%s]", 
										relacionado.getFechaNacimiento(), e.getCause(), e.getLocalizedMessage()));
				relacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
				relacionado.setDescOperacion("Fecha incorrecta");
				relacionadosErroneos.add(relacionado);
			}
		}
		
		relacionadosErroneos.addAll(relacionadosDAO.agregarRelacionados(relacionadosInsercion, claveClienteCuestionario, psession));
		
		return relacionadosErroneos;
	}
	/**
	 * creamod dto de cuestionario
	 * @param claveClienteCuestionario clave de cliente cuest
	 * @param paramTipoPersona tipo de persona
	 * @param paramSubtipoPersona subtipo de persona
	 * @param paramDivisa divisa
	 * @param idCuestionario id del cuestionario
	 * @return CuestionarioDTO con los datos enviados
	 */
	private CuestionarioDTO creCuestionarioDTO(int claveClienteCuestionario,ParametroDTO paramTipoPersona,ParametroDTO paramSubtipoPersona,ParametroDTO paramDivisa,String idCuestionario) {
		CuestionarioDTO cuestionarioDTO=new CuestionarioDTO();
		cuestionarioDTO.setClaveClienteCuestionario(claveClienteCuestionario);
		cuestionarioDTO.setTipoPersona(paramTipoPersona.getValor());
		cuestionarioDTO.setSubtipoPersona(paramSubtipoPersona.getValor());
		cuestionarioDTO.setDivisa(paramDivisa.getValor());
		cuestionarioDTO.setIdCuestionario(idCuestionario);
		return cuestionarioDTO;
	}

	@Override
	public List<RelacionadoDTO> obtenerRelacionados(String numeroContrato, ArchitechSessionBean psession) 
	throws BusinessException {
		if(StringUtils.isBlank(numeroContrato)){
			throw new BusinessException("ERIAGD01", "Es necesario proporcionar un N\u00FAmero de Contrato, sin este dato no es posible realizar la operaci\u00F3n");
		}
		
		return relacionadosDAO.obtenerRelacionados(numeroContrato, psession);
	}
	
	@Override
	public List<RelacionadoDTO> obtenerRelacionadosPorClaveClienteCuestTipo(int claveClienteCuest, String tipoRelacionado, ArchitechSessionBean psession) 
	throws BusinessException {
		return relacionadosDAO.obtenerRelacionadosPorClaveClienteCuestTipo(claveClienteCuest, tipoRelacionado, psession);
	}
}
