/**
 * Isban Mexico
 *   Clase: PaisesBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de los paises.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.ClientesBO;
import mx.isban.norkom.cuestionario.dao.ClienteDAO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.util.Mensajes;

/** 
 * 
 * @author Stefanini (jgarcia) 11/02/2015
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ClientesBOImpl extends Architech implements ClientesBO {
	/**
	 * Id de version utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = 8498654235251251654L;
	
	/**
	 * Objeto por el cual se maneja la informacion de los clientes en la base de datos
	 */
	@EJB
	private transient ClienteDAO clienteDAO;

	@Override
	public ClienteDTO obtenerClientePorBuc(String buc, ArchitechSessionBean psession) throws BusinessException {
		if(buc == null || buc.isEmpty()) {
			throw new BusinessException(Mensajes.EROBCL01.getCodigo(),Mensajes.EROBCL01.getDescripcion() );
		}
		
		return clienteDAO.obtenerClientePorBuc(buc, psession);
	}

	@Override
	public ClienteDTO obtenerClientePorClaveClteCuest(int claveClienteCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		return clienteDAO.obtenerClientePorClaveClteCuest(claveClienteCuestionario, psession);
	}
}
