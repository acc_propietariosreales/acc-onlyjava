/**
 * Isban Mexico
 *   Clase: ConsultaCuestionarioPRBOImpl.java
 *   Descripcion: Componente para realizar la Obetencion las pregunta y respuestas de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Jun 25, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bo.ConsultaCuestionarioPRBO;
import mx.isban.norkom.cuestionario.dao.ConsultaRespuestasDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.RespuestasRes;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

/**
 * @author mafranco
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ConsultaCuestionarioPRBOImpl extends Architech implements ConsultaCuestionarioPRBO {
	/**
	 * ConsultaCuestionarioPRBOImpl UID  de tipo long
	 */
	private static final long serialVersionUID = 4185840694833086304L;

	/**
	 * DAO para consultar respuestas de los cuestionarios
	 */
	@EJB
	private transient ConsultaRespuestasDAO consultaRespuestasDAO;
	/** EJB CuestionarioDAO**/
	@EJB
	private transient CuestionarioDAO cuestionariosDAO;
	/**
	 * dao para accesar a los parametros de bd
	 *******/
	@EJB
	private ParametrosDAO parametrosDAO;
	/**Consulta las preguntas y respuestas del cuestionarios en base al id del cuestionario
	 * @param request RequestObtenerCuestionario dto con datos para la consulta
	 * @param psession ArchitechSessionBean bean de session
	 * @throws BusinessException excepcion lanzada
	 * @return response RespuestasRes con datos de preguntas y respuestas del cuestionario para mostrarlos
	 */
	@Override
	public RespuestasRes consultaPreguntasRespuestas(RequestObtenerCuestionario request, ArchitechSessionBean psession)throws BusinessException {
		RespuestasRes response=new RespuestasRes();
		CuestionarioWSDTO datosGenerales = 
				cuestionariosDAO.obtenerDatosGeneralesCuestionarioPorId(request.getIdCuestionario(), psession);
		
		if(datosGenerales == null){
			response.setCodOperacion("ERCTCN01");
			response.setDescOperacion("No existe informaci\u00F3n para el Identificador de Cuestionario proporcionado");
			return response;
		}
			
		CuestionarioDTO encabezadoIP = consultaRespuestasDAO.obtenerEncabezadoIP(request, psession);
		if(null==encabezadoIP){
			response.setCodOperacion("ERCTCN03");
			response.setDescOperacion("No existe informaci\u00F3n");
		}else{
			response.setEncabezadoIP(mapearEncabezadoIP(encabezadoIP));
			//Aqui ordenamos las preguntas segun su seccion
			List<PreguntaDTO> preguntasIP=consultaRespuestasDAO.obtenerRespuestasIP(request,psession);
			Map<String, List<PreguntaDTO>>
			 listaAgrupada =ordenarPreguntasPorSeccion(preguntasIP, "IP");
			 
			 List<PreguntaDTO> listaOrdenada=agruparPorSeccion(listaAgrupada,"IP",datosGenerales);
			 
			response.setPreguntasIP(listaOrdenada);
			
			List<PreguntaDTO> preguntasIC = consultaRespuestasDAO.obtenerRespuestasIC(request,psession);
			if(preguntasIC != null && !preguntasIC.isEmpty()){
				Map<String, List<PreguntaDTO>>
				 listaAgrupadaIC =ordenarPreguntasPorSeccion(preguntasIC, "IC");
				 
				 List<PreguntaDTO> listaOrdenadaIC=agruparPorSeccion(listaAgrupadaIC,"IC",datosGenerales);
				response.setPreguntasIC(listaOrdenadaIC);
			}
			
			response.setFamiliares(consultaRespuestasDAO.obtenerFamiliares(request, psession));
			response.setRelacionados(consultaRespuestasDAO.obtenerAccionistas(request,psession));
			
		}
		
		return response;
	}

	@Override
	public Map<String, PreguntaDTO> obtenerRespuestasICPorAbreviatura(String numeroContrato, ArchitechSessionBean psession)
	throws BusinessException {
		RequestObtenerCuestionario request = new RequestObtenerCuestionario();
		request.setIdCuestionario(numeroContrato);
		
		List<PreguntaDTO> respuestas = consultaRespuestasDAO.obtenerRespuestasIC(request, psession);
		Map<String, PreguntaDTO> respuestasPorAbrev = new HashMap<String, PreguntaDTO>();
		
		for(PreguntaDTO respuesta : respuestas) {
			respuestasPorAbrev.put(respuesta.getAbreviatura(), respuesta);
		}
		
		return respuestasPorAbrev;
	}
	
	/**Mapea los datos del encabezado IP para mostrarlos en la respuesta en DTO que contienen la pregunta respuesta
	 * @param encabezadoIP Datos del Cuestionario 
	 * @return List<PreguntaDTO>  Preguntas correspondientes al encabezado
	 * 
	 */
	private List<PreguntaDTO> mapearEncabezadoIP(CuestionarioDTO encabezadoIP){
			if(null==encabezadoIP){
				return null;
			}else{
				ClienteDTO cliente = new ClienteDTO();
				ActividadEconomicaDTO actividad = new ActividadEconomicaDTO();
				List<PreguntaDTO> encabezado=new ArrayList<PreguntaDTO>();
			
				String tipoPersona = "";
				if("J".equals(encabezadoIP.getTipoPersona())) {
					tipoPersona = LEYENDA_PERSONA_MORAL;
				} else if("F".equals(encabezadoIP.getTipoPersona()) && "S".equals(encabezadoIP.getSubtipoPersona())) {
					tipoPersona = LEYENDA_PERSONA_FISICA_AE;
				}  else if("F".equals(encabezadoIP.getTipoPersona())) {
					tipoPersona = LEYENDA_PERSONA_FISICA;
				}
				
				encabezado.add(new PreguntaDTO("FECHA DE CUESTIONARIO",encabezadoIP.getDia()+"-"+encabezadoIP.getMes()+"-"+encabezadoIP.getAnio(),"strFecha"));
				encabezado.add(new PreguntaDTO(SUCURSAL,encabezadoIP.getNombreSucursal()+" "+encabezadoIP.getCodigoSucursal(),STRSUCURSAL));
				
				encabezado.add(new PreguntaDTO(ZONA,encabezadoIP.getNombreZona(),STRZONA));
				encabezado.add(new PreguntaDTO(PLAZA,encabezadoIP.getNombrePlaza(),STRPLAZA));
				cliente = encabezadoIP.getCliente();
				encabezado.add(new PreguntaDTO(NUMCLIENTE,cliente.getCodigoCliente(),STRCODCLIENTE));
				encabezado.add(new PreguntaDTO(NOMBRECLIENTE, getNombreCompletoNorkom(cliente, tipoPersona), STRNOMBREC));
				encabezado.add(new PreguntaDTO(CUENTA,encabezadoIP.getCuentaContrato(),STRNUMCUECON));
				encabezado.add(new PreguntaDTO(SEGMENTO,encabezadoIP.getDescSegmento(),STRSEGMENTO));
				encabezado.add(new PreguntaDTO(PRODUCTO,encabezadoIP.getDescProducto(),STRPRODUCTO));
				encabezado.add(new PreguntaDTO(PAIS,encabezadoIP.getNombrePais(),STRPAIS));
				encabezado.add(new PreguntaDTO(ESTADO,encabezadoIP.getNombreEntidad(),STRESTADO));
				encabezado.add(new PreguntaDTO(MUNICIPIO,encabezadoIP.getNombreMunicipio(),STRMUNICIPIO));
				encabezado.add(new PreguntaDTO(NACIONALIDAD,encabezadoIP.getNombreNacionalidad(),STRNACIONALIDAD));
				actividad = encabezadoIP.getActividadGenerica();
				encabezado.add(new PreguntaDTO(ACTGENERICA,actividad.getCodigo(),STRACTGENERICA));	
				actividad = encabezadoIP.getActividadEspecifica();
				encabezado.add(new PreguntaDTO(ACTESPECIFICA,actividad.getDescripcion(),STRACTESPECIFICA));
				encabezado.add(new PreguntaDTO(PREGUNTA_TIPO_PERSONA_IP, tipoPersona, ""));
			
				return encabezado;
			}
			
		
	}
	
	/**
	 * Obtiene el nombre completo del Cliente utilizado por Norkom
	 * @param cliente Datos del Cliente
	 * @param tipoPersona Tipo de Persona del cliente
	 * @return Nombre(s) y Apellidos del cliente para personas fisicas, Nombre para personas Morales
	 */
	private String getNombreCompletoNorkom(ClienteDTO cliente, String tipoPersona) {
		StringBuffer nombreCompleto = new StringBuffer(StringUtils.trimToEmpty(cliente.getNombreCliente()));
		String apellidoPaternoTmp = StringUtils.trimToEmpty(cliente.getApellidoPaterno());
		String apellidoMaternoTmp = StringUtils.trimToEmpty(cliente.getApellidoMaterno());
		
		if(StringUtils.isNotBlank(apellidoPaternoTmp) && !"J".equalsIgnoreCase(tipoPersona) && !"null".equalsIgnoreCase(apellidoPaternoTmp)){
			nombreCompleto.append(' ').append(apellidoPaternoTmp);
		}
		
		if(StringUtils.isNotBlank(apellidoMaternoTmp) && !"J".equalsIgnoreCase(tipoPersona) && !"null".equalsIgnoreCase(apellidoMaternoTmp)){
			nombreCompleto.append(' ').append(apellidoMaternoTmp);
		}
		
		return nombreCompleto.toString();
	}
	
	/**
	 * Genera el mapa de Preguntas agrupandolo por su Seccion para ser mostrado como lo piden los aplicativos que lo consumen
	 * 
	 * @param preguntasBd lista de preguntas a ordenar
	 * @param cuestionario tipo de cuestionario IP o IC
	 * @return Map<String,List<PreguntaDTO>> con resultado
	 * @throws BusinessException con mensaje de error
	 */
	private Map<String, List<PreguntaDTO>> ordenarPreguntasPorSeccion(List<PreguntaDTO> preguntasBd, String cuestionario) throws BusinessException {
		
		Map<String, List<PreguntaDTO>> preguntasPorSeccion = new HashMap<String, List<PreguntaDTO>>();
		
		for(PreguntaDTO preguntaBd : preguntasBd){
			String seccion = preguntaBd.getSeccion();
			List<PreguntaDTO> preguntas = preguntasPorSeccion.get(seccion);
			if(preguntas == null){
				preguntas = new ArrayList<PreguntaDTO>();
				preguntasPorSeccion.put(seccion, preguntas);
			}
			preguntas.add(preguntaBd);
		}
		if("IP".equals(cuestionario)){
			
			if(eliminaSeccionSaldo()){
				//Removemos seccion de saldos si no esta habilitada
				preguntasPorSeccion.remove("SALP");
			}
		}
		return preguntasPorSeccion;
	}
	/**
	 * Metodo para validar si sebe de quitar o no la pregunta de saldos en IP
	 * @return boolean validacion true si se debe de quitar false en otro caso
	 * @throws BusinessException ocn mensaje de error de la busqueda
	 */
	private boolean eliminaSeccionSaldo() throws BusinessException {
		boolean valida=true;
			//valida si el parametro esta en 1 que es activo...
		ParametroDTO dto= parametrosDAO.obtenerParametroPorNombre("PREG_SALDO_PDF");
		if(dto!=null && "1".equals(dto.getValor())){
			valida=false;
		}
			
		return valida;
	}
	/**
	 * metodo para la agrupacion de preguntas por la seccion a la que pertenecen
	 * @param listaAgrupada lista de preguntas a ordenar en el mapa
	 * @param cuestionario tipo de cuestionario para conocer su ordenamiento
	 * @param cuestionarioDTO dto con propiedades para obtener las secciones
	 * @return Lista ordenada segun las secciones
	 * @throws BusinessException con mensaje de error
	 */
	private List<PreguntaDTO> agruparPorSeccion(
			Map<String, List<PreguntaDTO>> listaAgrupada, String cuestionario,
			CuestionarioWSDTO cuestionarioDTO) throws BusinessException {
		List<PreguntaDTO> listaOrdenada= new ArrayList<PreguntaDTO>();
		
		List<String> secciones = obtenerSeccionesPorTipoPersona(cuestionarioDTO.getTipoPersona(), cuestionarioDTO.getSubtipoPersona(),cuestionario);
		
		for(String seccion : secciones){
			if(listaAgrupada.get(seccion) !=null){
				listaOrdenada.addAll(listaAgrupada.get(seccion) );
			}
		}
		return listaOrdenada;
	}
	/**
	 * obtener las preguntas divididas por las secciones del cuestionario,segun el tipo de persona, subtipo y cuestionario
	 * @param tipoPersona el tipo de persona
	 * @param subtipoPersona subtipo de persona
	 * @param cuestionario tipo de cuestionario
	 * @return lista de strings con las secciones
	 * @throws BusinessException con mensaje de error
	 */
	public List<String> obtenerSeccionesPorTipoPersona(String tipoPersona, String subtipoPersona,String cuestionario)
	throws BusinessException {
		List<String> secciones = new ArrayList<String>();
		
		ParametroDTO parametroSecciones = null;
		if("IP".equals(cuestionario)){
			if(GeneradorCuestionarioGenerico.IND_PERSONA_MORAL.equals(tipoPersona)) {
				parametroSecciones = parametrosDAO.obtenerParametroPorNombre(GeneradorCuestionarioGenerico.PARAM_SECCION_IP_JN);
			} else if(GeneradorCuestionarioGenerico.IND_PERSONA_FISICA.equals(tipoPersona) && GeneradorCuestionarioGenerico.IND_PERSONA_CON_ACT_EMP.equals(subtipoPersona)) {
				parametroSecciones = parametrosDAO.obtenerParametroPorNombre(GeneradorCuestionarioGenerico.PARAM_SECCION_IP_FS);
			} else if(GeneradorCuestionarioGenerico.IND_PERSONA_FISICA.equals(tipoPersona)) {
				parametroSecciones = parametrosDAO.obtenerParametroPorNombre(GeneradorCuestionarioGenerico.PARAM_SECCION_IP_FN);
			}
		}else if ("IC".equals(cuestionario)){
			if(GeneradorCuestionarioGenerico.IND_PERSONA_MORAL.equals(tipoPersona)) {
				parametroSecciones = parametrosDAO.obtenerParametroPorNombre(GeneradorCuestionarioGenerico.PARAM_SECCION_IC_JN);
			}else if(GeneradorCuestionarioGenerico.IND_PERSONA_FISICA.equals(tipoPersona) && GeneradorCuestionarioGenerico.IND_PERSONA_CON_ACT_EMP.equals(subtipoPersona)) {
				parametroSecciones = parametrosDAO.obtenerParametroPorNombre(GeneradorCuestionarioGenerico.PARAM_SECCION_IC_FS);
			} else if(GeneradorCuestionarioGenerico.IND_PERSONA_FISICA.equals(tipoPersona)) {
				parametroSecciones = parametrosDAO.obtenerParametroPorNombre(GeneradorCuestionarioGenerico.PARAM_SECCION_IC_FN);
			}
		}
		if(parametroSecciones != null && parametroSecciones.getValor() != null && !parametroSecciones.getValor().isEmpty()) {
			String[] seccionesSeparadas = parametroSecciones.getValor().split(",");
			
			for(String seccion : seccionesSeparadas){
				secciones.add(seccion.trim());
			}
		}
		
		return secciones;
	}

	/**
	 * @return the parametrosDAO
	 */
	public ParametrosDAO getParametrosDAO() {
		return parametrosDAO;
	}

	/**
	 * @param parametrosDAO 
	 * el parametrosDAO a agregar
	 */
	public void setParametrosDAO(ParametrosDAO parametrosDAO) {
		this.parametrosDAO = parametrosDAO;
	}
}
