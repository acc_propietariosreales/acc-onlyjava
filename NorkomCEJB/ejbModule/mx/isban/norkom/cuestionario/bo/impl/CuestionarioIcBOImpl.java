/**
 * Isban Mexico
 *   Clase: CuestionarioICBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.ActividadEconomicaBO;
import mx.isban.norkom.cuestionario.bo.ClientesBO;
import mx.isban.norkom.cuestionario.bo.ConsultaCuestionarioPRBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioIcBO;
import mx.isban.norkom.cuestionario.bo.ManejadorValoresIcBO;
import mx.isban.norkom.cuestionario.bo.PaisesBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.bo.PersonasBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIcBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIpBO;
import mx.isban.norkom.cuestionario.bo.ProcesadorCalificacionBO;
import mx.isban.norkom.cuestionario.bo.ValidadorContratacionMismoDiaBO;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioIc;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.UtilCuestionariosBack;
import mx.isban.norkom.cuestionario.util.ValidadorCuestionarioIC;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CuestionarioIcBOImpl extends GeneradorCuestionarioIc implements CuestionarioIcBO {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 711568534886938711L;
	/**Logger de bo*/
	private static final Logger LOG=Logger.getLogger(CuestionarioIpBOImpl.class);
	
	/**
	 * PreguntasDAO dao para obtener las preguntas
	 */
	@EJB
	private transient PreguntasIcBO preguntasIcBO;
	
	/**
	 * CuestionarioDAO dao para obtener los datos del cuestionario
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	/**
	 * AdministradorClavesDAO dao para obtener los datos del administrador de claves
	 */
	@EJB
	private transient AdministradorClavesDAO administradorClavesDAO;
	
	/**
	 * ParametrosBO bo para obtener los parametros de BD
	 */
	@EJB
	private transient ParametrosBO parametrosBO;
	
	/**
	 * ActividadEconomicaBO bo para obtener las actividad economica
	 */
	@EJB
	private transient ActividadEconomicaBO actividadEconomicaBO;
	
	/**
	 * Objeto para manejar los valores(respuestas) que pudieran tener los campos
	 */
	@EJB
	private transient ManejadorValoresIcBO manejadorValoresIcBO;
	
	/**
	 * Objeto para obtener las respuetas de un cuestionario
	 */
	@EJB
	private transient ConsultaCuestionarioPRBO consultaRespuestasBO;
	
	/**
	 * Objeto para obtener los cuestionarios llenados el mismo dia, por la misma aplicacion, mismo nivel y por el mismo cliente
	 */
	@EJB
	private transient ValidadorContratacionMismoDiaBO validadorContratacion;
	
    /**
     * PreguntasIpBO bo para obtener las preguntas del IP
     */
	@EJB
    private transient PreguntasIpBO preguntasIpBO;
    
    /**
     * clientesBO bo para obtener los datos del cliente
     */
	@EJB
    private transient ClientesBO clientesBO;
    /**
     * ProcesadorCalificacionBO bo para obtener los datos de caliifcacion
     */
	@EJB
    private transient ProcesadorCalificacionBO procesadorCalificacionBO;
	
    /**
     * CuestionarioIcBO bo para obtener los datos del cuestionario IC
     */
	@EJB
    private transient CuestionarioIcBO cuestionariosIcBO;
	
	/**
     * PersonasBO bo para obtener los datos de personas
     */
	@EJB
    private transient PersonasBO personasBO;
	/***
	 * EJB para obtener el pais
	 */
	@EJB
	private transient PaisesBO paisesBO;
	
	@Override
	public CuestionarioDTO obtenerTipoCuestionario(List<CuestionarioRespuestaDTO> respuestas,int claveClienteCuestionario,int claveCuestionario,boolean rsvalidacion,ArchitechSessionBean psession)
	throws BusinessException {
		LOG.info("Entramos a BO CuestionariosBOImpl.obtenerTipoCuestionario");
		
		//Se utiliza para la calificacion en Norkom y la actualizacion en Personas
		CuestionarioDTO clienteCuestionario = 
				cuestionariosIcBO.obtenerDatosGeneralesCuestionarioPorClaveCC(claveClienteCuestionario, getArchitechBean());
		
		ClienteDTO clienteDTO = clientesBO.obtenerClientePorClaveClteCuest(claveClienteCuestionario, getArchitechBean());
		clienteCuestionario.setClaveClienteCuestionario(claveClienteCuestionario);
		clienteCuestionario.setClaveCuestionario(claveCuestionario);
		clienteCuestionario.setCliente(clienteDTO);
		if(rsvalidacion){
			clienteCuestionario.setRegSimp(rsvalidacion);
		}
		preguntasIpBO.guardarRespuestasCuestionarioIp(clienteCuestionario, respuestas, getArchitechBean());
		
		// Se realiza la calificacion del cliente y posteriormente se ejecuta la actualizacion en Personas
		//if(!rsvalidacion){
		debug("codigo pais Nac:" + clienteCuestionario.getCodigoNacionalidad());
			clienteCuestionario = procesadorCalificacionBO.
				calificarCliente(clienteDTO.getCodigoCliente(), clienteCuestionario, getArchitechBean());
		
			personasBO.ejecutarOperacionesEnPersonas(clienteCuestionario, getArchitechBean());
			
			if(clienteCuestionario.getNivelRiesgo() == null) {
				throw new BusinessException(Mensajes.ERCCIP13.getCodigo());
			}
			if(clienteCuestionario.getNivelRiesgo() == null || clienteCuestionario.getNivelRiesgo().isEmpty()) {
				throw new BusinessException(Mensajes.ERCCIP13.getCodigo());
			}
		//}
		// Se obtiene para saber el tipo de persona, subtipo de persona y divisa con la que fue generado el IP
		//Con esa informacion y el Nivel de Riesgo se va a obtener el IC
		CuestionarioDTO cuestionarioIpBD = cuestionariosIcBO.
				obtenerTipoCuestionarioPorClave(claveCuestionario,0, getArchitechBean());
		
		if(cuestionarioIpBD.getTipoPersona() == null || cuestionarioIpBD.getTipoPersona().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP08.getCodigo());
		}
		
		if(cuestionarioIpBD.getSubtipoPersona() == null || cuestionarioIpBD.getSubtipoPersona().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP09.getCodigo());
		}
		
		if(cuestionarioIpBD.getDivisa() == null || cuestionarioIpBD.getDivisa().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP10.getCodigo());
		}
		
		CuestionarioDTO tipoCuestionarioIC = cuestionarioDAO.obtenerTipoCuestionario(cuestionarioIpBD, clienteCuestionario.getNivelRiesgo(), psession);
		
		if(tipoCuestionarioIC != null && tipoCuestionarioIC.getClaveCuestionario() != 0) {
			tipoCuestionarioIC.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		} else {
			tipoCuestionarioIC = new CuestionarioDTO();
			tipoCuestionarioIC.setCodError(Mensajes.ERROR_NO_DATOS.getCodigo());
		}
		
		UtilCuestionariosBack.complementarCuestionarioIc(tipoCuestionarioIC, claveClienteCuestionario, clienteCuestionario);
		tipoCuestionarioIC.setCliente(clienteDTO);
		
		PaisDTO paisNacionalidad = paisesBO.obtenerPaisPorCodigo(tipoCuestionarioIC.getCodigoNacionalidad(), psession);
		if(paisNacionalidad != null){
			tipoCuestionarioIC.setNombrePais(paisNacionalidad.getNombre());
			tipoCuestionarioIC.setCodigoPais(String.valueOf(paisNacionalidad.getIdPais()));
		}
		tipoCuestionarioIC.setCodigoPaisResidencia(clienteCuestionario.getCodigoPais());
		PaisDTO paisNacionalidadRes = paisesBO.obtenerPaisPorCodigo(tipoCuestionarioIC.getCodigoPaisResidencia(), psession);
		if(paisNacionalidadRes != null){
			tipoCuestionarioIC.setNombrePaisResidencia(paisNacionalidadRes.getNombre());
		}
		
		LOG.info("finaliza a BO CuestionariosIcBOImpl.obtenerTipoCuestionario");
		return tipoCuestionarioIC;
	}
	
	@Override
	public CuestionarioDTO obtenerTipoCuestionarioPorClave(int claveCuestionario,int claveClienteCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		CuestionarioDTO cuestionario = cuestionarioDAO.obtenerTipoCuestionarioPorClave(claveCuestionario, psession);
		
		if(cuestionario != null && cuestionario.getClaveCuestionario() != 0) {
			cuestionario.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		} else {
			cuestionario = new CuestionarioDTO();
			cuestionario.setCodError(Mensajes.ERROR_NO_DATOS.getCodigo());
		}
		if(claveClienteCuestionario>0){
			CuestionarioDTO clienteCuestionario = 
				cuestionariosIcBO.obtenerDatosGeneralesCuestionarioPorClaveCC(claveClienteCuestionario, getArchitechBean());
			
			UtilCuestionariosBack.complementarCuestionarioIc(cuestionario, claveClienteCuestionario, clienteCuestionario);
			
			cuestionariosIcBO.asignarClaveCuestionarioIC(claveClienteCuestionario, claveCuestionario, getArchitechBean());
		}
		LOG.info("finaliza a BO CuestionariosIcBOImpl.obtenerTipoCuestionarioPorClave");
		return cuestionario;
	}
	
	@Override
	public CuestionarioDTO obtenerDatosGeneralesCuestionarioPorClaveCC(int claveClienteCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		return cuestionarioDAO.obtenerDatosGeneralesCuestionarioPorClaveCC(claveClienteCuestionario, psession);
	}
	
	@Override
	public StringBuilder obtenerCuestionarioIcHtml(CuestionarioDTO requestCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		StringBuilder cuestionarioHtml = new StringBuilder();
		
		//Reasignar valor de respuesta para PPEP
		if(GeneradorCuestionarioGenerico.NIVEL_RIESGO_A2.equals(requestCuestionario.getNivelRiesgo()) 
				&& GeneradorCuestionarioGenerico.IND_UPLD_KYC_PEP.equals(requestCuestionario.getIndicadorUpld())) 
		{
			manejadorValoresIcBO.actualizarValorPEPAfirmativo(requestCuestionario, psession);
		}
		
		requestCuestionario = manejadorValoresIcBO.definirPEP(requestCuestionario, psession);
		
		Map<String, List<PreguntaDTO>> preguntas = obtenerPreguntasPorSeccion(requestCuestionario, psession);
		requestCuestionario.setPreguntasPorSeccion(preguntas);
		
		ActividadEconomicaDTO actividadEspecifica = 
				actividadEconomicaBO.obtenerActividadEconomica(requestCuestionario.getCodigoActEspecifica(), psession);
		requestCuestionario.setActividadEspecifica(actividadEspecifica);
		
		cuestionarioHtml.append(generarCuestionario(requestCuestionario, psession));
		
		return cuestionarioHtml;
	}
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasCuestionarioIc(int claveCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		LOG.info("inicia a BO CuestionariosBOImpl.obtenerCuestionario");
		
		return preguntasIcBO.obtenerPreguntasCuestionarioPorClave(claveCuestionario, psession);
	}
	
	/**
	 * @param cuestionarioPeticion datos de cuestionario para la busqueda
	 * @param psession ArchitechSessionBean bean de session
	 * @return Map<String,List<PreguntaDTO>> mapa con las preguntas obtenidas
	 * @throws BusinessException con mensaje de error
	 */
	private Map<String, List<PreguntaDTO>> obtenerPreguntasPorSeccion(CuestionarioDTO cuestionarioPeticion, ArchitechSessionBean psession)
	throws BusinessException {
		List<PreguntaDTO> preguntasBd = preguntasIcBO.obtenerPreguntasCuestionarioPorClave(cuestionarioPeticion.getClaveCuestionario(), psession);
		Map<String, List<PreguntaDTO>> preguntasPorSeccion = new HashMap<String, List<PreguntaDTO>>();
		for(PreguntaDTO preguntaBd : preguntasBd){
			String seccion = preguntaBd.getSeccion();
			List<PreguntaDTO> preguntas = preguntasPorSeccion.get(seccion);
			if(preguntas == null){
				preguntas = new ArrayList<PreguntaDTO>();
				preguntasPorSeccion.put(seccion, preguntas);
			}
			
			preguntas.add(preguntaBd);
		}
		
		return preguntasPorSeccion;
	}

	@Override
	public StringBuilder generarSeccion(CuestionarioDTO datosCuestionario, String seccion, ArchitechSessionBean session)
	throws BusinessException {
		if(datosCuestionario == null){
			throw new BusinessException(Mensajes.ERCCIP11.getCodigo());
		}
		
		Map<String, List<PreguntaDTO>> preguntasPorSeccion = datosCuestionario.getPreguntasPorSeccion();
		if(preguntasPorSeccion == null || preguntasPorSeccion.isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP12.getCodigo());
		}
		
		Map<String, PreguntaDTO> respuestas = new HashMap<String, PreguntaDTO>();
		List<CuestionarioDTO> cuestionariosMismoDia = validadorContratacion.productosContratadosMismoDia(
				datosCuestionario.getCodigoCliente(), datosCuestionario.getNivelRiesgo(), datosCuestionario.getIdCuestionario(), session);
		if(cuestionariosMismoDia != null && !cuestionariosMismoDia.isEmpty()) {
			String numeroContrato = cuestionariosMismoDia.get(0).getIdCuestionario();
			
			respuestas.putAll(consultaRespuestasBO.obtenerRespuestasICPorAbreviatura(numeroContrato, session));
		}
		
		StringBuilder seccionHtml = new StringBuilder();
		List<PreguntaDTO> preguntas = preguntasPorSeccion.get(seccion);
		if(preguntas != null && !preguntas.isEmpty()){
			String tituloPregunta = preguntas.get(0).getTitulo();
			
			agregarInicioSeccion(seccionHtml, seccion, tituloPregunta);
			
			for(PreguntaDTO pregunta : preguntas){
				agregarInicioPregunta(seccionHtml, pregunta);
				
				PreguntaDTO respuesta = respuestas.get(pregunta.getAbreviatura());
				String valor = StringUtils.EMPTY;
				if(respuesta != null && respuesta.getOpcionSeleccionada() > 0) {
					valor = String.valueOf(respuesta.getOpcionSeleccionada());
				} else if(respuesta != null && respuesta.getRespuesta() != null && !respuesta.getRespuesta().isEmpty()) {
					valor = respuesta.getRespuesta();
				}
				
				seccionHtml.append(manejadorValoresIcBO.agregarPregunta(datosCuestionario, pregunta, valor, session));
				
				agregarFinPregunta(seccionHtml);
				LOG.info("BO.datosCuestionario.hasFamiliarPep():"+datosCuestionario.hasFamiliarPep());
				ValidadorCuestionarioIC.agregarValidacionesEspeciales(datosCuestionario, pregunta, seccionHtml);
			}
			
			agregarFinSeccion(seccionHtml);
		}
			
		return seccionHtml;
	}
	
	@Override
	public List<String> obtenerSeccionesICPorTipoPersona(String tipoPersona, String subtipoPersona, ArchitechSessionBean session)
	throws BusinessException {
		List<String> secciones = new ArrayList<String>();
		
		ParametroDTO parametroSecciones = null;
		if(IND_PERSONA_MORAL.equals(tipoPersona)) {
			parametroSecciones = parametrosBO.obtenerParametroPorNombre(PARAM_SECCION_IC_JN, session);
		} else if(IND_PERSONA_FISICA.equals(tipoPersona) && IND_PERSONA_CON_ACT_EMP.equals(subtipoPersona)) {
			parametroSecciones = parametrosBO.obtenerParametroPorNombre(PARAM_SECCION_IC_FS, session);
		} else if(IND_PERSONA_FISICA.equals(tipoPersona)) {
			parametroSecciones = parametrosBO.obtenerParametroPorNombre(PARAM_SECCION_IC_FN, session);
		}
		
		if(parametroSecciones != null && parametroSecciones.getValor() != null && !parametroSecciones.getValor().isEmpty()) {
			String[] seccionesSeparadas = parametroSecciones.getValor().split(",");
			
			for(String seccion : seccionesSeparadas){
				secciones.add(seccion.trim());
			}
		}
		
		return secciones;
	}
	
	@Override
	public void asignarClaveCuestionarioIC(int claveClienteCuest, int claveCuestIC, ArchitechSessionBean psession) throws BusinessException {
		administradorClavesDAO.asignarClaveCuestionarioIC(claveClienteCuest, claveCuestIC, psession);
	}
}
