/**
 * Isban Mexico
 *   Clase: CuestionarioICPRBOImpl.java
 *   Descripcion: Componente para realizar 
 *   la logica de negocio del cuestionario IC
 *   en envio de relacionados por 
 *   bloque.
 *
 *   Control de Cambios:
 *   1.0  May 07, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.ClientesBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioIcBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioIcPRBO;
import mx.isban.norkom.cuestionario.bo.PaisesBO;
import mx.isban.norkom.cuestionario.bo.PersonasBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIpBO;
import mx.isban.norkom.cuestionario.bo.ProcesadorCalificacionBO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.UtilCuestionariosBack;

import org.apache.log4j.Logger;

/**
 * CuestionarioIcPRBOImpl extiende de
 *  CuestionarioIcBOImpl e implementa CuestionarioIcPRBO
 *  
 * BO que sirve para la nueva funcionalidad de
 * agregar relacionado a un cuestionario
 * el envio se realiza por bloques y por eso 
 * se obtiene solo la nformacion del IC A1
 * cuando aun no se cumple con todos los
 * relacionados en OK
 * @author Leopoldo F Espinosa R 07/05/2017
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CuestionarioIcPRBOImpl extends CuestionarioIcBOImpl implements CuestionarioIcPRBO {

	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = -5051565939218029828L;
	/**
	 * Logger de bo
	 **/
	private static final Logger LOG=Logger.getLogger(CuestionarioIcPRBOImpl.class);
    /**
     * CuestionarioIcBO bo para obtener 
     * los datos del cuestionario IC
     */
	@EJB
    private transient CuestionarioIcBO cuestionariosIcBO;
    /**
     * clientesBO bo para obtener los 
     * datos del cliente
     */
	@EJB
    private transient ClientesBO clientesBO;
    /**
     * PreguntasIpBO bo para obtener 
     * las preguntas del IP
     */
	@EJB
    private transient PreguntasIpBO preguntasIpBO;
	
	/**
	 * CuestionarioDAO dao para obtener los datos del cuestionario
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	/***
	 * EJB para obtener el pais
	 */
	@EJB
	private transient PaisesBO paisesBO;
    /**
     * ProcesadorCalificacionBO bo para obtener los datos de caliifcacion
     */
	@EJB
    private transient ProcesadorCalificacionBO procesadorCalificacionBO;
	/**
     * PersonasBO bo para obtener los datos de personas
     */
	@EJB
    private transient PersonasBO personasBO;
	
	
	@Override
	public CuestionarioDTO obtenerTipoCuestionarioA1(
			List<CuestionarioRespuestaDTO> respuestas,
			int claveClienteCuestionario, int claveCuestionario,
			boolean rsvalidacion,int intentos,boolean grabado, ArchitechSessionBean architechBean)throws BusinessException {
		LOG.info("Entramos a BO CuestionarioICPRBOImpl.obtenerTipoCuestionarioA1");
		
		//Se utiliza para la calificacion en Norkom y la actualizacion en Personas
		CuestionarioDTO dtoCuestionarioPR = 
				cuestionariosIcBO.obtenerDatosGeneralesCuestionarioPorClaveCC(claveClienteCuestionario, getArchitechBean());
		
		ClienteDTO clienteDTO = clientesBO.obtenerClientePorClaveClteCuest(claveClienteCuestionario, getArchitechBean());
		dtoCuestionarioPR.setClaveClienteCuestionario(claveClienteCuestionario);
		dtoCuestionarioPR.setClaveCuestionario(claveCuestionario);
		dtoCuestionarioPR.setCliente(clienteDTO);
		if(rsvalidacion){
			dtoCuestionarioPR.setRegSimp(rsvalidacion);
		}
		if(intentos==0){
			preguntasIpBO.guardarRespuestasCuestionarioIp(dtoCuestionarioPR, respuestas, getArchitechBean());
		}
		// Se realiza la calificacion del cliente y posteriormente se ejecuta la actualizacion en Personas
		debug("codigo pais Nac:" + dtoCuestionarioPR.getCodigoNacionalidad());
		dtoCuestionarioPR = agregaNivelRiesgo(grabado, dtoCuestionarioPR,
				clienteDTO);
		// Se obtiene para saber el tipo de persona, subtipo de persona y divisa con la que fue generado el IP
		//Con esa informacion y el Nivel de Riesgo se va a obtener el IC
		CuestionarioDTO cuestionarioDtoIc = cuestionariosIcBO.
				obtenerTipoCuestionarioPorClave(claveCuestionario,0, getArchitechBean());
		
		lanzaMensajeError(cuestionarioDtoIc);
		
		CuestionarioDTO tipoCuestionarioIC = cuestionarioDAO.obtenerTipoCuestionario(cuestionarioDtoIc, dtoCuestionarioPR.getNivelRiesgo(), architechBean);
		
		tipoCuestionarioIC = validaMensajeError(tipoCuestionarioIC);
		
		UtilCuestionariosBack.complementarCuestionarioIc(tipoCuestionarioIC, claveClienteCuestionario, dtoCuestionarioPR);
		tipoCuestionarioIC.setCliente(clienteDTO);
		
		tipoCuestionarioIC=agregaDatosPais(architechBean, dtoCuestionarioPR, tipoCuestionarioIC);
		
		LOG.info("finaliza a BO CuestionariosIcBOImpl.obtenerTipoCuestionario");
		return tipoCuestionarioIC;
	}


	/**
	 * Metodo auxiliar para validar 
	 * si se envia alguna excepcion dependiendo de
	 * las validacions de datos
	 * @param cuestionarioDtoIc CuestionarioDTO
	 * @throws BusinessException con mensaje de la validacion
	 */
	private void lanzaMensajeError(CuestionarioDTO cuestionarioDtoIc) throws BusinessException {
		if(cuestionarioDtoIc.getTipoPersona() == null || cuestionarioDtoIc.getTipoPersona().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP08.getCodigo());
		}
		
		if(cuestionarioDtoIc.getSubtipoPersona() == null || cuestionarioDtoIc.getSubtipoPersona().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP09.getCodigo());
		}
		
		if(cuestionarioDtoIc.getDivisa() == null || cuestionarioDtoIc.getDivisa().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP10.getCodigo());
		}
	}


	/**
	 * Metodo auxiliar para agregar el nivel de 
	 * riesgo del cliente
	 * @param grabado
	 * @param dtoCuestionarioPR
	 * @param clienteDTO
	 * @return
	 * @throws BusinessException
	 */
	private CuestionarioDTO agregaNivelRiesgo(boolean grabado,
			CuestionarioDTO dtoCuestionarioPR, ClienteDTO clienteDTO)
			throws BusinessException {
		CuestionarioDTO dtoCuestionarioPRin=dtoCuestionarioPR;
		if(grabado){
			debug("codigo pais Nac:" + dtoCuestionarioPRin.getCodigoNacionalidad());
			dtoCuestionarioPRin = procesadorCalificacionBO.
				calificarCliente(clienteDTO.getCodigoCliente(), dtoCuestionarioPRin, getArchitechBean());
		
			personasBO.ejecutarOperacionesEnPersonas(dtoCuestionarioPRin, getArchitechBean());
			
			if(dtoCuestionarioPRin.getNivelRiesgo() == null) {
				throw new BusinessException(Mensajes.ERCCIP13.getCodigo());
			}
			if(dtoCuestionarioPRin.getNivelRiesgo() == null || dtoCuestionarioPRin.getNivelRiesgo().isEmpty()) {
				throw new BusinessException(Mensajes.ERCCIP13.getCodigo());
			}
		}else{
			dtoCuestionarioPRin.setNivelRiesgo("A1");
			dtoCuestionarioPRin.setIndicadorUpld("NORMAL");
		}
		return dtoCuestionarioPRin;
	}


	/**
	 * Metodo auxiliar para agregar los datos de 
	 * Nacionalidad y pais de recidencia
	 * @param architechBean ArchitechSessionBean
	 * @param dtoCuestionarioPR  CuestionarioDTO
	 * @param tipoCuestionarioIC CuestionarioDTO
	 * @return CuestionarioDTO con los datos de pais
	 * @throws BusinessException
	 */
	private CuestionarioDTO agregaDatosPais(ArchitechSessionBean architechBean,
			CuestionarioDTO dtoCuestionarioPR,
			CuestionarioDTO tipoCuestionarioIC) throws BusinessException {
		CuestionarioDTO tipoCuestionarioICin=tipoCuestionarioIC;
		PaisDTO paisNacionalidad = paisesBO.obtenerPaisPorCodigo(tipoCuestionarioICin.getCodigoNacionalidad(), architechBean);
		if(paisNacionalidad != null){
			tipoCuestionarioICin.setNombrePais(paisNacionalidad.getNombre());
			tipoCuestionarioICin.setCodigoPais(String.valueOf(paisNacionalidad.getIdPais()));
		}
		tipoCuestionarioICin.setCodigoPaisResidencia(dtoCuestionarioPR.getCodigoPais());
		PaisDTO paisNacionalidadRes = paisesBO.obtenerPaisPorCodigo(tipoCuestionarioICin.getCodigoPaisResidencia(), architechBean);
		if(paisNacionalidadRes != null){
			tipoCuestionarioICin.setNombrePaisResidencia(paisNacionalidadRes.getNombre());
		}
		return tipoCuestionarioICin;
	}


	/**
	 * Metodo de ayuda para la validacion del
	 * mensaje de error validando si el DTO
	 * es nulo o viene un mensaje de error
	 * @param tipoCuestionarioIC CuestionarioDTO
	 * @return CuestionarioDTO
	 */
	private CuestionarioDTO validaMensajeError(CuestionarioDTO tipoCuestionarioIC) {
		CuestionarioDTO tipoCuestionarioICIn=tipoCuestionarioIC;
		if(tipoCuestionarioICIn != null && tipoCuestionarioICIn.getClaveCuestionario() != 0) {
			tipoCuestionarioICIn.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		} else {
			tipoCuestionarioICIn = new CuestionarioDTO();
			tipoCuestionarioICIn.setCodError(Mensajes.ERROR_NO_DATOS.getCodigo());
		}
		return tipoCuestionarioICIn;
	}

	
}
