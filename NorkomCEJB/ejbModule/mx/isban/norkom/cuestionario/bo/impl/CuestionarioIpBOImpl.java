/**
 * Isban Mexico
 *   Clase: CuestionarioIPBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del cuestionario IP.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestOpcionesPregunta;
import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.bo.CuestionarioIpBO;
import mx.isban.norkom.cuestionario.bo.GeneradorCatalogosBO;
import mx.isban.norkom.cuestionario.bo.OpcionesPreguntaBO;
import mx.isban.norkom.cuestionario.bo.PaisesBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.dao.ActividadEconomicaDAO;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.dao.ClienteDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.EstadoDAO;
import mx.isban.norkom.cuestionario.dao.MunicipioDAO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.EstadoDTO;
import mx.isban.norkom.cuestionario.dto.MunicipioDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioIp;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.manejador.ManejadorRadio;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.ValidadorCuestionarioIP;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CuestionarioIpBOImpl extends GeneradorCuestionarioIp implements CuestionarioIpBO {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 711568534886938711L;
	/**Logger de bo*/
	private static final Logger LOG=Logger.getLogger(CuestionarioIpBOImpl.class);
	
	/**
	 * CuestionarioDAO cuestionarioDAO
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	/**
	 * Variable utilizada para declarar preguntasDAO
	 */
	@EJB
	private transient PreguntasDAO preguntasDAO;
	
	/**
	 * AdministradorClavesDAO administradorClavesDAO
	 */
	@EJB
	private transient AdministradorClavesDAO administradorClavesDAO;
	
	/**
	 * Dao de Cliente 
	 */
	@EJB
	private transient ClienteDAO clienteDAO;
	
	/**
	 * OpcionesPreguntaBO opcionesPreguntaBO
	 */
	@EJB
	private transient OpcionesPreguntaBO opcionesPreguntaBO;
	
	/**
	 * PaisesBO bo para obtener los paises
	 */
	@EJB
	private transient PaisesBO paisesBO;
	
	/**
	 * GeneradorCatalogosBO bo de generador de ctalogos
	 */
	@EJB
	private transient GeneradorCatalogosBO generadorCatalogosBO;
	
	/**
	 * EstadoDAO dao para los estados
	 */
	@EJB
	private transient EstadoDAO estadoDAO;
	
	/**
	 * MunicipioDAO dao para los municipios
	 */
	@EJB
	private transient MunicipioDAO municipioDAO;
	
	/**
	 * ActividadEconomicaDAO dao para la actividad economica
	 */
	@EJB
	private transient ActividadEconomicaDAO actividadEconomicaDAO;
	
	/**
	 * ParametrosBO bo para obtener los parametros
	 */
	@EJB
	private transient ParametrosBO parametrosBO;
	
	@Override
	public CuestionarioDTO agregarCuestionarioCliente(CuestionarioDTO cuestionarioDTO, ArchitechSessionBean sesion)
	throws BusinessException {
		ClienteDTO cliente = cuestionarioDTO.getCliente();
		ClienteDTO clienteBd = clienteDAO.obtenerClientePorBuc(cliente.getCodigoCliente(), sesion);
		boolean insertarCuestionario = false;
		
		CuestionarioWSDTO cuestionarioWsDTO = 
				cuestionarioDAO.obtenerDatosGeneralesCuestionarioPorId(cuestionarioDTO.getIdCuestionario(), sesion);
		
		if(cuestionarioWsDTO != null && cuestionarioWsDTO.getNumeroContrato() != null && !cuestionarioWsDTO.getNumeroContrato().isEmpty()) {
			throw new BusinessException(Mensajes.ERAGCT01.getCodigo(),Mensajes.ERAGCT01.getDescripcion() );
		}
		
		if(cuestionarioWsDTO != null && cuestionarioWsDTO.getClaveCuestionarioIP() > 0 && cuestionarioWsDTO.getIdClienteAsignado() > 0) {
			throw new BusinessException(Mensajes.ERAGCT02.getCodigo(),Mensajes.ERAGCT02.getDescripcion() );
		}
		
		if(cuestionarioWsDTO != null && cuestionarioWsDTO.getClaveCuestionario() > 0) {
			/*
			 * Para la clave deberia obtener con getClaveClienteCuestionario pero en la 
			 * obtencion de la informacion se define el valor en ClaveCuestionario
			 */
			cuestionarioDTO.setClaveClienteCuestionario(cuestionarioWsDTO.getClaveCuestionario());
		} else {
			int claveClienteCuestionario = administradorClavesDAO.obtenerNuevaClaveClienteCuestionario();
			cuestionarioDTO.setClaveClienteCuestionario(claveClienteCuestionario);
			insertarCuestionario = true;
		}
		
		ActividadEconomicaDTO actividadEspecifica = actividadEconomicaDAO.obtenerActividadEconomica(cuestionarioDTO.getCodigoActEspecifica());
		if(actividadEspecifica == null) {
			throw new BusinessException(Mensajes.ERAGCT06.getCodigo(),Mensajes.ERAGCT06.getDescripcion());
		}
		try{
		cuestionarioDTO.setActividadEspecifica(actividadEspecifica);
		ActividadEconomicaDTO actividadGenerica = actividadEconomicaDAO.obtenerActividadEconomicaGenerica(cuestionarioDTO.getCodigoActGenerica());
		if(actividadGenerica != null) {
			cuestionarioDTO.setActividadGenerica(actividadGenerica);
		}
		}catch(BusinessException e){
			error("error al obtener la actividad generica se ira con la que viene de pantalla.."+e.getMessage());
		}
		CuestionarioDTO cuestionarioBD = cuestionarioDAO.obtenerTipoCuestionario(cuestionarioDTO, ID_CUESTIONARIO_IP, sesion);
		if(cuestionarioBD == null){
			throw new BusinessException(Mensajes.ERAGCT07.getCodigo(),Mensajes.ERAGCT07.getDescripcion());
		}
		PaisDTO paisDTO= paisesBO.obtenerPaisPorCodigo(cuestionarioDTO.getCodigoPais(), getArchitechBean());
		if(paisDTO==null){
			throw new BusinessException(Mensajes.ERAGCT08.getCodigo(),Mensajes.ERAGCT08.getDescripcion());
		}
		cuestionarioDTO.setNombrePais(paisDTO.getNombre());
		cuestionarioDTO.setClaveCuestionario(cuestionarioBD.getClaveCuestionario());
		cuestionarioDTO.setDescripcion(cuestionarioBD.getDescripcion());
		cuestionarioDTO.setFechaCreacion(cuestionarioBD.getFechaCreacion());
		cuestionarioDTO.setNombreCuestionario(cuestionarioBD.getNombreCuestionario());
		cuestionarioDTO.setVersionCuestionario(cuestionarioBD.getVersionCuestionario());
		
		
		if(clienteBd == null || clienteBd.getIdCliente() == 0){
			int idCliente = clienteDAO.obtenerNuevoIdCliente();
			cliente.setIdCliente(idCliente);
			
			cuestionarioDAO.agregarCuestionarioClienteNuevo(cuestionarioDTO, insertarCuestionario, sesion);
		} else {
			cliente.setIdCliente(clienteBd.getIdCliente());
			cuestionarioDAO.agregarCuestionarioClienteExistente(cuestionarioDTO, insertarCuestionario, sesion);
		}
		
		return cuestionarioDTO;
	}
	
	@Override
	public CuestionarioDTO complementarInformacionCuestionario(CuestionarioDTO cuestionarioDTO1, ArchitechSessionBean sesion)
	throws BusinessException {
		
		CuestionarioDTO cuestionarioDTO = agregarCuestionarioCliente(cuestionarioDTO1, getArchitechBean());
		
		PaisDTO paisResidencia = paisesBO.obtenerPaisPorCodigo(cuestionarioDTO.getCodigoPais(), sesion);
		if(paisResidencia != null){
			cuestionarioDTO.setNombrePais(paisResidencia.getNombre());
			cuestionarioDTO.setPaisBajoRiesgo(paisResidencia.getIndicadorRiesgo());
		}
		
		if(COD_PAIS_MX.equals(cuestionarioDTO.getCodigoPais())){
			EstadoDTO entidad=null;
			if(cuestionarioDTO.getCodigoMunicipio()!=null && cuestionarioDTO.getCodigoMunicipio().length()>6){
				entidad = estadoDAO.obtenerEstadoPorCodigo(cuestionarioDTO.getCodigoMunicipio().substring(5), sesion);
			}
			if(entidad != null){
				cuestionarioDTO.setNombreEntidad(entidad.getNombre());
			}
			
			MunicipioDTO municipio = null;
			if(cuestionarioDTO.getCodigoMunicipio()!=null && cuestionarioDTO.getCodigoMunicipio().length()>6){
				municipio=municipioDAO.obtenerMunicipioPorCodigo(cuestionarioDTO.getCodigoMunicipio().substring(0,5),cuestionarioDTO.getCodigoMunicipio().substring(5), sesion);
			}
					
			if(municipio != null){
				cuestionarioDTO.setNombreMunicipio(municipio.getNombre());
			}
		} else {
			cuestionarioDTO.setNombreEntidad(IND_NO_APLICA);
			cuestionarioDTO.setNombreMunicipio(IND_NO_APLICA);
		}
		
		PaisDTO paisNacionalidad = paisesBO.obtenerPaisPorCodigo(cuestionarioDTO.getCodigoNacionalidad(), sesion);
		if(paisNacionalidad != null){
			cuestionarioDTO.setNombreNacionalidad(paisNacionalidad.getNombre());
		}
		
		return cuestionarioDTO;
	}
	
	@Override
	public CuestionarioDTO obtenerTipoCuestionario(CuestionarioDTO peticion, ArchitechSessionBean psession)
	throws BusinessException {
		LOG.info("Entramos a BO CuestionariosBOImpl.obtenerTipoCuestionario");
		if(peticion.getTipoPersona() == null || peticion.getTipoPersona().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP08.getCodigo());
		}
		
		if(peticion.getSubtipoPersona() == null || peticion.getSubtipoPersona().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP09.getCodigo());
		}
		
		if(peticion.getDivisa() == null || peticion.getDivisa().isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP10.getCodigo());
		}
		
		CuestionarioDTO cuestionario = cuestionarioDAO.obtenerTipoCuestionario(peticion, ID_CUESTIONARIO_IP, psession);
		
		if(cuestionario != null && cuestionario.getClaveCuestionario() != 0){
			cuestionario.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		}else{
			cuestionario = new CuestionarioDTO();
			cuestionario.setCodError(Mensajes.ERROR_NO_DATOS.getCodigo());
		}
		
		LOG.info("finaliza a BO CuestionariosBOImpl.obtenerTipoCuestionario");
		return cuestionario;
	}
	
	@Override
	public StringBuilder obtenerCuestionarioIpHtml(CuestionarioDTO requestCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		StringBuilder cuestionarioHtml = new StringBuilder();
		
		Map<String, List<PreguntaDTO>> preguntas = obtenerPreguntasPorSeccion(requestCuestionario, getArchitechBean());
		requestCuestionario.setPreguntasPorSeccion(preguntas);
		
		cuestionarioHtml.append(generarCuestionario(requestCuestionario, psession));
		
		return cuestionarioHtml;
	}
	
	/**
	 * @param peticion CuestionarioDTO con datos para la busqueda
	 * @param psession ArchitechSessionBean de session
	 * @return Map<String,List<PreguntaDTO>> con resultado
	 * @throws BusinessException con mensaje de error
	 */
	private Map<String, List<PreguntaDTO>> obtenerPreguntasPorSeccion(CuestionarioDTO peticion, ArchitechSessionBean psession)
	throws BusinessException {
		List<PreguntaDTO> preguntasBd = preguntasDAO.obtenerPreguntasCuestionarioPorClave(peticion.getClaveCuestionario(), psession);
		Map<String, List<PreguntaDTO>> preguntasPorSeccion = new HashMap<String, List<PreguntaDTO>>();
		
		for(PreguntaDTO preguntaBd : preguntasBd){
			String seccion = preguntaBd.getSeccion();
			List<PreguntaDTO> preguntas = preguntasPorSeccion.get(seccion);
			if(preguntas == null){
				preguntas = new ArrayList<PreguntaDTO>();
				preguntasPorSeccion.put(seccion, preguntas);
			}
			
			preguntas.add(preguntaBd);
		}
		boolean seccionSaldo=validaSeccionSaldo(psession);
		if(seccionSaldo){
			//Removemos seccion de saldos si no esta habilitada
			preguntasPorSeccion.remove("SALP");
		}
		return preguntasPorSeccion;
	}
	/**
	 * Metodo para validar si sebe de quitar o no la pregunta de saldos en IP
	 * @param sessionBean ArchitechSessionBean de agave
	 * @return validacion true si se debe de quitar false en otro caso
	 */
	private boolean validaSeccionSaldo(ArchitechSessionBean sessionBean) {
			boolean valida=true;
			//valida si el parametro esta en 1 que es activo...
			try {
				ParametroDTO dto= parametrosBO.obtenerParametroPorNombre("PREG_SALDO_PDF", sessionBean);
				if(dto!=null && "1".equals(dto.getValor())){
					valida=false;
				}
			} catch (BusinessException e) {
				LOG.error(e);
			}
			return valida;
	}

	@Override
	public StringBuilder generarSeccion(CuestionarioDTO datosCuestionario, String seccion, ArchitechSessionBean session)
	throws BusinessException {
		if(datosCuestionario == null){
			throw new BusinessException(Mensajes.ERCCIP11.getCodigo());
		}
		
		Map<String, List<PreguntaDTO>> preguntasPorSeccion = datosCuestionario.getPreguntasPorSeccion();
		if(preguntasPorSeccion == null || preguntasPorSeccion.isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP12.getCodigo());
		}
		
		StringBuilder seccionHtml= new StringBuilder();
		List<PreguntaDTO> preguntas = preguntasPorSeccion.get(seccion);
		
		if(preguntas != null && !preguntas.isEmpty()){
			String tituloPregunta = preguntas.get(0).getTitulo();
			
			agregarInicioSeccion(seccionHtml, tituloPregunta);
			for(PreguntaDTO pregunta : preguntas){
				agregarInicioPregunta(seccionHtml, pregunta);
				
				seccionHtml.append(agregarPregunta(datosCuestionario, pregunta, session));
				
				seccionHtml.append(TD_CERRAR);
				seccionHtml.append(TR_CERRAR);
				seccionHtml.append(TABLE_CERRAR);
				seccionHtml.append(DIV_CERRAR); //DIV_INICIO_PREGUNTAS
				
				ValidadorCuestionarioIP.agregarValidacionesEspeciales(datosCuestionario, pregunta, seccionHtml);
			}
			agregarFinSeccion(seccionHtml);
		}
			
		return seccionHtml;
	}
	
	@Override
	public StringBuilder generarSeccionTransaccionalidad(CuestionarioDTO datosCuestionario, String seccion, ArchitechSessionBean session)
	throws BusinessException {
		if(datosCuestionario == null){
			throw new BusinessException(Mensajes.ERCCIP11.getCodigo());
		}
		
		Map<String, List<PreguntaDTO>> preguntasPorSeccion = datosCuestionario.getPreguntasPorSeccion();
		if(preguntasPorSeccion == null || preguntasPorSeccion.isEmpty()){
			throw new BusinessException(Mensajes.ERCCIP12.getCodigo());
		}
		
		StringBuilder seccionHtmlTransaccionalidad= new StringBuilder();
		List<PreguntaDTO> preguntas = preguntasPorSeccion.get(seccion);
		
		if(preguntas != null && !preguntas.isEmpty()){
			String tituloPregunta = preguntas.get(0).getTitulo();
			
			int contadorPreguntas = 1;
			agregarInicioSeccion(seccionHtmlTransaccionalidad, tituloPregunta);
			for(PreguntaDTO pregunta : preguntas){
				if(contadorPreguntas % 2 != 0) {
					seccionHtmlTransaccionalidad.append(String.format(DIV_INICIO_PREGUNTA, ID_DIV_PREGUNTA, pregunta.getAbreviatura()));
					seccionHtmlTransaccionalidad.append(TABLE_INICIO);
					seccionHtmlTransaccionalidad.append(String.format(TR_TITULO_PREGUNTA, pregunta.getIdPregunta(), pregunta.getIdPregunta()));
				}
				
				seccionHtmlTransaccionalidad.append(TD_INICIO_32);
				seccionHtmlTransaccionalidad.append(String.format("%s&nbsp;", pregunta.getPregunta()));
				seccionHtmlTransaccionalidad.append(TD_CERRAR);
				
				seccionHtmlTransaccionalidad.append(TD_INICIO);
				seccionHtmlTransaccionalidad.append(agregarPregunta(datosCuestionario, pregunta, session));
				seccionHtmlTransaccionalidad.append(TD_CERRAR);
				
				if(contadorPreguntas % 2 == 0) {
					seccionHtmlTransaccionalidad.append(TR_CERRAR);
					seccionHtmlTransaccionalidad.append(TABLE_CERRAR);
					seccionHtmlTransaccionalidad.append(DIV_CERRAR); //DIV_INICIO_PREGUNTAS
				}
				contadorPreguntas++;
			}
			agregarFinSeccion(seccionHtmlTransaccionalidad);
		}
			
		return seccionHtmlTransaccionalidad;
	}

	/**
	 * @param cuestionario datos de el cuestionario de tipo CuestionarioDTO
	 * @param pregunta datos de la pregunta a agregar en objeto de tipo PreguntaDTO
	 * @param session ArchitechSessionBean bean de session
	 * @return StringBuilder con la pregunta
	 * @throws BusinessException con mensaje de error
	 */
	private StringBuilder agregarPregunta(CuestionarioDTO cuestionario, PreguntaDTO pregunta, ArchitechSessionBean session)
	throws BusinessException {
		StringBuilder preguntaHtml = new StringBuilder();
		
		if(CAMPO_LIBRE.equals(pregunta.getTipoPregunta())){
			preguntaHtml.append(ManejadorInput.generarCampoLibre(pregunta, new AtributosTagHtmlDTO(), StringUtils.EMPTY));
		} else if(CAMPO_RADIO.equals(pregunta.getTipoPregunta())){
			RequestOpcionesPregunta peticion = new RequestOpcionesPregunta();
			peticion.setClaveGrupo(pregunta.getClaveGrupoPregunta());
			ResponseOpcionesPregunta response = opcionesPreguntaBO.obtenerOpcionesPregunta(peticion, getArchitechBean());
			
			preguntaHtml.append(ManejadorRadio.generarRadios(pregunta, response, new AtributosTagHtmlDTO()));
		} else if(CAMPO_SELECT.equals(pregunta.getTipoPregunta()) || CAMPO_SELECT_MUL.equals(pregunta.getTipoPregunta()) || CAMPO_SELECT_AUT.equals(pregunta.getTipoPregunta()))
		{
			RequestOpcionesPregunta peticion = new RequestOpcionesPregunta();
			peticion.setClaveGrupo(pregunta.getClaveGrupoPregunta());
			
			if(CAMPO_CAT_PAIS.equals(pregunta.getDescripcionGrupoPregunta())) {
				preguntaHtml.append(generadorCatalogosBO.generarCatalogoPaises(pregunta, session));
			} else if(TAG_CAT_OR_REC.equals(pregunta.getDescripcionGrupoPregunta())) {
				preguntaHtml.append(generadorCatalogosBO.generarCatalogoRecursos(cuestionario, pregunta, IND_ORIGEN_RECURSO, session));
			} else if(TAG_CAT_DT_REC.equals(pregunta.getDescripcionGrupoPregunta())) {
				preguntaHtml.append(generadorCatalogosBO.generarCatalogoRecursos(cuestionario, pregunta, IND_DESTINO_RECURSO, session));
			} else {
				ResponseOpcionesPregunta response = opcionesPreguntaBO.obtenerOpcionesPregunta(peticion, getArchitechBean());
				if(Mensajes.OPERACION_EXITOSA.getCodigo().equals(response.getCodError())){
					preguntaHtml.append(ManejadorSelect.generarSelect(pregunta, response, StringUtils.EMPTY, new AtributosTagHtmlDTO()));
				}
			}
		}
		
		return preguntaHtml;
	}
	
	@Override
	public List<String> obtenerSeccionesIPPorTipoPersona(String tipoPersona, String subtipoPersona, ArchitechSessionBean session)
	throws BusinessException {
		List<String> secciones = new ArrayList<String>();
		
		ParametroDTO parametroSecciones = null;
		if(IND_PERSONA_MORAL.equals(tipoPersona)) {
			parametroSecciones = parametrosBO.obtenerParametroPorNombre(PARAM_SECCION_IP_JN, session);
		} else if(IND_PERSONA_FISICA.equals(tipoPersona) && IND_PERSONA_CON_ACT_EMP.equals(subtipoPersona)) {
			parametroSecciones = parametrosBO.obtenerParametroPorNombre(PARAM_SECCION_IP_FS, session);
		} else if(IND_PERSONA_FISICA.equals(tipoPersona)) {
			parametroSecciones = parametrosBO.obtenerParametroPorNombre(PARAM_SECCION_IP_FN, session);
		}
		
		if(parametroSecciones != null && parametroSecciones.getValor() != null && !parametroSecciones.getValor().isEmpty()) {
			String[] seccionesSeparadas = parametroSecciones.getValor().split(",");
			
			for(String seccion : seccionesSeparadas){
				secciones.add(seccion.trim());
			}
		}
		
		return secciones;
	}
}
