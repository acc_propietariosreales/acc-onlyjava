/**
 * Isban Mexico
 *   Clase: CuestionarioLightHouseBOImpl.java
 *   Descripcion: Componente para guardar la informacion enviada por LightHouse para obtener la calificacion de Norkom.
 *
 *   Control de Cambios:
 *   1.0 May 23, 2016 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.CuestionarioIpBO;
import mx.isban.norkom.cuestionario.bo.CuestionarioLightHouseBO;
import mx.isban.norkom.cuestionario.bo.ManejadorValoresIcBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.bo.PersonasBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIpBO;
import mx.isban.norkom.cuestionario.bo.ProcesadorCalificacionBO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.NivelRiesgoDAO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseRequest;
import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseResponse;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.manejador.ManejadorRadio;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;
import mx.isban.norkom.cuestionario.util.Mensajes;

/**
 * @author J Ulises Garcia R 23/05/2016
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CuestionarioLightHouseBOImpl extends Architech implements CuestionarioLightHouseBO {
	/**
	 * UID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = -5780202679897736267L;

	/**Logger de bo*/
	private static final Logger LOG=Logger.getLogger(CuestionarioLightHouseBOImpl.class);
	
	/** Constante para el indicador UPLD KYC-DR */
	private static final String KYC_DR = "KYC_DR";
	
	 /**
     * ProcesadorCalificacionBO bo para obtener los datos de caliifcacion
     */
	@EJB
    private transient ProcesadorCalificacionBO procesadorCalificacionBO;
	/***
	 * EJB para obtener el objeto de cuestionarioIp
	 */
	@EJB
	private transient CuestionarioIpBO cuestionarioIpBO;
	
	/**
     * PreguntasIpBO bo para obtener las preguntas del IP
     */
	@EJB
    private transient PreguntasIpBO preguntasIpBO;
	
	/***
	 * EJB para obtener el objeto de cuestionarioIp
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	/**
     * PersonasBO bo para obtener los datos de personas
     */
	@EJB
    private transient PersonasBO personasBO;
	
	/**
	 * Variable utilizada para declarar preguntasDAO
	 */
	@EJB
	private transient PreguntasDAO preguntasDAO;
	
	/**
	 * ParametrosBO bo para obtener los parametros de BD
	 */
	@EJB
	private transient ParametrosBO parametrosBO;
	
	/**
	 * Objeto para manejar los valores(respuestas) que pudieran tener los campos
	 */
	@EJB
	private transient ManejadorValoresIcBO manejadorValoresIcBO;
	
	/**
	 * nivelRiesgoDAO Permite actualizar el Nivel de Riesgo y el Indicador UPLD en ACC
	 */
	@EJB
	private transient NivelRiesgoDAO nivelRiesgoDAO;
	
	@Override
	public CuestionarioLightHouseResponse procesarCuestionario(CuestionarioLightHouseRequest request) {
		CuestionarioDTO cuestionario = convertirObjeto(request);
		CuestionarioLightHouseResponse response = new CuestionarioLightHouseResponse();
		response.setIdCuestionario(cuestionario.getIdCuestionario());
		try {
			cuestionario = cuestionarioIpBO.complementarInformacionCuestionario(cuestionario, getArchitechBean());
			
			List<PreguntaDTO> preguntas = preguntasDAO.obtenerPreguntasCuestionarioPorClave(cuestionario.getClaveCuestionario(), getArchitechBean());
			List<CuestionarioRespuestaDTO> respuestas = new ArrayList<CuestionarioRespuestaDTO>();
			
			for(PreguntaDTO pregunta : preguntas){
				ParametroDTO parametro = parametrosBO.
						obtenerParametroPorNombre(String.format("preg%s_"+cuestionario.getIdCuestionario().substring(0,2), pregunta.getAbreviatura()), getArchitechBean());
				CuestionarioRespuestaDTO cuestionarioRespDTO = null;
				if("INPUT".equals(pregunta.getTipoPregunta())) {
					cuestionarioRespDTO = ManejadorInput.obtenerValorCampoLibre(pregunta.getIdPregunta(), 
							parametro.getValor(), cuestionario.getClaveClienteCuestionario(), cuestionario.getClaveCuestionario());
				} else if("RADIO".equals(pregunta.getTipoPregunta())) {
					cuestionarioRespDTO = ManejadorRadio.obtenerValorRadio(pregunta.getIdPregunta(), 
							parametro.getValor(), cuestionario.getClaveClienteCuestionario(), cuestionario.getClaveCuestionario());
				} else if("SELECT".equals(pregunta.getTipoPregunta()) || "SELECT_AUT".equals(pregunta.getTipoPregunta())) {
					cuestionarioRespDTO = ManejadorSelect.obtenerValorSelectUnico(pregunta.getIdPregunta(), 
							parametro.getValor(), cuestionario.getClaveClienteCuestionario(), cuestionario.getClaveCuestionario());
				} else if("SELECT_MUL".equals(pregunta.getTipoPregunta())) {
					String[] valores = {parametro.getValor()};
					respuestas.addAll(ManejadorSelect.obtenerValorSelectMultiple(pregunta.getIdPregunta(), 
							valores, cuestionario.getClaveClienteCuestionario(), cuestionario.getClaveCuestionario()));
				}
				
				LOG.info(cuestionarioRespDTO);
				if(cuestionarioRespDTO != null){
					respuestas.add(cuestionarioRespDTO);
				}
			}
			
			preguntasIpBO.guardarRespuestasCuestionarioIp(cuestionario, respuestas, getArchitechBean());
			
			//Obtener calificación
			cuestionario = procesadorCalificacionBO.calificarCliente(cuestionario.getCodigoCliente(), cuestionario, getArchitechBean());
			
			if(cuestionario.getNivelRiesgo() == null || cuestionario.getNivelRiesgo().isEmpty()) {
				throw new BusinessException(Mensajes.ERCCIP13.getCodigo());
			}
			
			CalificacionDTO nivelRiesgo = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getNivelRiesgo());
			CalificacionDTO indicadorUpld = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getIndicadorUpld());
			response.setNivelRiesgo(String.format("%s %s", nivelRiesgo.getCodigoIndicadorPersonas(), nivelRiesgo.getValorIndicadorPersonas()));
			response.setIndicadorUpld(String.format("%s %s", indicadorUpld.getCodigoIndicadorPersonas(), indicadorUpld.getValorIndicadorPersonas()));
			
			//Actualizar resultado en personas
			cuestionario = personasBO.ejecutarOperacionesEnPersonas(cuestionario, getArchitechBean());
			
			//Reasignar valor de respuesta para PPEP
			if(GeneradorCuestionarioGenerico.NIVEL_RIESGO_A2.equals(cuestionario.getNivelRiesgo()) 
					&& GeneradorCuestionarioGenerico.IND_UPLD_KYC_PEP.equals(cuestionario.getIndicadorUpld())) 
			{
				manejadorValoresIcBO.actualizarValorPEPAfirmativo(cuestionario, getArchitechBean());
			}
			String sugerenciaNivel = obtenerMensajeCalificacion(cuestionario.getIndicadorUpld(), request.getTipoPersona());
			response.setCodigoOperacion("OKCT000");
			if(StringUtils.isNotEmpty(sugerenciaNivel)) {
				response.setMensajeOperacion(sugerenciaNivel);
			} else {
				response.setMensajeOperacion("Operación Exitosa");
			}
		} catch (BusinessException e) {
			showException(e);
			response.setCodigoOperacion(e.getCode());
			response.setMensajeOperacion(e.getMessage());
		}
		
		return response;
	}
	
	/**
	 * Convierte un objeto de tipo CuestionarioLightHouseRequest a CuestionrioDTO
	 * @param request CuestionarioLightHouseRequest de la peticion
	 * @return CuestionarioDTO con la informacion de CuestionarioLightHouseRequest
	 */
	private CuestionarioDTO convertirObjeto(CuestionarioLightHouseRequest request){
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		ClienteDTO cliente = new ClienteDTO();
		cuestionario.setCliente(cliente);
		
		ActividadEconomicaDTO actividadGenerica = new ActividadEconomicaDTO();
		ActividadEconomicaDTO actividadEspecifica = new ActividadEconomicaDTO();
		cuestionario.setActividadGenerica(actividadGenerica);
		cuestionario.setActividadEspecifica(actividadEspecifica);
		
		cuestionario.setIdCuestionario(request.getIdCuestionario());
		
		cliente.setCodigoCliente(request.getCodigoCliente());
		cliente.setIndClienteNuevo(request.getIndClienteNuevo());
		cliente.setNombreCliente(request.getNombreCliente());
		cliente.setApellidoPaterno(request.getApellidoPaterno());
		cliente.setApellidoMaterno(request.getApellidoMaterno());
		cliente.setFechaNacimiento(request.getFechaNacimiento());
		
		cuestionario.setTipoPersona(request.getTipoPersona());
		cuestionario.setSubtipoPersona(request.getSubtipoPersona());
		cuestionario.setCodigoNacionalidad(request.getCodigoPaisNacionalidad());
		cuestionario.setCodigoPais(request.getCodigoPaisResidencia());
		cuestionario.setCodigoMunicipio(request.getCodigoMunicipio());
		
		actividadGenerica.setCodigo(request.getCodigoActGenerica());
		actividadEspecifica.setCodigo(request.getCodigoActEspecifica());
		
		cuestionario.setCodigoEntidad(request.getCodigoEntidad());
		cuestionario.setCodigoSegmento(request.getCodigoSegmento());
		cuestionario.setDescSegmento(request.getDescSegmento());
		cuestionario.setCodigoProducto(request.getCodigoProducto());
		cuestionario.setDescProducto(request.getDescProducto());
		cuestionario.setCodigoSubproducto(request.getCodigoSubproducto());
		cuestionario.setDivisa(request.getDivisa());
		cuestionario.setCodigoSucursal(request.getCodigoSucursal());
		cuestionario.setNombreSucursal(request.getDescripcionSucursal());
		cuestionario.setCodigoZona(request.getCodigoZona());
		cuestionario.setNombreZona(request.getDescripcionZona());
		cuestionario.setCodigoPlaza(request.getCodigoPlaza());
		cuestionario.setNombrePlaza(request.getDescripcionPlaza());
		cuestionario.setCodigoRegion(request.getCodigoRegion());
		cuestionario.setNombreRegion(request.getDescripcionRegion());
		
		cuestionario.setCodigoBranch(StringUtils.EMPTY);
		cuestionario.setCodigoCentroCostos(StringUtils.EMPTY);
		cuestionario.setCodigoTipoFormulario(StringUtils.EMPTY);
		
		return cuestionario;
	}
	
	/**
	 * Obtiene el mensaje que se muestra como resultado con base a la calificacion de Norkom
	 * @param indicadorUpldNkm Indicador UPLD asignado al cliente
	 * @param tipoPersona Tipo de persona que es el cliente
	 * @return Mensaje con base al Indicador UPLD y al Tipo de Persona
	 */
	private String obtenerMensajeCalificacion(String indicadorUpldNkm, String tipoPersona) {
		StringBuilder sugerenciaNivel = new StringBuilder();
		if(KYC_DR.equals(indicadorUpldNkm)) {
			sugerenciaNivel.append("Requiere autorización de su Director Regional / Ejecutivo. ");
		}
		
		if("KYC-PEP".equals(indicadorUpldNkm)  || KYC_DR.equals(indicadorUpldNkm) || "KYC-CCC".equals(indicadorUpldNkm) || "KYC-BLO".equals(indicadorUpldNkm)) {
	        sugerenciaNivel.append("EL CLIENTE REQUIERE LLEVAR A CABO VISITA DOMICILIARIA. ");
		}
		
		if("F".equals(tipoPersona) && (KYC_DR.equals(indicadorUpldNkm) || "KYC-CCC".equals(indicadorUpldNkm) || "KYC-BLO".equals(indicadorUpldNkm))) {
			sugerenciaNivel.append(
			"Se requiere adjuntar dentro del expediente del cliente copia del RFC y/o Formato BCOM-563 \"Manifestación de Actividad\".");
		}
		
		return sugerenciaNivel.toString();
	}
}
