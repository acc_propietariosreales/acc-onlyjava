/**
 * Isban Mexico
 *   Clase: EnvioRelacionadosBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio 
 *   de la informacion para el envio de relacionados
 *    actualizacion de estatus por norkom.
 *
 *   Control de Cambios:
 *   1.0 Jul 03, 2017 Stefanini - Creacion
 *   1.1 14/07/2017 Revision de sonar
 */

package mx.isban.norkom.cuestionario.bo.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerRelacionados;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerRelacionados;
import mx.isban.norkom.cuestionario.bo.EnvioRelacionadosBO;
import mx.isban.norkom.cuestionario.dao.EnvioRelacionadosDAO;
import mx.isban.norkom.cuestionario.util.EstatusRelacionados;
import mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO;

import org.apache.log4j.Logger;
/**
 * Bo princical para el envio de 
 * relacionados por bloque
 * EnvioRelacionadosBOImpl extien 
 * de Architech
 * implementa EnvioRelacionados 
 * 
 * los metodos principales son:
 * obtenerEstatusRelacionados(RequestObtenerRelacionados, ArchitechSessionBean)
 * consultaEstatusRelacionadosNorkom(RequestDatosRelacionadosWSDTO, String)
 * actualizaEstatusRelacionadosErr(RequestObtenerRelacionados, ArchitechSessionBean)
 * 
 * @author lespinosa
 * 1.0 jul 03, 2017 Stefanini - Creacion
 * 2.0 jul 14, 2017 Stefanini - Modificacion por revision de sonar
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class EnvioRelacionadosBOImpl extends Architech implements EnvioRelacionadosBO {

	/**
	 * Variable utilizada para 
	 * declarar serialVersionUID
	 * de tipo long
	 */
	private static final long serialVersionUID = 7330812517373659350L;
	
	/**
	 * Variable utilizada para escribir 
	 * el log de la aplicacion
	 */
	private static final Logger LOG = Logger.getLogger(EnvioRelacionadosBOImpl.class);
	
	/**
	 * Dao para obtener los relacionados
	 */
	@EJB
	private transient EnvioRelacionadosDAO envioRelacionadosDAO;
	
	@Override
	public ResponseObtenerRelacionados obtenerEstatusRelacionados(RequestObtenerRelacionados request, ArchitechSessionBean psession) throws BusinessException{
		
		LOG.info("Ingresa al metodo obtenerRelacionados de la clase EnvioRelacionadosBOImpl.");
		ResponseObtenerRelacionados responseestatus= new ResponseObtenerRelacionados();
		//Ejecuta select para saber el estatus de la relacion en base al idCuestionario y bloque
		List<ResponseObtenerRelacionados> response = envioRelacionadosDAO.obtenerRelacionados(request, psession);
		if(response!=null && response.isEmpty()){
			//no hay relacionados o todos son OK
			responseestatus.setEstatusRelacion(EstatusRelacionados.OK.toString());
			return responseestatus;
		}
		
		//Recorre los registros de la lista de relacionados obtenidos de la consulta anterior
		//separamos por bloques
		Map<String,ResponseObtenerRelacionados> separadosxBloque=creaMapaRelacionados(response);
		int bloques=separadosxBloque.size();
		int bloquesOk=0;
		Set<String> llaves=separadosxBloque.keySet();
		for (String keySet : llaves) {
			info(String.format("Bloque [%s] esta con estatus: [%s]",separadosxBloque.get(keySet).getNumeroBloque(),separadosxBloque.get(keySet).getEstatusRelacion()));
			if(EstatusRelacionados.OK.toString().equals(separadosxBloque.get(keySet).getEstatusRelacion())){
				bloquesOk++;
			}
		}
		if(bloquesOk==bloques){
			responseestatus.setEstatusRelacion(EstatusRelacionados.OK.toString());
		}else{
			responseestatus.setEstatusRelacion(EstatusRelacionados.NO_OK.toString());
		}		
		
		return responseestatus;
		
	}
	/**
	 * Crea un mapa de los relacionado por bloque
	 * si es que hay varios bloques
	 * y realizar las validaciones
	 * requeridas
	 * @param response Lista de relacionados
	 * @return Map<String, ResponseObtenerRelacionados>
	 */
	private Map<String, ResponseObtenerRelacionados> creaMapaRelacionados(List<ResponseObtenerRelacionados> response) {
		Map<String, ResponseObtenerRelacionados>  xmapa=new HashMap<String, ResponseObtenerRelacionados>();
			for (ResponseObtenerRelacionados dtoin:response) {
				if(!xmapa.containsKey(dtoin.getNumeroBloque())){
					xmapa.put(dtoin.getNumeroBloque(), dtoin);
				}
			}
		return xmapa;
	}


	@Override
	public void actualizaEstatusRelacionadosErr(
			RequestObtenerRelacionados request, ArchitechSessionBean psession)
			throws BusinessException {
		RequestDatosRelacionadosWSDTO entrada = new RequestDatosRelacionadosWSDTO();
		entrada.setIdCuestionario(request.getIdCuestionario());
		entrada.setNumeroBloque(request.getBloque());
		envioRelacionadosDAO.actualizarEstatusErr(entrada, psession);
		debug("Se realizo el update a err de los relacionados");
	}
	@Override
	public void actualizarEstatusRel(RequestDatosRelacionadosWSDTO request,
			String estatus, ArchitechSessionBean psession)
			throws BusinessException {
		debug("actualizarEstatusRel:"+ request.getIdCuestionario());
		envioRelacionadosDAO.actualizarEstatusRel(request, estatus, psession);
		
	}


}
