/**
 * Isban Mexico
 *   Clase: GeneraXmlDAO.java
 *   Descripcion: BOImpl para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.GeneraXmlBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.dao.GeneraXmlDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioIPDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesXmlDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;
import mx.isban.norkom.cuestionario.util.CreaFormatoXML;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

import org.apache.commons.lang.StringUtils;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class GeneraXmlBOImpl extends Architech implements GeneraXmlBO{

	/** Variable utilizada para declarar serialVersionUID */
	private static final long serialVersionUID = 2050863297656053350L;
	
	/** Busca Constancia Dividendos DAO */
	@EJB 
	private transient GeneraXmlDAO generarXmlDAO;
	
	/** Objeto para acceder a los parametros de la BD */
	@EJB
	private transient ParametrosBO parametrosBO;
	
	@Override
	public String generarEstructuraXML(String idCuestionario, ArchitechSessionBean psession) throws BusinessException {
		debug("### -> GeneraXmlBOImpl.generarEstructuraXML()###, Dato de entrada: id_cuestionario= " + idCuestionario);
		
		// Obtener encabezado de formularioIP, valores unicos
		CuestionarioIPDTO formularioIP = generarXmlDAO.obtenerFormularioIP(idCuestionario, psession);
		// Obtener encabezado de formularioIP, valores multiples
		List<OpcionesXmlDTO> opcionesIP = generarXmlDAO.obtenerOpcionesIP(idCuestionario, psession);		
		// Obtener recursos de formularioIP
		List<RecursoDTO> recursosIP = generarXmlDAO.obtenerRecursosIP(idCuestionario, psession);
		// Obtener relacionados de formularioIP
		List<RelacionadoDTO> relacionadosIP = generarXmlDAO.obtenerRelacionadosIP(idCuestionario, psession);
		// Obtener paises de Transferencias Internacionales
		List<PaisDTO> paisesTransInter = generarXmlDAO.obtenerPaisTransInter(idCuestionario, psession);
		
		/** GENERAR LA ESTRUCTURA XML **/
		try {
			CreaFormatoXML formatoXML= new CreaFormatoXML(); 
			
			formatoXML.setLimiteIntervinientes(obtenerLimite(PARAM_LIMITE_INTERVINIENTES, psession));
			
			formatoXML.setLimiteBeneficiarios(obtenerLimite(PARAM_LIMITE_BENEFICIARIOS, psession));
			
			formatoXML.setLimiteAcionistas(obtenerLimite(PARAM_LIMITE_ACCIONISTAS, psession));
			
			formatoXML.setLimiteProveedores(obtenerLimite(PARAM_LIMITE_PROVEEDORES, psession));
			
			//Se retorna la estructura XML en formato String
			debug("codigoNacionalidad:"+formularioIP.getCodigoNacionalidad());
			return formatoXML.creaFormatoXML(formularioIP, opcionesIP, recursosIP, relacionadosIP, paisesTransInter);
			
		} catch (ParserConfigurationException pce) {
			showException(pce);
		} catch (TransformerException tfe) {
			showException(tfe);
		}
		return StringUtils.EMPTY;
	}
	
	/**
	 * Obtiene de la base de datos el limite de referencias maximas que se deben manejar
	 * @param parametroLimite Nombre del parametro en la base de datos
	 * @param psession Ojbeto de sesion de Agave
	 * @return Limite de la referencias especificada
	 */
	private int obtenerLimite(String parametroLimite, ArchitechSessionBean psession) {
		try {
			ParametroDTO paramLimite = parametrosBO.obtenerParametroPorNombre(parametroLimite, psession);
			
			if(paramLimite != null) {
				return Integer.parseInt(paramLimite.getValor());
			} else {
				throw new BusinessException(Mensajes.ERRXML01.getCodigo(),Mensajes.ERRXML01.getDescripcion());
			}
		} catch(NumberFormatException nfe) {
			showException(nfe);
			error(String.format("No fue posible convertir el parametro[%s] a numero entero", parametroLimite));
		} catch(BusinessException be) {
			showException(be);
			error(String.format("No fue posible obtener el parametro[%s]", parametroLimite));
		}
		
		return LIMITE_MAXIMO_RELACIONADOS;
	}
	

	
	
	
	

}