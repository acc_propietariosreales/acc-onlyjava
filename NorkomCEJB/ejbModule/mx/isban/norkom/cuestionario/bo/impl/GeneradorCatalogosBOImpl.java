/**
 * Isban Mexico
 *   Clase:GeneradorCatalogosBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de los catalogos como paises y recursos.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.BeanResponsePaises;
import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.bo.GeneradorCatalogosBO;
import mx.isban.norkom.cuestionario.bo.OrigenDestinoRecursosBO;
import mx.isban.norkom.cuestionario.bo.PaisesBO;
import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class GeneradorCatalogosBOImpl extends Architech implements GeneradorCatalogosBO {
	/**
	 * String IND_PERSONA_CON_ACT_EMP
	 */
	public static final String IND_PERSONA_CON_ACT_EMP = "S";
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -2678393213247764419L;
	
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(GeneradorCatalogosBOImpl.class);
	
	/**
	 * PaisesBO bo para la obtencion de paises
	 */
	@EJB
	private transient PaisesBO paisesBO;
	
	/**
	 * BO para obtener el origen y destino de recursos
	 */
	@EJB
	private transient OrigenDestinoRecursosBO recursosBO;
	
	@Override
	public StringBuilder generarCatalogoPaises(PreguntaDTO pregunta, ArchitechSessionBean session) throws BusinessException {
		StringBuilder paisesHtml = new StringBuilder();
		ResponseOpcionesPregunta opcionesResponse = new ResponseOpcionesPregunta();
		List<OpcionesPreguntaDTO> opciones = new ArrayList<OpcionesPreguntaDTO>();
		opcionesResponse.setLstOpcionesPreguntas(opciones);
		LOG.info("Inicia generarCatalogoPaises");
		BeanResponsePaises response = paisesBO.obtenerPaises(session);
		if(response != null && response.getLstPaises() != null){
			for (PaisDTO pais : response.getLstPaises()) {
				OpcionesPreguntaDTO opcion = new OpcionesPreguntaDTO();
				opcion.setIdOpcion(String.valueOf(pais.getIdPais()));
				opcion.setDescripcion(pais.getNombre());
				opcion.setValor(pais.getCodigo());
				
				opciones.add(opcion);
			}
		}
		
		paisesHtml.append(ManejadorSelect.generarSelect(pregunta, opcionesResponse, StringUtils.EMPTY, new AtributosTagHtmlDTO()));
		
		return paisesHtml;
	}
	
	@Override
	public StringBuilder generarCatalogoRecursos(
			CuestionarioDTO cuestionario, PreguntaDTO pregunta, String tipoRecurso, ArchitechSessionBean session) 
	throws BusinessException {
		StringBuilder recursosHtml = new StringBuilder();
		ResponseOpcionesPregunta opcionesResponse = new ResponseOpcionesPregunta();
		List<OpcionesPreguntaDTO> opciones = new ArrayList<OpcionesPreguntaDTO>();
		opcionesResponse.setLstOpcionesPreguntas(opciones);
		LOG.info("Inicia generarCatalogoRecursos");
		String tipoPersona;
		if(IND_PERSONA_CON_ACT_EMP.equals(cuestionario.getSubtipoPersona())){
			tipoPersona = "E";
		} else {
			tipoPersona = cuestionario.getTipoPersona();
		}
		
		List<RecursoDTO> recursos = recursosBO.obtenerRecursos(tipoPersona, tipoRecurso, session);
		
		if(recursos != null && !recursos.isEmpty()){
			for (RecursoDTO recurso : recursos) {
				OpcionesPreguntaDTO opcion = new OpcionesPreguntaDTO();
				opcion.setIdOpcion(String.valueOf(recurso.getClaveRecurso()));
				opcion.setDescripcion(recurso.getDescripcion());
				opcion.setValor(recurso.getCodigoRecurso());
				
				opciones.add(opcion);
			}
		}
		
		recursosHtml.append(ManejadorSelect.generarSelect(pregunta, opcionesResponse, StringUtils.EMPTY, new AtributosTagHtmlDTO()));
		
		return recursosHtml;
	}

}
