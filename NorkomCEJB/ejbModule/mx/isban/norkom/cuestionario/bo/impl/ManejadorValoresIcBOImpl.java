/**
 * Isban Mexico
 *   Clase: CuestionarioICBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestOpcionesPregunta;
import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.bo.AdministradorOrigenDestinoRecursosBO;
import mx.isban.norkom.cuestionario.bo.AdministradorRelacionadosBO;
import mx.isban.norkom.cuestionario.bo.ManejadorValoresIcBO;
import mx.isban.norkom.cuestionario.bo.OpcionesPreguntaBO;
import mx.isban.norkom.cuestionario.bo.PersonasBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIcBO;
import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.ResponseODF3DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE71DTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.manejador.ManejadorRadio;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;
import mx.isban.norkom.cuestionario.util.GeneradorAuxiliarHtmlIC;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.ValidadorReferenciasBancariasIC;
import mx.isban.norkom.cuestionario.util.ValidadorReferenciasFisicasIC;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ManejadorValoresIcBOImpl extends Architech implements ManejadorValoresIcBO {
	/**
	 * ManejadorValoresIcBOImpl.java  de tipo long
	 */
	private static final long serialVersionUID = 4021972375901624711L;

	/**
	 * 
	 */
	private static final Logger LOG=Logger.getLogger(ManejadorValoresIcBOImpl.class);
	
	/**
	 * PreguntasDAO dao para obtener las preguntas de IC
	 */
	@EJB
	private transient PreguntasIcBO preguntasIcBO;
	
	/**
	 * OpcionesPreguntaBO bo para aaceder a las opciones de las preguntas
	 */
	@EJB
	private transient OpcionesPreguntaBO opcionesPreguntaBO;
	
	/**
	 * PaisesBO Objeto para el manejo de las operaciones con BO
	 */
	@EJB
	private transient PersonasBO personasBO;
	
	/**
	 * AdministradorOrigenDestinoRecursosBO bo para obtener los datos relacionados
	 */
	@EJB
	private transient AdministradorRelacionadosBO adminRelacionadosBO;
	
	/**
	 * AdministradorOrigenDestinoRecursosBO bo para obetenr el origen y destino de los recursos
	 */
	@EJB
	private transient AdministradorOrigenDestinoRecursosBO adminOrigenDestRecursosBO;
	
	@Override
	public StringBuilder agregarPregunta(CuestionarioDTO cuestionario, PreguntaDTO pregunta, String valor, ArchitechSessionBean session)
	throws BusinessException {
		LOG.info(String.format("Se va a procesar la pregunta [%s]", pregunta.getAbreviatura()));
		StringBuilder preguntaHtml = new StringBuilder();
		
		//Seccion para generar campo html tipo INPUT
		if(GeneradorCuestionarioGenerico.CAMPO_LIBRE.equals(pregunta.getTipoPregunta())) {
			String valorTexto;
			if(valor != null && !valor.isEmpty()) {
				valorTexto = valor;
			} else {
				valorTexto = obtenerValorPredefinidoInput(cuestionario, pregunta, session);
			}
			
			AtributosTagHtmlDTO atributos = new AtributosTagHtmlDTO();
			if("JUST".equals(pregunta.getAbreviatura())) {
				atributos.setHabilitado(false);
			}
			
			preguntaHtml.append(ManejadorInput.generarCampoLibre(pregunta, atributos, valorTexto));
		} else if(GeneradorCuestionarioGenerico.CAMPO_RADIO.equals(pregunta.getTipoPregunta())) {
			//Seccion para generar campo html tipo RADIOBUTTON
			RequestOpcionesPregunta peticion = new RequestOpcionesPregunta();
			peticion.setClaveGrupo(pregunta.getClaveGrupoPregunta());
			ResponseOpcionesPregunta response = opcionesPreguntaBO.obtenerOpcionesPregunta(peticion, session);
			
			if("PTSP".equals(pregunta.getAbreviatura())){
				List<RelacionadoDTO> accionistas = adminRelacionadosBO.obtenerRelacionadosPorClaveClienteCuestTipo(
						cuestionario.getClaveClienteCuestionario(), AdministradorRelacionadosBO.TIPO_RELACION_ACCIONISTA, session);
				LOG.debug(String.format(
						"Total de accionistas [%s - %s] para [%s]", 
						accionistas == null, (accionistas != null ? accionistas.size() : 0), cuestionario.getIdCuestionario()));
				
				GeneradorAuxiliarHtmlIC.generarTablaAccionistas(accionistas, preguntaHtml);
			} else {
				preguntaHtml.append(ManejadorRadio.generarRadios(pregunta, response, valor, new AtributosTagHtmlDTO()));
			}
			
		} else if(GeneradorCuestionarioGenerico.CAMPO_SELECT.equals(pregunta.getTipoPregunta()) || 
				GeneradorCuestionarioGenerico.CAMPO_SELECT_MUL.equals(pregunta.getTipoPregunta()) ||
				GeneradorCuestionarioGenerico.CAMPO_SELECT_AUT.equals(pregunta.getTipoPregunta()) ) 
		{//Seccion para generar campo html tipo SELECT
			RequestOpcionesPregunta peticion = new RequestOpcionesPregunta();
			peticion.setClaveGrupo(pregunta.getClaveGrupoPregunta());
			
			ResponseOpcionesPregunta response = opcionesPreguntaBO.obtenerOpcionesPregunta(peticion, session);
			if(Mensajes.OPERACION_EXITOSA.getCodigo().equals(response.getCodError())){
				preguntaHtml.append(ManejadorSelect.generarSelect(pregunta, response, valor, new AtributosTagHtmlDTO()));
			}
		} if(GeneradorCuestionarioGenerico.TAG_PREG_OR_REC_IC.equals(pregunta.getTipoPregunta())) {
			//Seccion para generar las preguntas de ORIGEN y DESTINO
			CuestionarioRespuestaDTO preguntaOrigenRecurso = 
					preguntasIcBO.obtenerPreguntaPorSeccionAbreviatura(cuestionario.getIdCuestionario(), "ODRC", "ORRC", session);
			
			//Se crea objeto para que realice la consulta, la consulta no regresa resultados. Se hace de esta manera para poder continuar el flujo
			if(preguntaOrigenRecurso == null) {
				preguntaOrigenRecurso = new CuestionarioRespuestaDTO();
			}
			
			LOG.info(String.format("PreguntaOrigenRecurso es Nulo[%s] TipoPersona[%s] subtipoPersona[%s] Opcion[%s]", 
					preguntaOrigenRecurso == null, cuestionario.getTipoPersona(), cuestionario.getSubtipoPersona(), preguntaOrigenRecurso.getValorOpcion()));
			
			List<PreguntaDTO> preguntas = adminOrigenDestRecursosBO.obtenerPreguntasPorRespuesta(
					cuestionario.getTipoPersona(), cuestionario.getSubtipoPersona(), preguntaOrigenRecurso.getValorOpcion(), session);
			
			Map<String, ResponseOpcionesPregunta> opcionesPreguntasOrigenRec = new HashMap<String, ResponseOpcionesPregunta>();
			for(PreguntaDTO preguntaDTO : preguntas){
				if(GeneradorCuestionarioGenerico.CAMPO_SELECT.equals(preguntaDTO.getTipoPregunta()) || 
						GeneradorCuestionarioGenerico.CAMPO_SELECT_MUL.equals(preguntaDTO.getTipoPregunta()) ||
						GeneradorCuestionarioGenerico.CAMPO_RADIO.equals(preguntaDTO.getTipoPregunta())) 
				{
					RequestOpcionesPregunta peticion = new RequestOpcionesPregunta();
					peticion.setClaveGrupo(preguntaDTO.getClaveGrupoPregunta());
					
					opcionesPreguntasOrigenRec.put(preguntaDTO.getAbreviatura(), opcionesPreguntaBO.obtenerOpcionesPregunta(peticion, session));
					
				}
			}
			GeneradorAuxiliarHtmlIC.agregarPreguntasOrigenRecursos(preguntaHtml, preguntas, opcionesPreguntasOrigenRec);
			
		}
		
		return preguntaHtml;
	}
	
	/**
	 * Obtiene el valor predefinido para un campo de tipo Input
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param pregunta Pregunta de la que se desea obtener el valor
	 * @param session Objeto de sesion de Agave
	 * @return Valor predeterminado para la pregunta
	 * @throws BusinessException En caso de un error con la informacion o en la consulta de BD
	 */
	public String obtenerValorPredefinidoInput(CuestionarioDTO cuestionario, PreguntaDTO pregunta, ArchitechSessionBean session)
	throws BusinessException {
		LOG.info(String.format("Se va a procesar la pregunta [%s]", pregunta.getAbreviatura()));
		String valorTexto = StringUtils.EMPTY;
		
		if("MAIL".equals(pregunta.getAbreviatura())) {
			ResponseODF3DTO odf3DTO = personasBO.obtenerCorreoElectronicoODF3(cuestionario.getCodigoCliente(), session);
			if(odf3DTO != null && odf3DTO.getCorreoElectronico() != null) {
				valorTexto = odf3DTO.getCorreoElectronico();
			}
		} else if("NMR1".equals(pregunta.getAbreviatura()) || "DMR1".equals(pregunta.getAbreviatura()) || "TLR1".equals(pregunta.getAbreviatura())) {
			List<ResponsePE58DTO> referencias = personasBO.obtenerReferenciasPersonasFisicasPE58(cuestionario.getCodigoCliente(), session);
			
			valorTexto = ValidadorReferenciasFisicasIC.obtenerValorReferencia(pregunta, referencias);
		} else if("NMR2".equals(pregunta.getAbreviatura()) || "DMR2".equals(pregunta.getAbreviatura()) || "TLR2".equals(pregunta.getAbreviatura())) {
			List<ResponsePE58DTO> referencias = personasBO.obtenerReferenciasPersonasFisicasPE58(cuestionario.getCodigoCliente(), session);
			
			valorTexto = ValidadorReferenciasFisicasIC.obtenerValorReferencia(pregunta, referencias);
		} else if("BNC1".equals(pregunta.getAbreviatura()) || "NCD1".equals(pregunta.getAbreviatura())) {
			if("F".equals(cuestionario.getTipoPersona())) {
				List<ResponsePE58DTO> referenciasFisicas = 
						personasBO.obtenerReferenciasPersonasFisicasActEmpPE58(cuestionario.getCodigoCliente(), session);
				
				valorTexto = ValidadorReferenciasFisicasIC.obtenerValorReferencia(pregunta, referenciasFisicas);
			} else {
				List<ResponsePE71DTO> referenciasMorales = 
						personasBO.obtenerReferenciasPersonasMoralesPE71(cuestionario.getCodigoCliente(), session);
				
				valorTexto = ValidadorReferenciasBancariasIC.agregarValidacionesEspeciales(pregunta, referenciasMorales);
			}
			
		} else if("BNC2".equals(pregunta.getAbreviatura()) || "NCD2".equals(pregunta.getAbreviatura())) {
			if("F".equals(cuestionario.getTipoPersona())) { 
				List<ResponsePE58DTO> referencias = 
						personasBO.obtenerReferenciasPersonasFisicasActEmpPE58(cuestionario.getCodigoCliente(), session);
				
				valorTexto = ValidadorReferenciasFisicasIC.obtenerValorReferencia(pregunta, referencias);
			} else {
				List<ResponsePE71DTO> referencias = 
						personasBO.obtenerReferenciasPersonasMoralesPE71(cuestionario.getCodigoCliente(), session);
				
				valorTexto = ValidadorReferenciasBancariasIC.agregarValidacionesEspeciales(pregunta, referencias);
			}
		}
		
		return valorTexto;
	}
	
	@Override
	public CuestionarioDTO definirPEP(CuestionarioDTO cuestionario, ArchitechSessionBean session) throws BusinessException {
		cuestionario.setFamiliarPep(false);
		//Obtengo la respuesta de la pregunta "Tiene un familiar PEP"
		CuestionarioRespuestaDTO preguntaTienePersonaPep = 
				preguntasIcBO.obtenerPreguntaPorSeccionAbreviatura(cuestionario.getIdCuestionario(), "SPEP", "PPEP", session);
		
		//Obtengo las opciones de esa respuesta
		ResponseOpcionesPregunta response = 
				opcionesPreguntaBO.obtenerOpcionesPorSeccionAbreviatura("SPEP", "PPEP", session);
		List<OpcionesPreguntaDTO> opciones = response.getLstOpcionesPreguntas();

		for(OpcionesPreguntaDTO opcion : opciones) {
			//Si la opcion es "SI" y es la que tengo almacenada entonces se asigna TRUE a variable del Cuestionario
			if(GeneradorCuestionarioGenerico.IND_PERSONA_PEP.equals(opcion.getValor()) && 
					Integer.parseInt(opcion.getIdOpcion()) == preguntaTienePersonaPep.getValorOpcion()){
				
				cuestionario.setFamiliarPep(true);
			}
		}
		
		return cuestionario;
	}
	
	@Override
	public CuestionarioDTO actualizarValorPEPAfirmativo(CuestionarioDTO cuestionario, ArchitechSessionBean session) throws BusinessException {
		cuestionario.setFamiliarPep(false);
		//Obtengo la respuesta de la pregunta "Tiene un familiar PEP"
		List<PreguntaDTO> preguntasTienePersonaPep = 
				preguntasIcBO.obtenerPreguntasAsignadasPorAbreviaturas(cuestionario.getIdCuestionario(), "'PPEP'", session);
		
		//Se obtienen las opciones de esa respuesta
		ResponseOpcionesPregunta response = 
				opcionesPreguntaBO.obtenerOpcionesPorSeccionAbreviatura("SPEP", "PPEP", session);
		List<OpcionesPreguntaDTO> opciones = response.getLstOpcionesPreguntas();
		
		int idOpcionPositiva = 0;
		for(OpcionesPreguntaDTO opcion : opciones) {
			//Si la opcion es "SI" y es la que tengo almacenada entonces se asigna TRUE a variable del Cuestionario
			if(GeneradorCuestionarioGenerico.IND_PERSONA_PEP.equals(opcion.getValor())){
				idOpcionPositiva = Integer.parseInt(opcion.getIdOpcion());
			}
		}
		
		if(preguntasTienePersonaPep != null) {
			for(PreguntaDTO pregunta : preguntasTienePersonaPep) {
				CuestionarioRespuestaDTO cuestionarioRespuesta = new CuestionarioRespuestaDTO();
				cuestionarioRespuesta.setClaveRespuestaPregunta(pregunta.getClaveRespuestaPregunta());
				cuestionarioRespuesta.setValorOpcion(idOpcionPositiva);
				cuestionarioRespuesta.setValorTexto(StringUtils.EMPTY);
				
				preguntasIcBO.ejecutarActualizacionRespuesta(cuestionarioRespuesta);
			}
		}
		
		return cuestionario;
	}
}
