/**
 * Isban Mexico
 *   Clase: ObtenerCuestionarioWSBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de la informacion para generar los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.rpt.ICListasBean;
import mx.isban.norkom.cuestionario.bo.ObtenerCuestionarioWSBO;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.bo.RegimenSimplificadoBO;
import mx.isban.norkom.cuestionario.bo.ValidadorRiesgoA1BO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.ObtenerCuestionarioWSDAO;
import mx.isban.norkom.cuestionario.dao.impl.ObtenerCuestionarioWSDAOImpl;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.NivelRiesgoDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ObtenerCuestionarioWSBOImpl extends Architech implements ObtenerCuestionarioWSBO {
	
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -1727892797945376104L;
	/**
	 * Logger de la clase
	 */
	private static final Logger LOG = Logger.getLogger(ObtenerCuestionarioWSDAOImpl.class);
	/**
	 *Constante String CODIGO_MEXICO
	 */
	private static final String CODIGO_MEXICO = "052";
	/**
	 * Constante String FLAG_ACTI_ESPEC
	 */
	private static final String FLAG_ACTI_ESPEC = "strFlagActEspecifica";
	/**
	 * DAO para obtener los datos de la generacion del PDF
	 */
	@EJB
	private transient ObtenerCuestionarioWSDAO obtenerPdfDAOWS;
	/**
	 * BO para la Validadcion de Riesgo
	 */
	@EJB
	private transient ValidadorRiesgoA1BO validadorRiesgoA1BO;
	
	/**
	 * BO para identificar si se trata de un Regimen Simplificado
	 */
	@EJB
	private transient RegimenSimplificadoBO regimenSimplificadoBO;
	/**
	 * BO para la obtencion de parametros
	 */
	@EJB
	private transient ParametrosBO parametrosBO;
	
	/**
	 * CuestionarioDAO dao para obtener los datos del cuestionario
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;

	@Override
	public ResponseObtenerCuestionario obtenerPdfIP(RequestObtenerCuestionario peticion,
			ArchitechSessionBean psession) throws BusinessException {
		validapeticion(peticion);
		LOG.info("Entrando a ----------> obtenerPDF ");
		 
		ClienteDTO cliente = new ClienteDTO();
		ActividadEconomicaDTO actividad = new ActividadEconomicaDTO();
		HashMap<String, Object> respuestas = new HashMap<String, Object>();
		ResponseObtenerCuestionario response = new ResponseObtenerCuestionario();
		response = obtenerPdfDAOWS.obtenerCuestionarioIP(peticion, psession);
		respuestas = (HashMap<String, Object> )response.getDatosReporte();
		CuestionarioDTO cuestionario = response.getCuestionarioDTO();
		HashMap<String, Object> encabezado = new HashMap<String, Object>();
		if(cuestionario!=null){
		encabezado.put("strDia",cuestionario.getDia());
		encabezado.put("strMes", cuestionario.getMes());
		encabezado.put("strAnio", cuestionario.getAnio());
		encabezado.put("strSucursal",cuestionario.getNombreSucursal()+" "+cuestionario.getCodigoSucursal());
		encabezado.put("strNombre", cuestionario.getNombreCompletoCliente());
		encabezado.put("strZona", cuestionario.getNombreRegion());
		encabezado.put("strPlaza", cuestionario.getNombrePlaza());
		cliente = cuestionario.getCliente();
		encabezado.put("strCodCliente", cliente.getCodigoCliente() );
		encabezado.put("strNumCueCon", cuestionario.getCuentaContrato());
		encabezado.put("strSegmento", cuestionario.getDescSegmento());
		encabezado.put("strProdContratado", cuestionario.getDescProducto());
		encabezado.put("strPais", cuestionario.getNombrePais());
		
		encabezado.put("strNombreCompletoCliente", cuestionario.getNombreCompletoCliente());
		encabezado.put("strNombreCompletoFuncionario", cuestionario.getNombreFuncionario());
		
		if(CODIGO_MEXICO.equals(cuestionario.getCodigoPais()))
		{
			encabezado.put("strFlagResidenteMexicano", "1");
		}
		encabezado.put("strEstado", cuestionario.getNombreEntidad());
		encabezado.put("strMunicipio", cuestionario.getNombreMunicipio());
		encabezado.put("strNacionalidad", cuestionario.getNombreNacionalidad());
		actividad = cuestionario.getActividadGenerica();
		encabezado.put("strActGenerica", actividad.getCodigo());
		actividad = cuestionario.getActividadEspecifica();
		}
		encabezado.put("strActEspecifica", actividad.getDescripcion());
		if("P".equals(actividad.getTipoSector()) || "R".equals(actividad.getTipoSector()))
		{
			encabezado.put(FLAG_ACTI_ESPEC, "1");
		}
		encabezado.put("PREG_SALDO_PDF", validaSeccionSaldo(psession));
		
		RequestObtenerCuestionario peticionRegimen = new RequestObtenerCuestionario();
		peticionRegimen.setIdCuestionario(cuestionario.getIdCuestionario());
		NivelRiesgoDTO regimenSimplificado = regimenSimplificadoBO.esRegimenSimplificado(peticionRegimen, getArchitechBean());
		encabezado.put("regimenSimplificadoA1", regimenSimplificado.isSimplificadoA1());
		
		respuestas.putAll(encabezado);
		response.setDatosReporte(respuestas);
		response.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		LOG.info("Saliendo de ----------> obtenerPDF ");
		return response;
	}
	/**
	 * Metodo para validar si debe de quitar o no la pregunta de saldos en IP
	 * @param sessionBean ArchitechSessionBean de agave
	 * @return cadena con valor de parametro para indicar si se muestra o no la seccion
	 * @throws BusinessException con mensaje de error
	 */
	private String validaSeccionSaldo(ArchitechSessionBean sessionBean) throws BusinessException {
			String valida="";
			//valida si el parametro esta en 1 que es activo...
			ParametroDTO dto= parametrosBO.obtenerParametroPorNombre("PREG_SALDO_PDF", sessionBean);
			if(dto!=null){
				return dto.getValor();
			}
			
			return valida;
	}
	/**
	 * valida si la peticion contiene los datos para realizar la busqueda
	 * @param peticion RequestObtenerCuestionario con datos de entrada para validar
	 * @throws BusinessException con mensaje de error de validacion
	 */
	private void validapeticion(RequestObtenerCuestionario peticion) throws BusinessException {
		if(peticion!=null){
			if((peticion.getIdCuestionario()==null || peticion.getIdCuestionario().isEmpty()) && validaRequeridosPeticion(peticion)){
					throw new BusinessException(Mensajes.ERR_EXP_PDF_NO_IN_OBLIG.getCodigo(), Mensajes.ERR_EXP_PDF_NO_IN_OBLIG.getDescripcion());
			}
		}else{
			throw new BusinessException(Mensajes.ERR_EXP_PDF_NO_IN_DATA.getCodigo(), Mensajes.ERR_EXP_PDF_NO_IN_DATA.getDescripcion());
		}
		
	}
	/***
	 * 
	 * @param peticion con datos a validar
	 * @return respuesta de validacion
	 */
	private boolean validaRequeridosPeticion(RequestObtenerCuestionario peticion) {
		return validaString(peticion.getCodProducto()) || validaString(peticion.getCodSubProducto()) || validaString(peticion.getContrato()) || validaString(peticion.getCodSucursal());
	}

	/**
	 * valida cadenas si son nulas
	 * @param cadena a validar
	 * @return respuesta de validacion
	 */
	private boolean validaString(String cadena) {
		if(cadena==null || cadena.isEmpty()){
			return true;
		}
		return false;
	}


	@Override
	public ResponseObtenerCuestionario obtenerPdfIC(
			RequestObtenerCuestionario peticion, CuestionarioWSDTO cuestionarioBD, ResponseObtenerCuestionario dto, ArchitechSessionBean psession)
			throws BusinessException {
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		ClienteDTO cliente = new ClienteDTO();
		HashMap<String, Object> respuestas = new HashMap<String, Object>();
		ResponseObtenerCuestionario response = new ResponseObtenerCuestionario();
		boolean bandera = validadorRiesgoA1BO.esRegimenSimplificado(peticion, psession);
		if(bandera)
		{
			return response;
		}
		response = obtenerPdfDAOWS.obtenerCuestionarioIC(peticion,dto, psession);
		respuestas = (HashMap<String, Object> )response.getDatosReporte();
		cuestionario = response.getCuestionarioDTO();
		HashMap<String, Object> encabezado = new HashMap<String, Object>();
		encabezado.put("strFlagCargoPublico",dto.getDatosReporte().get("strCargoPublico"));
		encabezado.put("strPais",dto.getDatosReporte().get("strPais"));
		encabezado.put(FLAG_ACTI_ESPEC,dto.getDatosReporte().get(FLAG_ACTI_ESPEC));
		if(!CODIGO_MEXICO.equals(dto.getDatosReporte().get("strPaisVal")))
		{		
			encabezado.put("strPaisResidencia", "1");
			if(!CODIGO_MEXICO.equals(dto.getDatosReporte().get("strNacionalidadVal")))
			{
				encabezado.put("strNacionalidad", "1");		
			}
			else 
			{
				encabezado.put("strNacionalidad", "0");
			}
		}
		else
		{
			encabezado.put("strPaisResidencia", "0");
		}
		if("USD".equals(cuestionario.getDivisa()))
		{
			encabezado.put("strFlagProdContratado", "1");
			encabezado.put("strTipoMoneda", "1");
		}
		if("MXP".equals(cuestionario.getDivisa()))
		{
			encabezado.put("strTipoMoneda", "0");
		}
		if("1".equals(dto.getDatosReporte().get(FLAG_ACTI_ESPEC)))
		{
			encabezado.put("strFlagPuestoEmpresa", "1");
		}
		encabezado.put("strDia",cuestionario.getDia());
		encabezado.put("strMes", cuestionario.getMes());
		encabezado.put("strAnio", cuestionario.getAnio());
		encabezado.put("strSucursal",cuestionario.getNombreSucursal()+" "+cuestionario.getCodigoSucursal());
		encabezado.put("strNombre", cuestionario.getNombreCompletoCliente());
		encabezado.put("strZona", cuestionario.getNombreRegion());
		encabezado.put("strPlaza", cuestionario.getNombrePlaza());
		cliente = cuestionario.getCliente();
		encabezado.put("strCodCliente", cliente.getCodigoCliente() );
		String nombreCompleto = cliente.getNombreCliente() + " " 
								+ cliente.getApellidoPaterno() + " " 
								+ cliente.getApellidoMaterno();
		encabezado.put("strNombre", nombreCompleto );
		encabezado.put("strNumCueCon", cuestionario.getCuentaContrato());
		
		encabezado.put("strNombreCompletoCliente", cuestionario.getNombreCompletoCliente());
		encabezado.put("strNombreCompletoFuncionario", cuestionario.getNombreFuncionario());
		
		StringBuffer observaciones = new StringBuffer();
		
		ParametroDTO indicadoresVisitaDTO = parametrosBO.obtenerParametroPorNombre("indUpldVisitaDom", psession);
		String valor = indicadoresVisitaDTO.getValor();
		List<String> calificaciones = new ArrayList<String>(); 
		if(StringUtils.isNotEmpty(valor)) {
			String[] parametrosBD = valor.split(",");
			for(String parametroBD : parametrosBD) {
				calificaciones.add(StringUtils.trimToEmpty(parametroBD));
			}
		}
		
		if(calificaciones.contains(cuestionarioBD.getIndicadorUpld())) {
			observaciones.append("EL CLIENTE REQUIERE LLEVAR A CABO VISITA DOMICILIARIA");
		}
		
		respuestas.put("strObservaciones", observaciones.toString());
		
		respuestas.putAll(encabezado);
		ICListasBean listas = new ICListasBean();
		listas = response.getListasBeanIC();
		response.setListasBeanIC(listas);
		response.setDatosReporte(respuestas);
		response.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		return response;
	}
	
	@Override
	public CuestionarioWSDTO obtenerDatosGeneralesCuestionarioPorIdCntrNoNulo(RequestObtenerCuestionario datosConsulta, ArchitechSessionBean psession) 
	throws BusinessException {
		return cuestionarioDAO.obtenerDatosGeneralesCuestionarioPorIdCntrNoNulo(datosConsulta, psession);
	}
	
	/**
	 * @param parametrosBO 
	 * el parametrosBO a agregar
	 */
	public void setParametrosBO(ParametrosBO parametrosBO) {
		this.parametrosBO = parametrosBO;
	}
}
