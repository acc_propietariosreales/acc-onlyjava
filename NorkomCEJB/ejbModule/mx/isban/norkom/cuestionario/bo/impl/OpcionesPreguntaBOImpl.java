/**
 * Isban Mexico
 *   Clase: OpcionesPreguntaBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de las opciones de las preguntas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestOpcionesPregunta;
import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.bo.OpcionesPreguntaBO;
import mx.isban.norkom.cuestionario.dao.OpcionesPreguntaDAO;
import mx.isban.norkom.cuestionario.util.Mensajes;

import org.apache.log4j.Logger;

/**
 * Session Bean implementation class OpcionesPreguntaBOImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class OpcionesPreguntaBOImpl extends Architech  implements OpcionesPreguntaBO {
    /**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -352876408753684063L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(OpcionesPreguntaBOImpl.class);
	/**
	 * Doa para la obtenecion de Opciones de Preguntas
	 */
	@EJB
	private transient OpcionesPreguntaDAO opcionesPeguntaDAO;

	@Override
	public ResponseOpcionesPregunta obtenerOpcionesPregunta(RequestOpcionesPregunta peticion, ArchitechSessionBean psession)
	throws BusinessException {
		LOG.info("llamado a obtenerTipoPregunta -> tipoPreguntaDAO.obtenerTiposPregunta");
		ResponseOpcionesPregunta responseTipoPregunta = new ResponseOpcionesPregunta();
		try{
			responseTipoPregunta.setLstOpcionesPreguntas(
				(opcionesPeguntaDAO.obtenerOpcionesPregunta(peticion.getClaveGrupo(), psession)));
			responseTipoPregunta.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		}catch(BusinessException be){
			responseTipoPregunta.setLstOpcionesPreguntas(null);
			responseTipoPregunta.setCodError(Mensajes.ERROR_COMUNICACION.getCodigo());
			responseTipoPregunta.setMsgError(Mensajes.ERROR_COMUNICACION.getDescripcion());
			showException(be);
		}
		LOG.info("regresa  obtenerTipoPregunta -> tipoPreguntaDAO.obtenerTiposPregunta");
		return responseTipoPregunta;
	}
	
	@Override
	public ResponseOpcionesPregunta obtenerOpcionesPorSeccionAbreviatura(String idSeccion, String idAbreviatura, ArchitechSessionBean psession)
			throws BusinessException {
		LOG.info("llamado a obtenerTipoPregunta -> tipoPreguntaDAO.obtenerTiposPregunta");
		ResponseOpcionesPregunta responseTipoPregunta = new ResponseOpcionesPregunta();
		try{
			responseTipoPregunta.setLstOpcionesPreguntas(
					(opcionesPeguntaDAO.obtenerOpcionesPorSeccionAbreviatura(idSeccion, idAbreviatura, psession)));
			responseTipoPregunta.setCodError(Mensajes.OPERACION_EXITOSA.getCodigo());
		}catch(BusinessException be){
			showException(be);
			responseTipoPregunta.setLstOpcionesPreguntas(null);
			responseTipoPregunta.setCodError(Mensajes.ERROR_COMUNICACION.getCodigo());
			responseTipoPregunta.setMsgError(Mensajes.ERROR_COMUNICACION.getDescripcion());
		}
		LOG.info("regresa  obtenerOpcionesPregunta -> tipoPreguntaDAO.obtenerOpcionesPorSeccionAbreviatura");
		return responseTipoPregunta;
	}
}
