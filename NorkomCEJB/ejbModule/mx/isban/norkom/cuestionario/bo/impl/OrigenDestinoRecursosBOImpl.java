/**
 * Isban Mexico
 *   Clase: OrigenDestinoRecursosBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del origen y destino de los recuros.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.OrigenDestinoRecursosBO;
import mx.isban.norkom.cuestionario.dao.OrigenDestinoRecursosDAO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;

import org.apache.log4j.Logger;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class OrigenDestinoRecursosBOImpl extends Architech implements OrigenDestinoRecursosBO {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -2678393213247764419L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(OrigenDestinoRecursosBOImpl.class);
	
	/**
	 * dao para el Origen y Destino de Recursos
	 */
	@EJB
	private transient OrigenDestinoRecursosDAO recursosDAO;
	
	@Override
	public List<RecursoDTO> obtenerRecursos(String tipoPersona, String tipoRecurso, ArchitechSessionBean session)
	throws BusinessException {
		LOG.info(String.format("Se obtienen recursos para TipoPersona[%s] TipoRecurso[%s]", tipoPersona, tipoRecurso));
		return recursosDAO.obtenerRecursos(tipoPersona, tipoRecurso, session);
	}

}
