/**
 * Isban Mexico
 *   Clase: PaisesBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de los paises.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.BeanResponsePaises;
import mx.isban.norkom.cuestionario.bo.PaisesBO;
import mx.isban.norkom.cuestionario.dao.PaisDAO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PaisesBOImpl extends Architech implements PaisesBO {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -2678393213247764419L;
	
	/**
	 * Dao para Paises
	 */
	@EJB
	private PaisDAO paisDAO;

	@Override
	public BeanResponsePaises obtenerPaises(ArchitechSessionBean session)
			throws BusinessException {
		info("llamado a obtenerPaises -> paisDAO.obtenerPaises");
		BeanResponsePaises responsePaises = new BeanResponsePaises();
		try{
			responsePaises.setLstPaises(paisDAO.obtenerPaises(session));
		}catch(BusinessException be){
			showException(be);
			error(String.format("Error al obtener los paises Codigo[%s] Causa[%s] Mensaje[%s]", 
									be.getCode(), be.getCause(), be.getMessage()));
			responsePaises.setLstPaises(null);
		}
		info("regresa  obtenerPaises -> paisDAO.obtenerPaises");
		return responsePaises;
	}
	
	@Override
    public PaisDTO obtenerPaisPorCodigo(String codigoPais, ArchitechSessionBean sesion) throws BusinessException {
		return paisDAO.obtenerPaisPorCodigo(codigoPais, sesion);
	}

}
