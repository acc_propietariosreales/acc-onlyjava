/**
 * Isban Mexico
 *   Clase: ParametrosBOImpl.java
 *   Descripcion: Componente para realizar la logica de los parametros de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.ParametrosBO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;

import org.apache.log4j.Logger;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametrosBOImpl extends Architech implements ParametrosBO {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -2678393213247764419L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(ParametrosBOImpl.class);
	
	/**
	 * PaisDAO EJB
	 */
	@EJB
	private transient ParametrosDAO parametrosDAO;
	
	@Override
	public ParametroDTO obtenerParametroPorNombre(String nombreParametro, ArchitechSessionBean psession) throws BusinessException {
		LOG.info(String.format("Se va a realizar la obtencion del Parametro[%s]", nombreParametro));
		return parametrosDAO.obtenerParametroPorNombre(nombreParametro);
	}
	
	@Override
	public List<ParametroDTO> obtenerParametros(ArchitechSessionBean psession) throws BusinessException {
		LOG.info("Obtencion de todos los Parametros activos");
		return parametrosDAO.obtenerParametros();
	}
}
