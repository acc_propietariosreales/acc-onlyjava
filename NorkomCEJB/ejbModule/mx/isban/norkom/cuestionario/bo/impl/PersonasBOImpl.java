/**
 * Isban Mexico
 *   Clase: PersonasBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de los clientes.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.PersonasBO;
import mx.isban.norkom.cuestionario.dao.NivelRiesgoDAO;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.IndicadorOPD2WS;
import mx.isban.norkom.cuestionario.dto.RequestODF3DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP2DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP3DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE58DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE71DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODF3DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP2DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE71DTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.mq.PersonasMQ;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PersonasBOImpl extends Architech implements PersonasBO {

	/**
	 * ID para la serializacion del objeto
	 */
	private static final long serialVersionUID = -1294209964333384620L;
	/** A3 - PENDIENTE AUTORIZACION BLO */
	private static final String IND_A3_KYC_BLO = "KYC-BLO";
	/** Indicador de cliente nuevo */
	private static final String IND_CLIENTE_NUEVO = "N";
	/** Indicador UPLD Autorizado por Bloqueo */
	private static final String IND_UPLD_BLO_AUT = "BLO-AUT";
	/** Indicador Anticliente */
	private static final String IND_ANTICLIENTE = "ACL";
	/** Indicador No-Cliente */
	private static final String IND_NO_CLIENTE = "NCL";
	
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(PersonasBOImpl.class);
	
	/**
	 * PersonasMQ personasMQ
	 */
	@EJB
	private transient PersonasMQ personasMQ;
	
	/**
	 * NivelRiesgoDAO EJB
	 */
	@EJB
	private transient NivelRiesgoDAO nivelRiesgoDAO;
	
	@Override
	public ResponseODF3DTO obtenerCorreoElectronicoODF3(String buc, ArchitechSessionBean sesion) throws BusinessException {
		RequestODF3DTO peticion = new RequestODF3DTO();
		peticion.setCodigoCliente(buc);
		
		return personasMQ.obtenerCorreoElectronicoODF3(peticion, sesion);
	}
	
	@Override
	public List<ResponsePE58DTO> obtenerReferenciasPersonasFisicasPE58(String buc, ArchitechSessionBean sesion) throws BusinessException {
		RequestPE58DTO input = new RequestPE58DTO();
		input.setNumeroPersona(buc);
		return personasMQ.obtenerReferenciasPersonasFisicasPE58(input, false, sesion);
	}
	
	@Override
	public List<ResponsePE58DTO> obtenerReferenciasPersonasFisicasActEmpPE58(String buc, ArchitechSessionBean sesion) throws BusinessException {
		RequestPE58DTO input = new RequestPE58DTO();
		input.setNumeroPersona(buc);
		return personasMQ.obtenerReferenciasPersonasFisicasPE58(input, true, sesion);
	}
	
	@Override
	public List<ResponsePE71DTO> obtenerReferenciasPersonasMoralesPE71(String buc, ArchitechSessionBean sesion) throws BusinessException {
		RequestPE71DTO input = new RequestPE71DTO();
		input.setNumeroPersona(buc);
		
		return personasMQ.obtenerReferenciasBancariasPE71(input, sesion);
	}
	
	@Override
	public CuestionarioDTO bloquearCliente(CuestionarioDTO cuestionario, ArchitechSessionBean sesion) 
	throws BusinessException {
		cuestionario.setNivelRiesgo(GeneradorCuestionarioGenerico.ID_CUESTIONARIO_A3);
		cuestionario.setIndicadorUpld(IND_A3_KYC_BLO);
		
		//Actualiza el Nivel de Riesgo en Personas
		RequestODP2DTO requestNivelRiesgo = new RequestODP2DTO();
		CalificacionDTO nivelRiesgo = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getNivelRiesgo());
		requestNivelRiesgo.setBuc(cuestionario.getCodigoCliente());
		requestNivelRiesgo.setIndicador(nivelRiesgo.getCodigoIndicadorPersonas());
		requestNivelRiesgo.setValorIndicador(nivelRiesgo.getValorIndicadorPersonas());
		personasMQ.actualizarRiesgoODP2(requestNivelRiesgo, sesion);
		
		//Actualiza el Indicador UPLD en Personas
		RequestODP2DTO requestIndicadorUpld = new RequestODP2DTO();
		CalificacionDTO indicadorUpld = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getIndicadorUpld());
		requestIndicadorUpld.setBuc(cuestionario.getCodigoCliente());
		requestIndicadorUpld.setIndicador(indicadorUpld.getCodigoIndicadorPersonas());
		requestIndicadorUpld.setValorIndicador(indicadorUpld.getValorIndicadorPersonas());
		personasMQ.actualizarRiesgoODP2(requestIndicadorUpld, sesion);
		
		return cuestionario;
	}
	
	@Override
	public CuestionarioDTO ejecutarOperacionesEnPersonas(CuestionarioDTO cuestionario, ArchitechSessionBean sesion) 
	throws BusinessException {
	
		ClienteDTO cliente = cuestionario.getCliente();
		
		//Actualiza el Nivel de Riesgo en Personas
		RequestODP2DTO requestNivelRiesgo = new RequestODP2DTO();
		CalificacionDTO nivelRiesgo = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getNivelRiesgo());
		requestNivelRiesgo.setBuc(cuestionario.getCodigoCliente());
		requestNivelRiesgo.setIndicador(nivelRiesgo.getCodigoIndicadorPersonas());
		requestNivelRiesgo.setValorIndicador(nivelRiesgo.getValorIndicadorPersonas());
		personasMQ.actualizarRiesgoODP2(requestNivelRiesgo, sesion);
		

		
		//Actualiza el Indicador UPLD en Personas
		RequestODP2DTO requestIndicadorUpld = new RequestODP2DTO();
		CalificacionDTO indicadorUpld = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getIndicadorUpld());
		requestIndicadorUpld.setBuc(cuestionario.getCodigoCliente());
		requestIndicadorUpld.setIndicador(indicadorUpld.getCodigoIndicadorPersonas());
		requestIndicadorUpld.setValorIndicador(indicadorUpld.getValorIndicadorPersonas());
		personasMQ.actualizarRiesgoODP2(requestIndicadorUpld, sesion);
		

		
		if(IND_CLIENTE_NUEVO.equals(cliente.getIndClienteNuevo()) && cuestionario.getIndicadorUpld().contains(IND_A3_KYC_BLO)) {
			RequestODP3DTO requestODP3DTO = new RequestODP3DTO();
			requestODP3DTO.setBuc(cliente.getCodigoCliente());
			requestODP3DTO.setCondicionCliente(IND_ANTICLIENTE);
			
			//Actualiza condicion del cliente en Personas - Clientes nuevos y que Norkom lo haya calificado como A3 CCC o BLO
			personasMQ.actualizarEstadoClienteNuevoODP3(requestODP3DTO, sesion);
			

		}
		
		return cuestionario;
	}
	
	@Override
	public void ejecutarActualizacionCalificacionManual(IndicadorOPD2WS indicadorODP2, ArchitechSessionBean psession) 
	throws BusinessException {
		LOG.info("BO ::: Inicia proceso de Actualizacion Manual del Riesgo");
		LOG.info(String.format("Parametros buc[%s] nivelRiesgo[%s] indicadorUpld[%s] idNorkom[%s] fechaActualizacion[%s]", 
				indicadorODP2.getBuc(), indicadorODP2.getNivelRiesgo(), indicadorODP2.getIndicadorUpld(), 
				indicadorODP2.getIdNorkom(), indicadorODP2.getFechaActualizacion()));
		
		if(indicadorODP2.getIdNorkom() == null || StringUtils.isBlank(indicadorODP2.getIdNorkom())) {
			throw new BusinessException(ERR_DATOS_ID_NORKOM, "Error de datos. Es necesario proporcionar el campo IdNorkom");
		}
		
		if(indicadorODP2.getBuc() == null || StringUtils.isBlank(indicadorODP2.getBuc())) {
			throw new BusinessException(ERR_DATOS_BUC, "Error de datos. Es necesario proporcionar el campo BUC");
		}
		
		if(indicadorODP2.getNivelRiesgo() == null || StringUtils.isBlank(indicadorODP2.getNivelRiesgo())) {
			throw new BusinessException(ERR_DATOS_NIVEL_RIESGO, "Error de datos. Es necesario proporcionar el campo Nivel de Riesgo");
		}
		
		if(indicadorODP2.getIndicadorUpld() == null || StringUtils.isBlank(indicadorODP2.getIndicadorUpld())) {
			throw new BusinessException(ERR_DATOS_IND_UPLD, "Error de datos. Es necesario proporcionar el campo Indicador UPLD");
		}
		
		//Obtiene el equivalente del Nivel de Riesgo para Personas
		CalificacionDTO nivelRiesgoDTO = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(indicadorODP2.getNivelRiesgo());
		if(nivelRiesgoDTO == null || StringUtils.isBlank(nivelRiesgoDTO.getCodigoIndicadorPersonas()) || 
				StringUtils.isBlank(nivelRiesgoDTO.getValorIndicadorPersonas())) 
		{
			throw new BusinessException(ERR_DATOS_NIVEL_RIESGO_INCORRECTO, "Error de datos. Es necesario proporcionar un Nivel de Riesgo valido");
		}
		
		//Obtiene el equivalente del Indicador UPLD para Personas
		CalificacionDTO indicadorUpldDTO = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(indicadorODP2.getIndicadorUpld());
		if(indicadorUpldDTO == null || StringUtils.isBlank(indicadorUpldDTO.getCodigoIndicadorPersonas()) || 
				StringUtils.isBlank(indicadorUpldDTO.getValorIndicadorPersonas())) 
		{
			throw new BusinessException(ERR_DATOS_IND_UPLD_INCORRECTO, "Error de datos. Es necesario proporcionar un Indicador UPLD valido");
		}
		
		//Actualiza el Nivel de Riesgo en Personas
		RequestODP2DTO requestNivelRiesgo = new RequestODP2DTO();
		requestNivelRiesgo.setBuc(indicadorODP2.getBuc());
		requestNivelRiesgo.setIndicador(nivelRiesgoDTO.getCodigoIndicadorPersonas());
		requestNivelRiesgo.setValorIndicador(nivelRiesgoDTO.getValorIndicadorPersonas());
		ResponseODP2DTO responseNivelRiesgo = personasMQ.actualizarRiesgoODP2(requestNivelRiesgo, getArchitechBean());
		
		String codigoRespuestaNivelRiesgo = responseNivelRiesgo.getCodigoOperacion();
		if (!ResponseODP2DTO.OPERACION_EXITOSA.equals(codigoRespuestaNivelRiesgo) && !ResponseODP2DTO.OPERACION_VALIDA.equals(codigoRespuestaNivelRiesgo)) {
			throw new BusinessException(codigoRespuestaNivelRiesgo, responseNivelRiesgo.getMsgOperacion());
		}
		
		//Actualiza el Indicador UPLD en Personas
		RequestODP2DTO requestIndicadorUpld = new RequestODP2DTO();
		requestIndicadorUpld.setBuc(indicadorODP2.getBuc());
		requestIndicadorUpld.setIndicador(indicadorUpldDTO.getCodigoIndicadorPersonas());
		requestIndicadorUpld.setValorIndicador(indicadorUpldDTO.getValorIndicadorPersonas());
		ResponseODP2DTO responseIndUPLD = personasMQ.actualizarRiesgoODP2(requestIndicadorUpld, getArchitechBean());
		
		String codigoRespuestaIndUPLD = responseIndUPLD.getCodigoOperacion();
		if (!ResponseODP2DTO.OPERACION_EXITOSA.equals(codigoRespuestaNivelRiesgo) && !ResponseODP2DTO.OPERACION_VALIDA.equals(codigoRespuestaNivelRiesgo)) {
			throw new BusinessException(codigoRespuestaIndUPLD, responseIndUPLD.getMsgOperacion());
		}
		
		info(String.format("Indicador UPLD[%s] del cliente[%s]", indicadorODP2.getIndicadorUpld(), indicadorODP2.getBuc()));
		if(IND_UPLD_BLO_AUT.equals(indicadorODP2.getIndicadorUpld())) {
			info(String.format(
					"Se va a realizar la actualizacion del cliente[%s] ::: pasa a ser [%s]", indicadorODP2.getBuc(), IND_NO_CLIENTE));
			RequestODP3DTO requestODP3DTO = new RequestODP3DTO();
			requestODP3DTO.setBuc(indicadorODP2.getBuc());
			requestODP3DTO.setCondicionCliente(IND_NO_CLIENTE);
			
			//Actualiza condicion del cliente en Personas - En caso de que el Indicado UPLD sea BLO-AUT se pasa a NCL
			personasMQ.actualizarEstadoClienteNuevoODP3(requestODP3DTO, psession);
			

		}
	}

}
