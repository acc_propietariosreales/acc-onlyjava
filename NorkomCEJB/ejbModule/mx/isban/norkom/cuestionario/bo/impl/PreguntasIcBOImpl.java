/**
 * Isban Mexico
 *   Clase: CuestionarioICBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.PreguntasIcBO;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.dao.NivelRiesgoDAO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.FamiliarDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

import org.apache.log4j.Logger;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PreguntasIcBOImpl extends Architech implements PreguntasIcBO {
	/**
	 * PreguntasIcBOImpl.java  de tipo long
	 */
	private static final long serialVersionUID = 2980584480649213023L;

	/**Logger de bo*/
	private static final Logger LOG=Logger.getLogger(PreguntasIcBOImpl.class);
	
	/**
	 * Variable utilizada para declarar preguntasDAO
	 */
	@EJB
	private transient PreguntasDAO preguntasDAO;
	
	/**
	 * DAO para Administrador de Claves
	 */
	@EJB
	private transient AdministradorClavesDAO administradorClavesDAO;
	
	/**
	 * nivelRiesgoDAO Permite actualizar el Nivel de Riesgo y el Indicador UPLD en ACC
	 */
	@EJB
	private transient NivelRiesgoDAO nivelRiesgoDAO;
	
	@Override
	public CuestionarioDTO guardarRespuestasCuestionarioIC(CuestionarioDTO cuestionario, List<CuestionarioRespuestaDTO> respuestas, ArchitechSessionBean sesion)
	throws BusinessException {
		LOG.info("Inicia proceso para almacenar las respuestas del Cuestionario IC");
		List<CuestionarioRespuestaDTO> respuestasPregunta = new ArrayList<CuestionarioRespuestaDTO>();
		Map<Integer, FamiliarDTO> familiares = new HashMap<Integer, FamiliarDTO>();
		
		int claveCuestionarioRespuesta = administradorClavesDAO.obtenerNuevaClaveCuestionarioRespuesta();
		
		for(CuestionarioRespuestaDTO cuestionarioRespuestasDTO : respuestas){
			//Se realizan validaciones especiales en caso de que sea la seccion de Parentesco
			if(cuestionarioRespuestasDTO.getValorTexto().startsWith(FamiliarDTO.ABREV_FAM_PARENTESCO) || 
					cuestionarioRespuestasDTO.getValorTexto().startsWith(FamiliarDTO.ABREV_FAM_NOMBRE) || 
					cuestionarioRespuestasDTO.getValorTexto().startsWith(FamiliarDTO.ABREV_FAM_DOMICILIO) || 
					cuestionarioRespuestasDTO.getValorTexto().startsWith(FamiliarDTO.ABREV_FAM_FECHA_NAC)) 
			{
				try{
					String valorTexto = cuestionarioRespuestasDTO.getValorTexto();
					int indiceSeparador = valorTexto.indexOf(FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA);
					
					if(indiceSeparador > 0) {
						int indicadorFamiliar = Integer.parseInt(valorTexto.substring(3, indiceSeparador));
						
						FamiliarDTO familiar = familiares.get(indicadorFamiliar);
						if(familiar == null) {
							familiar = new FamiliarDTO();
							familiares.put(indicadorFamiliar, familiar);
						}
						
						familiar.definirValorCampo(valorTexto);
					} else {
						procesarRespuestaDiferenteFamiliar(cuestionarioRespuestasDTO, respuestasPregunta);
					}
				} catch(NumberFormatException nfe) {
					showException(nfe);
					LOG.info("Error en la obtencion de Paretesco en la pregunta: "+cuestionarioRespuestasDTO.getValorTexto());
					//la agregamos a la lista de preguntas:
					procesarRespuestaDiferenteFamiliar(cuestionarioRespuestasDTO, respuestasPregunta);
				}
			} else {
				procesarRespuestaDiferenteFamiliar(cuestionarioRespuestasDTO, respuestasPregunta);
			}
		}
		
		for(Map.Entry<Integer, FamiliarDTO> entry : familiares.entrySet()){
			FamiliarDTO familiar = entry.getValue();
			familiar.setClaveFamiliar(administradorClavesDAO.obtenerNuevaClaveFamiliar());
			
			cuestionario.addFamiliar(familiar);
		}
		
		for(CuestionarioRespuestaDTO cuestionarioRespuestasDTO : respuestasPregunta){
			int claveRespuestaPregunta = administradorClavesDAO.obtenerNuevaClaveRespuestaPregunta();
			
			cuestionarioRespuestasDTO.setClaveCuestionarioResp(claveCuestionarioRespuesta);
			cuestionarioRespuestasDTO.setClaveRespuestaPregunta(claveRespuestaPregunta);
		}
		
		
		preguntasDAO.agregarRespuestasCuestionario(cuestionario, respuestasPregunta, claveCuestionarioRespuesta, sesion);
		
		CalificacionDTO nivelRiesgo = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getNivelRiesgo());
		CalificacionDTO indicadorUpld = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(cuestionario.getIndicadorUpld());
		cuestionario.setNivelRiesgo(String.format("%s %s", nivelRiesgo.getCodigoIndicadorPersonas(), nivelRiesgo.getValorIndicadorPersonas()));
		cuestionario.setIndicadorUpld(String.format("%s %s", indicadorUpld.getCodigoIndicadorPersonas(), indicadorUpld.getValorIndicadorPersonas()));
		
		return cuestionario;
	}
	
	/**
	 * Procesa las respuestas que no se traten de familiares
	 * @param cuestionarioRespuestasDTO Contiene la informacion de la respuesta
	 * @param respuestasPregunta Lista de preguntas que seran guardadas y la que se asignara la nueva
	 */
	private void procesarRespuestaDiferenteFamiliar(CuestionarioRespuestaDTO cuestionarioRespuestasDTO, List<CuestionarioRespuestaDTO> respuestasPregunta){
		if(cuestionarioRespuestasDTO.getValorOpcion() > 0) {
			respuestasPregunta.add(cuestionarioRespuestasDTO);
		} else if(cuestionarioRespuestasDTO.getValorTexto() != null) {
			respuestasPregunta.add(cuestionarioRespuestasDTO);
		}
	}
	
	@Override
	public CuestionarioRespuestaDTO obtenerPreguntaPorSeccionAbreviatura(String idCuestionario, String seccion, String abreviatura, ArchitechSessionBean psession) throws BusinessException {
		return preguntasDAO.obtenerPreguntaPorSeccionAbreviatura(idCuestionario, seccion, abreviatura, psession);
	}
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasAsignadasPorAbreviaturas(String idCuestionario, String abreviaturas, ArchitechSessionBean psession) throws BusinessException {
		return preguntasDAO.obtenerPreguntasAsignadasPorAbreviaturas(idCuestionario, abreviaturas, psession);
	}
	
	@Override
	public PreguntaDTO obtenerPreguntaPorAbreviatura(String abreviatura, ArchitechSessionBean psession) 
	throws BusinessException {
		List<PreguntaDTO> preguntas = preguntasDAO.obtenerPreguntasPorAbreviaturas(abreviatura, psession);
		
		if(preguntas != null && !preguntas.isEmpty()) {
			return preguntas.get(0);
		}
		
		return null;
	}
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasCuestionarioPorClave(int claveCuestionario, ArchitechSessionBean psession) throws BusinessException {
		return preguntasDAO.obtenerPreguntasCuestionarioPorClave(claveCuestionario, psession);
	}
	
	@Override
	public void ejecutarActualizacionRespuesta(CuestionarioRespuestaDTO respuesta) throws BusinessException {
		preguntasDAO.ejecutarActualizacionRespuesta(respuesta);
	}
}
