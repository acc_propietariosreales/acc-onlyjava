/**
 * Isban Mexico
 *   Clase: CuestionarioIPBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio del cuestionario IP.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.PreguntasIpBO;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

import org.apache.log4j.Logger;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PreguntasIpBOImpl extends Architech implements PreguntasIpBO {
	/**
	 * PreguntasIpBOImpl.java  UID de tipo long
	 */
	private static final long serialVersionUID = -5051336403736340051L;
	/**Logger de bo*/
	private static final Logger LOG=Logger.getLogger(PreguntasIpBOImpl.class);
	/**
	 * Variable utilizada para declarar preguntasDAO
	 */
	@EJB
	private transient PreguntasDAO preguntasDAO;
	
	/**
	 * Dao para  administrador de Claves
	 */
	@EJB
	private transient AdministradorClavesDAO administradorClavesDAO;
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasCuestionarioIp(int claveCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		LOG.info("inicia a BO CuestionariosBOImpl.obtenerCuestionario");
		
		return preguntasDAO.obtenerPreguntasCuestionarioPorClave(claveCuestionario, psession);
	}

	@Override
	public void guardarRespuestasCuestionarioIp(CuestionarioDTO cuestionario, List<CuestionarioRespuestaDTO> respuestas, ArchitechSessionBean sesion)
	throws BusinessException {
		int claveCuestionarioRespuesta = administradorClavesDAO.obtenerNuevaClaveCuestionarioRespuesta();
		
		for(CuestionarioRespuestaDTO cuestionarioRespuestasDTO : respuestas){
			int claveRespuestaPregunta = administradorClavesDAO.obtenerNuevaClaveRespuestaPregunta();
			
			cuestionarioRespuestasDTO.setClaveCuestionarioResp(claveCuestionarioRespuesta);
			cuestionarioRespuestasDTO.setClaveRespuestaPregunta(claveRespuestaPregunta);
		}
		
		preguntasDAO.agregarRespuestasCuestionario(cuestionario, respuestas, claveCuestionarioRespuesta, sesion);
	}
}
