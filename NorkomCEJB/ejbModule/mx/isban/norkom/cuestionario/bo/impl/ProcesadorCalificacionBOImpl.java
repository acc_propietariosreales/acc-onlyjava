/**
 * Isban Mexico
 *   Clase: ProcesadorCalificacionBOImpl.java
 *   Descripcion: Componente para procesar la calificacion de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.norkom.schemas.wsdl.realtimelogicservices.RealTimeLogicEjbEndpoint;
import com.norkom.schemas.wsdl.realtimelogicservices.RealTimeLogicService;
import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.GeneraXmlBO;
import mx.isban.norkom.cuestionario.bo.ProcesadorCalificacionBO;
import mx.isban.norkom.cuestionario.dao.NivelRiesgoDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.util.Mensajes;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 * Objetivo: Componente para realizar la logica de negocio relacionada con la obtencion de las calificaciones de Norkom
 * Justificacion: A traves de su comopnente "calificarCliente" va a realizar el proceso de obtener la calificacion de Norkom lo cual incluye invocar al componente de generacion de XML, preparar e invocar al servicio web de calificacion y obtener las equivalencias.
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ProcesadorCalificacionBOImpl extends Architech implements ProcesadorCalificacionBO {

	/** serialVersionUID */
	private static final long serialVersionUID = -3805400278287168956L;
	/**identificador de buc**/
	private static final String TXT_BUC = "BUC";
	/**identificador de forma**/
	private static final String TXT_IDENTIFICADOR_FORM = "IdentificadorForm";
	
	
	/**identificador de fecha fromulario**/
	private static final String TXT_FECHA_FORMULARIO = "FechaFormulario";
	/**indicador upld*/
	private static final String TXT_IND_UPLD = "IndUPLD";
	/**identificador de Nivel riesgo**/
	private static final String TXT_NIVEL_RIESGO = "NivelRiesgo";
	/** */
	private static final int MILI_POR_SEGUNDO = 1000;
	
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(ProcesadorCalificacionBOImpl.class);
	
	/**
	 * Timeout para la ejecucion del servicio web de Norkom
	 */
	private static final int DEFAULT_TIMEOUT_WS = 10000;
	
	/**
	 * Dao para obtener los Parametros 
	 */
	@EJB
	private transient ParametrosDAO parametrosDAO;
	
	/**
	 * nivelRiesgoDAO Permite actualizar el Nivel de Riesgo y el Indicador UPLD en ACC
	 */
	@EJB
	private transient NivelRiesgoDAO nivelRiesgoDAO;
	
	/**
	 * BO para la Generacion de Xml
	 */
	@EJB
	private transient GeneraXmlBO generaXmlBO;
    
	@Override
	public CuestionarioDTO calificarCliente(String buc, CuestionarioDTO cuestionario, ArchitechSessionBean sesion)
	throws BusinessException {
		LOG.info(String.format("Inicia proceso de calificacion para el Cliente[%s] con IdCuestionario[%s]", 
								buc, cuestionario.getIdCuestionario()));
		
		ParametroDTO paramNivelRiesgo = parametrosDAO.obtenerParametroPorNombre("nivelRiesgoDummie");
		ParametroDTO paramIndicadorUpld = parametrosDAO.obtenerParametroPorNombre("indicadorUpldDummie");
		
		String messageData = generaXmlBO.generarEstructuraXML(cuestionario.getIdCuestionario(), sesion);
		
		LOG.info(String.format("XML generado para enviar a Norkom [%s]", messageData));
		
		try {
			if(paramNivelRiesgo == null || StringUtils.isBlank(paramNivelRiesgo.getValor()) ||
					paramIndicadorUpld == null || StringUtils.isBlank(paramIndicadorUpld.getValor())) 
			{
				obtenerCalificacion(cuestionario, messageData);

				cuestionario.setDummy(false);
			} else {
				//INICIO DE DUMMY
				LOG.info("Inicia Proceso de calificacion con valores Dummy, NO se utiliza el WS de Norkom");
				CalificacionDTO nivelRiesgo = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(paramNivelRiesgo.getValor());
				CalificacionDTO indicadorUpld = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(paramIndicadorUpld.getValor());
				cuestionario.setNivelRiesgo(nivelRiesgo.getValorIndicadorNorkom());
				cuestionario.setIndicadorUpld(indicadorUpld.getValorIndicadorNorkom());
				cuestionario.setDummy(true);
				//FIN DE DUMMY
			}
		} catch (MalformedURLException e) {
			LOG.info(String.format("Error al generar URL con el parametro [%s] causa[%s] mensaje[%s] para el IdCuestionario[%s]", 
									"wsdlLocationRealTime", e.getCause(), e.getMessage(), cuestionario.getIdCuestionario()));
			
			throw new mx.isban.norkom.cuestionario.exception.BusinessException(Mensajes.ERCLCT02.getCodigo(), Mensajes.ERCLCT02.getDescripcion(),e);
		} catch(WebServiceException wsdlEx) {
			LOG.info(String.format("Error al generar WSDL con el parametro [%s] causa[%s] mensaje[%s] para el IdCuestionario[%s]", 
					"wsdlLocationRealTime", wsdlEx.getCause(), wsdlEx.getMessage(), cuestionario.getIdCuestionario()));
			
			throw new mx.isban.norkom.cuestionario.exception.BusinessException(Mensajes.ERCLCT03.getCodigo(), Mensajes.ERCLCT03.getDescripcion(),wsdlEx);
		}
		
		guardarCalificacionNorkomEnACC(
				cuestionario.getClaveClienteCuestionario(), cuestionario.getNivelRiesgo(), cuestionario.getIndicadorUpld(), sesion);
		

		return cuestionario;
	}
	
	@Override
	public void obtenerCalificacion(CuestionarioDTO cuestionario, String messageData) throws MalformedURLException, BusinessException {
		LOG.info("Inicia Proceso de calificacion a traves del WS de Norkom");

		ParametroDTO paramWsdlLocation = parametrosDAO.obtenerParametroPorNombre("wsdlLocationRealTime");
		ParametroDTO paramMessageType = parametrosDAO.obtenerParametroPorNombre("wsMessageTypeRealTime");
		ParametroDTO paramMessageFunctionSequence = parametrosDAO.obtenerParametroPorNombre("wsMessageFunctionSequenceRealTime");
		ParametroDTO paramTimeOut = parametrosDAO.obtenerParametroPorNombre("TimeOutWebService");
		
		int timeoutMiliseconds = DEFAULT_TIMEOUT_WS;
		try {
			timeoutMiliseconds = Integer.parseInt(paramTimeOut.getValor());
		} catch(NumberFormatException nfe){
			LOG.error("No existe un valor valido para el parametro TimeOutWebService, se maneja valor por default");
		}
		
		URL wsdlURL = new URL(paramWsdlLocation.getValor());
		RealTimeLogicService ss = new RealTimeLogicService(wsdlURL);
        RealTimeLogicEjbEndpoint port = ss.getServicesManagerPort();
        
        Map<String, Object> requestContext = ((BindingProvider)port).getRequestContext();
        requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, timeoutMiliseconds);
        requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, timeoutMiliseconds);
        
        String timeoutSeconds = String.valueOf(timeoutMiliseconds / MILI_POR_SEGUNDO);        
        requestContext.put(com.ibm.wsspi.webservices.Constants.CONNECTION_TIMEOUT_PROPERTY , timeoutSeconds);
        requestContext.put(com.ibm.wsspi.webservices.Constants.WRITE_TIMEOUT_PROPERTY , timeoutSeconds);
        requestContext.put(com.ibm.wsspi.webservices.Constants.RESPONSE_TIMEOUT_PROPERTY , timeoutSeconds);
        
        LOG.debug("messageId:["+cuestionario.getIdCuestionario()+"]"+" messageType:["+paramMessageType.getValor()+"]"+"messageFunctionSequence:["+paramMessageFunctionSequence.getValor()+"]"+"messageData:["+messageData+"]");
        String resultadoCalificacion = port.processMessage(
        		cuestionario.getIdCuestionario(), paramMessageType.getValor(), paramMessageFunctionSequence.getValor(), messageData);
        LOG.debug("Finaliza llamado a Servicio Web de Norkom");
        
        if(resultadoCalificacion == null || resultadoCalificacion.isEmpty()) {
        	throw new BusinessException(Mensajes.ERCLCT01.getCodigo(), Mensajes.ERCLCT01.getDescripcion());
        }
        
        LOG.info(String.format("El resultado de la calificacion es [%s]", resultadoCalificacion));
        
        asignarValoresResultadoCalificacion(cuestionario, resultadoCalificacion);

		
	}
	
	/**
	 * Obtiene los valores de la calificaion equivalentes en Personas y los asigna al cuestionario
	 * @param cuestionario Cuestionario al que seran asignadas las calificaciones
	 * @param resultadoCalificacion Resultado de la calificación en Norkom KYC
	 * @throws BusinessException En caso de un error al obtener las equivalencias de las calificaciones
	 */
	private void asignarValoresResultadoCalificacion(CuestionarioDTO cuestionario, String resultadoCalificacion)
	throws BusinessException {
		String indicadorUpldNkm = obtenerValorRespuesta(TXT_IND_UPLD, resultadoCalificacion);
        String nivelRiesgoNkm = obtenerValorRespuesta(TXT_NIVEL_RIESGO, resultadoCalificacion);
        if(StringUtils.EMPTY.equals(nivelRiesgoNkm)){
        	throw new BusinessException(Mensajes.ERCLCT04.getCodigo(),Mensajes.ERCLCT04.getDescripcion());
        }
        String bucNkm = obtenerValorRespuesta(TXT_BUC, resultadoCalificacion);
        String fechaFormularioNkm = obtenerValorRespuesta(TXT_FECHA_FORMULARIO, resultadoCalificacion);
        String identificadorFormNkm = obtenerValorRespuesta(TXT_IDENTIFICADOR_FORM, resultadoCalificacion);
        
        LOG.info(String.format("Respuesta Nkm ::: BUC[%s] IdentificadorForm[%s] IndicadorUPLD[%s] Fecha[%s] NivelRiesgo[%s]", 
        		bucNkm, identificadorFormNkm, indicadorUpldNkm, fechaFormularioNkm, nivelRiesgoNkm));
		
		CalificacionDTO nivelRiesgoDTO = new CalificacionDTO();
        nivelRiesgoDTO.setValorIndicadorNorkom(nivelRiesgoNkm);
		CalificacionDTO indicadorUpldDTO = new CalificacionDTO();
		indicadorUpldDTO.setValorIndicadorNorkom(indicadorUpldNkm);
		
		CalificacionDTO nivelRiesgo = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(nivelRiesgoNkm);
		CalificacionDTO indicadorUpld = nivelRiesgoDAO.obtenerValoresPorIndicadorNorkom(indicadorUpldNkm);
		cuestionario.setNivelRiesgo(nivelRiesgo.getValorIndicadorNorkom());
		cuestionario.setIndicadorUpld(indicadorUpld.getValorIndicadorNorkom());
	}

	/**
	 * Obtiene la respuesta del campo en el XML que envia Norkom
	 * @param campo Campo del que se desea obtener la respuesta
	 * @param xmlNorkom XML de respuesta de Norkom
	 * @return La respuesta obtenida del XML
	 */
	private String obtenerValorRespuesta(String campo, String xmlNorkom) {
		int indiceCampo = xmlNorkom.indexOf(campo);
		if(indiceCampo > 0) {
			String buscadorRespuesta = "<value><![CDATA[";
			int indiceBusqueda = xmlNorkom.indexOf(buscadorRespuesta, indiceCampo);
			int indiceInicioRespuesta = indiceBusqueda + buscadorRespuesta.length();
			int indiceFinRespuesta = xmlNorkom.indexOf(']', indiceInicioRespuesta);
			
			if(indiceBusqueda >= 0 && indiceFinRespuesta >= 0) {
				return xmlNorkom.substring(indiceInicioRespuesta, indiceFinRespuesta);
			}
		}
		
		return StringUtils.EMPTY;
	}
	
	@Override
	public void guardarCalificacionNorkomEnACC(int claveClienteCuest, String nivelRiesgo, String indicadorUpld, ArchitechSessionBean psession) throws BusinessException {
		nivelRiesgoDAO.guardarCalificacionNorkom(claveClienteCuest, nivelRiesgo, indicadorUpld);
	}

}
