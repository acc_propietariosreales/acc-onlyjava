package mx.isban.norkom.cuestionario.bo.impl;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bo.RegimenSimplificadoBO;
import mx.isban.norkom.cuestionario.bo.ValidadorRiesgoA1BO;
import mx.isban.norkom.cuestionario.dto.NivelRiesgoDTO;

/**
 * Session Bean implementation class RegimenSimplificadoBO
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class RegimenSimplificadoBOImpl extends Architech implements RegimenSimplificadoBO {
	/**
	 * Serial UID  de tipo long
	 */
	private static final long serialVersionUID = -7786340218995960811L;
	/**
	 * Ejb para validadorRiesgoA1BO
	 ***/
	@EJB
	ValidadorRiesgoA1BO validadorRiesgoA1BO;
    /**
     * Default constructor. 
     */
    public RegimenSimplificadoBOImpl() {
       
    }
	@Override
	public NivelRiesgoDTO esRegimenSimplificado(RequestObtenerCuestionario peticion,
			ArchitechSessionBean sesion) throws BusinessException {
		//
		NivelRiesgoDTO nivelRiesgoDTO= validadorRiesgoA1BO.esRegimenSimplificadoAx(peticion, sesion);
		
		return nivelRiesgoDTO;
	}
	/**
	 * @return the validadorRiesgoA1BO
	 */
	public ValidadorRiesgoA1BO getValidadorRiesgoA1BO() {
		return validadorRiesgoA1BO;
	}
	/**
	 * @param validadorRiesgoA1BO 
	 * el validadorRiesgoA1BO a agregar
	 */
	public void setValidadorRiesgoA1BO(ValidadorRiesgoA1BO validadorRiesgoA1BO) {
		this.validadorRiesgoA1BO = validadorRiesgoA1BO;
	}

}
