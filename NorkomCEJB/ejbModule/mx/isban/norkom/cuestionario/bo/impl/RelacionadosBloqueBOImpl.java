/**
 *   Isban Mexico
 *   Clase: RelacionadosBloqueBOImpl.java
 *   Descripcion: Componente para realizar la logica 
 *   de negocio de los relacionados por bloque e
 *   ingresarlos a DB a travez del dao
 *   correspondiente.
 *
 *   Control de Cambios:
 *   1.0 Jun 06, 2017 Stefanini - Creacion
 *   1.1 17/07/2017   Stefanini -LFER revision de sonar
 *   
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.RelacionadosBloqueBO;
import mx.isban.norkom.cuestionario.bo.RelacionadosEnvioWSBO;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dao.RelacionadosBloqueDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosRequestDTO;

import org.apache.commons.lang.StringUtils;
/***
 * BO RelacionadosBloqueBOImpl para la insercion 
 * de relacionados por bloque  en la DB atravez
 * del DAO de RelacionadosBloqueDAO
 * 
 * la clase extiende Architech
 * e implementa RelacionadosBloqueBO
 * 
 * @author lespinosa
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class RelacionadosBloqueBOImpl extends Architech implements RelacionadosBloqueBO {
	/**
	 * Variable utilizada para declarar 
	 * serialVersionUID de tipo
	 * Long
	 */
	private static final long serialVersionUID = 7391340431606963173L;

	/**
	 * Codigo para la advertencia de registro erroneos
	 */
	private static final String COD_REGISTROS_ERRONEOS = "WNRLGD00";

	/**
	 * Variable utilizada para describir TIPO_RELACION_PROVEEDOR
	 */
	private static final String TIPO_RELACION_PROVEEDOR = "P";
	/**
	 * Variable utilizada para describir TIPO_RELACION_ACCIONISTA
	 */
	private static final String TIPO_RELACION_ACCIONISTA = "A";
	/**
	 * Variable utilizada para describir TIPO_RELACION_BENEFICIARIO
	 */
	private static final String TIPO_RELACION_BENEFICIARIO = "B";
	/**
	 * Variable utilizada para describir TIPO_RELACION_INTERVINIENTE
	 */
	private static final String TIPO_RELACION_INTERVINIENTE = "I";
	/**
	 * Variable utilizada para declarar 
	 * el EJB RelacionadosBloqueDAO
	 * de nombre relacionadosDAO
	 */
	@EJB
	private transient RelacionadosBloqueDAO relacionadosDAO;
	
	/**
	 * Variable utilizada para declarar 
	 * el EJB CuestionarioDAO con nombre de
	 * cuestionarioDAO
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	/**
	 * Objeto para la obtencion de claves de BD
	 * AdministradorClavesDAO con nombre de
	 * administradorClavesDAO
	 */
	@EJB
	private transient AdministradorClavesDAO administradorClavesDAO;

	/**
	 * Obtencion de los parametros de BD
	 * ParametrosDAO con nombre de
	 * parametrosDAO
	 */
	
	@EJB
	private transient ParametrosDAO parametrosDAO;
	/**
	 * EJB de RelacionadosEnvioWSBO
	 * para el BO que realiza el envio 
	 * de los relacionados a Norkom
	 * via WS
	 */
	@EJB
	private transient RelacionadosEnvioWSBO envioWSBO;
	/**
	 * constante para el 
	 * Formato de fecha 
	 * FORMATO_FECHA_YYYY_MM_DD
	 * con loacale de US
	 */
	private static final SimpleDateFormat FORMATO_FECHA_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

	
	@Override
	public List<RelacionadoDTO> agregarRelacionados(RelacionadosRequestDTO relacionadosListWs, ArchitechSessionBean psession) 
	throws BusinessException {
		RelacionadosRequestDTO relacionadosListWsIn=relacionadosListWs;
		List<RelacionadoDTO> relacionadosErroneos = new ArrayList<RelacionadoDTO>();
		List<RelacionadoDTO> relacionadosInsert = new ArrayList<RelacionadoDTO>();
		int claveClienteCuestionario = 0;
		ParametroDTO paramTipoPersona = parametrosDAO.obtenerParametroPorNombre("tipoPersonaPred");
		ParametroDTO paramSubtipoPersona = parametrosDAO.obtenerParametroPorNombre("subtipoPersonaPred");
		ParametroDTO paramDivisa = parametrosDAO.obtenerParametroPorNombre("divisaPred");
		
		String idCuestionario = relacionadosListWsIn.getIdCuestionario();
		
		relacionadosListWsIn=validacionesRelacionados(relacionadosListWsIn, idCuestionario);
		
		CuestionarioWSDTO cuestionarioWS = cuestionarioDAO.obtenerDatosGeneralesCuestionarioPorId(idCuestionario, psession);
		
		claveClienteCuestionario = obtenerClaveClienteCuestionario(psession,relacionadosListWsIn,
				paramTipoPersona, paramSubtipoPersona,
				paramDivisa, idCuestionario, cuestionarioWS);
		
		
		
		for(RelacionadoDTO dtoRelacionado : relacionadosListWsIn.getRelacionados()){
			try {
				//Trata de hacer la conversion para validar que la fecha es correcta
				Date fecha = FORMATO_FECHA_YYYY_MM_DD.parse(dtoRelacionado.getFechaNacimiento());
				
				if(fecha == null) {
					dtoRelacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
					dtoRelacionado.setDescOperacion("Fecha incorrecta");
					relacionadosErroneos.add(dtoRelacionado);
				} else if(!TIPO_RELACION_ACCIONISTA.equals(dtoRelacionado.getTipoRelacion()) 
						&& !TIPO_RELACION_BENEFICIARIO.equals(dtoRelacionado.getTipoRelacion()) &&
						!TIPO_RELACION_INTERVINIENTE.equals(dtoRelacionado.getTipoRelacion()) 
						&& !TIPO_RELACION_PROVEEDOR.equals(dtoRelacionado.getTipoRelacion())){
					dtoRelacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
					dtoRelacionado.setDescOperacion("Tipo de Relaci\u00F3n incorrecta");
					relacionadosErroneos.add(dtoRelacionado);
				} 
				if(!GeneradorCuestionarioGenerico.IND_PERSONA_FISICA.equals(dtoRelacionado.getTipoPersona()) && 
						!GeneradorCuestionarioGenerico.IND_PERSONA_MORAL.equals(dtoRelacionado.getTipoPersona())){
					dtoRelacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
					dtoRelacionado.setDescOperacion("Tipo de Persona incorrecta");
					relacionadosErroneos.add(dtoRelacionado);
				} else {
					relacionadosInsert.add(dtoRelacionado);
				}
			} catch (ParseException e) {
				showException(e);
				dtoRelacionado.setCodigoOperacion(COD_REGISTROS_ERRONEOS);
				dtoRelacionado.setDescOperacion("Fecha incorrecta");
				error(String.format("Error al convertir fecha[%s] Causa[%s] Mensaje[%s]",dtoRelacionado.getFechaNacimiento(), e.getCause(), e.getLocalizedMessage()));
				relacionadosErroneos.add(dtoRelacionado);
			}
		}
		
		relacionadosErroneos.addAll(relacionadosDAO.agregarRelacionados(relacionadosInsert, claveClienteCuestionario,relacionadosListWsIn.getBloque(), psession));
		relacionadosInsert.removeAll(relacionadosErroneos);
		String codError = envioWSBO.enviaRelacionadosWS(relacionadosInsert,relacionadosListWsIn, psession );
		
		debug("Termina agregar Relacionados por Bloque, envio a Norkom:"+codError);
		return relacionadosErroneos;
	}
	
	




	/**
	 * Metodo para validar la clave cliente cuestionario
	 * si existe o se pediar crear una nueva
	 * con los datos ingresados en el WS
	 * 
	 * @param psession ArchitechSessionBean
	 * @param relacionadosListWsIn RelacionadosRequestDTO
	 * @param paramTipoPersona ParametroDTO
	 * @param paramSubtipoPersona ParametroDTO
	 * @param paramDivisa ParametroDTO
	 * @param idCuestionario String
	 * @param cuestionarioWS CuestionarioWSDTO
	 * @return int clave de cliente cuestionario
	 * @throws BusinessException con mensaje de error
	 */
	private int obtenerClaveClienteCuestionario(ArchitechSessionBean psession,
			RelacionadosRequestDTO relacionadosListWsIn,
			ParametroDTO paramTipoPersona, ParametroDTO paramSubtipoPersona,
			ParametroDTO paramDivisa, String idCuestionario,
			CuestionarioWSDTO cuestionarioWS) throws BusinessException {
		int claveClienteCuestionario;
		/*
		 * Si ya existe el cuestionario se obtiene la clave Cliente-Cuestionario para asignarle los Relacionados
		 * En caso de que no exista se agrega un nuevo registro.
		 */
		if(cuestionarioWS != null && cuestionarioWS.getClaveCuestionario() > 0) {
			claveClienteCuestionario = cuestionarioWS.getClaveCuestionario();
		} else {
			claveClienteCuestionario = administradorClavesDAO.obtenerNuevaClaveClienteCuestionario();
			info(String.format("Se obtiene la clave Cliente - Cuestionario [%s] para la insercion de los relacionados", claveClienteCuestionario));
			
			CuestionarioDTO cuestionarioDTO = creCuestionarioDTO(claveClienteCuestionario, paramTipoPersona, paramSubtipoPersona, paramDivisa, idCuestionario);
			
			CuestionarioDTO tipoCuestionarioBD = cuestionarioDAO.obtenerTipoCuestionario(cuestionarioDTO, "IP", psession);
			cuestionarioDTO.setClaveCuestionario(tipoCuestionarioBD.getClaveCuestionario());
			
			relacionadosDAO.agregarCuestionarioBasico(cuestionarioDTO,relacionadosListWsIn.getBuc(), psession);
			
		}
		return claveClienteCuestionario;
	}
	/**
	 * Metodo auxiliar para la validacion de los relacionados
	 * en caso de que falle se enviara una excepcion
	 * @param relacionadosListWs  RelacionadosRequestDTO
	 * @param idCuestionario String
	 * @throws BusinessException con mensaje de error
	 */
	private RelacionadosRequestDTO validacionesRelacionados(
			RelacionadosRequestDTO relacionadosListWs, String idCuestionario)
			throws BusinessException {
		RelacionadosRequestDTO relacionadosListWsin=relacionadosListWs;
		if(StringUtils.isBlank(idCuestionario)) {
			throw new BusinessException(Mensajes.ERRLGD01.getCodigo(), Mensajes.ERRLGD01.getDescripcion());
		}
		int bloque=-1;
		try{
		  bloque=Integer.parseInt(relacionadosListWsin.getBloque());
		}catch(NumberFormatException nfe){
			showException(nfe);
			relacionadosListWsin.setBloque("");
		}
		if(StringUtils.isBlank(relacionadosListWsin.getBloque()) || bloque<=0) {
			throw new BusinessException(Mensajes.ERRLGD01.getCodigo(),"Es necesario proporcionar un Numero de Bloque mayor a 0, sin este dato no es posible realizar la operaci\u00F3n");
		}
		return relacionadosListWsin;
	}
	/**
	 * metodo auxiliar para la funcionalidad de 
	 * crear dto de cuestionario con los datos de
	 * ingreso en metodo principal
	 * @param claveClienteCuestionario clave de cliente cuest
	 * @param paramTipoPersona tipo de persona
	 * @param paramSubtipoPersona subtipo de persona
	 * @param paramDivisa divisa
	 * @param idCuestionario id del cuestionario
	 * @return CuestionarioDTO con los datos enviados
	 */
	private CuestionarioDTO creCuestionarioDTO(int claveClienteCuestionario,ParametroDTO paramTipoPersona,ParametroDTO paramSubtipoPersona,ParametroDTO paramDivisa,String idCuestionario) {
		CuestionarioDTO cuestionarioDTO=new CuestionarioDTO();
		cuestionarioDTO.setClaveClienteCuestionario(claveClienteCuestionario);
		cuestionarioDTO.setTipoPersona(paramTipoPersona.getValor());
		cuestionarioDTO.setSubtipoPersona(paramSubtipoPersona.getValor());
		cuestionarioDTO.setDivisa(paramDivisa.getValor());
		cuestionarioDTO.setIdCuestionario(idCuestionario);
		return cuestionarioDTO;
	}	
	

}
