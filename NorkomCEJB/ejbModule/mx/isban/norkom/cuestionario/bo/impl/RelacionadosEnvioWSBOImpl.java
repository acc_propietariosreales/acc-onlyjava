/**
 * Isban Mexico
 *   Clase: RelacionadosEnvioWSBOImpl.java
 *   Descripcion: Componente para realizar la 
 *   logica de negocio del envio de relacionados
 *   a norkom por medio de el WS de Norkom.
 *
 *   Control de Cambios:
 *   1.0 Jul 14, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;

import com.norkom.schemas.wsdl.realtimelogicservices.RealTimeLogicEjbEndpoint;
import com.norkom.schemas.wsdl.realtimelogicservices.RealTimeLogicService;
import com.sun.xml.internal.ws.client.BindingProviderProperties;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.RelacionadosEnvioWSBO;
import mx.isban.norkom.cuestionario.dao.EnvioRelacionadosDAO;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.util.CreaFormatoXMLRelacionados;
import mx.isban.norkom.cuestionario.util.EstatusRelacionados;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosRequestDTO;
import mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO;

/** 
 * clase RelacionadosEnvioWSBOImpl
 * extiende de Architech
 * implementa la interfaz de 
 * RelacionadosEnvioWSBO.
 * 
 * Implementacion de la interfaz 
 * para el envio de Relacionados
 * a norkom por bloques
 * 
 * @author Stefanini (lespinosa) 14/07/2017
 * 
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class RelacionadosEnvioWSBOImpl extends Architech implements RelacionadosEnvioWSBO {

	/**
	 * Serila UID de tipo
	 * Long
	 * para identificar la clase
	 * en serializacion
	 */
	private static final long serialVersionUID = -4699443173611111138L;
	/**
	 * constante para el
	 * tiempo en segundos
	 *  
	 * */
	private static final int MILI_POR_SEGUNDO = 1000;
	
	/**
	 * Timeout para la ejecucion del servicio web de Norkom
	 */
	private static final int DEFAULT_TIMEOUT_WS = 10000;
	/**
	 * constante para almacenar la
	 * llave de wsdlLocationRealTime
	 * WSDL_LOCATION
	 */
	private static final String WSDL_LOCATION="wsdlLocationRealTime";
	/**
	 * constante ara el mensaje
	 * que se envia en caso
	 * de error en el envio
	 * de relacionados
	 * a norkom
	 * MENSAJE_ERROR
	 */
	private static final String MENSAJE_ERROR="Error al generar WSDL con el parametro [%s] causa[%s] mensaje[%s] para el IdCuestionario[%s]";
	/**
	 * EJB del DAO de parametros
	 * para la obtencion de variables
	 * en DB
	 */
	@EJB
	private transient ParametrosDAO parametrosDAO;
	/**
	 * EJB del BO de EnvioRelacionadosDAO
	 * con nombre de instancia
	 * envioRelacionadosDAO
	 */
	@EJB
	private transient EnvioRelacionadosDAO envioRelacionadosDAO;
	@Override
	public String enviaRelacionadosWS(List<RelacionadoDTO> relacionados,
			RelacionadosRequestDTO entradaDto, ArchitechSessionBean psession) {
		String codError =null;
		try{
			codError = agregarRelacionadoNorkom(relacionados,entradaDto.getIdCuestionario(),entradaDto.getBloque(),entradaDto.getBuc());
			//Terminamos operaciones
			RequestDatosRelacionadosWSDTO entrada = new RequestDatosRelacionadosWSDTO();
			entrada.setIdCuestionario(entradaDto.getIdCuestionario());
			entrada.setNumeroBloque(entradaDto.getBloque());
			envioRelacionadosDAO.actualizarEstatusRel(entrada, EstatusRelacionados.ENVIADO.toString(), psession);
			info("Codigo de error: " + codError);
		} catch (MalformedURLException e) {
			info(String.format("Error al generar URL con el parametro [%s] causa[%s] mensaje[%s] para el IdCuestionario[%s]", 
									WSDL_LOCATION, e.getCause(), e.getMessage(), entradaDto.getIdCuestionario()));
			showException(e);
			codError="Error MURLE"+e.getMessage();
		} catch(WebServiceException wsdlEx) {
			info(String.format(MENSAJE_ERROR, 
					WSDL_LOCATION, wsdlEx.getCause(), wsdlEx.getMessage(), entradaDto.getIdCuestionario()));
			showException(wsdlEx);
			codError="Error WE"+wsdlEx.getMessage();
		} catch (BusinessException e) {
			info(String.format(MENSAJE_ERROR, 
					WSDL_LOCATION, e.getCause(), e.getMessage(), entradaDto.getIdCuestionario()));
			showException(e);
			codError="Error BE"+e.getMessage();
		} catch (TransformerException e) {
			info(String.format(MENSAJE_ERROR, 
					WSDL_LOCATION, e.getCause(), e.getMessage(), entradaDto.getIdCuestionario()));
			showException(e);
			codError="Error TE"+e.getMessage();
		} catch (ParserConfigurationException e) {
			info(String.format(MENSAJE_ERROR, 
					WSDL_LOCATION, e.getCause(), e.getMessage(), entradaDto.getIdCuestionario()));
			showException(e);
			codError="Error PCE"+e.getMessage();
		}
		return codError;
	}
	
	
	/**
	 * Metodo principal par aggregar los rleacionados
	 * a norkom
	 * se valda si la bandera de offline no esta activa
	 * en caso afrimativo se enviara un mensaje
	 * y no se realiza el envio a norkom
	 * @param relacionados List<RelacionadoDTO>
	 * @param idCuestionario id de formulario
	 * @param numBloque numero de bloque
	 * @param buc buc del usuario
	 * @return mensaje de la operacion
	 * @throws BusinessException con mensaje de error
	 * @throws MalformedURLException con mensaje de error
	 * @throws TransformerException con mensaje de error
	 * @throws ParserConfigurationException con mensaje de error
	 */
	public String agregarRelacionadoNorkom(List<RelacionadoDTO> relacionados,String idCuestionario,String numBloque,String buc) throws BusinessException, MalformedURLException, TransformerException, ParserConfigurationException {
		info("Inicia Proceso de calificacion a traves del WS de Norkom");
		//Validamos si el parametros de NORKOM_WS_PR es 1 sera offline
		debug("Relacionados a agregar:"+relacionados.size());
		if(parametroNorkomOffLine()){
			return "NORKOM --> OffLine";
		}
		String codError="Error";
		ParametroDTO paramWsdl = parametrosDAO.obtenerParametroPorNombre(WSDL_LOCATION);
		ParametroDTO paramMessageTypePR = parametrosDAO.obtenerParametroPorNombre("wsMessageTypeRealTimePR");
		ParametroDTO paramMessageFunctionSeqPR = parametrosDAO.obtenerParametroPorNombre("wsMessageFunctionSequenceRealTimePR");
		ParametroDTO paramTimeOut = parametrosDAO.obtenerParametroPorNombre("TimeOutWebService");
		
		int timeoutMiliseconds = DEFAULT_TIMEOUT_WS;
		try {
			timeoutMiliseconds = Integer.parseInt(paramTimeOut.getValor());
		} catch(NumberFormatException nfe){
			error("No existe un valor valido para el parametro TimeOutWebService, se maneja valor por default");
		}
		
		URL wsdlURL = new URL(paramWsdl.getValor());
		RealTimeLogicService ss = new RealTimeLogicService(wsdlURL);
        RealTimeLogicEjbEndpoint portPR = ss.getServicesManagerPort();
        
        Map<String, Object> requestContext = ((BindingProvider)portPR).getRequestContext();
        requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, timeoutMiliseconds);
        requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, timeoutMiliseconds);
        
        String timeoutSeconds = String.valueOf(timeoutMiliseconds / MILI_POR_SEGUNDO);        
        requestContext.put(com.ibm.wsspi.webservices.Constants.CONNECTION_TIMEOUT_PROPERTY , timeoutSeconds);
        requestContext.put(com.ibm.wsspi.webservices.Constants.WRITE_TIMEOUT_PROPERTY , timeoutSeconds);
        requestContext.put(com.ibm.wsspi.webservices.Constants.RESPONSE_TIMEOUT_PROPERTY , timeoutSeconds);
        CreaFormatoXMLRelacionados formatoXML= new CreaFormatoXMLRelacionados(); 
        String messageData= formatoXML.creaFormatoXML(relacionados,idCuestionario,numBloque,buc);
        
        debug("messageId:["+idCuestionario+"]"+" messageType:["+paramMessageTypePR.getValor()+"]"+"messageFunctionSequence:["+paramMessageFunctionSeqPR.getValor()+"]"+"messageData:["+messageData+"]");
        String resultadoAltaPR = portPR.processMessage(
        		idCuestionario, paramMessageTypePR.getValor(), paramMessageFunctionSeqPR.getValor(), messageData);
        debug("Finaliza llamado a Servicio Web de Norkom");
        
        if(resultadoAltaPR == null || resultadoAltaPR.isEmpty()) {
        	codError="NO se Obtuvo Respuesta";
        }else{
        	codError=resultadoAltaPR;
        }
        
        info(String.format("El resultado de la Alta es [%s]", resultadoAltaPR));
        
		return codError;
	}
	
	
	
	/**
	 * Metodo para obtener el parametro de norkom 
	 * para saber si se trabajara con offline de norkom
	 * @return boolean si esta offline o false si no
	 */
	private boolean parametroNorkomOffLine() {
		try {
			ParametroDTO dto=parametrosDAO.obtenerParametroPorNombre("PR_NORKOM_OFF");
			if("1".equals(dto.getValor())){
				return true;
			}
		} catch (BusinessException e) {
			showException(e);
			return false;
		}
		return false;
	}

}
