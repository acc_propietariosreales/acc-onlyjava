/**
 * Isban Mexico
 *   Clase: ValidadorContratacionMismoDiaBOImpl.java
 *   Descripcion: Auxiliar para las operaciones de validacion de contratacion de mismo dia.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bo.ValidadorContratacionMismoDiaBO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;

/**
 * @author jmquillo
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ValidadorContratacionMismoDiaBOImpl extends Architech implements ValidadorContratacionMismoDiaBO {
	/**
	 * Identificador utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = -9080842191109159225L;

	/**
	 * Longitud de un Identificador de Cuestionario
	 */
	private static final int LONGITUD_ID_CUESTIONARIO = 16;
	
	/**
	 * Objeto por el cual se maneja la informacion de los cuestionarios en la base de datos
	 */
	@EJB
	private transient CuestionarioDAO cuestionarioDAO;
	
	@Override
	public List<CuestionarioDTO> productosContratadosMismoDia(String buc, String nivelRiesgo, String idCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		if(buc == null || buc.isEmpty()){
			return null;
		}

		if(nivelRiesgo == null || nivelRiesgo.isEmpty()){
			return null;
		}
		
		if(idCuestionario == null || idCuestionario.isEmpty() || idCuestionario.length() != LONGITUD_ID_CUESTIONARIO){
			return null;
		}
		
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		ClienteDTO cliente = new ClienteDTO();
		cuestionario.setCliente(cliente);
		
		cliente.setCodigoCliente(buc);
		cuestionario.setNivelRiesgo(nivelRiesgo);
		cuestionario.setIdCuestionario(idCuestionario);
		cuestionario.setIdAplicacion(idCuestionario.substring(0, 2));
		
		List<CuestionarioDTO> cuestionarios = 
				cuestionarioDAO.obtenerDatosGeneralesCuestionarioMismoClienteMismoDia(cuestionario, psession);
		
		return cuestionarios;
	}

}
