/**
 * Isban Mexico
 *   Clase: PaisesBOImpl.java
 *   Descripcion: Componente para realizar la logica de negocio de los paises.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.bo.OpcionesPreguntaBO;
import mx.isban.norkom.cuestionario.bo.PreguntasIcBO;
import mx.isban.norkom.cuestionario.bo.ValidadorEntidadFinancieraBO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ValidadorEntidadFinancieraBOImpl extends Architech implements ValidadorEntidadFinancieraBO {
	/**
	 * Id utilizado para la serializacion de un objeto
	 */
	private static final long serialVersionUID = 7503075138145210151L;

	/**
	 * BO para la obtencion de preguntas
	 */
	@EJB
	private transient PreguntasIcBO preguntasIcBO;
	
	/**
	 * 
	 * BO para la obtencion de las opciones de una pregunta
	 */
	@EJB
	private transient OpcionesPreguntaBO opcionesPreguntaBO;
	
	@Override
	public boolean entidadEstablecidaFisicamente(String idCuestionario, ArchitechSessionBean sesion) throws BusinessException {
		//Obtengo la respuesta de la pregunta "Tiene un familiar PEP"
		CuestionarioRespuestaDTO preguntaEstaEstablecidaMexico = 
				preguntasIcBO.obtenerPreguntaPorSeccionAbreviatura(idCuestionario, SECCION_ACTIVIDAD_ECON, ABREV_ESTABLECIDA_MEXICO, sesion);
		
		//Obtengo las opciones de esa respuesta
		ResponseOpcionesPregunta response = 
				opcionesPreguntaBO.obtenerOpcionesPorSeccionAbreviatura(SECCION_ACTIVIDAD_ECON, ABREV_ESTABLECIDA_MEXICO, sesion);
		List<OpcionesPreguntaDTO> opciones = response.getLstOpcionesPreguntas();
		
		for(OpcionesPreguntaDTO opcion : opciones) {
			//Si la opcion es "SI" y es la que tengo almacenada entonces se asigna TRUE a variable del Cuestionario

				if(GeneradorCuestionarioGenerico.IND_ENTIDAD_ESTABLECIDA_FISICA.equals(opcion.getValor()) && preguntaEstaEstablecidaMexico != null 
						&& Integer.parseInt(opcion.getIdOpcion()) == preguntaEstaEstablecidaMexico.getValorOpcion()) 
				{
					return true;
				}	
		}
		
		return false;
	}

}
