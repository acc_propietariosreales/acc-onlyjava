/**
 * Isban Mexico
 *   Clase: ValidadorRiesgoA1BOImpl.java
 *   Descripcion: Auxiliar para las operaciones de validacion de riesgo.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.cuestionario.bo.ValidadorEntidadFinancieraBO;
import mx.isban.norkom.cuestionario.bo.ValidadorRiesgoA1BO;
import mx.isban.norkom.cuestionario.dao.ObtenerCuestionarioWSDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.NivelRiesgoDTO;

/**
 * @author jmquillo
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ValidadorRiesgoA1BOImpl extends Architech implements ValidadorRiesgoA1BO {
	
	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 1809947794100866691L;
	/**
	 * ValidadorEntidadFinancieraBO validadorEntidadFinancieraBO
	 */
	@EJB
	private transient ValidadorEntidadFinancieraBO validadorEntidadFinancieraBO;
	/**
	 * DAO para obtener los datos del PDF
	 */
	@EJB
	private transient ObtenerCuestionarioWSDAO obtenerPdfDAOWS;

	/**
	 * Define si un cliente es del tipo regimen simplificado
	 * @param peticion Contiene la informacion que se va a validar
	 * @param sesion Objeto de sesion de Agave
	 * @return NivelRiesgoDTO con los datos obtenidos para ña validacion de regimen simplficado
	 * @throws BusinessException En caso de error en la informacion o en las consultas de BD
	 */
	private NivelRiesgoDTO obtenRegimenSimplificado(RequestObtenerCuestionario peticion, ArchitechSessionBean sesion)
	throws BusinessException {
		ResponseObtenerCuestionario response = new ResponseObtenerCuestionario();
		CuestionarioDTO cuestionarioDTO = new CuestionarioDTO();
		ActividadEconomicaDTO actividad = new ActividadEconomicaDTO();
		NivelRiesgoDTO nivelRiesgo= new NivelRiesgoDTO();
		
		response = obtenerPdfDAOWS.obtenerDatosNivelRiesgo(peticion, sesion);
		cuestionarioDTO = response.getCuestionarioDTO();
		actividad = cuestionarioDTO.getActividadEspecifica();
		nivelRiesgo.setEntidadFinanciera(actividad.getEntidadFinanciera());
		//nivelRiesgo = 
		nivelRiesgo.setNivelRiesgo(cuestionarioDTO.getNivelRiesgo());
		//paisRiesgo = 
		nivelRiesgo.setPaisBajoRiesgo(cuestionarioDTO.getPaisBajoRiesgo());
		nivelRiesgo.setIdCuestionario(cuestionarioDTO.getIdCuestionario());
		nivelRiesgo.setPresenciaFisica(validadorEntidadFinancieraBO.entidadEstablecidaFisicamente(nivelRiesgo.getIdCuestionario(), sesion));
		return nivelRiesgo;	
	}

	@Override
	public boolean esRegimenSimplificado(RequestObtenerCuestionario peticion,
			ArchitechSessionBean sesion) throws BusinessException {
		NivelRiesgoDTO nivelRiesgoDTO=obtenRegimenSimplificado(peticion, sesion);
		if("S".equals(nivelRiesgoDTO.getEntidadFinanciera()) && "S".equals(nivelRiesgoDTO.getPaisBajoRiesgo()) && "A1".equals(nivelRiesgoDTO.getNivelRiesgo()) && nivelRiesgoDTO.isPresenciaFisica()){
				return true;
		}
		return false;	
	}

	@Override
	public NivelRiesgoDTO esRegimenSimplificadoAx(RequestObtenerCuestionario peticion,
			ArchitechSessionBean sesion) throws BusinessException {
		NivelRiesgoDTO nivelRiesgoDTO=obtenRegimenSimplificado(peticion, sesion);
		if("S".equals(nivelRiesgoDTO.getEntidadFinanciera()) && "S".equals(nivelRiesgoDTO.getPaisBajoRiesgo()) && "A1".equals(nivelRiesgoDTO.getNivelRiesgo()) && nivelRiesgoDTO.isPresenciaFisica()){
				nivelRiesgoDTO.setSimplificadoA1(true);
		}
		if("S".equals(nivelRiesgoDTO.getEntidadFinanciera()) && "S".equals(nivelRiesgoDTO.getPaisBajoRiesgo()) && nivelRiesgoDTO.isPresenciaFisica()){
			if(!"A1".equals(nivelRiesgoDTO.getNivelRiesgo())){
				nivelRiesgoDTO.setSimplificadoAx(true);
			}
		}
		return nivelRiesgoDTO;
	}

}
