/**
 * Isban Mexico
 *   Clase: ActividadEconomicaDAO.java
 *   Descripcion: Interfaz para el DAO ActividadEconomicaDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface ActividadEconomicaDAO {
	/**
	 * String SQL_OBTENER_ACTIVIDAD_POR_CODIGO
	 */
	public static final String SQL_OBTENER_ACTIVIDAD_POR_CODIGO = 
			"SELECT CVE_ACT_ECON_PK, DSC_ACT_ECON, VAL_TIPO_ACT_ECON, FLG_ENT_FIN, COD_ACT_ECON, FLG_SECTR FROM NKM_MX_CAT_ACTIV_ECON_DRO WHERE TRIM(COD_ACT_ECON) = '%s' AND VAL_TIPO_ACT_ECON='%s'";

	/**
	 * String SQL_OBTENER_ACTIVIDAD_POR_CODIGO
	 */
	public static final String SQL_OBTENER_ACTIVIDAD_POR_CODIGO_PK = 
			"SELECT CVE_ACT_ECON_PK, DSC_ACT_ECON, VAL_TIPO_ACT_ECON, FLG_ENT_FIN, COD_ACT_ECON, FLG_SECTR FROM NKM_MX_CAT_ACTIV_ECON_DRO WHERE CVE_ACT_ECON_PK = '%s' AND VAL_TIPO_ACT_ECON='%s'";
	/**
	 * @param codigoActividad codigo de la actividad
	 * @return ActividadEconomicaDTO con resultado
	 * @throws BusinessException con mensaje de error 
	 */
	public ActividadEconomicaDTO obtenerActividadEconomica(String codigoActividad) throws BusinessException;
	/**
	 * @param clave de Actividad economica
	 * @return ActividadEconomicaDTO con resultado
	 * @throws BusinessException con mensaje de error 
	 */
	public ActividadEconomicaDTO obtenerActividadEconomicaPorID(String codigoActividad) throws BusinessException;
	/**
	 * @param codigoActividad codigo de la actividad generica
	 * @return ActividadEconomicaDTO con resultado
	 * @throws BusinessException con mensaje de error 
	 */
	public ActividadEconomicaDTO obtenerActividadEconomicaGenerica(String codigoActividad) throws BusinessException;
}
