/**
 * Isban Mexico
 *   Clase: CuestionarioDAO.java
 *   Descripcion: Interfaz para el DAO CuestionarioDAO
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Local
public interface AdministradorClavesDAO {
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_CUESTIONARIO
	 */
	public static final String SQL_OBTENER_NUEVO_ID_CUESTIONARIO = "SELECT SEQ_PM_CLIENTE_CUESTIONARIO.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_CUESTIONARIO_RESP
	 */
	public static final String SQL_OBTENER_NUEVO_ID_CUESTIONARIO_RESP = "SELECT SEQ_PM_CUES_RESP.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_FAMILIAR
	 */
	public static final String SQL_OBTENER_NUEVO_ID_FAMILIAR = "SELECT SEQ_PM_FAMILIAR.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_RESPUESTA_PREG
	 */
	public static final String SQL_OBTENER_NUEVO_ID_RESPUESTA_PREG = "SELECT SEQ_PM_RESP_PREG.NEXTVAL FROM dual";
	
	/**
	 * String SQL_ASIGNAR_CUESTIONARIO_IC
	 */
	public static final String SQL_ASIGNAR_CUESTIONARIO_IC =
		"UPDATE NKM_MX_MAE_CLTE_CUEST_DRO SET CVE_CUEST_IC_FK = %s WHERE CVE_CLTE_CUEST_PK = %s";
	
	/**
	 * Metodo para obtener una nueva clave de Cliente - Cuestionario
	 * @return La clave del Cliente - Cuestionario
	 * @throws BusinessException En caso de un error en la obtencion de datos
	 */
	public int obtenerNuevaClaveClienteCuestionario() throws BusinessException;
	
	/**
	 * Metodo para obtener una nueva clave de Cuestionario - Respuesta
	 * @return La clave del Cuestionario - Respuesta
	 * @throws BusinessException En caso de un error en la obtencion de datos
	 */
	public int obtenerNuevaClaveCuestionarioRespuesta() throws BusinessException;
	
	/**
	 * Metodo para obtener una nueva clave de Cuestionario - Respuesta
	 * @return La clave de la Respuesta - Pregunta
	 * @throws BusinessException En caso de un error en la obtencion de datos
	 */
	public int obtenerNuevaClaveRespuestaPregunta() throws BusinessException;
	
	/**
	 * Metodo para obtener una nueva clave de Familiar
	 * @return La clave del Familiar
	 * @throws BusinessException En caso de un error en la obtencion de datos
	 */
	public int obtenerNuevaClaveFamiliar() throws BusinessException;
	
	/**
	 * Asigna una clave del cuestionario IC a un registro de Cuestionario
	 * @param claveClienteCuest Clave de Cliente-Cuestionario
	 * @param claveCuestIC Clave del Cuestionario IC
	 * @param psession Objeto de sesion de agave
	 * @throws BusinessException con mensaje de error
	 */
	public void asignarClaveCuestionarioIC(int claveClienteCuest, int claveCuestIC, ArchitechSessionBean psession) 
	throws BusinessException;
}
