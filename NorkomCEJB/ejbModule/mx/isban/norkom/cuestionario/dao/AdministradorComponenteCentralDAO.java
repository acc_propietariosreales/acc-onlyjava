/**
 * Isban Mexico
 *   Clase: AdministradorComponenteCentralDAO.java
 *   Descripcion: Interfaz para el DAO AdministradorComponenteCentralDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */


package mx.isban.norkom.cuestionario.dao;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

@Remote
public interface AdministradorComponenteCentralDAO {
	/**
	 * Variable utilizada para declarar SQL_OBTENER_ID_CUEST
	 */
	public static final String SQL_OBTENER_ID_CUEST = "SELECT SEQ_PM_ID_CUEST.NEXTVAL ID_CUEST FROM DUAL";
	/**
	 * Variable utilizada para declarar SQL_ASIGNAR_NUMERO_CONTRATO
	 */
	public static final String SQL_ASIGNAR_NUMERO_CONTRATO = 
			"UPDATE NKM_MX_MAE_CLTE_CUEST_DRO SET NUM_CNTR = '%s', FCH_FIN_CNTR = SYSDATE WHERE ID_CUEST = '%s'";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NIV_R_IND_U
	 */
	public static final String SQL_OBTENER_NIV_R_IND_U = 
 	"SELECT DISTINCT TIND.COD_IND_PERS_PK IND_PLD, TIND.VAL_IND_PERS_PK NIVEL_RGO, TNR.COD_IND_PERS_PK IND_NOR, TNR.VAL_IND_PERS_PK IND_UPLD FROM (SELECT B.COD_IND_PERS_PK, B.VAL_IND_PERS_PK, A.ID_CUEST "
    + " FROM NKM_MX_MAE_CLTE_CUEST_DRO A, NKM_MX_AUX_NIVEL_RIESG_DRO B, NKM_MX_CAT_ACTIV_ECON_DRO C"
    + " WHERE (A.cve_cuest_ic_fk IS NOT NULL OR ( A.CVE_ACTIV_ESP = C.CVE_ACT_ECON_PK AND C.FLG_ENT_FIN = 'S' AND A.COD_NIVEL_RIESG='A1')) AND A.ID_CUEST = '%s' "
    + " AND B.VAL_IND_NKM = A.COD_NIVEL_RIESG ) TIND, " 
    + " (SELECT B.COD_IND_PERS_PK, B.VAL_IND_PERS_PK, A.ID_CUEST "
    + " FROM NKM_MX_MAE_CLTE_CUEST_DRO A, NKM_MX_AUX_NIVEL_RIESG_DRO B, NKM_MX_CAT_ACTIV_ECON_DRO C"
    + " WHERE (A.cve_cuest_ic_fk IS NOT NULL OR ( A.CVE_ACTIV_ESP = C.CVE_ACT_ECON_PK AND C.FLG_ENT_FIN = 'S' AND A.COD_NIVEL_RIESG='A1')) AND A.ID_CUEST = '%s' "
    + " AND B.VAL_IND_NKM = A.COD_IND_UPLD) TNR "
    + " WHERE TIND.ID_CUEST = TNR.ID_CUEST ";
		
	/**
	 * Obtiene el valor de la secuencia SEQ_PM_ID_CUEST
	 * @param psession bean de session
	 * @return Valor siguiente de la secuencia
	 * @throws BusinessException con mensaje de error
	 */
	public int obtenerSecuenciaCuestionario(ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Asignar numero de contrato a un cuestionario
	 * @param idCuestionario id del cuestionario
	 * @param numeroContrato numero de contrato
	 * @param psession objeto de session
	 * @throws BusinessException con mensaje de error
	 */
	public void asignarNumeroContrato(String idCuestionario, String numeroContrato, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene el Nivel de riesgo y el Indicador UPLD
	 * @param idCuestionario id del cuestionario
	 * @param psession objeto de session
	 * @return Nivel de riesgo e indicador UPLD
	 * @throws BusinessException con mensaje de error
	 */
	public List<HashMap<String, Object>> obtenerNivRIndU(String idCuestionario, ArchitechSessionBean psession) throws BusinessException;
}
