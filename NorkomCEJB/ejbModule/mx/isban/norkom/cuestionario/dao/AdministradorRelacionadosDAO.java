/**
 * Isban Mexico
 *   Clase: AdministradorRelacionadosDAO.java
 *   Descripcion: Interfaz para el DAO AdministradorRelacionadosDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

@Remote
public interface AdministradorRelacionadosDAO {
	/**
	 * Variable utilizada para declarar SQL_AGREGAR_RELACIONADO
	 */
	public static final String SQL_AGREGAR_RELACIONADO = new StringBuilder()
		.append("INSERT INTO NKM_MX_AUX_RELAC_DRO(CVE_RELAC_PK, COD_TIPO_RELAC, TXT_NOMB_RELAC, TXT_APE_PAT_RELAC, TXT_APE_MAT_REL, FCH_NAC_REL, ")
		.append("COD_TIPO_PERS_REL, COD_PAIS_NACIO_REL, COD_PAIS_RESID_REL, POR_PART, CVE_CLTE_CUES_FK) ")
		.append("VALUES(SEQ_PM_RELA.NEXTVAL, '%s', '%s', '%s', '%s', TO_DATE('%s', 'YYYY-MM-DD'), '%s', '%s', '%s', %s, %s)")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_RELACIONADOS_POR_CNTR
	 */
	public static final String SQL_OBTENER_RELACIONADOS_POR_CNTR = new StringBuilder()
		.append("SELECT rl.CVE_RELAC_PK, rl.COD_TIPO_RELAC, rl.TXT_NOMB_RELAC, rl.TXT_APE_PAT_RELAC, rl.TXT_APE_MAT_REL, TO_CHAR(rl.FCH_NAC_REL, 'YYYYMMDD') FEC_NAC, ")
		.append("rl.COD_TIPO_PERS_REL, rl.COD_PAIS_NACIO_REL, rl.COD_PAIS_RESID_REL, rl.POR_PART ")
		.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO cc, NKM_MX_AUX_RELAC_DRO rl ")
		.append("WHERE cc.CVE_CLTE_CUEST_PK = rl.CVE_CLTE_CUES_FK AND cc.NUM_CNTR = '%s'")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_RELACIONADOS_POR_CNTR_TIPO_RELACIONADO
	 */
	public static final String SQL_OBTENER_RELACIONADOS_POR_CNTR_TIPO_RELACIONADO = new StringBuilder()
		.append("SELECT rl.CVE_RELAC_PK, rl.COD_TIPO_RELAC, rl.TXT_NOMB_RELAC, rl.TXT_APE_PAT_RELAC, rl.TXT_APE_MAT_REL, TO_CHAR(rl.FCH_NAC_REL, 'DD-MM-YYYY') FEC_NAC, ")
		.append("rl.COD_TIPO_PERS_REL, rl.COD_PAIS_NACIO_REL, rl.COD_PAIS_RESID_REL, rl.POR_PART, NVL(ps.TXT_NOMB, 'Pais no encontrado') PAIS ")
		.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO cc, NKM_MX_AUX_RELAC_DRO rl, NKM_MX_CAT_PAIS ps ")
		.append("WHERE cc.CVE_CLTE_CUEST_PK = rl.CVE_CLTE_CUES_FK AND cc.CVE_CLTE_CUEST_PK = %s AND rl.COD_TIPO_RELAC = '%s' AND rl.COD_PAIS_NACIO_REL = ps.COD_PAIS(+)")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_AGREGAR_CUESTIONARIO
	 */
	public static final String SQL_AGREGAR_CUESTIONARIO_BASICO = new StringBuilder()
		.append("INSERT INTO NKM_MX_MAE_CLTE_CUEST_DRO(CVE_CLTE_CUEST_PK, CVE_CUEST_IP_FK, ID_CUEST, COD_TIPO_PERS, COD_SUB_TIPO_PERS, COD_DIV_PROD, FCH_CREAC) ")
		.append("VALUES(%s, %s, '%s', '%s', '%s', '%s', sysdate)")
		.toString();
	/**
	 * @param relacionadosWS lista con datos relacionados
	 * @param claveCuestionario clave del cuestionario
	 * @param psession bean de session
	 * @return List<RelacionadoDTO> con resultado
	 * @throws BusinessException con mensaje de error
	 */
	public List<RelacionadoDTO> agregarRelacionados(List<RelacionadoDTO> relacionadosWS, int claveCuestionario, ArchitechSessionBean psession) 
	throws BusinessException;
	/**
	 * @param numeroContrato numero del contrato
	 * @param psession bean de session
	 * @return List<RelacionadoDTO> con resultado
	 * @throws BusinessException con mensaje de error
	 */
	public List<RelacionadoDTO> obtenerRelacionados(String numeroContrato, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene los relacionados clave cliente-cuestionario y por tipo
	 * @param claveClienteCuest Clave cliente-cuestionario a los que estan asignados los relacionados
	 * @param tipoRelacionado Tipo de relacionado que se quiere buscar
	 * @param psession Objeto de sesion de Agave
	 * @return Lista de relacionados que coincidan con el tipo y esten asignados al contrato especificado
	 * @throws BusinessException En caso de un error de negocio
	 */
	public List<RelacionadoDTO> obtenerRelacionadosPorClaveClienteCuestTipo(int claveClienteCuest, String tipoRelacionado, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Agregar un cuestionario a un cliente existente
	 * @param cuestionario Informacion del cuestionario que se desea agregar
	 * @param sesion Objeto de sesion de Agave
	 * @throws BusinessException En caso de error con la informacion o con la operacion
	 */
	public void agregarCuestionarioBasico(CuestionarioDTO cuestionario, ArchitechSessionBean sesion)
	throws BusinessException;
}
