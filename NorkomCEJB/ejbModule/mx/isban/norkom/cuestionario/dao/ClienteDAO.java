/**
 * Isban Mexico
 *   Clase: ClienteDAO.java
 *   Descripcion: Interfaz para el DAO ClienteDAO
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Local
public interface ClienteDAO {
	/**
	 *  constante para queryes String SQL_OBTENER_NUEVO_ID_CLTE
	 */
	static final String SQL_OBTENER_NUEVO_ID_CLTE = "SELECT SEQ_PM_CLIENTE_NORKOM.NEXTVAL FROM dual";

	/**
	 *  constante para queryes String SQL_OBTENER_CLIENTE_POR_BUC
	 */
	static final String SQL_OBTENER_CLIENTE_POR_BUC =
			"SELECT CVE_CLTE_PK, VAL_BUC, FLG_INDIC_CLTE_NVO, TXT_NOMB_CLTE, TXT_APE_PAT_CLTE, TXT_APE_MAT_CLTE, FCH_NAC_CLTE " +
			"FROM NKM_MX_MAE_CLTE_DRO WHERE VAL_BUC = '%s'";

	/**
	 *  constante para queryes String SQL_EXISTE_CLIENTE
	 */
	static final String SQL_EXISTE_CLIENTE =
			"SELECT COUNT(CVE_CLTE_PK) FROM NKM_MX_MAE_CLTE_DRO WHERE VAL_BUC = '%s'";
	
	/**
	 *  constante para queryes String SQL_AGREGAR_CLIENTE
	 */
	static final String SQL_AGREGAR_CLIENTE = new StringBuilder()
		.append("INSERT INTO NKM_MX_MAE_CLTE_DRO(CVE_CLTE_PK, VAL_BUC, FLG_INDIC_CLTE_NVO, TXT_NOMB_CLTE, TXT_APE_PAT_CLTE, TXT_APE_MAT_CLTE, FCH_NAC_CLTE) ")
		.append("VALUES(%s, '%s', '%s', '%s', '%s', '%s', TO_DATE('%s', 'YYYY-MM-DD')) ")
		.toString();
	
	/**
	 *  constante para queryes String SQL_OBTENER_CLIENTE_POR_CLAVE_CLTE_CUEST
	 */
	static final String SQL_OBTENER_CLIENTE_POR_CLAVE_CLTE_CUEST =
			"SELECT clte.CVE_CLTE_PK, clte.FCH_NAC_CLTE, clte.FLG_INDIC_CLTE_NVO, clte.VAL_BUC, clte.TXT_APE_PAT_CLTE, clte.TXT_APE_MAT_CLTE, clte.TXT_NOMB_CLTE " +
			"FROM NKM_MX_MAE_CLTE_DRO clte, NKM_MX_MAE_CLTE_CUEST_DRO cc " +
			"WHERE cc.CVE_CLTE_CUEST_PK = %s AND cc.CVE_CLTE_FK = clte.CVE_CLTE_PK";
	static final String SQL_UPDATE_CLIENTE="UPDATE NKM_MX_MAE_CLTE_DRO SET FLG_INDIC_CLTE_NVO = 'N', TXT_NOMB_CLTE = '%s', TXT_APE_PAT_CLTE = '%s', TXT_APE_MAT_CLTE = '%s', FCH_NAC_CLTE = TO_DATE('%s', 'YYYY-MM-DD') WHERE CVE_CLTE_PK= %s";
	/**
	 * @return int NUEVO CLIENTE
	 * @throws BusinessException con mensaje de error
	 */
	public int obtenerNuevoIdCliente() throws BusinessException;
	/**
	 * @param buc del cliente
	 * @return int EXISTE CLIENTE
	 * @throws BusinessException con mensaje de error
	 */
	public int existeCliente(String buc) throws BusinessException;
	/**
	 * @param buc del cliente
	 * @param psession ArchitechSessionBean de arquitectura
	 * @return ClienteDTO con datos de resultado
	 * @throws BusinessException con mensaje de error
	 */
	public ClienteDTO obtenerClientePorBuc(String buc, ArchitechSessionBean psession)throws BusinessException;
	/**
	 * 
	 * @param claveClienteCuestionario Clave Cliente Cuestionario
	 * @param psession Objeto de Sesion Agave
	 * @return El cliente encontrado
	 * @throws BusinessException En caso de error de negocio
	 */
	public ClienteDTO obtenerClientePorClaveClteCuest(int claveClienteCuestionario, ArchitechSessionBean psession)throws BusinessException;
	/**
	 * Este metodo solo se debe invocar desde otros dao y no de otros BO
	 * @param cliente objeto con datos del cliente
	 * @param requestDTO dto con datos a agregar
	 * @param dataAccess refrerencia de DATA ACCES
	 * @param psession objeto de session de arquitectura
	 * @throws BusinessException con mensaje de error
	 * @throws ExceptionDataAccess con mensaje de error
	 */
	public void agregarCliente(ClienteDTO cliente, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess, ArchitechSessionBean psession)
	throws BusinessException, ExceptionDataAccess;
	
	/**
	 * Este metodo sirve para actualizar los datos del cliente
	 * @param cliente objeto con datos del cliente
	 * @param psession objeto de session de arquitectura
	 * @throws BusinessException con mensaje de error
	 */
	public void actualizarCliente(ClienteDTO cliente, ArchitechSessionBean psession)throws BusinessException;
}
