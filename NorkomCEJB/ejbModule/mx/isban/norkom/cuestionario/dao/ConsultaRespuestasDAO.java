/**
 * Isban Mexico
 *   Clase: ConsultaRespuestasDAO.java
 *   Descripcion: Interfaz para el DAO ConsultaRespuestasDAO
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

/**
 * @author Miguel Angel Ayala Franco
 *
 */
@Remote
public interface ConsultaRespuestasDAO {

	/** constante para queryes COMILLA  ESPACIO */
	public static final String PARAMETRO = "'%s'";
	
	/** constante para queryes SELECT*/
	public static final String SELECT="SELECT  ";
	
	
	/** constante para queryes PR.TXT_TITUL PR.TXT_SECCN*/
	public static final String TXT_TITUL_TXT_SECCN=" upper(PREGUNTAS.TXT_PREG) TXT_PREG, PREGUNTAS.TXT_TITUL, PREGUNTAS.TXT_SECCN, PREGUNTAS.TXT_ABRV ";
	

	/** constante para queryes SELECT_DISTINCT*/
	public static final String SELECT_DISTINCT = "SELECT DISTINCT";
	
	/** constante para queryes WHERE CCD.ID_CUEST = */
	public static final String WHERE_CCD_ID_CUEST=" WHERE CCD.ID_CUEST = ";
	
	/** constante para queryes AND_RESPUESTA_CVE_CUEST_RESP_FK */
	public static final String  AND_RESPUESTA_CVE_CUEST_RESP_FK=" AND RESPUESTA.CVE_CUEST_RESP_FK = RCR.CVE_CUEST_RESP_PK";
	
	/** constante para queryes AND_PREGUNTAS_CVE_PREG_PK */
	public static final String AND_PREGUNTAS_CVE_PREG_PK= " AND PREGUNTAS.CVE_PREG_PK = DETALLE.CVE_PREG_PK";
	
	/** constante para queryes INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO*/
	public static final String  INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO=" INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IP_FK = DETALLE.CVE_CUEST_PK";
	
	/** constante para queryes INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA*/
	public static final String INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA =" INNER JOIN NKM_MX_PRC_RESP_PREG_DRO RESPUESTA ON PREGUNTAS.CVE_PREG_PK = RESPUESTA.CVE_PREG_FK";

	/** constante para queryes PREGUNTAS.CVE_RPT*/
	public static final String PREGUNTAS_CVE_RPT=" PREGUNTAS.CVE_RPT,";
	
	/** constante para queryes INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO*/
	public static final String INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO=" INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR ON CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK";
	
	/** constante para queryes INNER_JOIN_NKM_MX_MAE_PREG_DRO*/
	public static final String INNER_JOIN_NKM_MX_MAE_PREG_DRO =" INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_RPT NOT IN('strParametroX')";
	
	/** constante para queryes FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD*/
	public static final String FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD=" FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD";
	/**constante para queryes PREGUNTAS_CVE_PREG_PK*/
	public static final String PREGUNTAS_CVE_PREG_PK=",PREGUNTAS.CVE_PREG_PK ";
	/**constante para queryes PREGUNTAS_CVE_PREG_PK*/
	public static final String ORDER_BY_CVE_PREG_PK=" ORDER BY PREGUNTAS.CVE_PREG_PK ";

	/**
	 * constante para queryes  ENCABEZADO IP
	 */
	public String QRY_ENCABEZADO_IP  =
	       new StringBuilder().append(SELECT) 
	    		   .append(" CCD.CVE_CLTE_CUEST_PK, CCD.FCH_CREAC,")
	    		   .append(" CCD.TXT_NOMB_SUCU, CCD.COD_SUCU,")
	    		   .append(" CCD.TXT_NOMB_PLAZA, CCD.TXT_NOMB_ZONA,")
	    		   .append(" CCD.NUM_CNTR, CCD.DSC_SEGMT,")
	    		   .append(" CCD.DSC_PROD, CCD.COD_NACIO,")
	    		   .append(" NAC.TXT_NOMB NACIONALIDAD, ACTIEGEN.DSC_ACT_ECON CVE_ACTIV_GEN,") 
	    		   .append(" CCD.COD_MUN_RESID,CLIENTE.VAL_BUC,") 
	    		   .append(" CLIENTE.TXT_NOMB_CLTE, CLIENTE.TXT_APE_PAT_CLTE, CLIENTE.TXT_APE_MAT_CLTE,") 
	    		   .append(" CUESTIONARIO.COD_TIPO_PERS, CUESTIONARIO.COD_SUB_TIPO_PERS,") 
	    		   .append(" CUESTIONARIO.COD_DIV, ACTIECON.DSC_ACT_ECON,") 
	    		   .append(" ACTIECON.FLG_SECTR, CUESTIONARIO.VAL_TIPO_CUEST,") 
	    		   .append(" PAIS.TXT_NOMB PAIS, PAIS.COD_PAIS,") 
	    		   .append(" ESTADO.TXT_NOMB ESTADO, MUNICIPIO.TXT_NOMB MUNICIPIO")
	    		   .append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	    		   .append(" INNER JOIN NKM_MX_MAE_CLTE_DRO CLIENTE ON CCD.CVE_CLTE_FK = CLIENTE.CVE_CLTE_PK")
	    		   .append(" INNER JOIN NKM_MX_CAT_ACTIV_ECON_DRO ACTIECON ON CCD.CVE_ACTIV_ESP = ACTIECON.CVE_ACT_ECON_PK")
	    		   .append(" LEFT JOIN NKM_MX_CAT_ACTIV_ECON_DRO ACTIEGEN ON CCD.CVE_ACTIV_GEN = ACTIEGEN.CVE_ACT_ECON_PK ")
	    		   .append(" INNER JOIN NKM_MX_MAE_CUEST_DRO CUESTIONARIO ON CCD.CVE_CUEST_IP_FK = CUESTIONARIO.CVE_CUEST_PK")
	    		   .append(" INNER JOIN NKM_MX_CAT_PAIS PAIS ON CCD.COD_PAIS_RESID = PAIS.COD_PAIS")
	    		   .append(" INNER JOIN NKM_MX_CAT_PAIS NAC ON CCD.COD_NACIO = NAC.COD_PAIS")
	    		   .append(" LEFT JOIN NKM_MX_CAT_EDO ESTADO ON SUBSTR( CCD.COD_MUN_RESID,6,2) = ESTADO.COD_EDO")
	    		   .append(" LEFT JOIN NKM_MX_CAT_MUN MUNICIPIO ON SUBSTR( CCD.COD_MUN_RESID,6,2) = MUNICIPIO.COD_EDO  AND SUBSTR( CCD.COD_MUN_RESID,0,5) = MUNICIPIO.COD_MUN ")
	    		   .append(WHERE_CCD_ID_CUEST)
	    		   .append(PARAMETRO)
	    		   .toString();

	/** constante para queryes  QRTY_RESPUESTAS_LIBRES_IP */
	public String QRTY_RESPUESTAS_LIBRES_IP  =
	       new StringBuilder().append(SELECT) 
	    		   .append(" OPCIONES.DSC_OPC, PREGUNTAS.CVE_RPT,")
	    		   .append(TXT_TITUL_TXT_SECCN)
	    		   .append(PREGUNTAS_CVE_PREG_PK)
	    		   .append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD) 
	    		   .append(INNER_JOIN_NKM_MX_MAE_PREG_DRO) 
	    		   .append(" AND PREGUNTAS.CVE_TIPO_PREG_FK  = 1 AND PREGUNTAS.CVE_GRPO_FK  = 0")
	    		   .append(" INNER JOIN NKM_MX_PRC_RESP_PREG_DRO RESPUESTA ON  PREGUNTAS.CVE_PREG_PK = RESPUESTA.CVE_PREG_FK")
	    		   .append(" INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.VAL_OPC")
	    		   .append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	    		   .append("  AND RESPUESTA.CVE_CUEST_RESP_FK = RCR.CVE_CUEST_RESP_PK")
	    		   .append("  INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IP_FK = DETALLE.CVE_CUEST_PK")
	    		   .append("  AND PREGUNTAS.CVE_PREG_PK = DETALLE.CVE_PREG_PK")
	    		   .append(WHERE_CCD_ID_CUEST)
	    		   .append(PARAMETRO)
	    		   .append(ORDER_BY_CVE_PREG_PK)
	    		   .toString();
	
	/** constante para queryes  QRTY_RESPUESTAS_SELECT_UNICA_IP */
	public String QRTY_RESPUESTAS_SELECT_UNICA_IP  =
	       new StringBuilder().append(SELECT)
	    		   .append(" opciones.dsc_opc,OPCIONES.VAL_OPC,")
	    		   .append(PREGUNTAS_CVE_RPT)
	    		   .append(TXT_TITUL_TXT_SECCN)
	    		   .append(PREGUNTAS_CVE_PREG_PK)
	    		   .append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	    		   .append(INNER_JOIN_NKM_MX_MAE_PREG_DRO) 
	    		   .append(" AND PREGUNTAS.CVE_TIPO_PREG_FK = 2 and PREGUNTAS.CVE_GRPO_FK BETWEEN 1 AND 8 or PREGUNTAS.CVE_GRPO_FK =66")  
	    		   .append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	    		   .append(" INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK AND OPCIONES.DSC_OPC IS NOT NULL")
	    		   .append(INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO)
	    		   .append(AND_PREGUNTAS_CVE_PREG_PK)
	    		   .append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	    		   .append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	    		   .append(WHERE_CCD_ID_CUEST)
	    		   .append(PARAMETRO)
	    		   .append(ORDER_BY_CVE_PREG_PK)
	    		   .toString();

	/**  constante para queryes QRTY_RESPUESTAS_RADIO_BUTTON_IP */
	public String QRTY_RESPUESTAS_RADIO_IP  =
	       new StringBuilder().append(SELECT)
	       			.append(" DECODE (OPCIONES.VAL_OPC,'2','0',OPCIONES.VAL_OPC) VAL_OPC,")
	       			.append(" OPCIONES.DSC_OPC,")
	       			.append(PREGUNTAS_CVE_RPT)
	       			.append(TXT_TITUL_TXT_SECCN)
	       			.append(PREGUNTAS_CVE_PREG_PK)
	       			.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	       			.append(" INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_RPT NOT IN('strParametroX') AND PREGUNTAS.CVE_GRPO_FK = 10 AND PREGUNTAS.CVE_TIPO_PREG_FK = 4") 
	       			.append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	       			.append(" INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK")
	       			.append(INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO)
	       			.append(AND_PREGUNTAS_CVE_PREG_PK)
	       			.append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	       			.append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	       			.append(WHERE_CCD_ID_CUEST)
	       			.append(PARAMETRO)
	       			.append(ORDER_BY_CVE_PREG_PK)
	       			.toString();
	
	/** constante para queryes  QRTY_RESPUESTAS_CATALOGO_PAIS_IP */
	public String QRTY_RESPUESTAS_PAIS_IP  =
	       new StringBuilder().append(SELECT)
	       			.append(" PAIS.TXT_NOMB,")
	       			.append(PREGUNTAS_CVE_RPT)
	       			.append(TXT_TITUL_TXT_SECCN)
	       			.append(PREGUNTAS_CVE_PREG_PK)
	       			.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	       			.append(INNER_JOIN_NKM_MX_MAE_PREG_DRO)
	       			.append(" AND PREGUNTAS.CVE_GRPO_FK = 11")
	       			.append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	       			.append(" INNER JOIN NKM_MX_CAT_PAIS PAIS ON RESPUESTA.VAL_OPCN = PAIS.CVE_PAIS_PK")
	       			.append(INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO)
	       			.append(AND_PREGUNTAS_CVE_PREG_PK)
	       			.append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	       			.append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	       			.append(WHERE_CCD_ID_CUEST)
	       			.append(PARAMETRO)
	       			.append(ORDER_BY_CVE_PREG_PK)
	       			.toString();
	
	/** constante para queryes  QRTY_RESPUESTAS_ORIGEN_CATALOGO_RECURSOS_IP */
	public String QRTY_RESPUESTAS_ORIGEN_IP  =
	       new StringBuilder().append(SELECT)
	       			.append(" RECURSOS.DSC_REC,")
	       			.append(PREGUNTAS_CVE_RPT)
	       			.append(TXT_TITUL_TXT_SECCN)
	       			.append(PREGUNTAS_CVE_PREG_PK)
	       			.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	       			.append(INNER_JOIN_NKM_MX_MAE_PREG_DRO)
	       			.append(" AND PREGUNTAS.CVE_GRPO_FK = 12")
	    		    .append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	    		    .append(INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO)
	    		    .append(AND_PREGUNTAS_CVE_PREG_PK)
	    		    .append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	    		    .append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	    		    .append(" INNER JOIN NKM_MX_CAT_ORIG_DEST_REC_DRO RECURSOS ON  RESPUESTA.VAL_OPCN = RECURSOS.CVE_REC_PK")
	    		    .append(" AND RECURSOS.FLG_TIPO_REC = 'O'")
	    		    .append(WHERE_CCD_ID_CUEST)
	    		    .append(PARAMETRO)
	    		    .append(ORDER_BY_CVE_PREG_PK)
	    		    .toString();
	
	/** constante para queryes  QRTY_RESPUESTAS_ORIGEN_CATALOGO_RECURSOS_IP */
	public String QRTY_RESPUESTAS_DESTINO_IP  =
	       new StringBuilder().append(SELECT)
	       			.append(" RECURSOS.DSC_REC,")
	       			.append(PREGUNTAS_CVE_RPT)
	       			.append(TXT_TITUL_TXT_SECCN)
	       			.append(PREGUNTAS_CVE_PREG_PK)
	       			.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	       			.append(INNER_JOIN_NKM_MX_MAE_PREG_DRO)
	       			.append(" AND PREGUNTAS.CVE_GRPO_FK = 13")
	       			.append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	       			.append(INNER_JOIN_NKM_MX_REL_DETA_CUEST_DRO)
	       			.append(AND_PREGUNTAS_CVE_PREG_PK)
	       			.append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	       			.append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	       			.append(" INNER JOIN NKM_MX_CAT_ORIG_DEST_REC_DRO RECURSOS ON  RESPUESTA.VAL_OPCN = RECURSOS.CVE_REC_PK") 
	       			.append(" AND RECURSOS.FLG_TIPO_REC = 'D'")
	       			.append(WHERE_CCD_ID_CUEST)
	       			.append(PARAMETRO)
	       			.append(ORDER_BY_CVE_PREG_PK)
	       			.toString();
	
	
	//------------------Cuestionario IC

	/** constante para queryes  QRTY_ENCABEZADO_IC */
	public String QRY_ENCABEZADO_IC1  =
	       new StringBuilder().append( SELECT)
	        		.append(" CCD.FCH_CREAC, CCD.TXT_NOMB_SUCU,")
	        		.append(" CCD.COD_SUCU, CCD.TXT_NOMB_PLAZA,")
	        		.append(" CCD.TXT_NOMB_ZONA, CLIENTE.TXT_NOMB_CLTE,")
	        		.append(" CLIENTE.TXT_APE_PAT_CLTE, CLIENTE.TXT_APE_MAT_CLTE,")
	        		.append(" CLIENTE.VAL_BUC, CCD.NUM_CNTR,")
	        		.append(" CUESTIONARIO.VAL_TIPO_CUEST, CUESTIONARIO.COD_TIPO_PERS,")
	        		.append(" CUESTIONARIO.COD_SUB_TIPO_PERS, CUESTIONARIO.COD_DIV")
	        		.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	        		.append(" INNER JOIN NKM_MX_MAE_CLTE_DRO CLIENTE ON CCD.CVE_CLTE_FK = CLIENTE.CVE_CLTE_PK")
	        		.append(" INNER JOIN NKM_MX_MAE_CUEST_DRO CUESTIONARIO ON CCD.CVE_CUEST_IC_FK = CUESTIONARIO.CVE_CUEST_PK")
	        		.append(WHERE_CCD_ID_CUEST)
	        		.append(PARAMETRO)
	        		.toString();
	
	
	/** constante para queryes  QRTY_RESPUESTAS_LIBRES_IC */
	public String QRTY_RESPUESTAS_LIBRES_IC  =
	       new StringBuilder().append( SELECT)
	       			.append(" DECODE(upper(RESPUESTA.TXT_RESP),'NA',' ',RESPUESTA.TXT_RESP) DSC_OPC,PREGUNTAS.CVE_RPT,")
	       			.append(TXT_TITUL_TXT_SECCN)
	       			.append(PREGUNTAS_CVE_PREG_PK)
	       			.append(" FROM NKM_MX_PRC_RESP_PREG_DRO RESPUESTA")
	       			.append(" INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS on PREGUNTAS.CVE_PREG_PK = RESPUESTA.CVE_PREG_FK AND RESPUESTA.TXT_RESP IS NOT NULL")
	       			.append(" AND PREGUNTAS.CVE_RPT NOT IN('strParametroX') AND PREGUNTAS.CVE_TIPO_PREG_FK = 1 and PREGUNTAS.CVE_GRPO_FK = 0")
	       			.append(" INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR on RCR.CVE_CUEST_RESP_PK = RESPUESTA.CVE_CUEST_RESP_FK")
	       			.append(" INNER JOIN NKM_MX_MAE_CLTE_CUEST_DRO  CCD on CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK")
	       			.append(WHERE_CCD_ID_CUEST)
	       			.append(PARAMETRO)
	       			.append(ORDER_BY_CVE_PREG_PK)
	       			.toString();
	
	/** constante para queryes  QRTY_RESPUESTAS_SELECT_UNICA_IC */
	public String QRTY_RESPUESTAS_SELECT_UNICA_IC  =
	       new StringBuilder().append( SELECT)
	        		.append(" OPCIONES.VAL_OPC,OPCIONES.DSC_OPC,")
	        		.append(PREGUNTAS_CVE_RPT)
	        		.append(TXT_TITUL_TXT_SECCN)
	        		.append(PREGUNTAS_CVE_PREG_PK)
	        		.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	        		.append(" INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_RPT NOT IN('strParametroX') AND PREGUNTAS.CVE_TIPO_PREG_FK = 2 or PREGUNTAS.CVE_PREG_PK=269 ") 
	        		.append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	        		.append(" INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK AND OPCIONES.DSC_OPC IS NOT NULL")
	        		.append(" INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IC_FK = DETALLE.CVE_CUEST_PK")
	        		.append(AND_PREGUNTAS_CVE_PREG_PK)
	        		.append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	        		.append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	        		.append(WHERE_CCD_ID_CUEST)
	        		.append(PARAMETRO)
	        		.append(ORDER_BY_CVE_PREG_PK)
	        		.toString();
	
	/** constante para queryes  QRTY_RESPUESTAS_RADIO_BUTTON_IC */
	public String QRTY_RESPUESTAS_RADIO_IC  =
	       new StringBuilder().append( "SELECT ")
	       			.append(" OPCIONES.VAL_OPC, OPCIONES.DSC_OPC,")
	       			.append(PREGUNTAS_CVE_RPT)
	       			.append(TXT_TITUL_TXT_SECCN)
	       			.append(PREGUNTAS_CVE_PREG_PK)
	       			.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	       			.append(INNER_JOIN_NKM_MX_MAE_PREG_DRO) 
	       			.append(" AND PREGUNTAS.CVE_GRPO_FK = 10 AND PREGUNTAS.CVE_TIPO_PREG_FK = 4") 
	       			.append(INNER_JOIN_NKM_MX_PRC_RESP_PREG_DRO_RESPUESTA)
	       			.append(" INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK")
	       			.append(" INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IC_FK = DETALLE.CVE_CUEST_PK")
	       			.append(AND_PREGUNTAS_CVE_PREG_PK)
	       			.append(INNER_JOIN_NKM_MX_REL_CUEST_RESP_DRO)
	       			.append(AND_RESPUESTA_CVE_CUEST_RESP_FK)
	       			.append(WHERE_CCD_ID_CUEST)
	       			.append(PARAMETRO)
	       			.append(ORDER_BY_CVE_PREG_PK)
	       			.toString();
	
	
	
	/** constante para queryes  QRTY_PREGUNTAS_INFORMACION_FAMILIARES_DEL_CLIENTE_IC */
	public String QRY_PREGUNTAS_INFORMACION_FAMILIARES_IC  =
	       new StringBuilder().append( SELECT_DISTINCT)
	       			.append(" FAMILIARES.TXT_NOMB_FAM,")
	       			.append(" FAMILIARES.TXT_DOMIC_FAM,")   	 
	       			.append("  to_char(FAMILIARES.TXT_FCH_NAC,'dd-mm-yyyy') TXT_FCH_NAC,")
	       			.append(" FAMILIARES.TXT_PAREN")
	       			.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	       			.append(" INNER JOIN NKM_MX_AUX_FAM_DRO FAMILIARES ON FAMILIARES.CVE_CLTE_CUEST_FK = CCD.CVE_CLTE_CUEST_PK")
	       			.append(WHERE_CCD_ID_CUEST)
	       			.append(PARAMETRO)
	       			.toString();
	
	/** constante para queryes  QRTY_INFORMACION_ACCIONISTAS_DEL_CLIENTE_IC */
	public String QRY_INFORMACION_ACCIONISTAS_IC  =
	       new StringBuilder().append( SELECT_DISTINCT)
	         		.append(" RELACION.TXT_NOMB_RELAC,")
	         		.append(" RELACION.TXT_APE_PAT_RELAC,")
	         		.append(" RELACION.TXT_APE_MAT_REL,")
	         		.append(" to_char(RELACION.FCH_NAC_REL,'dd-mm-yyyy') FCH_NAC_REL,")
	         		.append(" PAIS.TXT_NOMB,")
	         		.append(" RELACION.COD_TIPO_PERS_REL,")
	         		.append(" RELACION.POR_PART, RELACION.COD_TIPO_RELAC")
	         		.append(FROM_NKM_MX_MAE_CLTE_CUEST_DRO_CCD)
	         		.append(" INNER JOIN NKM_MX_AUX_RELAC_DRO RELACION ON RELACION.CVE_CLTE_CUES_FK = CCD.CVE_CLTE_CUEST_PK")
	         		.append(" INNER JOIN NKM_MX_CAT_PAIS PAIS ON PAIS.COD_PAIS = RELACION.COD_PAIS_NACIO_REL")
	         		.append(WHERE_CCD_ID_CUEST)
	         		.append(PARAMETRO)
	         		.toString();
	
	
	/**
	 * Metodo para obtener las respuestas IP
	 * @param request RequestObtenerCuestionario con datoa para la busqueda
	 * @param psession ArchitechSessionBean de aquitectura
	 * @throws BusinessException con mensaje de error
	 * @return preguntas List<PreguntaDTO> con resultado de la operacion
	 */
	public List<PreguntaDTO> obtenerRespuestasIP(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException;
	
	/**
	 * Metodo para obtener las respuestas IC
	 * @param request RequestObtenerCuestionario con datos para la busqueda
	 * @param psession ArchitechSessionBean de arquitecura
	 * @throws BusinessException  con mensaje de error
	 * @return preguntas List<PreguntaDTO> 
	 */
	public List<PreguntaDTO> obtenerRespuestasIC(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException;
	
	/**Obtiene el encabezado del cuestionario IP
	 * @param request RequestObtenerCuestionario con datos para obtener el encabezado
	 * @param psession ArchitechSessionBean de aqruitectura
	 * @throws BusinessException  con mensaje de error
	 * @return preguntas CuestionarioDTO
	 */
	public CuestionarioDTO obtenerEncabezadoIP(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException;
	
	
	/** Obtiene el encabezado del cuestionario IC
	 * @param request RequestObtenerCuestionario con datos para la busqueda
	 * @param psession ArchitechSessionBean
	 * @throws BusinessException con mensaje de error
	 * @return preguntas CuestionarioDTO
	 */
	public CuestionarioDTO obtenerEncabezadoIC(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException;
	
	/** Obtiene el referencias personales de IC
	 * @param request RequestObtenerCuestionario con datos para obtener los familiares
	 * @param psession ArchitechSessionBean de arquitectura
	 * @throws BusinessException con mensaje de error
	 * @return preguntas List<PreguntaDTO> 
	 */
	public List<PreguntaDTO> obtenerFamiliares(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException;
	
	
	/** Obtiene Accionistas de IC
	 * @param request RequestObtenerCuestionario con datos para realizar la busqueda
	 * @param psession ArchitechSessionBean bean de session
	 * @throws BusinessException con mensaje de error
	 * @return preguntas List<PreguntaDTO> 
	 */
	public List<RelacionadoDTO> obtenerAccionistas(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException;
	
}
