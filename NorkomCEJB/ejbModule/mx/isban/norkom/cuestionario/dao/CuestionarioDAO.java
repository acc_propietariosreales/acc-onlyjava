/**
 * Isban Mexico
 *   Clase: CuestionarioDAO.java
 *   Descripcion: Interfaz para el DAO CuestionarioDAO
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Local
public interface CuestionarioDAO {
	
	/**
	 * Variable utilizada para declarar el formato de fecha en YYYYMMDD
	 */
	public static final SimpleDateFormat FORMATO_FECHA_YYYYMMDD = new SimpleDateFormat("yyyyMMdd", Locale.US); 
	/**
	 * Variable utilizada para declarar SQL_OBTENER_DATOS_GRALES_CUEST
	 */
	public static final String SQL_OBTENER_DATOS_GRALES_CUEST = new StringBuilder()
		.append("SELECT c.VAL_BUC, cc.ID_CUEST, SUBSTR(cc.ID_CUEST, 1, 2) ID_APP, cc.NUM_CNTR, cc.FCH_CREAC, cc.COD_PROD, cc.DSC_PROD, cc.COD_SUB_PROD, ") 
		.append("cc.DSC_SUB_PROD, aeg.COD_ACT_ECON CVE_ACTIV_GEN,aeg.DSC_ACT_ECON DSC_ACT_GEN, aee.COD_ACT_ECON CVE_ACTIV_ESP, ")
		.append("aee.DSC_ACT_ECON DSC_ACT_ESP, cc.DSC_SEGMT ,cc.COD_TIPO_PERS , cc.COD_SUB_TIPO_PERS, cc.COD_NIVEL_RIESG, cc.COD_IND_UPLD, ")
		.append("c.TXT_NOMB_CLTE || NVL(' ' || c.TXT_APE_PAT_CLTE, '') || NVL(' ' || c.TXT_APE_MAT_CLTE, '') NOMBRE_CLTE, cc.COD_SUCU ")
		.append(" FROM NKM_MX_MAE_CLTE_DRO c ")
		.append(" join NKM_MX_MAE_CLTE_CUEST_DRO cc on c.CVE_CLTE_PK = cc.CVE_CLTE_FK") 
		.append(" left join NKM_MX_CAT_ACTIV_ECON_DRO aee on aee.CVE_ACT_ECON_PK=cc.CVE_ACTIV_ESP and aee.VAL_TIPO_ACT_ECON='ESP'")
		.append(" left join NKM_MX_CAT_ACTIV_ECON_DRO aeg on aeg.CVE_ACT_ECON_PK=cc.CVE_ACTIV_GEN and aeg.VAL_TIPO_ACT_ECON='GEN' ")
		.toString();
	/**
	 * Variable utilizada para declarar SQL_WHERE_BUC
	 */
	public static final String SQL_WHERE_BUC = " (c.VAL_BUC = '%s' OR c.VAL_BUC = '%s') ";
	
	/**
	 * Variable utilizada para declarar SQL_WHERE_NUM_CNTR
	 */
	public static final String SQL_WHERE_NUM_CNTR = " cc.NUM_CNTR = '%s' ";
	
	/**
	 * Variable utilizada para declarar SQL_WHERE_ID_CUEST
	 */
	public static final String SQL_WHERE_ID_CUEST = " cc.ID_CUEST = '%s' ";
	
	/**
	 * Variable utilizada para declarar SQL_WHERE_FCH_MAYOR_IGUAL
	 */
	public static final String SQL_WHERE_FCH_MAYOR_IGUAL = " TRUNC(cc.FCH_CREAC) >= TO_DATE('%s', 'YYYYMMDD') ";
	
	/**
	 * Variable utilizada para declarar SQL_WHERE_FCH_MENOR_IGUAL
	 */
	public static final String SQL_WHERE_FCH_MENOR_IGUAL = " TRUNC(cc.FCH_CREAC) <= TO_DATE('%s', 'YYYYMMDD') ";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_DATOS_GRALES_CUEST_POR_ID con contrato nulo
	 */
	public static final String SQL_OBTENER_DATOS_GRALES_CUEST_POR_ID = new StringBuilder()
		.append("SELECT cc.CVE_CLTE_CUEST_PK, cc.ID_CUEST, SUBSTR(cc.ID_CUEST, 1, 2) ID_APP, cc.NUM_CNTR, cc.FCH_CREAC, cc.COD_PROD, cc.DSC_PROD, nvl(cc.CVE_CLTE_FK,0) CVE_CLTE_FK ")
		.append(", cc.COD_SUB_PROD,cc.DSC_SUB_PROD, cc.CVE_ACTIV_GEN, cc.CVE_ACTIV_ESP, cc.DSC_SEGMT, cc.COD_PAIS_RESID, cc.COD_NACIO ,nvl(cc.CVE_CUEST_IP_FK,0) CVE_CUEST_IP ")
		.append(" ,cc.COD_TIPO_PERS, cc.COD_SUB_TIPO_PERS, cc.COD_IND_UPLD ")
		.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO cc ")
		.append("WHERE cc.ID_CUEST = '%s'")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_DATOS_GRALES_CUEST_POR_ID con contrato NO nulo
	 */
	public static final String SQL_OBTENER_DATOS_GRALES_CUEST_POR_ID_CNTR_NOT_NULL = new StringBuilder()
		.append("SELECT cc.CVE_CLTE_CUEST_PK, cc.ID_CUEST, SUBSTR(cc.ID_CUEST, 1, 2) ID_APP, cc.NUM_CNTR, cc.FCH_CREAC, cc.COD_PROD, cc.DSC_PROD, cc.COD_SUB_PROD, ")
		.append("cc.DSC_SUB_PROD, cc.CVE_ACTIV_GEN, cc.CVE_ACTIV_ESP, cc.DSC_SEGMT, cc.COD_PAIS_RESID, cc.COD_NACIO ,nvl(cc.CVE_CUEST_IP_FK,0) CVE_CUEST_IP, nvl(cc.CVE_CLTE_FK,0) CVE_CLTE_FK, ")
		.append("cc.COD_NIVEL_RIESG, cc.COD_IND_UPLD, cc.COD_TIPO_PERS, cc.COD_SUB_TIPO_PERS ")
		.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO cc ")
		.append("WHERE cc.NUM_CNTR IS NOT NULL")
		.toString();
	
	/**
	 * String SQL_OBTENER_DATOS_GRALES_CUEST_POR_CLAVE_CC
	 */
	public static final String SQL_OBTENER_DATOS_GRALES_CUEST_POR_CLAVE_CC = new StringBuilder()
		.append("SELECT cc.CVE_CLTE_CUEST_PK, cc.ID_CUEST, SUBSTR(cc.ID_CUEST, 1, 2) ID_APP, cc.NUM_CNTR, cc.FCH_CREAC, cc.COD_PROD, cc.DSC_PROD, cc.COD_SUB_PROD, ")
		.append("cc.DSC_SUB_PROD, cc.CVE_ACTIV_GEN, cc.CVE_ACTIV_ESP, cc.DSC_SEGMT, cc.COD_PAIS_RESID, cc.COD_NACIO, cc.COD_NIVEL_RIESG, cc.COD_IND_UPLD ")
		.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO cc ")
		.append("WHERE cc.CVE_CLTE_CUEST_PK = %s ")
		.toString();
	
	/**
	 * Obtiene el cuestionario de un cliente con base a la aplicacion, fecha de creacion y nivel de riesgo
	 */
	public static final String SQL_OBTENER_CUESTIONARIO_MISMO_CLIENTE = new StringBuilder()
		.append("SELECT cc.CVE_CLTE_CUEST_PK, cc.ID_CUEST, SUBSTR(cc.ID_CUEST, 1, 2) ID_APP, cc.NUM_CNTR, cc.FCH_CREAC, cc.COD_PROD, cc.DSC_PROD, cc.COD_SUB_PROD, ")
		.append("cc.DSC_SUB_PROD, cc.CVE_ACTIV_GEN, cc.CVE_ACTIV_ESP, cc.DSC_SEGMT, cc.COD_PAIS_RESID, cc.COD_NACIO, cc.COD_NIVEL_RIESG, cc.COD_IND_UPLD ")
		.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO cc, NKM_MX_MAE_CLTE_DRO clte ")
		.append("WHERE cc.CVE_CLTE_FK = clte.CVE_CLTE_PK AND clte.VAL_BUC = '%s' AND SUBSTR(cc.ID_CUEST, 1, 2) = '%s' AND TRUNC(cc.FCH_CREAC) = TRUNC(SYSDATE) ")
		.append("AND cc.COD_NIVEL_RIESG = '%s' AND cc.NUM_CNTR IS NOT NULL ")
		.append("ORDER BY cc.FCH_CREAC DESC ")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_CUESTIONARIO
	 */
	static final String SQL_OBTENER_NUEVO_ID_CUESTIONARIO = "SELECT SEQ_PM_CLIENTE_CUESTIONARIO.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_CUESTIONARIO_RESP
	 */
	static final String SQL_OBTENER_NUEVO_ID_CUESTIONARIO_RESP = "SELECT SEQ_PM_CUES_RESP.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_FAMILIAR
	 */
	static final String SQL_OBTENER_NUEVO_ID_FAMILIAR = "SELECT SEQ_PM_FAMILIAR.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_NUEVO_ID_RESPUESTA_PREG
	 */
	static final String SQL_OBTENER_NUEVO_ID_RESPUESTA_PREG = "SELECT SEQ_PM_RESP_PREG.NEXTVAL FROM dual";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_TIPO_CUESTIONARIO
	 */
	static final String SQL_OBTENER_TIPO_CUESTIONARIO =
			"SELECT CVE_CUEST_PK, TXT_NOMB, DSC_CUEST, VAL_TIPO_CUEST, COD_DIV, COD_TIPO_PERS, COD_SUB_TIPO_PERS, NUM_VERSN, TO_CHAR(SYSDATE,'dd-MM-yyyy') as FECHA " +
			"FROM NKM_MX_MAE_CUEST_DRO WHERE COD_TIPO_PERS = '%s' AND COD_SUB_TIPO_PERS = '%s' AND COD_DIV = '%s' and VAL_TIPO_CUEST = '%s' and ID_APP = '%s'";
	
	/**
	 * String SQL_OBTENER_TIPO_CUESTIONARIO_POR_CLAVE
	 */
	static final String SQL_OBTENER_TIPO_CUESTIONARIO_POR_CLAVE =
			"SELECT CVE_CUEST_PK, TXT_NOMB, DSC_CUEST, VAL_TIPO_CUEST, COD_DIV, COD_TIPO_PERS, COD_SUB_TIPO_PERS, NUM_VERSN, TO_CHAR(SYSDATE,'dd-MM-yyyy') as FECHA " +
			"FROM NKM_MX_MAE_CUEST_DRO WHERE CVE_CUEST_PK = %s";
	
	/**
	 * Obtencion del Tipo de Cuestionario
	 * @param peticion de obtenerTipoCuestionario
	 * @param tipoCuestionario TipoCuestionario
	 * @param psession de obtenerTipoCuestionario
	 * @return CuestionarioDTO con resultado de la operacion
	 * @throws BusinessException de obtenerTipoCuestionario
	 */
	public CuestionarioDTO obtenerTipoCuestionario(CuestionarioDTO peticion, String tipoCuestionario, ArchitechSessionBean psession)throws BusinessException ;
	
	/**
	 * @param claveCuestionario Clave del cuestionario
	 * @param psession ArchitechSessionBean de arquitectura
	 * @return CuestionarioDTO con resultado de operacion
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	public CuestionarioDTO obtenerTipoCuestionarioPorClave(int claveCuestionario, ArchitechSessionBean psession)
	throws BusinessException;
	
	/**
	 * Agregar un cuestionario al cliente
	 * @param cuestionario de agregarCuestionarioClienteNuevo
	 * @param insertarCuestionario indica si es necesario insertar un nuevo registro para cuestionario
	 * @param sesion de agregarCuestionarioClienteNuevo
	 * @throws BusinessException de agregarCuestionarioClienteNuevo
	 */
	public void agregarCuestionarioClienteNuevo(CuestionarioDTO cuestionario, boolean insertarCuestionario, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Agregar un cuestionario a un cliente existente
	 * @param cuestionario de agregarCuestionarioClienteExistente
	 * @param insertarCuestionario indica si es necesario insertar un nuevo registro para cuestionario
	 * @param sesion de agregarCuestionarioClienteExistente
	 * @throws BusinessException de agregarCuestionarioClienteExistente
	 */
	public void agregarCuestionarioClienteExistente(CuestionarioDTO cuestionario, boolean insertarCuestionario, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Obteiene informacion de los datos generales
	 * @param idCuestionario Identificador de Cuestionario
	 * @param buc Identificador del numero de cliente
	 * @param numeroContrato Numero de contrato que le fue asignado al Cuestionario
	 * @param fechaInicio de obtenerDatosGeneralesCuestionario
	 * @param fechaFin de obtenerDatosGeneralesCuestionario
	 * @param psession de obtenerDatosGeneralesCuestionario
	 * @return List<CuestionarioWSDTO> Datos generales de los cuestionarios encontrados con los filtros
	 * @throws BusinessException En caso de un problema con el acceso a los datos
	 */
	public List<CuestionarioWSDTO> obtenerDatosGeneralesCuestionario(String idCuestionario, String buc, String numeroContrato, Date fechaInicio, Date fechaFin, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene los datos generales de un cuestionario
	 * @param idCuestionario Identificador de cuestioanrio
	 * @param psession Objeto de sesion de Agave
	 * @return CuestionarioWSDTO Informacion del cuestionario
	 * @throws BusinessException En caso de un error al obtener los datos
	 */
	public CuestionarioWSDTO obtenerDatosGeneralesCuestionarioPorId(String idCuestionario, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene los datos generales de un cuestionario en donde el numero de contrato es diferente de nulo
	 * @param datosConsulta Datos de consulta
	 * @param psession Objeto de sesion de Agave
	 * @return CuestionarioWSDTO Informacion del cuestionario
	 * @throws BusinessException En caso de un error al obtener los datos
	 */
	public CuestionarioWSDTO obtenerDatosGeneralesCuestionarioPorIdCntrNoNulo(RequestObtenerCuestionario datosConsulta, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * @param claveClienteCuestionario clave de cliente-cuestionario
	 * @param psession ArchitechSessionBean de arquitectura
	 * @return CuestionarioDTO con resultado de operacion
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	public CuestionarioDTO obtenerDatosGeneralesCuestionarioPorClaveCC(int claveClienteCuestionario, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene los datos generales de un cuestionario con base al codigo de cliente, id de la aplicacion y el nivel de riesgo.
	 * Ademas de esos parametros la informacion debio haber sido dada de alta el dia de hoy
	 * @param psession Objeto de sesion de Agave
	 * @param cuestionarioDTO objeto con datos de consulta
	 * @return Cuestionarios que cumplen la condicion
	 * @throws BusinessException En caso de un problema al ejecutar las operaciones
	 */
	public List<CuestionarioDTO> obtenerDatosGeneralesCuestionarioMismoClienteMismoDia(CuestionarioDTO cuestionarioDTO, ArchitechSessionBean psession) 
	throws BusinessException;
}
