/**
 * Isban Mexico
 *   Clase: EnvioRelacionadosDAO.java
 *   Descripcion: Interfaz para el DAO EnvioRelacionadosDAO
 *   
 *   Control de Cambios:
 *   1.0 Jun 23, 2017 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerRelacionados;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerRelacionados;
import mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO;
/**
 * EnvioRelacionadosDAO
 * interface para el acceso a datos de los relacionados
 * por bloque
 * @author lespinosa
 *
 */
@Local
public interface EnvioRelacionadosDAO {
	
	
	/**
	 * Obtiene la informacion de los relacionados
	 * @param request Objeto para ejecutar la consulta
	 * @param psession Objeto de sesion de Agave
	 * @return response Informacion de los relacionados
	 * @throws BusinessException Mensaje de error
	 */
	List<ResponseObtenerRelacionados> obtenerRelacionados(RequestObtenerRelacionados request, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Actualiza estatus de la relacion en base a la clave de la relacion
	 * @param request Objeto para ejecutar la actualizacion del estatus
	 * @param estatus Estatus OK
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException Mensaje de error
	 */
	void actualizarEstatusRel(RequestDatosRelacionadosWSDTO request, String estatus, ArchitechSessionBean psession)throws BusinessException;
	
	/**
	 * Actualiza numero de intentos en base a la clave de la relacion
	 * @param request Objeto para ejecutar la actualizacion del intentos
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException Mensaje de error
	 */
	void actualizarIntentos(RequestDatosRelacionadosWSDTO request, ArchitechSessionBean psession)throws BusinessException;

	/**
	 * Actualiza estatusa error de la relacion en base a el id de cusetionario
	 * @param request Objeto para ejecutar la actualizacion del estatus
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException Mensaje de error
	 */
	void actualizarEstatusErr(RequestDatosRelacionadosWSDTO request, ArchitechSessionBean psession)throws BusinessException;
	
}
