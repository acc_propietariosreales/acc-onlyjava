/**
 * Isban Mexico
 *   Clase: EstadoDAO.java
 *   Descripcion: Interfaz para el DAO EstadoDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.EstadoDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface EstadoDAO {
	
	/** 
	 * Query para obtener todos los registros de la tabla NKM_MX_CAT_EDO
	 */
	public static final String SQL_OBTENER_ESTADO = 
			"select CVE_EDO_PK, COD_EDO, TXT_NOMB from NKM_MX_CAT_EDO";
	
	/**
	 * String SQL_OBTENER_ESTADO_POR_CODIGO
	 */
	public static final String SQL_OBTENER_ESTADO_POR_CODIGO = 
			"select CVE_EDO_PK, COD_EDO, TXT_NOMB from NKM_MX_CAT_EDO WHERE COD_EDO = '%s'";

	/**
	 * @param sesion ArchitechSessionBean de arquitectura
	 * @return List<EstadoDTO> lista de resultado
	 * @throws BusinessException BusinessException con mensaje de error 
	 */
	public List<EstadoDTO> obtenerEstados(ArchitechSessionBean sesion) throws BusinessException;
	/**
	 * @param codigoEstado codigo del estado
	 * @param sesion ArchitechSessionBean de arquitectura
	 * @return EstadoDTO con resultado
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	public EstadoDTO obtenerEstadoPorCodigo(String codigoEstado, ArchitechSessionBean sesion) throws BusinessException;
	
}
