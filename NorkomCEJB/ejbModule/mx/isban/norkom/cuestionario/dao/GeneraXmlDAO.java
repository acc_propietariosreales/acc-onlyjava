/**
 * Isban Mexico
 *   Clase: GeneraXmlDAO.java
 *   Descripcion: DAO para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioIPDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesXmlDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

@Local
public interface GeneraXmlDAO {
	
	/** Variable utilizada para definir aplicativo TERMINAL FINANCIERO **/
	public static final String TERM_FINANCIERO = "TF";
	
	/** Variable utilizada para definir aplicativo OPICS **/
	public static final String OPICS = "OP";
	
	/** Variable utilizada para definir aplicativo OPICS FRONT CLIENTES **/
	public static final String OPICS_FRONT = "FC";
	
	/** Variable utilizada para definir aplicativo LIGHT HOUSE **/
	public static final String LIGHT_HOUSE = "LH";
	
	/** Variable utilizada para declarar SQL_OBTENER_FORMULARIO_IP **/
	public static final String SQL_OBTENER_FORMULARIO_IP = new StringBuilder()
	.append("SELECT CCD.ID_CUEST, PARAM.VAL_PARAM, CCD.FCH_CREAC, CLIENTE.VAL_BUC, CCD.COD_ENT, ")
	.append("CLIENTE.TXT_NOMB_CLTE||' '||CLIENTE.TXT_APE_PAT_CLTE||' '||CLIENTE.TXT_APE_MAT_CLTE NOM_EMPRESA, ")
	.append("to_char(CLIENTE.FCH_NAC_CLTE,'yyyymmdd') FCH_NAC_CLTE, CCD.COD_TIPO_PERS, CCD.COD_SUB_TIPO_PERS, CCD.COD_SEGMT, ")
	.append("ACTIECON.COD_ACT_ECON, CCD.COD_PROD, CCD.COD_PAIS_RESID, CCD.COD_MUN_RESID, CCD.COD_NACIO, ")
	.append("CCD.COD_DIV_PROD, CCD.COD_SUCU, CCD.COD_REGN, CCD.COD_ZONA, CCD.COD_CENT_COSTO, CCD.COD_BRNCH, ")
	.append("CCD.COD_SUB_PROD, CCD.NUM_CNTR, CCD.CVE_CLTE_CUEST_PK, CCD.CVE_CLTE_FK, CCD.CVE_CUEST_IP_FK ")
	.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD, NKM_MX_MAE_CLTE_DRO CLIENTE, ")
	.append("NKM_MX_AUX_PARAM_DRO PARAM, NKM_MX_CAT_ACTIV_ECON_DRO ACTIECON ")
	.append("WHERE CCD.ID_CUEST = '%s' ")
	.append("AND SUBSTR(CCD.ID_CUEST,1,2) = PARAM.TXT_NOMBR_PK AND CCD.CVE_ACTIV_ESP = ACTIECON.CVE_ACT_ECON_PK ")
	.append("AND CCD.CVE_CLTE_FK = CLIENTE.CVE_CLTE_PK")
	.toString();
	
	/** Variable ultilizada para declarar SQL_OBTENER_OPCIONES_IP **/
	public static final String SQL_OBTENER_OPCIONES_IP = new StringBuilder()
	.append("SELECT PREGUNTA.CVE_PREG_PK, OPCIONES.VAL_OPC, OPCIONES.DSC_OPC, PREGUNTA.TXT_ABRV, PREGUNTA.CVE_RPT ")
	.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD, NKM_MX_REL_CUEST_RESP_DRO RCR, NKM_MX_PRC_RESP_PREG_DRO RESPUESTA, ")
	.append("NKM_MX_MAE_PREG_DRO PREGUNTA, NKM_MX_AUX_TIPO_PREG_DRO TIPO, NKM_MX_AUX_GRPO_OPC_PREG_DRO GRUPO, ")
	.append("NKM_MX_AUX_OPC_PREG_DRO OPCIONES ")
	.append("WHERE CCD.ID_CUEST = '%s' ")
	.append("AND CCD.CVE_CUEST_IP_FK = RCR.CVE_CUEST_FK AND CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK ")
	.append("AND RCR.CVE_CUEST_RESP_PK = RESPUESTA.CVE_CUEST_RESP_FK AND PREGUNTA.CVE_PREG_PK = RESPUESTA.CVE_PREG_FK ")
	.append("AND OPCIONES.CVE_OPC_PREG_PK = RESPUESTA.VAL_OPCN AND OPCIONES.CVE_GRPO_FK = PREGUNTA.CVE_GRPO_FK ")
	.append("GROUP BY PREGUNTA.CVE_PREG_PK, OPCIONES.VAL_OPC, OPCIONES.DSC_OPC, PREGUNTA.TXT_ABRV, PREGUNTA.CVE_RPT ")
	.append("ORDER BY PREGUNTA.CVE_PREG_PK")
	.toString();
	
	/** Variable ultilizada para declarar SQL_OBTENER_PAIS_TRANS **/
	public static final String SQL_OBTENER_PAIS_TRANS = new StringBuilder()
	.append(" SELECT RESPUESTA.VAL_OPCN, PAIS.COD_PAIS, PAIS.TXT_NOMB, PREGUNTA.CVE_RPT, PREGUNTA.TXT_ABRV FROM ") 
	.append("NKM_MX_MAE_CLTE_CUEST_DRO CCD join NKM_MX_REL_CUEST_RESP_DRO RCR on CCD.CVE_CUEST_IP_FK = RCR.CVE_CUEST_FK and  CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK ")
	.append("join NKM_MX_PRC_RESP_PREG_DRO RESPUESTA on RCR.CVE_CUEST_RESP_PK = RESPUESTA.CVE_CUEST_RESP_FK  AND RESPUESTA.CVE_PREG_FK = 5  ")
	.append("join NKM_MX_MAE_PREG_DRO PREGUNTA  on PREGUNTA.CVE_TIPO_PREG_FK = 3 AND PREGUNTA.CVE_GRPO_FK = 11  ")
	.append("join NKM_MX_CAT_PAIS PAIS on PAIS.CVE_PAIS_PK = RESPUESTA.VAL_OPCN  ")
	.append("WHERE CCD.ID_CUEST = '%s'").toString();
	
	/** Variable ultilizada para declarar SQL_OBTENER_RECURSOS_IP **/
	public static final String SQL_OBTENER_RECURSOS_IP = new StringBuilder()
	.append("SELECT RESPUESTA.VAL_OPCN, RECURSOS.COD_REC, RECURSOS.DSC_REC, PREGUNTA.TXT_ABRV, PREGUNTA.CVE_RPT ")
	.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD, NKM_MX_REL_CUEST_RESP_DRO RCR, NKM_MX_PRC_RESP_PREG_DRO RESPUESTA, ")
	.append("NKM_MX_MAE_PREG_DRO PREGUNTA, NKM_MX_AUX_TIPO_PREG_DRO TIPO, NKM_MX_CAT_ORIG_DEST_REC_DRO RECURSOS ")
	.append("WHERE CCD.ID_CUEST = '%s' ")
	.append("AND CCD.CVE_CUEST_IP_FK = RCR.CVE_CUEST_FK AND CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK ")
	.append("AND RCR.CVE_CUEST_RESP_PK = RESPUESTA.CVE_CUEST_RESP_FK AND RESPUESTA.CVE_PREG_FK = PREGUNTA.CVE_PREG_PK ")
	.append("AND PREGUNTA.CVE_TIPO_PREG_FK = TIPO.CVE_TIPO_PREG_PK AND PREGUNTA.CVE_GRPO_FK IN (12,13) ")
	.append("AND RECURSOS.CVE_REC_PK = RESPUESTA.VAL_OPCN ")
	.append("GROUP BY RESPUESTA.VAL_OPCN, RECURSOS.COD_REC, RECURSOS.DSC_REC,  PREGUNTA.TXT_ABRV, PREGUNTA.CVE_RPT")
	.toString();
	
	/** Variable ultilizada para declarar SQL_RELACIONADOS_IP **/
	public static final String SQL_OBTENER_RELACIONADOS_IP = new StringBuilder()
	.append("SELECT RELAC.TXT_NOMB_RELAC, RELAC.TXT_APE_PAT_RELAC, RELAC.TXT_APE_MAT_REL, ")
	.append("RELAC.COD_TIPO_PERS_REL, to_char(RELAC.FCH_NAC_REL,'yyyymmdd') FCH_NAC_REL , RELAC.COD_PAIS_NACIO_REL, RELAC.COD_PAIS_RESID_REL, ")
	.append("RELAC.COD_TIPO_RELAC ")
	.append("FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD, NKM_MX_AUX_RELAC_DRO RELAC ")
	.append("WHERE CCD.ID_CUEST = '%s' AND RELAC.CVE_CLTE_CUES_FK = CCD.CVE_CLTE_CUEST_PK ")
	.toString();
	
	/**
	 * METODO PARA CONSULTAR FORMULARIO_IP
	 * @param id_cuestionario de obtenerFormularioIP
	 * @param psession de obtenerFormularioIP
	 * @return CuestionarioIPDTO formularioIP de obtenerFormularioIP
	 * @throws BusinessException de obtenerFormularioIP
	 */
	public CuestionarioIPDTO obtenerFormularioIP(String id_cuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * METODO PARA CONSULTAR OPCIONES_IP
	 * @param id_cuestionario de obtenerOpcionesIP
	 * @param psession de obtenerOpcionesIP
	 * @return List<OpcionesRadioDTO> opcionesIP de obtenerOpcionesIP
	 * @throws BusinessException de obtenerOpcionesIP
	 */
	public  List<OpcionesXmlDTO> obtenerOpcionesIP(String id_cuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * METODO PARA CONSULTAR PAISES_TRANS_INTER
	 * @param id_cuestionario de obtenerPaisTransInter
	 * @param psession de obtenerPaisTransInter
	 * @return List<PaisDTO> paisesTransInter de obtenerPaisTransInter
	 * @throws BusinessException de obtenerPaisTransInter
	 */
	public List<PaisDTO> obtenerPaisTransInter(String id_cuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * METODO PARA CONSULTAR RECURSOS_IP
	 * @param id_cuestionario de obtenerRecursosIP 
	 * @param psession de obtenerRecursosIP
	 * @return List<RecursoDTO> recursosIP de obtenerRecursosIP
	 * @throws BusinessException from de obtenerRecursosIP
	 */
	public List<RecursoDTO> obtenerRecursosIP(String id_cuestionario, ArchitechSessionBean psession) throws BusinessException;

	
	/**
	 * METODO PARA CONSULTAR RELACIONADOS_IP
	 * @param id_cuestionario de obtenerRelacionadosIP
	 * @param psession de obtenerRelacionadosIP
	 * @return List<RelacionadoDTO> relacionadosIP de obtenerRelacionadosIP
	 * @throws BusinessException de obtenerRelacionadosIP
	 */
	public List<RelacionadoDTO> obtenerRelacionadosIP(String id_cuestionario, ArchitechSessionBean psession) throws BusinessException;
}
