/**
 * Isban Mexico
 *   Clase: PaisDAO.java
 *   Descripcion: Interfaz para el DAO PaisDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.PistaAuditoriaDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface ManejadorPistasDAO {
	
	/** 
	 * Query para obtener todos los registros de la tabla PM_PAIS
	 */
	public static final String SQL_AGREGAR_PISTA = new StringBuilder()
		.append("INSERT INTO EBE_AUDIT_ADMIN(SERV_TRANS, FECHA, HORA, IP_TERM, CAN_APLIC, USR, ID_OPER, ID_SESION, ID_WEB, HOST_NAME,")
		.append("COD_OPER, DATO_AFEC, VAL_ANT, VAL_NVO, TABLA_AFEC, TIPO_OPER, DATO_FIJO)")
		.append("VALUES('%s', SYSDATE, SYSDATE, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s')")
		.toString();
	
	/**
	 * Agrega pista de auditoria
	 * @param pistaDTO Objeto con la informacion de la pista
	 * @param sesion Objeto de sesion de Agave
	 * @throws BusinessException En caso de error de negocio
	 */
	public void agregarPista(PistaAuditoriaDTO pistaDTO, ArchitechSessionBean sesion) throws BusinessException;
	
}
