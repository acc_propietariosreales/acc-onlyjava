/**
 * Isban Mexico
 *   Clase: MunicipioDAO.java
 *   Descripcion: Interfaz para el DAO MunicipioDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.MunicipioDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface MunicipioDAO {
	
	/** 
	 * Query para obtener todos los registros de la tabla PM_PAIS
	 */
	public static final String SQL_OBTENER_MUNICIPIO = 
			"select CVE_MUN_PK, COD_MUN, TXT_NOMB, COD_EDO from NKM_MX_CAT_MUN";
	
	/**
	 * String SQL_OBTENER_MUNICIPIO_POR_CODIGO
	 */
	public static final String SQL_OBTENER_MUNICIPIO_POR_CODIGO = 
			"select CVE_MUN_PK, COD_MUN, TXT_NOMB, COD_EDO from NKM_MX_CAT_MUN WHERE COD_MUN = '%s' AND COD_EDO = '%s'";

	/**
	 * @param codigoMunicipio codigo del municipio
	 * @param codigoEstado codigo del estado
	 * @param sesion ArchitechSessionBean de arquitectura
	 * @return MunicipioDTO con resultado de la operacion
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	public MunicipioDTO obtenerMunicipioPorCodigo(String codigoMunicipio, String codigoEstado, ArchitechSessionBean sesion) throws BusinessException;
	
}
