/**
 * Isban Mexico
 *   Clase: NivelRiesgoDAO.java
 *   Descripcion: Interfaz para el DAO NivelRiesgoDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface NivelRiesgoDAO {
	/**
	 * String SQL_OBTENER_PARAMETRO_POR_NOMBRE
	 */
	public static final String SQL_OBTENER_VALORES_POR_IND_NKM = 
			"SELECT COD_IND_PERS_PK, VAL_IND_PERS_PK, VAL_IND_NKM, DSC_IND FROM NKM_MX_AUX_NIVEL_RIESG_DRO WHERE VAL_IND_NKM = '%s'";
	
	/**
	 * Sentencia SQL para obtener indicador de Norkom con base al indicador de Personas
	 */
	public static final String SQL_OBTENER_VALORES_POR_IND_PERSONAS = 
			"SELECT COD_IND_PERS_PK, VAL_IND_PERS_PK, VAL_IND_NKM, DSC_IND FROM NKM_MX_AUX_NIVEL_RIESG_DRO WHERE VAL_IND_PERS_PK = '%s'";
	
	/**
	 * String SQL_ACTUALIZAR_CALIFICACION_NKM
	 */
	public static final String SQL_ACTUALIZAR_CALIFICACION_NKM =
			"UPDATE NKM_MX_MAE_CLTE_CUEST_DRO SET COD_NIVEL_RIESG = '%s', COD_IND_UPLD = '%s' WHERE CVE_CLTE_CUEST_PK = %s";
	
	/**
	 * Obtiene la equivalencia del Nivel de Riesgo Norkom - Personas
	 * @param indicadorNorkom Indicador(Nivel Riesgo, Indicador UPLD) en Norkom
	 * @return CalificacionDTO Indicador del Nivel/UPLD en Personas
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public CalificacionDTO obtenerValoresPorIndicadorNorkom(String indicadorNorkom) throws BusinessException;
	
	/**
	 * Obtiene la equivalencia del Nivel de Riesgo Personas - Norkom
	 * @param indicadorPersonas Indicador(Nivel Riesgo, Indicador UPLD) en Personas
	 * @return CalificacionDTO Indicador del Nivel/UPLD en Norkom
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public CalificacionDTO obtenerValoresPorIndicadorPersonas(String indicadorPersonas) throws BusinessException;
	
	/**
	 * Guarda el nivel de riesgo y el indicador UPLD en Personas
	 * @param claveClienteCuest Clave de Cliente Cuestionario
	 * @param nivelRiesgo Nivel de Riesgo de Norkom
	 * @param indicadorUpld Indicador UPLD de Norkom
	 * @throws BusinessException En caso de un problema de negocio
	 */
	public void guardarCalificacionNorkom(int claveClienteCuest, String nivelRiesgo, String indicadorUpld) throws BusinessException;
}
