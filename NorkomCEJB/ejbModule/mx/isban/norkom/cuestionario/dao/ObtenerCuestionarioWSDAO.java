/**
 * Isban Mexico
 *   Clase: ObtenerCuestionarioWSDAO.java
 *   Descripcion: Interfaz para el DAO ObtenerCuestionarioWSDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;

/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
@Local
public interface ObtenerCuestionarioWSDAO {
	
	/** COMILLA  ESPACIO */
	public static final String PARAMETRO = "%s";
	/**
	 * String SELECT
	 */
	public static final String SELECT = "SELECT  ";
	/**
	 * String SELECT
	 */
	public static final String SELECT_DISTINCT = "SELECT DISTINCT ";
	/**
	 * String CVE_RPT
	 */
	public static final String CVE_RPT = " PREGUNTAS.CVE_RPT,  ";
	/**
	 * String FROM_PREGUNTAS
	 */
	public static final String FROM_CCD= " FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD ";
	/**
	 * String WHERE
	 */
	public static final String WHERE = " WHERE ";
	/**
	 * String JOIN_PREGUNTAS
	 */
	public static final String JOIN_PREGUNTAS = " INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_RPT NOT IN('strParametroX') ";
	/**
	 * String JOIN_RESPUESTA
	 */
	public static final String JOIN_RESPUESTA = " INNER JOIN NKM_MX_PRC_RESP_PREG_DRO RESPUESTA ON PREGUNTAS.CVE_PREG_PK = RESPUESTA.CVE_PREG_FK ";
	/**
	 * String JOIN_DETALLE
	 */
	public static final String JOIN_DETALLE = " INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IP_FK = DETALLE.CVE_CUEST_PK ";
	/**
	 * String AND_DETALLE
	 */
	public static final String AND_DETALLE = " AND PREGUNTAS.CVE_PREG_PK = DETALLE.CVE_PREG_PK ";
	/**
	 * String JOIN_RCR
	 */
	public static final String JOIN_RCR = " INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR ON CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK ";
	/**
	 * String AND_RCR
	 */
	public static final String AND_RCR = " AND RESPUESTA.CVE_CUEST_RESP_FK = RCR.CVE_CUEST_RESP_PK ";
	
	/** QRTY_ENCABEZADO_IP */
	public String QRY_ENCABEZADO_IP  =
	       new StringBuilder().append( SELECT)
	       .append( " CCD.CVE_CLTE_CUEST_PK, CCD.FCH_CREAC,CLIENTE.TXT_NOMB_CLTE,CLIENTE.TXT_APE_PAT_CLTE,CLIENTE.TXT_APE_MAT_CLTE, ")
	       .append( " upper(CCD.TXT_NOMB_SUCU) TXT_NOMB_SUCU, CCD.COD_SUCU, ")
	       .append( " upper(CCD.TXT_NOMB_PLAZA) TXT_NOMB_PLAZA, upper(CCD.TXT_NOMB_ZONA) TXT_NOMB_ZONA, ")
	       .append( " CCD.NUM_CNTR, upper(CCD.DSC_SEGMT) DSC_SEGMT, CCD.ID_CUEST, ")
	       .append( " upper(CCD.DSC_PROD) DSC_PROD, CCD.COD_NACIO,  ")
	       .append( " NAC.TXT_NOMB NACIONALIDAD, ACTIGEN.DSC_ACT_ECON CVE_ACTIV_GEN, ")
	       .append( " CCD.COD_MUN_RESID,CLIENTE.VAL_BUC, ")
	       .append( " CUESTIONARIO.COD_TIPO_PERS, CUESTIONARIO.COD_SUB_TIPO_PERS, ")
	       .append( " CUESTIONARIO.COD_DIV, ACTIECON.DSC_ACT_ECON, ")
	       .append( " ACTIECON.FLG_ENT_FIN, PAIS.FLG_RIESG, trim(CCD.COD_NIVEL_RIESG) COD_NIVEL_RIESG, ")
	       .append( " ACTIECON.FLG_SECTR, CUESTIONARIO.VAL_TIPO_CUEST, ")
	       .append( " PAIS.TXT_NOMB PAIS, PAIS.COD_PAIS, ")
	       .append( " ESTADO.TXT_NOMB ESTADO, MUNICIPIO.TXT_NOMB MUNICIPIO, CCD.TXT_NMBR_FUNC ")
	       .append( FROM_CCD)
	       .append( " INNER JOIN NKM_MX_MAE_CLTE_DRO CLIENTE ON CCD.CVE_CLTE_FK = CLIENTE.CVE_CLTE_PK ")
	       .append( " INNER JOIN NKM_MX_CAT_ACTIV_ECON_DRO ACTIECON ON CCD.CVE_ACTIV_ESP = ACTIECON.CVE_ACT_ECON_PK ")
	       .append( " LEFT JOIN NKM_MX_CAT_ACTIV_ECON_DRO ACTIGEN ON CCD.CVE_ACTIV_GEN = ACTIGEN.CVE_ACT_ECON_PK  ")
	       .append( " INNER JOIN NKM_MX_MAE_CUEST_DRO CUESTIONARIO ON CCD.CVE_CUEST_IP_FK = CUESTIONARIO.CVE_CUEST_PK ")
	       .append( " INNER JOIN NKM_MX_CAT_PAIS PAIS ON CCD.COD_PAIS_RESID = PAIS.COD_PAIS ")
	       .append( " INNER JOIN NKM_MX_CAT_PAIS NAC ON CCD.COD_NACIO = NAC.COD_PAIS  ")
	       .append( " LEFT JOIN NKM_MX_CAT_EDO ESTADO ON SUBSTR( CCD.COD_MUN_RESID,6,2) = ESTADO.COD_EDO")
	       .append( " LEFT JOIN NKM_MX_CAT_MUN MUNICIPIO ON SUBSTR( CCD.COD_MUN_RESID,6,2) = MUNICIPIO.COD_EDO  AND SUBSTR( CCD.COD_MUN_RESID,0,5) = MUNICIPIO.COD_MUN ")
	       .append( WHERE ).toString();
	
	/** QRTY_PREGUNTAS_SELECT_UNICA_IP */
	public String QRY_PREGUNTAS_SELECT_UNICA_IP  =
	       new StringBuilder().append( SELECT)
	       .append( CVE_RPT)	
	       .append( " decode(PREGUNTAS.CVE_RPT,'strPregSalProm',OPCIONES.DSC_OPC,OPCIONES.VAL_OPC) VAL_OPC ")	       
	       .append( " FROM NKM_MX_MAE_CLTE_CUEST_DRO CCD")
	       .append( JOIN_PREGUNTAS)
	       .append( " AND PREGUNTAS.CVE_TIPO_PREG_FK = 2 and PREGUNTAS.CVE_GRPO_FK BETWEEN 1 AND 8  or PREGUNTAS.CVE_GRPO_FK =66")
	       .append( JOIN_RESPUESTA)
	       .append( " INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK ")
	       .append( " AND OPCIONES.DSC_OPC IS NOT NULL ") 
	       .append( JOIN_DETALLE)
	       .append( AND_DETALLE)
	       .append( JOIN_RCR)
	       .append( AND_RCR)
	       .append( WHERE).toString();

	/** QRTY_PREGUNTAS_RADIO_BUTTON_IP */
	public String QRY_PREGUNTAS_RADIO_IP  =
	       new StringBuilder().append( SELECT)    
	       .append( CVE_RPT)
	       .append( " DECODE (OPCIONES.VAL_OPC,'2','0',OPCIONES.VAL_OPC) VAL_OPC  ")	   
	       .append( FROM_CCD)
	       .append( JOIN_PREGUNTAS)
	       .append( " AND PREGUNTAS.CVE_GRPO_FK = 10 AND PREGUNTAS.CVE_TIPO_PREG_FK = 4 ")
	       .append( JOIN_RESPUESTA)
	       .append( " INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK ")
	       .append( JOIN_DETALLE)
	       .append( AND_DETALLE)
	       .append( JOIN_RCR)
	       .append( AND_RCR)
	       .append( WHERE).toString();
	
	/** QRTY_PREGUNTAS_CATALOGO_PAIS_IP */
	public String QRY_PREGUNTAS_PAIS_IP  =
	       new StringBuilder().append( SELECT)	       
	       .append( CVE_RPT)
	       .append( " PAIS.TXT_NOMB  ")	       
	       .append( FROM_CCD)
	       .append( JOIN_PREGUNTAS)
	       .append( " AND PREGUNTAS.CVE_GRPO_FK = 11")
	       .append( JOIN_RESPUESTA)
	       .append( " INNER JOIN NKM_MX_CAT_PAIS PAIS ON RESPUESTA.VAL_OPCN = PAIS.CVE_PAIS_PK ")
	       .append( JOIN_DETALLE)
	       .append( AND_DETALLE)
	       .append( " INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR ON CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK")
	       .append( AND_RCR)
	       .append( WHERE).toString();
	
	/** QRTY_PREGUNTAS_ORIGEN_CATALOGO_RECURSOS_IP */
	public String QRY_PREGUNTAS_ORIGEN_IP  =
	       new StringBuilder().append( SELECT)	       
	       .append( CVE_RPT)
	       .append( " RECURSOS.DSC_REC  ")
	       .append( FROM_CCD)
	       .append( JOIN_PREGUNTAS)
	       .append( " AND PREGUNTAS.CVE_GRPO_FK = 12")
	       .append( JOIN_RESPUESTA)
	       .append( JOIN_DETALLE)
	       .append( " AND PREGUNTAS.CVE_PREG_PK       = DETALLE.CVE_PREG_PK ")
	       .append( JOIN_RCR)
	       .append( AND_RCR)
	       .append( " INNER JOIN NKM_MX_CAT_ORIG_DEST_REC_DRO RECURSOS ON  RESPUESTA.VAL_OPCN = RECURSOS.CVE_REC_PK ")
	       .append( " AND RECURSOS.FLG_TIPO_REC = 'O' ")
	       .append( WHERE).toString();
	
	/** QRTY_PREGUNTAS_DESTINO_CATALOGO_RECURSOS_IP */
	public String QRY_PREGUNTAS_DESTINO_IP  =
	       new StringBuilder().append( SELECT)	      
	       .append( CVE_RPT)
	       .append( " RECURSOS.DSC_REC  ")
	       .append( FROM_CCD)
	       .append( JOIN_PREGUNTAS)
	       .append( " AND PREGUNTAS.CVE_GRPO_FK = 13")
	       .append( JOIN_RESPUESTA)
	       .append( JOIN_DETALLE)
	       .append( AND_DETALLE)
	       .append( JOIN_RCR)
	       .append( AND_RCR)
	       .append( " INNER JOIN NKM_MX_CAT_ORIG_DEST_REC_DRO RECURSOS ON  RESPUESTA.VAL_OPCN = RECURSOS.CVE_REC_PK ")
	       .append( " AND RECURSOS.FLG_TIPO_REC = 'D' ")
	       .append( WHERE).toString();
	
	/** QRTY_ENCABEZADO_IC */
	public String QRY_ENCABEZADO_IC  =
	       new StringBuilder().append( SELECT)
	       .append( " CCD.FCH_CREAC, upper(CCD.TXT_NOMB_SUCU) TXT_NOMB_SUCU, ")
	       .append( " CCD.COD_SUCU, CCD.TXT_NOMB_PLAZA, ")
	       .append( " upper(CCD.TXT_NOMB_ZONA) TXT_NOMB_ZONA,upper(CLIENTE.TXT_NOMB_CLTE) TXT_NOMB_CLTE, ")
	       .append( " upper(CLIENTE.TXT_APE_PAT_CLTE) TXT_APE_PAT_CLTE, upper(CLIENTE.TXT_APE_MAT_CLTE) TXT_APE_MAT_CLTE, ")
	       .append( " CLIENTE.VAL_BUC, CCD.NUM_CNTR, ")
	       .append( " CUESTIONARIO.VAL_TIPO_CUEST, CUESTIONARIO.COD_TIPO_PERS, ")
	       .append( " CUESTIONARIO.COD_SUB_TIPO_PERS, CUESTIONARIO.COD_DIV, CCD.TXT_NMBR_FUNC ")
	       .append( FROM_CCD)
	       .append( " INNER JOIN NKM_MX_MAE_CLTE_DRO CLIENTE ON CCD.CVE_CLTE_FK = CLIENTE.CVE_CLTE_PK ")
	       .append( " INNER JOIN NKM_MX_MAE_CUEST_DRO CUESTIONARIO ON CCD.CVE_CUEST_IC_FK = CUESTIONARIO.CVE_CUEST_PK ")
	       .append( WHERE ).toString();
	
	/** QRTY_PREGUNTAS_LIBRES_IC */
	public String QRY_PREGUNTAS_LIBRES_IC  =
	       new StringBuilder().append( SELECT_DISTINCT)	       
	       .append( CVE_RPT)
	       .append( " DECODE(upper(RESPUESTA.TXT_RESP),'NA',' ',RESPUESTA.TXT_RESP) TXT_RESP  ")	       
	       .append( " FROM NKM_MX_PRC_RESP_PREG_DRO RESPUESTA ")
	       .append( " INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_PREG_PK=RESPUESTA.CVE_PREG_FK AND RESPUESTA.TXT_RESP IS NOT NULL ")
	       .append( " AND PREGUNTAS.CVE_RPT NOT IN('strParametroX') AND PREGUNTAS.CVE_TIPO_PREG_FK = 1 and PREGUNTAS.CVE_GRPO_FK = 0 ")
	       .append( " INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR ON RCR.CVE_CUEST_RESP_PK = RESPUESTA.CVE_CUEST_RESP_FK ")
	       .append( " INNER JOIN NKM_MX_MAE_CLTE_CUEST_DRO  CCD on CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK ")
	       .append( " INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON PREGUNTAS.CVE_PREG_PK = DETALLE.CVE_PREG_PK ")
	       .append( WHERE).toString();
	
	/** QRTY_PREGUNTAS_ESPECIFICAS_RECURSOS_IC_1 */
	public String QRY_PREGUNTAS_ESPECIFICAS_RECURSOS_IC_1  =
	       new StringBuilder().append( SELECT_DISTINCT)	       
	       .append( CVE_RPT)
	       .append( " RESPUESTA.TXT_RESP, ")	    
	       .append( " PREGUNTAS.TXT_PREG ")
	       .append( " FROM NKM_MX_PRC_RESP_PREG_DRO RESPUESTA ")
	       .append( " INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_PREG_PK=RESPUESTA.CVE_PREG_FK AND RESPUESTA.TXT_RESP IS NOT NULL ")
	       .append( " AND PREGUNTAS.CVE_RPT NOT IN('strParametroX') AND PREGUNTAS.CVE_TIPO_PREG_FK = 1 ")
	       .append( " AND PREGUNTAS.CVE_GRPO_FK = 0 AND PREGUNTAS.CVE_RPT LIKE '%strPreg%'")
	       .toString();
	
	 /** QRTY_PREGUNTAS_ESPECIFICAS_RECURSOS_IC_2 */
	public String QRY_PREGUNTAS_ESPECIFICAS_RECURSOS_IC_2  =      new StringBuilder()
		   .append( " INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR ON RCR.CVE_CUEST_RESP_PK = RESPUESTA.CVE_CUEST_RESP_FK ")
	       .append( " INNER JOIN NKM_MX_MAE_CLTE_CUEST_DRO  CCD on CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK ")
	       .append( WHERE).toString();
	
	/** QRTY_PREGUNTAS_SELECT_UNICA_IC */
	public String QRY_PREGUNTAS_SELECT_UNICA_IC  =
	       new StringBuilder().append( SELECT)
	       .append( CVE_RPT)	
	       .append( " OPCIONES.VAL_OPC  ")	       
	       .append( FROM_CCD)
	       .append( " INNER JOIN NKM_MX_MAE_PREG_DRO PREGUNTAS ON PREGUNTAS.CVE_RPT NOT IN('strParametroX') AND PREGUNTAS.CVE_TIPO_PREG_FK = 2 or PREGUNTAS.CVE_PREG_PK=269 ")
	       .append( JOIN_RESPUESTA)
	       .append( " INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK AND OPCIONES.DSC_OPC IS NOT NULL ")
	       .append( " INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IC_FK = DETALLE.CVE_CUEST_PK ")
	       .append( " AND PREGUNTAS.CVE_PREG_PK = DETALLE.CVE_PREG_PK")
	       .append( " INNER JOIN NKM_MX_REL_CUEST_RESP_DRO RCR ON CCD.CVE_CLTE_CUEST_PK = RCR.CVE_CLTE_CUEST_FK")
	       .append( AND_RCR)
	       .append( WHERE).toString();
		
	/** QRTY_PREGUNTAS_RADIO_BUTTON_IC */
	public String QRY_PREGUNTAS_RADIO_IC  =
	       new StringBuilder().append( SELECT)    
	       .append( CVE_RPT)
	       .append( " DECODE (OPCIONES.VAL_OPC,'2','0',OPCIONES.VAL_OPC) VAL_OPC  ")	   
	       .append( FROM_CCD)
	       .append( JOIN_PREGUNTAS)
	       .append( " AND PREGUNTAS.CVE_GRPO_FK = 10 AND PREGUNTAS.CVE_TIPO_PREG_FK = 4 ")
	       .append( JOIN_RESPUESTA)
	       .append( " INNER JOIN NKM_MX_AUX_OPC_PREG_DRO OPCIONES ON RESPUESTA.VAL_OPCN = OPCIONES.CVE_OPC_PREG_PK ")
	       .append( " INNER JOIN NKM_MX_REL_DETA_CUEST_DRO DETALLE ON CCD.CVE_CUEST_IC_FK = DETALLE.CVE_CUEST_PK ")
	       .append( AND_DETALLE)
	       .append( JOIN_RCR)
	       .append( AND_RCR)
	       .append( WHERE).toString();
	
	/** QRTY_PREGUNTAS_INFORMACION_FAMILIARES_DEL_CLIENTE_IC */
	public String QRY_PREGUNTAS_INFORMACION_FAMILIARES_IC  =
	       new StringBuilder().append( SELECT_DISTINCT )
	       .append( " upper(FAMILIARES.TXT_NOMB_FAM) TXT_NOMB_FAM,  ")	
	       .append( " upper(FAMILIARES.TXT_DOMIC_FAM) TXT_DOMIC_FAM,  ")	 
	       .append( " FAMILIARES.TXT_FCH_NAC,  ")
	       .append( " FAMILIARES.TXT_PAREN  ")
	       .append( FROM_CCD)
	       .append( " INNER JOIN NKM_MX_AUX_FAM_DRO FAMILIARES ON FAMILIARES.CVE_CLTE_CUEST_FK = CCD.CVE_CLTE_CUEST_PK ")
	       .append( WHERE).toString();
	
	/** QRTY_INFORMACION_ACCIONISTAS_DEL_CLIENTE_IC */
	public String QRY_INFORMACION_ACCIONISTAS_IC  =
	       new StringBuilder().append( SELECT_DISTINCT )
	       .append( " upper(RELACION.TXT_NOMB_RELAC) TXT_NOMB_RELAC,  ")	
	       .append( " upper(RELACION.TXT_APE_PAT_RELAC) TXT_APE_PAT_RELAC,  ")	 
	       .append( " upper(RELACION.TXT_APE_MAT_REL) TXT_APE_MAT_REL,  ")	
	       .append( " RELACION.FCH_NAC_REL,  ")	
	       .append( " PAIS.TXT_NOMB,  ")	 
	       .append( " RELACION.COD_TIPO_PERS_REL,  ")	
	       .append( " RELACION.POR_PART  ")
	       .append( FROM_CCD)
	       .append( " INNER JOIN NKM_MX_AUX_RELAC_DRO RELACION ON RELACION.CVE_CLTE_CUES_FK = CCD.CVE_CLTE_CUEST_PK ")
	       .append( " LEFT JOIN NKM_MX_CAT_PAIS PAIS ON PAIS.COD_PAIS = RELACION.COD_PAIS_NACIO_REL ")
	       .append( " WHERE RELACION.COD_TIPO_RELAC = 'A' AND ").toString();
	/**
	 * Metodo para obtener ls datos de el cuestionario IP 
	 * @param peticion  con datos para la busqueda IP
	 * @param psession ArchitechSessionBean de arquitectura
	 * @return ResponseObtenerPdf con resultado
	 * @throws BusinessException con mensaje de error
	 */
	ResponseObtenerCuestionario obtenerCuestionarioIP(RequestObtenerCuestionario peticion,ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Metodo para obtener ls datos de el cuestionario IC  
	 * @param peticion  con datos para la busqueda
	 * @param dto ResponseObtenerCuestionario con datos de IP 
	 * @param psession ArchitechSessionBean bean de session
	 * @return ResponseObtenerPdf con resultado
	 * @throws BusinessException con mensaje de error
	 */
	ResponseObtenerCuestionario obtenerCuestionarioIC(RequestObtenerCuestionario peticion,ResponseObtenerCuestionario dto,ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Metodo para obtener los datos para obtener los datos para el Nivel de Riesgo  
	 * @param peticion  con datos para realizar la busqueda
	 * @param psession ArchitechSessionBean de arquitectura
	 * @return ResponseObtenerPdf con resultado
	 * @throws BusinessException con mensaje de error
	 */
	ResponseObtenerCuestionario obtenerDatosNivelRiesgo(RequestObtenerCuestionario peticion,ArchitechSessionBean psession) throws BusinessException;
	
}
