/**
 * Isban Mexico
 *   Clase: OpcionesPreguntaDAO.java
 *   Descripcion: Interfaz para el DAO OpcionesPreguntaDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;

@Local
public interface OpcionesPreguntaDAO {
	/***
	 * Query para la consulta del catalogo de opciones de pregunta.
	 */
	public static final String SQL_QUERY_SELECT_OPCIONES_PREGUNTA_BY_TIPO=
			"SELECT CVE_OPC_PREG_PK, DSC_OPC, VAL_OPC, CVE_GRPO_FK FROM NKM_MX_AUX_OPC_PREG_DRO WHERE CVE_GRPO_FK = %s ORDER BY CVE_GRPO_FK, CVE_OPC_PREG_PK";
	
	/**
	 * String SQL_OBTENER_OPCIONES_POR_PREGUNTA
	 */
	public static final String SQL_OBTENER_OPCIONES_POR_PREGUNTA = 
			"SELECT DISTINCT op.CVE_OPC_PREG_PK, op.VAL_OPC, op.CVE_GRPO_FK, op.DSC_OPC " +
			"FROM NKM_MX_MAE_PREG_DRO pr, NKM_MX_AUX_GRPO_OPC_PREG_DRO go, NKM_MX_AUX_OPC_PREG_DRO op " +
			"WHERE pr.TXT_SECCN = '%s' AND pr.TXT_ABRV = '%s' " +
			"AND pr.CVE_GRPO_FK = go.CVE_GRPO_PK AND go.CVE_GRPO_PK = op.CVE_GRPO_FK";
	/**
	 * Metodo para obtener todos los tipos de Pregunta de la base de datos
	 * @param idTipo id del tipo a buscar
	 * @param sesion session bean de agave
	 * @return List<OpcionesPreguntaDTO> con datos obtenidos
	 * @throws BusinessException En caso de error de negocio
	 */
	public List<OpcionesPreguntaDTO> obtenerOpcionesPregunta(String idTipo,ArchitechSessionBean sesion) throws BusinessException;
	/**
	 * @param idSeccion id de la seccion
	 * @param idAbreviatura id de la abreviatura
	 * @param sesion ArchitechSessionBean de arquitectura
	 * @return List<OpcionesPreguntaDTO> con resultado de la operacion
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	public List<OpcionesPreguntaDTO> obtenerOpcionesPorSeccionAbreviatura(String idSeccion, String idAbreviatura, ArchitechSessionBean sesion) throws BusinessException;
}
