/**
 * Isban Mexico
 *   Clase: OrigenDestinoRecursosDAO.java
 *   Descripcion: Interfaz para el DAO OrigenDestinoRecursosDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface OrigenDestinoRecursosDAO {
	/**
	 * String SQL_OBTENER_RECURSOS
	 */
	public static final String SQL_OBTENER_RECURSOS = 
			"SELECT distinct CVE_REC_PK, TRIM(DSC_REC) DESCRIPCION, COD_REC, FLG_TIPO_PERS, FLG_TIPO_REC FROM NKM_MX_CAT_ORIG_DEST_REC_DRO WHERE FLG_TIPO_PERS = '%s' AND FLG_TIPO_REC = '%s' AND FLG_ACTIV = 1";
	
	/**
	 * String SQL_OBTENER_RECURSO_POR_CLAVE
	 */
	public static final String SQL_OBTENER_RECURSO_POR_CLAVE = 
			"SELECT distinct CVE_REC_PK, TRIM(DSC_REC) DESCRIPCION, COD_REC, FLG_TIPO_PERS, FLG_TIPO_REC FROM NKM_MX_CAT_ORIG_DEST_REC_DRO WHERE CVE_REC_PK = %s";

	/**
	 * @param tipoPersona el tipo de persona
	 * @param tipoRecurso el tipo de recurso
	 * @param sesion ArchitechSessionBean bean
	 * @return List<RecursoDTO> con datos de respuesta
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	List<RecursoDTO> obtenerRecursos(String tipoPersona, String tipoRecurso, ArchitechSessionBean sesion) throws BusinessException;
	/**
	 * @param claveRecurso clave del recurso
	 * @param sesion ArchitechSessionBean de aqrquitectura
	 * @return RecursoDTO con datos del resultado
	 * @throws BusinessException BusinessException con mensaje de error 
	 */
	RecursoDTO obtenerRecursoPorClave(int claveRecurso, ArchitechSessionBean sesion) throws BusinessException;
}
