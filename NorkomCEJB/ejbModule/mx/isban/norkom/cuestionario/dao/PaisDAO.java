/**
 * Isban Mexico
 *   Clase: PaisDAO.java
 *   Descripcion: Interfaz para el DAO de paises
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.PaisDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface PaisDAO {
	
	/** 
	 * Query para obtener todos los registros de la tabla PM_PAIS
	 */
	public static final String SQL_OBTENER_PAIS = 
			"select CVE_PAIS_PK, COD_PAIS, TXT_NOMB, FLG_RIESG from NKM_MX_CAT_PAIS where FLG_ACTIV = 1  ORDER BY TXT_NOMB ASC";
	
	/**
	 * String SQL_OBTENER_PAIS_POR_CODIGO
	 */
	public static final String SQL_OBTENER_PAIS_POR_CODIGO = 
			"select CVE_PAIS_PK, COD_PAIS, TXT_NOMB, FLG_RIESG from NKM_MX_CAT_PAIS WHERE COD_PAIS = '%s'";


	/**
	 * Metodo para obtener todos los paises de la base de datos
	 * @param sesion ArchitechSessionBean bean de session agave
	 * @return List<PaisDTO> con los registros obtenidos de la base de datos
	 * @throws BusinessException con mensaje de error
	 */
	public List<PaisDTO> obtenerPaises(ArchitechSessionBean sesion) throws BusinessException;
	/**
	 * @param codigoPais codigo de pais a buscar
	 * @param sesion ArchitechSessionBean bean de session agave
	 * @return PaisDTO con datos del resultado
	 * @throws BusinessException con mensaje de error
	 */
	public PaisDTO obtenerPaisPorCodigo(String codigoPais, ArchitechSessionBean sesion) throws BusinessException;
	
}
