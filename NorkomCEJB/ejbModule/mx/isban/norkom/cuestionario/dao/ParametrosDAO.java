/**
 * Isban Mexico
 *   Clase: ParametrosDAO.java
 *   Descripcion: Interfaz para el DAO ParametrosDAO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

@Local
public interface ParametrosDAO {
	/**
	 * String SQL_OBTENER_PARAMETRO_POR_NOMBRE
	 */
	public static final String SQL_OBTENER_PARAMETRO_POR_NOMBRE = 
			"SELECT TXT_NOMBR_PK, VAL_PARAM, FLG_ACTIV FROM NKM_MX_AUX_PARAM_DRO WHERE TXT_NOMBR_PK = '%s' AND FLG_ACTIV = %s";
	
	/**
	 * String SQL_OBTENER_PARAMETROS_ACTIVOS
	 */
	public static final String SQL_OBTENER_PARAMETROS_ACTIVOS = 
			"SELECT TXT_NOMBR_PK, VAL_PARAM, FLG_ACTIV FROM NKM_MX_AUX_PARAM_DRO WHERE FLG_ACTIV = %s";
	
	/**
	 * @param nombreParametro el nombre del parametro a buscar
	 * @return ParametroDTO con datos de la respuesta
	 * @throws BusinessException BusinessException  con mensaje de error
	 */
	public ParametroDTO obtenerParametroPorNombre(String nombreParametro) throws BusinessException;
	/**
	 * @return List<ParametroDTO> lista de parametros encontrados
	 * @throws BusinessException BusinessException con mensaje de error
	 */
	public List<ParametroDTO> obtenerParametros() throws BusinessException;
}
