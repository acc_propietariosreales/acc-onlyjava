/**
 * Isban Mexico
 *   Clase: CuestionarioDAO.java
 *   Descripcion: Interfaz para el DAO CuestionarioDAO
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Local
public interface PreguntasDAO {
	/**
	 * Sentencia SQL para actualizar la respuesta de una pregunta
	 */
	public static final String SQL_ACTUALIZAR_RESP_PREG =
			"UPDATE NKM_MX_PRC_RESP_PREG_DRO SET TXT_RESP = '%s', VAL_OPCN = %s WHERE CVE_RESP_PREG_PK = %s";
	
	/**
	 * Variable utilizada para declarar SQL_OBTENER_PREGUNTAS_POR_ID_CUESTIONARIO
	 */
	static final String SQL_OBTENER_PREGUNTAS_POR_ID_CUESTIONARIO =
		"SELECT PR.CVE_PREG_PK, PR.TXT_TITUL, PR.TXT_PREG, TP.COD_TIPO_PREG, PR.VAL_LONG_RESP, PR.FLG_NUMER, " +
		"TP.CVE_TIPO_PREG_PK as CVE_TIPO_PREG, GP.CVE_GRPO_PK, GP.DSC_GRPO, PR.TXT_SECCN, PR.TXT_ABRV " +
		"FROM NKM_MX_REL_DETA_CUEST_DRO det, NKM_MX_MAE_PREG_DRO pr, NKM_MX_AUX_TIPO_PREG_DRO tp, NKM_MX_AUX_GRPO_OPC_PREG_DRO gp " +
		"WHERE det.CVE_PREG_PK = PR.CVE_PREG_PK AND PR.CVE_TIPO_PREG_FK = TP.CVE_TIPO_PREG_PK AND PR.CVE_GRPO_FK = GP.CVE_GRPO_PK " +
		"AND det.CVE_CUEST_PK = %s " +
		"ORDER BY PR.TXT_SECCN, PR.VAL_ORD, PR.CVE_PREG_PK";

	/**
	 * String SQL_OBTENER_PREGUNTAS_ASIGNADAS_POR_ABREVIATURAS
	 */
	static final String SQL_OBTENER_PREGUNTAS_ASIGNADAS_POR_ABREVIATURAS = 
			"SELECT PR.CVE_PREG_PK, RP.CVE_RESP_PREG_PK, PR.TXT_TITUL, PR.TXT_PREG, TP.COD_TIPO_PREG, PR.VAL_LONG_RESP, PR.FLG_NUMER, " +
			"    TP.CVE_TIPO_PREG_PK as CVE_TIPO_PREG, GP.CVE_GRPO_PK, GP.DSC_GRPO, PR.TXT_SECCN, PR.TXT_ABRV " +
			"FROM NKM_MX_MAE_CLTE_CUEST_DRO cc, NKM_MX_REL_CUEST_RESP_DRO cr, NKM_MX_PRC_RESP_PREG_DRO rp, " + 
			"    NKM_MX_MAE_PREG_DRO pr, NKM_MX_AUX_TIPO_PREG_DRO tp, NKM_MX_AUX_GRPO_OPC_PREG_DRO gp " +
			"WHERE cc.ID_CUEST = '%s' AND pr.TXT_ABRV IN (%s) AND cc.CVE_CLTE_CUEST_PK = cr.CVE_CLTE_CUEST_FK " + 
			"    AND cr.CVE_CUEST_RESP_PK = rp.CVE_CUEST_RESP_FK AND rp.CVE_PREG_FK = pr.CVE_PREG_PK " + 
			"    AND PR.CVE_TIPO_PREG_FK = TP.CVE_TIPO_PREG_PK AND PR.CVE_GRPO_FK = GP.CVE_GRPO_PK " +
			"ORDER BY PR.TXT_SECCN, PR.VAL_ORD, PR.CVE_PREG_PK ";
	
	/**
	 * String SQL_OBTENER_PREGUNTAS_POR_ABREV
	 */
	static final String SQL_OBTENER_PREGUNTAS_POR_ABREV = 
			"SELECT np.CVE_PREG_PK, np.TXT_TITUL, np.TXT_PREG, np.VAL_LONG_RESP, np.FLG_NUMER, " +
			"np.CVE_TIPO_PREG_FK, np.CVE_GRPO_FK, np.TXT_SECCN, np.TXT_ABRV, ntp.COD_TIPO_PREG " +
			"FROM NKM_MX_MAE_PREG_DRO np, NKM_MX_AUX_TIPO_PREG_DRO ntp " +
			"WHERE np.CVE_TIPO_PREG_FK = ntp.CVE_TIPO_PREG_PK AND np.TXT_ABRV IN (%s)";
	
	/**
	 * String SQL_OBTENER_RESP_PREG_POR_SECCION_ABREVIATURA
	 */
	static final String SQL_OBTENER_RESP_PREG_POR_SECCION_ABREVIATURA = 
			"SELECT pr.CVE_PREG_PK, pr.TXT_TITUL, pr.TXT_PREG, pr.TXT_SECCN, pr.TXT_ABRV, rp.TXT_RESP, rp.VAL_OPCN, " +
			"tp.CVE_TIPO_PREG_PK, tp.COD_TIPO_PREG, gp.CVE_GRPO_PK, gp.DSC_GRPO " +
			"FROM NKM_MX_MAE_CLTE_CUEST_DRO cc, NKM_MX_REL_CUEST_RESP_DRO cr, NKM_MX_PRC_RESP_PREG_DRO rp, " +
			"NKM_MX_MAE_PREG_DRO pr, NKM_MX_AUX_TIPO_PREG_DRO tp, NKM_MX_AUX_GRPO_OPC_PREG_DRO gp " +
			"WHERE cc.ID_CUEST = '%s' AND cc.CVE_CLTE_CUEST_PK = cr.CVE_CLTE_CUEST_FK " +
			"AND cc.CVE_CUEST_IP_FK = cr.CVE_CUEST_FK AND cr.CVE_CUEST_RESP_PK = rp.CVE_CUEST_RESP_FK " +
			"AND rp.CVE_PREG_FK = pr.CVE_PREG_PK AND pr.TXT_SECCN = '%s' AND pr.TXT_ABRV = '%s' " +
			"AND pr.CVE_TIPO_PREG_FK = tp.CVE_TIPO_PREG_PK AND pr.CVE_GRPO_FK = gp.CVE_GRPO_PK";
	
	/**
	 * Variable utilizada para declarar SQL_NUMERO_PREGUNTAS_CUESTIONARIO
	 */
	static final String SQL_NUMERO_PREGUNTAS_CUESTIONARIO = new StringBuilder()
			.append("SELECT COUNT(cc.CVE_CLTE_CUEST_PK) FROM NKM_MX_MAE_CLTE_CUEST_DRO cc, NKM_MX_REL_CUEST_RESP_DRO cr, NKM_MX_PRC_RESP_PREG_DRO rp ")
			.append("WHERE cc.ID_CUEST = '%s' AND cc.CVE_CLTE_CUEST_PK = cr.CVE_CLTE_CUEST_FK AND cr.CVE_CUEST_RESP_PK = rp.CVE_CUEST_RESP_FK")
			.toString();
	
	/**
	 * Obtiene las preguntas del Cuestionario IP
	 * @param claveCuestionario de obtenerPreguntasCuestionarioIp
	 * @param psession de obtenerPreguntasCuestionarioIp
	 * @return List<PreguntaDTO>
	 * @throws BusinessException con mensaje de error
	 */
	public List<PreguntaDTO> obtenerPreguntasCuestionarioPorClave(int claveCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene las preguntas asignadas a un cuestionario con base a la abreviatura de la pregunta
	 * @param idCuestionario Identificador de cuestionario al que pertenecen las preguntas
	 * @param abreviaturas abreviatura por la cual buscar
	 * @param psession SESSION objeto de session
	 * @return List<PreguntaDTO> lista de preguntas
	 * @throws BusinessException  con mensaje de error
	 */
	public List<PreguntaDTO> obtenerPreguntasAsignadasPorAbreviaturas(String idCuestionario, String abreviaturas, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * @param abreviaturas abreviatura por la cual buscar
	 * @param psession objeto de session
	 * @return List<PreguntaDTO lista de preguntas
	 * @throws BusinessException  con mensaje de error
	 */
	public List<PreguntaDTO> obtenerPreguntasPorAbreviaturas(String abreviaturas, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * @param idCuestionario id del cuestionario
	 * @param seccion nobre de la seccion
	 * @param abreviatura Abreviatura de las preguntas
	 * @param psession objeto de session
	 * @return CuestionarioRespuestaDTO con datos de respuesta
	 * @throws BusinessException con mensaje de error
	 */
	public CuestionarioRespuestaDTO obtenerPreguntaPorSeccionAbreviatura(String idCuestionario, String seccion, String abreviatura, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtener el numero de preguntas del cuestionario
	 * @param idCuestionario de numeroPreguntasEnCuestionario
	 * @return Numero de preguntas en un cuestionario
	 * @throws BusinessException  con mensaje de error
	 */
	public int numeroPreguntasEnCuestionario(String idCuestionario) throws BusinessException;
	
	/**
	 * Guardar las respuestas de las preguntas asignadas a un cuestionario
	 * @param cuestionario Informacion del cuestionario
	 * @param respuestas Respuestas que se desean agregar
	 * @param claveCuestionarioRespuesta Clave de la relacion entre el cuestionario y la respuesta
	 * @param sesion Datos de la sesion de Agave
	 * @throws BusinessException En caso de error con los datos
	 */
	public void agregarRespuestasCuestionario(
			CuestionarioDTO cuestionario, List<CuestionarioRespuestaDTO> respuestas, int claveCuestionarioRespuesta, ArchitechSessionBean sesion)
	throws BusinessException;
	
	/***
	 * 
	 * @param claveClienteCuestionario calve del cuestionario
	 * @param claveCuestionario tipo de cuestionario
	 * @throws BusinessException con mensaje de error
	 * @return id de cveCuestResp
	 */
	public BigDecimal obtenerCveCuestResp(Integer claveClienteCuestionario,Integer claveCuestionario) throws BusinessException;
	
	/**
	 * Actualiza los valores de una respuesta
	 * @param respuesta La respuesta que se desea actualizar
	 * @throws BusinessException En caso de un error al obtener la información 
	 */
	public void ejecutarActualizacionRespuesta(CuestionarioRespuestaDTO respuesta) throws BusinessException;
}
