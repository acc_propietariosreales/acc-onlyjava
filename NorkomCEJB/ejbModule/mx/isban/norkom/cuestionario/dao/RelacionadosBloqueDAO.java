/**
 * Isban Mexico
 *   Clase: RelacionadosBloqueDAO.java
 *   Descripcion: Interfaz para el DAO RelacionadosBloqueDAO
 *   
 *   Control de Cambios:
 *   1.0 Jun 14, 2017 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

/**
 * 
 * @author jagutierrez1
 *
 */
@Local
public interface RelacionadosBloqueDAO {
	/**
	 * Metodo para agregar los relacionados por bloque
	 * @param relacionadosWS lista con datos relacionados
	 * @param claveCuestionario clave del cuestionario
	 * @param bloque numero de bloque
	 * @param psession bean de session
	 * @return List<RelacionadoDTO> con resultado
	 * @throws BusinessException con mensaje de error
	 */
	List<RelacionadoDTO> agregarRelacionados(List<RelacionadoDTO> relacionadosWS, int claveCuestionario,String bloque, ArchitechSessionBean psession) 
	throws BusinessException;

	/**
	 * Agregar un cuestionario a un cliente existente
	 * @param cuestionario Informacion del cuestionario que se desea agregar
	 * @param buc string buc de usuario
	 * @param sesion Objeto de sesion de Agave
	 * @throws BusinessException En caso de error con la informacion o con la operacion
	 */
	void agregarCuestionarioBasico(CuestionarioDTO cuestionario,String buc, ArchitechSessionBean sesion)
	throws BusinessException;


}
