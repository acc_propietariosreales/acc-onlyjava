/**
 * Isban Mexico
 *   Clase: ActividadEconomicaDAOImpl.java
 *   Descripcion: Componente para consultar informacion de las actividades economicas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.ActividadEconomicaDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;


/**
 * Session Bean implementation class ActividadEconomicaDAOImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ActividadEconomicaDAOImpl extends GenericBdIsbanDA implements ActividadEconomicaDAO,MapeoConsulta<ActividadEconomicaDTO>  {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -4400473263983659694L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(ActividadEconomicaDAOImpl.class);
	/**
     * @see Architech#Architech()
     */
    public ActividadEconomicaDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    

    /**
     * @param codigoActividad codigo de la actividad para la busqueda
     * @return ActividadEconomicaDTO con resultado
     * @throws BusinessException con mensaje de error
     */
    public ActividadEconomicaDTO obtenerActividadEconomica(String codigoActividad) throws BusinessException {
    	LOG.info(String.format("Obteniendo datos de la Actividad Economica [%s]", codigoActividad));
    	return consultarUnico(String.format(SQL_OBTENER_ACTIVIDAD_POR_CODIGO, codigoActividad,"ESP"), this);
    }
    /**
     * @param CVE de la actividad Especifica para la busqueda
     * @return ActividadEconomicaDTO con resultado
     * @throws BusinessException con mensaje de error
     */
    public ActividadEconomicaDTO obtenerActividadEconomicaPorID(String codigoActividad) throws BusinessException {
    	LOG.info(String.format("Obteniendo datos de la Actividad Economica [%s]", codigoActividad));
    	return consultarUnico(String.format(SQL_OBTENER_ACTIVIDAD_POR_CODIGO_PK, codigoActividad,"ESP"), this);
    }
    /**
     * @param codigoActividad codigo de la actividad para la busqueda actividad Generica
     * @return ActividadEconomicaDTO con resultado
     * @throws BusinessException con mensaje de error
     */
    public ActividadEconomicaDTO obtenerActividadEconomicaGenerica(String codigoActividad) throws BusinessException {
    	LOG.info(String.format("Obteniendo datos de la Actividad Economica [%s]", codigoActividad));
    	return consultarUnico(String.format(SQL_OBTENER_ACTIVIDAD_POR_CODIGO, codigoActividad,"GEN"), this);
    }
	@Override
	public ActividadEconomicaDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		ActividadEconomicaDTO actividadEconomica = new ActividadEconomicaDTO();
		
		actividadEconomica.setIdActividad(getIntegerValue(registro, "CVE_ACT_ECON_PK"));
		actividadEconomica.setDescripcion(getStringValue(registro, "DSC_ACT_ECON"));
		actividadEconomica.setTipoActividad(getStringValue(registro, "VAL_TIPO_ACT_ECON"));
		actividadEconomica.setEntidadFinanciera(getStringValue(registro, "FLG_ENT_FIN"));
		actividadEconomica.setCodigo(getStringValue(registro, "COD_ACT_ECON"));
		actividadEconomica.setTipoSector(getStringValue(registro, "FLG_SECTR"));
		
		return actividadEconomica;
	}

}