/**
 * Isban Mexico
 *   Clase: CuestionarioDAOImpl.java
 *   Descripcion: Componente para obtener informacion de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.AdministradorClavesDAO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AdministradorClavesDAOImpl extends GenericBdIsbanDA implements AdministradorClavesDAO {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -3878335927617057884L;

	/**
	 * Constructor CuestionarioDAOImpl
	 */
	public AdministradorClavesDAOImpl() {
		super(IDACanal.CANAL_DB_NORKOM);
	}

	@Override
	public int obtenerNuevaClaveClienteCuestionario() throws BusinessException {
		return consultarEntero(SQL_OBTENER_NUEVO_ID_CUESTIONARIO);
	}
	
	@Override
	public int obtenerNuevaClaveCuestionarioRespuesta() throws BusinessException {
		return consultarEntero(SQL_OBTENER_NUEVO_ID_CUESTIONARIO_RESP);
	}
	
	@Override
	public int obtenerNuevaClaveRespuestaPregunta() throws BusinessException {
		return consultarEntero(SQL_OBTENER_NUEVO_ID_CUESTIONARIO_RESP);
	}
	
	@Override
	public int obtenerNuevaClaveFamiliar() throws BusinessException {
		return consultarEntero(SQL_OBTENER_NUEVO_ID_FAMILIAR);
	}
	
	@Override
	public void asignarClaveCuestionarioIC(int claveClienteCuest, int claveCuestIC, ArchitechSessionBean psession) 
	throws BusinessException {
		update(String.format(SQL_ASIGNAR_CUESTIONARIO_IC, claveCuestIC, claveClienteCuest));
	}
}
