/**
 * Isban Mexico
 *   Clase: AdministradorComponenteCentralDAOImpl.java
 *   Descripcion: Componente para obtener cuestionario y relacionarlo a un contrato.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.AdministradorComponenteCentralDAO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AdministradorComponenteCentralDAOImpl extends GenericBdIsbanDA implements AdministradorComponenteCentralDAO {

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 733916456111398542L;

	/**
     * @see Architech#Architech()
     */
    public AdministradorComponenteCentralDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
	
	@Override
	public int obtenerSecuenciaCuestionario(ArchitechSessionBean psession) throws BusinessException {
		return consultarUnico(
			SQL_OBTENER_ID_CUEST, 
			new MapeoConsulta<Integer>(){
				@Override
				public Integer mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					return getIntegerValue(registro, "ID_CUEST");
					
				}
			});
	}
	
	@Override
	public void asignarNumeroContrato(String idCuestionario, String numeroContrato, ArchitechSessionBean psession) throws BusinessException {
		update(String.format(SQL_ASIGNAR_NUMERO_CONTRATO, numeroContrato, idCuestionario));
	}

	@Override
	public List<HashMap<String, Object>> obtenerNivRIndU(String idCuestionario,
			ArchitechSessionBean psession) throws BusinessException {
			return consultarSinMapeo(String.format(SQL_OBTENER_NIV_R_IND_U, idCuestionario,idCuestionario));
		
	}

	

}
