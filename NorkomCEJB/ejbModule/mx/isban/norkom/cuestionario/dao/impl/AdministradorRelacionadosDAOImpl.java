/**
 * Isban Mexico
 *   Clase: AdministradorRelacionadosDAOImpl.java
 *   Descripcion: Componente para insertar nuevos accionistas y consultar informacion de los accionistas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.AdministradorRelacionadosDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AdministradorRelacionadosDAOImpl extends GenericBdIsbanDA implements  AdministradorRelacionadosDAO {

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 733916456111398542L;

	/**
     * @see Architech#Architech()
     */
    public AdministradorRelacionadosDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
	
	@Override
	public List<RelacionadoDTO> agregarRelacionados(List<RelacionadoDTO> relacionadosWS, int claveCuestionario, ArchitechSessionBean psession) 
	throws BusinessException {
		List<RelacionadoDTO> relacionadosErroneos = new ArrayList<RelacionadoDTO>();
		
		for(RelacionadoDTO relacionado : relacionadosWS){
			try {
				String sentenciaInsercion = String.format(
						SQL_AGREGAR_RELACIONADO,
						relacionado.getTipoRelacion(),
						relacionado.getNombre(),
						relacionado.getApellidoPaterno(),
						relacionado.getApellidoMaterno(),
						relacionado.getFechaNacimiento(),
						relacionado.getTipoPersona(),
						relacionado.getCodPaisNacionalidad(),
						relacionado.getCodPaisResidencia(),
						relacionado.getPorcentajeParticipacion(),
						claveCuestionario);
				
				insert(sentenciaInsercion);
			} catch(BusinessException be){
				relacionado.setCodigoOperacion(Mensajes.ERRLBD01.getCodigo());
				relacionado.setDescOperacion(Mensajes.ERRLBD01.getDescripcion());
				
				relacionadosErroneos.add(relacionado);
			}
		}
		
		return relacionadosErroneos;
	}
	
	@Override
	public List<RelacionadoDTO> obtenerRelacionados(String numeroContrato, ArchitechSessionBean psession) 
	throws BusinessException {
		return consultar(
			String.format(SQL_OBTENER_RELACIONADOS_POR_CNTR, numeroContrato), 
			new MapeoConsulta<RelacionadoDTO>(){
				@Override
				public RelacionadoDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					final RelacionadoDTO relacionado = new RelacionadoDTO();
					
					relacionado.setClaveRelacionado(getIntegerValue(registro, "CVE_RELAC_PK"));
					relacionado.setTipoRelacion(getStringValue(registro, "COD_TIPO_RELAC"));
					relacionado.setNombre(getStringValue(registro, "TXT_NOMB_RELAC"));
					relacionado.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_RELAC"));
					relacionado.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_REL"));
					relacionado.setFechaNacimiento(getStringValue(registro, "FEC_NAC"));
					relacionado.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS_REL"));
					relacionado.setCodPaisNacionalidad(getStringValue(registro, "COD_PAIS_NACIO_REL"));
					relacionado.setCodPaisResidencia(getStringValue(registro, "COD_PAIS_RESID_REL"));
					relacionado.setPorcentajeParticipacion(getBigDecimalValue(registro, "POR_PART").doubleValue());
					
					return relacionado;
				}
			});
	}
	
	@Override
	public List<RelacionadoDTO> obtenerRelacionadosPorClaveClienteCuestTipo(int claveClienteCuest, String tipoRelacionado, ArchitechSessionBean psession) 
	throws BusinessException {
		return consultar(
			String.format(SQL_OBTENER_RELACIONADOS_POR_CNTR_TIPO_RELACIONADO, claveClienteCuest, tipoRelacionado), 
			new MapeoConsulta<RelacionadoDTO>(){
				@Override
				public RelacionadoDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					final RelacionadoDTO relacionado = new RelacionadoDTO();
					
					relacionado.setClaveRelacionado(getIntegerValue(registro, "CVE_RELAC_PK"));
					relacionado.setTipoRelacion(getStringValue(registro, "COD_TIPO_RELAC"));
					relacionado.setNombre(getStringValue(registro, "TXT_NOMB_RELAC"));
					relacionado.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_RELAC"));
					relacionado.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_REL"));
					relacionado.setFechaNacimiento(getStringValue(registro, "FEC_NAC"));
					relacionado.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS_REL"));
					relacionado.setCodPaisNacionalidad(String.format(
							"%s - %s", getStringValue(registro, "COD_PAIS_NACIO_REL"), getStringValue(registro, "PAIS")));
					relacionado.setCodPaisResidencia(getStringValue(registro, "COD_PAIS_RESID_REL"));
					relacionado.setPorcentajeParticipacion(getBigDecimalValue(registro, "POR_PART").doubleValue());
					
					return relacionado;
				}
			});
	}
	
	@Override
	public void agregarCuestionarioBasico(CuestionarioDTO cuestionario, ArchitechSessionBean sesion)
	throws BusinessException {
		update(formarSentenciaInsercionCuestionarioBasico(cuestionario));
	}
	/**
	 * Genera sentencia SQL de insercion de cuestionario con la informacion minima requerida
	 * @param cuestionario Contiene la informacion para generar la sentencia SQL
	 * @return String Sentencia SQL para agregar un cuestionario
	 */
	private String formarSentenciaInsercionCuestionarioBasico(CuestionarioDTO cuestionario){
		return String.format(SQL_AGREGAR_CUESTIONARIO_BASICO,
				cuestionario.getClaveClienteCuestionario(),
				cuestionario.getClaveCuestionario(),
				cuestionario.getIdCuestionario(),
				cuestionario.getTipoPersona(),
				cuestionario.getSubtipoPersona(),
				cuestionario.getDivisa()
		);
	}
}
