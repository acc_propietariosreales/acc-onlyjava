/**
 * Isban Mexico
 *   Clase: ClienteAOImpl.java
 *   Descripcion: Componente para insertar nuevos clientes y consultar informacion de los clientes.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.norkom.cuestionario.dao.ClienteDAO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ClienteDAOImpl extends GenericBdIsbanDA implements ClienteDAO {
	
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 3381004753618377197L;
	/**
	 * constructor
	 */
	public ClienteDAOImpl() {
		super(IDACanal.CANAL_DB_NORKOM);
	}

	@Override
	public int obtenerNuevoIdCliente() throws BusinessException {
		return consultarEntero(SQL_OBTENER_NUEVO_ID_CLTE);
	}
	
	@Override
	public int existeCliente(String buc) throws BusinessException {
		return consultarEntero(String.format(SQL_EXISTE_CLIENTE, buc));
	}
	
	@Override
	public ClienteDTO obtenerClientePorBuc(String buc, ArchitechSessionBean psession) throws BusinessException {
		return consultarUnico(
			String.format(SQL_OBTENER_CLIENTE_POR_BUC, buc), 
			new MapeoConsulta<ClienteDTO>(){
				@Override
				public ClienteDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					final ClienteDTO notificacion = new ClienteDTO();
					
					notificacion.setIdCliente(getIntegerValue(registro, "CVE_CLTE_PK"));
					notificacion.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
					notificacion.setIndClienteNuevo(getStringValue(registro, "FLG_INDIC_CLTE_NVO"));
					notificacion.setNombreCliente(getStringValue(registro, "TXT_NOMB_CLTE"));
					notificacion.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_CLTE"));
					notificacion.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_CLTE"));
					notificacion.setFechaNacimiento(getStringValue(registro, "FCH_NAC_CLTE"));
					
					return notificacion;
				}
			});
	}
	
	@Override
	public ClienteDTO obtenerClientePorClaveClteCuest(int claveClienteCuestionario, ArchitechSessionBean psession) throws BusinessException {
		return consultarUnico(
				String.format(SQL_OBTENER_CLIENTE_POR_CLAVE_CLTE_CUEST, claveClienteCuestionario), 
				new MapeoConsulta<ClienteDTO>(){
					@Override
					public ClienteDTO mapeoRegistro(Map<String, Object> registro)
							throws BusinessException {
						final ClienteDTO notificacion = new ClienteDTO();
						
						notificacion.setIdCliente(getIntegerValue(registro, "CVE_CLTE_PK"));
						notificacion.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
						notificacion.setIndClienteNuevo(getStringValue(registro, "FLG_INDIC_CLTE_NVO"));
						notificacion.setNombreCliente(getStringValue(registro, "TXT_NOMB_CLTE"));
						notificacion.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_CLTE"));
						notificacion.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_CLTE"));
						notificacion.setFechaNacimiento(getStringValue(registro, "FCH_NAC_CLTE"));
						
						return notificacion;
					}
				});
	}
	
	@Override
	public void agregarCliente(ClienteDTO cliente, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess, ArchitechSessionBean psession)
	throws BusinessException, ExceptionDataAccess {
		requestDTO.setQuery(
			String.format(SQL_AGREGAR_CLIENTE, 
					cliente.getIdCliente(),
					cliente.getCodigoCliente(),
					cliente.getIndClienteNuevo(),
					cliente.getNombreCliente(),
					cliente.getApellidoPaterno(),
					cliente.getApellidoMaterno(),
					cliente.getFechaNacimiento()));
		
		requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
		ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
				dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
		
		if (response == null) {
			error("No se obtuvo respuesta de la BD");
			throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
		}
	}

	@Override
	public void actualizarCliente(ClienteDTO cliente,
			ArchitechSessionBean psession) throws BusinessException {
		update(String.format(SQL_UPDATE_CLIENTE,cliente.getNombreCliente(),cliente.getApellidoPaterno(),cliente.getApellidoMaterno(),cliente.getFechaNacimiento(),cliente.getIdCliente()));
		
	}
}
