/**
 * Isban Mexico
 *   Clase: ConsultaRespuestasDAOHelper.java
 *   Descripcion: Componente ayuda de ConsultaRespuestasDAO para consulta de respuestas
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * @author mafranco
 *
 */
public class ConsultaRespuestasDAOHelper extends GenericBdIsbanDA{
	
	/**
	 * long serialVersionUID
	 */
	protected static final long serialVersionUID = 7587703087933841203L;
	

	/**
	 * constante para respuestas TXT_NOMB
	 */
	protected static final String TXT_NOMB="TXT_NOMB";
	
	
	/**
	 * constante para respuestas TXT_RESP
	 */
	protected static final String TXT_RESP="TXT_RESP";
	
	/**
	 * constante para respuestas TXT_PREG
	 */
	protected static final String TXT_PREG="TXT_PREG";

	/**
	 * constante para respuestas CVE_RPT
	 */
	protected static final String CVE_RPT="CVE_RPT";
	
	/**
	 * constante para respuestas DSC_OPC
	 */
	protected static final String DSC_OPC="DSC_OPC";
	
	/**
	 * constante para respuestas DSC_REC
	 */
	protected static final String DSC_REC="DSC_REC";
	
	/**
	 * constante para respuestas TXT_SECCN
	 */
	protected static final String TXT_SECCN="TXT_SECCN";
	
	/**
	 * constante para respuestas TXT_TITUL
	 */
	protected static final String TXT_TITUL="TXT_TITUL";
	
	/**
	 * Valor para las opciones de Radio y Select
	 */
	protected static final String TXT_VAL_OPC="VAL_OPC";
	
	/**
	 * Abreviatura de las preguntas
	 */
	protected static final String TXT_ABRV="TXT_ABRV";
	/**constante para la obtnecion del campo CVE_PREG_PK***/
	protected static final String CVE_PREG_PK="CVE_PREG_PK";
	/**
	 * CONSTRUCTOR
	 */
	public ConsultaRespuestasDAOHelper() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	
	
	/***
	 * Metodo para obtener las preguntas de tipo Select unica respuesta
	 * @param contrato numero de contrato
	 * @param query query a ejecutar
	 * @return preguntas List<PreguntaDTO>
	 * @throws BusinessException con mensaje de error
	 */
	protected List<PreguntaDTO> obtenerPreguntasLibres(
			String contrato,String query) throws BusinessException {	
		 List<PreguntaDTO> preguntas = consultar(String.format(query,contrato),new MapeoConsulta<PreguntaDTO>() {
				@Override
				public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					PreguntaDTO dto = new PreguntaDTO();					
					dto.setPregunta(getStringValue(registro, TXT_PREG));
					dto.setRespuesta(getStringValue(registro, DSC_OPC));
					dto.setClaveMapeo(getStringValue(registro, CVE_RPT));
					dto.setSeccion(getStringValue(registro, TXT_SECCN));
					dto.setTitulo(getStringValue(registro, TXT_TITUL));
					dto.setAbreviatura(getStringValue(registro, TXT_ABRV));
					dto.setIdPregunta(getIntegerValue(registro, CVE_PREG_PK));
					return dto;
					}
				});
		return preguntas;
	}
		/***
		 * Metodo para obtener las preguntas de tipo Select unica respuesta
		 * @param contrato numero de contrato
		 * @param query a ejecutar
		 * @return preguntas List<PreguntaDTO>
		 * @throws BusinessException con mensaje de error
		 */
		protected List<PreguntaDTO> obtenerPreguntasSelectUnicaRadio(
				String contrato,String query) throws BusinessException {	
				List<PreguntaDTO> preguntas = consultar(String.format(query,contrato),new MapeoConsulta<PreguntaDTO>() {
					@Override
					public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						PreguntaDTO dto = new PreguntaDTO();					
						dto.setPregunta(getStringValue(registro, TXT_PREG));
						dto.setRespuesta(getStringValue(registro, DSC_OPC));
						dto.setClaveMapeo(getStringValue(registro, CVE_RPT));
						dto.setSeccion(getStringValue(registro, TXT_SECCN));
						dto.setTitulo(getStringValue(registro, TXT_TITUL));
						dto.setOpcionSeleccionada(getIntegerValue(registro, TXT_VAL_OPC));
						dto.setAbreviatura(getStringValue(registro, TXT_ABRV));
						dto.setIdPregunta(getIntegerValue(registro, CVE_PREG_PK));
						return dto;
						}
					});
			return preguntas;
		}
	

	
		/***
		 * Metodo para obtener las preguntas Catalogo de Paises con su respuesta
		 * @param contrato numero de contrato
		 * @param query a ejecutar
		 * @return preguntas List<PreguntaDTO>
		 * @throws BusinessException con mensaje de error
		 */
		protected List<PreguntaDTO> obtenerPreguntasPais(
				String contrato,String query) throws BusinessException {	
				List<PreguntaDTO> preguntas = consultar(String.format(query,contrato),new MapeoConsulta<PreguntaDTO>() {
					@Override
					public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						PreguntaDTO dto = new PreguntaDTO();					
						dto.setRespuesta(getStringValue(registro, TXT_NOMB));
						dto.setPregunta(getStringValue(registro, TXT_PREG));
						dto.setClaveMapeo(getStringValue(registro, CVE_RPT));
						dto.setSeccion(getStringValue(registro, TXT_SECCN));
						dto.setTitulo(getStringValue(registro, TXT_TITUL));
						dto.setAbreviatura(getStringValue(registro, TXT_ABRV));
						dto.setIdPregunta(getIntegerValue(registro, CVE_PREG_PK));
						return dto;
						}
					});
			return preguntas;
		}


		
		/***
		 * Metodo para obtener las preguntas Catalogo de Recursos Origen con su respuesta
		 * @param contrato numero de contrato
		 * @param query a ejecutar
		 * @return preguntas List<PreguntaDTO>
		 * @throws BusinessException Exception manejada
		 */
		protected List<PreguntaDTO> obtenerPreguntasOrigenDestino(
				String contrato,String query) throws BusinessException {	
				List<PreguntaDTO> preguntas = consultar(String.format(query,contrato),new MapeoConsulta<PreguntaDTO>() {
					@Override
					public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						PreguntaDTO dto = new PreguntaDTO();					
						dto.setRespuesta(getStringValue(registro, DSC_REC));
						dto.setPregunta(getStringValue(registro, TXT_PREG));
						dto.setClaveMapeo(getStringValue(registro, CVE_RPT));
						dto.setSeccion(getStringValue(registro, TXT_SECCN));
						dto.setTitulo(getStringValue(registro, TXT_TITUL));
						dto.setAbreviatura(getStringValue(registro, TXT_ABRV));
						dto.setIdPregunta(getIntegerValue(registro, CVE_PREG_PK));
						return dto;
						}
					});
			return preguntas;
		}

		

	
}
