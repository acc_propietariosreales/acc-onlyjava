/**
 * Isban Mexico
 *   Clase: ConsultarRespuestasDAOImpl.java
 *   Descripcion: Componente para obtener las respuestas de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dao.ConsultaRespuestasDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.ObtenerAccionistasDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.RespuestaListaDTO;
import mx.isban.norkom.isbanda.MapeoConsulta;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

import org.apache.log4j.Logger;
/**
 * @author mafranco
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ConsultarRespuestasDAOImpl extends ConsultaRespuestasDAOHelper implements ConsultaRespuestasDAO,MapeoConsulta<RespuestaListaDTO>{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -9202915780014964779L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = Logger.getLogger(ConsultarRespuestasDAOImpl.class);
	
	/**
	 * Metodo para obtener las respuestas IP
	 * @param request RequestObtenerCuestionario con datos para la busqueda
	 * @param psession ArchitechSessionBean bean de session
	 * @throws BusinessException con mensaje de error
	 * @return preguntas List<PreguntaDTO>
	 */
	@Override
	public  List<PreguntaDTO> obtenerRespuestasIP(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException {
		{
			LOG.info("Entrando a obtenerCuestionarioIP ------>");
			 List<PreguntaDTO> list= new ArrayList<PreguntaDTO>();
			 
			 list.addAll(obtenerPreguntasSelectUnicaRadio(request.getIdCuestionario(),QRTY_RESPUESTAS_SELECT_UNICA_IP));
			 list.addAll(obtenerPreguntasSelectUnicaRadio(request.getIdCuestionario(),QRTY_RESPUESTAS_RADIO_IP));
			 list.addAll(obtenerPreguntasPais(request.getIdCuestionario(),QRTY_RESPUESTAS_PAIS_IP));
			 list.addAll(obtenerPreguntasOrigenDestino(request.getIdCuestionario(),QRTY_RESPUESTAS_ORIGEN_IP));
			 list.addAll(obtenerPreguntasOrigenDestino(request.getIdCuestionario(),QRTY_RESPUESTAS_DESTINO_IP));
			 list.addAll(obtenerPreguntasLibres(request.getIdCuestionario(),QRTY_RESPUESTAS_LIBRES_IP));
			 
			 Collections.sort(list);

			 //devolvemos la lista
			 return list;
			}
	}
	
	
	/***
	 * Metodo para obtener preguntas y respuestas de cuestionario IC
	 * @param request RequestObtenerCuestionario  con filtros para ls respuestas
	 * @param psession ArchitechSessionBean objeto de sesion
	 * @return preguntas List<PreguntaDTO> con las preguntas segun los filtros
	 * @throws BusinessException con mensaje de error
	 */
	@Override
	public  List<PreguntaDTO> obtenerRespuestasIC(RequestObtenerCuestionario request,ArchitechSessionBean psession)throws BusinessException {
		LOG.info("Entrando a obtenerCuestionarioIC ------>");
		
		List<PreguntaDTO> preguntaDTOs= new ArrayList<PreguntaDTO>();
		preguntaDTOs.addAll(obtenerPreguntasSelectUnicaRadio(request.getIdCuestionario(),QRTY_RESPUESTAS_SELECT_UNICA_IC));
		preguntaDTOs.addAll(obtenerPreguntasSelectUnicaRadio(request.getIdCuestionario(),QRTY_RESPUESTAS_RADIO_IC));
		preguntaDTOs.addAll(obtenerPreguntasLibres(request.getIdCuestionario(),QRTY_RESPUESTAS_LIBRES_IC));
		//ordenamos en base a el id de pregunta
		Collections.sort(preguntaDTOs);
		
		return preguntaDTOs;
	}


		/** Obtiene el referencias personales de IC
		 * @param request RequestObtenerCuestionario  con filtros para las repuestas
		 * @param psession ArchitechSessionBean bean de session
		 * @return lista List<PreguntaDTO> lista de preguntas
		 * @throws BusinessException con mensaje de error
		 */
		@Override
		public List<PreguntaDTO> obtenerFamiliares(RequestObtenerCuestionario request,ArchitechSessionBean psession) throws BusinessException {
			List<PreguntaDTO> referencias = new ArrayList<PreguntaDTO>();
			
			//
			List<RespuestaListaDTO> datos=	
				consultar(String.format(QRY_PREGUNTAS_INFORMACION_FAMILIARES_IC,request.getIdCuestionario()),
					this);
			
			for(RespuestaListaDTO a:datos){
				PreguntaDTO nombre = new PreguntaDTO();
				nombre.setPregunta("NOMBRE COMPLETO");
				nombre.setRespuesta(a.getDatos().get(0));
				nombre.setClaveMapeo("strNomFamiliar");
				referencias.add(nombre);
				PreguntaDTO domicilio = new PreguntaDTO();
				domicilio.setPregunta("DOMICILIO");
				domicilio.setRespuesta(a.getDatos().get(1));
				domicilio.setClaveMapeo("strDomFamiliar");
				referencias.add(domicilio);
				PreguntaDTO fechaN = new PreguntaDTO();
				fechaN.setPregunta("FECHA DE NACIMIENTO");
				fechaN.setRespuesta(a.getDatos().get(2));
				fechaN.setClaveMapeo("strFecNacFamiliar");
				referencias.add(fechaN);
				PreguntaDTO parentesco = new PreguntaDTO();
				parentesco.setPregunta("PARENTESCO");
				parentesco.setRespuesta(a.getDatos().get(3));
				parentesco.setClaveMapeo("strParentesco");
				referencias.add(parentesco);
			}
			
			return referencias;
		}



		


		/**Obtiene el encabezado del cuestionario IP
		 * @param request RequestObtenerCuestionario con filtros para el encabezado ip
		 * @param psession ArchitechSessionBean  objeto de sesion
		 * @return Cuestionario CuestionarioDTO con encabezado IP
		 * @throws BusinessException con mensaje de error
	 	*/
		@Override
		public CuestionarioDTO obtenerEncabezadoIP(RequestObtenerCuestionario request,ArchitechSessionBean psession)
				throws BusinessException {
			return consultarUnico(
					String.format(QRY_ENCABEZADO_IP, request.getIdCuestionario()),
					new MapeoConsulta<CuestionarioDTO>(){
						@Override
						public CuestionarioDTO mapeoRegistro(Map<String, Object> registro)
						throws BusinessException {
							CuestionarioDTO cuestionario = new CuestionarioDTO();
							ActividadEconomicaDTO actividad = new ActividadEconomicaDTO();
							ClienteDTO cliente = new ClienteDTO();
							String fechaCues = getStringValue(registro, "FCH_CREAC");
							cuestionario.setAnio(new StringBuilder()
									.append(fechaCues.substring(0,4)).toString());
							cuestionario.setMes(new StringBuilder()
									.append(fechaCues.substring(5,7)).toString());
							cuestionario.setDia(new StringBuilder()
									.append(fechaCues.substring(8,10)).toString());
							cuestionario.setNombreSucursal(getStringValue(registro, "TXT_NOMB_SUCU"));
							cuestionario.setCodigoSucursal(getStringValue(registro, "COD_SUCU"));
							cuestionario.setNombrePlaza(getStringValue(registro, "TXT_NOMB_PLAZA"));
							cuestionario.setNombreZona(getStringValue(registro, "TXT_NOMB_ZONA"));
							cuestionario.setNombreRegion(getStringValue(registro, "TXT_NOMB_REGN"));
							cliente.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
							cliente.setNombreCliente(getStringValue(registro, "TXT_NOMB_CLTE"));
							cliente.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_CLTE"));
							cliente.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_CLTE"));
							cuestionario.setCliente(cliente);
							cuestionario.setCuentaContrato(getStringValue(registro, "NUM_CNTR"));
							cuestionario.setDescSegmento(getStringValue(registro, "DSC_SEGMT"));
							cuestionario.setDescProducto(getStringValue(registro, "DSC_PROD"));
							cuestionario.setNombrePais(getStringValue(registro, "PAIS"));
							cuestionario.setNombreEntidad(getStringValue(registro, "ESTADO"));
							cuestionario.setNombreMunicipio(getStringValue(registro, "MUNICIPIO"));
							cuestionario.setCodigoNacionalidad(getStringValue(registro, "COD_NACIO"));
							cuestionario.setNombreNacionalidad(getStringValue(registro,"NACIONALIDAD"));
							actividad.setCodigo(getStringValue(registro, "CVE_ACTIV_GEN"));
							cuestionario.setActividadGenerica(actividad);
							actividad.setDescripcion(getStringValue(registro, "DSC_ACT_ECON"));
							cuestionario.setActividadEspecifica(actividad);
							cuestionario.setCodigoTipoFormulario(getStringValue(registro, "VAL_TIPO_CUEST"));
							cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
							cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
							cuestionario.setDivisa(getStringValue(registro, "COD_DIV"));
							return cuestionario;
						}
						});
		}

		/**Obtiene el encabezado del cuestionario IC
		 * @param request RequestObtenerCuestionario con filtros para el encabezado
		 * @param psession ArchitechSessionBean   objeto de sesion
		 * @return Cuestionario CuestionarioDTO con los datos del encabezado
		 * @throws BusinessException con mensaje de error
	 	*/
		@Override
		public CuestionarioDTO obtenerEncabezadoIC(RequestObtenerCuestionario request, ArchitechSessionBean psession)
				throws BusinessException {
			return consultarUnico(
					String.format(QRY_ENCABEZADO_IC1, request.getIdCuestionario()), 
					new MapeoConsulta<CuestionarioDTO>(){
						@Override
						public CuestionarioDTO mapeoRegistro(Map<String, Object> registro)
						throws BusinessException {
							CuestionarioDTO cuestionario = new CuestionarioDTO();
							ClienteDTO cliente = new ClienteDTO();
							String fechaCues = getStringValue(registro, "FCH_CREAC");
							
							cuestionario.setAnio(new StringBuilder()
										.append(fechaCues.substring(0, 5)).toString());
							cuestionario.setMes(new StringBuilder()
										.append(fechaCues.substring(5, 7)).toString());
							cuestionario.setDia(new StringBuilder()
										.append(fechaCues.substring(8, 10)).toString());
							
							cuestionario.setNombreSucursal(getStringValue(registro, "TXT_NOMB_SUCU"));
							cuestionario.setCodigoSucursal(getStringValue(registro, "COD_SUCU"));
							cuestionario.setNombrePlaza(getStringValue(registro, "TXT_NOMB_PLAZA"));
							cuestionario.setNombreRegion(getStringValue(registro, "TXT_NOMB_ZONA"));
							cliente.setNombreCliente(getStringValue(registro,"TXT_NOMB_CLTE"));
							cliente.setApellidoPaterno(getStringValue(registro,"TXT_APE_PAT_CLTE"));
							cliente.setApellidoMaterno(getStringValue(registro,"TXT_APE_MAT_CLTE"));
							cliente.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
							cuestionario.setCliente(cliente);
							cuestionario.setCuentaContrato(getStringValue(registro, "NUM_CNTR"));
							return cuestionario;
						}
					});

		}
		
		/** Obtiene el accionistas de IC
		 * @param request RequestObtenerCuestionario con filtros para las repuestas
		 * @param psession ArchitechSessionBean  objeto de sesion
		 * @return lista List<PreguntaDTO> con las preguntas obtenida
		 * @throws BusinessException  con mensaje de error
		 */
		public List<RelacionadoDTO> obtenerAccionistas(RequestObtenerCuestionario request,ArchitechSessionBean psession) throws BusinessException {
			//List<ArrayList<String>> infoAccionistas = 
				
				
			List<RespuestaListaDTO>	datos=consultar(String.format(QRY_INFORMACION_ACCIONISTAS_IC,request.getIdCuestionario()),new ObtenerAccionistasDTO());
			
			List<RelacionadoDTO> accionistas=new ArrayList<RelacionadoDTO>();
			for(RespuestaListaDTO a:datos){
				RelacionadoDTO accionista = new RelacionadoDTO();
				accionista.setNombre(a.getDatos().get(0));
				accionista.setFechaNacimiento(a.getDatos().get(1));
				accionista.setCodPaisNacionalidad(a.getDatos().get(2));
				if(a.getDatos().get(3)!=null && "F".equalsIgnoreCase(a.getDatos().get(3))){
					accionista.setTipoPersona("FISICA");
				}else if(a.getDatos().get(3)!=null && "J".equalsIgnoreCase(a.getDatos().get(3))){
					accionista.setTipoPersona("MORAL");
				}else if(a.getDatos().get(3)!=null && "E".equalsIgnoreCase(a.getDatos().get(3))){
					accionista.setTipoPersona("FISICA CON ACTIVIDAD EMPRESARIAL");
				}
				accionista.setTipoRelacion(a.getDatos().get(5));
				try{
					accionista.setPorcentajeParticipacion( Double.valueOf(a.getDatos().get(4)));
				}catch( NumberFormatException nfe){
					LOG.error(nfe);
					accionista.setPorcentajeParticipacion(0);
				}
				accionistas.add(accionista);
			}
			return accionistas;
		}
		@Override
		public RespuestaListaDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
			RespuestaListaDTO dto= new RespuestaListaDTO();
			dto.setDatos(new ArrayList<String>());
			dto.getDatos().add(getStringValue(registro, "TXT_NOMB_FAM"));
			dto.getDatos().add(getStringValue(registro, "TXT_DOMIC_FAM"));
			dto.getDatos().add(getStringValue(registro, "TXT_FCH_NAC"));
			dto.getDatos().add(getStringValue(registro, "TXT_PAREN"));
			return dto;
		}
}
