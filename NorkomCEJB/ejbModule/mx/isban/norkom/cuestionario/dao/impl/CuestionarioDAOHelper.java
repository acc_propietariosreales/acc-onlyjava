/**
 * Isban Mexico
 *   Clase: CuestionarioDAOHelper.java
 *   Descripcion: Auxiliar para las operaciones de Cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;

import org.apache.log4j.Logger;

/**
 * Auxiliar para las operaciones de Cuestionarios
 * @author jugarcia
 *
 */
public class CuestionarioDAOHelper extends GenericBdIsbanDA{
	/**
	 * CuestionarioDAOHelper.java  de tipo long
	 */
	private static final long serialVersionUID = 4275165966041664437L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(CuestionarioDAOHelper.class);
	
	/**
	 * Sentencia SQL para actualizar un cuestionario
	 */
	private static final String SQL_ACTUALIZAR_CUESTIONARIO = new StringBuilder()
		.append("UPDATE NKM_MX_MAE_CLTE_CUEST_DRO SET ")
		.append("CVE_CUEST_IP_FK = %s, CVE_CLTE_FK = %s, COD_TIPO_PERS = '%s', COD_SUB_TIPO_PERS = '%s', COD_DIV_PROD = '%s', COD_SUCU = '%s', TXT_NOMB_SUCU = '%s', ")
		.append("COD_PLAZA = '%s', TXT_NOMB_PLAZA = '%s', COD_REGN = '%s', TXT_NOMB_REGN = '%s', COD_ENT = '%s', COD_SEGMT = '%s', DSC_SEGMT = '%s', ")
		.append("COD_PROD = '%s', DSC_PROD = '%s', COD_SUB_PROD = '%s', DSC_SUB_PROD = '%s', COD_PAIS_RESID = '%s', COD_MUN_RESID = '%s', COD_NACIO = '%s', ")
		.append("COD_BRNCH = '%s', COD_CENT_COSTO = '%s', VAL_TIPO_FORM = '%s', CVE_ACTIV_GEN = %s, CVE_ACTIV_ESP = %s, NUM_VERSN_CUEST = '%s'")
		.append(",COD_ZONA='%s',TXT_NOMB_ZONA='%s', TXT_NMBR_FUNC='%s' ")
		.append("WHERE ID_CUEST = '%s'")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_AGREGAR_CUESTIONARIO
	 */
	private final static String SQL_AGREGAR_CUESTIONARIO = new StringBuilder()
		.append("INSERT INTO NKM_MX_MAE_CLTE_CUEST_DRO(CVE_CLTE_CUEST_PK, CVE_CUEST_IP_FK, CVE_CLTE_FK, ID_CUEST, NUM_CNTR, COD_TIPO_PERS, COD_SUB_TIPO_PERS, ")
		.append("COD_DIV_PROD, COD_SUCU, TXT_NOMB_SUCU, COD_PLAZA, TXT_NOMB_PLAZA, COD_REGN, TXT_NOMB_REGN, COD_ENT, COD_SEGMT, DSC_SEGMT, COD_PROD, ")
		.append("DSC_PROD, COD_SUB_PROD, DSC_SUB_PROD, COD_PAIS_RESID, COD_MUN_RESID, COD_NACIO, COD_BRNCH, COD_CENT_COSTO, VAL_TIPO_FORM, ")
		.append("CVE_ACTIV_GEN, CVE_ACTIV_ESP, NUM_VERSN_CUEST, FCH_CREAC,COD_ZONA,TXT_NOMB_ZONA, TXT_NMBR_FUNC) ")
		.append("VALUES(%s, %s, %s, '%s', null, '%s', '%s', ")
		.append("'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',")
		.append("'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', ")
		.append("%s, %s, '%s', sysdate,'%s','%s','%s')")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_AGREGAR_CUESTIONARIO
	 */
	private final static String SQL_AGREGAR_CUESTIONARIO_BASICO = new StringBuilder()
		.append("INSERT INTO NKM_MX_MAE_CLTE_CUEST_DRO(CVE_CLTE_CUEST_PK, CVE_CUEST_IP_FK, ID_CUEST, COD_TIPO_PERS, COD_SUB_TIPO_PERS, COD_DIV_PROD, FCH_CREAC) ")
		.append("VALUES(%s, %s, '%s', '%s', '%s', '%s', sysdate)")
		.toString();
	
	/**constructor
	 * @param canal IDACanal**/
	protected CuestionarioDAOHelper(IDACanal canal) {
		super(canal);
	}
	
	/**
	 * Realiza la insercion del Cuestionario
	 * @param cuestionario de datos del cuestionario
	 * @param requestDTO de datos para la insercion
	 * @param dataAccess instancia de IDA para la transaccionalidad
	 * @throws BusinessException con mensje de error
	 * @throws ExceptionDataAccess con mensje de error
	 */
	public void ejecutarInsercionCuestionarioBasico(
			CuestionarioDTO cuestionario, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess)
	throws BusinessException, ExceptionDataAccess {
		requestDTO.setQuery(formarSentenciaInsercionCuestionarioBasico(cuestionario));
		
		requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
		ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
				dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
		
		if (response == null) {
			LOG.error("No se obtuvo respuesta de la BD");
			throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
		}
	}
	
	/**
	 * Genera sentencia SQL de insercion de cuestionario con la informacion minima requerida
	 * @param cuestionario Contiene la informacion para generar la sentencia SQL
	 * @return String Sentencia SQL para agregar un cuestionario
	 */
	public String formarSentenciaInsercionCuestionarioBasico(CuestionarioDTO cuestionario){
		return String.format(SQL_AGREGAR_CUESTIONARIO_BASICO,
				cuestionario.getClaveClienteCuestionario(),
				cuestionario.getClaveCuestionario(),
				cuestionario.getIdCuestionario(),
				cuestionario.getTipoPersona(),
				cuestionario.getSubtipoPersona(),
				cuestionario.getDivisa()
		);
	}
	
	/**
	 * Realiza la insercion del Cuestionario
	 * @param cuestionario bean con datos del cuestionario
	 * @param requestDTO con datos para la insercion
	 * @param dataAccess instancia de IDA para la transaccionalidad
	 * @throws BusinessException con mensaje de error
	 * @throws ExceptionDataAccess con mensaje de error
	 */
	public void ejecutarInsercionCuestionario(
			CuestionarioDTO cuestionario, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess)
					throws BusinessException, ExceptionDataAccess {
		requestDTO.setQuery(formarSentenciaInsercionCuestionario(cuestionario));
		
		requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
		ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
				dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
		
		if (response == null) {
			LOG.error("No se obtuvo respuesta de la BD");
			throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
		}
	}
	
	/**
	 * Genera sentencia SQL de insercion de cuestionario con toda la informacion
	 * @param cuestionario Contiene la informacion para generar la sentencia SQL
	 * @return String Sentencia SQL para agregar un cuestionario
	 */
	public String formarSentenciaInsercionCuestionario(CuestionarioDTO cuestionario){
		return String.format(SQL_AGREGAR_CUESTIONARIO,
				cuestionario.getClaveClienteCuestionario(),
				cuestionario.getClaveCuestionario(),
				cuestionario.getIdClienteAsignado(),
				cuestionario.getIdCuestionario(),
				cuestionario.getTipoPersona(),
				cuestionario.getSubtipoPersona(),
				cuestionario.getDivisa(),
				cuestionario.getCodigoSucursal(),
				cuestionario.getNombreSucursal(),
				cuestionario.getCodigoPlaza(),
				cuestionario.getNombrePlaza(),
				cuestionario.getCodigoRegion(),
				cuestionario.getNombreRegion(),
				cuestionario.getCodigoEntidad(),
				cuestionario.getCodigoSegmento(),
				cuestionario.getDescSegmento(),
				cuestionario.getCodigoProducto(),
				cuestionario.getDescProducto(),
				cuestionario.getCodigoSubproducto(),
				cuestionario.getDescSubproducto(),
				cuestionario.getCodigoPais(),
				cuestionario.getCodigoMunicipio(),
				cuestionario.getCodigoNacionalidad(),
				cuestionario.getCodigoBranch(),
				cuestionario.getCodigoCentroCostos(),
				cuestionario.getCodigoTipoFormulario(),
				cuestionario.getIdActividadGenerica(),
				cuestionario.getIdActividadEspecifica(),
				cuestionario.getVersionCuestionario(),cuestionario.getCodigoZona(),cuestionario.getNombreZona(),
				((cuestionario.getNombreFuncionario()== null)?cuestionario.getNombreFuncionario(): cuestionario.getNombreFuncionario().replace("'", "''"))
				);
	}
	
	/**
	 * Realiza la actualizacion de la informacion de un cuestionario
	 * @param cuestionario datos del cuestionario
	 * @param requestDTO Contiene la informacion con la cual se va a realizar la peticion de actualizacion
	 * @param dataAccess Contiene la informacion de la conexion
	 * @throws BusinessException En caso de error con la informacion
	 * @throws ExceptionDataAccess En caso de error al ejecutar la operacion
	 */
	public void ejecutarActualizacionCuestionario(
			CuestionarioDTO cuestionario, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess)
	throws BusinessException, ExceptionDataAccess {
		requestDTO.setQuery(formarSentenciaActualizacionCuestionario(cuestionario));
		
		requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_UPDATE);
		ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
				dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
		
		if (response == null) {
			LOG.error("No se obtuvo respuesta de la BD");
			throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
		}
	}
	
	/**
	 * Forma la sentencia SQL de la actualizacion de un cuestionario
	 * @param cuestionario Contiene la informacion con la cual se va a formar la sentencia
	 * @return Sentencia SQL de actualizacion
	 */
	public String formarSentenciaActualizacionCuestionario(CuestionarioDTO cuestionario){
		return String.format(SQL_ACTUALIZAR_CUESTIONARIO,
				cuestionario.getClaveCuestionario(),
				cuestionario.getIdClienteAsignado(),
				cuestionario.getTipoPersona(),
				cuestionario.getSubtipoPersona(),
				cuestionario.getDivisa(),
				cuestionario.getCodigoSucursal(),
				cuestionario.getNombreSucursal(),
				cuestionario.getCodigoPlaza(),
				cuestionario.getNombrePlaza(),
				cuestionario.getCodigoRegion(),
				cuestionario.getNombreRegion(),
				cuestionario.getCodigoEntidad(),
				cuestionario.getCodigoSegmento(),
				cuestionario.getDescSegmento(),
				cuestionario.getCodigoProducto(),
				cuestionario.getDescProducto(),
				cuestionario.getCodigoSubproducto(),
				cuestionario.getDescSubproducto(),
				cuestionario.getCodigoPais(),
				cuestionario.getCodigoMunicipio(),
				cuestionario.getCodigoNacionalidad(),
				cuestionario.getCodigoBranch(),
				cuestionario.getCodigoCentroCostos(),
				cuestionario.getCodigoTipoFormulario(),
				cuestionario.getIdActividadGenerica(),
				cuestionario.getIdActividadEspecifica(),
				cuestionario.getVersionCuestionario(),
				cuestionario.getCodigoZona(),cuestionario.getNombreZona(),
				cuestionario.getNombreFuncionario(),
				cuestionario.getIdCuestionario()
		);
	}

}
