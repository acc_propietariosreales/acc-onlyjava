/**
 * Isban Mexico
 *   Clase: CuestionarioDAOImpl.java
 *   Descripcion: Componente para obtener informacion de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dao.ClienteDAO;
import mx.isban.norkom.cuestionario.dao.CuestionarioDAO;
import mx.isban.norkom.cuestionario.dao.mapeo.CuestionarioMapper;
import mx.isban.norkom.cuestionario.dao.mapeo.CuestionarioWSMapper;
import mx.isban.norkom.cuestionario.dao.mapeo.TipoCuestionarioMapper;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.MapeoConsulta;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 * Objetivo: Realizar las operaciones principales en Base de Datos relacionadas con la tabla de Cuestionarios
 * Justificacion: Es necesario agrupar las operaciones principales de la tabla NKM_MX_MAE_CLTE_CUEST_DRO en un solo componente. Estas operaciones incluyen la obtencion de Tipos de Cuestionario, almacenar Cuestionarios y obtener la informacion general de los Cuestionarios  
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CuestionarioDAOImpl extends CuestionarioDAOHelper implements CuestionarioDAO {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 3381004753618377197L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(CuestionarioDAOImpl.class);
	/**Constante para AND****/
	private static final String AND=" AND ";
	/**
	 * Variable utilizada para declarar clienteDao
	 */
	@EJB
	private transient ClienteDAO clienteDao;
	/**
	 * Constructor CuestionarioDAOImpl
	 */
	public CuestionarioDAOImpl() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	
	@Override
	public CuestionarioDTO obtenerTipoCuestionario(CuestionarioDTO peticion, String tipoCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		String idAplicacion = "DF";
		if(StringUtils.isNotBlank(peticion.getIdCuestionario()) && "LH".equals(peticion.getIdCuestionario().substring(0, 2))) {
			idAplicacion = "LH";
		}
		return consultarUnico(
				String.format(SQL_OBTENER_TIPO_CUESTIONARIO, peticion.getTipoPersona(), peticion.getSubtipoPersona(), 
						peticion.getDivisa(), tipoCuestionario, idAplicacion), 
				new TipoCuestionarioMapper());
	}
	
	@Override
	public CuestionarioDTO obtenerTipoCuestionarioPorClave(int claveCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		return consultarUnico(
				String.format(SQL_OBTENER_TIPO_CUESTIONARIO_POR_CLAVE, claveCuestionario), 
				new TipoCuestionarioMapper());
	}
	
	@Override
	public void agregarCuestionarioClienteNuevo(CuestionarioDTO cuestionario, boolean insertarCuestionario, ArchitechSessionBean sesion)
	throws BusinessException {
		ClienteDTO cliente = cuestionario.getCliente();
		RequestMessageDataBaseDTO requestDTO = new RequestMessageDataBaseDTO();
		DataAccessDataBase dataAccess = obtenerDataAccess(requestDTO);

		try {
			dataAccess.beginTransaction(IDACanal.CANAL_DB_NORKOM.getNombre());
			
			clienteDao.agregarCliente(cliente, requestDTO, dataAccess, sesion);
			if(insertarCuestionario) {
				ejecutarInsercionCuestionario(cuestionario, requestDTO, dataAccess);
			} else {
				ejecutarActualizacionCuestionario(cuestionario, requestDTO, dataAccess);
			}
			
			dataAccess.commitTransaction();
		} catch (ExceptionDataAccess e) {
			error(String.format(
					"Ocurrio un error durante la insercion del cuestionario ::: [%s][%s]", 
					e.getMessage(), e.getCause()));
			error(e.toString());
			dataAccess.rollbackTransaction();
			throw new mx.isban.norkom.cuestionario.exception.BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo(),Mensajes.ERROR_COMUNICACION.getDescripcion(),e);
			
		}
	}
	
	@Override
	public void agregarCuestionarioClienteExistente(CuestionarioDTO cuestionario, boolean insertarCuestionario, ArchitechSessionBean sesion)
	throws BusinessException {
		LOG.debug(":::NPE::: agregarCuestionarioClienteExistente:" + cuestionario);
		if(insertarCuestionario) {
			update(formarSentenciaInsercionCuestionario(cuestionario));
		} else {
			update(formarSentenciaActualizacionCuestionario(cuestionario));
		}
		clienteDao.actualizarCliente(cuestionario.getCliente(), sesion);
	}
	
	@Override
	public List<CuestionarioWSDTO> obtenerDatosGeneralesCuestionario(String idCuestionario,
			String buc, String numeroContrato, Date fechaInicio, Date fechaFin, ArchitechSessionBean psession) 
	throws BusinessException {
		StringBuilder sentenciaSQL = new StringBuilder();
		StringBuilder sentenciaSQLWhere = new StringBuilder();
		sentenciaSQL.append(SQL_OBTENER_DATOS_GRALES_CUEST);
		sentenciaSQL.append(" WHERE (cc.NUM_CNTR is not null OR COD_IND_UPLD IN ('CCC-AUT','CCC-NO','BLO-AUT','BLO-NO', 'KYC-BLO', 'KYC-CCC'))");
		if(buc != null && !buc.isEmpty()) {
			int bucNumerica = 0;
			try {
				bucNumerica = Integer.parseInt(buc);
			} catch(NumberFormatException nfe){
				error(nfe.getMessage());
				showException(nfe);
			}
			
			sentenciaSQLWhere.append(AND);
			sentenciaSQLWhere.append(String.format(SQL_WHERE_BUC, buc, String.format("%010d", bucNumerica)));
		}
		
		if(numeroContrato != null && !numeroContrato.isEmpty()){
			sentenciaSQLWhere.append(AND).append(" (");
			sentenciaSQLWhere.append(String.format(SQL_WHERE_NUM_CNTR, numeroContrato))
			.append(" or ")
			.append(String.format(SQL_WHERE_NUM_CNTR, "0"+numeroContrato))
			.append(")");
		}
		
		if(fechaInicio != null){
			sentenciaSQLWhere.append(AND);
			sentenciaSQLWhere.append(String.format(SQL_WHERE_FCH_MAYOR_IGUAL, FORMATO_FECHA_YYYYMMDD.format(fechaInicio)));
		}
		
		if(fechaFin != null){
			sentenciaSQLWhere.append(AND);
			sentenciaSQLWhere.append(String.format(SQL_WHERE_FCH_MENOR_IGUAL, FORMATO_FECHA_YYYYMMDD.format(fechaFin)));
		}
		
		if(idCuestionario != null){
			sentenciaSQLWhere.append(AND);
			sentenciaSQLWhere.append(String.format(SQL_WHERE_ID_CUEST, idCuestionario));
		}
		
		if(sentenciaSQLWhere.length()>0){
			sentenciaSQL.append(sentenciaSQLWhere.toString());
		}
		
		return consultar(
			sentenciaSQL.toString(), 
			new MapeoConsulta<CuestionarioWSDTO>(){
				@Override
				public CuestionarioWSDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					final CuestionarioWSDTO cuestionario = new CuestionarioWSDTO();
					final ClienteDTO cliente = new ClienteDTO();
					cuestionario.setCliente(cliente);
					
					ActividadEconomicaDTO actividadGenerica = new ActividadEconomicaDTO();
					cuestionario.setActividadGenerica(actividadGenerica);
					
					ActividadEconomicaDTO actividadEspecifica = new ActividadEconomicaDTO();
					cuestionario.setActividadEspecifica(actividadEspecifica);
					
					cliente.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
					cliente.setNombreCliente(getStringValue(registro, "NOMBRE_CLTE"));
					cuestionario.setIdCuestionario(getStringValue(registro, "ID_CUEST"));
					cuestionario.setIdAplicacion(getStringValue(registro, "ID_APP"));
					cuestionario.setNumeroContrato(getStringValue(registro, "NUM_CNTR"));
					cuestionario.setFechaCreacion(getStringValue(registro, "FCH_CREAC"));
					cuestionario.setCodigoProducto(getStringValue(registro, "COD_PROD"));
					cuestionario.setDescProducto(getStringValue(registro, "DSC_PROD"));
					cuestionario.setCodigoSubproducto(getStringValue(registro, "COD_SUB_PROD"));
					cuestionario.setDescSubproducto(getStringValue(registro, "DSC_SUB_PROD"));
					cuestionario.setNivelRiesgo(getStringValue(registro, "COD_NIVEL_RIESG"));
					cuestionario.setIndicadorUpld(getStringValue(registro, "COD_IND_UPLD"));
					actividadGenerica.setCodigo(getStringValue(registro, "CVE_ACTIV_GEN"));
					actividadGenerica.setDescripcion(getStringValue(registro, "DSC_ACT_GEN"));
					actividadEspecifica.setCodigo(getStringValue(registro, "CVE_ACTIV_ESP"));
					actividadEspecifica.setDescripcion(getStringValue(registro, "DSC_ACT_ESP"));
					cuestionario.setDescSegmento(getStringValue(registro, "DSC_SEGMT"));
					cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
					cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
					cuestionario.setCodigoSucursal(getStringValue(registro, "COD_SUCU"));
					
					return cuestionario;
				}
			});
	}
	
	@Override
	public CuestionarioWSDTO obtenerDatosGeneralesCuestionarioPorId(String idCuestionario, ArchitechSessionBean psession) 
	throws BusinessException {
		return consultarUnico(
			String.format(SQL_OBTENER_DATOS_GRALES_CUEST_POR_ID, idCuestionario), new MapeoConsulta<CuestionarioWSDTO>() {
				@Override
				public CuestionarioWSDTO mapeoRegistro(
						Map<String, Object> registro) throws BusinessException {
				
					final CuestionarioWSDTO cuestionario = new CuestionarioWSDTO();
					final ClienteDTO cliente = new ClienteDTO();
					cuestionario.setCliente(cliente);
					
					cuestionario.setClaveCuestionario(getIntegerValue(registro, "CVE_CLTE_CUEST_PK"));
					cuestionario.setIdCuestionario(getStringValue(registro, "ID_CUEST"));
					cuestionario.setIdAplicacion(getStringValue(registro, "ID_APP"));
					cuestionario.setNumeroContrato(getStringValue(registro, "NUM_CNTR"));
					cuestionario.setFechaCreacion(getStringValue(registro, "FCH_CREAC"));
					cuestionario.setCodigoProducto(getStringValue(registro, "COD_PROD"));
					cuestionario.setDescProducto(getStringValue(registro, "DSC_PROD"));
					cuestionario.setCodigoSubproducto(getStringValue(registro, "COD_SUB_PROD"));
					cuestionario.setDescSubproducto(getStringValue(registro, "DSC_SUB_PROD"));
					cuestionario.setDescSegmento(getStringValue(registro, "DSC_SEGMT"));
					cuestionario.setCodigoPais(getStringValue(registro, "COD_PAIS_RESID"));
					cuestionario.setCodigoNacionalidad(getStringValue(registro, "COD_NACIO"));
					cuestionario.setClaveCuestionarioIP(getIntegerValue(registro, "CVE_CUEST_IP"));
					cliente.setIdCliente(getIntegerValue(registro, "CVE_CLTE_FK"));
					cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
					cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
					cuestionario.setIndicadorUpld(getStringValue(registro, "COD_IND_UPLD"));
					
					return cuestionario;
				}
			});
	}
	
	@Override
	public CuestionarioWSDTO obtenerDatosGeneralesCuestionarioPorIdCntrNoNulo(RequestObtenerCuestionario datosConsulta, ArchitechSessionBean psession) 
	throws BusinessException {
		StringBuilder sentenciaSQL = new StringBuilder(SQL_OBTENER_DATOS_GRALES_CUEST_POR_ID_CNTR_NOT_NULL);
		
		if(StringUtils.isNotBlank(datosConsulta.getIdCuestionario())) {
			sentenciaSQL.append(" AND cc.ID_CUEST = '%s' ");
			return consultarUnico(String.format(sentenciaSQL.toString(), datosConsulta.getIdCuestionario()), new CuestionarioWSMapper());
		} else {
			sentenciaSQL
				.append(" AND cc.NUM_CNTR='%s'" )
				.append(" AND cc.COD_PROD= '%s'")
				.append(" AND cc.COD_SUB_PROD = '%s'")
				.append(" AND cc.COD_SUCU= '%s'")
				.append(" ORDER BY cc.CVE_CLTE_CUEST_PK DESC ");
			
			List<CuestionarioWSDTO> cuestionarios = consultar(
				String.format(sentenciaSQL.toString(), datosConsulta.getContrato(), datosConsulta.getCodProducto(), 
														datosConsulta.getCodSubProducto(), datosConsulta.getCodSucursal()), 
				new CuestionarioWSMapper());
			
			if(cuestionarios == null || cuestionarios.isEmpty()) {
				return null;
			}
			
			return cuestionarios.get(0);
		}
	}
	
	@Override
	public CuestionarioDTO obtenerDatosGeneralesCuestionarioPorClaveCC(int claveClienteCuestionario, ArchitechSessionBean psession) 
	throws BusinessException {
		return consultarUnico(
			String.format(SQL_OBTENER_DATOS_GRALES_CUEST_POR_CLAVE_CC, claveClienteCuestionario), 
			new CuestionarioMapper());
	}
	
	@Override
	public List<CuestionarioDTO> obtenerDatosGeneralesCuestionarioMismoClienteMismoDia(CuestionarioDTO cuestionarioDTO, ArchitechSessionBean psession) 
	throws BusinessException {
		return consultar(String.format(
				SQL_OBTENER_CUESTIONARIO_MISMO_CLIENTE, 
				cuestionarioDTO.getCodigoCliente(), cuestionarioDTO.getIdAplicacion(), cuestionarioDTO.getNivelRiesgo()), 
			new CuestionarioMapper());
	}
}
