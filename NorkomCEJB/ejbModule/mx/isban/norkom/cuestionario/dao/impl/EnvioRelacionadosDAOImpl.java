/**
 * Isban Mexico
 *   Clase: EnvioRelacionadosDAOImpl.java
 *   Descripcion: Componente para consultar informacion de los relacionados.
 *   extiende de GenericBdIsbanDA
 *   e implementa EnvioRelacionadosDAO
 *   Control de Cambios:
 *   1.0 Jul 23, 2017 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerRelacionados;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerRelacionados;
import mx.isban.norkom.cuestionario.dao.EnvioRelacionadosDAO;
import mx.isban.norkom.cuestionario.dao.mapeo.RelacionadoMapper;
import mx.isban.norkom.cuestionario.util.EstatusRelacionados;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO;
/**
 * clase Dao para el envio de los 
 * relacionados por bloque
 * en los nuevos flujos de norkom
 * de validacion de relacionados
 * la clase ectien de GenericBdIsbanDA
 * e immplementa a EnvioRelacionadosDAO
 * @author lespinosa
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class EnvioRelacionadosDAOImpl extends GenericBdIsbanDA implements EnvioRelacionadosDAO {
	
	/**
	 * Variable utilizada para declarar
	 *  serialVersionUID
	 *  de tipo long
	 */
	private static final long serialVersionUID = -4943747808654610394L;
	/**
	 * Constante para Query 
	 * QRY_OBTENER_RELACIONADOS_BLOQUE
	 */
	private static final String QRY_OBTENER_RELACIONADOS_WS  = "SELECT cc.ID_CUEST, r.TXT_EST_REL, r.NUM_BLOQUE, r.NUM_ENV "
			+ "FROM NKM_MX_AUX_RELAC_DRO r INNER JOIN NKM_MX_MAE_CLTE_CUEST_DRO cc ON r.CVE_CLTE_CUES_FK = cc.CVE_CLTE_CUEST_PK "
			+ "WHERE CC.ID_CUEST ='%s' "
			+ "AND r.NUM_BLOQUE = %s "
			+ "AND (r.TXT_EST_REL IS NOT NULL AND r.TXT_EST_REL ='%s') ";
	
	/**
	 * Constante para Query 
	 * QRY_OBTENER_RELACIONADOS
	 */
	private static final String QRY_OBTENER_RELACIONADOS_PANTALLA  = "SELECT cc.ID_CUEST, r.TXT_EST_REL, r.NUM_BLOQUE "
			+ "FROM NKM_MX_AUX_RELAC_DRO r INNER JOIN NKM_MX_MAE_CLTE_CUEST_DRO cc ON r.CVE_CLTE_CUES_FK = cc.CVE_CLTE_CUEST_PK "
			+ "WHERE CC.ID_CUEST ='%s' "
			+ "AND (r.TXT_EST_REL IS NOT NULL AND r.TXT_EST_REL !='%s') ";
	
	/** 
	 * Constante para Query 
	 * SQL_UPDATE_ESTATUS_REL
	 */
	private static final String SQL_UPDATE_ESTATUS_REL = "UPDATE NKM_MX_AUX_RELAC_DRO "
			+ "SET TXT_EST_REL = '%s' "
			+ "WHERE CVE_CLTE_CUES_FK = (SELECT CVE_CLTE_CUEST_PK FROM NKM_MX_MAE_CLTE_CUEST_DRO WHERE ID_CUEST = '%s') "
			+ "AND NUM_BLOQUE = %s ";
	
	/**
	 * Constante para Query para
	 * actualizar los intentos
	 * SQL_UPDATE_INTENTOS
	 */
	private static final String SQL_UPDATE_INTENTOS = "update NKM_MX_AUX_RELAC_DRO set NUM_ENV=(NUM_ENV+1) where CVE_CLTE_CUES_FK="+
		"(SELECT CVE_CLTE_CUEST_PK FROM NKM_MX_MAE_CLTE_CUEST_DRO WHERE ID_CUEST = '%s')"+
		" and NUM_BLOQUE=%s ";
	/**
	 * Constante ara update de Estatus a lor relacionados
	 * en estatus de enviado despues de los intentos
	 */
	private static final String SQL_UPDATE_ESTATUS_REL_ERR="UPDATE NKM_MX_AUX_RELAC_DRO "
		+ "SET TXT_EST_REL = '"+EstatusRelacionados.RECHAZADO.toString()+"'"
		+ "WHERE CVE_CLTE_CUES_FK = (SELECT CVE_CLTE_CUEST_PK FROM NKM_MX_MAE_CLTE_CUEST_DRO WHERE ID_CUEST = '%s') "
		+ "AND TXT_EST_REL = 'ENV' ";

	/**
	 * Constructor
	 * para tener la relacion al canal
	 * de Norkom
	 */
	public EnvioRelacionadosDAOImpl() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	
	@Override
	public List<ResponseObtenerRelacionados> obtenerRelacionados (RequestObtenerRelacionados request, 
			 ArchitechSessionBean psession) throws BusinessException{

		List<ResponseObtenerRelacionados> preguntas = new ArrayList<ResponseObtenerRelacionados>();
		if(request.getBloque()!=null && !request.getBloque().isEmpty()){
			preguntas = consultar(String.format(QRY_OBTENER_RELACIONADOS_WS, 
					request.getIdCuestionario(), request.getBloque(), EstatusRelacionados.RECIBIDO.toString()),new RelacionadoMapper());
		}else{
			preguntas = consultar(String.format(QRY_OBTENER_RELACIONADOS_PANTALLA, 
					request.getIdCuestionario(),EstatusRelacionados.OK.toString()),new RelacionadoMapper());
		}
		
		return preguntas;

	}
	
	@Override
	public void actualizarEstatusRel(mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO request, String estatus, ArchitechSessionBean psession) throws BusinessException {
		update(String.format(SQL_UPDATE_ESTATUS_REL, estatus, request.getIdCuestionario(), request.getNumeroBloque()));
	}
	
	@Override
	public void actualizarIntentos(RequestDatosRelacionadosWSDTO request, ArchitechSessionBean psession) throws BusinessException {
		update(String.format(SQL_UPDATE_INTENTOS, request.getIdCuestionario(), request.getNumeroBloque()));
	}

	@Override
	public void actualizarEstatusErr(RequestDatosRelacionadosWSDTO request, ArchitechSessionBean psession)
			throws BusinessException {
		debug("Realizamos el update a error de los relacionados");
		update(String.format(SQL_UPDATE_ESTATUS_REL_ERR, request.getIdCuestionario()));
		
	}

}
