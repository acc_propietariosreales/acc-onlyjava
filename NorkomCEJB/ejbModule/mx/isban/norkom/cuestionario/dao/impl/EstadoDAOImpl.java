/**
 * Isban Mexico
 *   Clase: EstadoDAOImpl.java
 *   Descripcion: Componente para consultar informacion de los estados.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.EstadoDAO;
import mx.isban.norkom.cuestionario.dto.EstadoDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class EstadoDAOImpl extends GenericBdIsbanDA implements EstadoDAO, MapeoConsulta<EstadoDTO> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 6636154044978477836L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(EstadoDAOImpl.class);

    /**
     * CONSTRUCTOR EstadoDAOImpl
     */
    public EstadoDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    
    @Override
    public List<EstadoDTO> obtenerEstados(ArchitechSessionBean sesion) throws BusinessException {
		LOG.info(String.format("Ejecutando consulta de obtenerEstados [%s]", SQL_OBTENER_ESTADO));
		return consultar(SQL_OBTENER_ESTADO, this);
	}
    
    @Override
    public EstadoDTO obtenerEstadoPorCodigo(String codigoEstado, ArchitechSessionBean sesion) throws BusinessException {
    	LOG.info(String.format("Ejecutando consulta de obtenerEstadoPorCodigo [%s][%s]", 
    							SQL_OBTENER_ESTADO_POR_CODIGO, codigoEstado));
    	return consultarUnico(String.format(SQL_OBTENER_ESTADO_POR_CODIGO, codigoEstado), this);
    }
	
	@Override
	public EstadoDTO mapeoRegistro(Map<String, Object> registro) throws BusinessException {
		EstadoDTO estadoDTO = new EstadoDTO();
		
		estadoDTO.setClaveEstado(Integer.parseInt(getStringValue(registro, "CVE_EDO_PK")));
		estadoDTO.setCodigo(getStringValue(registro, "COD_EDO"));
		estadoDTO.setNombre(getStringValue(registro, "TXT_NOMB"));
		
		return estadoDTO;
	}

}
