/**
 * Isban Mexico
 *   Clase: GeneraXmlDAO.java
 *   Descripcion: DAOImpl para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.GeneraXmlDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioIPDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesXmlDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

/**
 * 
 * @author jugarcia
 * Objetivo: Realizar las operaciones para la generacion del XML que se envia a Norkom KYC
 * Justificacion: Obtiene las preguntas del Cuestionario IP, los paises para transferencias, el origen y destino de los recursos asi como los relacionados.
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class GeneraXmlDAOImpl extends GenericBdIsbanDA implements GeneraXmlDAO {
	
	/** Variable utilizada para declarar serialVersionUID **/
	private static final long serialVersionUID = 3381004753618377197L;
	
	/** Constructor CuestionarioDAOImpl **/
	public GeneraXmlDAOImpl() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
		
	@Override
	public CuestionarioIPDTO obtenerFormularioIP(String id_cuestionario, ArchitechSessionBean psession) 
		throws BusinessException {
		return consultarUnico(
				String.format(SQL_OBTENER_FORMULARIO_IP, id_cuestionario), 
				new MapeoConsulta<CuestionarioIPDTO>(){
					@Override
					public CuestionarioIPDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						final CuestionarioIPDTO formularioIP = new CuestionarioIPDTO();
						final String aplicactivo = getStringValue(registro,"ID_CUEST").substring(0,2);
						final String idCuestionario = getStringValue(registro,"ID_CUEST");
						//Se modifica para una buena obtencion de id y fecha LFER 21/07/2017
						formularioIP.setIdFormulario(idCuestionario.substring(0,idCuestionario.length()-8));
						formularioIP.setDescAplicativo(getStringValue(registro,"VAL_PARAM"));
						formularioIP.setFechaCreacion(idCuestionario.substring(idCuestionario.length()-8));
						formularioIP.setValBUC(getStringValue(registro,"VAL_BUC"));
						formularioIP.setCodigoEntidad(getStringValue(registro,"COD_ENT"));
						formularioIP.setNombrePersona(getStringValue(registro,"NOM_EMPRESA"));
						formularioIP.setFechaPersona(getStringValue(registro,"FCH_NAC_CLTE"));
						formularioIP.setTipoPersona(getStringValue(registro,"COD_TIPO_PERS"));
						formularioIP.setSubtipoPersona(getStringValue(registro,"COD_SUB_TIPO_PERS"));
						formularioIP.setCodigoSegmento(getStringValue(registro,"COD_SEGMT"));
						formularioIP.setCodigoActividadEcon(getStringValue(registro,"COD_ACT_ECON"));
						if(TERM_FINANCIERO.equals(aplicactivo) || LIGHT_HOUSE.equals(aplicactivo)){
							formularioIP.setCodigoProducto(getStringValue(registro,"COD_PROD")+getStringValue(registro,"COD_SUB_PROD"));
						}else if(OPICS.equals(aplicactivo) || OPICS_FRONT.equals(aplicactivo)){
							formularioIP.setCodigoProducto(getStringValue(registro,"COD_BRNCH")+getStringValue(registro,"COD_CENT_COSTO"));
						}
						formularioIP.setCodigoPais(getStringValue(registro,"COD_PAIS_RESID"));
						formularioIP.setCodigoMunicipio(getStringValue(registro,"COD_MUN_RESID"));
						formularioIP.setCodigoNacionalidad(getStringValue(registro,"COD_NACIO"));
						formularioIP.setDivisa(getStringValue(registro,"COD_DIV_PROD"));
						formularioIP.setRegionSucursal(getStringValue(registro,"COD_REGN"));
						formularioIP.setZonaSucursal(getStringValue(registro,"COD_ZONA"));
						formularioIP.setCodigoCentroCostos(getStringValue(registro,"COD_SUCU"));						
						
						return formularioIP;
					}
				});
	}

	@Override
	public List<OpcionesXmlDTO> obtenerOpcionesIP(String id_cuestionario, ArchitechSessionBean psession) 
		throws BusinessException {
		return consultar(
				String.format(SQL_OBTENER_OPCIONES_IP, id_cuestionario), 
				new MapeoConsulta<OpcionesXmlDTO>(){
					@Override
					public OpcionesXmlDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						final OpcionesXmlDTO opcionesIP = new OpcionesXmlDTO();
						
						opcionesIP.setValor(getStringValue(registro, "VAL_OPC"));
						opcionesIP.setDescripcion(getStringValue(registro, "DSC_OPC"));
						opcionesIP.setAbreviatura(getStringValue(registro, "TXT_ABRV"));
						
						return opcionesIP;
					}
				});
	}
	
	@Override
	public List<PaisDTO> obtenerPaisTransInter(String id_cuestionario,
			ArchitechSessionBean psession) throws BusinessException {
		return consultar(
				String.format(SQL_OBTENER_PAIS_TRANS, id_cuestionario),
				new MapeoConsulta<PaisDTO>(){
					@Override
					public PaisDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException{
						final PaisDTO paisesTransInter = new PaisDTO();
						
						paisesTransInter.setCodigo(getStringValue(registro,"COD_PAIS"));

						return paisesTransInter;
					}
				});
	}

	@Override
	public List<RecursoDTO> obtenerRecursosIP(String id_cuestionario,
			ArchitechSessionBean psession) throws BusinessException {
		return consultar(
				String.format(SQL_OBTENER_RECURSOS_IP, id_cuestionario), 
				new MapeoConsulta<RecursoDTO>(){
					@Override
					public RecursoDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						final RecursoDTO recursosIP = new RecursoDTO();
						
						recursosIP.setCodigoRecurso(getStringValue(registro,"COD_REC"));
						recursosIP.setDescripcion(getStringValue(registro,"DSC_REC"));
						recursosIP.setTipoRecurso(getStringValue(registro, "TXT_ABRV"));
						
						return recursosIP;
					}
				});
	}


	@Override
	public List<RelacionadoDTO> obtenerRelacionadosIP(String id_cuestionario,
			ArchitechSessionBean psession) throws BusinessException {
		return consultar(
				String.format(SQL_OBTENER_RELACIONADOS_IP, id_cuestionario),
				new MapeoConsulta<RelacionadoDTO>(){
					@Override
					public RelacionadoDTO mapeoRegistro(Map<String, Object> registro)
					throws BusinessException {
						final RelacionadoDTO relacionadosIP = new RelacionadoDTO();
						final String tipoRelacionado = getStringValue(registro, "COD_TIPO_RELAC");
						
						relacionadosIP.setNombre(getStringValue(registro,"TXT_NOMB_RELAC"));
						relacionadosIP.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_RELAC"));
						relacionadosIP.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_REL"));
						relacionadosIP.setTipoPersona(getStringValue(registro,"COD_TIPO_PERS_REL"));
						relacionadosIP.setFechaNacimiento(getStringValue(registro,"FCH_NAC_REL"));
						relacionadosIP.setCodPaisNacionalidad(getStringValue(registro,"COD_PAIS_NACIO_REL"));
						relacionadosIP.setCodPaisResidencia(getStringValue(registro,"COD_PAIS_RESID_REL"));
						relacionadosIP.setTipoRelacion(tipoRelacionado);
						
						return relacionadosIP;
					}
				});
	}
	
}