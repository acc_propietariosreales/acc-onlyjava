/**
 * Isban Mexico
 *   Clase: ManejadorPistasDAOImpl.java
 *   Descripcion: Clase encarga de realizar la insercion de las pistas
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.ManejadorPistasDAO;
import mx.isban.norkom.cuestionario.dto.PistaAuditoriaDTO;
import mx.isban.norkom.cuestionario.util.ConstantesAuditoria;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;

import org.apache.log4j.Logger;

/** 
 * Clase encarga de realizar la insercion de las pistas
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ManejadorPistasDAOImpl extends GenericBdIsbanDA implements ManejadorPistasDAO {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 6636154044978477836L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(ManejadorPistasDAOImpl.class);

    /**
     * CONSTRUCTOR para el Manejador de Pistas
     */
    public ManejadorPistasDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    
    @Override
    public void agregarPista(PistaAuditoriaDTO pistaDTO, ArchitechSessionBean sesion) throws BusinessException {
    	LOG.info(String.format("Se va a realizar insercion de la pista[%s]", pistaDTO.getServicio()));
    	update(generarSentenciaInsercion(pistaDTO, sesion));
    }
    
    /**
     * Realiza la sentencia SQL para la insercion de las pistas
     * @param pistaDTO Objeto con la informacion de la pista
     * @param sesion Objeto de sesion de Agave
     * @return La sentencia SQL que se va a ejecutar para la insercion de la pista
     */
    private String generarSentenciaInsercion(PistaAuditoriaDTO pistaDTO, ArchitechSessionBean sesion){
    	return String.format(SQL_AGREGAR_PISTA, 
    			pistaDTO.getServicio(),
    			sesion.getIPCliente(),
    			ConstantesAuditoria.CANAL_APP,
    			sesion.getUsuario(),
    			sesion.getIdSesion(),
    			sesion.getIdRefApp(),
    			sesion.getNombreServidor(),
    			pistaDTO.getCodigoOperacion(),
    			pistaDTO.getDatoAfectado(),
    			pistaDTO.getValorAnterior(),
    			pistaDTO.getValorNuevo(),
    			pistaDTO.getTablaAfectada(),
    			pistaDTO.getTipoOperacion(),
    			pistaDTO.getDatoFijo());
    }
}
