/**
 * Isban Mexico
 *   Clase: MunicipioDAOImpl.java
 *   Descripcion: Componente para consultar informacion de los municipios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.MunicipioDAO;
import mx.isban.norkom.cuestionario.dto.MunicipioDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class MunicipioDAOImpl extends GenericBdIsbanDA implements MunicipioDAO, MapeoConsulta<MunicipioDTO> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 6636154044978477836L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(MunicipioDAOImpl.class);

    /**
     * COSNTRUCTOR
     */
    public MunicipioDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    
    @Override
    public MunicipioDTO obtenerMunicipioPorCodigo(String codigoMunicipio, String codigoEstado, ArchitechSessionBean sesion) throws BusinessException {
    	LOG.info(String.format("Ejecutando consulta de obtenerMunicipioPorCodigo [%s][%s][%s]", 
    							SQL_OBTENER_MUNICIPIO_POR_CODIGO, codigoMunicipio, codigoEstado));
    	return consultarUnico(String.format(SQL_OBTENER_MUNICIPIO_POR_CODIGO, codigoMunicipio, codigoEstado), this);
    }
	
	@Override
	public MunicipioDTO mapeoRegistro(Map<String, Object> registro) throws BusinessException {
		MunicipioDTO municipioDTO = new MunicipioDTO();
		
		municipioDTO.setClaveMunicipio(Integer.parseInt(getStringValue(registro, "CVE_MUN_PK")));
		municipioDTO.setCodigoMunicipio(getStringValue(registro, "COD_MUN"));
		municipioDTO.setNombre(getStringValue(registro, "TXT_NOMB"));
		municipioDTO.setCodigoEstado(getStringValue(registro, "COD_EDO"));
		
		return municipioDTO;
	}

}
