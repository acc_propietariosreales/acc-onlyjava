/**
 * Isban Mexico
 *   Clase: NivelRiesgoDAOImpl.java
 *   Descripcion: Componente para obtener el nivel de riesgo.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.NivelRiesgoDAO;
import mx.isban.norkom.cuestionario.dto.CalificacionDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;


/**
 * Session Bean implementation class ActividadEconomicaDAOImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class NivelRiesgoDAOImpl extends GenericBdIsbanDA implements NivelRiesgoDAO, MapeoConsulta<CalificacionDTO>  {
	/**
	 * serial version id
	 */
	private static final long serialVersionUID = -4764725976960825246L;

	/**
     * @see Architech#Architech()
     */
    public NivelRiesgoDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    
    @Override
    public CalificacionDTO obtenerValoresPorIndicadorNorkom(String indicadorNorkom) throws BusinessException {
    	return consultarUnico(String.format(SQL_OBTENER_VALORES_POR_IND_NKM, indicadorNorkom), this);
    }
    
    @Override
    public CalificacionDTO obtenerValoresPorIndicadorPersonas(String indicadorPersonas) throws BusinessException {
    	return consultarUnico(String.format(SQL_OBTENER_VALORES_POR_IND_PERSONAS, indicadorPersonas), this);
    }
	
	@Override
	public void guardarCalificacionNorkom(int claveClienteCuest, String nivelRiesgo, String indicadorUpld) throws BusinessException {
		update(String.format(SQL_ACTUALIZAR_CALIFICACION_NKM, nivelRiesgo, indicadorUpld, claveClienteCuest));
	}

	@Override
	public CalificacionDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		CalificacionDTO calificacionDTO = new CalificacionDTO();
		
		calificacionDTO.setCodigoIndicadorPersonas(getStringValue(registro, "COD_IND_PERS_PK"));
		calificacionDTO.setValorIndicadorPersonas(getStringValue(registro, "VAL_IND_PERS_PK"));
		calificacionDTO.setValorIndicadorNorkom(getStringValue(registro, "VAL_IND_NKM"));
		calificacionDTO.setDescripcionNorkom(getStringValue(registro, "DSC_IND"));
		
		return calificacionDTO;
	}
	
	
}