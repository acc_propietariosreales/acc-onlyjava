/**
 * Isban Mexico
 *   Clase: ObtenerCuestionarioWSDAOHelperIC
 *   Descripcion: Componente de ayuda para ObtenerCuestionarioWSDAOIC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 -Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.rpt.AccionistasBean;
import mx.isban.norkom.cuestionario.bean.rpt.FamiliaresBean;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

public class ObtenerCuestionarioWSDAOIC extends GenericBdIsbanDA {

	/**
	 * String ESPACIO
	 */
	public static final String ESPACIO = " ";
	/**
	 * String SEPARADOR
	 */
	public static final String SEPARADOR = "-";
	/**
	 * String CONSULTA_ERROR
	 */
	public static final String CONSULTA_ERROR = "No se encontraron cuestionarios con los datos proporcionados ";
	/**
  	 * String FUENTE_INGRESO
  	 */
  	public static final String FUENTE_INGRESO = "strFuenteIngreso";
	/**
	 * String CVE_RPT
	 */
	public static final String CVE_RPT = "CVE_RPT";
	/**
	 * String VAL_OPC
	 */
	public static final String VAL_OPC = "VAL_OPC";
	/**
	 * String TXT_RESP
	 */
	public static final String TXT_RESP = "TXT_RESP";
	
	/**
	 * Sentencia SQL para obtener la respuesta de preguntas diferentes a INPUT de HTML
	 */
	private static final String SENTENCIA_SQL_RECURSOS_NO_INPUT = 
			"SELECT pr.CVE_RPT, op.DSC_OPC, pr.TXT_PREG " +
			"FROM NKM_MX_MAE_CLTE_CUEST_DRO ccd, NKM_MX_REL_CUEST_RESP_DRO ctrp, NKM_MX_PRC_RESP_PREG_DRO rppr, " +
			"NKM_MX_MAE_PREG_DRO pr, NKM_MX_AUX_GRPO_OPC_PREG_DRO gop, NKM_MX_AUX_OPC_PREG_DRO op " +
			"WHERE ccd.CVE_CLTE_CUEST_PK = ctrp.CVE_CLTE_CUEST_FK AND ccd.CVE_CUEST_IC_FK = ctrp.CVE_CUEST_FK " +
			"AND ctrp.CVE_CUEST_RESP_PK = rppr.CVE_CUEST_RESP_FK AND rppr.CVE_PREG_FK = pr.CVE_PREG_PK " +
			"AND pr.CVE_GRPO_FK = gop.CVE_GRPO_PK AND gop.CVE_GRPO_PK = op.CVE_GRPO_FK " +
			"AND pr.TXT_SECCN = 'IAPR' AND pr.CVE_GRPO_FK != 0 AND rppr.VAL_OPCN = op.CVE_OPC_PREG_PK ";
	
	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = -7075641624754142946L;
	
	/**
	 * ObtenerCuestionarioWSDAOHelperIC
	 */
	public ObtenerCuestionarioWSDAOIC() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	
	/***
	 * Metodo para obtener la informacion de los familiares
	 * @param contrato Contrato del cliente
	 * @param query Query para consulta
	 * @return Lista de objetos
	 * @throws BusinessException Exception manejada
	 */
	protected List<FamiliaresBean> obtenerInfoFamiliares(String contrato,String query)
	throws BusinessException {
		return consultar(query+contrato, 
			new MapeoConsulta<FamiliaresBean>(){
				@Override
				public FamiliaresBean mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					FamiliaresBean parentesco = new FamiliaresBean();
					parentesco.setStrNomFamiliar(getStringValue(registro,"TXT_NOMB_FAM"));	
					parentesco.setStrDomFamiliar(getStringValue(registro,"TXT_DOMIC_FAM"));
					String fecha = getStringValue(registro, "TXT_FCH_NAC");
					String [] fechaNac = fecha.split(ESPACIO);
					String [] year;
					year = fechaNac[0].split(SEPARADOR);
					String formato = year[2] + SEPARADOR + year[1] + SEPARADOR + year[0];
					parentesco.setStrFecNacFamiliar(formato);
					parentesco.setStrParentesco(getStringValue(registro,"TXT_PAREN"));
					return parentesco;
				}
			});
	}
	
	/***
	 * Metodo para obtener la informacion de los accionistas
	 * @param contrato Contrato del cliente
	 * @param query Query para consulta
	 * @return Lista de objetos
	 * @throws BusinessException Exception manejada
	 */
	protected List<AccionistasBean> obtenerInfoAccionistas(String contrato, String query)
	throws BusinessException {
		return consultar(query+contrato, 
			new MapeoConsulta<AccionistasBean>(){
				@Override
				public AccionistasBean mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					AccionistasBean accionistas = new AccionistasBean("","","","","");	
					String nombre = getStringValue(registro, "TXT_NOMB_RELAC");
					String apePat = getStringValue(registro, "TXT_APE_PAT_RELAC");
					String apeMat = getStringValue(registro, "TXT_APE_MAT_REL");
					String nombreCompleto = nombre + ESPACIO + apePat + ESPACIO + apeMat;
					accionistas.setStrNomAccionista(nombreCompleto);	
					String fecha = getStringValue(registro, "FCH_NAC_REL");
					String [] fechaNac = fecha.split(ESPACIO);
					String [] year;
					year = fechaNac[0].split(SEPARADOR);
					String formato = year[2] + SEPARADOR + year[1] + SEPARADOR + year[0];
					accionistas.setStrFecNacAccionista(formato);
					accionistas.setStrNacionAccionista(getStringValue(registro, "TXT_NOMB"));
					accionistas.setStrTipoAccionista(getStringValue(registro, "COD_TIPO_PERS_REL"));
					accionistas.setStrParticipacionAccionista(getStringValue(registro, "POR_PART"));
					return accionistas;
				}
			});
	}
	
	 /***
  	 * Metodo para obtener las preguntas de tipo Libre
  	 * @param query Query para consulta
  	 * @return Lista con respuesta de pregunta de tipo Libre
  	 * @throws BusinessException Exception manejada
  	 */
  	protected Map<String, Object> obtenerPreguntasLibresiIC( String query) throws BusinessException {	
  		List<HashMap<String, Object>> info = consultarSinMapeo(query);
  		if(info.isEmpty() || info == null)
		{
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(), 
					CONSULTA_ERROR);
		}
		else
		{
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get(TXT_RESP)!=null?hashMap.get(TXT_RESP).toString().toUpperCase():hashMap.get(TXT_RESP));
			}
			return resultado;
		}	
  	}
  	

  	/**
  	 * Metodo para obtener preguntas especificas de acuerdo a los recursos
  	 * @param contrato Contrato del cliente
  	 * @param query1 query  parte 1
  	 * @param query2 query parte 2
  	 * @return List<PreguntaDTO> lista con las respuestas y preguntas
  	 * @throws BusinessException Excepcion manejada
  	 */
  	protected List<PreguntaDTO> obtenerPreguntasRecursosIC(String contrato, String query1, String query2)
	throws BusinessException {
  		StringBuilder sentenciaSQL = new StringBuilder();
  		sentenciaSQL.append(query1);
  		sentenciaSQL.append(query2);
  		sentenciaSQL.append(contrato);
  		sentenciaSQL.append(" UNION ");
  		sentenciaSQL.append(String.format(SENTENCIA_SQL_RECURSOS_NO_INPUT, contrato));
  		sentenciaSQL.append(" AND ");
  		sentenciaSQL.append(contrato);
  		
		return consultar(sentenciaSQL.toString(), 
			new MapeoConsulta<PreguntaDTO>(){
				@Override
				public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					PreguntaDTO preguntas = new PreguntaDTO();	
					String clave = getStringValue(registro, "CVE_RPT");
					String txtPregunta = getStringValue(registro, "TXT_PREG");
					String respuesta = getStringValue(registro, TXT_RESP);
					preguntas.setClaveMapeo(clave);
					preguntas.setPregunta(txtPregunta);
					preguntas.setRespuesta(respuesta!=null?respuesta.toUpperCase():respuesta);
					return preguntas;
				}
			});
	}
  	
  	/***
	 * Metodo para obtener las preguntas de tipo Select unica respuesta
	 * 
	 * @param query Query para consulta
	 * @return Lista con preguntas y respuesta de select con unica respuesta
	 * @throws BusinessException Exception manejada
	 */
	protected Map<String, Object> obtenerPreguntasSelectUnicaIC( String query) throws BusinessException {	
		List<HashMap<String, Object>> info = consultarSinMapeo(query);			
		if(info.isEmpty() || info == null)
		{
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(), 
				 CONSULTA_ERROR);
		}
		else
		{
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get(VAL_OPC));	
			}		
			return resultado;
		}
	}
	
	/***
	 * Metodo para obtener las preguntas de tipo Radio Button con su respuesta
	 * @param query Query para consulta
	 * @return Lista con preguntas y respuesta de Radio Button
	 * @throws BusinessException Exception manejada
	 */
	protected Map<String, Object> obtenerPreguntasRadioIC(String query) throws BusinessException {	
		List<HashMap<String, Object>> info = consultarSinMapeo(query);
		if(info.isEmpty() || info == null)
		{
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(), 
					CONSULTA_ERROR);
		}
		else
		{
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get(VAL_OPC));
			}
			return resultado;
		}
	}
	
	/**
	 * @param respuestas Mapa con preguntas y respuestas de los recursos
	 * @param contrato contrato del cliente
	 * @param query1 query parte 1
	 * @param query2 query parte 2
	 * @return Map<String,Object>
	 * @throws BusinessException Excepcion manejada
	 */
	protected Map<String, Object> preguntasRecursos(Map<String, Object> respuestas, String contrato,
			String query1, String query2) throws BusinessException
	{
		List<PreguntaDTO> preguntasRecursos = new ArrayList<PreguntaDTO>();
		preguntasRecursos = obtenerPreguntasRecursosIC(contrato, query1, query2);
		for (PreguntaDTO preguntaDTO : preguntasRecursos) {
			String preguntaRespuesta = preguntaDTO.getPregunta() + 
			":  " + validaRespuestaVacia(preguntaDTO.getRespuesta());
			respuestas.put( preguntaDTO.getClaveMapeo(), preguntaRespuesta);
		}
		return respuestas;
	}
	/**
	 * valida si esta vacio o viene la palabra null
	 * @param respuesta a validar
	 * @return respuesta valida
	 */
	private String validaRespuestaVacia(String respuesta) {
		if(respuesta==null || "null".equals(respuesta) || "NA".equalsIgnoreCase(respuesta)){
			respuesta="";
		}
		return respuesta;
	}

}
