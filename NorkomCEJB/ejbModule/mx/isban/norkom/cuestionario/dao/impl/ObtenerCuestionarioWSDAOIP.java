/**
 * Isban Mexico
 *   Clase: ObtenerCuestionarioWSDAOHelperIP
 *   Descripcion: Componente de ayuda  de ObtenerCuestionarioWSDAOIP.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.Cuestionarios;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.MapeoConsulta;

public class ObtenerCuestionarioWSDAOIP extends ObtenerCuestionarioWSDAOIC {
	
	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 2524639563299973613L;
	/**postrofe final para queries**/
	private static final String APOSFINAL="' ";
	/** Orden de paises en consulta */
	private static final String ORDER_BY_PAISES = " ORDER BY PAIS.TXT_NOMB ";
    /***
  	 * Metodo para obtener las preguntas de tipo Libre
  	 * @param contrato Contrato del cliente
  	 * @param query Query para consulta
  	 * @return Lista con respuesta de pregunta de tipo Libre
  	 * @throws BusinessException Exception manejada
  	 */
  	protected Map<String, Object> obtenerPreguntasLibresIP(
  			 String contrato, String query) throws BusinessException {	
  		List<HashMap<String, Object>> info = consultarSinMapeo(String.format(query, contrato).toString());
  		if(info.isEmpty() || info == null)
		{
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(), 
					CONSULTA_ERROR);
		}
		else
		{
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get("TXT_RESP"));
			}
			return resultado;
		}	
  	}
	
	/***
	 * Metodo para obtener las preguntas de tipo Select unica respuesta
	 * @param peticion datos de la peticion
	 * @param query Query para consulta
	 * @return Lista con preguntas y respuesta de select con unica respuesta
	 * @throws BusinessException Exception manejada
	 */
  	protected Map<String, Object> obtenerPreguntasSelectUnicaIP(
			RequestObtenerCuestionario peticion, String query) throws BusinessException {	
		List<HashMap<String, Object>> info = consultarSinMapeo(query+creaWhere(peticion));			
		if(info.isEmpty() || info == null)
		{
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(), 
					CONSULTA_ERROR);
		}
		else
		{
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get(VAL_OPC));
			}
			return resultado;
		}
	}


	/***
	 * Metodo para obtener las preguntas de tipo Radio Button con su respuesta
	 * @param peticion datos de la peticion
	 * @param query Query para consulta
	 * @return Lista con preguntas y respuesta de Radio Button
	 * @throws BusinessException Exception manejada
	 */
  	protected Map<String, Object> obtenerPreguntasRadioIP(
  			RequestObtenerCuestionario peticion, String query) throws BusinessException {	
		List<HashMap<String, Object>> info = consultarSinMapeo(query+creaWhere(peticion));
		if(info.isEmpty() || info == null)
		{
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(), 
					CONSULTA_ERROR);
		}
		else
		{
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get(VAL_OPC));
			}
			return resultado;
		}
	}
	
	/***
	 * Metodo para obtener las preguntas Catalogo de Paises con su respuesta
	 * @param peticion datos de la peticion
	 * @param query Query para consulta
	 * @return Lista con respuesta del Catalogo de Pais
	 * @throws BusinessException Exception manejada
	 */
	protected List<PreguntaDTO> obtenerPreguntasPais(
			RequestObtenerCuestionario peticion,String query) throws BusinessException {	
			List<PreguntaDTO> preguntas = consultar(query+creaWhere(peticion)+ORDER_BY_PAISES,new MapeoConsulta<PreguntaDTO>() {
				@Override
				public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					PreguntaDTO dto = new PreguntaDTO();					
					dto.setRespuesta(getStringValue(registro, "TXT_NOMB"));
					return dto;
					}
				});
		return preguntas;
	}
	
	/***
	 * Metodo para obtener las preguntas Catalogo de Recursos Origen con su respuesta
	 * @param peticion datos de la peticion
	 * @param query Query para consulta
	 * @return Lista con preguntas y respuesta del Catalogo de Origen de los recursos
	 * @throws BusinessException Exception manejada
	 */
	protected Map<String, Object> obtenerPreguntasOrigenIP(
			RequestObtenerCuestionario peticion, String query) throws BusinessException {	
		List<HashMap<String, Object>> info = consultarSinMapeo(query+creaWhere(peticion));
		
			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get("DSC_REC"));
			}
			return resultado;
		
	}
	
	/***
	 * Metodo para obtener las preguntas Catalogo de Recursos Destino con su respuesta
	 * @param peticion datos de la peticion
	 * @param query Query para consulta
	 * @return Lista con preguntas y respuesta del Catalogo de Origen de los recursos
	 * @throws BusinessException Exception manejada
	 */
	protected Map<String, Object> obtenerPreguntasDestinoIP(
			RequestObtenerCuestionario peticion, String query) throws BusinessException {	
		List<HashMap<String, Object>> info = consultarSinMapeo(query+creaWhere(peticion));

			HashMap<String, Object> resultado= new HashMap<String, Object>();
			for (HashMap<String, Object> hashMap : info) {
				resultado.put(getStringValue(hashMap,CVE_RPT), hashMap.get("DSC_REC"));
			}
			return resultado;
	}
	
	/**
	 * creacion de where para los queryes de obtencion de PDF
	 * @param peticion RequestObtenerCuestionario con los datos para el where
	 * @return string con el where segun datos de entrada
	 */
	protected String creaWhere( RequestObtenerCuestionario peticion){
		StringBuffer where= new StringBuffer();
		if(peticion.getIdCuestionario()!=null && !peticion.getIdCuestionario().isEmpty()){
			where.append("CCD.ID_CUEST='" ).append(peticion.getIdCuestionario()).append(APOSFINAL);
		}else{
			where.append("CCD.NUM_CNTR='" ).append(peticion.getContrato()).append(APOSFINAL)
			.append(" AND CCD.COD_PROD= '").append(peticion.getCodProducto()).append(APOSFINAL)
			.append(" AND CCD.COD_SUB_PROD = '").append(peticion.getCodSubProducto()).append(APOSFINAL)
			.append(" AND CCD.COD_SUCU= '").append(peticion.getCodSucursal()).append(APOSFINAL);
		}
		return where.toString();
		
	}
	
	/**
	 * Define el tipo de Cuestionario IC
	 * @param response Objeto para guardar el tipo de cuestionario IC
	 * @param cuestionario Contiene el valor con el cual se va a definir el tipo de cuestionario
	 */
	protected void agregarTipoIC(ResponseObtenerCuestionario response, CuestionarioDTO cuestionario) {
		if("A1".equals(cuestionario.getCodigoTipoFormulario())) {
			response.setTipo(Cuestionarios.TIPO_IC_A1);
		} else if("A2".equals(cuestionario.getCodigoTipoFormulario())) {
			response.setTipo(Cuestionarios.TIPO_IC_A2);
		} else {
			response.setTipo(Cuestionarios.TIPO_IC_A3);
		}
	}
}
