/**
 * Isban Mexico
 *   Clase: ObtenerCuestionarioWSDAOImpl.java
 *   Descripcion: Componente para consultar informacion de los cuestionarios para el WS.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.rpt.AccionistasBean;
import mx.isban.norkom.cuestionario.bean.rpt.ClientesBean;
import mx.isban.norkom.cuestionario.bean.rpt.FamiliaresBean;
import mx.isban.norkom.cuestionario.bean.rpt.ICListasBean;
import mx.isban.norkom.cuestionario.bean.rpt.MontosBean;
import mx.isban.norkom.cuestionario.bean.rpt.ProveedoresBean;
import mx.isban.norkom.cuestionario.bean.rpt.RefBancariasBean;
import mx.isban.norkom.cuestionario.bean.rpt.ReferenciasBean;
import mx.isban.norkom.cuestionario.bean.rpt.SociedadesBean;
import mx.isban.norkom.cuestionario.dao.ObtenerCuestionarioWSDAO;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.Cuestionarios;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.ObtenerCuestionarioWSListas;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ObtenerCuestionarioWSDAOImpl extends ObtenerCuestionarioWSDAOIP implements ObtenerCuestionarioWSDAO,MapeoConsulta<CuestionarioDTO> {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -9202915780014964779L;
	/**
	 * Logger de la clase
	 */
	private static final Logger LOG = Logger.getLogger(ObtenerCuestionarioWSDAOImpl.class);
	
	/**
	 * Numero de registros que se encuentran en la lista de familiares
	 */
	private static final String INT_NUMERO_REGISTROS = "intNumeroRegistros";
  	
	/***
	 * Metodo para obtener el encabezado del cuestionario IP
	 * @param registro Mapa de parametros
	 * @return DTO con informacion del encabezado
	 * @throws BusinessException Exception manejada  con mensaje de error
	 */
	@Override
	public CuestionarioDTO mapeoRegistro(Map<String, Object> registro)
			throws BusinessException {
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		ActividadEconomicaDTO actividad = new ActividadEconomicaDTO();
		ClienteDTO cliente = new ClienteDTO();
		cuestionario.setClaveCuestionario(Integer.parseInt(getStringValue(registro, "CVE_CLTE_CUEST_PK")));
		cuestionario.setIdCuestionario(getStringValue(registro, "ID_CUEST"));
		String fechaCues = getStringValue(registro, "FCH_CREAC");
		String [] fecha;
		String [] year;
		fecha = fechaCues.split(SEPARADOR);
		cuestionario.setAnio(fecha[0]);
		cuestionario.setMes(fecha[1]);
		year = fecha [2].split(ESPACIO); 
		cuestionario.setDia(year[0]);
		cuestionario.setNombreSucursal(getStringValue(registro, "TXT_NOMB_SUCU"));
		cuestionario.setCodigoSucursal(getStringValue(registro, "COD_SUCU"));
		cuestionario.setNombrePlaza(getStringValue(registro, "TXT_NOMB_PLAZA"));
		cuestionario.setNombreRegion(getStringValue(registro, "TXT_NOMB_ZONA"));
		cuestionario.setCuentaContrato(getStringValue(registro, "NUM_CNTR"));
		cuestionario.setDescSegmento(getStringValue(registro, "DSC_SEGMT"));
		cuestionario.setDescProducto(getStringValue(registro, "DSC_PROD"));
		cuestionario.setCodigoNacionalidad(getStringValue(registro, "COD_NACIO"));
		cuestionario.setNombreNacionalidad(getStringValue(registro, "NACIONALIDAD"));
		actividad.setCodigo(getStringValue(registro, "CVE_ACTIV_GEN"));
		cuestionario.setActividadGenerica(actividad);
		cuestionario.setCodigoMunicipio(getStringValue(registro,"COD_MUN_RESID"));
		cliente.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
		cliente.setNombreCliente(getStringValue(registro, "TXT_NOMB_CLTE"));
		cliente.setApellidoPaterno(getStringValue(registro, "TXT_APE_PAT_CLTE"));
		cliente.setApellidoMaterno(getStringValue(registro, "TXT_APE_MAT_CLTE"));
		cuestionario.setCliente(cliente);
		cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
		cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
		cuestionario.setDivisa(getStringValue(registro, "COD_DIV"));
		actividad.setDescripcion(getStringValue(registro, "DSC_ACT_ECON"));
		actividad.setTipoSector(getStringValue(registro, "FLG_SECTR"));
		actividad.setEntidadFinanciera(getStringValue(registro, "FLG_ENT_FIN"));
		cuestionario.setActividadEspecifica(actividad);
		cuestionario.setCodigoTipoFormulario(getStringValue(registro, "VAL_TIPO_CUEST"));
		cuestionario.setNombrePais(getStringValue(registro, "PAIS"));
		cuestionario.setCodigoPais(getStringValue(registro, "COD_PAIS"));
		cuestionario.setPaisBajoRiesgo(getStringValue(registro, "FLG_RIESG"));
		cuestionario.setNombreEntidad(getStringValue(registro, "ESTADO"));
		cuestionario.setNombreMunicipio(getStringValue(registro, "MUNICIPIO"));
		cuestionario.setNivelRiesgo(getStringValue(registro, "COD_NIVEL_RIESG"));
		cuestionario.setNombreFuncionario(getStringValue(registro, "TXT_NMBR_FUNC"));
		
		return cuestionario;
	}  

	/***
	 * Metodo para obtener el cuestionario IP
	 * @param peticion Contrato del cliente
	 * @param psession ArchitechSessionBean
	 * @return Response con informacion del cuestinario IP
	 * @throws BusinessException Exception manejada  con mensaje de error
	 */
	@Override
	public ResponseObtenerCuestionario obtenerCuestionarioIP(
			RequestObtenerCuestionario peticion, ArchitechSessionBean psession)
			throws BusinessException {
		LOG.info("Entrando a obtenerCuestionarioIP ------>");
		ResponseObtenerCuestionario response = new ResponseObtenerCuestionario();
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		HashMap<String, Object> respuestas = new HashMap<String, Object>();
		respuestas.putAll(obtenerPreguntasSelectUnicaIP(peticion, QRY_PREGUNTAS_SELECT_UNICA_IP));
		respuestas.putAll(obtenerPreguntasRadioIP(peticion,QRY_PREGUNTAS_RADIO_IP));
		List<PreguntaDTO> listaPaises = obtenerPreguntasPais(peticion, QRY_PREGUNTAS_PAIS_IP);
		respuestas.putAll(obtenerPreguntasOrigenIP(peticion,QRY_PREGUNTAS_ORIGEN_IP));
		respuestas.putAll(obtenerPreguntasDestinoIP(peticion,QRY_PREGUNTAS_DESTINO_IP));
		response.setListasBeanIC(new ICListasBean());
		response.getListasBeanIC().setLstPaises(listaPaises);
		cuestionario = consultarUnico(String.format(QRY_ENCABEZADO_IP+creaWhere(peticion)), this);
		if(cuestionario!=null){
		respuestas.put("strNacionalidadVal", cuestionario.getCodigoNacionalidad());
		respuestas.put("strPaisVal", cuestionario.getCodigoPais());
		}
		response.setDatosReporte(respuestas);
		if(cuestionario != null){
			response.setCuestionarioDTO(cuestionario);
			if("IP".equals(cuestionario.getCodigoTipoFormulario()))
			{
				response.setTipo(Cuestionarios.TIPO_IP);
			}
			if("F".equals(cuestionario.getTipoPersona()) && "N".equals(cuestionario.getSubtipoPersona()))
			{
				response.setTipoPersona(Cuestionarios.PERSONA_FISICA);
			}
			else if("F".equals(cuestionario.getTipoPersona()) && "S".equals(cuestionario.getSubtipoPersona()))
			{
				response.setTipoPersona(Cuestionarios.PERSONA_FISICA_AE);
			}
			else if("J".equals(cuestionario.getTipoPersona()) && "N".equals(cuestionario.getSubtipoPersona()))
			{
				response.setTipoPersona(Cuestionarios.PERSONA_MORAL);
			}
			if("MXP".equals(cuestionario.getDivisa()))
			{
				response.setTipoMoneda(Cuestionarios.TIPO_MONEDA_PESOS);
			}
			else if("USD".equals(cuestionario.getDivisa()))
			{
				response.setTipoMoneda(Cuestionarios.TIPO_MONEDA_DOLARES);
			}
		}		
		return response;
	}

	/***
	 * Metodo para obtener el cuestionario IC
	 * @param peticion Contrato del cliente
	 * @param dto ResponseObtenerCuestionario 
	 * @param psession ArchitechSessionBean
	 * @return Response con informacion del cuestinario IC
	 * @throws BusinessException Exception manejada
	 */
	@Override
	public ResponseObtenerCuestionario obtenerCuestionarioIC(
			RequestObtenerCuestionario peticion,ResponseObtenerCuestionario dto, ArchitechSessionBean psession)
			throws BusinessException {
		ResponseObtenerCuestionario response = new ResponseObtenerCuestionario();
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		ICListasBean listas = new ICListasBean();
		HashMap<String, Object> respuestas = dto.getDatosReporte()!=null?(HashMap<String, Object> )dto.getDatosReporte():new HashMap<String, Object>();
		respuestas = (HashMap<String, Object>) respuestas(peticion);
		listas = listasIC(response, respuestas);
		
		response.setListasBeanIC(listas);
		respuestas.putAll(preguntasRecursos(respuestas,creaWhere(peticion), QRY_PREGUNTAS_ESPECIFICAS_RECURSOS_IC_1, QRY_PREGUNTAS_ESPECIFICAS_RECURSOS_IC_2));
		response.setDatosReporte(respuestas);
		cuestionario = obtenerEncabezadoIC(creaWhere(peticion),QRY_ENCABEZADO_IC);
		
		if(cuestionario == null){
			throw new BusinessException(Mensajes.ERROR_NO_DATOS.getCodigo(),CONSULTA_ERROR + peticion.getContrato());
		} else {
			response.setCuestionarioDTO(cuestionario);
			agregarTipoIC(response, cuestionario);
			
			if("F".equals(cuestionario.getTipoPersona()) && "N".equals(cuestionario.getSubtipoPersona())) {
				if("A1".equals(cuestionario.getCodigoTipoFormulario())) {
					response.setTipoPersona(Cuestionarios.PERSONA_FISICA);
				} else {
					List<FamiliaresBean> lstFamiliares = obtenerInfoFamiliares(creaWhere(peticion),QRY_PREGUNTAS_INFORMACION_FAMILIARES_IC);
					listas.setLstFamiliares(lstFamiliares);
					
					agregarCampoNumeroRegistrosFamiliares(lstFamiliares, respuestas);
					
					response.setTipoPersona(Cuestionarios.PERSONA_FISICA);
				}
			}
			
			if("F".equals(cuestionario.getTipoPersona()) && "S".equals(cuestionario.getSubtipoPersona())) {
				if("A1".equals(cuestionario.getCodigoTipoFormulario())) {
					response.setTipoPersona(Cuestionarios.PERSONA_FISICA_AE);
				} else {
					List<FamiliaresBean> lstFamiliares = obtenerInfoFamiliares(creaWhere(peticion),QRY_PREGUNTAS_INFORMACION_FAMILIARES_IC);
					listas.setLstFamiliares(lstFamiliares);
					
					agregarCampoNumeroRegistrosFamiliares(lstFamiliares, respuestas);
					
					response.setTipoPersona(Cuestionarios.PERSONA_FISICA_AE);
				}
			}
			
			if("J".equals(cuestionario.getTipoPersona()) && "N".equals(cuestionario.getSubtipoPersona())) {
				response.setTipoPersona(Cuestionarios.PERSONA_MORAL);
				List<AccionistasBean> lstAccionistas = obtenerInfoAccionistas(creaWhere(peticion),QRY_INFORMACION_ACCIONISTAS_IC);
				validaAccionistas(lstAccionistas,response);
				listas.setLstAccionistas(lstAccionistas);
			}
			
			if("MXP".equals(cuestionario.getDivisa())) {
				response.setTipoMoneda(Cuestionarios.TIPO_MONEDA_PESOS);
			}
			
			if("USD".equals(cuestionario.getDivisa())) {
				response.setTipoMoneda(Cuestionarios.TIPO_MONEDA_DOLARES);
			}
		}		
		return response;
	}
	
	/**
	 * Agrega el numero de registros de familiares al mapa de preguntas
	 * @param lstFamiliares Lista de familiares
	 * @param respuestas Respuestas del cuestionario
	 */
	private void agregarCampoNumeroRegistrosFamiliares(List<FamiliaresBean> lstFamiliares, Map<String, Object> respuestas){
		if(lstFamiliares != null) {
			respuestas.put(INT_NUMERO_REGISTROS, lstFamiliares.size());
		} else {
			respuestas.put(INT_NUMERO_REGISTROS, 0);
		}
	}
	
	/**
	 * Metodo para validar la seccion de accionistas si es que hay y que tengan un porcentaje mayor a 25%
	 * @param lstAccionistas lista de accionistas obtenidos
	 * @param response dto conrespuesta de validaciones
	 */
	private void validaAccionistas(List<AccionistasBean> lstAccionistas,
			ResponseObtenerCuestionario response) {
		String valor="0";
		if(lstAccionistas!=null && !lstAccionistas.isEmpty()){
			//buscamos los accionistas mayores a 25%
			for (AccionistasBean accionistasBean : lstAccionistas) {
				if(accionistasBean.getStrParticipacionAccionista()!=null){
					try{
					int porcentaje=Integer.parseInt(accionistasBean.getStrParticipacionAccionista());
					if(porcentaje>=25){
						valor="1";
					}
					}catch(NumberFormatException nfe){
						LOG.debug("Error en porcentaje"+nfe.getMessage());
					}
				}
			}
		}
		response.getDatosReporte().put("strAccPoseeParticipacion", valor);
		
	}

	/***
	 * Metodo para obtener el encabezado de cuestionario IC
	 * @param contrato Contrato del cliente
	 * @param query Query para consulta
	 * @return DTO con informacion del encabezado
	 * @throws BusinessException Exception manejada
	 */
	public CuestionarioDTO obtenerEncabezadoIC(String contrato, String query)throws BusinessException {
		return consultarUnico(
			query+contrato, 
			new MapeoConsulta<CuestionarioDTO>(){
				@Override
				public CuestionarioDTO mapeoRegistro(Map<String, Object> registro)
				throws BusinessException {
					CuestionarioDTO cuestionario = new CuestionarioDTO();
					ClienteDTO cliente = new ClienteDTO();
					String fechaCues = getStringValue(registro, "FCH_CREAC");
					String [] fecha;
					String [] year;
					fecha = fechaCues.split("-");
					cuestionario.setAnio(fecha[0]);
					cuestionario.setMes(fecha[1]);
					year = fecha [2].split(ESPACIO); 
					cuestionario.setDia(year[0]);
					cuestionario.setNombreSucursal(getStringValue(registro, "TXT_NOMB_SUCU"));
					cuestionario.setCodigoSucursal(getStringValue(registro, "COD_SUCU"));
					cuestionario.setNombrePlaza(getStringValue(registro, "TXT_NOMB_PLAZA"));
					cuestionario.setNombreRegion(getStringValue(registro, "TXT_NOMB_ZONA"));
					cliente.setNombreCliente(getStringValue(registro,"TXT_NOMB_CLTE"));
					cliente.setApellidoPaterno(getStringValue(registro,"TXT_APE_PAT_CLTE"));
					cliente.setApellidoMaterno(getStringValue(registro,"TXT_APE_MAT_CLTE"));
					cliente.setCodigoCliente(getStringValue(registro, "VAL_BUC"));
					cuestionario.setCliente(cliente);
					cuestionario.setCuentaContrato(getStringValue(registro, "NUM_CNTR"));
					cuestionario.setCodigoTipoFormulario(getStringValue(registro, "VAL_TIPO_CUEST"));
					cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
					cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
					cuestionario.setDivisa(getStringValue(registro, "COD_DIV"));
					cuestionario.setNombreFuncionario(getStringValue(registro, "TXT_NMBR_FUNC"));
					return cuestionario;
				}
			});
	}
	
	/**
	 * 
	 * @param response ResponseObtenerCuestionario
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean
	 */
	public ICListasBean listasIC(ResponseObtenerCuestionario response, Map<String, Object> respuestas)
	{
		ICListasBean listas = new ICListasBean();
		ObtenerCuestionarioWSListas listasPDF = new ObtenerCuestionarioWSListas();
		List<MontosBean> lstOtrosIngresos = new ArrayList<MontosBean>();
		List<ReferenciasBean> lstRefPersonales = new ArrayList<ReferenciasBean>();
		List<SociedadesBean> lstSociedades = new ArrayList<SociedadesBean>();
		List<RefBancariasBean> lstRefBancos = new ArrayList<RefBancariasBean>();
		List<ClientesBean> lstClientes  = new ArrayList<ClientesBean>();
		List<ProveedoresBean> lstProveedores = new ArrayList<ProveedoresBean>();
		lstOtrosIngresos = listasPDF.listasMontos(respuestas);
		lstRefPersonales = listasPDF.listasReferencias(respuestas);
		lstSociedades = listasPDF.listasSociedades(respuestas);
		lstRefBancos = listasPDF.listasBancos(respuestas);
		lstClientes = listasPDF.listasClientes(respuestas);
		lstProveedores = listasPDF.listasProveedores(respuestas);
		listas.setLstOtrosIngresos(lstOtrosIngresos);
		listas.setLstRefPersonales(lstRefPersonales);
		listas.setLstSociedades(lstSociedades);
		listas.setLstRefBancos(lstRefBancos);
		listas.setLstClientes(lstClientes);
		listas.setLstProveedores(lstProveedores);
		return listas;
	}
	
	
	
	/**
	 * Metodo que regresa el tipo de ingreso
	 * @param contrato Contrato cliente
	 * @return Map<String,Object> con resultado
	 * @throws BusinessException  con mensaje de error
	 */
	public Map<String, Object> respuestas (RequestObtenerCuestionario contrato)throws BusinessException
	{
		HashMap<String, Object> respuestas = new HashMap<String, Object>();
		respuestas.putAll(obtenerPreguntasLibresiIC(QRY_PREGUNTAS_LIBRES_IC + creaWhere(contrato)));
		respuestas.putAll(obtenerPreguntasSelectUnicaIC(QRY_PREGUNTAS_SELECT_UNICA_IC+ creaWhere(contrato)));
		respuestas.putAll(obtenerPreguntasRadioIC(QRY_PREGUNTAS_RADIO_IC+ creaWhere(contrato)));
		return respuestas;
	}

	@Override
	public ResponseObtenerCuestionario obtenerDatosNivelRiesgo(
			RequestObtenerCuestionario peticion, ArchitechSessionBean psession)
			throws BusinessException {
		LOG.info("Entrando a obtenerDatosNivelRiesgo ------>");
		ResponseObtenerCuestionario response = new ResponseObtenerCuestionario();
		CuestionarioDTO cuestionario = new CuestionarioDTO();
		
		cuestionario = consultarUnico(String.format(QRY_ENCABEZADO_IP+creaWhere(peticion)), this);
		
		if(cuestionario != null){
			response.setCuestionarioDTO(cuestionario);
			if("IP".equals(cuestionario.getCodigoTipoFormulario()))
			{
				response.setTipo(Cuestionarios.TIPO_IP);
			}
			if("F".equals(cuestionario.getTipoPersona()) && "N".equals(cuestionario.getSubtipoPersona()))
			{
				response.setTipoPersona(Cuestionarios.PERSONA_FISICA);
			}
			else if("F".equals(cuestionario.getTipoPersona()) && "S".equals(cuestionario.getSubtipoPersona()))
			{
				response.setTipoPersona(Cuestionarios.PERSONA_FISICA_AE);
			}
			else if("J".equals(cuestionario.getTipoPersona()) && "N".equals(cuestionario.getSubtipoPersona()))
			{
				response.setTipoPersona(Cuestionarios.PERSONA_MORAL);
			}
			if("MXP".equals(cuestionario.getDivisa()))
			{
				response.setTipoMoneda(Cuestionarios.TIPO_MONEDA_PESOS);
			}
			else if("USD".equals(cuestionario.getDivisa()))
			{
				response.setTipoMoneda(Cuestionarios.TIPO_MONEDA_DOLARES);
			}
		}		
		return response;
	}
}
