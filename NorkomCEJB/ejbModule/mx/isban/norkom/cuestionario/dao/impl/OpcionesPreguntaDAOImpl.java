/**
 * Isban Mexico
 *   Clase: OpcionesPreguntaDAOImpl.java
 *   Descripcion: Componente para consultar las opciones para las preguntas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.OpcionesPreguntaDAO;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;

/**
 * Session Bean implementation class OpcionesPreguntaDAOImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class OpcionesPreguntaDAOImpl extends GenericBdIsbanDA implements OpcionesPreguntaDAO, MapeoConsulta<OpcionesPreguntaDTO> {
       
    /**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 8580298532534542509L;

	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(OpcionesPreguntaDAOImpl.class);
	/**
     * @see Architech#Architech()
     */
    public OpcionesPreguntaDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }



	@Override
	public List<OpcionesPreguntaDTO> obtenerOpcionesPregunta(String idTipo,
			ArchitechSessionBean sesion) throws BusinessException {
		LOG.info(String.format("Ejecutando consulta de obtenerTiposPregunta [%s]", SQL_QUERY_SELECT_OPCIONES_PREGUNTA_BY_TIPO));
		return consultar(String.format(SQL_QUERY_SELECT_OPCIONES_PREGUNTA_BY_TIPO,idTipo), this);
	}
	
	@Override
	public List<OpcionesPreguntaDTO> obtenerOpcionesPorSeccionAbreviatura(String idSeccion, String idAbreviatura, ArchitechSessionBean sesion)
	throws BusinessException {
		LOG.info(String.format("Ejecutando consulta de obtenerTiposPregunta [%s]", SQL_OBTENER_OPCIONES_POR_PREGUNTA));
		return consultar(String.format(SQL_OBTENER_OPCIONES_POR_PREGUNTA, idSeccion, idAbreviatura), this);
	}
    
	
	@Override
	public OpcionesPreguntaDTO mapeoRegistro(Map<String, Object> registro) throws BusinessException {
		OpcionesPreguntaDTO dto= new OpcionesPreguntaDTO();
		dto.setIdOpcion(getStringValue(registro, "CVE_OPC_PREG_PK"));
		dto.setDescripcion(getStringValue(registro, "DSC_OPC"));
		dto.setValor(getStringValue(registro, "VAL_OPC"));
		dto.setGrupoTipo(getStringValue(registro, "CVE_GRPO"));
		return dto;
	}


}
