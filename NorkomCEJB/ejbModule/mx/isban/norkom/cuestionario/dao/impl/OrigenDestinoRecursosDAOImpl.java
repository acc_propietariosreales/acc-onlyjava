/**
 * Isban Mexico
 *   Clase: OrigenDestinoRecursosDAOImpl.java
 *   Descripcion: Componente para consultar informacion del origen y destino de los recursos.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.OrigenDestinoRecursosDAO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class OrigenDestinoRecursosDAOImpl extends GenericBdIsbanDA implements OrigenDestinoRecursosDAO, MapeoConsulta<RecursoDTO> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 7572329336807941707L;
	
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(OrigenDestinoRecursosDAOImpl.class);

    /**
     * CONSTRUCTOR del dao Origen y destino de recursos
     */
    public OrigenDestinoRecursosDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    
    @Override
	public List<RecursoDTO> obtenerRecursos(String tipoPersona, String tipoRecurso, ArchitechSessionBean sesion)
	throws BusinessException {
		LOG.info(String.format("Ejecutando consulta de obtenerRecursos [%s]", SQL_OBTENER_RECURSOS));
		return consultar(String.format(SQL_OBTENER_RECURSOS, tipoPersona, tipoRecurso), this);
	}
    
    @Override
    public RecursoDTO obtenerRecursoPorClave(int claveRecurso, ArchitechSessionBean sesion)
    		throws BusinessException {
    	LOG.info(String.format("Ejecutando consulta de obtenerRecursos [%s]", SQL_OBTENER_RECURSO_POR_CLAVE));
    	return consultarUnico(String.format(SQL_OBTENER_RECURSO_POR_CLAVE, claveRecurso), this);
    }
	
	@Override
	public RecursoDTO mapeoRegistro(Map<String, Object> registro) throws BusinessException {
		RecursoDTO recursoDto = new RecursoDTO();
		
		recursoDto.setClaveRecurso(getIntegerValue(registro, "CVE_REC_PK"));
		recursoDto.setDescripcion(getStringValue(registro, "DESCRIPCION"));
		recursoDto.setTipoRecurso(getStringValue(registro, "FLG_TIPO_REC"));
		recursoDto.setTipoPersona(getStringValue(registro, "FLG_TIPO_PERS"));
		recursoDto.setCodigoRecurso(getStringValue(registro, "COD_REC"));
		
		return recursoDto;
	}
}
