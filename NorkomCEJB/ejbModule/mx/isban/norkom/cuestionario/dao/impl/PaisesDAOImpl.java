/**
 * Isban Mexico
 *   Clase: PaisesDAOImpl.java
 *   Descripcion: Componente para consultar informacion de los paises.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.PaisDAO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PaisesDAOImpl extends GenericBdIsbanDA implements PaisDAO, MapeoConsulta<PaisDTO> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 6636154044978477836L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(PaisesDAOImpl.class);

    /**
     * CONSTRUCTOR
     */
    public PaisesDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    
    @Override
	public List<PaisDTO> obtenerPaises(ArchitechSessionBean sesion) throws BusinessException {
		LOG.info(String.format("Ejecutando consulta de obtenerPaises [%s]", SQL_OBTENER_PAIS));
		return consultar(SQL_OBTENER_PAIS, this);
	}
    
    @Override
    public PaisDTO obtenerPaisPorCodigo(String codigoPais, ArchitechSessionBean sesion) throws BusinessException {
    	LOG.info(String.format("Ejecutando consulta de obtenerPaisPorCodigo [%s][%s]", 
    							SQL_OBTENER_PAIS_POR_CODIGO, codigoPais));
    	return consultarUnico(String.format(SQL_OBTENER_PAIS_POR_CODIGO, codigoPais), this);
    }
	
	@Override
	public PaisDTO mapeoRegistro(Map<String, Object> registro) throws BusinessException {
		PaisDTO paisDTO = new PaisDTO();
		
		paisDTO.setIdPais(Integer.parseInt(getStringValue(registro, "CVE_PAIS_PK")));
		paisDTO.setCodigo(getStringValue(registro, "COD_PAIS"));
		paisDTO.setNombre(getStringValue(registro, "TXT_NOMB"));
		paisDTO.setIndicadorRiesgo(getStringValue(registro, "FLG_RIESG"));
		
		return paisDTO;
	}

}
