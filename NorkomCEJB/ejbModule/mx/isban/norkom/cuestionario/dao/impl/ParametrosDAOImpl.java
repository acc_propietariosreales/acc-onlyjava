/**
 * Isban Mexico
 *   Clase: ParametrosDAOImpl.java
 *   Descripcion: Componente para obtener los parametros del cuestionario.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.ParametrosDAO;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.log4j.Logger;


/**
 * Session Bean implementation class ActividadEconomicaDAOImpl
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametrosDAOImpl extends GenericBdIsbanDA implements ParametrosDAO, MapeoConsulta<ParametroDTO>  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4764725976960825246L;
	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(ParametrosDAOImpl.class);

	/**
     * @see Architech#Architech()
     */
    public ParametrosDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
    /**
     * @param nombreParametro nombre del parametro
     * @return ParametroDTO con resultado de operacion
     * @throws BusinessException con mensaje de error
     */
    public ParametroDTO obtenerParametroPorNombre(String nombreParametro) throws BusinessException {
    	LOG.info(String.format("Obteniendo Parametro por Nombre [%s]", nombreParametro));
    	return consultarUnico(String.format(SQL_OBTENER_PARAMETRO_POR_NOMBRE, nombreParametro, IND_ACTIVO), this);
    }
    /**
     * @return List<ParametroDTO> con resultado
     * @throws BusinessException  con mensaje de error
     */
    public List<ParametroDTO> obtenerParametros() throws BusinessException {
    	LOG.info("Obteniendo Parametros Activos");
    	return consultar(String.format(SQL_OBTENER_PARAMETROS_ACTIVOS, IND_ACTIVO), this);
    }
    
	@Override
	public ParametroDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		ParametroDTO parametroDTO = new ParametroDTO();
		
		parametroDTO.setNombre(getStringValue(registro, "TXT_NOMBR"));
		parametroDTO.setValor(getStringValue(registro, "VAL_PARAM"));
		parametroDTO.setActivo(getIntegerValue(registro, "FLG_ACTIV") == IND_ACTIVO ? true : false);
		
		return parametroDTO;
	}

}