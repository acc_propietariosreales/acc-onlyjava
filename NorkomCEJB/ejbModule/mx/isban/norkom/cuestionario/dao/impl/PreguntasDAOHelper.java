/**
 * Isban Mexico
 *   Clase: PreguntasDAOHelper.java
 *   Descripcion: Auxiliar para las operaciones de Preguntas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.FamiliarDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;

import org.apache.log4j.Logger;

/**
 * Clase auxiliar para las operaciones de Preguntas
 * @author jugarcia
 *
 */
public class PreguntasDAOHelper extends GenericBdIsbanDA{

	/**
	 * serialVersionUID  de tipo long
	 */
	private static final long serialVersionUID = -5002115120742892543L;

	/**
	 * Componente para el log
	 */
	private static final Logger LOG = Logger.getLogger(PreguntasDAOHelper.class);

	/**
	 * Variable utilizada para declarar SQL_AGREGAR_REFERENCIA_CUES_RESP
	 */
	private static final String SQL_AGREGAR_REFERENCIA_CUES_RESP = new StringBuilder()
		.append("INSERT INTO NKM_MX_REL_CUEST_RESP_DRO(CVE_CUEST_RESP_PK, CVE_CLTE_CUEST_FK, CVE_CUEST_FK) ")
		.append("VALUES(%s, %s, %s)")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_AGREGAR_RESP_PREG
	 */
	private static final String SQL_AGREGAR_RESP_PREG = new StringBuilder()
		.append("INSERT INTO NKM_MX_PRC_RESP_PREG_DRO(CVE_RESP_PREG_PK, CVE_PREG_FK, TXT_RESP, VAL_OPCN, CVE_CUEST_RESP_FK) ")
		.append("VALUES(%s, %s, '%s', %s, %s)")
		.toString();
	
	/**
	 * Variable utilizada para declarar SQL_AGREGAR_FAMILIAR
	 */
	private static final String SQL_AGREGAR_FAMILIAR = new StringBuilder()
		.append("INSERT INTO NKM_MX_AUX_FAM_DRO(CVE_FAM_PK, TXT_NOMB_FAM, TXT_DOMIC_FAM, TXT_FCH_NAC, TXT_PAREN, CVE_GRPO_FK, CVE_CLTE_CUEST_FK) ")
		.append("VALUES(%s, '%s', '%s', to_date('%s','dd/mm/yyyy'), '%s', %s, %s)")
		.toString();
	/**Query para borrar preguntas ip**/
	private static final String SQL_DEL_PREGUNTAS_IP_BY_CVE_CUEST_RESP="delete from NKM_MX_PRC_RESP_PREG_DRO where CVE_CUEST_RESP_FK=%s";
	/**Query para buscar la cleve cuesResp segun el idcuest y tip cuest**/
	private static final String SQL_BUSCAR_CVE_CUES_RESP="select CVE_CUEST_RESP_PK from NKM_MX_REL_CUEST_RESP_DRO where  CVE_CLTE_CUEST_FK=%s and CVE_CUEST_FK=%s";
	
	/**
	 * Constructor
	 * @param canal IDACanal
	 */
	protected PreguntasDAOHelper(IDACanal canal) {
		super(canal);
	}
	/**
	 * Ejecuta la insercion de las respuestas de un cuestionario
	 * @param cuestionario Cuestionario al que son asignadas las respuestas
	 * @param claveCuestionarioRespuesta Clave de la relacion entre el cuestionario y las respuestas
	 * @param requestDTO DTO de peticion
	 * @param dataAccess Objeto de acceso a los datos
	 * @throws BusinessException En caso de un error con las operaciones de negocio
	 * @throws ExceptionDataAccess con mensaje de error
	 */
	public void ejecutarInsercionCuestionarioRespuesta(CuestionarioDTO cuestionario, 
			int claveCuestionarioRespuesta, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess)
	throws BusinessException, ExceptionDataAccess {
		requestDTO.setQuery(String.format(
				SQL_AGREGAR_REFERENCIA_CUES_RESP, 
				claveCuestionarioRespuesta, cuestionario.getClaveClienteCuestionario(), cuestionario.getClaveCuestionario()));
		
		requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
		ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
				dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
		
		if (response == null) {
			LOG.error("No se obtuvo respuesta de la BD");
			throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
		}
	}
	
	/**
	 * @param respuestas lista de respuestas
	 * @param requestDTO bean con datos para la insercion
	 * @param dataAccess instancia de IDA para transaccionalidad
	 * @throws BusinessException con mensaje de error
	 * @throws ExceptionDataAccess con mensaje de error
	 */
	public void ejecutarInsercionRespuestas(
			List<CuestionarioRespuestaDTO> respuestas, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess)
	throws BusinessException, ExceptionDataAccess {
		
		for(CuestionarioRespuestaDTO respuesta : respuestas){
			requestDTO.setQuery(String.format(
					SQL_AGREGAR_RESP_PREG, 
					respuesta.getClaveRespuestaPregunta(), respuesta.getClavePregunta(), respuesta.getValorTexto(), 
					respuesta.getValorOpcion(), respuesta.getClaveCuestionarioResp()));
			
			requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
			ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
					dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
			
			if (response == null) {
				LOG.error("No se obtuvo respuesta de la BD");
				throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
			}
		}
		
	}
	
	/**
	 * @param cuestionario datos del cuestionario
	 * @param requestDTO DTO con datos de busqueda
	 * @param dataAccess referencia de dataacces para transaccionalidad
	 * @throws BusinessException con mensaje de error
	 * @throws ExceptionDataAccess con mensaje de error
	 */
	public void ejecutarInsercionFamiliares(
			CuestionarioDTO cuestionario, RequestMessageDataBaseDTO requestDTO, DataAccessDataBase dataAccess)
	throws BusinessException, ExceptionDataAccess {
		if(cuestionario.getFamiliares() != null && !cuestionario.getFamiliares().isEmpty()) {
			for(FamiliarDTO respuesta : cuestionario.getFamiliares()){
				requestDTO.setQuery(String.format(
						SQL_AGREGAR_FAMILIAR, 
						respuesta.getClaveFamiliar(), respuesta.getNombre(), respuesta.getDomicilio(), 
						respuesta.getFechaNacimiento(), respuesta.getParentesco(), 22, cuestionario.getClaveClienteCuestionario()));
				
				requestDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
				ResponseMessageDataBaseDTO response = (ResponseMessageDataBaseDTO) 
						dataAccess.execute(IDACanal.CANAL_DB_NORKOM.toString());
				
				if (response == null) {
					LOG.error("No se obtuvo respuesta de la BD");
					throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
				}
			}
		}
	}
	/**
	 * metodo para el borrado de respuestas de cuestionario por clave de cuestionario respuesta
	 * @param claveCuestionarioResp clave de cuestionario rspuesta para borrar sus respuestas
	 * @throws BusinessException con mensaje de error
	 * @throws ExceptionDataAccess con mensaje de error
	 */
	public void borrarRespuestasIP(int claveCuestionarioResp) throws BusinessException, ExceptionDataAccess {
		delete(String.format(
				SQL_DEL_PREGUNTAS_IP_BY_CVE_CUEST_RESP,claveCuestionarioResp));
	}
	/***
	 * 
	 * @param claveClienteCuestionario calve del cuestionario
	 * @param claveCuestionario tipo de cuestionario
	 * @throws BusinessException con mensaje de error
	 * @return id de cveCuestResp
	 */
	public BigDecimal obtenerCveCuestResp(Integer claveClienteCuestionario,Integer claveCuestionario)throws BusinessException {
		return consultarBigDecimal(String.format(SQL_BUSCAR_CVE_CUES_RESP,claveClienteCuestionario,claveCuestionario));
		
	}
}
