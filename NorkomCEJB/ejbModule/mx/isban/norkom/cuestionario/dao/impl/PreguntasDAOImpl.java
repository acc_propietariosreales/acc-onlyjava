/**
 * Isban Mexico
 *   Clase: CuestionarioDAOImpl.java
 *   Descripcion: Componente para obtener informacion de los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.norkom.cuestionario.dao.PreguntasDAO;
import mx.isban.norkom.cuestionario.dao.mapeo.PreguntaMapper;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PreguntasDAOImpl extends PreguntasDAOHelper implements PreguntasDAO {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 3381004753618377197L;
	
	/**
	 * String para indicar el campo FLG_NUMER
	 */
	private static final String STR_FLG_NUMER = "FLG_NUMER";
	
	/**
	 * Constructor CuestionarioDAOImpl
	 */
	public PreguntasDAOImpl() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasCuestionarioPorClave(int claveCuestionario, ArchitechSessionBean psession)
	throws BusinessException {
		return consultar(
			String.format(SQL_OBTENER_PREGUNTAS_POR_ID_CUESTIONARIO, claveCuestionario), 
			new PreguntaMapper());
	}
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasAsignadasPorAbreviaturas(String idCuestionario, String abreviaturas, ArchitechSessionBean psession)
	throws BusinessException {
		return consultar(
			String.format(SQL_OBTENER_PREGUNTAS_ASIGNADAS_POR_ABREVIATURAS, idCuestionario, abreviaturas), 
			new PreguntaMapper());
	}
	
	@Override
	public List<PreguntaDTO> obtenerPreguntasPorAbreviaturas(String abreviaturas, ArchitechSessionBean psession)
	throws BusinessException {
		return consultar(
				String.format(SQL_OBTENER_PREGUNTAS_POR_ABREV, abreviaturas), 
				new MapeoConsulta<PreguntaDTO>(){
					@Override
					public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
							throws BusinessException {
						final PreguntaDTO preguntaDTO = new PreguntaDTO();
						
						preguntaDTO.setIdPregunta(getIntegerValue(registro,"CVE_PREG_PK"));
						preguntaDTO.setTitulo(getStringValue(registro,"TXT_TITUL"));
						preguntaDTO.setPregunta(getStringValue(registro,"TXT_PREG"));
						preguntaDTO.setIdTipoPregunta(getStringValue(registro,"CVE_TIPO_PREG_FK"));
						preguntaDTO.setTipoPregunta(getStringValue(registro, "COD_TIPO_PREG"));
						preguntaDTO.setClaveGrupoPregunta(getStringValue(registro,"CVE_GRPO_FK"));
						preguntaDTO.setSeccion(getStringValue(registro,"TXT_SECCN"));
						preguntaDTO.setAbreviatura(getStringValue(registro, "TXT_ABRV"));
						preguntaDTO.setLongitudMaxima(getIntegerValue(registro, "VAL_LONG_RESP"));
						preguntaDTO.setCampoNumerico(getIntegerValue(registro, STR_FLG_NUMER) == 1 ? true : false);
						preguntaDTO.setCampoFecha(getIntegerValue(registro, STR_FLG_NUMER) == 2 ? true : false);
						
						return preguntaDTO;
					}
				});
	}
	
	@Override
	public CuestionarioRespuestaDTO obtenerPreguntaPorSeccionAbreviatura(String idCuestionario, String seccion, String abreviatura, ArchitechSessionBean psession)
	throws BusinessException {
		return consultarUnico(
				String.format(SQL_OBTENER_RESP_PREG_POR_SECCION_ABREVIATURA, idCuestionario, seccion, abreviatura), 
				new MapeoConsulta<CuestionarioRespuestaDTO>(){
					@Override
					public CuestionarioRespuestaDTO mapeoRegistro(Map<String, Object> registro)
							throws BusinessException {
						final CuestionarioRespuestaDTO preguntaDTO = new CuestionarioRespuestaDTO();
						
						preguntaDTO.setClavePregunta(getIntegerValue(registro, "CVE_PREG_PK"));
						preguntaDTO.setValorOpcion(getIntegerValue(registro, "VAL_OPCN"));
						preguntaDTO.setValorTexto(getStringValue(registro, "TXT_RESP"));
						
						return preguntaDTO;
					}
				});
	}
	
	@Override
	public int numeroPreguntasEnCuestionario(String idCuestionario) throws BusinessException {
		return consultarEntero(String.format(SQL_NUMERO_PREGUNTAS_CUESTIONARIO, idCuestionario));
	}
	
	@Override
	public void agregarRespuestasCuestionario(
			CuestionarioDTO cuestionario, List<CuestionarioRespuestaDTO> respuestas, int claveCuestionarioRespuesta, ArchitechSessionBean sesion)
	throws BusinessException {
		RequestMessageDataBaseDTO requestDTO = new RequestMessageDataBaseDTO();
		DataAccessDataBase dataAccess = obtenerDataAccess(requestDTO);

		try {
			if(cuestionario.isRegSimp() && !respuestas.isEmpty()){
				claveCuestionarioRespuesta=obtenerCveCuestResp(cuestionario.getClaveClienteCuestionario(),cuestionario.getClaveCuestionario()).intValue();
				borrarRespuestasIP(claveCuestionarioRespuesta);
				for (CuestionarioRespuestaDTO cuestionarioRespuestaDTO : respuestas) {
					cuestionarioRespuestaDTO.setClaveCuestionarioResp(claveCuestionarioRespuesta);
				}
			}
			dataAccess.beginTransaction(IDACanal.CANAL_DB_NORKOM.getNombre());
			if(!cuestionario.isRegSimp()){//ya se habian registrado al ser RS
				ejecutarInsercionCuestionarioRespuesta(cuestionario, claveCuestionarioRespuesta, requestDTO, dataAccess);
			}
			
			ejecutarInsercionRespuestas(respuestas, requestDTO, dataAccess);
			ejecutarInsercionFamiliares(cuestionario, requestDTO, dataAccess);
			
			dataAccess.commitTransaction();
		} catch (ExceptionDataAccess e) {
			error(String.format(
					"Ocurrio un error durante la insercion del cuestionario ::: [%s][%s]", 
					e.getMessage(), e.getCause()));
			error(e.toString());
			dataAccess.rollbackTransaction();
			throw new mx.isban.norkom.cuestionario.exception.BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo(),Mensajes.ERROR_COMUNICACION.getDescripcion(),e);
		}
	}


	
	@Override
	public void ejecutarActualizacionRespuesta(CuestionarioRespuestaDTO respuesta)
	throws BusinessException {
		update(String.format(SQL_ACTUALIZAR_RESP_PREG, 
				respuesta.getValorTexto(), respuesta.getValorOpcion(), respuesta.getClaveRespuestaPregunta()));
	}
}
