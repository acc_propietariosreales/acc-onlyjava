/**
 * Isban Mexico
 *   Clase: RelacionadosBloqueDAOImpl.java
 *   Descripcion: Componente para insertar nuevos relacionados
 *   por bloque
 *
 *   Control de Cambios:
 *   1.0 Jun 20, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dao.RelacionadosBloqueDAO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.EstatusRelacionados;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
/**
 * Dao para el acceso a los relacionados
 * por bloque extiende de GenericBdIsbanDA
 * e implementa RelacionadosBloqueDAO
 * los metodos principales son los de:
 *  agregarRelacionados(List<RelacionadoDTO>, int, String, ArchitechSessionBean)
 *  agregarCuestionarioBasico(CuestionarioDTO, String, ArchitechSessionBean)
 *  ver detlle en cada metodo
 * @author lespinosa
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class RelacionadosBloqueDAOImpl extends GenericBdIsbanDA implements  RelacionadosBloqueDAO {

	/**
	 * Variable utilizada para declarar 
	 * serialVersionUID de tipo
	 * long
	 */
	private static final long serialVersionUID = 733916456111398542L;
	/**
	 * Variable utilizada para declarar 
	 * SQL_AGREGAR_RELACIONADO
	 * para agregar relacionados por bloque
	 */
	private static final String SQL_AGREGAR_RELACIONADO = new StringBuilder()
	.append("INSERT INTO NKM_MX_AUX_RELAC_DRO(CVE_RELAC_PK, COD_TIPO_RELAC, TXT_NOMB_RELAC, TXT_APE_PAT_RELAC, TXT_APE_MAT_REL, FCH_NAC_REL, ")
	.append("COD_TIPO_PERS_REL, COD_PAIS_NACIO_REL, COD_PAIS_RESID_REL, POR_PART, CVE_CLTE_CUES_FK, TXT_EST_REL, NUM_BLOQUE) ")
	.append("VALUES(SEQ_PM_RELA.NEXTVAL, '%s', '%s', '%s', '%s', TO_DATE('%s', 'YYYY-MM-DD'), '%s', '%s', '%s', %s, %s, '%s', %s)")
	.toString();

	/**
	 * Variable utilizada para declarar 
	 * SQL_AGREGAR_CUESTIONARIO
	 * para agregar un cuestionario basico
	 */
	private static final String SQL_AGREGAR_CUESTIONARIO_BASICO = new StringBuilder()
			.append("INSERT INTO NKM_MX_MAE_CLTE_CUEST_DRO(CVE_CLTE_CUEST_PK, CVE_CUEST_IP_FK, ID_CUEST, COD_TIPO_PERS, COD_SUB_TIPO_PERS, COD_DIV_PROD, FCH_CREAC,CVE_CLTE_FK) ")
			.append("VALUES(%s, %s, '%s', '%s', '%s', '%s', sysdate, (select CVE_CLTE_PK FROM NKM_MX_MAE_CLTE_DRO where VAL_BUC='%s') )")
			.toString();
	

	
	/**
	 * constructor de BloqueDAO
	 * para agregar el canal de
	 * Norkom en el cual se 
	 * realizaran las operaciones
     * @see Architech#Architech()
     */
    public RelacionadosBloqueDAOImpl() {
    	super(IDACanal.CANAL_DB_NORKOM);
    }
	
	@Override
	public List<RelacionadoDTO> agregarRelacionados(List<RelacionadoDTO> relacionadosWS, int claveCuestionario,String bloque, ArchitechSessionBean psession) 
	throws BusinessException {
		List<RelacionadoDTO> relacionadosErroneos = new ArrayList<RelacionadoDTO>();

			
		for(RelacionadoDTO relacionado : relacionadosWS){
			try {
				String sentenciaInsercion = String.format(
						SQL_AGREGAR_RELACIONADO,
						relacionado.getTipoRelacion(),
						relacionado.getNombre(),
						relacionado.getApellidoPaterno(),
						relacionado.getApellidoMaterno(),
						relacionado.getFechaNacimiento(),
						relacionado.getTipoPersona(),
						relacionado.getCodPaisNacionalidad(),
						relacionado.getCodPaisResidencia(),
						relacionado.getPorcentajeParticipacion(),
						claveCuestionario,
						EstatusRelacionados.RECIBIDO.toString(),
						bloque);
				
				insert(sentenciaInsercion);
				
				
			} catch(BusinessException be){
				showException(be);
				relacionado.setCodigoOperacion(Mensajes.ERRLBD01.getCodigo());
				relacionado.setDescOperacion(Mensajes.ERRLBD01.getDescripcion());
				relacionadosErroneos.add(relacionado);
			}
		}
		
		return relacionadosErroneos;
	}
	
	@Override
	public void agregarCuestionarioBasico(CuestionarioDTO cuestionario,String buc, ArchitechSessionBean sesion)
	throws BusinessException {
		update(formarSentenciaInsercionCuestionarioBasico(cuestionario,buc));
	}
	/**
	 * Genera sentencia SQL de insercion de cuestionario 
	 * con la informacion minima requerida incluyendo el 
	 * buc del usuario que este ya se proporciona
	 * en el WS de relacionados por bloque
	 * @param cuestionario Contiene la informacion 
	 * para generar la sentencia SQL
	 * @return String Sentencia SQL para agregar un cuestionario
	 */
	private String formarSentenciaInsercionCuestionarioBasico(CuestionarioDTO cuestionario,String buc){
		return String.format(SQL_AGREGAR_CUESTIONARIO_BASICO,
				cuestionario.getClaveClienteCuestionario(),
				cuestionario.getClaveCuestionario(),
				cuestionario.getIdCuestionario(),
				cuestionario.getTipoPersona(),
				cuestionario.getSubtipoPersona(),
				cuestionario.getDivisa(),buc
		);
	}


}
