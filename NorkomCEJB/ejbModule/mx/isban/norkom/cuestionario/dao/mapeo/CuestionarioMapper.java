/**
 * Isban Mexico
 *   Clase: CuestionarioMapper.java
 *   Descripcion: Componente para mapear cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.mapeo;

import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * Define el mapeo de la base de datos al objeto CuestionarioDTO
 * @author jugarcia
 *
 */
public class CuestionarioMapper extends GenericBdIsbanDA implements MapeoConsulta<CuestionarioDTO> {
	/**
	 * Id utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = -3091193940289626044L;

	/**
	 * Constructor en donde se define el canal de conexion
	 */
	public CuestionarioMapper() {
		super(IDACanal.CANAL_DB_NORKOM);
	}

	@Override
	public CuestionarioDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		final CuestionarioDTO cuestionario = new CuestionarioDTO();
		final ActividadEconomicaDTO actividadEspecifica = new ActividadEconomicaDTO();
		
		cuestionario.setActividadEspecifica(actividadEspecifica);
		
		cuestionario.setClaveCuestionario(getIntegerValue(registro, "CVE_CLTE_CUEST_PK"));
		cuestionario.setIdCuestionario(getStringValue(registro, "ID_CUEST"));
		cuestionario.setIdAplicacion(getStringValue(registro, "ID_APP"));
		cuestionario.setNumeroContrato(getStringValue(registro, "NUM_CNTR"));
		cuestionario.setFechaCreacion(getStringValue(registro, "FCH_CREAC"));
		cuestionario.setCodigoProducto(getStringValue(registro, "COD_PROD"));
		cuestionario.setDescProducto(getStringValue(registro, "DSC_PROD"));
		cuestionario.setCodigoSubproducto(getStringValue(registro, "COD_SUB_PROD"));
		cuestionario.setDescSubproducto(getStringValue(registro, "DSC_SUB_PROD"));
		cuestionario.setDescSegmento(getStringValue(registro, "DSC_SEGMT"));
		cuestionario.setCodigoPais(getStringValue(registro, "COD_PAIS_RESID"));
		cuestionario.setCodigoNacionalidad(getStringValue(registro, "COD_NACIO"));
		cuestionario.setNivelRiesgo(getStringValue(registro, "COD_NIVEL_RIESG"));
		cuestionario.setIndicadorUpld(getStringValue(registro, "COD_IND_UPLD"));
		actividadEspecifica.setCodigo(getStringValue(registro, "CVE_ACTIV_ESP"));
		
		return cuestionario;
	}
}
