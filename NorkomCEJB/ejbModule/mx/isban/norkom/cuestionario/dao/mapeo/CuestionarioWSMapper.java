/**
 * Isban Mexico
 *   Clase: CuestionarioWSMapper.java
 *   Descripcion: Componente WS para mapear cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.mapeo;

import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ClienteDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

/**
 * 
 * @author jugarcia
 * Objetivo: Definer el mapeo de la base de datos al objeto CuestionarioWSDTO
 * Justificacion: Reutilizar la funcionalidad de mapear la respuesta de base de datos (tabla NKM_MX_MAE_CLTE_CUEST_DRO)
 */
public class CuestionarioWSMapper extends GenericBdIsbanDA implements MapeoConsulta<CuestionarioWSDTO> {
	/**
	 * Id utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = -3091193940289626044L;

	/**
	 * Constructor en donde se define el canal de conexion
	 */
	public CuestionarioWSMapper() {
		super(IDACanal.CANAL_DB_NORKOM);
	}

	@Override
	public CuestionarioWSDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		final CuestionarioWSDTO cuestionario = new CuestionarioWSDTO();
		final ClienteDTO cliente = new ClienteDTO();
		cuestionario.setCliente(cliente);
		
		cuestionario.setClaveCuestionario(getIntegerValue(registro, "CVE_CLTE_CUEST_PK"));
		cuestionario.setIdCuestionario(getStringValue(registro, "ID_CUEST"));
		cuestionario.setIdAplicacion(getStringValue(registro, "ID_APP"));
		cuestionario.setNumeroContrato(getStringValue(registro, "NUM_CNTR"));
		cuestionario.setFechaCreacion(getStringValue(registro, "FCH_CREAC"));
		cuestionario.setCodigoProducto(getStringValue(registro, "COD_PROD"));
		cuestionario.setDescProducto(getStringValue(registro, "DSC_PROD"));
		cuestionario.setCodigoSubproducto(getStringValue(registro, "COD_SUB_PROD"));
		cuestionario.setDescSubproducto(getStringValue(registro, "DSC_SUB_PROD"));
		cuestionario.setDescSegmento(getStringValue(registro, "DSC_SEGMT"));
		cuestionario.setCodigoPais(getStringValue(registro, "COD_PAIS_RESID"));
		cuestionario.setCodigoNacionalidad(getStringValue(registro, "COD_NACIO"));
		cuestionario.setClaveCuestionarioIP(getIntegerValue(registro, "CVE_CUEST_IP"));
		cuestionario.setNivelRiesgo(getStringValue(registro, "CVE_CUEST_IP"));
		cuestionario.setIndicadorUpld(getStringValue(registro, "COD_IND_UPLD"));
		cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
		cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
		cliente.setIdCliente(getIntegerValue(registro, "CVE_CLTE_FK"));
		
		return cuestionario;
	}
}
