/**
 * Isban Mexico
 *   Clase: PreguntaMapper.java
 *   Descripcion: Componente para mapear preguntas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.mapeo;

import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * Define el mapeo de las preguntas en la base de datos al objeto PreguntaDTO
 * @author jugarcia
 *
 */
public class PreguntaMapper extends GenericBdIsbanDA implements MapeoConsulta<PreguntaDTO> {
	/**
	 * Id utilizado para la serializacion del objeto
	 */
	private static final long serialVersionUID = 4683801139094981032L;
	
	/**
	 * String para indicar el campo FLG_NUMER
	 */
	private static final String STR_FLG_NUMER = "FLG_NUMER";

	/**
	 * Constructor en donde se define el canal de conexion
	 */
	public PreguntaMapper() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	
	@Override
	public PreguntaDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		final PreguntaDTO preguntaDTO = new PreguntaDTO();
		
		preguntaDTO.setIdPregunta(getIntegerValue(registro,"CVE_PREG_PK"));

		Integer claveRespuestaPregunta = getIntegerValue(registro,"CVE_RESP_PREG_PK");
		if(claveRespuestaPregunta != null){
			preguntaDTO.setClaveRespuestaPregunta(claveRespuestaPregunta.intValue());
		}
		
		preguntaDTO.setTitulo(getStringValue(registro,"TXT_TITUL"));
		preguntaDTO.setPregunta(getStringValue(registro,"TXT_PREG"));
		preguntaDTO.setTipoPregunta(getStringValue(registro,"COD_TIPO_PREG"));
		preguntaDTO.setIdTipoPregunta(getStringValue(registro,"CVE_TIPO_PREG_FK"));
		preguntaDTO.setClaveGrupoPregunta(getStringValue(registro,"CVE_GRPO_PK"));
		preguntaDTO.setDescripcionGrupoPregunta(getStringValue(registro, "DSC_GRPO"));
		preguntaDTO.setSeccion(getStringValue(registro,"TXT_SECCN"));
		preguntaDTO.setAbreviatura(getStringValue(registro, "TXT_ABRV"));
		preguntaDTO.setLongitudMaxima(getIntegerValue(registro, "VAL_LONG_RESP"));
		preguntaDTO.setCampoNumerico(getIntegerValue(registro, STR_FLG_NUMER) == 1 ? true : false);
		preguntaDTO.setCampoFecha(getIntegerValue(registro, STR_FLG_NUMER) == 2 ? true : false);
		
		return preguntaDTO;
	}
}
