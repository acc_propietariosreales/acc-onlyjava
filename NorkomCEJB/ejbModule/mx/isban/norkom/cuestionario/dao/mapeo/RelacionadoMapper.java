/**
 * RelacionadoMapper
 * Mapper para el dao de Envio de relacionados
 * para obtener los relacionados
 */
package mx.isban.norkom.cuestionario.dao.mapeo;

import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerRelacionados;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * Mapper para los datos de los relacionados
 * en el flujo de Relacionados no OK 
 * por Norkom
 * @author lespinosa
 *
 */
public class RelacionadoMapper extends GenericBdIsbanDA implements MapeoConsulta<ResponseObtenerRelacionados>{

	/**
	 * Serial UID
	 * Version de tipo long
	 */
	private static final long serialVersionUID = 6257917946748098613L;
	/**
	 * Constructor en donde se define el
	 *  canal de conexion
	 */
	public RelacionadoMapper() {
		super(IDACanal.CANAL_DB_NORKOM);
	}
	@Override
	public ResponseObtenerRelacionados mapeoRegistro(Map<String, Object> registro)
			throws BusinessException {
		ResponseObtenerRelacionados relacionados= new ResponseObtenerRelacionados();
		relacionados.setIdCuestionario(getStringValue(registro, "ID_CUEST"));
		relacionados.setEstatusRelacion(getStringValue(registro, "TXT_EST_REL"));
		relacionados.setNumeroBloque(getStringValue(registro, "NUM_BLOQUE"));	
		return relacionados;
	}

}
