/**
 * Isban Mexico
 *   Clase: TipoCuestionarioMapper.java
 *   Descripcion: Componente para mapear cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dao.mapeo;

import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.isbanda.GenericBdIsbanDA;
import mx.isban.norkom.isbanda.MapeoConsulta;

/**
 * Define el mapeo del Tipo de Cuetionario en la base de datos al objeto CuestionarioDTO
 * @author jugarcia
 *
 */
public class TipoCuestionarioMapper extends GenericBdIsbanDA implements MapeoConsulta<CuestionarioDTO> {
	/**
	 * Id utilizado para la serializacion del objeto
	 */
	private static final long serialVersionUID = 1498171382488093069L;

	/**
	 * Constructor en donde se define el canal de conexion
	 */
	public TipoCuestionarioMapper() {
		super(IDACanal.CANAL_DB_NORKOM);
	}

	@Override
	public CuestionarioDTO mapeoRegistro(Map<String, Object> registro)
	throws BusinessException {
		final CuestionarioDTO cuestionario = new CuestionarioDTO();
		
		cuestionario.setClaveCuestionario(getIntegerValue(registro,"CVE_CUEST_PK"));
		cuestionario.setNombreCuestionario(getStringValue(registro,"TXT_NOMB"));
		cuestionario.setDescripcion(getStringValue(registro,"DSC_CUEST"));
		cuestionario.setNivelRiesgo(getStringValue(registro,"VAL_TIPO_CUEST"));
		cuestionario.setDivisa(getStringValue(registro, "COD_DIV"));
		cuestionario.setTipoPersona(getStringValue(registro, "COD_TIPO_PERS"));
		cuestionario.setSubtipoPersona(getStringValue(registro, "COD_SUB_TIPO_PERS"));
		cuestionario.setVersionCuestionario(getStringValue(registro, "NUM_VERSN"));
		cuestionario.setFechaCreacion(getStringValue(registro,"FECHA"));
		
		return cuestionario;
	}
}
