/**
 * NorkomCEJBClient ObtenerAccionistasDTO.java
 *  Clase: ObtenerAccionistasDTO.java
 *   Descripcion: DTO para almacenar datos de accionistas
 */
package mx.isban.norkom.cuestionario.dto;

import java.util.ArrayList;
import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.util.Utilerias;
import mx.isban.norkom.isbanda.MapeoConsulta;

import org.apache.commons.lang.StringUtils;

/**
 * @author Leopoldo F Espinosa R 01/04/2015
 *
 */
public class ObtenerAccionistasDTO implements MapeoConsulta<RespuestaListaDTO>{

	@Override
	public RespuestaListaDTO mapeoRegistro(Map<String, Object> registro)
			throws BusinessException {
		RespuestaListaDTO datos= new RespuestaListaDTO();
		datos.setDatos(new ArrayList<String>());
		datos.getDatos().add(new StringBuilder().append(getStringValue(registro, "TXT_NOMB_RELAC"))
				.append(" ")
				.append(getStringValue(registro, "TXT_APE_PAT_RELAC"))
				.append(" ")
				.append(getStringValue(registro, "TXT_APE_MAT_REL"))
				.toString());
		datos.getDatos().add(getStringValue(registro, "FCH_NAC_REL"));
		datos.getDatos().add(getStringValue(registro, "TXT_NOMB"));
		datos.getDatos().add(getStringValue(registro, "COD_TIPO_PERS_REL"));
		datos.getDatos().add(getStringValue(registro, "POR_PART"));
		datos.getDatos().add(getStringValue(registro, "COD_TIPO_RELAC"));
		return datos;
	}
	/***
	 * 
	 * @param registro resgistro a evaluar
	 * @param columna columna a obtener
	 * @return registro segun columna
	 */
	protected static final String getStringValue(Map<String, Object> registro,
			String columna) {
		if (StringUtils.isBlank(columna)) {
			throw new IllegalArgumentException("La columna es nulo");
		}
		if (registro == null) {
			return StringUtils.EMPTY;
		}
		return Utilerias.defaultStringIfBlank(registro.get(columna.trim()
				.toUpperCase()));
	}
}
