/**
 * Isban Mexico
 *   Clase: GeneradorCustionarioGenerico.java
 *   Descripcion: Componente para generar cuestionario generico.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.generador;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;

public interface GeneradorCuestionarioGenerico {
	
	/**
	 * String ID_DIV_PREGUNTA
	 */
	public static final String ID_DIV_PREGUNTA = "PreguntasDiv";
	/**
	 * String DIV_INICIO_SECCION
	 */
	public static final String DIV_INICIO_SECCION = " <DIV class='frameTablaEstandar' id='contenedorPaginadorResultados%s'>";
	/**
	 * String DIV_TITULO_SECCION
	 */
	public static final String DIV_TITULO_SECCION = "<DIV class='titleBuscadorSimple'><span>%s</span></DIV>";
	/**
	 * String DIV_INICIO_DATOS
	 */
	public static final String DIV_INICIO_DATOS = "<DIV id='divPrincipalDatos' >";
	/**
	 * String DIV_INICIO_PREGUNTA
	 */
	public static final String DIV_INICIO_PREGUNTA = "<DIV id='%s%s' class='contentTablaVariasColumnas'>";
	/**
	 * String DIV_CERRAR
	 */
	public static final String DIV_CERRAR = "</DIV>";
	/**
	 * String TABLE_INICIO
	 */
	public static final String TABLE_INICIO = "<TABLE>";
	/**
	 * String TABLE_CERRAR
	 */
	public static final String TABLE_CERRAR = "</TABLE>";
	/**
	 * String TR_TITULO_PREGUNTA
	 */
	public static final String TR_TITULO_PREGUNTA ="<tr id='TRpreg1_%s' idPreg='%s'>";
	/**
	 * String TR_INICIO
	 */
	public static final String TR_INICIO = "<TR>";
	/**
	 * String TR_CERRAR
	 */
	public static final String TR_CERRAR = "</TR>";
	/**
	 * String TD_INICIO
	 */
	public static final String TD_INICIO = "<TD>";
	/**
	 * String TD_INICIO con 32% de espacio
	 */
	public static final String TD_INICIO_32 = "<TD width='32%'>";
	/**
	 * String TD_CERRAR
	 */
	public static final String TD_CERRAR = "</TD>";
	/**
	 * String VISIBLE
	 */
	public static final String VISIBLE="visible";
	/**
	 * String HIDDEN
	 */
	public static final String HIDDEN ="hidden";
	/**
	 * String SECCION_ENCB
	 */
	public static final String SECCION_ENCB = "ENCB";
	/**
	 * String SECCION_SGMT
	 */
	public static final String SECCION_SGMT = "SGMT";
	/**
	 * String SECCION_PRCT
	 */
	public static final String SECCION_PRCT = "PRCT";
	/**
	 * String SECCION_ZNGF
	 */
	public static final String SECCION_ZNGF = "ZNGF";
	/**
	 * String SECCION_NACN
	 */
	public static final String SECCION_NACN = "NACN";
	/**
	 * String SECCION_ACTE
	 */
	public static final String SECCION_ACTE = "ACTE";
	/**
	 * String SECCION_ODRC
	 */
	public static final String SECCION_ODRC = "ODRC";
	/**
	 * String SECCION_PEP
	 */
	public static final String SECCION_PEP  = "SPEP";
	/**
	 * String SECCION_TRSI
	 */
	public static final String SECCION_TRSI = "TRSI";
	/**
	 * String SECCION_CVDV
	 */
	public static final String SECCION_CVDV = "CVDV";
	/**
	 * String SECCION_TRNS
	 */
	public static final String SECCION_TRNS = "TRNS";
	/**
	 * String SECCION_CTIC
	 */
	public static final String SECCION_CTIC = "CTIC";
	/**
	 * String CAMPO_LIBRE
	 */
	public static final String CAMPO_LIBRE = "LIBRE";
	/**
	 * String CAMPO_RADIO
	 */
	public static final String CAMPO_RADIO = "RADIO";
	/**
	 * String CAMPO_SELECT
	 */
	public static final String CAMPO_SELECT = "SELECT";
	/**
	 * String CAMPO_SELECT_MUL
	 */
	public static final String CAMPO_SELECT_MUL = "SELECT_MUL";
	/**
	 * String CAMPO_SELECT_AUT
	 */
	public static final String CAMPO_SELECT_AUT = "SELECT_AUT";
	/**
	 * String CAMPO_CAT_PAIS
	 */
	public static final String CAMPO_CAT_PAIS = "CAT_PAIS";
	/**
	 * String TAG_CAT_OR_REC
	 */
	public static final String TAG_CAT_OR_REC = "CAT_OR_REC";
	/**
	 * String TAG_CAT_DT_REC
	 */
	public static final String TAG_CAT_DT_REC = "CAT_DT_REC";
	/**
	 * String TAG_PREG_OR_REC_IC
	 */
	public static final String TAG_PREG_OR_REC_IC = "OR_REC_IC";
	/**
	 * String IND_ORIGEN_RECURSO
	 */
	public static final String IND_ORIGEN_RECURSO = "O";
	/**
	 * String IND_DESTINO_RECURSO
	 */
	public static final String IND_DESTINO_RECURSO = "D";
	/**
	 * String ID_ENTIDAD_FINANCIERA
	 */
	public static final String ID_ENTIDAD_FINANCIERA = "F";
	/**
	 * String ID_ENTIDAD_FINANCIERA_REG_SIMP
	 */
	public static final String ID_ENTIDAD_FINANCIERA_REG_SIMP = "S";
	/**
	 * String IND_PERSONA_FISICA
	 */
	public static final String IND_PERSONA_FISICA = "F";
	/**
	 * String IND_PERSONA_MORAL
	 */
	public static final String IND_PERSONA_MORAL = "J";
	/**
	 * String IND_PERSONA_CON_ACT_EMP
	 */
	public static final String IND_PERSONA_CON_ACT_EMP = "S";
	/**
	 * String IND_PERSONA_SIN_ACT_EMP
	 */
	public static final String IND_PERSONA_SIN_ACT_EMP = "N";
	/**
	 * String COD_PAIS_MX
	 */
	public static final String COD_PAIS_MX = "052";
	/**
	 * String IND_NO_APLICA
	 */
	public static final String IND_NO_APLICA = "N/A";
	/**
	 * Opcion de respuesta para indicar una Persona Politicamente Expuesta
	 */
	public static final String IND_PERSONA_PEP = "1";
	/**
	 * Opcion de respuesta para indicar que una entidad se encuentra establecida fisicamente
	 */
	public static final String IND_ENTIDAD_ESTABLECIDA_FISICA = "1";
	/**
	 * Indicador de Cuestionario para Informacion Preliminar
	 */
	public static final String ID_CUESTIONARIO_IP = "IP";
	
	/**
	 * Indicador de Cuestionario para Informacion Complementaria A1
	 */
	public static final String ID_CUESTIONARIO_A1 = "A1";
	
	/**
	 * Indicador de Cuestionario para Informacion Complementaria A2
	 */
	public static final String ID_CUESTIONARIO_A2 = "A2";
	
	/**
	 * Indicador de Cuestionario para Informacion Complementaria A3
	 */
	public static final String ID_CUESTIONARIO_A3 = "A3";
	
	/**
	 * NIVEL DE RIESGO MEDIO
	 */
	public static final String NIVEL_RIESGO_A2 = "A2";
	
	/**
	 * A2  PERSONA POLITICAMENTE EXPUESTA
	 */
	public static final String IND_UPLD_KYC_PEP = "KYC-PEP";
	
	/**
	 * Parametro de las secciones IP de Personas Fisicas sin activdad empresarial
	 */
	public static final String PARAM_SECCION_IP_FN = "seccionIPFN";
	
	/**
	 * Parametro de las secciones IP de Personas Fisicas con activdad empresarial
	 */
	public static final String PARAM_SECCION_IP_FS = "seccionIPFS";
	/**
	 * Parametro de las secciones IP de Personas Morales
	 */
	public static final String PARAM_SECCION_IP_JN = "seccionIPJN";
	
	/**
	 * Parametro de las secciones IC de Personas Fisicas sin activdad empresarial
	 */
	public static final String PARAM_SECCION_IC_FN = "seccionICFN";
	
	/**
	 * Parametro de las secciones IC de Personas Fisicas con activdad empresarial
	 */
	public static final String PARAM_SECCION_IC_FS = "seccionICFS";
	/**
	 * Parametro de las secciones IC de Personas Morales
	 */
	public static final String PARAM_SECCION_IC_JN = "seccionICJN";
	
	/**
	 * Generar el codigo HTML de un cuetionario
	 * @param responseCuestionario Datos del cuestionario
	 * @param session Objeto de sesion de Agave
	 * @return StringBuilder Codigo HTML con las preguntas del Cuestionario
	 * @throws BusinessException En caso de error con la informacion o con las operaciones
	 */
	public StringBuilder generarCuestionario(CuestionarioDTO responseCuestionario, ArchitechSessionBean session) throws BusinessException;
}
