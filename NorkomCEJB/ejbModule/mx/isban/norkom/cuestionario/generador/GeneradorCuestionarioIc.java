/**
 * Isban Mexico
 *   Clase: GeneradorCustionarioIC.java
 *   Descripcion: Componente para generar cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.generador;

import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

public abstract class GeneradorCuestionarioIc extends Architech implements GeneradorCuestionarioGenerico {
	/**
	 * serialVersionUID
	 */
	public static final long serialVersionUID = 6747780993469788449L;

	@Override
	public StringBuilder generarCuestionario(CuestionarioDTO cuestionarioDTO, ArchitechSessionBean session) throws BusinessException {
		StringBuilder cuestionarioIc = new StringBuilder();
		List<String> secciones = obtenerSeccionesICPorTipoPersona(cuestionarioDTO.getTipoPersona(), cuestionarioDTO.getSubtipoPersona(), session);
		
		for(String seccion : secciones) {
			if("INLB".equals(seccion) && "F".equals(cuestionarioDTO.getTipoPersona()) ){
				//se agrega la seccion INLB si son "Empleado del sector publico" o "Empleado del sector privado".
				if(cuestionarioDTO.getActividadEspecifica()!=null 
						&& ("P".equals(cuestionarioDTO.getActividadEspecifica().getTipoSector()) || "R".equals(cuestionarioDTO.getActividadEspecifica().getTipoSector()))){
					cuestionarioIc.append(generarSeccion(cuestionarioDTO, seccion, session));
				}
			}else{
				cuestionarioIc.append(generarSeccion(cuestionarioDTO, seccion, session));
			}
		}
		
		return cuestionarioIc;
	}
	
	/**
	 * Agrega el Inico de una seccion de preguntas
	 * @param seccionHtml Codigo HTML de la pregunta
	 * @param idSeccion SECCION
	 * @param tituloPregunta Titulo que lleva la pregunta
	 */
	public void agregarInicioSeccion(StringBuilder seccionHtml, String idSeccion, String tituloPregunta){
		seccionHtml.append(String.format(DIV_INICIO_SECCION, idSeccion));
		seccionHtml.append(String.format(DIV_TITULO_SECCION, tituloPregunta));
		seccionHtml.append(DIV_INICIO_DATOS);
		
	}
	
	/**
	 * Agrega el codigo HTML inicial de una pregunta
	 * @param seccionHtml StringBuilder al cual se le va a agregar el codigo HTML
	 */
	public void agregarFinSeccion(StringBuilder seccionHtml){
		seccionHtml.append(DIV_CERRAR); //DIV_INICIO_DATOS
		seccionHtml.append(DIV_CERRAR); //DIV_INICIO_SECCION
	}
	
	/**
	 * Agrega el codigo HTML inicial de una pregunta
	 * @param seccionHtml StringBuilder al cual se le va a agregar el codigo HTML
	 * @param pregunta Pregunta con la cual se va a generar el codigo HTML
	 */
	protected void agregarInicioPregunta(StringBuilder seccionHtml, PreguntaDTO pregunta){
		seccionHtml.append(String.format(DIV_INICIO_PREGUNTA, ID_DIV_PREGUNTA, pregunta.getAbreviatura()));
		seccionHtml.append(TABLE_INICIO);
		seccionHtml.append(String.format(TR_TITULO_PREGUNTA, pregunta.getIdPregunta(), pregunta.getIdPregunta()));
		seccionHtml.append(TD_INICIO);
		seccionHtml.append(String.format("%s&nbsp;", pregunta.getPregunta()));
	}
	
	/**
	 * Agrega el codigo HTML final de una pregunta
	 * @param seccionHtml StringBuilder al cual se le va a agregar el codigo HTML
	 */
	protected void agregarFinPregunta(StringBuilder seccionHtml) {
		seccionHtml.append(TD_CERRAR);
		seccionHtml.append(TR_CERRAR);
		seccionHtml.append(TABLE_CERRAR);
		seccionHtml.append(DIV_CERRAR); //DIV_INICIO_PREGUNTAS
	}
	
	/**
	 * Genera la seccion de una pregunta
	 * @param datosCuestionario Cuestionario al que pertenece la pregunta
	 * @param seccion Identificador de la seccion
	 * @param session Objeto de sesion de Agave
	 * @return StringBuilder Codigo HTML con la seccion
	 * @throws BusinessException En caso de error con la informacion
	 */
	public abstract StringBuilder generarSeccion(CuestionarioDTO datosCuestionario, String seccion, ArchitechSessionBean session)
	throws BusinessException;
	
	/**
	 * Busca las secciones IC en base al tipo de persona
	 * @param tipoPersona Tipo de persona de la cual se quieren obtener las secciones
	 * @param subtipoPersona Subtipo de persona de la cual se quieren obtener las secciones
	 * @param session Objeto de sesion de Agave
	 * @return Secciones asignadas
	 * @throws BusinessException En caso de error de negocio durante la operacion
	 */
	public abstract List<String> obtenerSeccionesICPorTipoPersona(String tipoPersona, String subtipoPersona, ArchitechSessionBean session)
	throws BusinessException;
}
