/**
 * Isban Mexico
 *   Clase: GeneradorCustionarioIP.java
 *   Descripcion: Componente para generar cuestionario IP.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.generador;

import java.util.List;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

public abstract class GeneradorCuestionarioIp extends Architech implements GeneradorCuestionarioGenerico {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6747780993469788449L;

	@Override
	public StringBuilder generarCuestionario(CuestionarioDTO cuestionarioDTO, ArchitechSessionBean session) throws BusinessException {
		StringBuilder cuestionarioIp = new StringBuilder();
		List<String> secciones = obtenerSeccionesIPPorTipoPersona(cuestionarioDTO.getTipoPersona(), cuestionarioDTO.getSubtipoPersona(), session);
		
		for(String seccion : secciones){
			/*
			 * Se trata de la seccion Actividad Economica. Si es una entidad financiera
			 * entonces se activa la seccion, actualmente solo cuenta con una pregunta: ¿Esta establecida fisicamente?
			 */
			if(SECCION_ACTE.equals(seccion) && (ID_ENTIDAD_FINANCIERA.equals(cuestionarioDTO.getEntidadFinancieraActEspecifica()) || 
				ID_ENTIDAD_FINANCIERA_REG_SIMP.equals(cuestionarioDTO.getEntidadFinancieraActEspecifica())))
			{
				cuestionarioIp.append(generarSeccion(cuestionarioDTO, seccion, session));
			} else if(SECCION_TRNS.equals(seccion)) {
				cuestionarioIp.append(generarSeccionTransaccionalidad(cuestionarioDTO, seccion, session));
			} else if(!SECCION_ACTE.equals(seccion)) {
				cuestionarioIp.append(generarSeccion(cuestionarioDTO, seccion, session));
			}else if(cuestionarioDTO.isRegSimp()){
				cuestionarioIp.append(generarSeccion(cuestionarioDTO, seccion, session));
			}
		}
		
		return cuestionarioIp;
	}
	
	/**
	 * Agrega el Inico de una seccion de preguntas
	 * @param seccionHtml Codigo HTML de la pregunta
	 * @param titulo Titulo que lleva la seccion
	 */
	protected void agregarInicioSeccion(StringBuilder seccionHtml, String titulo){
		seccionHtml.append(DIV_INICIO_SECCION);
		seccionHtml.append(String.format(DIV_TITULO_SECCION, titulo));
		seccionHtml.append(DIV_INICIO_DATOS);
		
	}
	
	/**
	 * Agrega el Fin de una seccion de preguntas
	 * @param seccionHtml Codigo HTML de la pregunta
	 */
	protected void agregarFinSeccion(StringBuilder seccionHtml){
		seccionHtml.append(DIV_CERRAR); //DIV_INICIO_DATOS
		seccionHtml.append(DIV_CERRAR); //DIV_INICIO_SECCION
	}
	
	/**
	 * Agrega el codigo HTML inicial de una pregunta
	 * @param seccionHtml StringBuilder al cual se le va a agregar el codigo HTML
	 * @param pregunta Pregunta con la cual se va a generar el codigo HTML
	 */
	protected void agregarInicioPregunta(StringBuilder seccionHtml, PreguntaDTO pregunta){
		seccionHtml.append(String.format(DIV_INICIO_PREGUNTA, ID_DIV_PREGUNTA, pregunta.getAbreviatura()));
		seccionHtml.append(TABLE_INICIO);
		seccionHtml.append(String.format(TR_TITULO_PREGUNTA, pregunta.getIdPregunta(), pregunta.getIdPregunta()));
		seccionHtml.append(TD_INICIO);
		seccionHtml.append(String.format("%s&nbsp;", pregunta.getPregunta()));
	}
	
	/**
	 * Genera la seccion de una pregunta
	 * @param datosCuestionario Cuestionario al que pertenece la pregunta
	 * @param seccion Identificador de la seccion
	 * @param session Objeto de sesion de Agave
	 * @return StringBuilder Codigo HTML con la seccion
	 * @throws BusinessException En caso de error con la informacion
	 */
	public abstract StringBuilder generarSeccion(CuestionarioDTO datosCuestionario, String seccion, ArchitechSessionBean session)
	throws BusinessException;
	
	/**
	 * Genera la seccion para las preguntas de Transaccionalidad
	 * @param datosCuestionario Cuestionario al que pertenece la pregunta
	 * @param seccion Identificador de la seccion
	 * @param session Objeto de sesion de Agave
	 * @return StringBuilder Codigo HTML con la seccion
	 * @throws BusinessException En caso de error con la informacion
	 */
	public abstract StringBuilder generarSeccionTransaccionalidad(CuestionarioDTO datosCuestionario, String seccion, ArchitechSessionBean session)
	throws BusinessException;
	
	/**
	 * Busca las secciones en base al tipo de persona
	 * @param tipoPersona Tipo de persona de la cual se quieren obtener las secciones
	 * @param subtipoPersona Subtipo de persona de la cual se quieren obtener las secciones
	 * @param session Objeto de sesion de Agave
	 * @return Secciones asignadas
	 * @throws BusinessException En caso de error de negocio durante la operacion
	 */
	public abstract List<String> obtenerSeccionesIPPorTipoPersona(String tipoPersona, String subtipoPersona, ArchitechSessionBean session)
	throws BusinessException;
}
