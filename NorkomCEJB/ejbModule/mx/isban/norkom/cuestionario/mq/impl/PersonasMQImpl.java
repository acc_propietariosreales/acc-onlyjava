/**************************************************************************************** 
 * 	Isban Mexico
 *   Clase: PersonasMQ 
 *   Descripcion: DAO para la implementacion del consumo de las transacciones en Personas
 *   Control de Cambios: 
 *   1.0 Oct 24, 2012 Stefanini - Creacion
 ****************************************************************************************/
package mx.isban.norkom.cuestionario.mq.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.norkom.cuestionario.dto.RequestODF3DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP1DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP2DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP3DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE58DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE71DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODF3DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP1DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP2DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP3DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE71DTO;
import mx.isban.norkom.cuestionario.mq.PersonasMQ;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.PersonasMQUtil;
import mx.isban.norkom.isbanda.GenericCicsIsbanDA;

import org.apache.commons.lang.StringUtils;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class PersonasMQImpl extends GenericCicsIsbanDA  implements PersonasMQ {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8564121584382601725L;

	/**
	 * Constructor de la clase
	 */
	public PersonasMQImpl() {
		super(IDACanal.CANAL_MQ_NORKOM);
	}
	
	@Override
	public ResponseODP1DTO obtenerCalificacionesODP1(RequestODP1DTO input, ArchitechSessionBean sesion) throws BusinessException {
		final ResponseODP1DTO respuesta = new ResponseODP1DTO();
		
		String buc8 = PersonasMQUtil.buc8posiciones(input.getNumeroPersona());
		final String tramaEntrada = RequestODP1DTO.generaTramaODP1(buc8);
		info(String.format("Obtencion Nivel de Riesgo e Indicador UPLD[%s]", tramaEntrada));
		
		ResponseMessageCicsDTO regreso = ejecutar(tramaEntrada, ID_TRAMA_ODP1);
		
		if(regreso == null || regreso.getResponseMessage() == null) {
			respuesta.setCodigoOperacion(Mensajes.NKM0001.getCodigo());
			respuesta.setMsgOperacion("Error de conexión en la obtencion del Nivel de Riesgo e Indicador UPLD");
		} else {
			final String tramaSalida = regreso.getResponseMessage();
			info(String.format("Obtencion Nivel de Riesgo  e Indicador UPLD [%s]", tramaSalida));
			
			final String[] tramaSeparada = tramaSalida.split("@");
			
			respuesta.setCodigoOperacion(tramaSeparada[ODP1_CODIGO_RESPUESTA].substring(2, 9));
			respuesta.setMsgOperacion(tramaSeparada[ODP1_CODIGO_RESPUESTA].substring(11, tramaSeparada[ODP1_CODIGO_RESPUESTA].length() - 2));
			
			if(ResponseODP1DTO.OPERACION_EXITOSA.equals(respuesta.getCodigoOperacion()) && tramaSeparada.length >= 4 && tramaSeparada[3].length() >= 19){
				String tramaRespuesta = tramaSeparada[3];
				
				respuesta.setIndicadorNivelRiesgo(StringUtils.trimToNull(tramaRespuesta.substring(14, 15)));
				respuesta.setIndicadorUpld(StringUtils.trimToNull(tramaRespuesta.substring(18, 19)));
			} else {
				info("La trama de respuesta no tiene la longitud correcta");
			}
		}
		
		return respuesta;
	}
	
	@Override
	public ResponseODP2DTO actualizarRiesgoODP2(RequestODP2DTO input, ArchitechSessionBean sesion) throws BusinessException {
		final ResponseODP2DTO respuesta = new ResponseODP2DTO();
		final String tramaEntrada = new PersonasMQUtil().generaTramaODP2(input);
		String codigoRespuesta = null;
		String mensajeRespuesta = null;
		info(String.format("Mantenimiento Nivel de Riesgo [%s]", tramaEntrada));
		
		ResponseMessageCicsDTO regreso = ejecutar(tramaEntrada, ID_TRAMA_ODP2);
		
		if(regreso == null || regreso.getResponseMessage() == null) {
			codigoRespuesta = Mensajes.NKM0001.getCodigo();
			mensajeRespuesta = "Error de conexión en la actualización del Nivel de Riesgo";
		} else {
			final String tramaSalida = regreso.getResponseMessage();
			info(String.format("Mantenimiento Nivel de Riesgo  [%s]", tramaSalida));
			
			final String[] tramaSeparada = tramaSalida.split("@");
			codigoRespuesta = tramaSeparada[ODP2_CODIGO_RESPUESTA].substring(2, 9);
			mensajeRespuesta = tramaSeparada[ODP2_CODIGO_RESPUESTA].substring(10, tramaSeparada[ODP2_CODIGO_RESPUESTA].length() - 2);
		}
		
		respuesta.setCodigoOperacion(codigoRespuesta);
		respuesta.setMsgOperacion(mensajeRespuesta);
		
		return respuesta;
	}
	
	@Override
	public ResponseODP3DTO actualizarEstadoClienteNuevoODP3(RequestODP3DTO input, ArchitechSessionBean sesion) throws BusinessException {
		final ResponseODP3DTO respuesta = new ResponseODP3DTO();
		final String tramaEntrada = new PersonasMQUtil().generaTramaODP3(input);
		String codigoRespuesta = null;
		String mensajeRespuesta = null;
		info(String.format("Mantenimiento Condicion del Cliente [%s]", tramaEntrada));
		
		ResponseMessageCicsDTO regreso = ejecutar(tramaEntrada, ID_TRAMA_ODP3);
		
		if(regreso == null || regreso.getResponseMessage() == null) {
			codigoRespuesta = Mensajes.NKM0001.getCodigo();
			mensajeRespuesta = "Error de conexión en la actualización del Estado del Cliente";
		} else {
			final String tramaSalida = regreso.getResponseMessage();
			info(String.format("Mantenimiento Condicion del Cliente [%s]", tramaSalida));
			
			final String[] tramaSeparada = tramaSalida.split("@");
			codigoRespuesta = tramaSeparada[ODP3_CODIGO_RESPUESTA].substring(2, 9);
			mensajeRespuesta = tramaSeparada[ODP3_CODIGO_RESPUESTA].substring(10, tramaSeparada[ODP3_CODIGO_RESPUESTA].length() - 2);
		}
		
		respuesta.setCodigoOperacion(codigoRespuesta);
		respuesta.setMsgOperacion(mensajeRespuesta);
		
		return respuesta;
	}
	
	
	@Override
	public ResponseODF3DTO obtenerCorreoElectronicoODF3(RequestODF3DTO input, ArchitechSessionBean sesion) throws BusinessException {
		final ResponseODF3DTO respuesta = new ResponseODF3DTO();
		final String tramaEntrada = new PersonasMQUtil().generaTramaODF3(input);
		info(String.format("Obtencion Correo del Cliente ODF3 [%s]", tramaEntrada));
		
		ResponseMessageCicsDTO regreso = ejecutar(tramaEntrada, ID_TRAMA_ODF3);
		
		if(regreso == null || regreso.getResponseMessage() == null || regreso.getResponseMessage().contains(ERROR_CICS)) {
			regreso = new ResponseMessageCicsDTO();
			regreso.setResponseMessage("@@ERNKM0001 ERROR DE CONEXION>>");
			regreso.setCodeError(Mensajes.NKM0001.getCodigo());
			regreso.setMessageError("ERROR DE CONEXION");
		}
		
		final String tramaSalida = regreso.getResponseMessage();

		info(String.format("Trama de salida para la obtencion Correo del Cliente [%s]", tramaSalida));
		
		final String[] tramaSeparada = tramaSalida.split("@");
		String codigoRespuesta = tramaSeparada[ODP2_CODIGO_RESPUESTA].substring(2, 9);
		
		if (!ResponseODF3DTO.OPERACION_EXITOSA.equals(codigoRespuesta)) {
			respuesta.setCodigoCliente(input.getCodigoCliente());
			respuesta.setCorreoElectronico(StringUtils.EMPTY);
		} else {
			int indiceCopy = tramaSalida.indexOf(ResponseODF3DTO.COPY_TRANSACCION);
			indiceCopy = indiceCopy + ResponseODF3DTO.COPY_TRANSACCION.length();
			respuesta.setCodigoCliente(tramaSalida.substring(indiceCopy, indiceCopy + 8));
			indiceCopy = indiceCopy + 8;
			respuesta.setSecuenciaDomicilio(tramaSalida.substring(indiceCopy, indiceCopy + 3));
			indiceCopy = indiceCopy + 3;
			respuesta.setCorreoElectronico(StringUtils.trimToEmpty(tramaSalida.substring(indiceCopy, indiceCopy + 81)));
			indiceCopy = indiceCopy + 81;
			respuesta.setTimeStamp(tramaSalida.substring(indiceCopy, indiceCopy + 26));
			indiceCopy = indiceCopy + 26;
		}
		
		return respuesta;
	}
	
	@Override
	public List<ResponsePE58DTO> obtenerReferenciasPersonasFisicasPE58(RequestPE58DTO input, boolean conActividadEmpresarial, ArchitechSessionBean sesion) throws BusinessException {
		final String tramaEntrada = new PersonasMQUtil().generaTramaPE58(input);
		info(String.format("Obtencion Referencias Personas Fisicas PE58 [%s]", tramaEntrada));
		
		ResponseMessageCicsDTO regreso = ejecutar(tramaEntrada, ID_TRAMA_PE58);
		
		if(regreso == null || regreso.getResponseMessage() == null || regreso.getResponseMessage().contains(ERROR_CICS)) {
			regreso = new ResponseMessageCicsDTO();
			regreso.setResponseMessage("@@ERNKM0001 ERROR DE CONEXION>>");
			regreso.setCodeError(Mensajes.NKM0001.getCodigo());
			regreso.setMessageError("ERROR DE CONEXION");
		}
		
		final String tramaSalida = regreso.getResponseMessage();
		info(String.format("Obtencion Referencias Fisicas [%s]", tramaSalida));
		
		List<String> codigosValidos = new ArrayList<String>();
		
		if(conActividadEmpresarial) {
			codigosValidos.add(CODIGO_REFERENCIAS_BANCARIAS);
		} else {
			codigosValidos.add(CODIGO_REFERENCIAS_PERSONALES);
		}
		
		List<ResponsePE58DTO> referencias = new PersonasMQUtil().desentramaPE58(tramaSalida, codigosValidos);
		
		if(referencias == null) {
			referencias = new ArrayList<ResponsePE58DTO>();
		}
		
		return referencias;
	}
	
	@Override
	public List<ResponsePE71DTO> obtenerReferenciasBancariasPE71(RequestPE71DTO input, ArchitechSessionBean sesion) throws BusinessException {
		final String tramaEntrada = new PersonasMQUtil().generaTramaPE71(input);
		info(String.format("Obtencion Referencias Bancarias PE71 [%s]", tramaEntrada));
		
		ResponseMessageCicsDTO regreso = ejecutar(tramaEntrada, ID_TRAMA_PE71);
		
		if(regreso == null || regreso.getResponseMessage() == null || regreso.getResponseMessage().contains(ERROR_CICS)) {
			regreso = new ResponseMessageCicsDTO();
			regreso.setResponseMessage("@@ERNKM0001 ERROR DE CONEXION>>");
			regreso.setCodeError(Mensajes.NKM0001.getCodigo());
			regreso.setMessageError("ERROR DE CONEXION");
		}
		
		final String tramaSalida = regreso.getResponseMessage();

		info(String.format("Obtencion Referencias Bancarias [%s]", tramaSalida));
		
		List<ResponsePE71DTO> referencias = new PersonasMQUtil().desentramaPE71(tramaSalida);
		
		if(referencias == null) {
			referencias = new ArrayList<ResponsePE71DTO>();
		}
		
		return referencias;
	}
}
