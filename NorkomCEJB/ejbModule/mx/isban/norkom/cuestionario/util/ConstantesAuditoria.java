/**
 *   Isban Mexico
 *   Clase: Mensaje.java
 *   Descripcion: Constantes que aplican para el almacenamiento y consulta de pistas de auditoria.
 *
 *   Control de Cambios:
 *   1.0 Feb 10, 2015 jgarcia - Creacion
 */
package mx.isban.norkom.cuestionario.util;

/**
 * @author jgarcia
 */
public class ConstantesAuditoria {
	/*++++++++++++ SERVICIOS ++++++++++++*/
	/** Servicio/Transaccion - Calcular nivel de riesgo en Norkom */
	public static final String SERV_OBTENER_ID_CUEST = "calcNvlRiesgoNkm";
	/** Servicio/Transaccion - Mantenimiento Nivel de Riesgo e Indicador UPLD con ODP2 */
	public static final String SERV_MANTO_NIVEL_IND_ODP2 = "mantoNvlIndODP2";
	/** Servicio/Transaccion - Mantenimiento Condicion del Cliente con ODP3 */
	public static final String SERV_MANTO_COND_CLTE_ODP3 = "mantoCondCteODP3";
	
	/*++++++++++++ CANAL DE LA APLICACION ++++++++++++*/
	/** Canal de aplicacion admin.*/
	public static final String CANAL_APP = "ACC_NKM";
	
	/*++++++++++++ TIPOS DE OPERACION ++++++++++++*/
	/**	Tipo de operacion alta.*/
	public static final String TIPO_OPER_ALTA = "A";
	/** Tipo de operacion modificar.*/
	public static final String TIPO_OPER_MOD = "M";
	/** Tipo de operacion consulta.*/
	public static final String TIPO_OPER_CONS = "C";
	/** Tipo de operacion baja.*/
	public static final String TIPO_OPER_BAJA = "B";
	
	/*++++++++++++ CODIGOS DE TABLAS Y TRANSACCIONES ++++++++++++*/
	/** Transaccion ODP2 */
	public static final String TRX_ODP2 = "ODP2";
	/** Transaccion ODP3 */
	public static final String TRX_ODP3 = "ODP3";
	
	/*++++++++++++ CODIGOS DE OPERACION ++++++++++++*/
	/**
	 * Codigo de operacion para calcular el nivel de riesgo
	 */
	public static final int COD_OPER_CALC_NIVEL_RIEGO = 1;
	/**
	 * Codigo de operacion para mantenimiento del nivel de riesgo e indicador UPLD con la ODP2
	 */
	public static final int COD_OPER_MANTO_NIVEL_IND_ODP2 = 2;
	/**
	 * Codigo de operacion para mantenimiento de la condicion del cliente(CLI, ACL) con la ODP3
	 */
	public static final int COD_OPER_MANTO_CLTE_ODP3 = 3;
	
	
	/**resultado Error de la operacion*/
	public static final String RESULTADO_ERROR = "Error"; 
	
	/**resultado Exito de la operacion*/
	public static final String RESULTADO_EXITOSO = "Exitoso";
	
	/** Constante para la key ID_ARCHIVO,FECHA_PROCESO. */
	public static final String KEY_ID_ARC_FECH = "ID_ARCHIVO,FECHA_PROCESO";
		
	/** Constante para la key ID_BITA_REG. */
	public static final String KEY_ID_BITA_REG = "ID_BITA_REG";
	
	/**Separador de bitacoras**/
	public static final String SEPARADOR = "|";
	
	/**Separador del valor y el nombre del campo para la pista**/
	public static final String SEPARADOR_CAMPO_VALOR = ":";
	
	/**leyenda no aplica.**/
	public static final String NA = "N/A";
}
