package mx.isban.norkom.cuestionario.util;

import java.util.List;

import mx.isban.norkom.ws.dto.RelacionadoDTO;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreaFormatoRelacionadosXML extends CreaFormatoXMLC {
	/**Limite para intervinientes***/
	private int limiteIntervinientes=150;
	/**Limite para beneficiarios***/
	private int limiteBeneficiarios=150;
	/**Limite para accionistas***/
	private int limiteAcionistas=150;
	/**Limite para intervinientes***/
	private int limiteProveedores=150;
	
	/**
	 * METODO PARA ARMAR INTERVINIENTES
	 * @param relacionadosIP de addIntervinientes a agregar
	 * @param doc de addIntervinientes a agregar
	 * @param formulario de addIntervinientes a agregar
	 * @param limI de addIntervinientes a agregar
	 * @return form de addIntervinientes
	 */
	protected Element addIntervenientes(List<RelacionadoDTO> relacionadosIP, Document doc, Element formulario, int limI){
		// Total de relacionados
		int tRelacionados = relacionadosIP.size();
		// Vector de elementos para relacionados
		Element[] relacionados = new Element[tRelacionados];
		
		// Intervinientes - Padre
		final Element Intervinientes = addElementoPadre(doc, formulario, "Intervinientes");
		
		// Contadors de Intervinientes
		int contI = 0;
		
		for(int i=0;i<tRelacionados && limI>0;i++){
			if(TIPO_RELACION_INTERVINIENTE.equals(relacionadosIP.get(i).getTipoRelacion()) && contI < limI){
				// Interviniente - Hijo
				relacionados[i] = addElementoPadre(doc, Intervinientes, "Interviniente");
				addElementoHijo(doc, relacionados[i], "NombreIntervinienteEmpresa", relacionadosIP.get(i).getNombreCompletoNorkom());
				addElementoHijo(doc, relacionados[i], "TipoPersonaInterviniente", relacionadosIP.get(i).getTipoPersona());		
				addElementoHijo(doc, relacionados[i], "FechaNacimientoConstitucionInterviniente", relacionadosIP.get(i).getFechaNacimiento());	
				addElementoHijo(doc, relacionados[i], "NacionalidadInterviniente", relacionadosIP.get(i).getCodPaisNacionalidad());
				addElementoHijo(doc, relacionados[i], "PaisResidenciaInterviniente", relacionadosIP.get(i).getCodPaisResidencia());
				contI++;
			}
		}
		
		return formulario;
	}
	
	/**
	 * METODO PARA ARMAR BENEFICIARIOS
	 * @param relacionadosIP de addBeneficiarios a agregar
	 * @param doc de addBeneficiarios a agregar
	 * @param formulario de addBeneficiarios a agregar
	 * @param limB de addBeneficiarios a agregar
	 * @return form de addBeneficiarios
	 */
	protected Element addBeneficiarios(List<RelacionadoDTO> relacionadosIP, Document doc, Element formulario, int limB){
		// Total de relacionados
		int tRelacionados = relacionadosIP.size();
		// Vector de elementos para relacionados
		Element[] relacionados = new Element[tRelacionados];
		
		// Beneficiarios - Padre
		final Element Beneficiarios = addElementoPadre(doc, formulario, "Beneficiarios");
		
		// Contadors de Beneficiarios
		int contB = 0;
		
		for(int i=0;i<tRelacionados && limB>0;i++){	
			if(TIPO_RELACION_BENEFICIARIO.equals(relacionadosIP.get(i).getTipoRelacion()) && contB < limB){
				// Beneficiario - Hijo
				relacionados[i] = addElementoPadre(doc, Beneficiarios, "Beneficiario");
				addElementoHijo(doc, relacionados[i], "NombreBeneficiarioEmpresa", relacionadosIP.get(i).getNombreCompletoNorkom());
				addElementoHijo(doc, relacionados[i], "TipoPersonaBeneficiario", relacionadosIP.get(i).getTipoPersona());		
				addElementoHijo(doc, relacionados[i], "FechaNacimientoConstitucionBeneficiario", relacionadosIP.get(i).getFechaNacimiento());	
				addElementoHijo(doc, relacionados[i], "NacionalidadBeneficiario", relacionadosIP.get(i).getCodPaisNacionalidad());
				addElementoHijo(doc, relacionados[i], "PaisResidenciaBeneficiario", relacionadosIP.get(i).getCodPaisResidencia());
				contB++;
			}
		}
		
		return formulario;
	}
	
	/**
	 * METODO PARA ARMAR ACCIONISTAS
	 * @param relacionadosIP de addAccionistas a agregar
	 * @param doc de addAccionistas a agregar
	 * @param formulario de addAccionistas a agregar
	 * @param limA de addAccionistas a agregar
	 * @return form de addAccionistas
	 */
	protected Element addAccionistas(List<RelacionadoDTO> relacionadosIP, Document doc, Element formulario, int limA){
		// Total de relacionados
		int tRelacionados = relacionadosIP.size();
		// Vector de elementos para relacionados
		Element[] relacionados = new Element[tRelacionados];
		
		// Accionistas - Padre
		final Element Accionistas = addElementoPadre(doc, formulario, "Accionistas");
		
		// Contadors de Accionistas
		int contA = 0;
		
		for(int i=0;i<tRelacionados && limA>0;i++){
			if(TIPO_RELACION_ACCIONISTA.equals(relacionadosIP.get(i).getTipoRelacion()) && contA < limA){
				// Accionista - Hijo
				relacionados[i] = addElementoPadre(doc, Accionistas, "Accionista");
				addElementoHijo(doc, relacionados[i], "NombreAccionistaEmpresa", relacionadosIP.get(i).getNombreCompletoNorkom()); 
				addElementoHijo(doc, relacionados[i], "TipoPersonaAccionista", relacionadosIP.get(i).getTipoPersona());		
				addElementoHijo(doc, relacionados[i], "FechaNacimientoConstitucionAccionista", relacionadosIP.get(i).getFechaNacimiento());	
				addElementoHijo(doc, relacionados[i], "NacionalidadAccionista", relacionadosIP.get(i).getCodPaisNacionalidad());
				addElementoHijo(doc, relacionados[i], "PaisResidenciaAccionista", relacionadosIP.get(i).getCodPaisResidencia());
				contA++;
			}
		}
		return formulario;
	}
	
	

	
	/**
	 * METODO PARA ARMAR PROVEEDORES
	 * @param relacionadosIP de addProveedores a agregar
	 * @param doc de addProveedores a agregar
	 * @param formulario de addProveedores a agregar
	 * @param limP de addProveedores a agregar
	 * @return form de addProveedores
	 */
	protected Element addProveedores(List<RelacionadoDTO> relacionadosIP, Document doc, Element formulario, int limP){
		// Total de relacionados
		int tRelacionados = relacionadosIP.size();
		// Vector de elementos para relacionados
		Element[] relacionados = new Element[tRelacionados];
		
		// ProveedoresRecursos - Padre
		final Element ProveedoresRecursos = addElementoPadre(doc, formulario, "ProveedoresRecursos");
		
		// Contadors de Proveedores
		int contP = 0;
		
		for(int i=0;i<tRelacionados && limP>0;i++){
			if(TIPO_RELACION_PROVEEDOR.equals(relacionadosIP.get(i).getTipoRelacion()) && contP < limP){
				// ProveedorRecursos - Hijo
				relacionados[i] = addElementoPadre(doc, ProveedoresRecursos, "ProveedorRecursos");
				addElementoHijo(doc, relacionados[i], "NombreProveedorRecursosEmpresa", relacionadosIP.get(i).getNombreCompletoNorkom()); 
				addElementoHijo(doc, relacionados[i], "TipoPersonaProveedorRecursos", relacionadosIP.get(i).getTipoPersona());		
				addElementoHijo(doc, relacionados[i], "FechaNacimientoConstitucionProveedorRecursos", relacionadosIP.get(i).getFechaNacimiento());	
				addElementoHijo(doc, relacionados[i], "NacionalidadProveedorRecursos", relacionadosIP.get(i).getCodPaisNacionalidad());
				addElementoHijo(doc, relacionados[i], "PaisResidenciaProveedorRecursos", relacionadosIP.get(i).getCodPaisResidencia());
				contP++;
			}
		}
		
		return formulario;
	}
	
	/**
	 * @return the limiteIntervinientes
	 */
	public int getLimiteIntervinientes() {
		return limiteIntervinientes;
	}

	/**
	 * @param limiteIntervinientes 
	 * el limiteIntervinientes a agregar
	 */
	public void setLimiteIntervinientes(int limiteIntervinientes) {
		this.limiteIntervinientes = limiteIntervinientes;
	}

	/**
	 * @return the limiteBeneficiarios
	 */
	public int getLimiteBeneficiarios() {
		return limiteBeneficiarios;
	}

	/**
	 * @param limiteBeneficiarios 
	 * el limiteBeneficiarios a agregar
	 */
	public void setLimiteBeneficiarios(int limiteBeneficiarios) {
		this.limiteBeneficiarios = limiteBeneficiarios;
	}

	/**
	 * @return the limiteAcionistas
	 */
	public int getLimiteAcionistas() {
		return limiteAcionistas;
	}

	/**
	 * @param limiteAcionistas 
	 * el limiteAcionistas a agregar
	 */
	public void setLimiteAcionistas(int limiteAcionistas) {
		this.limiteAcionistas = limiteAcionistas;
	}

	/**
	 * @return the limiteProveedores
	 */
	public int getLimiteProveedores() {
		return limiteProveedores;
	}

	/**
	 * @param limiteProveedores 
	 * el limiteProveedores a agregar
	 */
	public void setLimiteProveedores(int limiteProveedores) {
		this.limiteProveedores = limiteProveedores;
	}
}
