/**
  * Isban Mexico NorkomCEJB
  *   Clase:  CreaFormatoXML.java
  *   Descripcion: clase de utileria para generar estructura 
  *   			  XML de cuestionario IP y enviarla el WS de Norkom
  *
  *   Control de Cambios:
  *   1.0 31-03-2012 Stefanini (LFER) - Creacion
  */
package mx.isban.norkom.cuestionario.util;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import mx.isban.norkom.cuestionario.dto.CuestionarioIPDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesXmlDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Leopoldo F Espinosa R 31/03/2015
 *
 */
public class CreaFormatoXML extends CreaFormatoRelacionadosXML {
	/**
	 * Metodo principal para la generacion del formtao XMl de el cuestionario IP
	 * @param formularioIP CuestionarioIPDTO con informacion del cuestionario
	 * @param opcionesIP lista de opciones xml
	 * @param recursosIP dto de recursos
	 * @param relacionadosIP datos relacionados
	 * @param paisesTransInter paises de transferencias internacionales
	 * @return cadena en formato xml para enviar a WS
	 * @throws TransformerException con stack de error
	 * @throws ParserConfigurationException con stack de error
	 */
	public String creaFormatoXML(CuestionarioIPDTO formularioIP ,List<OpcionesXmlDTO> opcionesIP,List<RecursoDTO> recursosIP,List<RelacionadoDTO> relacionadosIP,List<PaisDTO> paisesTransInter) throws TransformerException, ParserConfigurationException{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
 
		/** RAIZ TXN **/
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("txn");
		doc.appendChild(rootElement);
		/** RAIZ FORMULARIO IP **/
		Element formulario = addFormulario(formularioIP, doc, rootElement);
		/** OPCIONES IP **/
		addOpciones(opcionesIP, doc, formulario);
		/** ORIGEN/DESTINO IP **/
		addRecursos(recursosIP, doc, formulario);			
		/** PAISES TRANSF-INTER **/
		addPaises(paisesTransInter, doc, formulario);
		/** TRANSACCIONALIDAD **/
		addTransacciones(opcionesIP, doc, formulario);
		/** INTERVENIENTES **/
		addIntervenientes(relacionadosIP, doc, formulario,getLimiteIntervinientes() );
		/** BENEFICIARIOS **/
		addBeneficiarios(relacionadosIP, doc, formulario, getLimiteBeneficiarios());
		/** ACCIONISTAS **/
		addAccionistas(relacionadosIP, doc, formulario, getLimiteAcionistas());
		/** PROVEEDORES **/
		addProveedores(relacionadosIP, doc, formulario, getLimiteProveedores());
	
		//Se retorna la estructura XML en formato String
		return creaFormatoXML(doc);
	}
	
	/**
	 * METODO PARA ARMAR RECURSOS_IP
	 * @param recursosIP de addRecursos a agregar
	 * @param doc de addRecursos a agregar
	 * @param formulario de addRecursos a agregar
	 * @return form de addRecursos
	 */
	private Element addRecursos(List<RecursoDTO> recursosIP, Document doc, Element formulario){

		for(RecursoDTO recurso : recursosIP){
			if(ABRV_ORIGEN_RECURSOS.equals(recurso.getTipoRecurso())){
				addElementoHijo(doc, formulario, "OrigenRecursos", recurso.getCodigoRecurso());
			}else if(ABRV_DETINO_RECURSOS.equals(recurso.getTipoRecurso())){
				addElementoHijo(doc, formulario, "DestinoRecursos", recurso.getCodigoRecurso());
			}
		}
		
		return formulario;
	}


	
	/**
	 * METODO PARA ARMAR TRANSACCIONES_IP
	 * @param transaccionesIP de addTransacciones a agregar
	 * @param doc de addTransacciones a agregar
	 * @param formulario de addTransacciones a agregar
	 * @return form de addTransacciones
	 */
	private  Element addTransacciones(List<OpcionesXmlDTO> transaccionesIP, Document doc, Element formulario){
		// Transaccionalidad
		final Element transacciones = addElementoPadre(doc, formulario, "Transaccionalidad");
		
		for(OpcionesXmlDTO opcion : transaccionesIP){
			String valor = opcion.getValor();
			valor=rellena(valor,2,'0');
			if(ABRV_NUM_TRANS_INTER_ENV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumTransInterEnv", valor);
			}else if(ABRV_MON_TRANS_INTER_ENV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonTransInterEnv", valor);
			}else if(ABRV_NUM_TRANS_INTER_REC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumTransInterRec", valor);
			}else if(ABRV_MON_TRANS_INTER_REC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonTransInterRec", valor);
			}else if(ABRV_NUM_TRANS_NAC_ENV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumTransNacEnv", valor);
			}else if(ABRV_MON_TRANS_NAC_ENV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonTransNacEnv", valor);
			}else if(ABRV_NUM_TRANS_NAC_REC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumTransNacRec", valor);
			}else if(ABRV_MON_TRANS_NAC_REC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonTransNacRec", valor);
			}else if(ABRV_NUM_DEP_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumDepositosEfect", valor);
			}else if(ABRV_MON_DEP_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonDepositosEfect", valor);
			}
		}
		//Completar la otra mitad
		addTransacciones2(transaccionesIP, doc, formulario, transacciones);
		
		return formulario;
	}

	/**
	 * METODO PARA ARMAR TRANSACCIONES_IP_AUX
	 * @param transaccionesIP de addTransacciones a agregar
	 * @param doc de addTransacciones a agregar
	 * @param formulario de addTransacciones a agregar
	 * @param transacciones de addTransacciones a agregar
	 * @return form de addTransacciones
	 */
	private  Element addTransacciones2(List<OpcionesXmlDTO> transaccionesIP, Document doc, Element formulario, Element transacciones){
		
		for(OpcionesXmlDTO opcion : transaccionesIP){
			String valor = opcion.getValor();
			valor=rellena(valor,2,'0');
			if(ABRV_NUM_DEP_NO_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumDepositosNoEfect", valor);
			}else if(ABRV_MON_DEP_NO_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonDepositosNoEfect", valor);
			}else if(ABRV_NUM_RET_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumRetirosEfect", valor);
			}else if(ABRV_MON_RET_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonRetirosEfect", valor);
			}else if(ABRV_NUM_RET_NO_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumRetirosNoEfect", valor);
			}else if(ABRV_MON_RET_NO_EFEC.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonRetirosNoEfect", valor);
			}else if(ABRV_NUM_COMP_DIV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumCompraDiv", valor);
			}else if(ABRV_MON_COMP_DIV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonCompraDiv", valor);
			}else if(ABRV_NUM_VENT_DIV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "NumVentaDiv", valor);
			}else if(ABRV_MON_VENT_DIV.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, transacciones, "MonVentaDiv", valor);
			}
		}
		return formulario;
	}
	/**
	 * funcion para relleno de numero
	 * @param valor cadena a validar
	 * @param tamanio tanio que debe tener
	 * @param relleno relleno de caracter
	 * @return nueva cadena con relleno si es nesesario
	 */
	private String rellena(String valor, int tamanio, char relleno) {
		if(valor!=null){
			if(valor.length()<tamanio){
				for(int y=valor.length();y<tamanio;y++){
					valor=relleno+valor;
				}
				return valor; 
			}else{
				return valor;
			}
		}else{
			return "";
		}
		
	}

	/**
	 * METODO PARA ARMAR FORMULARIO_IP
	 * @param formularioIP de addFormulario a agregar
	 * @param doc de addFormulario a agregar
	 * @param raiz de addFormulario a agregar
	 * 
	 * @return form de addFormulario
	 */
	private Element addFormulario(CuestionarioIPDTO formularioIP, Document doc, Element raiz){
		// formulario - raiz
		Element formulario = addElementoPadre(doc, raiz, "formulario");
		
		// IdFormulario
		addElementoHijo(doc, formulario, "IdFormulario", formularioIP.getIdFormulario()+ formularioIP.getFechaCreacion());
		// DescAplicativo
		addElementoHijo(doc, formulario, "DescAplicativo", formularioIP.getDescAplicativo());
		// FechaFormulario
		addElementoHijo(doc, formulario, "FechaFormulario", formularioIP.getFechaCreacion());
		// BUC
		addElementoHijo(doc, formulario, "BUC", PersonasMQUtil.buc8posiciones(formularioIP.getValBUC())); 
		// Entidad
		addElementoHijo(doc, formulario, "Entidad", formularioIP.getCodigoEntidad());		
		// NombrePersonaEmpresa
		addElementoHijo(doc, formulario, "NombrePersonaEmpresa", formularioIP.getNombrePersona());		
		// FechaNacimientoConstitucion
		addElementoHijo(doc, formulario, "FechaNacimientoConstitucion", formularioIP.getFechaPersona());
		// TipoPersona
		addElementoHijo(doc, formulario, "TipoPersona", formularioIP.getTipoPersona());		
		// SubtipoPersona
		addElementoHijo(doc, formulario, "SubtipoPersona", formularioIP.getSubtipoPersona());		
		// SegmentoNegCliente
		addElementoHijo(doc, formulario, "SegmentoNegCliente", formularioIP.getCodigoSegmento());
		// ActividadEconomica
		addElementoHijo(doc, formulario, "ActividadEconomica", formularioIP.getCodigoActividadEcon());
		// CodProducto
		addElementoHijo(doc, formulario, "CodProducto", formularioIP.getCodigoProducto());		
		// PaisResidencia
		addElementoHijo(doc, formulario, "PaisResidencia", formularioIP.getCodigoPais());
		// MunicipioResidencia
		addElementoHijo(doc, formulario, "MunicipioResidencia", formularioIP.getCodigoMunicipio());
		// Nacionalidad
		addElementoHijo(doc, formulario, "Nacionalidad", formularioIP.getCodigoNacionalidad()); 
		// Divisa 
		addElementoHijo(doc, formulario, "Divisa", formularioIP.getDivisa());			
		// CodigoRegionSucursal 
		addElementoHijo(doc, formulario, "CodigoRegionSucursal", formularioIP.getRegionSucursal());
		// CodigoZonaSucursal 
		addElementoHijo(doc, formulario, "CodigoZonaSucursal", formularioIP.getZonaSucursal());
		// CodigoCentroCostos 
		addElementoHijo(doc, formulario, "CodigoCentroCostos", formularioIP.getCodigoCentroCostos());
			
		return formulario;
	}
	
	/***
	 * creacion de Formato XML
	 * @param doc Documento para la conversion de XML
	 * @return String con el formtao de XML
	 * @throws TransformerException con mensaje de error
	 */
	private String creaFormatoXML(Document doc)throws TransformerException{
		// Intancia usada para realizar la implementacion de una Fabrica de Tranformacion
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		// Tranforma un Source tree en un Result tree
		Transformer transformer = transformerFactory.newTransformer();
		// Transforma el Source tree en forma de un Document Object Model (DOM) tree.
		DOMSource source = new DOMSource(doc);
		// Instancia generada para convertir el Result tree en String
		StringWriter xml = new StringWriter();
		// Intancia creada para asignar el Result tree a "xml"
		StreamResult result =  new StreamResult(xml);
		// Se realiza la tranformacion de Source tree a Result tree
		transformer.transform(source, result);
		// Se elimina el encabezado de xml para dejar solo la estructura en strResult
		
		return new StringBuilder(xml.toString().substring(xml.toString().indexOf("?>")+2)).toString();
	}
}
