/**
  * Isban Mexico NorkomCEJB
  *   Clase:  CreaFormatoXML.java
  *   Descripcion: clase de utileria para generar estructura 
  *   			  XML de cuestionario IP y enviarla el WS de Norkom
  *
  *   Control de Cambios:
  *   1.0 31-03-2012 Stefanini (LFER) - Creacion
  */
package mx.isban.norkom.cuestionario.util;

import java.util.List;

import mx.isban.norkom.cuestionario.dto.OpcionesXmlDTO;
import mx.isban.norkom.cuestionario.dto.PaisDTO;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Leopoldo F Espinosa R 31/03/2015
 *
 */
public class CreaFormatoXMLC {
	/** Variable utilizada para abreviatura de Presencia Fisica**/
	public static final String ABRV_PRESENCIA_FISICA = "ENFI";
	/** Variable utilizada para abreviatura de PEP**/
	public static final String ABRV_PEP = "PPEP";
	/** Variable utilizada para abreviatura de Compra/Venta de Divisas**/
	public static final String ABRV_CVDIVISAS = "CVDV";
	/** Variable utilizada para abreviatura de origen de recursos **/
	public static final String ABRV_ORIGEN_RECURSOS = "ORRC";
	/** Variable utilizada para abreviatura de destino de recursos **/
	public static final String ABRV_DETINO_RECURSOS = "DTRC";
	/** Variable utilizada para abreviatura de Num. Trans. Inter. Enviadas **/
	public static final String ABRV_NUM_TRANS_INTER_ENV = "TIEN";
	/** Variable utilizada para abreviatura de Mon. Trans. Inter. Enviadas **/
	public static final String ABRV_MON_TRANS_INTER_ENV = "TIEM";
	/** Variable utilizada para abreviatura de Num. Trans. Inter. Recibidas **/
	public static final String ABRV_NUM_TRANS_INTER_REC = "TIRN";
	/** Variable utilizada para abreviatura de Mon. Trans. Inter. Recibidas **/
	public static final String ABRV_MON_TRANS_INTER_REC = "TIRM";
	/** Variable utilizada para abreviatura de Num. Trans. Nac. Enviadas **/
	public static final String ABRV_NUM_TRANS_NAC_ENV = "TNEN";
	/** Variable utilizada para abreviatura de Mon. Trans. Nac. Enviadas **/
	public static final String ABRV_MON_TRANS_NAC_ENV = "TNEM";
	/** Variable utilizada para abreviatura de Num. Trans. Nac. Recibidas**/
	public static final String ABRV_NUM_TRANS_NAC_REC = "TNRN";
	/** Variable utilizada para abreviatura de Mon. Trans. Nac. Recibidas **/
	public static final String ABRV_MON_TRANS_NAC_REC = "TNRM";
	/** Variable utilizada para abreviatura de Num. Depositos Efectivo **/
	public static final String ABRV_NUM_DEP_EFEC = "DPEN";
	/** Variable utilizada para abreviatura de Mon. Depositos Efectivo **/
	public static final String ABRV_MON_DEP_EFEC = "DPEM";
	/** Variable utilizada para abreviatura de Num. Depositos No Efectivo**/
	public static final String ABRV_NUM_DEP_NO_EFEC = "DNEN";
	/** Variable utilizada para abreviatura de Mon. Depositos No Efectivo **/
	public static final String ABRV_MON_DEP_NO_EFEC = "DNEM";
	/** Variable utilizada para abreviatura de Num. Retiros EFectivo **/
	public static final String ABRV_NUM_RET_EFEC = "RTEN";
	/** Variable utilizada para abreviatura de Mon. Retiros EFectivo **/
	public static final String ABRV_MON_RET_EFEC = "RTEM";
	/** Variable utilizada para abreviatura de Num. Retiros No Efectivo **/
	public static final String ABRV_NUM_RET_NO_EFEC = "RNEN";
	/** Variable utilizada para abreviatura de Mon. Retiros No Efectivo **/
	public static final String ABRV_MON_RET_NO_EFEC = "RNEM";
	/** Variable utilizada para abreviatura de Num. Compra Divisas **/
	public static final String ABRV_NUM_COMP_DIV = "CPDN";
	/** Variable utilizada para abreviatura de Mon. Compra Divisas **/
	public static final String ABRV_MON_COMP_DIV = "CPDM";
	/** Variable utilizada para abreviatura de Num. Venta Divisas **/
	public static final String ABRV_NUM_VENT_DIV = "VTDN";
	/** Variable utilizada para abreviatura de Mon. Venta Divisas **/
	public static final String ABRV_MON_VENT_DIV = "VTDM";
	/** Variable utilizada para describir TIPO_RELACION_INTERVINIENTE **/
	public static final String TIPO_RELACION_INTERVINIENTE = "I";
	/** Variable utilizada para describir TIPO_RELACION_PROVEEDOR **/
	public static final String TIPO_RELACION_PROVEEDOR = "P";
	/** Variable utilizada para describir TIPO_RELACION_ACCIONISTA **/
	public static final String TIPO_RELACION_ACCIONISTA = "A";
	/** Variable utilizada para describir TIPO_RELACION_BENEFICIARIO **/
	public static final String TIPO_RELACION_BENEFICIARIO = "B";

	
	


	/**
	 * METODO GENERAR ELEMENTO PADRE
	 * @param doc de addElementoPadre
	 * @param raiz de addElementoPadre
	 * @param elemento de addElementoPadre
	 * @return node de addElementoPadre
	 */
	protected Element addElementoPadre(Document doc, Element raiz, String elemento){
		
		Element nodo = doc.createElement(elemento);
		raiz.appendChild(nodo);
		
		return nodo;
	}
	/**
	 * METODO GENERAR ELEMENTO HIJO
	 * @param doc de addElementoHijo a agregar
	 * @param padre de addElementoHijo a agregar
	 * @param elemento de addElementoHijo a agregar
	 * @param valor de addElementoHijo a agregar
	 * @return node de addElementoHijo
	 */
	protected Element addElementoHijo(Document doc, Element padre, String elemento, String valor){
		
		Element nodo = doc.createElement(elemento);
		nodo.appendChild(doc.createTextNode(valor));
		padre.appendChild(nodo);
		
		return nodo;
	}

	/**
	 * METODO PARA ARMAR OPCIONES_IP
	 * @param opcionesIP de addOpciones a agregar
	 * @param doc de addOpciones a agregar
	 * @param formulario de addOpciones a agregar
	 * @return form de addOpciones
	 */
	protected Element addOpciones(List<OpcionesXmlDTO> opcionesIP, Document doc, Element formulario){
		// Bandera para saber si se especifico o no la PresenciaFisica
		int ban = 0;
		
		for(OpcionesXmlDTO opcion : opcionesIP){
		// Verificar que el valor no viene nulo
		final String valor = opcion.getValor()!=null ? opcion.getValor() : "";
		
			if(ABRV_PRESENCIA_FISICA.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, formulario, "PresenciaFisica", "0"+valor);
				ban = 1;
			}else if(ABRV_PEP.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, formulario, "PEP", "0"+valor);
			}else if(ABRV_CVDIVISAS.equals(opcion.getAbreviatura())){
				addElementoHijo(doc, formulario, "CVDiv", "0"+valor);
			}
		}
		
		if(ban == 0){
			addElementoHijo(doc, formulario, "PresenciaFisica", StringUtils.EMPTY);
		}
		
		return formulario;
	}

	/**
	 * METODO PARA ARMAR TRANS_INTER_PAISES
	 * @param paisesTransInter de addPaises a agregar
	 * @param doc de addPaises a agregar
	 * @param formulario de addPaises a agregar
	 * @return form de addPaises
	 */
	protected Element addPaises(List<PaisDTO> paisesTransInter, Document doc, Element formulario){
		// TransfInternac
		final Element paises = addElementoPadre(doc, formulario, "PaisTransferenciasInternacionales");
		
		for(PaisDTO opcion : paisesTransInter){
			addElementoHijo(doc, paises, "Pais", opcion.getCodigo());
		}
		
		return formulario;
	}
}
