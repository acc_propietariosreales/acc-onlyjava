/**
  * Isban Mexico NorkomCEJB
  *   Clase:  CreaFormatoXML.java
  *   Descripcion: clase de utileria para generar estructura 
  *   			  XML de cuestionario IP y enviarla el WS de Norkom
  *
  *   Control de Cambios:
  *   1.0 31-03-2012 Stefanini (LFER) - Creacion
  */
package mx.isban.norkom.cuestionario.util;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import mx.isban.norkom.ws.dto.RelacionadoDTO;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Leopoldo F Espinosa R 31/03/2015
 *
 */
public class CreaFormatoXMLRelacionados extends CreaFormatoXMLC {
	/**
	 * Metodo principal para la generacion del
	 * XMl que se envia a el servicio
	 * de Norkom para agregar los relacionados
	 * 
	 * @param relacionados List<RelacionadoDTO>
	 * @param formularioIP id de cuestionario
	 * @param bloque numero de bloque
	 * @param buc buc del usuario
	 * @return xml para enviar
	 * @throws TransformerException con mensaje de error
	 * @throws ParserConfigurationException con mensaje de error
	 */
	public String creaFormatoXML(List<RelacionadoDTO> relacionados,String formularioIP,String bloque,String buc ) throws TransformerException, ParserConfigurationException{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
 
		/** RAIZ TXN **/
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("txn");
		doc.appendChild(rootElement);
		/** RAIZ FORMULARIO IP **/
		Element formulario=addElementoPadre(doc, rootElement, "formulario");
		//IdFormulario
		addElementoHijo(doc, formulario, "IdFormulario",formularioIP);
		//NumBloque
		addElementoHijo(doc, formulario, "NumBloque",bloque);
		//BUC
		addElementoHijo(doc, formulario, "BUC",buc);
		//BUC
		Element relacionadosList=addElementoPadre(doc, formulario, "Relacionados");
		
		for (RelacionadoDTO dtoRelacionado : relacionados) {
			Element relacionado=addElementoPadre(doc, relacionadosList, "Relacionado");
			// <NombreRelacionado/>
			addElementoHijo(doc, relacionado, "NombreRelacionado",dtoRelacionado.getNombreCompleto());
			//<TipoPersonaRelacionado/>
			addElementoHijo(doc, relacionado, "TipoPersonaRelacionado",dtoRelacionado.getTipoPersona());
			//<FechaNacimientoRelacionado/>
			addElementoHijo(doc, relacionado, "FechaNacimientoRelacionado",dtoRelacionado.getFechaNacimiento());
			//<NacionalidadRelacionado/>
			addElementoHijo(doc, relacionado, "NacionalidadRelacionado",dtoRelacionado.getCodPaisNacionalidad());
			//<PaisResidenciaRelacionado/>
			addElementoHijo(doc, relacionado, "PaisResidenciaRelacionado",dtoRelacionado.getCodPaisResidencia());
			//<TipoRelacionado/>
			addElementoHijo(doc, relacionado, "TipoRelacionado",dtoRelacionado.getTipoRelacion());
		}
		
		//Se retorna la estructura XML en formato String
		return creaFormatoXML(doc);
	}
	
	/***
	 * creacion de Formato XML
	 * @param doc Documento para la conversion de XML
	 * @return String con el formtao de XML
	 * @throws TransformerException con mensaje de error
	 */
	private String creaFormatoXML(Document doc)throws TransformerException{
		// Intancia usada para realizar la implementacion de una Fabrica de Tranformacion
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		// Tranforma un Source tree en un Result tree
		Transformer transformer = transformerFactory.newTransformer();
		// Transforma el Source tree en forma de un Document Object Model (DOM) tree.
		DOMSource source = new DOMSource(doc);
		// Instancia generada para convertir el Result tree en String
		StringWriter xml = new StringWriter();
		// Intancia creada para asignar el Result tree a "xml"
		StreamResult result =  new StreamResult(xml);
		// Se realiza la tranformacion de Source tree a Result tree
		transformer.transform(source, result);
		// Se elimina el encabezado de xml para dejar solo la estructura en strResult
		
		return new StringBuilder(xml.toString().substring(xml.toString().indexOf("?>")+2)).toString();
	}

	

}
