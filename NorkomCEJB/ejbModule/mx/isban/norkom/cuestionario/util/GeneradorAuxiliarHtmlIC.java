/**
 * Isban Mexico
 *   Clase: ValidadorCuestionarioIP.java
 *   Descripcion: Componente para validaciones del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.generador.GeneradorCuestionarioGenerico;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;
import mx.isban.norkom.cuestionario.manejador.ManejadorRadio;
import mx.isban.norkom.cuestionario.manejador.ManejadorSelect;
import mx.isban.norkom.ws.dto.RelacionadoDTO;

public final class GeneradorAuxiliarHtmlIC {
	/**
	 * Inicio de TD para tablas HTML
	 */
	private static final String TD_INICIO = "<TD>";
	
	/**
	 * Fin de TD para tablas HTML
	 */
	private static final String TD_FINAL = "</TD>";
	
	/**
	 * Inicio de TH para tablas HTML
	 */
	private static final String TH_INICIO = "<TH>";
	
	/**
	 * Fin de TH para tablas HTML
	 */
	private static final String TH_FINAL = "</TH>";
	
	/**
	 * Inicio de TR para tablas HTML
	 */
	private static final String TR_INICIO = "<TR>";
	
	/**
	 * Fin de TR para tablas HTML
	 */
	private static final String TR_FINAL = "</TR>";
	
	/**Constructor*/
	private GeneradorAuxiliarHtmlIC(){
		
	}
	/**
	 * Generar codigo HTML para la lista de accionistas
	 * @param accionistas Accionistas con los cuales se va a generar la informacion
	 * @param preguntaHtml Codigo HTML correspondiente a un TABLE
	 */
	public static void generarTablaAccionistas(List<RelacionadoDTO> accionistas, StringBuilder preguntaHtml){
		if(accionistas != null && !accionistas.isEmpty()) {
			
			preguntaHtml.append("<TABLE class='table_wrap'>");
			
			preguntaHtml.append(TR_INICIO);
			preguntaHtml.append(TH_INICIO).append("Nombre").append(TH_FINAL);
			preguntaHtml.append(TH_INICIO).append("Fecha Nacimiento").append(TH_FINAL);
			preguntaHtml.append(TH_INICIO).append("Pa&iacute;s Nacionalidad").append(TH_FINAL);
			preguntaHtml.append(TH_INICIO).append("Tipo de Persona").append(TH_FINAL);
			preguntaHtml.append(TH_INICIO).append("% Participaci&oacute;n").append(TH_FINAL);
			preguntaHtml.append(TR_FINAL);
			
			for(RelacionadoDTO accionista : accionistas){
				preguntaHtml.append(TR_INICIO);
				preguntaHtml.append(TD_INICIO).append(accionista.getNombreCompleto()).append(TD_FINAL);
				preguntaHtml.append(TD_INICIO).append(accionista.getFechaNacimiento()).append(TD_FINAL);
				preguntaHtml.append(TD_INICIO).append(accionista.getCodPaisNacionalidad()).append(TD_FINAL);
				preguntaHtml.append(TD_INICIO).append(accionista.getTipoPersona()).append(TD_FINAL);
				preguntaHtml.append(TD_INICIO).append(accionista.getPorcentajeParticipacion()).append("%").append(TD_FINAL);
				preguntaHtml.append(TR_FINAL);
			}
			
			preguntaHtml.append("</TABLE>");
		} else {
			preguntaHtml.append("No se han proporcionado accionistas con un participaci&oacute;n igual o superior al 25%.");
		}
	}
	
	/**
	 * Agrega las preguntas correspondientes al origen de los recursos seleccionado
	 * @param preguntaHtml String con el HTML de las preguntas
	 * @param preguntas Preguntas que se van a mostrar
	 * @param opcionesPreguntasOrigenRec Opciones que se van a manejar en la pregunta en caso de que sea RADIO o SELECT 
	 */
	public static void agregarPreguntasOrigenRecursos(StringBuilder preguntaHtml, List<PreguntaDTO> preguntas, Map<String, ResponseOpcionesPregunta> opcionesPreguntasOrigenRec){
		if(preguntas == null || preguntas.isEmpty()) {
			preguntaHtml.append("No hay preguntas de Informaci\u00F3n Adicional para el Origen de los Recursos");
		}
		
		for(PreguntaDTO preguntaDTO : preguntas){
			preguntaHtml.append(preguntaDTO.getPregunta());
			preguntaHtml.append("<BR>");
			if(GeneradorCuestionarioGenerico.CAMPO_SELECT.equals(preguntaDTO.getTipoPregunta()) || 
					GeneradorCuestionarioGenerico.CAMPO_SELECT_MUL.equals(preguntaDTO.getTipoPregunta()) || 
					GeneradorCuestionarioGenerico.CAMPO_SELECT_AUT.equals(preguntaDTO.getTipoPregunta())) 
			{//Seccion para generar campo html tipo SELECT
				preguntaHtml.append(ManejadorSelect.generarSelect(preguntaDTO, opcionesPreguntasOrigenRec.get(preguntaDTO.getAbreviatura()), StringUtils.EMPTY, new AtributosTagHtmlDTO()));
			} else if(GeneradorCuestionarioGenerico.CAMPO_RADIO.equals(preguntaDTO.getTipoPregunta())) {
				preguntaHtml.append(ManejadorRadio.generarRadios(preguntaDTO, opcionesPreguntasOrigenRec.get(preguntaDTO.getAbreviatura()), StringUtils.EMPTY, new AtributosTagHtmlDTO()));
			} else {
				preguntaHtml.append(ManejadorInput.generarCampoLibre(preguntaDTO, new AtributosTagHtmlDTO(), StringUtils.EMPTY));
			}
			preguntaHtml.append("<BR>");
		}
	}
}
