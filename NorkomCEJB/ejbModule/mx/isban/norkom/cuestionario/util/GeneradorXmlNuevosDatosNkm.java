/**
  * Isban Mexico NorkomCEJB
  *   Clase:  CreaFormatoXML.java
  *   Descripcion: clase de utileria para generar estructura 
  *   			  XML de cuestionario IP y enviarla el WS de Norkom
  *
  *   Control de Cambios:
  *   1.0 31-03-2012 Stefanini (LFER) - Creacion
  */
package mx.isban.norkom.cuestionario.util;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

/**
 * @author Leopoldo F Espinosa R 31/03/2015
 *
 */
public class GeneradorXmlNuevosDatosNkm extends CreaFormatoRelacionadosXML {
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOG = Logger.getLogger(GeneradorXmlNuevosDatosNkm.class);
	
	/**
	 * Metodo principal para la generacion del formtao XMl de el cuestionario IP
	 * @param cuestionarioDTO cuestionarioDTO CuestionarioIPDTO con informacion del cuestionario
	 * @return Cadena en formato xml para enviar a WS
	 */
	public String creaFormatoXML(CuestionarioWSDTO cuestionarioDTO) {
		String xmlNkm = StringUtils.EMPTY;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			/** RAIZ TXN **/
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("txn");
			doc.appendChild(rootElement);
			/** RAIZ FORMULARIO IP **/
			addFormulario(cuestionarioDTO, doc, rootElement);
			
			xmlNkm = creaFormatoXML(doc);
		} catch (ParserConfigurationException e) {
			LOG.error("No fue posible generar el XML, se regresa valor vacio", e);
		} catch (TransformerException e) {
			LOG.error("No fue posible generar el XML, se regresa valor vacio", e);
		}
 
		return xmlNkm;
	}
	
	/**
	 * METODO PARA ARMAR FORMULARIO_IP
	 * @param cuestionarioDTO de addFormulario a agregar
	 * @param doc de addFormulario a agregar
	 * @param raiz de addFormulario a agregar
	 */
	private void addFormulario(CuestionarioWSDTO cuestionarioDTO, Document doc, Element raiz){
		Element formulario = addElementoPadre(doc, raiz, "DatosAsignacion");
		
		addElementoHijo(doc, formulario, "NumeroContrato", cuestionarioDTO.getNumeroContrato());
		addElementoHijo(doc, formulario, "IndicadorUPLD", cuestionarioDTO.getIndicadorUpld());
		addElementoHijo(doc, formulario, "IndicadorNivelRiesgo", cuestionarioDTO.getNivelRiesgo());
		addElementoHijo(doc, formulario, "CodigoProducto", cuestionarioDTO.getCodigoProducto());
		addElementoHijo(doc, formulario, "DescripcionProducto", cuestionarioDTO.getDescProducto());		
		addElementoHijo(doc, formulario, "CodigoSubproducto", cuestionarioDTO.getCodigoSubproducto());
		addElementoHijo(doc, formulario, "DescripcionSubproducto", cuestionarioDTO.getDescSubproducto());
		addElementoHijo(doc, formulario, "Sucursal", cuestionarioDTO.getCodigoSucursal());
		addElementoHijo(doc, formulario, "BUC", PersonasMQUtil.buc8posiciones(cuestionarioDTO.getCodigoCliente())); 
		addElementoHijo(doc, formulario, "FechaContratacion", cuestionarioDTO.getIdCuestionario().substring(8));
		addElementoHijo(doc, formulario, "IdCuestionario", cuestionarioDTO.getIdCuestionario());
	}
	
	/***
	 * creacion de Formato XML
	 * @param doc Documento para la conversion de XML
	 * @return String con el formtao de XML
	 * @throws TransformerException con mensaje de error
	 */
	private String creaFormatoXML(Document doc) throws TransformerException{
		// Intancia usada para realizar la implementacion de una Fabrica de Tranformacion
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		// Tranforma un Source tree en un Result tree
		Transformer transformer = transformerFactory.newTransformer();
		// Transforma el Source tree en forma de un Document Object Model (DOM) tree.
		DOMSource source = new DOMSource(doc);
		// Instancia generada para convertir el Result tree en String
		StringWriter xml = new StringWriter();
		// Intancia creada para asignar el Result tree a "xml"
		StreamResult result =  new StreamResult(xml);
		// Se realiza la tranformacion de Source tree a Result tree
		transformer.transform(source, result);
		// Se elimina el encabezado de xml para dejar solo la estructura en strResult
		
		return new StringBuilder(xml.toString().substring(xml.toString().indexOf("?>")+2)).toString();
	}
}
