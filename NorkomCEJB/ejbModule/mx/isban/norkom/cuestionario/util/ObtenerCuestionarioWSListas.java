/**
 *   Isban Mexico
 *   Clase: ObtenerCuestionarioWSListas.java
 *   Descripcion: Clase para guardar las listas de cuetionarios del WS.
 *
 *   Control de Cambios:
 *   1.0 Feb 10, 2015 jgarcia - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.isban.norkom.cuestionario.bean.rpt.ClientesBean;
import mx.isban.norkom.cuestionario.bean.rpt.MontosBean;
import mx.isban.norkom.cuestionario.bean.rpt.ProveedoresBean;
import mx.isban.norkom.cuestionario.bean.rpt.RefBancariasBean;
import mx.isban.norkom.cuestionario.bean.rpt.ReferenciasBean;
import mx.isban.norkom.cuestionario.bean.rpt.SociedadesBean;


public class ObtenerCuestionarioWSListas{

	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 7135944087854413325L;
	
	/**
	 * Metodo para obtener la lista de montos
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean con resultado
	 */
	public List<MontosBean> listasMontos(Map<String, Object> respuestas)
	{
		List<MontosBean> lstOtrosIngresos = new ArrayList<MontosBean>();
		MontosBean montos1 = new MontosBean();
		MontosBean montos2 = new MontosBean();
		
		double monto = 0;
		String format = "";
		if(respuestas.containsKey("lstOtrosIngre.strFuente"))
		{
			montos1.setStrFuente(respuestas.get("lstOtrosIngre.strFuente").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstOtrosIngre.strMontoDolar"))
		{
			monto = Double.parseDouble(respuestas.get("lstOtrosIngre.strMontoDolar").toString().toUpperCase());
			BigDecimal formato = new BigDecimal(monto);
			format = Utilerias.getFormattedCurrency(formato);
			montos1.setStrMonto(format);
		}
		if(respuestas.containsKey("lstOtrosIngre.strMonto"))
		{
			monto = Double.parseDouble(respuestas.get("lstOtrosIngre.strMonto").toString().toUpperCase());
			BigDecimal formato = new BigDecimal(monto);
			format = Utilerias.getFormattedCurrency(formato);
			montos1.setStrMonto(format);
		}
		if(respuestas.containsKey("lstOtrosIngre.strFuente2"))
		{
			montos2.setStrFuente(respuestas.get("lstOtrosIngre.strFuente2").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstOtrosIngre.strMontoDolar2"))
		{
			monto = Double.parseDouble(respuestas.get("lstOtrosIngre.strMontoDolar2").toString().toUpperCase());
			BigDecimal formato = new BigDecimal(monto);
			format = Utilerias.getFormattedCurrency(formato);
			montos2.setStrMonto(format);
		}
		if(respuestas.containsKey("lstOtrosIngre.strMonto2"))
		{
			monto = Double.parseDouble(respuestas.get("lstOtrosIngre.strMonto2").toString().toUpperCase());
			BigDecimal formato = new BigDecimal(monto);
			format = Utilerias.getFormattedCurrency(formato);
			montos2.setStrMonto(format);
		}
		lstOtrosIngresos.add(montos1);
		lstOtrosIngresos.add(montos2);
		return lstOtrosIngresos;
	}
	
	/**
	 * Metodo para obtener la lista de referencias
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean con resultado
	 */
	public List<ReferenciasBean> listasReferencias(Map<String, Object> respuestas)
	{
		ReferenciasBean referencia1 = new ReferenciasBean();
		ReferenciasBean referencia2 = new ReferenciasBean();
		List<ReferenciasBean> lstRefPersonales = new ArrayList<ReferenciasBean>();
		if(respuestas.containsKey("lstRef.strNomRefPersonal1"))
		{
			referencia1.setStrNomRefPersonal(respuestas.get("lstRef.strNomRefPersonal1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRef.strDomRefPersonal1"))
		{
			referencia1.setStrDomRefPersonal(respuestas.get("lstRef.strDomRefPersonal1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRef.strTelRefPersonal1"))
		{
			referencia1.setStrTelRefPersonal(respuestas.get("lstRef.strTelRefPersonal1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRef.strNomRefPersonal2"))
		{
			referencia2.setStrNomRefPersonal(respuestas.get("lstRef.strNomRefPersonal2").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRef.strDomRefPersonal2"))
		{
			referencia2.setStrDomRefPersonal(respuestas.get("lstRef.strDomRefPersonal2").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRef.strTelRefPersonal2"))
		{
			referencia2.setStrTelRefPersonal(respuestas.get("lstRef.strTelRefPersonal2").toString().toUpperCase());
		}
		lstRefPersonales.add(referencia1);
		lstRefPersonales.add(referencia2);
		return lstRefPersonales;
	}
	
	/**
	 * Metodo para obtener la lista de sociedades
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean con resultado
	 */
	public List<SociedadesBean> listasSociedades(Map<String, Object> respuestas)
	{
		SociedadesBean sociedad1 = new SociedadesBean();
		SociedadesBean sociedad2 = new SociedadesBean();
		SociedadesBean sociedad3 = new SociedadesBean();
		SociedadesBean sociedad4 = new SociedadesBean();
		List<SociedadesBean> lstSociedades = new ArrayList<SociedadesBean>();
		if(respuestas.containsKey("lstSoci.strNomSociedad1") && !"NA".equalsIgnoreCase(respuestas.get("lstSoci.strNomSociedad1").toString()))
		{
			sociedad1.setStrNomSociedad(respuestas.get("lstSoci.strNomSociedad1").toString().toUpperCase());
			lstSociedades.add(sociedad1);
		}
		if(respuestas.containsKey("lstSoci.strNomSociedad2") && !"NA".equalsIgnoreCase(respuestas.get("lstSoci.strNomSociedad2").toString()))
		{
			sociedad2.setStrNomSociedad(respuestas.get("lstSoci.strNomSociedad2").toString().toUpperCase());
			lstSociedades.add(sociedad2);
		}
		if(respuestas.containsKey("lstSoci.strNomSociedad3") && !"NA".equalsIgnoreCase(respuestas.get("lstSoci.strNomSociedad3").toString()))
		{
			sociedad3.setStrNomSociedad(respuestas.get("lstSoci.strNomSociedad3").toString().toUpperCase());
			lstSociedades.add(sociedad3);
		}
		if(respuestas.containsKey("lstSoci.strNomSociedad4") &&  !"NA".equalsIgnoreCase(respuestas.get("lstSoci.strNomSociedad4").toString()))
		{
			sociedad4.setStrNomSociedad(respuestas.get("lstSoci.strNomSociedad4").toString().toUpperCase());
			lstSociedades.add(sociedad4);
		}
		return lstSociedades;
	}
	
	/**
	 * Metodo para obtener la lista de bancos
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean con resultado
	 */
	public List<RefBancariasBean> listasBancos(Map<String, Object> respuestas)
	{
		List<RefBancariasBean> lstRefBancos = new ArrayList<RefBancariasBean>();
		RefBancariasBean bancos1 = new RefBancariasBean();
		RefBancariasBean bancos2 = new RefBancariasBean();
		if(respuestas.containsKey("lstRefBancos.strNomBanco1"))
		{
			bancos1.setStrNomBanco(respuestas.get("lstRefBancos.strNomBanco1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRefBancos.strNumCuenta1"))
		{
			bancos1.setStrNumCuenta(respuestas.get("lstRefBancos.strNumCuenta1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRefBancos.strNomBanco2"))
		{
			bancos2.setStrNomBanco(respuestas.get("lstRefBancos.strNomBanco2").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstRefBancos.strNumCuenta2"))
		{
			bancos2.setStrNumCuenta(respuestas.get("lstRefBancos.strNumCuenta2").toString().toUpperCase());
		}
		lstRefBancos.add(bancos1);
		lstRefBancos.add(bancos2);
		return lstRefBancos;
	}
	
	/**
	 * Metodo para obtener la lista de clientes
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean con resultado
	 */
	public List<ClientesBean> listasClientes(Map<String, Object> respuestas)
	{
		List<ClientesBean> lstClientes  = new ArrayList<ClientesBean>();
		ClientesBean cliente1 = new ClientesBean("");
		ClientesBean cliente2 = new ClientesBean("");
		ClientesBean cliente3 = new ClientesBean("");
		if(respuestas.containsKey("lstCliente.strNomCliente1"))
		{
			cliente1.setStrNomCliente(respuestas.get("lstCliente.strNomCliente1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstCliente.strNomCliente2"))
		{
			cliente2.setStrNomCliente(respuestas.get("lstCliente.strNomCliente2").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstCliente.strNomCliente3"))
		{
			cliente3.setStrNomCliente(respuestas.get("lstCliente.strNomCliente3").toString().toUpperCase());
		}
		lstClientes.add(cliente1);
		lstClientes.add(cliente2);
		lstClientes.add(cliente3);
		return lstClientes;
	}
	
	/**
	 * Metodo para obtener la lista de proveedores
	 * @param respuestas Mapa con las respuestas
	 * @return ICListasBean con resultado
	 */
	public List<ProveedoresBean> listasProveedores(Map<String, Object> respuestas)
	{
		List<ProveedoresBean> lstProveedores = new ArrayList<ProveedoresBean>();
		ProveedoresBean proveedor1 = new ProveedoresBean();
		ProveedoresBean proveedor2 = new ProveedoresBean();
		ProveedoresBean proveedor3 = new ProveedoresBean();
		if(respuestas.containsKey("lstProveedor.strNomProveedor1"))
		{
			proveedor1.setStrNomProveedor(respuestas.get("lstProveedor.strNomProveedor1").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstProveedor.strNomProveedor2"))
		{
			proveedor2.setStrNomProveedor(respuestas.get("lstProveedor.strNomProveedor2").toString().toUpperCase());
		}
		if(respuestas.containsKey("lstProveedor.strNomProveedor3"))
		{
			proveedor3.setStrNomProveedor(respuestas.get("lstProveedor.strNomProveedor3").toString().toUpperCase());
		}
		lstProveedores.add(proveedor1);
		lstProveedores.add(proveedor2);
		lstProveedores.add(proveedor3);
		return lstProveedores;
	}
}
