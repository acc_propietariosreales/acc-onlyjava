/**
 *   Isban Mexico
 *   Clase: PersonasMQUtil.java
 *   Descripcion: Clase para guardar las constantes y metodos utilizadas en MQ
 *
 *   Control de Cambios:
 *   1.0 Feb 10, 2015 jgarcia - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import java.util.ArrayList;
import java.util.List;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.DireccionPE58DTO;
import mx.isban.norkom.cuestionario.dto.DireccionPE71DTO;
import mx.isban.norkom.cuestionario.dto.IndicadoresPE71DTO;
import mx.isban.norkom.cuestionario.dto.RequestODF3DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP2DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP3DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE58DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE71DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE71DTO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class PersonasMQUtil {
	
	/**
	 * serialVersionUID  de tipo long
	 */
	private static final long serialVersionUID = -7191536979836931070L;

	/**
	 * COPY para los relacionados de la trama PE58
	 */
	private static final String COPY_PE58 = "DCPEM224A P";
	
	/**
	 * COPY para los relacionados de la trama PE71
	 */
	private static final String COPY_PE71 = "DCPEM28C  P";
	
	/**
	 * Identificador de las referencias morales de tipo Bancarias
	 */
	private static final String ID_REFERENCIAS_BANCARIAS = "30";
	
	/** 
	 * Variable utilizada para escribir el log de la aplicacion 
	 */
	private static final Logger LOG = Logger.getLogger(PersonasMQUtil.class);
	
	/**
	 * Genera la trama con la cual se va a ejecutar la transaccion ODP2
	 * @param input Objeto con la informacion para la trama
	 * @return Trama ODP2 generada
	 * @throws BusinessException En caso de error con la informacion
	 */
	public String generaTramaODP2(RequestODP2DTO input) throws BusinessException {
		StringBuffer trama = new StringBuffer();
		
		if(input.getBuc() == null || input.getBuc().isEmpty()) {
			throw new BusinessException("ERRODP2001");
		}
		
		if(input.getIndicador() == null || input.getIndicador().isEmpty()) {
			throw new BusinessException("ERRODP2002");
		}
		
		if(input.getValorIndicador() == null || input.getValorIndicador().isEmpty()) {
			throw new BusinessException("ERRODP2003");
		}
		
		trama.append(StringUtils.leftPad(buc8posiciones(input.getBuc()), 8, "0"));
		trama.append(StringUtils.leftPad(input.getIndicador(), 3));
		trama.append(StringUtils.leftPad(input.getValorIndicador(), 1));
		
		return trama.toString();
	}
	
	/**
	 * Genera la trama con la cual se va a ejecutar la transaccion ODP3
	 * @param input Objeto con la informacion para la trama
	 * @return Trama ODP3 generada
	 * @throws BusinessException En caso de error con la informacion
	 */
	public String generaTramaODP3(RequestODP3DTO input) throws BusinessException {
		StringBuffer trama = new StringBuffer();
		
		if(input.getBuc() == null || input.getBuc().isEmpty()) {
			throw new BusinessException("ERRODP2001");
		}
		
		trama.append(StringUtils.leftPad(buc8posiciones(input.getBuc()), 8, "0"));
		trama.append(StringUtils.leftPad(input.getCondicionCliente(), 3, " "));
		
		return trama.toString();
	}
	
	/**
	 * Genera la trama con la cual se va a ejecutar la transaccion ODF3
	 * @param input Objeto con la informacion para la trama
	 * @return Trama ODF3 generada
	 * @throws BusinessException En caso de error con la informacion
	 */
	public String generaTramaODF3(RequestODF3DTO input) throws BusinessException {
		StringBuffer trama = new StringBuffer();
		
		if(input.getCodigoCliente() == null || input.getCodigoCliente().isEmpty()) {
			throw new BusinessException("ERRODP2001");
		}
		
		trama.append(StringUtils.leftPad(buc8posiciones(input.getCodigoCliente()), 8, "0"));
		trama.append(StringUtils.leftPad(StringUtils.EMPTY, 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.EMPTY, 81, " "));
		trama.append(StringUtils.leftPad(StringUtils.EMPTY, 26, " "));
		
		return trama.toString();
	}
	
	/**
	 * Genera la trama con la cual se va a ejecutar la transaccion PE71
	 * @param input Objeto con la informacion para la trama
	 * @return Trama PE71 generada
	 * @throws BusinessException En caso de error con la informacion
	 */
	public String generaTramaPE71(RequestPE71DTO input) throws BusinessException {
		StringBuffer trama = new StringBuffer();
		DireccionPE71DTO direccion = input.getDireccion();
		IndicadoresPE71DTO indicadores = input.getIndicadores();
		
		if(direccion == null){
			direccion = new DireccionPE71DTO();
		}
		
		if(indicadores == null){
			indicadores = new IndicadoresPE71DTO();
		}
		
		if(input.getNumeroPersona() == null || input.getNumeroPersona().isEmpty()) {
			throw new BusinessException("ERPE7101");
		}
		
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(buc8posiciones(input.getNumeroPersona())), 8, "0"));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getTipoDocumento()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNumeroDocumento()), 11, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getSecuenciaDocumento()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getSecuenciaContacto()), 3, "0"));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNombreFantasia()), 30, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getPrimerApellido()), 20, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getSegundoApellido()), 20, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNombre()), 40, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getFechaConstitucion()), 10, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getTipoPersona()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getEstadoPersona()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getCondicion()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNivelAcceso()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorRelaciones()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getTenerContratos()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getPertenenciaGrupos()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorAvisos()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorUsoUno()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorUsoDos()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorUsoTres()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorUsoCuatro()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(indicadores.getIndicadorUsoCinco()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getTipoVia()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCalle()), 50, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getObservacionesUno()), 100, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getObservacionesDos()), 100, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getBloque()), 10, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getLocalidad()), 7, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getComuna()), 5, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCodigoPostal()), 8, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getRutaCartero()), 9, "0"));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCodigoProvincia()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCodigoProvinciaDos()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getMarcaNormalizacion()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getTimeStamp()), 26, " "));
		
		return trama.toString();
	}
	
	/**
	 * Desentrama la cadena de respuesta de la transaccion PE71 y genera la lista de referencias bancarias
	 * @param tramaPE71 Trama PE71 de respuesta
	 * @return lista de referencias bancarias
	 */
	public List<ResponsePE71DTO> desentramaPE71(String tramaPE71) {
		List<ResponsePE71DTO> referencias = new ArrayList<ResponsePE71DTO>();
		int longitudCopy = COPY_PE71.length();
//		String tramaPE71 = "@112345@DCPEM28C  P000184430014004                                                                                                                                 01                                                                                        ANTELMA ACEVEDO PEREZ             AL   1          EX01GERARDO RODRI   32                                                                                                                                                                                  2014-11-13-07.48.26.873298�@DCPEM28C  P000184430024005                                                                                                                                 12                                                                                        HERIBERTO SUAREZ AVILA            AL   2          EX01GERARDO RODRI   01                                                                                                                                                                                  2014-11-13-08.11.50.280050�@DCPEM28C  P000184430034005                                                                                                                                 13                                                                                        HERIBERTO SUAREZ AVILA            AR   2          MX11GERARDO RODRI   01                                                                                                                                                                                  2014-11-13-08.11.50.282260�@DCPEM28C  P000184430044006                                                                                                                                 14                                                                                        HERIBERTO SUAREZ AVILA            AF   3          MX06GERARDO RODRI   01                                                                                                                                                                                  2014-11-13-08.11.50.282297�@DCPEM28C  P000184430054006                                                                                                                                 15                                                                                        HERIBERTO SUAREZ AVILA            AC   3          MX04GERARDO RODRI   01                                                                                                                                                                                  2014-11-13-08.11.50.282417�@DCPEM28C  P000184430064007                                                                                                                                 16                                                                                        HERIBERTO SUAREZ AVILA            AD   4          MX13GERARDO RODRI   01                                                                                                                                                                                  2014-11-13-08.11.50.282454�@DCPEM28C  P000184430074008                                                                                                                                 17                                                                                        HERIBERTO SUAREZ AVILA                            MX9911000358949     01                                                                                                                                                                                  2014-11-13-08.11.50.284264�@DCPEM28C  P000184430084008                                                                                                                                 18                                                                                        HERIBERTO SUAREZ AVILA                            MX9911000358949     01                                                                                                                                                                                  2014-11-13-08.11.50.284327�@DCPEM28C  P000184430094008                                                                                                                                 19                                                                                        HERIBERTO SUAREZ AVILA                            MX9911000358949     01                                                                                                                                                                                  2014-11-13-08.11.50.284355�@DCPEM28C  P000184430104008                                                                                                                                 10                                                                                        HERIBERTO SUAREZ AVILA                            MX9911000358983     01                                                                                                                                                                                  2014-11-13-08.11.50.284372�@DCPEM28C  P000184430114004                                                                                                                                 11                                                                                        SADFSAF                                           MX99                06                                                                                                                                                                                  2014-11-13-08.11.50.284443�@DCPEM28C  P000184430124004                                                                                                                                 12                                                                                        SDAF                                              MX99                06                                                                                                                                                                                  2014-11-13-08.11.50.285420�@DCPEM28C  P000184430134008                                                                                                                                 13                                                                                        HERIBERTO SUAREZ AVILA                            MX9986475210079     01                                                                                                                                                                                  2014-11-13-08.11.50.285467�@DCPEM28C  P000184430144008                                                                                                                                 14                                                                                        BENJA RUIZ LOPEZ                                  MX9986475210079     01                                                                                                                                                                                  2014-11-13-08.11.50.285485�@DCPEM28C  P000184430154008                                                                                                                                 15                                                                                        SAUL RUIZ LOPEZ                                   MX9986475210079     01                                                                                                                                                                                  2014-11-13-08.11.50.285501�@DCPEM28B  P00018443DI00018443   01                                 ACEVEDO             PEREZ               ANTELMA                                 1957-06-24F010CLI0SSNSN1   00DON CARLO MZ 4 LT 4                                                             09AGRICOLA METROPOLITANA                                                                              1957-06-24                                                                    00000010001300013280         DF052 2013-07-11-10.30.25.533087�?";
		
		if(tramaPE71 == null || tramaPE71.isEmpty()) {
			return referencias;
		}
		
		int indiceCopy = tramaPE71.indexOf(COPY_PE71);
		if(indiceCopy > 0) {
			indiceCopy = indiceCopy + longitudCopy;
		}
		while(indiceCopy > 0){
			ResponsePE71DTO response = new ResponsePE71DTO();
			
			response.setNumeroPersona(tramaPE71.substring(indiceCopy, indiceCopy + 8));
			indiceCopy = indiceCopy + 8;
			response.setSecuenciaContacto(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 3)));
			indiceCopy = indiceCopy + 3;
			response.setTipoContacto(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 2)));
			indiceCopy = indiceCopy + 2;
			response.setAccesibilidadContacto(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 2)));
			indiceCopy = indiceCopy + 2;
			response.setPrimerApellido(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 20)));
			indiceCopy = indiceCopy + 20;
			response.setSegundoApellido(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 20)));
			indiceCopy = indiceCopy + 20;
			response.setNombre(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 40)));
			indiceCopy = indiceCopy + 40;
			response.setTipoDocumento(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 2)));
			indiceCopy = indiceCopy + 2;
			response.setNumeroDocumento(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 11)));
			indiceCopy = indiceCopy + 11;
			response.setMarcaPpal(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 1)));
			indiceCopy = indiceCopy + 1;
			response.setNombreSecretaria(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 30)));
			indiceCopy = indiceCopy + 30;
			response.setCargoContacto(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 2)));
			indiceCopy = indiceCopy + 2;
			response.setDepartamentoContacto(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 3)));
			indiceCopy = indiceCopy + 3;
			
			indiceCopy = obtenerComplementoPE71(response, indiceCopy, tramaPE71);
			
			LOG.debug(response.toString());
			
			if(ID_REFERENCIAS_BANCARIAS.equals(response.getTipoContacto())) {
				referencias.add(response);
			}
			
			indiceCopy = obtenerSiguienteValorIndicePE71(tramaPE71, longitudCopy, indiceCopy);
		}
		
		return referencias;
	}

	
	/**
	 * Genera la trama con la cual se va a ejecutar la transaccion PE58
	 * @param input Objeto con la informacion para la trama
	 * @return Trama PE58 generada
	 * @throws BusinessException En caso de error con la informacion
	 */
	public String generaTramaPE58(RequestPE58DTO input) throws BusinessException {
		StringBuffer trama = new StringBuffer();
		DireccionPE58DTO direccion = input.getDireccion();
		
		if(direccion == null){
			direccion = new DireccionPE58DTO();
		}
		
		if(input.getNumeroPersona() == null || input.getNumeroPersona().isEmpty()) {
			throw new BusinessException("ERPE5801");
		}
		
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(buc8posiciones(input.getNumeroPersona())), 8, "0"));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getTipoDocumento()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNumeroDocumento()), 11, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getSecuenciaDocumento()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getSecuenciaReferencia()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNombreFantasia()), 30, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getPrimerApellido()), 20, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getSegundoApellido()), 20, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNombre()), 40, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getFechaNacimiento()), 10, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getTipoPersona()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getEstadoPersona()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getCondicion()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getNivelAcceso()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getTipoVia()), 2, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCalle()), 50, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getBloque()), 10, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getIndicadorUsoUno()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getIndicadorUsoDos()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getIndicadorUsoTres()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getIndicadorUsoCuatro()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getIndicadorUsoCinco()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getObservacionesUno()), 100, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getObservacionesDos()), 100, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(input.getMarcaNormalizacion()), 1, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getLocalidad()), 7, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getComuna()), 5, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCodigoPostal()), 8, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCodigoProvincia()), 3, " "));
		trama.append(StringUtils.leftPad(StringUtils.trimToEmpty(direccion.getCodigoPais()), 3, " "));
		
		return trama.toString();
	}

	/**
	 * Desentrama la cadena de respuesta de la transaccion PE58 y genera la lista de referencias personales
	 * @param tramaPE58 Trama PE58 de respuesta
	 * @return lista de referencias personales
	 */
	public List<ResponsePE58DTO> desentramaPE58(String tramaPE58, List<String> codigosValidos) {
		List<ResponsePE58DTO> referenciasBancarias = new ArrayList<ResponsePE58DTO>();
		int longitudCopy = COPY_PE58.length();
//		String tramaPE58 = "@112345@DCPEM224A P00018443001             10PATERNO CNT         MATERNO CNT         JUAN BENJAMIN                           N0155                                                                                   0002014-11-16-18.49.19.805948                                                                                                                                                                                                                                                                                                            �@DCPEM224A P00018443002             10HERI                SUAREZ              AVIAL                                   S                                                                                       0092014-11-16-18.49.19.807430                                                                                                                                                                                                                                                                                                            �@DCPEM224A P00018443003             30                                        BANAMEX                                 N                                                                                       0092014-11-16-18.49.19.807467                                                                                                                                                                         111111111                                                                                                                          �@DCPEM224A P00018443004             30                                        SANTANDER                               N                                                                                       0092014-11-16-18.49.19.807485                                                                                                                                                                         22222222222                                                                                                                        �@DCPEM2246 P00018443DI00018443   01                                 ACEVEDO             PEREZ               ANTELMA                                 1957-06-24F010CLI000DON CARLO MZ 4 LT 4                                         N1                                 09AGRICOLA METROPOLITANA                                                                           1957-06-24                                                              00000010001300013280DF 052�?";
		
		if(tramaPE58 == null || tramaPE58.isEmpty()) {
			return referenciasBancarias;
		}
		
		int indiceCopy = tramaPE58.indexOf(COPY_PE58);
		if(indiceCopy > 0) {
			indiceCopy = indiceCopy + longitudCopy;
		}
		while(indiceCopy > 0){
			ResponsePE58DTO response = new ResponsePE58DTO();
			
			response.setNumeroPersona(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 8)));
			indiceCopy = indiceCopy + 8;
			response.setNumeroSecuencia(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 3)));
			indiceCopy = indiceCopy + 3;
			response.setTipoDocumento(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 2)));
			indiceCopy = indiceCopy + 2;
			response.setNumeroDocumento(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 11)));
			indiceCopy = indiceCopy + 11;
			response.setTipoRelacion(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 2)));
			indiceCopy = indiceCopy + 2;
			response.setPrimerApellido(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 20)));
			indiceCopy = indiceCopy + 20;
			response.setSegundoApellido(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 20)));
			indiceCopy = indiceCopy + 20;
			response.setNombre(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 40)));
			indiceCopy = indiceCopy + 40;
			response.setTipoPersona(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 1)));
			indiceCopy = indiceCopy + 1;
			response.setPrefijoTelefono(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 7)));
			indiceCopy = indiceCopy + 7;
			response.setNumeroTelefono(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 10)));
			indiceCopy = indiceCopy + 10;
			response.setAnexos(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 20)));
			indiceCopy = indiceCopy + 20;
			response.setCorreoElectronico(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 50)));
			indiceCopy = indiceCopy + 50;
			response.setSecuenciaDomicilio(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 3)));
			indiceCopy = indiceCopy + 3;
			response.setTimeStamp(StringUtils.trimToEmpty(tramaPE58.substring(indiceCopy, indiceCopy + 26)));
			indiceCopy = indiceCopy + 26;
			response.setObservacionesUno(tramaPE58.substring(indiceCopy, indiceCopy + 100));
			indiceCopy = indiceCopy + 100;
			response.setObservacionesDos(tramaPE58.substring(indiceCopy, indiceCopy + 100));
			indiceCopy = indiceCopy + 100;
			response.setObservacionesTres(tramaPE58.substring(indiceCopy, indiceCopy + 100));
			indiceCopy = indiceCopy + 100;
			
			if(codigosValidos.contains(StringUtils.trimToEmpty(response.getTipoRelacion()))) {
				referenciasBancarias.add(response);
			}
			
			
			indiceCopy = obtenerSiguienteValorIndicePE58(tramaPE58, longitudCopy, indiceCopy);
		}
		
		return referenciasBancarias;
	}
	/**
	 * metodo para validar que solo sean 8 posiciones en el buc, si son mas se obtendran solo las ultimas 8 posiciones.
	 * @param buc buc a validar que no pase de 8 posiciones
	 * @return String del buc a 8 posiciones como maximo
	 */
	public static String buc8posiciones(String buc){
		if(buc!=null && buc.length()>8){
			buc=buc.substring(buc.length()-8,buc.length());
		}
		return buc;
	}
	
	/**
	 * 
	 * @param tramaPE58 trama PE58
	 * @param longitudCopy longitud del copy
	 * @param indiceActual indice actual
	 * @return int con resultado
	 */
	private int obtenerSiguienteValorIndicePE58(String tramaPE58, int longitudCopy, int indiceActual){
		int indiceCopy = tramaPE58.indexOf(COPY_PE58, indiceActual);
		if(indiceCopy > 0) {
			indiceCopy = indiceCopy + longitudCopy;
		}
		
		return indiceCopy;
	}
	/**
	 * 
	 * @param response ResponsePE71DTO con datos de entrada
	 * @param indiceCopy indicador del copy
	 * @param tramaPE71 trama pe71
	 * @return int con resultado
	 */
	private int obtenerComplementoPE71(ResponsePE71DTO response, int indiceCopy, String tramaPE71){
		response.setReferenciaDomicilio(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 2)));
		indiceCopy = indiceCopy + 2;
		response.setPrefijoTelefono(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 7)));
		indiceCopy = indiceCopy + 7;
		response.setNumeroTelefono(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 10)));
		indiceCopy = indiceCopy + 10;
		response.setNumeroInternoExterno(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 20)));
		indiceCopy = indiceCopy + 20;
		response.setCorreoContacto(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 50)));
		indiceCopy = indiceCopy + 50;
		response.setMarcaCliente(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 1)));
		indiceCopy = indiceCopy + 1;
		response.setObservacionesUno(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 100)));
		indiceCopy = indiceCopy + 100;
		response.setObservacionesDos(tramaPE71.substring(indiceCopy, indiceCopy + 100));
		indiceCopy = indiceCopy + 100;
		response.setObservacionesTres(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 50)));
		indiceCopy = indiceCopy + 50;
		response.setTimeStamp(StringUtils.trimToEmpty(tramaPE71.substring(indiceCopy, indiceCopy + 26)));
		indiceCopy = indiceCopy + 26;
		
		return indiceCopy;
	}
	/**
	 * obtenerSiguienteValorIndicePE71
	 * @param tramaPE71 trama de PE71
	 * @param longitudCopy longitud
	 * @param indiceActual indice actual
	 * @return int con resultado
	 */
	private int obtenerSiguienteValorIndicePE71(String tramaPE71, int longitudCopy, int indiceActual){
		int indiceCopy = tramaPE71.indexOf(COPY_PE71, indiceActual);
		if(indiceCopy > 0) {
			indiceCopy = indiceCopy + longitudCopy;
		}
		
		return indiceCopy;
	}
}
