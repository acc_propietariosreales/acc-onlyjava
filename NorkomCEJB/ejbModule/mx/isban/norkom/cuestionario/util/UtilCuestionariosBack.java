/**
 * Isban Mexico
 *   Clase: UtilCuestionarios.java
 *   Descripcion: Componente de utileria para los cuestionarios.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;


import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.FamiliarDTO;
import mx.isban.norkom.cuestionario.manejador.ManejadorInput;

public final class UtilCuestionariosBack {
	/**
	 * String FORMATO_VALOR_PARENTESCO
	 */
	private static final String FORMATO_VALOR_PARENTESCO = "%s%s%s%s";
	/**Constructor**/
	private UtilCuestionariosBack(){
		
	}
	
	
	/**
	 * @param cuestionarioIcBD CuestionarioDTO con los datos del cuestionario IC
	 * @param claveClienteCuestionario clave del clientecuestionario
	 * @param clienteCuestionario  objeto con datos de cuestionario del cliente
	 */
	public static void complementarCuestionarioIc(CuestionarioDTO cuestionarioIcBD, int claveClienteCuestionario, CuestionarioDTO clienteCuestionario){
		cuestionarioIcBD.setClaveClienteCuestionario(claveClienteCuestionario);
		cuestionarioIcBD.setIdCuestionario(clienteCuestionario.getIdCuestionario());
		cuestionarioIcBD.setCodigoPais(clienteCuestionario.getCodigoPais());
		cuestionarioIcBD.setCodigoNacionalidad(clienteCuestionario.getCodigoNacionalidad());
		cuestionarioIcBD.setActividadEspecifica(clienteCuestionario.getActividadEspecifica());		
		cuestionarioIcBD.setNivelRiesgo(clienteCuestionario.getNivelRiesgo());
		cuestionarioIcBD.setIndicadorUpld(clienteCuestionario.getIndicadorUpld());
		cuestionarioIcBD.setDummy(clienteCuestionario.isDummy());
	}
	
	/**
	 * Asigna el valor para las preguntas de Parentesco
	 * @param cuestionarioRespDTO Pregunta a la que se le asigna el valor
	 * @param nombreParametro Nombre del parametro del que se va a obtener la informacion
	 */
	public static void asignarValoresPreguntaParentesco(CuestionarioRespuestaDTO cuestionarioRespDTO, String nombreParametro){
		if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_PARENTESCO, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_PARENTESCO, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		} else if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_NOMBRE, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_NOMBRE, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		} else if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_DOMICILIO, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_DOMICILIO, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		} else if(nombreParametro.startsWith(FamiliarDTO.ABREV_FAM_FECHA_NAC, ManejadorInput.ID_INPUT_TEXT.length())) {
			cuestionarioRespDTO.setValorTexto(String.format(FORMATO_VALOR_PARENTESCO, 
					FamiliarDTO.ABREV_FAM_FECHA_NAC, nombreParametro.substring(10), FamiliarDTO.SEPARADOR_VALOR_CAMPO_ENTRADA, cuestionarioRespDTO.getValorTexto()));
		}
	}
	
	
	
}
