/**
 * Isban Mexico
 *   Clase: ValidadorCuestionarioIP.java
 *   Descripcion: Componente para validaciones del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

public class ValidadorCuestionario {
	/**
	 * String JS_FUNC_HAB_OTRAS_FUENTES_ING
	 */
	public static final String JS_FUNC_HAB_OTRAS_FUENTES_ING = " mostrarOtrasFuentesIngresos(); ";
	/**
	 * String JS_FUNC_DES_OTRAS_FUENTES_ING
	 */
	public static final String JS_FUNC_DES_OTRAS_FUENTES_ING = " ocultarOtrasFuentesIngresos(); ";
	/**
	 * String JS_FUNC_HAB_REFERENCIAS_BANC
	 */
	public static final String JS_FUNC_HAB_REFERENCIAS_BANC = " mostrarReferenciasBancarias(); ";
	/**
	 * String JS_FUNC_DES_REFERENCIAS_BANC
	 */
	public static final String JS_FUNC_DES_REFERENCIAS_BANC = " ocultarReferenciasBancarias(); ";
	/**
	 * String JS_FUNC_HAB_CONCUBINA
	 */
	public static final String JS_FUNC_HAB_CONCUBINA = " mostrarConcubina(); ";
	/**
	 * String JS_FUNC_DES_CONCUBINA
	 */
	public static final String JS_FUNC_DES_CONCUBINA = " ocultarConcubina(); ";
	/**
	 * String JS_FUNC_HAB_PART_ACCIONARIA
	 */
	public static final String JS_FUNC_HAB_PART_ACCIONARIA = " mostrarParticipacionesAccionarias(); ";
	/**
	 * String JS_FUNC_DES_PART_ACCIONARIA
	 */
	public static final String JS_FUNC_DES_PART_ACCIONARIA = " ocultarParticipacionesAccionarias(); ";
	
	/**
	 * String para agregar la funcion onclick
	 */
	protected static final String JS_FUNCION_ONCLICK = " onclick='%s' ";
	
	/**
	 * Codigo de pais Mexico
	 */
	protected static final String CODIGO_PAIS_MX = "052";
	
	/**
	 * String IND_ACTIVIDAD_SECTOR_PUBLICO
	 */
	protected static final String IND_ACTIVIDAD_SECTOR_PUBLICO = "P";
	/**
	 * String IND_ACTIVIDAD_SECTOR_PRIVADO
	 */
	protected static final String IND_ACTIVIDAD_SECTOR_PRIVADO = "R";
	/**
	 * String NIVEL_RIESGO_A1
	 */
	protected static final String NIVEL_RIESGO_A1 = "A1";
	
	/**
	 * Codigo HTML para el cierre de un tag de tipo DIV
	 */
	protected static final String HTML_DIV_CERRAR = "</DIV>";
	/**Objeto singleto de ValidadorCuestionario ***/
	private ValidadorCuestionario validadorCuest;
	/**constructor*/
	protected ValidadorCuestionario(){}
	/**get singleton
	 * @return validadorCuestionario singleton**/
	public ValidadorCuestionario getInstance(){
		if(validadorCuest==null){
			validadorCuest= new ValidadorCuestionario();
		}
		return validadorCuest;
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Informacion Laboral
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	protected static void agregarValidacionesInformacionLaboral(CuestionarioDTO cuestionario, PreguntaDTO pregunta, StringBuilder preguntaHtml) {
			ActividadEconomicaDTO actividadEspecifica = cuestionario.getActividadEspecifica();
			if(actividadEspecifica!=null && !IND_ACTIVIDAD_SECTOR_PUBLICO.equals(actividadEspecifica.getTipoSector()) 
					&& !IND_ACTIVIDAD_SECTOR_PRIVADO.equals(actividadEspecifica.getTipoSector())) 
			{
				int indiceInicioCambio = preguntaHtml.indexOf(">");
				int indiceFinalCambio = preguntaHtml.lastIndexOf(HTML_DIV_CERRAR);
				preguntaHtml.replace(indiceInicioCambio + 1, indiceFinalCambio, "Debido al sector de la Actividad Econ&oacute;mica Espec&iacute;fica no es necesaria esta pregunta.");
			}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Familiares de Personas Politicamente Expuestas
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	protected static void agregarValidacionFamiliarPep(CuestionarioDTO cuestionario, PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		if(NIVEL_RIESGO_A1.equals(cuestionario.getNivelRiesgo()) || !cuestionario.hasFamiliarPep()) {
			int indiceInicioCambio = preguntaHtml.indexOf(">");
			int indiceFinalCambio = preguntaHtml.lastIndexOf(HTML_DIV_CERRAR);
			preguntaHtml.replace(indiceInicioCambio + 1, indiceFinalCambio, "Debido a Informaci&oacute;n Preliminar no es necesario agregar familiares.");
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Clientes y Proveedores
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	protected static void agregarValidacionClientesProveedores(CuestionarioDTO cuestionario, PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		if(NIVEL_RIESGO_A1.equals(cuestionario.getNivelRiesgo())) {
			int indiceInicioCambio = preguntaHtml.indexOf(">");
			int indiceFinalCambio = preguntaHtml.lastIndexOf(HTML_DIV_CERRAR);
			preguntaHtml.replace(indiceInicioCambio + 1, indiceFinalCambio, "Debido al Nivel de Riesgo no es necesario proporcionar esta informaci&oacute;n.");
		}
	}
	
	/**
	 * Genera el codigo HTML del boton para agregar Familiares
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	protected static void agregarBotonFamiliarPep(CuestionarioDTO cuestionario, PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		int posicionSelect = preguntaHtml.indexOf("</select>");
		if(cuestionario.hasFamiliarPep() && posicionSelect > 0) {
			String inputHtml = 
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' value='Agregar Familiar' id='btnAgregarFamiliar' name='btnAgregarFamiliar' onclick=' agregarFamiliarPep();'><BR/>";
			preguntaHtml.insert(posicionSelect + 9, inputHtml);
			preguntaHtml.append("<div id='seccionPreguntas").append(pregunta.getAbreviatura()).append("'></div>");
		} else {
			int indiceInicioCambio = preguntaHtml.indexOf(">");
			int indiceFinalCambio = preguntaHtml.lastIndexOf(HTML_DIV_CERRAR);
			preguntaHtml.replace(indiceInicioCambio + 1, indiceFinalCambio, "Debido a Informaci&oacute;n Preliminar no es necesario llenar informaci&oacute;n sobre familiares.");
		}
	}
	/**
	 * @return the validadorCuest
	 */
	public ValidadorCuestionario getValidadorCuest() {
		return validadorCuest;
	}
	/**
	 * @param validadorCuest 
	 * el validadorCuest a agregar
	 */
	public void setValidadorCuest(ValidadorCuestionario validadorCuest) {
		this.validadorCuest = validadorCuest;
	}
}
