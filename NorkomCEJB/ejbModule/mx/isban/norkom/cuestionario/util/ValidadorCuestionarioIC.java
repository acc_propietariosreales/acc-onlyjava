/**
 * Isban Mexico
 *   Clase: ValidadorCuestionarioIP.java
 *   Descripcion: Componente para validaciones del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

public final class ValidadorCuestionarioIC extends ValidadorCuestionario {
	/**
	 * Constructor predefinido
	 */
	private ValidadorCuestionarioIC(){}
	
	/**
	 * Agrega validaciones a las preguntas HTML del Cuestionario IC
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Pregunta en codigo HTML
	 */
	public static void agregarValidacionesEspeciales(CuestionarioDTO cuestionario, PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		if("OTFI".equals(pregunta.getAbreviatura())) {
			agregarValidacionOtrasFuentesIngresos(pregunta, preguntaHtml);
		} else if("RFBC".equals(pregunta.getAbreviatura())) {
			agregarValidacionReferenciasBancarias(pregunta, preguntaHtml);
		} else if("INEC".equals(pregunta.getAbreviatura())) {
			agregarValidacionExtranjerosNoResidentes(cuestionario, preguntaHtml);
		} else if("INDM".equals(pregunta.getAbreviatura())) {
			agregarValidacionDomicilioExtranjeros(cuestionario, preguntaHtml);
		} else if("REPS".equals(pregunta.getAbreviatura())) {
			agregarValidacionPaisDomicilioExtranjeros(cuestionario, preguntaHtml);
		} else if("JUST".equals(pregunta.getAbreviatura())) {
			agregarValidacionJustificacionReferenciasBancarias(pregunta, preguntaHtml);
		} else if("PSTO".equals(pregunta.getAbreviatura())) {
			agregarValidacionesInformacionLaboral(cuestionario, pregunta, preguntaHtml);
		} else if("EMLB".equals(pregunta.getAbreviatura())) {
			agregarValidacionesInformacionLaboral(cuestionario, pregunta, preguntaHtml);
		} else if("PPEP".equals(pregunta.getAbreviatura())) {
			agregarValidacionFamiliarPep(cuestionario, pregunta, preguntaHtml);
		} else if("NPC1".equals(pregunta.getAbreviatura()) || "NPC2".equals(pregunta.getAbreviatura()) || "NPC3".equals(pregunta.getAbreviatura())) {
			agregarValidacionClientesProveedores(cuestionario, pregunta, preguntaHtml);
		} else if("NPP1".equals(pregunta.getAbreviatura()) || "NPP2".equals(pregunta.getAbreviatura()) || "NPP3".equals(pregunta.getAbreviatura())) {
			agregarValidacionClientesProveedores(cuestionario, pregunta, preguntaHtml);
		} else if("IAPR".equals(pregunta.getAbreviatura())) {
			agregarBotonFamiliarPep(cuestionario, pregunta, preguntaHtml);
		} else if("ITCO".equals(pregunta.getAbreviatura())) {
			agregarValidacionConcubina(pregunta, preguntaHtml);
		} else if("NSC0".equals(pregunta.getAbreviatura())) {
			agregarValidacionParticipacionAccionaria(pregunta, preguntaHtml);
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Otras Fuentes de Ingresos
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionOtrasFuentesIngresos(PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_OTRAS_FUENTES_ING));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_OTRAS_FUENTES_ING));
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Referencias Bancarias
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionReferenciasBancarias(PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_REFERENCIAS_BANC));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_REFERENCIAS_BANC));
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Extranjeros no residentes en el pais
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionExtranjerosNoResidentes(CuestionarioDTO cuestionario, StringBuilder preguntaHtml){
		if(CODIGO_PAIS_MX.equals(cuestionario.getCodigoPaisResidencia())|| CODIGO_PAIS_MX.equals(cuestionario.getCodigoNacionalidad())
			){
			int indiceInicioCambio = preguntaHtml.indexOf("<TABLE>");
			int indiceFinalCambio = preguntaHtml.lastIndexOf("</TABLE>");
			preguntaHtml.replace(indiceInicioCambio, indiceFinalCambio + 8, 
					"Al ser mexicano o tener residencia en M&eacute;xico no es necesario especificar el interes por abrir una cuenta en este banco.");
		}
	}
	
	/**
	 * Agrega la leyenda para la pregunta del Domicilio para extranjeros con residencia en MX
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionDomicilioExtranjeros(CuestionarioDTO cuestionario, StringBuilder preguntaHtml){
		if(CODIGO_PAIS_MX.equals(cuestionario.getCodigoPaisResidencia())){
			int indiceInicioCambio = preguntaHtml.indexOf("<TABLE>");
			int indiceFinalCambio = preguntaHtml.lastIndexOf("</TABLE>");
			preguntaHtml.replace(indiceInicioCambio, indiceFinalCambio + 8, "Al tener residencia en M&eacute;xico no es necesario proporcionar un domicilio.");
		}
	}
	
	/**
	 * Agrega la leyenda para la pregunta del Pais de Residencia cuando se encuentra en MX
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionPaisDomicilioExtranjeros(CuestionarioDTO cuestionario, StringBuilder preguntaHtml){
		if(CODIGO_PAIS_MX.equals(cuestionario.getCodigoPaisResidencia())){
			int indiceInicioCambio = preguntaHtml.indexOf("<TABLE>");
			int indiceFinalCambio = preguntaHtml.indexOf("</TABLE>");
			preguntaHtml.replace(indiceInicioCambio, indiceFinalCambio + 8, "Al tener residencia en M&eacute;xico no es necesario proporcionar un pa&iacute;s.");
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta Justificacion en las Referencias Bancarias
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionJustificacionReferenciasBancarias(PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		int indiceCambio = preguntaHtml.indexOf(String.format("PreguntasDiv%s", pregunta.getAbreviatura()));
		
		if(indiceCambio > -1){
			preguntaHtml.insert(indiceCambio - 4, " style='display: none;' ");
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Concubina
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionConcubina(PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_CONCUBINA));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_CONCUBINA));
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta de Participacion accionaria
	 * @param pregunta Pregunta a la que se le deben agregar las validaciones
	 * @param preguntaHtml Codigo HTML de la pregunta a la cual se le van a agregar las condiciones
	 */
	private static void agregarValidacionParticipacionAccionaria(PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_PART_ACCIONARIA));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_PART_ACCIONARIA));
		}
	}
}
