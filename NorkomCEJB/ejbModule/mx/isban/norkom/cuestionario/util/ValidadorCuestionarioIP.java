/**
 * Isban Mexico
 *   Clase: ValidadorCuestionarioIP.java
 *   Descripcion: Componente para validaciones del cuestionario IP.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

public final class ValidadorCuestionarioIP {
	/**
	 * String Funcion javascript para Habilitar la seccion de Transferencias
	 */
	public static final String JS_FUNC_HAB_TRANS_INT = " habilitarPreguntasTransferencias(); ";
	/**
	 * String Funcion javascript para Deshabilitar la seccion de Transferencias
	 */
	public static final String JS_FUNC_DES_TRANS_INT = " deshabilitarPreguntasTransferencias(); ";
	/**
	 * String Funcion javascript para Habilitar el Regimen Simplificado
	 */
	public static final String JS_FUNC_HAB_REG_SIMP = " habilitarRegimenSimplificado(); ";
	/**
	 * String Funcion javascript para deshabilitar el Regimen Simplificado
	 */
	public static final String JS_FUNC_DES_REG_SIMP = " deshabilitarRegimenSimplificado(); ";
	/**
	 * String Funcion javascript para Habilitar la seccion de Compra-Venta de Divisas
	 */
	public static final String JS_FUNC_HAB_COMP_VTA_DIV = " habilitarCompraVentaDivisas(); ";
	/**
	 * String Funcion javascript para Deshabilitar la seccion de Compra-Venta de Divisas
	 */
	public static final String JS_FUNC_DES_COMP_VTA_DIV = " deshabilitarCompraVentaDivisas(); ";
	
	/**
	 * String para agregar la funcion onclick
	 */
	private static final String JS_FUNCION_ONCLICK = " onclick='%s' ";
	/**Constructor*/
	private ValidadorCuestionarioIP(){}
	
	/**
	 * Agrega validaciones especiales para las preguntas del Cuestionario IP
	 * @param cuestionario objeto con datos del cuestionario
	 * @param pregunta Pregunta de la que se obtendra la informacion
	 * @param preguntaHtml Codigo HTML con la informacion de la pregunta
	 */
	public static void agregarValidacionesEspeciales(CuestionarioDTO cuestionario, PreguntaDTO pregunta, StringBuilder preguntaHtml) {
		if("STFI".equals(pregunta.getAbreviatura())) {
			agregarValidacionTransferenciasInt(pregunta, preguntaHtml);
		} else if("EDRS".equals(pregunta.getAbreviatura())) {
			agregarValidacionPaisResidencia(cuestionario, preguntaHtml);
		} else if("MNRS".equals(pregunta.getAbreviatura())) {
			agregarValidacionPaisResidencia(cuestionario, preguntaHtml);
		} else if("ENFI".equals(pregunta.getAbreviatura())) {
			agregarValidacionRegimenSimplificado(pregunta, preguntaHtml);
		} else if("CVDV".equals(pregunta.getAbreviatura())) {
			agregarValidacionCompraVentaDivisas(pregunta, preguntaHtml);
		} else if("PPEP".equals(pregunta.getAbreviatura())) {
			agregarValidacionPEP(pregunta, preguntaHtml);
		}
	}
	
	/**
	 * Agrega las validaciones para las preguntas de Transferencias Internacionales
	 * @param pregunta Pregunta de la que se obtendra la informacion
	 * @param preguntaHtml Codigo HTML con la informacion de la pregunta
	 */
	private static void agregarValidacionTransferenciasInt(PreguntaDTO pregunta, StringBuilder preguntaHtml){
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_TRANS_INT));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_TRANS_INT));
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta del Pais de Residencia
	 * @param cuestionario Cuestionario al que pertenece la pregunta
	 * @param preguntaHtml Codigo HTML con la informacion de la pregunta
	 */
	private static void agregarValidacionPaisResidencia(CuestionarioDTO cuestionario, StringBuilder preguntaHtml){
		if(!"052".equals(cuestionario.getCodigoNacionalidad())){
			int indiceInicioCambio = preguntaHtml.indexOf(">");
			int indiceFinalCambio = preguntaHtml.lastIndexOf("</DIV>");
			preguntaHtml.replace(indiceInicioCambio + 1, indiceFinalCambio, "");
		}
	}
	
	/**
	 * Agrega las validaciones para el Regimen Simplificado
	 * @param pregunta Pregunta de la que se obtendra la informacion
	 * @param preguntaHtml Codigo HTML con la informacion de la pregunta
	 */
	private static void agregarValidacionRegimenSimplificado(PreguntaDTO pregunta, StringBuilder preguntaHtml){
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_REG_SIMP));
			
			int posicionChecked = preguntaHtml.indexOf("checked");
			preguntaHtml.replace(posicionChecked, posicionChecked + 7, "");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_REG_SIMP));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
	}
	
	/**
	 * Agrega las validaciones para la pregunta Compra-Venta de Divisas
	 * @param pregunta Pregunta de la que se obtendra la informacion
	 * @param preguntaHtml Codigo HTML con la informacion de la pregunta
	 */
	private static void agregarValidacionCompraVentaDivisas(PreguntaDTO pregunta, StringBuilder preguntaHtml){
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_HAB_COMP_VTA_DIV));
			preguntaHtml.insert(indiceCambio, " checked ");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.replace(indiceCambio, indiceCambio + 24, String.format(JS_FUNCION_ONCLICK, JS_FUNC_DES_COMP_VTA_DIV));
		}
	}
	
	/**
	 * Agrega las validaciones para Personas Politicamente Expuestas
	 * @param pregunta Pregunta de la que se obtendra la informacion
	 * @param preguntaHtml Codigo HTML con la informacion de la pregunta
	 */
	private static void agregarValidacionPEP(PreguntaDTO pregunta, StringBuilder preguntaHtml){
		int indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sSI", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			
			int posicionChecked = preguntaHtml.indexOf("checked");
			preguntaHtml.replace(posicionChecked, posicionChecked + 7, "");
		}
		
		indiceCambio = preguntaHtml.indexOf(String.format("validacionEspecial%sNO", pregunta.getAbreviatura()));
		if(indiceCambio > -1){
			preguntaHtml.insert(indiceCambio, " checked ");
		}
	}
}
