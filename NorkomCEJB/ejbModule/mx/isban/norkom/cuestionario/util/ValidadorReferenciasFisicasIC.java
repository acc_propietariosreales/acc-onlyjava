/**
 * Isban Mexico
 *   Clase: ValidadorCuestionarioIP.java
 *   Descripcion: Componente para validaciones del cuestionario IC.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import java.util.List;

import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;

import org.apache.commons.lang.StringUtils;

public final class ValidadorReferenciasFisicasIC {
	/**
	 * Indice para obtener la primer referencia de la lista de referencias
	 */
	private static final int INDICE_PRIMER_REFERENCIA = 0;
	
	/**
	 * Indice para obtener la segunda referencia de la lista de referencias
	 */
	private static final int INDICE_SEGUNDA_REFERENCIA = 1;
	
	/**Constructor**/
	private ValidadorReferenciasFisicasIC(){
		
	}
	/**
	 * Obtiene el nombre, domicilio o telefono de la referencia con base a la pregunta
	 * @param pregunta Pregunta de la cual se espera obtener el valor predeterminado
	 * @param referencias Lista con las referencias
	 * @return El valor de la referencia
	 */
	public static String obtenerValorReferencia(PreguntaDTO pregunta, List<ResponsePE58DTO> referencias) {
		if("NMR1".equals(pregunta.getAbreviatura())) {
			return obtenerValorNombre(referencias, INDICE_PRIMER_REFERENCIA);
		} else if("DMR1".equals(pregunta.getAbreviatura())) {
			return obtenerValorDomicilio();
		} else if("TLR1".equals(pregunta.getAbreviatura())) {
			return obtenerValorTelefono(referencias, INDICE_PRIMER_REFERENCIA);
		} else if("NMR2".equals(pregunta.getAbreviatura())) {
			return obtenerValorNombre(referencias, INDICE_SEGUNDA_REFERENCIA);
		} else if("DMR2".equals(pregunta.getAbreviatura())) {
			return obtenerValorDomicilio();
		} else if("TLR2".equals(pregunta.getAbreviatura())) {
			return obtenerValorTelefono(referencias, INDICE_SEGUNDA_REFERENCIA);
		} else if("BNC1".equals(pregunta.getAbreviatura())) {
			return obtenerValorNombre(referencias, INDICE_PRIMER_REFERENCIA);
		} else if("NCD1".equals(pregunta.getAbreviatura())) {
			return obtenerValorCuenta(referencias, INDICE_PRIMER_REFERENCIA);
		} else if("BNC2".equals(pregunta.getAbreviatura())) {
			return obtenerValorNombre(referencias, INDICE_SEGUNDA_REFERENCIA);
		} else if("NCD2".equals(pregunta.getAbreviatura())) {
			return obtenerValorCuenta(referencias, INDICE_SEGUNDA_REFERENCIA);
		}
		
		return StringUtils.EMPTY;
	}
	
	/**
	 * Obtiene el nombre de la referencia
	 * @param referencias Lista con las referencias
	 * @param indiceReferencia Indice de la referencia de la cual se espera obtener el nombre
	 * @return Nombre de la referencia o cadena vacia en caso de no encontrarla
	 */
	private static String obtenerValorNombre(List<ResponsePE58DTO> referencias, int indiceReferencia) {
		if(referencias == null || referencias.isEmpty() || referencias.size() <= indiceReferencia) {
			return StringUtils.EMPTY;
		}
		
		return referencias.get(indiceReferencia).getNombreCompleto();
	}
	
	/**
	 * Obtiene el domicilio de la referencia
	 * @return Domicilio de la referencia o cadena vacia en caso de no encontrarla
	 */
	private static String obtenerValorDomicilio() {
		return StringUtils.EMPTY;
	}
	
	/**
	 * Obtiene el Telefono de la referencia
	 * @param referencias Lista con las referencias
	 * @param indiceReferencia Indice de la referencia de la cual se espera obtener el Telefono
	 * @return Telefono de la referencia o cadena vacia en caso de no encontrarla
	 */
	private static String obtenerValorTelefono(List<ResponsePE58DTO> referencias, int indiceReferencia) {
		if(referencias == null || referencias.isEmpty() || referencias.size() <= indiceReferencia) {
			return StringUtils.EMPTY;
		}
		
		return referencias.get(indiceReferencia).getTelefonoCompleto();
	}
	
	/**
	 * @param referencias lista de referencias a buscar
	 * @param indiceReferencia indice de la referencia a buscar
	 * Obtiene el valor de la cuenta de la referencia
	 * @return Cuenta de la referencia o cadena vacia en caso de no encontrarla
	 */
	private static String obtenerValorCuenta(List<ResponsePE58DTO> referencias, int indiceReferencia) {
		if(referencias == null || referencias.isEmpty() || indiceReferencia >= referencias.size()) {
			return StringUtils.EMPTY;
		}
		
		String numeroCuenta = StringUtils.EMPTY;
		if(referencias.get(indiceReferencia).getObservacionesDos()!=null && !referencias.get(indiceReferencia).getObservacionesDos().isEmpty() && 
				referencias.get(indiceReferencia).getObservacionesDos().length() >= 90)
		{
			numeroCuenta = referencias.get(indiceReferencia).getObservacionesDos().substring(69, 90).trim();
		}
		return numeroCuenta;
	}
}
