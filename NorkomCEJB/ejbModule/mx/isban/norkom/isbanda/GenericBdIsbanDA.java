/**
 * Isban Mexico
 *   Clase: GenericBdIsbanDA.java
 *   Descripcion: Componente generico para el Acceso a Datos desde BD con
 *   IsbanDataAccess.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package mx.isban.norkom.isbanda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Mensajes;
import mx.isban.norkom.cuestionario.util.Utilerias;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * Componente generico para el Acceso a Datos desde BD con IsbanDataAccess.
 */
public abstract class GenericBdIsbanDA extends GenericIsbanDA {
	/**
	 * Indicador de Registro de BD ACTIVO
	 */
	public static final int IND_ACTIVO = 1;
	
	/**
	 * Encabezado para el insert multiple.
	 **/
	protected static final String INSERT_MULTIPLE_ENCABEZADO = "INSERT ALL %n%sSELECT * FROM dual";
	/**
	 * El formato usado para obtener el total de resultados de una consulta.
	 **/
	protected static final String COUNT_QUERY_FMT = "SELECT COUNT(1) AS TOTAL FROM(%s)";


	/**
	 * Version de la clase
	 */
	private static final long serialVersionUID = -8378279837777377011L;
	/**
	 * El objeto de escritura en LOG para la clase.
	 **/
	private static final Logger LOGGER = Logger
			.getLogger(GenericBdIsbanDA.class);

	/**
	 * @param canal : canal
	 */
	protected GenericBdIsbanDA(IDACanal canal) {
		super(canal);
	}

	/**
	 * Realiza la consulta indicada y regresa la informacion de la base de datos
	 * 
	 * @param query
	 *            la consulta.
	 * @return los resultados de la consulta, donde el {@code key} del mapa
	 *         equivale al alias de la columna y el {@code value} equivale al
	 *         valor del registro.
	 * @throws BusinessException
	 *             maneja la excepcion
	 */
	protected List<HashMap<String, Object>> consultarSinMapeo(String query)
			throws BusinessException {
		ResponseMessageDataBaseDTO response = consultarBase(query);

		if ((response == null) || (response.getResultQuery() == null)
				|| response.getResultQuery().isEmpty()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Consulta sin resultados:"+query);
			}
			return Collections.emptyList();
		}
		return response.getResultQuery();
	}

	/**
	 * Realiza una consulta a la base de datos.
	 * 
	 * @param <T>
	 *            entrada
	 * @param query
	 *            la consulta.
	 * @param mapeo
	 *            el componente que realiza el mapeo de los resultados a
	 *            instancias de DTO.
	 * @return la lista de resultados obtenidos.
	 * @throws BusinessException
	 *             maneja la excepcion
	 */
	protected <T extends Serializable> List<T> consultar(String query,
			MapeoConsulta<T> mapeo) throws BusinessException {
		List<T> resultado = null;
		List<HashMap<String, Object>> response = null;

		if (mapeo == null) {
			throw new IllegalArgumentException(
					"Indicar el componente de mapeo de resultados");
		}

		response = consultarSinMapeo(query);
		resultado = new ArrayList<T>(response.size());
		for (Map<String, Object> registro : response) {
			resultado.add(mapeo.mapeoRegistro(registro));
		}

		return resultado;
	}

	/**
	 * Realiza una consulta de registro unico.
	 * 
	 * @param <T>
	 *            entrada
	 * @param query
	 *            el query con la consulta a la base de datos.
	 * @param mapeo
	 *            componente que realiza el mapeo del resultado de la consulta a
	 *            las propiedades del DTO.
	 * @return el resultado unico de la consulta.
	 * @throws BusinessException
	 *             manejo de la excepcion
	 */
	protected <T extends Serializable> T consultarUnico(String query,
			MapeoConsulta<T> mapeo) throws BusinessException {
		List<T> resultado = null;

		resultado = consultar(query, mapeo);
		if ((resultado == null) || resultado.isEmpty()) {
			return null;
		}
		return resultado.get(resultado.size()-1);
	}

	

	/**
	 * Realiza una consulta a la base de datos y obtiene el primer valor del
	 * primer registro de la consulta.
	 * 
	 * @param query
	 *            el query con la consulta.
	 * @return el valor de la primera columna del primer registro como un
	 *         {@link Integer}.
	 * @throws BusinessException
	 *             Manejo de la excepcion
	 */
	protected Integer consultarEntero(String query) throws BusinessException {
		return Utilerias.defaultInteger(consultarObjeto(query));
	}

	/**
	 * Realiza una consulta a la base de datos y obtiene el primer valor del
	 * primer registro de la consulta.
	 * 
	 * @param query
	 *            el query con la consulta.
	 * @return el valor de la primera columna del primer registro como un
	 *         {@link String}.
	 * @throws BusinessException
	 *             lanza una Excepcion
	 */
	protected String consultarString(String query) throws BusinessException {
		return Utilerias.defaultStringIfBlank(consultarObjeto(query), null);
	}


	/**
	 * Ejecuta un insert en la base de datos.
	 * 
	 * @param query
	 *            el query con la sentencia de insert.
	 * @throws BusinessException
	 *             Manejo de excepcion
	 */
	protected void insert(String query) throws BusinessException {
		insertUpdateDelete(query, ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
	}

	/**
	 * Ejecuta un update en la base de datos.
	 * 
	 * @param query
	 *            entrada sql
	 * @throws BusinessException
	 *             Manejo del a excepcion
	 */
	protected void update(String query) throws BusinessException {
		insertUpdateDelete(query, ConfigFactoryJDBC.OPERATION_TYPE_UPDATE);
	}

	/**
	 * Ejecuta un delete en la base de datos.
	 * 
	 * @param query
	 *            el query con la sentencia de delete.
	 * @throws BusinessException
	 *             si ocurre algun error al eliminar en la base de datos.
	 */
	protected void delete(String query) throws BusinessException {
		insertUpdateDelete(query, ConfigFactoryJDBC.OPERATION_TYPE_DELETE);
	}

	/**
	 * Realiza una operacion de Insert, Update o Delete.
	 * 
	 * @param query
	 *            l query con la operacion.
	 * @param tipoOperacion
	 *            el tipo de operacion.
	 * @throws BusinessException
	 *             si ocurre algun error.
	 */
	private void insertUpdateDelete(String query, int tipoOperacion)
			throws BusinessException {
		ResponseMessageDataBaseDTO response = ejecutar(query, tipoOperacion);

		if (response == null) {
			LOGGER.error("No se obtuvo respuesta de la BD");
			throw new BusinessException(Mensajes.ERROR_COMUNICACION.getCodigo());
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(response.getResultToJSONString());
		}
	}

	/**
	 * Obtiene el query necesario para conocer el total de registros que ha de
	 * regresar el query indicado.
	 * 
	 * @param query
	 *            el query con la consulta a efectuar.
	 * @return el query necesario para conocer el total de registros que ha de
	 *         regresar el query indicado.
	 */
	protected String getCountQuery(String query) {
		if (StringUtils.isBlank(query)) {
			throw new IllegalArgumentException("Consulta nula");
		}

		return String.format(COUNT_QUERY_FMT, query);
	}
}