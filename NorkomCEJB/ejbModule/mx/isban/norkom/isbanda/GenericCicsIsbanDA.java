/**
 * Isban Mexico
 *   Clase: GenericCicsIsbanDA.java
 *   Descripcion: Componente generico para consumir transacciones 390.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */
package mx.isban.norkom.isbanda;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.dataaccess.channels.cics.dto.RequestMessageCicsDTO;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.norkom.cuestionario.util.IDACanal;


/**
 * Componente generico para consumir transacciones 390.
 */
public abstract class GenericCicsIsbanDA extends GenericIsbanDA {

	/**
	 * Version de la clase
	 */
	private static final long serialVersionUID = -98782121548912191L;

	/**
	 * Crea una instancia del acceso a 390 para el canal indicado
	 * 
	 * @param canal
	 *            El canal por el cual se va a realizar la operaicon, CICS
	 */
	public GenericCicsIsbanDA(IDACanal canal) {
		super(canal);
	}

	/**
	 * Ejecuta una transaccion de 390.
	 * 
	 * @param mensaje
	 *            mensaje de la operacion.
	 * @param transaccion
	 *            la transaccion.
	 * @return la respuesta de la transaccion.
	 * @throws BusinessException
	 *             si ocurre algun error al ejecutar la operacion.
	 */
	protected ResponseMessageCicsDTO ejecutar(String mensaje, String transaccion)
			throws BusinessException {
		RequestMessageCicsDTO request = null;

		request = new RequestMessageCicsDTO();
		request.setCodeOperation(transaccion);
		request.setMessage(mensaje);
		request.setTransaction(transaccion);
		request.setTimeout(10000L);

		return (ResponseMessageCicsDTO) ejecutar(request);
	}



	/**
	 * Metodo rellenar, con algun caracter, hacia la izquierda o derecha
	 * 
	 * @param cad
	 *            cadena que se quiere rellenar
	 * @param lon
	 *            longitud hasta la que se rellenara
	 * @param caractRellen
	 *            aractRellen, caracter con el que se rellenara
	 * @param tipo
	 *            tipo de relleno, a la izquierda o derecha
	 * @return cadena con el resultado
	 */
	public static String rellenar(String cad, int lon, char caractRellen, char tipo) {
		StringBuilder aux = new StringBuilder();
		
		if (cad.length() > lon) {
			return cad.substring(0, lon);
		}
		
		if (tipo != 'I' && tipo != 'D') { 
			tipo = 'I';
		}

		if (tipo == 'D') {
			aux.append(cad);
		}

		for (int i = 0; i < (lon - cad.length()); i++) {
			aux.append(caractRellen);
		}

		if (tipo == 'I') {
			aux.append(cad);
		}

		return aux.toString();
	}

	/**
	 * Obtener codigo de error de buffer
	 * 
	 * @param buffer
	 *            entrada de la funcion
	 * @return cadena de respuesta con el codigo de error de buffer
	 */
	public static final String obtenerCodigoDeErrorDeBuffer(String buffer) {
		if (buffer == null || "".equals(buffer)) {
			return null;
		}

		String[] bufferSeparado = buffer.split("@");
		if (bufferSeparado.length >= 3 && bufferSeparado[2].length() > 9) {
			return bufferSeparado[2].substring(2, 9);
		}
		return null;
	}

	/**
	 * Obtener mensaje de error de buffer
	 * 
	 * @param buffer
	 *            entrada de la funcion
	 * @return cadena de salida con el mensaje de error de buffer
	 */
	public static final String obtenerMensajeDeErrorDeBuffer(String buffer) {
		if (buffer == null || "".equals(buffer)) {
			return null;
		}

		String[] bufferSeparado = buffer.split("@");
		if (bufferSeparado.length >= 3 && bufferSeparado[2].length() > 10) {
			return bufferSeparado[2].substring(10,
					bufferSeparado[2].length() - 2);
		}
		return null;
	}
}