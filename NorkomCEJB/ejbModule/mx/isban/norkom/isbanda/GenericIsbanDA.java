/**
 * Isban Mexico
 *   Clase: GenericIsbanDA.java
 *   Descripcion: Componente generico para el acceso a datos por medio del
 *   componente de IsbanDataAccess.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 *   06
 */
package mx.isban.norkom.isbanda;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.RequestMessageDTO;
import mx.isban.agave.dataaccess.ResponseMessageDTO;
import mx.isban.agave.dataaccess.channels.database.DataAccessDataBase;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.norkom.cuestionario.util.IDACanal;
import mx.isban.norkom.cuestionario.util.Utilerias;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Componente generico para el acceso a datos por medio del componente de
 * IsbanDataAccess.
 */
public abstract class GenericIsbanDA extends Architech {
	/**
	 * Version de la Clase
	 */
	private static final long serialVersionUID = -6666666666666666631L;

	/**
	 * El componente selector del canal.
	 **/
	private IDACanal canal = null;
	
	/**
	 * Constructor para crear un componente generico de Acceso a Datos
	 * @param canal : canal
	 */
	protected GenericIsbanDA(IDACanal canal) {
		this.canal = canal;
	}

	/**
	 * Ejecuta la operacion de Acceso a Datos indicada por el request y devuelve
	 * la respuesta obtenida.
	 * 
	 * @param request de ejecutar
	 * @return ResponseMessageDTO
	 * @throws BusinessException de ejecutar
	 */
	protected ResponseMessageDTO ejecutar(RequestMessageDTO request) throws BusinessException {
		if (request == null) {
			throw new IllegalArgumentException("El request es nulo");
		}
		try {
			DataAccess da=DataAccess.getInstance(request, this);
			
			return da.execute(canal.getNombre());
		} catch (ExceptionDataAccess e) {
			showException(e);
			throw new mx.isban.norkom.cuestionario.exception.BusinessException(e.getCode(),e);
		}
	}

	/**
	 * Obtener Data Access
	 * 
	 * @param request de entrada
	 * @return DataAccessDataBase con los resultados obtenidos
	 * @throws BusinessException En caso de un error de conexion
	 */
	protected DataAccessDataBase obtenerDataAccess(RequestMessageDTO request) throws BusinessException {	

			try {
				return (DataAccessDataBase) DataAccess.getInstance(request, this);
			} catch (ExceptionDataAccess e) {
				showException(e);
				throw new mx.isban.norkom.cuestionario.exception.BusinessException(e.getCode(), "Error al obtener la instancia (DataAccess)",e);
			}
	}

	/**
	 * Realiza una consulta a la base de datos y obtiene el primer valor del
	 * primer registro de la consulta.
	 * 
	 * @param query
	 *            el query con la consulta.
	 * @return el valor de la primera columna del primer registro como un
	 *         {@link BigDecimal}.
	 * @throws BusinessException
	 *             si ocurre algun error al realizar la consulta.
	 */
	protected BigDecimal consultarBigDecimal(String query)
			throws BusinessException {
		return Utilerias.defaultBigDecimal(consultarObjeto(query), null);
	}
	/**
	 * Realiza una consulta a la base de datos y obtiene el primer valor del
	 * primer registro de la consulta.
	 * 
	 * @param query
	 *            el query con la consulta.
	 * @return el valor de la primera columna del primer registro.
	 * @throws BusinessException
	 *             si ocurre algun error al realizar la consulta.
	 */
	protected Object consultarObjeto(String query) throws BusinessException {
		ResponseMessageDataBaseDTO response = consultarBase(query);
		if ((response == null) || response.getResultQuery().isEmpty()) {
			debug("Consulta sin resultados:"+query);
			return null;
		}

		for (Map<String, Object> registro : response.getResultQuery()) {
			for (Entry<String, Object> entry : registro.entrySet()) {
				return entry.getValue();
			}
		}

		return null;
	}
	/**
	 * Ejecuta la consulta indicada y regresa la respuesta del IsbanDataAccess.
	 * 
	 * @param query
	 *            el query con la consulta.
	 * @return el componente de respuesta del IsbanDataAccess.
	 * @throws BusinessException
	 *             si ocurre algun error al ejecutar la consulta.
	 */
	protected ResponseMessageDataBaseDTO consultarBase(String query)
			throws BusinessException {
		ResponseMessageDataBaseDTO response = ejecutar(query,
				ConfigFactoryJDBC.OPERATION_TYPE_QUERY);
		return response;
	}
	/**
	 * Ejecuta la operacion de Base de Datos (INSERT, UPDATE, DELETE, SELECT)
	 * indicada.
	 * 
	 * @param query
	 *            la operacion que se ha de ejecutar.
	 * @param tipoOperacion
	 *            el tipo de la operacion que se ha de ejecutar, los tipos de
	 *            operacion se encuentran definidos en
	 *            {@linkplain ConfigFactoryJDBC}.
	 * @return la respuesta de la Base de datos.
	 * @throws BusinessException
	 *             si ocurre algun error al ejecutar la operacion.
	 */
	protected ResponseMessageDataBaseDTO ejecutar(String query,
			int tipoOperacion) throws BusinessException {
		RequestMessageDataBaseDTO request = null;

		if (StringUtils.isBlank(query)) {
			throw new IllegalArgumentException("El query es nulo");
		}
		request = new RequestMessageDataBaseDTO();
		request.setQuery(query);
		request.setTypeOperation(tipoOperacion);

		return (ResponseMessageDataBaseDTO) ejecutar(request);
	}
	/**
	 * Obtiene el valor de registro para la columna indicada en {@link String}.
	 * 
	 * @param registro
	 *            el registro.
	 * @param columna
	 *            el nombre de la columna.
	 * @return el valor de registro para la columna indicada en {@link String}.
	 */
	protected final String getStringValue(Map<String, Object> registro,
			String columna) {
		if (StringUtils.isBlank(columna)) {
			throw new IllegalArgumentException("La columna es nulo");
		}
		if (registro == null) {
			error("Registro nulo");
			return StringUtils.EMPTY;
		}
		return Utilerias.defaultStringIfBlank(registro.get(columna.trim()
				.toUpperCase()));
	}

	/**
	 * Obtiene el valor del registro en {@link BigDecimal} para la columna
	 * indicada.
	 * 
	 * @param registro
	 *            el registro
	 * @param columna
	 *            la columna que se quiere obtener.
	 * @return el valor del registro en {@link BigDecimal} para la columna
	 *         indicada.
	 */
	protected final BigDecimal getBigDecimalValue(
			Map<String, Object> registro, String columna) {
		Object data = null;
		if (StringUtils.isBlank(columna)) {
			throw new IllegalArgumentException("La columna es nula");
		}
		if (registro == null) {
			error("Registro nulo");
			return null;
		}
		data = registro.get(columna.trim().toUpperCase());
		if (data == null) {
			return null;
		}
		if (StringUtils.isBlank(data.toString())) {
			return null;
		}
		if (!(data instanceof BigDecimal)) {
			throw new UnsupportedOperationException(String.format(
					"No se puede convertir %s a BigDecimal", data.toString()));
		}
		return (BigDecimal) data;
	}

	/**
	 * Obtiene el valor del registro en {@link Double} para la columna indicada.
	 * 
	 * @param registro
	 *            el registro.
	 * @param columna
	 *            la columna que se quiere obtener.
	 * @return el valor del registro en {@link Double} para la columna indicada.
	 */
	protected final Double getDoubleValue(Map<String, Object> registro,
			String columna) {
		BigDecimal val = getBigDecimalValue(registro, columna);
		if (val == null) {
			return null;
		}
		return val.doubleValue();
	}

	/**
	 * Obtiene el valor de registro para la columna indicada en {@link Integer}.
	 * 
	 * @param registro
	 *            el registro
	 * @param columna
	 *            el nombre de la columna
	 * @return el valor de registro para la columna indicada en {@link Integer}.
	 */
	protected final Integer getIntegerValue(
			Map<String, Object> registro, String columna) {
		if (StringUtils.isBlank(columna)) {
			throw new IllegalArgumentException("La columna es nulo");
		}
		if (registro == null) {
			error("Registro nulo");
			return null;
		}
		return Utilerias.defaultInteger(registro.get(columna.trim()
				.toUpperCase()), null);
	}

	/**
	 * Obtiene el valor de registro para la columna indicada en {@link Boolean}.
	 * 
	 * @param registro
	 *            el registro
	 * @param columna
	 *            el nombre de la columna
	 * @return el valor de registro para la columna indicada en {@link Boolean}.
	 */
	protected final Boolean getBooleanValue(
			Map<String, Object> registro, String columna) {

		return BooleanUtils.toBooleanObject(getIntegerValue(registro, columna));
	}

	/**
	 * @return the canal
	 */
	public IDACanal getCanal() {
		return canal;
	}

	/**
	 * @param canal 
	 * el canal a agregar
	 */
	public void setCanal(IDACanal canal) {
		this.canal = canal;
	}
	

}
