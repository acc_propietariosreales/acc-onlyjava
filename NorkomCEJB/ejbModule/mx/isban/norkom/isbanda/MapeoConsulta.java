/**
 * Isban Mexico
 *   Clase: MapeoConsulta.java
 *   Descripcion: Componente que realiza el mapeo de campos para una consulta.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package mx.isban.norkom.isbanda;

import java.io.Serializable;
import java.util.Map;

import mx.isban.agave.commons.exception.BusinessException;

/**
 * Componente que realiza el mapeo de campos para una consulta.
 */
public interface MapeoConsulta<T extends Serializable> {

	/**
	 * Metodo que realiza un mapeo directo del resultado de una consulta a una
	 * entidad del sistema.
	 * 
	 * @param registro
	 *            un mapa con el registro obtenido como resultado de una
	 *            consulta, donde el {@code key} del mapa equivale al alias de
	 *            la columna y el {@code value} equivale al valor del registro.
	 * @return la entidad del sistema correspondiente al registro obtenido.
	 * @throws BusinessException
	 *             si ocurre algun error al mapear el registro.
	 **/
	public T mapeoRegistro(Map<String, Object> registro)
			throws BusinessException;
}
