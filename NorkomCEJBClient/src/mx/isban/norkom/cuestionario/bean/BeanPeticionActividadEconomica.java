/**
 * Isban Mexico
 *   Clase: BeanPeticionActividadEconomica.java
 *   Descripcion: Request para consultar actividad economica
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class BeanPeticionActividadEconomica implements Serializable{

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -2805161454197701001L;
	
}
