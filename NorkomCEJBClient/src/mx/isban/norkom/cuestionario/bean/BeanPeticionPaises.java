/**
 * Isban Mexico
 *   Clase: BeanPeticionPaises.java
 *   Descripcion: Request para consultar paises
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

public class BeanPeticionPaises implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -4482177637325604476L;

}
