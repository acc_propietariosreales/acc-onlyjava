/**
 * Isban Mexico
 *   Clase: BeanPeticionSegmento.java
 *   Descripcion: Request para consultar segmento
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 12/11/2014
 *
 */
public class BeanPeticionSegmento implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 7157325861231194022L;

}
