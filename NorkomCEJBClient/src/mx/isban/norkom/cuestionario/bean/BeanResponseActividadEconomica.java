/**
 * Isban Mexico
 *   Clase: BeanResponseActividadEconomica.java
 *   Descripcion: Response que devuelve actividades economicas
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;
import java.util.List;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */

public class BeanResponseActividadEconomica extends ResponseBase implements Serializable{

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 366496054209922158L;
	
	/**
	 * lstActivEcon List<ActividadEconomicaDTO> lista
	 */
	private transient List<ActividadEconomicaDTO> lstActivEcon;
	
	/**
	 * @param lstActivEcon a agregar
	 */
	public void setlstActivEcon(List<ActividadEconomicaDTO> lstActivEcon) {
		this.lstActivEcon = lstActivEcon;
	}
	
	/**
	 * @return lstActivEcon
	 */
	public List<ActividadEconomicaDTO> getlstActivEcon() {
		return lstActivEcon;
	}

}
