/**
 * Isban Mexico
 *   Clase: BeanResponsePaises.java
 *   Descripcion: Response que devuelve la lista de paises
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;
import java.util.List;
import mx.isban.norkom.cuestionario.dto.PaisDTO;

public class BeanResponsePaises extends ResponseBase implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 5270036237134858082L;
	
	/**
	 * lstPaises List<PaisDTO> lista
	 */
	private List<PaisDTO> lstPaises;

	/**
	 * @param lstPaises 
	 * el lstPaises a agregar
	 */
	public void setLstPaises(List<PaisDTO> lstPaises) {
		this.lstPaises = lstPaises;
	}
	
	/**
	 * @return lstPaises
	 */
	public List<PaisDTO> getLstPaises(){
		return lstPaises;
	}

}
