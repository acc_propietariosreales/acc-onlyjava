/**
 * Isban Mexico
 *   Clase: BeanResponseSegmento.java
 *   Descripcion: Response que devuelve el segmento
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
 
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;
import java.util.List;
import mx.isban.norkom.cuestionario.dto.SegmentoDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 12/11/2014
 *
 */
public class BeanResponseSegmento extends ResponseBase implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 317983044941522651L;
	
	/**
	 * lstSegmento List<SegmentoDTO> lista
	 */
	private transient List<SegmentoDTO> lstSegmento;

	/**
	 * @param lstSegmento a agregar
	 */
	public void setLstSegmento(List<SegmentoDTO> lstSegmento) {
		this.lstSegmento = lstSegmento;
	}
	
	/**
	 * @return lstSegmento
	 */
	public List<SegmentoDTO> getlstSegmento() {
		return lstSegmento;
	}

}
