/**
 * Isban Mexico
 *   Clase: RequestCuestionario.java
 *   Descripcion: Request para obtener el cuestionario
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;


/**
 * @author Leopoldo F Espinosa R 
 * clase de peticion de cuestionarrio 26/11/2014
 */
public class RequestCuestionario extends ResponseBase implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -8507531912489254606L;
	/**
	 * int claveCuestionario
	 */
	private int claveCuestionario;
	/**tipo de persona**/
	private String tipoPersona;
	/**subtipo de persona**/
	private String subtipoPersona;
	/**divisa**/
	private String divisa;
	/**id del cuestionario*/
	private String idCuestionario;
	/**
	 * @return the idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}

	/**
	 * @param idCuestionario 
	 * el idCuestionario a agregar
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * @param tipoPersona 
	 * el tipoPersona a agregar
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param subtipoPersona 
	 * el subtipoPersona a agregar
	 */
	public void setSubtipoPersona(String subtipoPersona) {
		this.subtipoPersona = subtipoPersona;
	}

	/**
	 * @return the subtipoPersona
	 */
	public String getSubtipoPersona() {
		return subtipoPersona;
	}

	/**
	 * @param divisa 
	 * el divisa a agregar
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * @return the divisa
	 */
	public String getDivisa() {
		return divisa;
	}

	/**
	 * @return int
	 */
	public int getClaveCuestionario() {
		return claveCuestionario;
	}

	/**
	 * @param claveCuestionario void
	 */
	public void setClaveCuestionario(int claveCuestionario) {
		this.claveCuestionario = claveCuestionario;
	}
	
}
