/**
 * Isban Mexico
 *   Clase: RequestObtenerCuestionario.java
 *   Descripcion: Request para obtener el cuestionario
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;

/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
public class RequestObtenerCuestionario implements Serializable{

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 4697931776334202795L;
	
	/**Identificador del Cuestionario*/
	private String idCuestionario;
	
	/**numero de contrato**/
	private String contrato;
	/**Codigo de sucursal*/
	private String codSucursal;
	/**Codigo de producto*/
	private String codProducto;
	/**Codigo de Sub Producto*/
	private String codSubProducto;
	
	/**
	 * Obtiene el Identificador del Cuestionario
	 * @return El Identificador del Cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	
	/**
	 * Define un nuevo valor para el Identificador del Cuestionario
	 * @param idCuestionario El nuevo valor del Identificador del Cuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}
	/**
	 * @param contrato 
	 * el contrato a agregar
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * @return the contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * @return the codSucursal
	 */
	public String getCodSucursal() {
		return codSucursal;
	}

	/**
	 * @param codSucursal 
	 * el codSucursal a agregar
	 */
	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}

	/**
	 * @return the codProducto
	 */
	public String getCodProducto() {
		return codProducto;
	}

	/**
	 * @param codProducto 
	 * el codProducto a agregar
	 */
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	/**
	 * @return the codSubProducto
	 */
	public String getCodSubProducto() {
		return codSubProducto;
	}

	/**
	 * @param codSubProducto 
	 * el codSubProducto a agregar
	 */
	public void setCodSubProducto(String codSubProducto) {
		this.codSubProducto = codSubProducto;
	}

}
