/**
 * Isban Mexico
 *   Clase: RequestObtenerRelacionados.java
 *   Descripcion: Request para obtener estatus de relacionados
 *   
 *   Control de Cambios:
 *   1.0 Jun 23, 2017 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;
/**
 * RequestObtenerRelacionados
 * implementa Serializable
 * para identificarse
 * @author lespinosa
 *
 */
public class RequestObtenerRelacionados implements Serializable{

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -3381421692309829511L;
	
	/**
	 * Variable utilizada para declarar idCuestionario
	 */
	private String idCuestionario;
	
	/**
	 * Variable utilizada para declarar bloque
	 */
	private String bloque;

	/**
	 * Obtiene el valor de idCuestionario
	 * @return El valor de idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}

	/**
	 * Define el nuevo valor para idCuestionario
	 * @param idCuestionario El nuevo valor de idCuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * Obtiene el valor de bloque
	 * @return El valor de bloque
	 */
	public String getBloque() {
		return bloque;
	}

	/**
	 * Define el nuevo valor para bloque
	 * @param bloque El nuevo valor de bloque
	 */
	public void setBloque(String bloque) {
		this.bloque = bloque;
	}

}
