/**
 * Isban Mexico
 *   Clase: RequestOpcionesPregunta.java
 *   Descripcion: Bean para almacenar claves de preguntas
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;

/**
 * @author STEFANINI (Leopoldo F Espinosa R) 04/12/2014
 *
 */
public class RequestOpcionesPregunta implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 7844999201359822989L;
	/**id del grupo a buscar*/
	private String claveGrupo;
	
	/**
	 * @param claveGrupo 
	 * la ClaveGrupo a agregar
	 */
	public void setClaveGrupo(String claveGrupo) {
		this.claveGrupo = claveGrupo;
	}
	/**
	 * @return the ClaveGrupo
	 */
	public String getClaveGrupo() {
		return claveGrupo;
	}
	
}
