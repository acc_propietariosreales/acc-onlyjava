/**
 * Isban Mexico
 *   Clase: ResponseBase.java
 *   Descripcion: Response base a utilizar
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;


/**
 * Clase base para los Beans de repuesta de Norkom
 * @author STEFANINI (Leopoldo F Espinosa R) 04/11/2014
 *
 */
public class ResponseBase implements Serializable{
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 8468006419085070723L;
	/**Codigo de Error*/
	private String codError = "";
	/**Mensaje de Error**/
	private String msgError = "";
	/**
	 * 
	 * @return CodError
	 */
	public String getCodError() {
		return this.codError;
	}

	/***
	 * 
	 * @return MsgError
	 */
	public String getMsgError() {
		return this.msgError;
	}

	/***
	 * 
	 * @param codErr String
	 */
	public void setCodError(String codErr) {
		this.codError=codErr;
		
	}

	/**
	 * 
	 * @param msj String
	 */
	public void setMsgError(String msj) {
		this.msgError=msj;
	}

}
