/**
 * Isban Mexico
 *   Clase: ResponseObtenerCuestionario.java
 *   Descripcion: Bean para almacenar cuestionario
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean;

import java.util.Map;

import mx.isban.norkom.cuestionario.bean.rpt.ICListasBean;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.util.Cuestionarios;

/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
public class ResponseObtenerCuestionario extends ResponseBase{

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -7909531182904871593L;
	/**Datos del reporte*/
	private Map<String, Object> datosReporte;
	/**tipo de Cuestionario (TIPO_IP,TIPO_IC_A1,TIPO_IC_A2,TIPO_IC_A3)**/
	private Cuestionarios tipo;
	 /**tipoPersona tipo de persona(PERSONA_FISICA,PERSONA_FISICA_AE,PERSONA_MORAL)**/
	private Cuestionarios tipoPersona;
	/**tipoMoneda tipo de moneda (TIPO_MONEDA_DOLARES,TIPO_MONEDA_PESOS)**/
	private Cuestionarios tipoMoneda;
	/**DTO con datos del encabezado**/
	private CuestionarioDTO cuestionarioDTO;
	/**DTO para listas de los reportes Complementarios*/
	private ICListasBean listasBeanIC;
	
	/**
	 * @param datosReporte 
	 * el datosReporte a agregar
	 */
	public void setDatosReporte(Map<String, Object> datosReporte) {
		this.datosReporte = datosReporte;
	}
	/**
	 * @return the datosReporte
	 */
	public Map<String, Object> getDatosReporte() {
		return datosReporte;
	}
	/**
	 * @param tipo 
	 * el tipo a agregar
	 */
	public void setTipo(Cuestionarios tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the tipo
	 */
	public Cuestionarios getTipo() {
		return tipo;
	}
	/**
	 * @param tipoPersona 
	 * el tipoPersona a agregar
	 */
	public void setTipoPersona(Cuestionarios tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return the tipoPersona
	 */
	public Cuestionarios getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoMoneda 
	 * el tipoMoneda a agregar
	 */
	public void setTipoMoneda(Cuestionarios tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	/**
	 * @return the tipoMoneda
	 */
	public Cuestionarios getTipoMoneda() {
		return tipoMoneda;
	}
	/**
	 * @return CuestionarioDTO
	 */
	public CuestionarioDTO getCuestionarioDTO() {
		return cuestionarioDTO;
	}
	/**
	 * @param cuestionarioDTO void
	 */
	public void setCuestionarioDTO(CuestionarioDTO cuestionarioDTO) {
		this.cuestionarioDTO = cuestionarioDTO;
	}
	/**
	 * @param listasBeanIC 
	 * el listasBeanIC a agregar
	 */
	public void setListasBeanIC(ICListasBean listasBeanIC) {
		this.listasBeanIC = listasBeanIC;
	}
	/**
	 * @return the listasBeanIC
	 */
	public ICListasBean getListasBeanIC() {
		return listasBeanIC;
	}
}
