/**
 * Isban Mexico
 *   Clase: ResponseObtenerRelacionados.java
 *   Descripcion: Response para obtener estatus de relacionados
 *   
 *   Control de Cambios:
 *   1.0 Jun 23, 2017 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;
/**
 * ResponseObtenerRelacionados
 * implementa  Serializable
 * 
 * @author lespinosa
 *
 */
public class ResponseObtenerRelacionados implements Serializable{

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -5763327155309212805L;
	
	/**
	 * Variable utilizada para declarar idCuestionario
	 */
	private String idCuestionario = "";
	
	/**
	 * Variable utilizada para declarar estatusRelacion
	 */
	private String estatusRelacion = "";
	
	/**
	 * Variable utilizada para declarar numeroBloque
	 */
	private String numeroBloque;

	/**
	 * Obtiene el valor de idCuestionario
	 * @return El valor de idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}

	/**
	 * Define el nuevo valor para idCuestionario
	 * @param idCuestionario El nuevo valor de idCuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * Obtiene el valor de estatusRelacion
	 * @return El valor de estatusRelacion
	 */
	public String getEstatusRelacion() {
		return estatusRelacion;
	}

	/**
	 * Define el nuevo valor para estatusRelacion
	 * @param estatusRelacion El nuevo valor de estatusRelacion
	 */
	public void setEstatusRelacion(String estatusRelacion) {
		this.estatusRelacion = estatusRelacion;
	}

	/**
	 * Obtiene el valor de numeroBloque
	 * @return El valor de numeroBloque
	 */
	public String getNumeroBloque() {
		return numeroBloque;
	}

	/**
	 * Define el nuevo valor para numeroBloque
	 * @param numeroBloque El nuevo valor de numeroBloque
	 */
	public void setNumeroBloque(String numeroBloque) {
		this.numeroBloque = numeroBloque;
	}	
}
