/**
 * Isban Mexico
 *   Clase: ResponseOpcionesPregunta.java
 *   Descripcion: Bean para almacenar listas de opciones de preguntas
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean;

import java.io.Serializable;
import java.util.List;

import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;




/**
 * Clase base para los Beans de repuesta de Norkom
 * @author STEFANINI (Leopoldo F Espinosa R) 04/12/2014
 *
 */
public class ResponseOpcionesPregunta extends ResponseBase implements Serializable{
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 3179743174640239662L;
	/**opciones de preguntas**/
	private List<OpcionesPreguntaDTO> lstOpcionesPreguntas;
	/**
	 * @return the lstOpcionesPreguntas
	 */
	public List<OpcionesPreguntaDTO> getLstOpcionesPreguntas() {
		return lstOpcionesPreguntas;
	}
	/**
	 * @param lstOpcionesPreguntas 
	 * el lstOpcionesPreguntas a agregar
	 */
	public void setLstOpcionesPreguntas(
			List<OpcionesPreguntaDTO> lstOpcionesPreguntas) {
		this.lstOpcionesPreguntas = lstOpcionesPreguntas;
	}


	
}
