/**
 * Isban Mexico
 *   Clase: AccionistasBean.java
 *   Descripcion: Bean para almacenar accionistas
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * 
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class AccionistasBean implements Serializable {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -285189661206225812L;
	/**strNomAccionista*/
	private String strNomAccionista;
	/**strFecNacAccionista*/
	private String strFecNacAccionista;
	/**strNacionAccionista*/
	private String strNacionAccionista;
	/**strTipoAccionista*/
	private String strTipoAccionista;
	/**strParticipacionAccionista*/
	private String strParticipacionAccionista;
	
	/**
	 * 
	 * @param strNomAccionista nombre
	 * @param strFecNacAccionista fecha
	 * @param strNacionAccionista nacionalidad
	 * @param strTipoAccionista tipo
	 * @param strParticipacionAccionista participacion
	 */
	public AccionistasBean(String strNomAccionista, String strFecNacAccionista,
			String strNacionAccionista, String strTipoAccionista,
			String strParticipacionAccionista) {
		this.strNomAccionista=strNomAccionista;
		this.strFecNacAccionista=strFecNacAccionista;
		this.strNacionAccionista=strNacionAccionista;
		this.strTipoAccionista=strTipoAccionista;
		this.strParticipacionAccionista=strParticipacionAccionista;
	}

	/**
	 * @param strTipoAccionista 
	 * el strTipoAccionista a agregar
	 */
	public void setStrTipoAccionista(String strTipoAccionista) {
		this.strTipoAccionista = strTipoAccionista;
	}

	/**
	 * @return the strTipoAccionista
	 */
	public String getStrTipoAccionista() {
		return strTipoAccionista;
	}

	/**
	 * @param strFecNacAccionista 
	 * el strFecNacAccionista a agregar
	 */
	public void setStrFecNacAccionista(String strFecNacAccionista) {
		this.strFecNacAccionista = strFecNacAccionista;
	}

	/**
	 * @return the strFecNacAccionista
	 */
	public String getStrFecNacAccionista() {
		return strFecNacAccionista;
	}

	/**
	 * @param strNomAccionista 
	 * el strNomAccionista a agregar
	 */
	public void setStrNomAccionista(String strNomAccionista) {
		this.strNomAccionista = strNomAccionista;
	}

	/**
	 * @return the strNomAccionista
	 */
	public String getStrNomAccionista() {
		return strNomAccionista;
	}

	/**
	 * @param strNacionAccionista 
	 * el strNacionAccionista a agregar
	 */
	public void setStrNacionAccionista(String strNacionAccionista) {
		this.strNacionAccionista = strNacionAccionista;
	}

	/**
	 * @return the strNacionAccionista
	 */
	public String getStrNacionAccionista() {
		return strNacionAccionista;
	}

	/**
	 * @param strParticipacionAccionista 
	 * el strParticipacionAccionista a agregar
	 */
	public void setStrParticipacionAccionista(String strParticipacionAccionista) {
		this.strParticipacionAccionista = strParticipacionAccionista;
	}

	/**
	 * @return the strParticipacionAccionista
	 */
	public String getStrParticipacionAccionista() {
		return strParticipacionAccionista;
	}
	
}
