/**
 * Isban Mexico
 *   Clase: ClientesBean.java
 *   Descripcion: Bean para almacenar clientes
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * 
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class ClientesBean implements Serializable {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 7539239909361710769L;
	/**nombre del cliente*/
	private String strNomCliente;
	
	/**
	 * 
	 * @param strNomCliente nombre del cliente
	 */
	public ClientesBean(String strNomCliente) {
		this.strNomCliente=strNomCliente;
	}
	/**
	 * @param strNomCliente 
	 * el strNomCliente a agregar
	 */
	public void setStrNomCliente(String strNomCliente) {
		this.strNomCliente = strNomCliente;
	}
	/**
	 * @return the strNomCliente
	 */
	public String getStrNomCliente() {
		return strNomCliente;
	}

	
}
