/**
 * Isban Mexico
 *   Clase: DatosReporteBean.java
 *   Descripcion: Bean para almacenar datos de los reportes
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
public class DatosReporteBean implements Serializable {
	/**
	 * SID
	 */
	private static final long serialVersionUID = 3074863002750381872L;
	/**Parametro para preguntas libres*/
	private Map<String, Object> libres= new HashMap<String, Object>();
	
	/**Parametro para preguntas select unica*/
	private Map<String, Object> selectUnica= new HashMap<String, Object>();
	
	/**Parametro para preguntas de catalogo pais*/
	private Map<String, Object> pais= new HashMap<String, Object>();
	
	/**Parametro para preguntas radio button*/
	private Map<String, Object> radio= new HashMap<String, Object>();
	
	/**Parametro para preguntas origen de los recursos*/
	private Map<String, Object> origen= new HashMap<String, Object>();
	
	/**Parametro para preguntas destino de los recursos*/
	private Map<String, Object> destino= new HashMap<String, Object>();
	
	/**Parametro para informacion de encabezado*/
	private Map<String, Object> encabezado= new HashMap<String, Object>();

	/**
	 * @param parametros el parametros a establecer
	 */
	public void setLibres(Map<String, Object> parametros) {
		this.libres = parametros;
	}

	/**
	 * @return el parametros
	 */
	public Map<String, Object> getLibres() {
		return libres;
	}

	/**
	 * @return HashMap<String,Object>
	 */
	public Map<String, Object> getSelectUnica() {
		return selectUnica;
	}

	/**
	 * @param selectUnica void
	 */
	public void setSelectUnica(Map<String, Object> selectUnica) {
		this.selectUnica = selectUnica;
	}

	/**
	 * @return HashMap<String,Object>
	 */
	public Map<String, Object> getPais() {
		return pais;
	}

	/**
	 * @param pais void
	 */
	public void setPais(Map<String, Object> pais) {
		this.pais = pais;
	}

	/**
	 * @return HashMap<String,Object>
	 */
	public Map<String, Object> getRadio() {
		return radio;
	}

	/**
	 * @param radio void
	 */
	public void setRadio(Map<String, Object> radio) {
		this.radio = radio;
	}

	/**
	 * @return HashMap<String,Object>
	 */
	public Map<String, Object> getOrigen() {
		return origen;
	}

	/**
	 * @param origen void
	 */
	public void setOrigen(Map<String, Object> origen) {
		this.origen = origen;
	}

	/**
	 * @return HashMap<String,Object>
	 */
	public Map<String, Object> getDestino() {
		return destino;
	}

	/**
	 * @param destino void
	 */
	public void setDestino(Map<String, Object> destino) {
		this.destino = destino;
	}

	/**
	 * @return HashMap<String,Object>
	 */
	public Map<String, Object> getEncabezado() {
		return encabezado;
	}

	/**
	 * @param encabezado void
	 */
	public void setEncabezado(Map<String, Object> encabezado) {
		this.encabezado = encabezado;
	}
	
}
