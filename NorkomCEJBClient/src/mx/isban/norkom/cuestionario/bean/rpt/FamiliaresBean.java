/**
 * Isban Mexico
 *   Clase: FamiliaresBean.java
 *   Descripcion: Bean para almacenar familiares 
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * 
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class FamiliaresBean implements Serializable {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -372942542821513718L;
	/**strParentesco*/
	private String strParentesco;
	/**strParOtros**/
	private String strParOtros;
	/**strNomFamiliar**/
	private String strNomFamiliar;
	/**strDomFamiliar*/
	private String strDomFamiliar;
	/**strFecNacFamiliar**/
	private String strFecNacFamiliar;
	/**
	 * default constructor
	 */
	public  FamiliaresBean() {
	}
	/***
	 * constructor con parametros
	 * @param strParentesco PARENTESCO
	 * @param strParOtros OTROS
	 * @param strNomFamiliar NOMBRE FAMILIAR
	 * @param strDomFamiliar DOMICILIO FAMILIAR
	 * @param strFecNacFamiliar FECHA NACIMIENTO FAMILIAR
	 */
	public FamiliaresBean(String strParentesco, String strParOtros,
			String strNomFamiliar, String strDomFamiliar,
			String strFecNacFamiliar) {
		this.strParentesco=strParentesco;
		this.strParOtros=strParOtros;
		this.strNomFamiliar=strNomFamiliar;
		this.strDomFamiliar=strDomFamiliar;
		this.strFecNacFamiliar=strFecNacFamiliar;
	}

	/**
	 * @param strParentesco 
	 * el strParentesco a agregar
	 */
	public void setStrParentesco(String strParentesco) {
		this.strParentesco = strParentesco;
	}

	/**
	 * @return the strParentesco
	 */
	public String getStrParentesco() {
		return strParentesco;
	}

	/**
	 * @param strParOtros 
	 * el strParOtros a agregar
	 */
	public void setStrParOtros(String strParOtros) {
		this.strParOtros = strParOtros;
	}

	/**
	 * @return the strParOtros
	 */
	public String getStrParOtros() {
		return strParOtros;
	}

	/**
	 * @param strDomFamiliar 
	 * el strDomFamiliar a agregar
	 */
	public void setStrDomFamiliar(String strDomFamiliar) {
		this.strDomFamiliar = strDomFamiliar;
	}

	/**
	 * @return the strDomFamiliar
	 */
	public String getStrDomFamiliar() {
		return strDomFamiliar;
	}

	/**
	 * @param strNomFamiliar 
	 * el strNomFamiliar a agregar
	 */
	public void setStrNomFamiliar(String strNomFamiliar) {
		this.strNomFamiliar = strNomFamiliar;
	}

	/**
	 * @return the strNomFamiliar
	 */
	public String getStrNomFamiliar() {
		return strNomFamiliar;
	}

	/**
	 * @param strFecNacFamiliar 
	 * el strFecNacFamiliar a agregar
	 */
	public void setStrFecNacFamiliar(String strFecNacFamiliar) {
		this.strFecNacFamiliar = strFecNacFamiliar;
	}

	/**
	 * @return the strFecNacFamiliar
	 */
	public String getStrFecNacFamiliar() {
		return strFecNacFamiliar;
	}

}
