/**
 * Isban Mexico
 *   Clase: ICListasBean.java
 *   Descripcion: Bean para almacenar listas de datos de cuestionario IC
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

/***
 * Clase que contiene las listas que se envian a los reportes para su generacion
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class ICListasBean implements Serializable {
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 4365386265604007257L;
	/**lstOtrosIngresos*/
	private List<MontosBean> lstOtrosIngresos = new ArrayList<MontosBean>();
	/**lstRefPersonales*/
	private List<ReferenciasBean> lstRefPersonales = new ArrayList<ReferenciasBean>();
	/**lstFamiliares*/
	private List<FamiliaresBean> lstFamiliares = new ArrayList<FamiliaresBean>();
	/**lstSociedades*/
	private List<SociedadesBean> lstSociedades = new ArrayList<SociedadesBean>();
	/**lstClientes*/
	private List<ClientesBean> lstClientes  = new ArrayList<ClientesBean>();
	/**lstProveedores*/
	private List<ProveedoresBean> lstProveedores = new ArrayList<ProveedoresBean>();
	/**lstRefBancos*/
	private List<RefBancariasBean> lstRefBancos = new ArrayList<RefBancariasBean>();
	/**lstAccionistas*/
	private List<AccionistasBean> lstAccionistas = new ArrayList<AccionistasBean>();
	/**lista de paises*/
	private List<PreguntaDTO>  lstPaises = new ArrayList<PreguntaDTO>();
	/**
	 * @param lstOtrosIngresos 
	 * el lstOtrosIngresos a agregar
	 */
	public void setLstOtrosIngresos(List<MontosBean> lstOtrosIngresos) {
		this.lstOtrosIngresos = lstOtrosIngresos;
	}
	/**
	 * @return the lstOtrosIngresos
	 */
	public List<MontosBean> getLstOtrosIngresos() {
		return lstOtrosIngresos;
	}
	/**
	 * @param lstRefPersonales 
	 * el lstRefPersonales a agregar
	 */
	public void setLstRefPersonales(List<ReferenciasBean> lstRefPersonales) {
		this.lstRefPersonales = lstRefPersonales;
	}
	/**
	 * @return the lstRefPersonales
	 */
	public List<ReferenciasBean> getLstRefPersonales() {
		return lstRefPersonales;
	}
	/**
	 * @param lstFamiliares 
	 * el lstFamiliares a agregar
	 */
	public void setLstFamiliares(List<FamiliaresBean> lstFamiliares) {
		this.lstFamiliares = lstFamiliares;
	}
	/**
	 * @return the lstFamiliares
	 */
	public List<FamiliaresBean> getLstFamiliares() {
		return lstFamiliares;
	}
	/**
	 * @param lstSociedades 
	 * el lstSociedades a agregar
	 */
	public void setLstSociedades(List<SociedadesBean> lstSociedades) {
		this.lstSociedades = lstSociedades;
	}
	/**
	 * @return the lstSociedades
	 */
	public List<SociedadesBean> getLstSociedades() {
		return lstSociedades;
	}
	/**
	 * @param lstClientes 
	 * el lstClientes a agregar
	 */
	public void setLstClientes(List<ClientesBean> lstClientes) {
		this.lstClientes = lstClientes;
	}
	/**
	 * @return the lstClientes
	 */
	public List<ClientesBean> getLstClientes() {
		return lstClientes;
	}
	/**
	 * @param lstProveedores 
	 * el lstProveedores a agregar
	 */
	public void setLstProveedores(List<ProveedoresBean> lstProveedores) {
		this.lstProveedores = lstProveedores;
	}
	/**
	 * @return the lstProveedores
	 */
	public List<ProveedoresBean> getLstProveedores() {
		return lstProveedores;
	}
	/**
	 * @param lstRefBancos 
	 * el lstRefBancos a agregar
	 */
	public void setLstRefBancos(List<RefBancariasBean> lstRefBancos) {
		this.lstRefBancos = lstRefBancos;
	}
	/**
	 * @return the lstRefBancos
	 */
	public List<RefBancariasBean> getLstRefBancos() {
		return lstRefBancos;
	}
	/**
	 * @param lstAccionistas 
	 * el lstAccionistas a agregar
	 */
	public void setLstAccionistas(List<AccionistasBean> lstAccionistas) {
		this.lstAccionistas = lstAccionistas;
	}
	/**
	 * @return the lstAccionistas
	 */
	public List<AccionistasBean> getLstAccionistas() {
		return lstAccionistas;
	}
	/**
	 * @param lstPaises 
	 * el lstPaises a agregar
	 */
	public void setLstPaises(List<PreguntaDTO> lstPaises) {
		this.lstPaises = lstPaises;
	}
	/**
	 * @return the lstPaises
	 */
	public List<PreguntaDTO> getLstPaises() {
		return lstPaises;
	}
	
	
}
