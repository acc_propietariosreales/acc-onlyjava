/**
 * Isban Mexico
 *   Clase: MontosBean.java
 *   Descripcion: Bean para almacenar montos y sus fuentes
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * @author Leopoldo F Espinosa R 26/01/2015
 *
 */
public class MontosBean implements Serializable{
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 7911480504865957266L;
	/**String fuente*/
	private String strFuente;
	/**String monto dolar*/
	private String strMonto;

	/**
	 * @param strFuente 
	 * el strFuente a agregar
	 */
	public void setStrFuente(String strFuente) {
		this.strFuente = strFuente;
	}
	/**
	 * @return the strFuente
	 */
	public String getStrFuente() {
		return strFuente;
	}
	/**
	 * @param strMonto Dolar 
	 * el strMonto a agregar
	 */
	public void setStrMonto(String strMonto) {
		this.strMonto = strMonto;
	}
	/**
	 * @return the strMonto
	 */
	public String getStrMonto() {
		return strMonto;
	}
	
}
