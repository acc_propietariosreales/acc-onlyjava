/**
 * Isban Mexico
 *   Clase: ProveedoresBean.java
 *   Descripcion: Bean para almacenar proveedores
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

public class ProveedoresBean implements Serializable{
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 6835391417266855156L;
	/**strNomProveedor*/
	private String strNomProveedor;
	/**
	 * constructor
	 */
	public ProveedoresBean(){
	}
	/**
	 * 
	 * @param strNomProveedor  a agregar
	 */
	public ProveedoresBean(String strNomProveedor) {
		this.strNomProveedor=strNomProveedor;
	}
	/**
	 * @param strNomProveedor 
	 * el strNomProveedor a agregar
	 */
	public void setStrNomProveedor(String strNomProveedor) {
		this.strNomProveedor = strNomProveedor;
	}
	/**
	 * @return the strNomProveedor
	 */
	public String getStrNomProveedor() {
		return strNomProveedor;
	}

}
