/**
 * Isban Mexico
 *   Clase: RefBancariasBean.java
 *   Descripcion: Bean para almacenar referencias bancarias
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * 
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class RefBancariasBean implements Serializable {
	/**
	 * RefBancariasBean.java  de tipo long
	 */
	private static final long serialVersionUID = -6919146027623747098L;
	/**strNomBanco*/
	private String strNomBanco;
	/**strNumCuenta*/
	private String strNumCuenta;
	/**
	 * RefBancariasBean
	 */
	public RefBancariasBean(){
		
	}
	/***
	 * 
	 * @param strNomBanco nombre del banco
	 * @param strNumCuenta numero de cuenta
	 */
	public RefBancariasBean(String strNomBanco, String strNumCuenta) {
		super();
		this.strNomBanco=strNomBanco;
		this.strNumCuenta=strNumCuenta;
	}
	/**
	 * @param strNomBanco 
	 * el strNomBanco a agregar
	 */
	public void setStrNomBanco(String strNomBanco) {
		this.strNomBanco = strNomBanco;
	}
	/**
	 * @return the strNomBanco
	 */
	public String getStrNomBanco() {
		return strNomBanco;
	}
	/**
	 * @param strNumCuenta 
	 * el strNumCuenta a agregar
	 */
	public void setStrNumCuenta(String strNumCuenta) {
		this.strNumCuenta = strNumCuenta;
	}
	/**
	 * @return the strNumCuenta
	 */
	public String getStrNumCuenta() {
		return strNumCuenta;
	}

	
}
