/**
 * Isban Mexico
 *   Clase: ReferenciasBean.java
 *   Descripcion: Bean para almacenar referencias personales
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * 
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class ReferenciasBean implements Serializable  {
	/**
	 * ReferenciasBean.java  de tipo long
	 */
	private static final long serialVersionUID = -4874848344919249581L;
	/**nombre de la referencia*/
	private String strNomRefPersonal;
	/**docmicilio de la referencia*/
	private String strDomRefPersonal;
	/**telefono de la referencia*/
	private String strTelRefPersonal;
	
	/**ReferenciasBean*/
	public ReferenciasBean(){}
			
	/**
	 * 
	 * @param strNomRefPersonal nombre
	 * @param strDomRefPersonal domicilio
	 * @param strTelRefPersonal telefono
	 */
	public ReferenciasBean(String strNomRefPersonal, String strDomRefPersonal,
			String strTelRefPersonal) {
		this.strNomRefPersonal = strNomRefPersonal;
		this.strDomRefPersonal = strDomRefPersonal;
		this.strTelRefPersonal = strTelRefPersonal;
	}
	/**
	 * @param strNomRefPersonal 
	 * el strNomRefPersonal a agregar
	 */
	public void setStrNomRefPersonal(String strNomRefPersonal) {
		this.strNomRefPersonal = strNomRefPersonal;
	}
	/**
	 * @return the strNomRefPersonal
	 */
	public String getStrNomRefPersonal() {
		return strNomRefPersonal;
	}
	/**
	 * @param strDomRefPersonal 
	 * el strDomRefPersonal a agregar
	 */
	public void setStrDomRefPersonal(String strDomRefPersonal) {
		this.strDomRefPersonal = strDomRefPersonal;
	}
	/**
	 * @return the strDomRefPersonal
	 */
	public String getStrDomRefPersonal() {
		return strDomRefPersonal;
	}
	/**
	 * @param strTelRefPersonal 
	 * el strTelRefPersonal a agregar
	 */
	public void setStrTelRefPersonal(String strTelRefPersonal) {
		this.strTelRefPersonal = strTelRefPersonal;
	}
	/**
	 * @return the strTelRefPersonal
	 */
	public String getStrTelRefPersonal() {
		return strTelRefPersonal;
	}

}
