/**
 * Isban Mexico
 *   Clase: SociedadesBean.java
 *   Descripcion: Bean para almacenar nombres de las sociedades
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bean.rpt;

import java.io.Serializable;

/**
 * 
 * @author Leopoldo F Espinosa R 11/02/2015
 *
 */
public class SociedadesBean implements Serializable{
	/**
	 * SociedadesBean.java  de tipo long
	 */
	private static final long serialVersionUID = -5934444027123951428L;
	/**
	 * strNomSociedad
	 */
	private String strNomSociedad;
	/**
	 * SociedadesBean
	 */
	public SociedadesBean(){
		
	}
	/**
	 * 
	 * @param strNomSociedad nombre
	 */
	public SociedadesBean(String strNomSociedad) {
		super();
		this.strNomSociedad=strNomSociedad;
	}
	/**
	 * @param strNomSociedad 
	 * el strNomSociedad a agregar
	 */
	public void setStrNomSociedad(String strNomSociedad) {
		this.strNomSociedad = strNomSociedad;
	}
	/**
	 * @return the strNomSociedad
	 */
	public String getStrNomSociedad() {
		return strNomSociedad;
	}
	
}
