/**
 * Isban Mexico
 *   Clase: ActividadEconomicaBO.java
 *   Descripcion: Interfaz para el BO ActividadEconomicaBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface ActividadEconomicaBO {
	/**
	 * Obtiene la actividad economica con base al codigo
	 * @param codigoActividad Codigo de la actividad economica que se desea obtener
	 * @param psession Objeto de sesion de Agave
	 * @return ActividadEconomicaDTO Actividad economica obtenida con base al codigo
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public ActividadEconomicaDTO obtenerActividadEconomica(String codigoActividad, ArchitechSessionBean psession) throws BusinessException;
}
