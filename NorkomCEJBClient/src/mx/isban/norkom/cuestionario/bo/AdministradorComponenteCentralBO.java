/**
 * Isban Mexico
 *   Clase: AdministradorComponenteCentralBO.java
 *   Descripcion: Interfaz para el BO AdministradorComponenteCentralBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

@Remote
public interface AdministradorComponenteCentralBO {
	/**
	 * Variable utilizada para declarar error EROBID02
	 */
	public static final String COD_ER_ID_APLICACION = "EROBID02";
	/**
	 * Variable utilizada para declarar error EROBID03
	 */
	public static final String COD_ER_BUC = "EROBID03";
	
	/**
	 * Variable utilizada para declarar error ERGDCT01
	 */
	public static final String COD_ER_ID_CUESTIONARIO = "ERGDCT01";
	
	/**
	 * Variable utilizada para declarar error EROBID01
	 */
	public static final String COD_ER_NUMERO_CONTRATO = "ERGDCT02";
	
	/**
	 * Variable utilizada para declarar error EROBID01
	 */
	public static final String COD_ER_CUESTIONARIO_INCOMPLETO = "ERGDCT03";
	
	/**
	 * Variable utilizada para declarar error ERACDT01
	 */
	public static final String COD_ER_NUMERO_CNTR_VACIO = "ERACDT01";
	
	/**
	 * Variable utilizada para declarar error ERACDT02
	 */
	public static final String COD_ER_BUC_VACIO = "ERACDT02";
	
	/**
	 * Variable utilizada para declarar error ERACDT03
	 */
	public static final String COD_ER_INFO_BD_INEXISTENTE = "ERACDT03";
	
	/**
	 * Variable utilizada para declarar error ERACDT04
	 */
	public static final String COD_ER_GENERAR_XML = "ERACDT04";
	
	/**
	 * Variable utilizada para declarar error ERACDT05
	 */
	public static final String COD_ER_LLAMADO_WS = "ERACDT05";
	
	/**
	 * Variable utilizada para declarar error ERACDT06
	 */
	public static final String COD_ER_LLAMADO_390 = "ERACDT06";
	
	/**
	 * Variable utilizada para declarar error ERACDT07
	 */
	public static final String COD_ER_DATOS_390 = "ERACDT07";
	
	
	/**
	 * Variable utilizada para describir error DESC_ER_ID_APLICACION
	 */
	public static final String DESC_ER_ID_APLICACION = "Es necesario proporcionar un Identificador de Aplicativo, sin este dato no es posible realizar la operaci\u00F3n";
	/**
	 * Variable utilizada para describir error DESC_ER_ID_APLICACION
	 */
	public static final String DESC_ER_ID_APLICACION_VAL = "Es necesario proporcionar un Identificador de Aplicativo Valido, sin este dato no es posible realizar la operaci\u00F3n";
	
	/**
	 * Variable utilizada para describir error DESC_ER_BUC
	 */
	public static final String DESC_ER_BUC = "Es necesario proporcionar un BUC, sin este dato no es posible realizar la operaci\u00F3n";
	/**
	 * Variable utilizada para describir error DESC_ER_ID_CUESTIONARIO
	 */
	public static final String DESC_ER_ID_CUESTIONARIO = "Es necesario proporcionar un Identificador de Cuestionario, sin este dato no es posible realizar la operaci\u00F3n";
	/**
	 * Variable utilizada para describir error DESC_ER_NUMERO_CONTRATO
	 */
	public static final String DESC_ER_NUMERO_CONTRATO = "Es necesario proporcionar un N\u00FAmero de Contrato, sin este dato no es posible realizar la operaci\u00F3n";
	/**
	 * Variable utilizada para describir error DESC_ER_CUESTIONARIO_INCOMPLETO
	 */
	public static final String DESC_ER_CUESTIONARIO_INCOMPLETO = "La informaci\u00F3n del Cuestionario a\u00FAn es incompleta, es necesario terminar el proceso";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT01
	 */
	public static final String DESC_ER_NUMERO_CNTR_VACIO = "El n\u00FAmero de contrato no fue informado";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT02
	 */
	public static final String DESC_ER_BUC_VACIO = "El n\u00FAmero de cliente no fue informado";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT03
	 */
	public static final String DESC_ER_INFO_BD_INEXISTENTE = "No existe informaci\u00F3n en la Base de Datos del sistema";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT04
	 */
	public static final String DESC_ER_GENERAR_XML = "No fue posible generar el XML con la informaci\u00F3n";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT05
	 */
	public static final String DESC_ER_LLAMADO_WS = "No fue posible realizar el llamado al servicio web";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT06
	 */
	public static final String DESC_ER_LLAMADO_390 = "No fue posible obtener el Nivel de Riesgo y el Indicador UPLD";
	
	/**
	 * Variable utilizada para declarar descripcion ERACDT07
	 */
	public static final String DESC_ER_DATOS_390 = "No existen los valores para Nivel de Riesgo y/o Indicador UPLD";
	
	/**
	 * Obtiene un Identificador de Cuestionario
	 * @param idAplicacion Identificador de la aplicacion que esta realizando la operacion
	 * @param psession Objeto de sesion de Agave
	 * @return Identificador de Cuestionario generado
	 * @throws BusinessException En caso de un problema con la informacion
	 */
	public String obtenerIdCuestionario(String idAplicacion, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Asiga un numero de contrato a un cuestionario
	 * @param idCuestionario Identificador del cuestionario al cual se le va a realizar la asignacion de contrato
	 * @param numeroContrato Contrato que se va a asignr
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException En caso de un problema con la operacion
	 */
	public void asignarNumeroContrato(String idCuestionario, String numeroContrato, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene los datos generales de un cuestionario
	 * @param buc Codigo de cliente al que pertenece el cuestionario
	 * @param numeroContrato Numero de contrato asignado al cuestionario
	 * @param fechaInicio Fecha inicial de creacion del contrato
	 * @param fechaFin Fecha final de creacion del contrato
	 * @param psession Objeto de sesion de Agave
	 * @return List<CuestionarioWSDTO> Lista de cuestionarios que coinciden con los parametros de busqueda
	 * @throws BusinessException En caso de un problema con la operacion
	 */
	public List<CuestionarioWSDTO> obtenerDatosGeneralesCuestionario(
			String buc, String numeroContrato, Date fechaInicio, Date fechaFin, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Metodo para obtener el Nivel de riesgo y el Indicador UPLD despues de capturar los cuestionarios IP e IC
	 * @param idCuestionario Id generado para identificar a los cuestionarios IP e IC
	 * @param psession  Objeto de sesion de Agave
	 * @return  Nivel de riesgo e indicador UPLD separados por pipe |
	 * @throws BusinessException En caso de un problema con la operacion
	 */
	public List<HashMap<String, Object>> obtenerNivRIndU(String idCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Realiza la actualizacion de la informacion en Norkom
	 * @param idCuestionario Identificador de Cuestionario del que se desea actualizar la informacion
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException En caso de un problema con la operacion
	 */
	public void actualizarDatosEnNorkom(String idCuestionario, ArchitechSessionBean psession) throws BusinessException;
}
