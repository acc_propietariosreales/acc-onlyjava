/**
 * Isban Mexico
 *   Clase: AdministradorOrigenDestinoRecursosBO.java
 *   Descripcion: Interfaz para el BO AdministradorOrigenDestinoRecursosBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
/**
 * @author jmquillo
 *
 */
@Remote
public interface AdministradorOrigenDestinoRecursosBO {
	
	/**
	 * Obtiene las preguntas que se solicitan con bsae al Origen de los recursos
	 * @param tipoPersona Tipo de persona a la que pertenecen las preguntas
	 * @param subtipoPersona Subtipo de persona a la que pertencen las preguntas
	 * @param opcionRespuesta Clave del recurso del cual se desean obtener las preguntas
	 * @param psession Objeto de sesion de Agave
	 * @return List<PreguntaDTO> Preguntas asignadas al tipo de recurso
	 * @throws BusinessException En caso de error con las operaciones
	 */
	public List<PreguntaDTO> obtenerPreguntasPorRespuesta(String tipoPersona, String subtipoPersona, int opcionRespuesta, ArchitechSessionBean psession)
	throws BusinessException;
}
