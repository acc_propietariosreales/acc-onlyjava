/**
 * Isban Mexico
 *   Clase: AdministradorRelacionadosBO.java
 *   Descripcion: Interfaz para el BO AdministradorRelacionadosBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */


package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosWebServiceDTO;

@Remote
public interface AdministradorRelacionadosBO {
	/**
	 * Variable utilizada para declarar error
	 */
	public static final String COD_ER_ID_APLICACION = "ERCGRL01";
	/**
	 * Variable utilizada para declarar error
	 */
	public static final String COD_ER_BUC = "ERCGRL02";
	
	/**
	 * Codigo para la advertencia de registro erroneos
	 */
	public static final String COD_REGISTROS_ERRONEOS = "WNRLGD00";
	
	/**
	 * Variable utilizada para declarar error
	 */
	public static final String COD_ER_ID_CUESTIONARIO = "ERCGRLB02";
	
	/**
	 * Variable utilizada para describir error DESC_ER_ID_APLICACION
	 */
	public static final String DESC_ER_ID_APLICACION = "Es necesario proporcionar un Identificador de Aplicativo, sin este dato no es posible realizar la operaci\u00F3n";
	/**
	 * Variable utilizada para describir error DESC_ER_BUC
	 */
	public static final String DESC_ER_BUC = "Es necesario proporcionar un BUC, sin este dato no es posible realizar la operaci\u00F3n";
	/**
	 * Variable utilizada para describir error DESC_ER_ID_CUESTIONARIO
	 */
	public static final String DESC_ER_ID_CUESTIONARIO = "Es necesario proporcionar un Identificador de Cuestionario, sin este dato no es posible realizar la operaci\u00F3n";
	
	/**
	 * Variable utilizada para describir TIPO_RELACION_INTERVINIENTE
	 */
	public static final String TIPO_RELACION_INTERVINIENTE = "I";
	/**
	 * Variable utilizada para describir TIPO_RELACION_PROVEEDOR
	 */
	public static final String TIPO_RELACION_PROVEEDOR = "P";
	/**
	 * Variable utilizada para describir TIPO_RELACION_ACCIONISTA
	 */
	public static final String TIPO_RELACION_ACCIONISTA = "A";
	/**
	 * Variable utilizada para describir TIPO_RELACION_BENEFICIARIO
	 */
	public static final String TIPO_RELACION_BENEFICIARIO = "B";
	
	/**
	 * Agrega relacionados a un cliente
	 * @param relacionadosWS Relacionados que se desean gregar
	 * @param psession Objeto de sesion de Agave
	 * @return List<RelacionadoDTO> Relacionados que no fue posible agregar
	 * @throws BusinessException En caso de error con la informacion
	 */
	public List<RelacionadoDTO> agregarRelacionados(RelacionadosWebServiceDTO relacionadosWS, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/** 
	 * 
	 * @param numeroContrato de obtenerRelacionados
	 * @param psession de obtenerRelacionados
	 * @return List<RelacionadoDTO>
	 * @throws BusinessException de obtenerRelacionados
	 */
	public List<RelacionadoDTO> obtenerRelacionados(String numeroContrato, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene los relacionados numero de contrato y por tipo 
	 * @param claveClienteCuest Clave cliente-cuestionario a los que estan asignados los relacionados
	 * @param tipoRelacionado Tipo de relacionado que se quiere buscar
	 * @param psession Objeto de sesion de Agave
	 * @return Lista de relacionados que coincidan con el tipo y esten asignados al contrato especificado
	 * @throws BusinessException En caso de un error de negocio
	 */
	public List<RelacionadoDTO> obtenerRelacionadosPorClaveClienteCuestTipo(int claveClienteCuest, String tipoRelacionado, ArchitechSessionBean psession) 
	throws BusinessException;
}
