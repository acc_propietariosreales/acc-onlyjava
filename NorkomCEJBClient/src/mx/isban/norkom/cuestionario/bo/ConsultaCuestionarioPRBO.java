/**
 * Isban Mexico
 *   Clase: ConsultaCuestionarioPRBO.java
 *   Descripcion: Interfaz para el BO ConsultaCuestionarioPRBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
 
package mx.isban.norkom.cuestionario.bo;

import java.util.Map;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.dto.RespuestasRes;

/**
 * @author Miguel Angel Ayala Franco
 *
 */
@Remote
public interface ConsultaCuestionarioPRBO {
	/**
	 * Pregunta 'Tipo de Persona' del Cuestionario IP
	 */
	public static final String PREGUNTA_TIPO_PERSONA_IP = "Tipo de Persona";
	
	/**
	 * Leyenda para la pregunta 'Tipo de Persona' para las personas fisicas
	 */
	public static final String LEYENDA_PERSONA_FISICA = "Personas F\u00EDsicas";
	
	/**
	 * Leyenda para la pregunta 'Tipo de Persona' para las personas fisicas con actividad empresarial
	 */
	public static final String LEYENDA_PERSONA_FISICA_AE = "Personas F\u00EDsicas con Actividad Empresarial";
	
	/**
	 * Leyenda para la pregunta 'Tipo de Persona' para las personas morales
	 */
	public static final String LEYENDA_PERSONA_MORAL = "Personas Morales";
	
	/**
	 * constante para la cadena DIA
	 */
	public static final String DIA="DIA";
	
	/**
	 * constante para la cadena STRDIA
	 */
	public static final String STRDIA="strDia";
	
	/**
	 * constante para la cadena MES
	 */
	public static final String MES="MES";
	
	/**
	 * constante para la cadena STRMES
	 */
	public static final String STRMES="strMes";
	
	/**
	 * constante para la cadena ANIO
	 */
	public static final String ANIO="ANIO";
	
	/**
	 * constante para la cadena STRANIO
	 */
	public static final String STRANIO="strAnio";
	
	/**
	 * constante para la cadena SUCURSAL
	 */
	public static final String SUCURSAL="NOMBRE Y NUMERO";
	
	/**
	 * constante para la cadena STRSUCURSAL
	 */
	public static final String STRSUCURSAL="strSucursal";
	
	/**
	 * constante para la cadena NOMBRE
	 */
	public static final String NOMBRE="NOMBRE";
	
	/**
	 * constante para la cadena STRNOMBRE
	 */
	public static final String STRNOMBRE="srtNombre";
	
	/**
	 * constante para la cadena ZONA
	 */
	public static final String ZONA="ZONA";
	
	/**
	 * constante para la cadena STRZONA
	 */
	public static final String STRZONA="strZona";
	
	/**
	 * constante para la cadena PLAZA
	 */
	public static final String PLAZA="PLAZA";
	
	/**
	 * constante para la cadena STRPLAZA
	 */
	public static final String STRPLAZA="strPlaza";
	
	/**
	 * constante para la cadena NUMCLIENTE
	 */
	public static final String NUMCLIENTE="NUMERO CLIENTE";
	
	/**
	 * constante para la cadena STRCODCLIENTE
	 */
	public static final String STRCODCLIENTE="strCodCliente";
	
	/**
	 * constante para la cadena CUENTA
	 */
	public static final String CUENTA="CUENTA/CONTRATO";
	
	/**
	 * constante para la cadena STRCUECON
	 */
	public static final String STRNUMCUECON="strNumCueCon";
	
	/**
	 * constante para la cadena SEGMENTO
	 */
	public static final String SEGMENTO="SEGMENTO";
	
	/**
	 * constante para la cadena STRSEGMENTO
	 */
	public static final String STRSEGMENTO="strSegmento";
	
	/**
	 * constante para la cadena PRODUCTO
	 */
	public static final String PRODUCTO="PRODUCTO CONTRATADO";
	
	/**
	 * constante para la cadena STRPRODCONTRATADO
	 */
	public static final String STRPRODUCTO="strProdContratado";
	
	/**
	 * constante para la cadena PAIS
	 */
	public static final String PAIS="PAIS DE RESIDENCIA";
	
	/**
	 * constante para la cadena STRPAIS
	 */
	public static final String STRPAIS="strPais";
	
	/**
	 * constante para la cadena ESTADO
	 */
	public static final String ESTADO="ESTADO";
	
	/**
	 * constante para la cadena STRESTADO
	 */
	public static final String STRESTADO="strEstado";
	
	/**
	 *constante para la cadena  MUNICIPIO
	 */
	public static final String MUNICIPIO="MUNICIPIO";

	/**
	 * constante para la cadena STRMUNICIPIO
	 */
	public static final String STRMUNICIPIO="strMunicipio";
	
	/**
	 * constante para la cadena NACIONALIDAD
	 */
	public static final String NACIONALIDAD="NACIONALIDAD";
	
	/**
	 * constante para la cadena STRNACIONALIDAD
	 */
	public static final String STRNACIONALIDAD="strNacionalidad";
	
	/**
	 * constante para la cadena ACTIVIDAD GENERICA
	 */
	public static final String ACTGENERICA="ACTIVIDAD GENERICA";
	
	/**
	 * constante para la cadena STRACTGENERICA
	 */
	public static final String STRACTGENERICA="strActGenerica";
	
	/**
	 * constante para la cadena ACTIVIDAD ESPECIFICA
	 */
	public static final String ACTESPECIFICA="ACTIVIDAD ESPECIFICA";
	
	/**
	 * constante para la cadena STRACTESPECIFICA
	 */
	public static final String STRACTESPECIFICA="strActEspefica";
	/**
	 * constante para la cadena NOMBRE CLIENTE
	 */
	public static final String NOMBRECLIENTE="NOMBRE";
	
	/**
	 * constante para la cadena STRNOMBRECLIENTE
	 */
	public static final String STRNOMBREC="strNombre";
	/**
	 * Metodo para obtener las respuestas de un cuestionario
	 * @param request Informacion del cuestionario del cual se desean obtener las respuestas
	 * @param psession Objeto de sesion de Agave
	 * @return respuestas Respuestas del Cuestionario IP e IC
	 * @throws BusinessException En caso de error con la informacion
	 */
	public RespuestasRes consultaPreguntasRespuestas(RequestObtenerCuestionario request,ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene las respuestas IC de un cuestionario ordenandolas por abreviatura
	 * @param numeroContrato Numero de contrato del que se desean obtener las respuestas
	 * @param psession Objeto de sesion de Agave
	 * @return Mapa<Abreviatura, Respuesta> con las respuestas IC del cuestionario
	 * @throws BusinessException En caso de error con la informacion
	 */
	public Map<String, PreguntaDTO> obtenerRespuestasICPorAbreviatura(String numeroContrato, ArchitechSessionBean psession)
	throws BusinessException;
}
