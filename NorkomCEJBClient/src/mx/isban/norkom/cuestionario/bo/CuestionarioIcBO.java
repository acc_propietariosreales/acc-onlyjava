/**
 * Isban Mexico
 *   Clase: CuestionarioIcBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIcBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
/**
 * interfaz de CustionarioBO
 * @author STEFANINI (Leopoldo F Espinosa R) 26/11/2014
 *
 */
@Remote
public interface CuestionarioIcBO {
	/**
	 * Metodo para obtener el Tipo de cuestionario
	 * @param respuestas Contiene la lista de respuestas del cuestionario
	 * @param claveClienteCuestionario la clave del cliente cuestionario
	 * @param claveCuestionario clave del cuestionario
	 * @param rsvalidacion variable para saber si se graban o no devuelta las preguntas por ser Regimen Simplificado
	 * @param psession Objeto de sesion de Agave
	 * @return Tipo de cuestionario con base a los parametros de busqueda
	 * @throws BusinessException En caso de error con la informacion
	 */
	public CuestionarioDTO obtenerTipoCuestionario(List<CuestionarioRespuestaDTO> respuestas,int claveClienteCuestionario,int claveCuestionario,boolean rsvalidacion ,ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene el Tipo de cuestionario por clave
	 * @param claveCuestionario Clave del tipo de cuestionario
	 * @param claveClienteCuestionario clave cte cuestionario para realizar la busqueda
	 * @param psession Objeto de sesion de Agave
	 * @return CuestionarioDTO con la informacion obtenida
	 * @throws BusinessException En caso de error con la informacion 
	 */
	public CuestionarioDTO obtenerTipoCuestionarioPorClave(int claveCuestionario,int claveClienteCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * @param claveClienteCuestionario para realizar la busqueda
	 * @param psession Objeto de sesion de Agave
	 * @return CuestionarioDTO con respuesta obtenida
	 * @throws BusinessException En caso de error con la informacion
	 */
	public CuestionarioDTO obtenerDatosGeneralesCuestionarioPorClaveCC(int claveClienteCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * @param claveCuestionario para realizar la busqueda
	 * @param psession Objeto de sesion de Agave
	 * @return List<PreguntaDTO> lista de preguntas
	 * @throws BusinessException En caso de error con la informacion
	 */
	public List<PreguntaDTO> obtenerPreguntasCuestionarioIc(int claveCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * @param requestCuestionario dto de request con datos de busqueda
	 * @param psession Objeto de sesion de Agave
	 * @return StringBuilder
	 * @throws BusinessException En caso de error con la informacion
	 */
	public StringBuilder obtenerCuestionarioIcHtml(CuestionarioDTO requestCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * @param claveClienteCuest para realizar la busqueda
	 * @param claveCuestIC para realizar la busqueda por ic
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException En caso de error con la informacion
	 */
	public void asignarClaveCuestionarioIC(int claveClienteCuest, int claveCuestIC, ArchitechSessionBean psession) throws BusinessException;
}
