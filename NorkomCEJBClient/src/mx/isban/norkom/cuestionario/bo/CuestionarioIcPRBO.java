/**
 * Isban Mexico
 *   Clase: CuestionarioIcBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIcPRBO para 
 *   los relacionados por bloques
 *   
 *   Control de Cambios:
 *   1.0 Jun 27, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
/**
 * interfaz de PreguntasIcBO
 * @author STEFANINI (Leopoldo F Espinosa R) 27/06/2017
 *
 */
@Remote
public interface CuestionarioIcPRBO{
	/**
	 * Metodo para obtener las preguntas solo del cuestionario IC de tipo 
	 * A1 cuando aun no se han terminado de procesar los Relacionados
	 * @param respuestas respuestas de IP a guardar
	 * @param claveClienteCuestionario clave cliente cuestionario
	 * @param claveCuestionario clave de cuestionario
	 * @param rsyavalidado rsyavalidado
	 * @param intentos numero de intentos
	 * @param grabado boolean para indicar si se graba la informacion 
	 * para la calificacion o no
	 * @param architechBean bean de session
	 * @throws BusinessException con mensaje de error
	 * @return CuestionarioDTO con las preguntas de IC
	 */
	CuestionarioDTO obtenerTipoCuestionarioA1(
			List<CuestionarioRespuestaDTO> respuestas,
			int claveClienteCuestionario, int claveCuestionario,
			boolean rsyavalidado,int intentos,boolean grabado, ArchitechSessionBean architechBean)throws BusinessException;
	
}
