/**
 * Isban Mexico
 *   Clase: CuestionarioIpBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIpBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
/**
 * interfaz de CustionarioBO
 * @author STEFANINI (Leopoldo F Espinosa R) 26/11/2014
 *
 */
@Remote
public interface CuestionarioIpBO {
	/**
	 * Metodo para obtener el cuestionario asignado
	 * @param peticion CuestionarioDTO
	 * @param psession Objeto de sesion de Agave
	 * @return BeanResultadoTipoPregunta con resultado
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public CuestionarioDTO obtenerTipoCuestionario(CuestionarioDTO peticion, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Agrega un cuestionario a un cliente
	 * @param cuestionario Cuestionario que se desea agregar
	 * @param sesion Objeto de sesion de Agave
	 * @return CuestionarioDTO Cuestionario con la clave de creacion
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public CuestionarioDTO agregarCuestionarioCliente(CuestionarioDTO cuestionario, ArchitechSessionBean sesion)
	throws BusinessException;
	
	/**
	 * Complementa la informacion del cuestionario. Se agrega pais de residencia, pais de nacionalidad y el estado con
	 * el municipio en caso de que aplique
	 * @param cuestionario Cuestionario al cual se desea agregar la informacion
	 * @param sesion Objeto de sesion de Agave
	 * @return CuestionarioDTO Cuestionario actualizado
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public CuestionarioDTO complementarInformacionCuestionario(CuestionarioDTO cuestionario, ArchitechSessionBean sesion)
	throws BusinessException;
	
	/**
	 * Obtiene el codigo HTML con las preguntas del cuestionario IP
	 * @param requestCuestionario Informacion del cuestionario del cual se desean obtener las preguntas
	 * @param psession Objeto de sesion de Agave
	 * @return StringBuilder Cuestionario IP en HTML
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion 
	 */
	public StringBuilder obtenerCuestionarioIpHtml(CuestionarioDTO requestCuestionario, ArchitechSessionBean psession) throws BusinessException;
}
