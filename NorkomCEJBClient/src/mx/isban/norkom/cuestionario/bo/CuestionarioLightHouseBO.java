/**
 * Isban Mexico
 *   Clase: CuestionarioIpBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIpBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseRequest;
import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseResponse;
/**
 * interfaz de CustionarioBO
 * @author STEFANINI (J ULISES GARCIA MARTINEZ) 26/05/2016
 */
@Remote
public interface CuestionarioLightHouseBO {
	/**
	 * Procesa la información de LightHouse - Guardar la informacion y obtener la calificacion en Norkom 
	 * @param request Cuestionario con la informacion de la solicitud
	 * @return El cuestionario El resultado de la operacion
	 * @throws BusinessException En caso de un problema con la operacion
	 */
	public CuestionarioLightHouseResponse procesarCuestionario(CuestionarioLightHouseRequest request) throws BusinessException;
}
