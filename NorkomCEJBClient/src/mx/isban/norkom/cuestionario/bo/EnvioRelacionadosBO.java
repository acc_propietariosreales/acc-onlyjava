/**
 * Isban Mexico
 *   Clase: EnvioRelacionadosBO.java
 *   Descripcion: Interfaz para el BO EnvioRelacionadosBO
 *   
 *   Control de Cambios:
 *   1.0 Jun 23, 2017 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerRelacionados;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerRelacionados;
import mx.isban.norkom.ws.dto.RequestDatosRelacionadosWSDTO;
/**
 * interfaz para el envio de relacionados
 * en el flujo de relacionados no ok en norkom
 * @author lespinosa
 *
 */
@Remote
public interface EnvioRelacionadosBO {
	
	/**
	 * Obtiene la informacion de los relacionados
	 * primero en DB y despues del WS de Norkom
	 * @param request Objeto para ejecutar la consulta
	 * @param psession Objeto de sesion de Agave
	 * @return ResponseObtenerRelacionados Informacion de los relacionados
	 * @throws BusinessException Mensaje de error
	 */
	ResponseObtenerRelacionados obtenerEstatusRelacionados(RequestObtenerRelacionados request, ArchitechSessionBean psession) throws BusinessException;
	/**
	 * Metodo para actualizar el estatus a error de los relacionados 
	 * que se enviaron pero despues de los intentos en IP e IC
	 * no se obtuvo aun la respuesta ok
	 * en la base de datos
	 * @param request
	 * @param psession
	 * @return
	 * @throws BusinessException
	 */
	void actualizaEstatusRelacionadosErr(RequestObtenerRelacionados request, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Actualiza estatus de la relacion en base a la clave de la relacion
	 * @param request Objeto para ejecutar la actualizacion del estatus
	 * @param estatus Estatus OK
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException Mensaje de error
	 */
	void actualizarEstatusRel(RequestDatosRelacionadosWSDTO request, String estatus, ArchitechSessionBean psession)throws BusinessException;
	
}
