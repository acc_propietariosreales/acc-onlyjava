/**
 * Isban Mexico
 *   Clase: GeneraXmlDAO.java
 *   Descripcion: BO para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;

@Remote
public interface GeneraXmlBO {
	
	/** En caso de no existe un valor para el limite de algun relacionado entonces se toma este limite */
	public static final int LIMITE_MAXIMO_RELACIONADOS = 150;
	/** Parametro para obtener el limite de Intervinientes */
	public static final String PARAM_LIMITE_INTERVINIENTES = "limIntervenietes";
	/** Parametro para obtener el limite de Beneficiarios */
	public static final String PARAM_LIMITE_BENEFICIARIOS = "limBeneficiarios";
	/** Parametro para obtener el limite de Accionistas */
	public static final String PARAM_LIMITE_ACCIONISTAS = "limAccionistas";
	/** Parametro para obtener el limite de Proveedores */
	public static final String PARAM_LIMITE_PROVEEDORES = "limProveedores";
	/**
	 * Genera la estructura XML de la informacion que se va a mandar a realizar la calificacion a Norkom
	 * @param idCuestionario Identificador de cuestionario del que se va a obtener la informacion
	 * @param psession Objeto de sesion de Agave
	 * @return String Estructuca del XML
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public String generarEstructuraXML(String idCuestionario, ArchitechSessionBean psession) throws BusinessException;
}

