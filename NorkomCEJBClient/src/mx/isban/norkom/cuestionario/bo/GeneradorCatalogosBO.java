/**
 * Isban Mexico
 *   Clase: GeneradorCatalogosBO.java
 *   Descripcion: Interfaz para el BO GeneradorCatalogosBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface GeneradorCatalogosBO {
	 
	/**
	 * Genera el codigo de un SELECT de HTML con los paises registrados en ACC
	 * @param pregunta Pregunta a la que se va a asignar el SELECT
	 * @param session Objeto de sesion de Agave
	 * @return StringBuilder Codigo HTML con la pregunta y el SELECT de los paises
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public StringBuilder generarCatalogoPaises(PreguntaDTO pregunta, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Genera el codigo de un SELECT de HTML con los Recusos registrados en ACC
	 * @param cuestionario Se utiliza para definir el tipo de persona
	 * @param pregunta Pregunta a la que van a ser asignados los recursos
	 * @param tipoRecurso Define si se trata de los recusos de Origen o de Destino
	 * @param session Objeto de sesion de Agave
	 * @return StringBuilder Codigo HTML con la pregunta y el SELECT de los recursos
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public StringBuilder generarCatalogoRecursos(
			CuestionarioDTO cuestionario, PreguntaDTO pregunta, String tipoRecurso, ArchitechSessionBean session) 
	throws BusinessException;
}
