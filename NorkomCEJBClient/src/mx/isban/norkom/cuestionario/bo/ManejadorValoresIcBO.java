/**
 * Isban Mexico
 *   Clase: CuestionarioIcBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIcBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
/**
 * interfaz de CustionarioBO
 * @author STEFANINI (Leopoldo F Espinosa R) 26/11/2014
 *
 */
@Remote
public interface ManejadorValoresIcBO {
	/**
	 * Genera el codigo HTML de una pregunta que se desea agregar a un cuestionario
	 * @param cuestionario Informacion del cuestionario al que pertenece la pregunta
	 * @param pregunta Informacion del pregunta con la cual se va a generar el codigo HTML
	 * @param valor Valor predeterminado de la pregunta
	 * @param session Objeto de sesion de Agave
	 * @return Codigo HTML con la informacion de la pregunta
	 * @throws BusinessException En caso de error con la informacion
	 */
	public StringBuilder agregarPregunta(CuestionarioDTO cuestionario, PreguntaDTO pregunta, String valor, ArchitechSessionBean session)
	throws BusinessException;
	
	/**
	 * Define si la persona que esta llenado el cuestionario es PEP
	 * @param cuestionario Cuestionario del que se va a obtener la informacion
	 * @param session Objeto de sesion de Agave
	 * @return CuestionarioDTO con datos de la respuesta
	 * @throws BusinessException En caso de error con la validacion
	 */
	public CuestionarioDTO definirPEP(CuestionarioDTO cuestionario, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Define el valor de la pregunta PEP a afirmativo
	 * @param cuestionario Cuestionario al que se le va a actualizar la pregunta
	 * @param session Objeto de sesion de Agave
	 * @return CuestionarioDTO con datos de la respuesta
	 * @throws BusinessException En caso de error con la informacion
	 */
	public CuestionarioDTO actualizarValorPEPAfirmativo(CuestionarioDTO cuestionario, ArchitechSessionBean session) throws BusinessException;
}
