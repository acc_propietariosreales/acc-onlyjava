/**
 * Isban Mexico
 *   Clase: ObtenerCuestionarioWSBO.java
 *   Descripcion: Interfaz para el BO ObtenerCuestionarioWSBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.bean.ResponseObtenerCuestionario;
import mx.isban.norkom.ws.dto.CuestionarioWSDTO;

@Remote
public interface ObtenerCuestionarioWSBO {
	/***
	 * Metodo para obtener los datos del Cuestionario IP en base al numero de contrato
	 * @param peticion datos para obtener el PDF
	 * @param psession Objeto de sesion de Agave
	 * @return ResponsePeticionPdf con datos de la respuesta
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public ResponseObtenerCuestionario obtenerPdfIP(RequestObtenerCuestionario peticion, ArchitechSessionBean psession)throws BusinessException;
	/**
	 * Metodo para obtener los datos del comprobante IC
	 * @param peticion datos para obtener el PDF
	 * @param cuestionarioBD Informacion general del Cuestionario
	 * @param dto datos de el primer reporte
	 * @param psession Objeto de sesion de Agave
	 * @return  ResponsePeticionPdf con datos de la respuesta
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public ResponseObtenerCuestionario obtenerPdfIC(RequestObtenerCuestionario peticion, CuestionarioWSDTO cuestionarioBD, ResponseObtenerCuestionario dto,ArchitechSessionBean psession)throws BusinessException;
	
	/**
	 * Obtiene los datos generales de un cuestionario en donde el numero de contrato es diferente de nulo
	 * @param datosConsulta Datos de consulta
	 * @param psession Objeto de sesion de Agave
	 * @return CuestionarioWSDTO Informacion del cuestionario
	 * @throws BusinessException En caso de un error al obtener los datos
	 */
	public CuestionarioWSDTO obtenerDatosGeneralesCuestionarioPorIdCntrNoNulo(RequestObtenerCuestionario datosConsulta, ArchitechSessionBean psession) 
	throws BusinessException;
}
