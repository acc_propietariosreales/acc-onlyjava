/**
 * Isban Mexico
 *   Clase: OpcionesPreguntaBO.java
 *   Descripcion: Interfaz para el BO OpcionesPreguntaBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
 

package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestOpcionesPregunta;
import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;


/**
 * 
 * @author STEFANINI (Leopoldo F Espinosa R) 04/12/2014
 *
 */
@Remote
public interface OpcionesPreguntaBO {
	/**
	 * 
	 * Metodo para obtener las opciones de pregunta
	 * @param peticion RequestOpcionesPregunta
	 * @param psession Objeto de sesion de Agave
	 * @return BeanResultadoTipoPregunta con resultado
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	ResponseOpcionesPregunta obtenerOpcionesPregunta(RequestOpcionesPregunta peticion,ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene una pregunta con base a su seccion y su abreviatura
	 * @param idSeccion Seccion a la que pertenece la abreviatura
	 * @param idAbreviatura Abreviatura de la pregunta
	 * @param psession Objeto de sesion de Agave
	 * @return ResponseOpcionesPregunta Pregunta obtenida con base a la abreviatura y la seccion
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	ResponseOpcionesPregunta obtenerOpcionesPorSeccionAbreviatura(String idSeccion, String idAbreviatura, ArchitechSessionBean psession) throws BusinessException;

}
