/**
 * Isban Mexico
 *   Clase: OrigenDestinoRecursosBO.java
 *   Descripcion: Interfaz para el BO OrigenDestinoRecursosBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.RecursoDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface OrigenDestinoRecursosBO {
	
	/**
	 * Obtiene los recursos con base al Tipo de Persona y al Tipo de Recurso
	 * @param tipoPersona Filtro del tipo de persona de los recursos
	 * @param tipoRecurso Filtro del tipo de recurso, Orige o Destino
	 * @param session Objeto de sesion de Agave
	 * @return List<RecursoDTO> Recursos obtenidos con base a los filtros de busqueda
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	List<RecursoDTO> obtenerRecursos(String tipoPersona, String tipoRecurso, ArchitechSessionBean session) throws BusinessException;
}
