/**
 * Isban Mexico
 *   Clase: PaisesBO.java
 *   Descripcion: Interfaz para el BO PaisesBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.BeanResponsePaises;
import mx.isban.norkom.cuestionario.dto.PaisDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface PaisesBO {
	 
	 /** 
	  * Metodo para obtener los paises
	  * @param session Objeto de sesion de Agave
	  * @return BeanResponsePaises Objeto con la lista de paises en ACC
	  * @throws BusinessException En caso de un error con la informacion o durante la operacion
	  */
	 public BeanResponsePaises obtenerPaises(ArchitechSessionBean session) throws BusinessException;
	 
	 /**
	  * Obtiene la informacion de un Pais con base a su codigo
	  * @param codigoPais Codigo del Pais
	  * @param sesion Objeto de sesion de Agave
	  * @return PaisDTO Pais obtenido con base al codigo proporcionado
	  * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public PaisDTO obtenerPaisPorCodigo(String codigoPais, ArchitechSessionBean sesion) throws BusinessException;
}
