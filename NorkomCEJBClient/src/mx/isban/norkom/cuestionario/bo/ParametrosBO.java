/**
 * Isban Mexico
 *   Clase: ParametrosBO.java
 *   Descripcion: Interfaz para el BO ParametrosBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.ParametroDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface ParametrosBO {
	/**
	 * Obtiene un parametro de ACC con base al nombre
	 * @param nombreParametro Nombre del parametro que se desea obtener
	 * @param psession Objeto de sesion de Agave
	 * @return ParametroDTO Parametro que coincide con el nombre proporcionado
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public ParametroDTO obtenerParametroPorNombre(String nombreParametro, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene los parametros que se manejan en ACC
	 * @param psession Objeto de sesion de Agave
	 * @return List<ParametroDTO> Parametros existentes en ACC
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public List<ParametroDTO> obtenerParametros(ArchitechSessionBean psession) throws BusinessException;
}
