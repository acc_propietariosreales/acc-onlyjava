/**
 * Isban Mexico
 *   Clase: PersonasBO.java
 *   Descripcion: Interfaz para el BO PersonasBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.IndicadorOPD2WS;
import mx.isban.norkom.cuestionario.dto.ResponseODF3DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE71DTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface PersonasBO {
	/**
	 * Codigo de error cuando falta el campo payLoad
	 */
	public static final String ERR_DATOS_ID_NORKOM = "ER0001"; 
	/**
	 * Codigo de error cuando falta el campo buc
	 */
	public static final String ERR_DATOS_BUC = "ER0002";
	/**
	 * Codigo de error cuando falta el campo nivelRiesgo
	 */
	public static final String ERR_DATOS_NIVEL_RIESGO = "ER0003";
	/**
	 * Codigo de error cuando falta el campo indicadorUpld
	 */
	public static final String ERR_DATOS_IND_UPLD = "ER0004";
	/**
	 * Codigo de error cuando el Nivel de Riesgo es incorrecto
	 */
	public static final String ERR_DATOS_NIVEL_RIESGO_INCORRECTO = "ER0005"; 
	/**
	 * Codigo de error cuando el Indicador UPLD es incorrecto
	 */
	public static final String ERR_DATOS_IND_UPLD_INCORRECTO = "ER0006"; 
	
	/**
	 * Obtiene el correo electronico del cliente a traves de la transaccion ODF3
	 * @param buc Cliente del que se quiere obtener el correo electronico
	 * @param sesion Objeto de la sesion de Agave
	 * @return ResponseODF3DTO Objeto con el correo electronico o con una cadena vacia
	 * @throws BusinessException En caso de un error de negocio durante la operacion
	 */
	public ResponseODF3DTO obtenerCorreoElectronicoODF3(String buc, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Ejecuta las operaciones de actualizar el Nivel de Riesgo y el Indicador UPLD en Personas
	 * @param cuestionario Ojbeto del cual se va a obtener el cliente, nivel de riesgo e indicador UPLD
	 * @param sesion Objeto de la sesion de Agave
	 * @return CuestionarioDTO Cuestionario actualizado con la informacion de la actualizacion
	 * @throws BusinessException En caso de un error de negocio durante la operacion
	 */
	public CuestionarioDTO ejecutarOperacionesEnPersonas(CuestionarioDTO cuestionario, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Metodo para bloquear un cliente en Personas con A3-BLO
	 * @param cuestionario Ojbeto del cual se va a obtener el cliente
	 * @param sesion Objeto de la sesion de Agave
	 * @return CuestionarioDTO Cuestionario actualizado con la informacion de la actualizacion
	 * @throws BusinessException En caso de un error de negocio durante la operacion
	 */
	public CuestionarioDTO bloquearCliente(CuestionarioDTO cuestionario, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Realiza la calificacion manual del Nivel de Riesgo
	 * @param indicadorODP2 Objeto con la informacion para la actualizacion del Nivel de Riesgo
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException En caso de un problema de negocio
	 */
	public void ejecutarActualizacionCalificacionManual(IndicadorOPD2WS indicadorODP2, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene las referencias personales para una persona fisica
	 * @param buc Codigo del cliente del que se desean obtener las referencias
	 * @param sesion Objeto de sesion de Agave
	 * @return Lista de referencias
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public List<ResponsePE58DTO> obtenerReferenciasPersonasFisicasPE58(String buc, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Obtiene las referencias bancarias para una persona fisica con actividad empresarial
	 * @param buc Codigo del cliente del que se desean obtener las referencias
	 * @param sesion Objeto de sesion de Agave
	 * @return Lista de referencias bancarias
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public List<ResponsePE58DTO> obtenerReferenciasPersonasFisicasActEmpPE58(String buc, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Obtiene las referencias bancarias para una persona moral
	 * @param buc Codigo del cliente del que se desean obtener las referencias
	 * @param sesion Objeto de sesion de Agave
	 * @return Lista de referencias bancarias
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public List<ResponsePE71DTO> obtenerReferenciasPersonasMoralesPE71(String buc, ArchitechSessionBean sesion) throws BusinessException;
}
