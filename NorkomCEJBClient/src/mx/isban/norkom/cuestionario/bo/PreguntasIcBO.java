/**
 * Isban Mexico
 *   Clase: CuestionarioIcBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIcBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
/**
 * interfaz de PreguntasIcBO
 * @author STEFANINI (Leopoldo F Espinosa R) 26/11/2014
 *
 */
@Remote
public interface PreguntasIcBO {
	/**
	 * Obtiene una pregunta asignada a un cuestionario con base a su seccion y su abreviatura
	 * @param idCuestionario Identificador del cuestionario en el cual fue contestada la pregunta
	 * @param seccion Seccion de la pregunta
	 * @param abreviatura Abreviatura de la pregunta
	 * @param psession Objeto de sesion de Agave
	 * @return CuestionarioRespuestaDTO Pregunta obtenida con base a los filtros
	 * @throws BusinessException En caso de un error con la informacion o la consulta
	 */
	public CuestionarioRespuestaDTO obtenerPreguntaPorSeccionAbreviatura(String idCuestionario, String seccion, String abreviatura, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Obtiene la pregunta asignada a un cuestionario con base a las abreviaturas
	 * @param idCuestionario Identificador del cuestionario
	 * @param abreviaturas Abreviaturas en el formato "'ABRV1','ABRV2', ... , 'ABRVn'"
	 * @param psession Objeto de sesion de Agave
	 * @return List<PreguntaDTO> Preguntas encontradas
	 * @throws BusinessException En caso de un error con la informacion o la consulta
	 */
	public List<PreguntaDTO> obtenerPreguntasAsignadasPorAbreviaturas(String idCuestionario, String abreviaturas, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Obtiene una pregunta con base a sus abreviatura
	 * @param abreviatura Abreviatura de la pregunta, deben estar en el formato "'ABREV1'"
	 * @param psession Objeto de sesion de Agave
	 * @return PreguntaDTO Pregunta que coincide con la abreviatura
	 * @throws BusinessException En caso de un error con la informacion o la consulta
	 */
	public PreguntaDTO obtenerPreguntaPorAbreviatura(String abreviatura, ArchitechSessionBean psession) 
	throws BusinessException;
	
	/**
	 * Almacena las respuestas de un Cuestionario IC
	 * @param cuestionario Cuestionario IC al que se le van a guardar las respuestas
	 * @param respuestas Respuestas que se desean almacenar
	 * @param sesion Objeto de sesion de Agave
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion 
	 * @return CuestionarioDTO con datos de respuesta
	 */
	public CuestionarioDTO guardarRespuestasCuestionarioIC(CuestionarioDTO cuestionario, List<CuestionarioRespuestaDTO> respuestas, ArchitechSessionBean sesion)
	throws BusinessException;
	
	/**
	 * Obtiene las preguntas del Cuestionario con base a su clave
	 * @param claveCuestionario Clave del cuestionario del que se desean obtener las preguntas
	 * @param psession Objeto de sesion de Agave
	 * @return List<PreguntaDTO> Preguntas asignadas al cuestionario
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public List<PreguntaDTO> obtenerPreguntasCuestionarioPorClave(int claveCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Actualiza los valores de una respuesta
	 * @param respuesta La respuesta que se desea actualizar
	 * @throws BusinessException
	 * @throws ExceptionDataAccess
	 */
	public void ejecutarActualizacionRespuesta(CuestionarioRespuestaDTO respuesta) throws BusinessException;
}
