/**
 * Isban Mexico
 *   Clase: CuestionarioIpBO.java
 *   Descripcion: Interfaz para el BO CuestionarioIpBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
/**
 * interfaz de CustionarioBO
 * @author STEFANINI (Leopoldo F Espinosa R) 26/11/2014
 *
 */
@Remote
public interface PreguntasIpBO {
	/**
	 * Obtiene las preguntas con base a la Clave de Cuestionario
	 * @param claveCuestionario Clave del cuestionario al que pertenecen las preguntas
	 * @param psession Objeto de sesion de Agave
	 * @return List<PreguntaDTO> Preguntas asignadas al cuestionario
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public List<PreguntaDTO> obtenerPreguntasCuestionarioIp(int claveCuestionario, ArchitechSessionBean psession) throws BusinessException;
	
	/**
	 * Almacena las respuestas de un Cuestionario IP
	 * @param cuestionario Cuestionario IP al que se le van a guardar las respuestas
	 * @param respuestas Respuestas que se desean almacenar
	 * @param sesion Objeto de sesion de Agave
	 * @throws BusinessException En caso de un error con la informacion o durante la operacion
	 */
	public abstract void guardarRespuestasCuestionarioIp(CuestionarioDTO cuestionario, List<CuestionarioRespuestaDTO> respuestas, ArchitechSessionBean sesion)
	throws BusinessException;
}
