/**
 * Isban Mexico
 *   Clase: ProcesadorCalificacionBO.java
 *   Descripcion: Interfaz para el BO ProcesadorCalificacionBO
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.bo;

import java.net.MalformedURLException;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 11/11/2014
 *
 */
@Remote
public interface ProcesadorCalificacionBO {
	 /**
	  * Solicita la calificacion de un cliente a Norkom
	 * @param buc Codigo del Cliente
	 * @param cuestionario Cuestionario que esta llenando el cliente
	 * @param sesion Objeto de sesion de Agave
	 * @return CuestionarioDTO Cuestionario actualizado
	 * @throws BusinessException En caso de un error con la informacion o durante la actualizacion
	 */
	public CuestionarioDTO calificarCliente(String buc, CuestionarioDTO cuestionario, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * metodo para la obtencion de la calificacion del custionario
	 * @param cuestionario datos del cuestionario
	 * @param messageData mensaje a enviar al WS
	 * @throws MalformedURLException con mensaje de error
	 * @throws BusinessException con mensaje de error
	 */
	public void obtenerCalificacion(CuestionarioDTO cuestionario, String messageData) throws MalformedURLException, BusinessException;
	
	/**
	 * Realiza la actualizacion del Nivel de Riesgo y el Indicador UPLD en la BD del ACC
	 * @param claveClienteCuest Clave de Cliente - Cuestionario a la que se le va a realizar la actualizacion
	 * @param nivelRiesgo Nuevo nivel de riesgo
	 * @param indicadorUpld Nuevo indicador UPLD
	 * @param psession Objeto de sesion de Agave
	 * @throws BusinessException En caso de un error con la informacion o durante la actualizacion
	 */
	public void guardarCalificacionNorkomEnACC(int claveClienteCuest, String nivelRiesgo, String indicadorUpld, ArchitechSessionBean psession) throws BusinessException;
}
