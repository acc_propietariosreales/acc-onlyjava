package mx.isban.norkom.cuestionario.bo;
import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.bean.RequestObtenerCuestionario;
import mx.isban.norkom.cuestionario.dto.NivelRiesgoDTO;

@Remote
public interface RegimenSimplificadoBO{
	
	/**
	 * Define si un cliente es del tipo regimen simplificado
	 * @param peticion Contiene la informacion que se va a validar
	 * @param sesion Objeto de sesion de Agave
	 * @return nivelRiesgoDTO con datos de la validacion de regimen 
	 * @throws BusinessException En caso de error en la informacion o en las consultas de BD
	 */
	public NivelRiesgoDTO esRegimenSimplificado(RequestObtenerCuestionario peticion, ArchitechSessionBean sesion) throws BusinessException;

}
