/**
 * Isban Mexico
 *   Clase: RelacionadosBloqueBO.java
 *   Descripcion: Interfaz para el BO RelacionadosBloqueBO
 *   
 *   Control de Cambios:
 *   1.0 Jun 14, 2017 LFER - Creacion
 */


package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.ws.dto.RelacionadoDTO;
import mx.isban.norkom.ws.dto.RelacionadosRequestDTO;
/**
 * interfaz de 
 * RelacionadosBloqueBO
 * 
 * @author lespinosa
 *
 */
@Remote
public interface RelacionadosBloqueBO {

	/**
	 * Agrega relacionados a un cliente
	 * @param relacionadosWS Relacionados que se desean gregar
	 * @param psession Objeto de sesion de Agave
	 * @return List<RelacionadoDTO> Relacionados que no fue posible agregar
	 * @throws BusinessException En caso de error con la informacion
	 */
	List<RelacionadoDTO> agregarRelacionados(RelacionadosRequestDTO relacionadosWS, ArchitechSessionBean psession) 
	throws BusinessException;

}
