/**
 * Isban Mexico
 *   Clase: ValidadorContratacionMismoDiaBO.java
 *   Descripcion: Interfaz para el BO  de validacion de contratacion del mismo dia
 *   
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.bo;

import java.util.List;

import javax.ejb.Remote;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.CuestionarioDTO;

/**
 * @author jgarcia
 *
 */
@Remote
public interface ValidadorContratacionMismoDiaBO {
	/**
	 * Obtiene la lista de los cuestionarios contratados el mismo dia
	 * @param buc Cliente al que pertenecen los productos
	 * @param nivelRiesgo Nivel riesgo que deben tener los productos
	 * @param idCuestionario Identificador del Cuestionario del que deben ser diferentes los productos
	 * @param psession Objeto de sesion de Agave
	 * @return Productos contratados el mismo dia
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public List<CuestionarioDTO> productosContratadosMismoDia(String buc, String nivelRiesgo, String idCuestionario, ArchitechSessionBean psession)
	throws BusinessException;
}
