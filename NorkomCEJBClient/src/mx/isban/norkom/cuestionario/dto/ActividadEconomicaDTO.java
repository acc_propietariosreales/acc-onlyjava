/**
 * Isban Mexico
 *   Clase: ActividadEconomicaDTO.java
 *   Descripcion: DTO para las actividades economicas de los socios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class ActividadEconomicaDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4017228962025826516L;
	/**
	 * idActividad Economica
	 */
	private int idActividad;
	/**
	 * codigo de  Economica
	 */
	private String codigo;
	/**
	 * descripcion de  Economica
	 */
	private String descripcion;
	/**
	 * tipoActividad de  Economica
	 */
	private String tipoActividad;
	/**
	 * entidadFinanciera de  Economica
	 */
	private String entidadFinanciera;
	/**
	 * tipoSector de  Economica
	 */
	private String tipoSector;
	
	/**
	 * @return Obtiene el id de la actividad economica
	 */
	public int getIdActividad() {
		return idActividad;
	}
	/**
	 * Define el id de la actividad economica a agregar
	 * @param idActividad ID
	 */
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	/**
	 * @return Obtiene el codigo de la actividad economica
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * Define el codigo de la actividad economica a agregar
	 * @param codigo String
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return Obtiene la descripcion de la actividad economica
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * Define la descripcion de la actividad economica a agregar
	 * @param descripcion String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return Obtiene el tipo de la actividad economica
	 */
	public String getTipoActividad() {
		return tipoActividad;
	}
	/**
	 * Define el tipo de la actividad economica a agregar
	 * @param tipoActividad String
	 */
	public void setTipoActividad(String tipoActividad) {
		this.tipoActividad = tipoActividad;
	}
	/**
	 * @return Obtiene la entidad financiera
	 */
	public String getEntidadFinanciera() {
		return entidadFinanciera;
	}
	/**
	 * Define la entidad financiera a agregar
	 * @param entidadFinanciera String
	 */
	public void setEntidadFinanciera(String entidadFinanciera) {
		this.entidadFinanciera = entidadFinanciera;
	}
	/**
	 * @return Obtiene el sector
	 */
	public String getTipoSector() {
		return tipoSector;
	}
	/**
	 * Define el sector a agregar
	 * @param tipoSector String
	 */
	public void setTipoSector(String tipoSector) {
		this.tipoSector = tipoSector;
	}
}
