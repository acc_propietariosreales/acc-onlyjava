/**
 * Isban Mexico
 *   Clase: AtributosTagHtmlDTO.java
 *   Descripcion: Componente que almacena atributos de html
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class AtributosTagHtmlDTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6587937410694780670L;
	/**
	 * habilitado
	 */
	private boolean habilitado;
	/**
	 * soloLectura
	 */
	private boolean soloLectura;
	/**
	 * obligatorio
	 */
	private boolean obligatorio;
	/**
	 * numerico
	 */
	private boolean numerico;
	/**
	 * respuestaMultiple
	 */
	private boolean respuestaMultiple;
	/**
	 * maximaLongitud
	 */
	private int maximaLongitud;
	
	/**
	 * Clases de CSS que aplican
	 */
	private String classCss;
	
	/**
	 * Constructor en donde se definen las siguientes valores como predeterminados:
	 * habilitado = false
	 * habilitado = true
	 * soloLectura = false
	 * obligatorio = true
	 * numerico = false
	 * maximaLongitud = 0 para indicar que no se tome en cuenta el valor, cualquier valor distinto debera ser considerado
	 * Si se desea cambiar algun valor entonces es necesario llamar al metodo 'set' correspondiente.
	 */
	public AtributosTagHtmlDTO(){
		habilitado = true;
		soloLectura = false;
		obligatorio = true;
		numerico = false;
		respuestaMultiple = false;
		maximaLongitud = 0;
	}
	
	/**
	 * @return Obtiene si esta habilitado
	 */
	public boolean isHabilitado() {
		return habilitado;
	}
	/**
	 * Define el valor de si esta habilitado el campo
	 * @param habilitado boolean
	 */
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	/**
	 * @return Obtiene si esta habilitado en HTML
	 */
	public String getHabilitadoHtml(){
		String codigoHtml = "";
		
		if(!habilitado){
			codigoHtml = " disabled='disabled' ";
		}
		
		return codigoHtml;
	}
	
	/**
	 * @return Obtiene si es solo de lectura el campo
	 */
	public boolean isSoloLectura() {
		return soloLectura;
	}
	/**
	 * Define el valor de solo de lectura
	 * @param soloLectura boolean
	 */
	public void setSoloLectura(boolean soloLectura) {
		this.soloLectura = soloLectura;
	}
	/**
	 * @return Obtiene el valor de si solo de lectura HTML
	 */
	public String getSoloLecturaHtml(){
		String codigoHtml = "";
		
		if(soloLectura){
			codigoHtml = " readOnly='readOnly' ";
		}
		
		return codigoHtml;
	}
	
	/**
	 * @return Obtiene si es obligatorio el campo
	 */
	public boolean isObligatorio() {
		return obligatorio;
	}
	/**
	 * Define el valor de si es obligatorio el campo
	 * @param obligatorio boolean
	 */
	public void setObligatorio(boolean obligatorio) {
		this.obligatorio = obligatorio;
	}
	/**
	 * @return Obtiene si es numerico el campo
	 */
	public boolean isNumerico() {
		return numerico;
	}
	/**
	 * Define el valor de si es numerico el campo
	 * @param numerico boolean
	 */
	public void setNumerico(boolean numerico) {
		this.numerico = numerico;
	}

	/**
	 * @return Obtiene si es de respuesta multiple el campo
	 */
	public boolean isRespuestaMultiple() {
		return respuestaMultiple;
	}
	/**
	 * Define el valor de si es de respuesta multiple el campo
	 * @param respuestaMultiple boolean
	 */
	public void setRespuestaMultiple(boolean respuestaMultiple) {
		this.respuestaMultiple = respuestaMultiple;
	}
	/**
	 * @return Obtiene si es de respuesta multiple el campo HTML
	 */
	public String getRespuestaMultipleHtml() {
		String codigoHtml = "";
		
		if(respuestaMultiple){
			codigoHtml = " multiple ";
		}
		
		return codigoHtml;
	}

	/**
	 * @return Obtiene la longitud maxima del campo
	 */
	public int getMaximaLongitud() {
		return maximaLongitud;
	}

	/**
	 * Define la longitud maxima del campo a agregar
	 * @param maximaLongitud int
	 */
	public void setMaximaLongitud(int maximaLongitud) {
		this.maximaLongitud = maximaLongitud;
	}

	/**
	 * Obtiene el valor de classCss
	 * @return El valor de classCss
	 */
	public String getClassCss() {
		if(classCss == null) {
			return StringUtils.EMPTY;
		}
		
		return classCss;
	}

	/**
	 * Define el nuevo valor para classCss
	 * @param classCss El nuevo valor de classCss
	 */
	public void setClassCss(String classCss) {
		this.classCss = classCss;
	}
}
