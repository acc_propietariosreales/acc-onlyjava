/**
 * Isban Mexico
 *   Clase: CalificacionDTO.java
 *   Descripcion:DTO para almacenar calificacion de cuestionarios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author Leopoldo F Espinosa R
 * 
 *
 */
public class CalificacionDTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4510456824077310390L;
	/**
	 * String codigoIndicadorPersonas
	 */
	String codigoIndicadorPersonas;
	/**
	 * String valorIndicadorPersonas
	 */
	String valorIndicadorPersonas;
	/**
	 * String valorIndicadorNorkom
	 */
	String valorIndicadorNorkom;
	/**
	 * String descripcionNorkom
	 */
	String descripcionNorkom;
	
	/**
	 * @return Obtiene el codigo del indicador de personas
	 */
	public String getCodigoIndicadorPersonas() {
		return codigoIndicadorPersonas;
	}
	/**
	 * Define el codigo del indicador de personas a agregar
	 * @param codigoIndicadorPersonas void
	 */
	public void setCodigoIndicadorPersonas(String codigoIndicadorPersonas) {
		this.codigoIndicadorPersonas = codigoIndicadorPersonas;
	}
	/**
	 * @return Obtiene el valor del indicador de personas
	 */
	public String getValorIndicadorPersonas() {
		return valorIndicadorPersonas;
	}
	/**
	 * Define el valor del indicador de personas a agregar
	 * @param valorIndicadorPersonas void
	 */
	public void setValorIndicadorPersonas(String valorIndicadorPersonas) {
		this.valorIndicadorPersonas = valorIndicadorPersonas;
	}
	/**
	 * @return Obtiene el valor del indicador de Norkom
	 */
	public String getValorIndicadorNorkom() {
		return valorIndicadorNorkom;
	}
	/**
	 * Define el valor del indicador de Norkom a agregar
	 * @param valorIndicadorNorkom void
	 */
	public void setValorIndicadorNorkom(String valorIndicadorNorkom) {
		this.valorIndicadorNorkom = valorIndicadorNorkom;
	}
	/**
	 * @return Obtiene la descripcion de Norkom
	 */
	public String getDescripcionNorkom() {
		return descripcionNorkom;
	}
	/**
	 * Define la descripcion de Norkom a agregar
	 * @param descripcionNorkom void
	 */
	public void setDescripcionNorkom(String descripcionNorkom) {
		this.descripcionNorkom = descripcionNorkom;
	}
}
