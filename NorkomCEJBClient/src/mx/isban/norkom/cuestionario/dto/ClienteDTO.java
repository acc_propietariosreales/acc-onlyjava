/**
 * Isban Mexico
 *   Clase: ClienteDTO.java
 *   Descripcion:DTO para almacenar datos del cliente
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author Leopoldo F Espinosa R
 * 
 *
 */
public class ClienteDTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6266786939328623736L;
	
	/**
	 * idCliente
	 */
	private int idCliente;
	/**
	 * codigoCliente
	 */
	private String codigoCliente;
	/** Indicador para saber si es un cliente nuevo o ya existente (N = Nuevo, E = Existente) */
	private String indClienteNuevo;
	/**
	 * nombreCliente
	 */
	private String nombreCliente;
	/**
	 * apellidoPaterno
	 */
	private String apellidoPaterno;
	/**
	 * apellidoMaterno
	 */
	private String apellidoMaterno;
	/**
	 * fechaNacimiento
	 */
	private String fechaNacimiento;
	/**
	 * nivelRiesgo
	 */
	private String nivelRiesgo;
	/**
	 * indicadorUpld
	 */
	private String indicadorUpld;
	
	/**
	 * @return Obtiene el id del cliente
	 */
	public int getIdCliente() {
		return idCliente;
	}
	/**
	 * Define el id del cliente a agregar
	 * @param idCliente int
	 */
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return Obtiene el codigo del cliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}
	/**
	 * Define el codigo del cliente a agregar
	 * @param codigoCliente string
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	/**
	 * @return Obtiene el indicador de cliente nuevo
	 */
	public String getIndClienteNuevo() {
		return indClienteNuevo;
	}
	/**
	 * Define el indicador de cliente nuevo a agregar
	 * @param indClienteNuevo string
	 */
	public void setIndClienteNuevo(String indClienteNuevo) {
		this.indClienteNuevo = indClienteNuevo;
	}
	/**
	 * @return Obtiene el nombre del cliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * Define el nombre del cliente a agregar
	 * @param nombreCliente string
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return Obtiene el apellido paterno del cliente
	 */
	public String getApellidoPaterno() {
		if(apellidoPaterno==null){
			apellidoPaterno="";
		}
		return apellidoPaterno;
	}
	/**
	 * Define el apellido paterno del cliente a agregar
	 * @param apellidoPaterno string
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * @return Obtiene el apellido materno del cliente
	 */
	public String getApellidoMaterno() {
		if(apellidoMaterno==null){
			apellidoMaterno="";
		}
		return apellidoMaterno;
	}
	/**
	 * Define el apellido materno del cliente a agregar
	 * @param apellidoMaterno string
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * @return Obtiene fecha de nacimiento del cliente
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * Define fecha de nacimiento del cliente a agregar
	 * @param fechaNacimiento string
	 */
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * @return Obtiene el nombre completo del cliente
	 */
	public String getNombreCompleto(){
		StringBuffer nombreCompleto = new StringBuffer(nombreCliente!=null?nombreCliente:" ");
		
		if(apellidoPaterno != null && !apellidoPaterno.isEmpty()){
			nombreCompleto.append(" ").append(apellidoPaterno);
		}
		
		if(apellidoMaterno != null && !apellidoMaterno.isEmpty()){
			nombreCompleto.append(" ").append(apellidoMaterno);
		}
		
		return nombreCompleto.toString();
	}
	/**
	 * @return Obtiene el nivel de riesgo
	 */
	public String getNivelRiesgo() {
		return nivelRiesgo;
	}
	/**
	 * Define el nivel de riesgo a agregar
	 * @param nivelRiesgo string
	 */
	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}
	/**
	 * @return Obtiene el indicador del cliente
	 */
	public String getIndicadorUpld() {
		return indicadorUpld;
	}
	/**
	 * Define el indicador del cliente a agregar
	 * @param indicadorUpld string
	 */
	public void setIndicadorUpld(String indicadorUpld) {
		this.indicadorUpld = indicadorUpld;
	}
}
