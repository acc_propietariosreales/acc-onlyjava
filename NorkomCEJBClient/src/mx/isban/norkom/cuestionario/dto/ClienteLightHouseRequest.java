/**
 * 
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 *
 */
public class ClienteLightHouseRequest extends UbicacionLightHouseRequest implements Serializable {
	/**
	 * UID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = 6755073922670479211L;
	/** Tipo de persona F - Fisica / J - Moral*/
	private String tipoPersona;
	/** Subtipo de persona S - Con actividad empresarial / N - Sin actividad empresarial*/
	private String subtipoPersona;
	/** Codigo del cliente */
	private String codigoCliente;
	/** Indicador de cliente nuevo */
	private String indClienteNuevo;
	/** Nombre del cliente */
	private String nombreCliente;
	/** Apellido paterno del cliente en caso de aplicar */
	private String apellidoPaterno;
	/** Apellido materno del cliente en caso de aplicar */
	private String apellidoMaterno;
	/** Fecha de nacimiento del cliente */
	private String fechaNacimiento;
	
	/**
	 * Obtiene el valor de tipoPersona
	 * @return El valor de tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * Define el nuevo valor para tipoPersona
	 * @param tipoPersona El nuevo valor de tipoPersona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * Obtiene el valor de subtipoPersona
	 * @return El valor de subtipoPersona
	 */
	public String getSubtipoPersona() {
		return subtipoPersona;
	}
	/**
	 * Define el nuevo valor para subtipoPersona
	 * @param subtipoPersona El nuevo valor de subtipoPersona
	 */
	public void setSubtipoPersona(String subtipoPersona) {
		this.subtipoPersona = subtipoPersona;
	}
	/**
	 * Obtiene el valor de codigoCliente
	 * @return El valor de codigoCliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}
	/**
	 * Define el nuevo valor para codigoCliente
	 * @param codigoCliente El nuevo valor de codigoCliente
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	/**
	 * Obtiene el valor de indClienteNuevo
	 * @return El valor de indClienteNuevo
	 */
	public String getIndClienteNuevo() {
		return indClienteNuevo;
	}
	/**
	 * Define el nuevo valor para indClienteNuevo
	 * @param indClienteNuevo El nuevo valor de indClienteNuevo
	 */
	public void setIndClienteNuevo(String indClienteNuevo) {
		this.indClienteNuevo = indClienteNuevo;
	}
	/**
	 * Obtiene el valor de nombreCliente
	 * @return El valor de nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * Define el nuevo valor para nombreCliente
	 * @param nombreCliente El nuevo valor de nombreCliente
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * Obtiene el valor de apellidoPaterno
	 * @return El valor de apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * Define el nuevo valor para apellidoPaterno
	 * @param apellidoPaterno El nuevo valor de apellidoPaterno
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * Obtiene el valor de apellidoMaterno
	 * @return El valor de apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * Define el nuevo valor para apellidoMaterno
	 * @param apellidoMaterno El nuevo valor de apellidoMaterno
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * Obtiene el valor de fechaNacimiento
	 * @return El valor de fechaNacimiento
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * Define el nuevo valor para fechaNacimiento
	 * @param fechaNacimiento El nuevo valor de fechaNacimiento
	 */
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
}
