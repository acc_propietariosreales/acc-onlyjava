/**
 * Isban Mexico
 *   Clase: CuestionarioDTO.java
 *   Descripcion: DTO para almacenar datos de Cuestionarios encabezados y preguntas.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Leopoldo F Espinosa R
 * 
 *
 */
public class CuestionarioDTO extends CuestionarioDTOHelper {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1894481478601726235L;	
	/** Dia de creacion del cuestionario * */
	private String dia;
	/** Mes de creacion del cuestionario * */
	private String mes;
	/** Anio de creacion del cuestionario * */
	private String anio;
	/** Clave cliente cuestionario * */
	private int claveClienteCuestionario;
	/** Clave del cuestionario * */
	private int claveCuestionario;
	/**id del cuestionario**/
	private String idCuestionario;
	/** Id del cuestionario * */
	private ClienteDTO cliente;
	/**tipo de Persona**/
	private String tipoPersona;
	/**subtipo de persona**/
	private String subtipoPersona;
	/**divisa MXP o USD**/
	private String divisa;
	/** Codigo de sucursal * */
	private String codigoSucursal;
	/** Sucursal * */
	private String nombreSucursal;
	/***
	 * indicador de relacionados
	 */
	private boolean relacionadosOk;

	/**Lista de preguntas**/
	private List<PreguntaDTO> preguntas;
	/**Preguntas por seccion**/
	private Map<String, List<PreguntaDTO>> preguntasPorSeccion;
	
	/**
	 * Familiares del Cliente
	 */
	private List<FamiliarDTO> familiares;
	
	/**
	 * @return Obtiene la clave del cuestionario del cliente
	 */
	public Integer getClaveClienteCuestionario() {
		return claveClienteCuestionario;
	}
	/**
	 * Define la clave del cuestionario del cliente a agregar
	 * @param claveClienteCuestionario integer
	 */
	public void setClaveClienteCuestionario(Integer claveClienteCuestionario) {
		this.claveClienteCuestionario = claveClienteCuestionario;
	}
	/**
	 * @return Obtiene la clave del cuestionario
	 */
	public Integer getClaveCuestionario() {
		return claveCuestionario;
	}
	/**
	 * Define la clave del cuestionario a agregar
	 * @param claveCuestionario integer
	 */
	public void setClaveCuestionario(Integer claveCuestionario) {
		this.claveCuestionario = claveCuestionario;
	}
	/**
	 * @return Obtiene el id de Cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * Define el id de Cuestionario a agregar
	 * @param idCuestionario 
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}
	/**
	 * @return Obtiene el tipo de Persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * Define el tipoPersona a agregar
	 * @param tipoPersona 
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return Obtiene el subtipo de Persona
	 */
	public String getSubtipoPersona() {
		return subtipoPersona;
	}
	/**
	 * Define el subtipoPersona a agregar
	 * @param subtipoPersona 
	 */
	public void setSubtipoPersona(String subtipoPersona) {
		this.subtipoPersona = subtipoPersona;
	}
	/**
	 * @return Obtiene la divisa
	 */
	public String getDivisa() {
		return divisa;
	}
	/**
	 * Define el valor de la divisa a agregar
	 * @param divisa 
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * @return Obtiene las preguntas
	 */
	public List<PreguntaDTO> getPreguntas() {
		return preguntas;
	}
	/**
	 * Define las preguntas a agregar
	 * @param preguntas 
	 */
	public void setPreguntas(List<PreguntaDTO> preguntas) {
		this.preguntas = preguntas;
	}
	/**
	 * @return Obtiene las preguntas por seccion
	 */
	public Map<String, List<PreguntaDTO>> getPreguntasPorSeccion() {
		return preguntasPorSeccion;
	}
	/**
	 * Define las preguntas por seccion a agregar
	 * @param preguntasPorSeccion map
	 */
	public void setPreguntasPorSeccion(
			Map<String, List<PreguntaDTO>> preguntasPorSeccion) {
		this.preguntasPorSeccion = preguntasPorSeccion;
	}
	
	/**
	 * @return Obtiene el cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}
	/**
	 * Define el cliente a agregar
	 * @param cliente dto
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	/**
	 * @return Obtiene el id del cliente asignado
	 */
	public int getIdClienteAsignado(){
		if(cliente == null){
			return 0;
		}
		
		return cliente.getIdCliente();
	}
	/**
	 * @return Obtiene el nombre del cliente
	 */
	public String getNombreCompletoCliente(){
		if(cliente == null){
			return "No hay datos";
		}
		
		return cliente.getNombreCompleto();
	}
	/**
	 * @return Obtiene el codigo del cliente
	 */
	public String getCodigoCliente(){
		if(cliente == null){
			return "No hay datos";
		}
		
		return cliente.getCodigoCliente();
	}
	
	/**
	 * @return Obtiene el codigo de sucursal
	 */
	public String getCodigoSucursal() {
		return codigoSucursal;
	}
	/**
	 * Define el codigo de sucursal a agregar
	 * @param codigoSucursal string
	 */
	public void setCodigoSucursal(String codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}
	/**
	 * @return Obtiene el nombre de sucursal
	 */
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	/**
	 * Define el nombre de sucursal a agregar
	 * @param nombreSucursal string
	 */
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	
	/**
	 * @return Obtiene el dia
	 */
	public String getDia() {
		return dia;
	}
	/**
	 * Define el dia a agregar
	 * @param dia string
	 */
	public void setDia(String dia) {
		this.dia = dia;
	}
	/**
	 * @return Obtiene el mes
	 */
	public String getMes() {
		return mes;
	}
	/**
	 * Define el mes a agregar
	 * @param mes string
	 */
	public void setMes(String mes) {
		this.mes = mes;
	}

	/**
	 * @return Obtiene the year
	 */
	public String getAnio() {
		return anio;
	}
	/**
	 * Define the year a agregar
	 * @param anio void
	 */
	public void setAnio(String anio) {
		this.anio = anio;
	}
	
	/**
	 * @return Obtiene la lista de los familiares
	 */
	public List<FamiliarDTO> getFamiliares() {
		return familiares;
	}
	/**
	 * Define la lista de los familiares a agregar
	 * @param familiares void
	 */
	public void setFamiliares(List<FamiliarDTO> familiares) {
		this.familiares = familiares;
	}
	/**
	 * Agrega un familiar
	 * @param familiar void
	 */
	public void addFamiliar(FamiliarDTO familiar) {
		if(this.familiares == null) {
			this.familiares = new ArrayList<FamiliarDTO>();
		}
		
		this.familiares.add(familiar);
	}
	/**
	 * Obtiene el valor de relacionadosOk
	 * @return El valor de relacionadosOk
	 */
	public boolean isRelacionadosOk() {
		return relacionadosOk;
	}
	/**
	 * Define el nuevo valor para relacionadosOk
	 * @param relacionadosOk El nuevo valor de relacionadosOk
	 */
	public void setRelacionadosOk(boolean relacionadosOk) {
		this.relacionadosOk = relacionadosOk;
	}
}
