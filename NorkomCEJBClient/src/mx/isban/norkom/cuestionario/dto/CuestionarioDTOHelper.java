/**
 * NorkomCEJBClient CuestionarioDTOHelper.java
 */
package mx.isban.norkom.cuestionario.dto;

import org.apache.commons.lang.StringUtils;

/**
 * @author Leopoldo F Espinosa R 27/02/2015
 *
 */
public class CuestionarioDTOHelper extends CuestionarioDTOHelper2{
	/**
	 * CuestionarioDTOHelper.java  de tipo long
	 */
	private static final long serialVersionUID = 1L;
	/** Codigo de la region * */
	private String codigoRegion;
	/** Nombre de la region * */
	private String nombreRegion;
	/** Codigo de la plaza* */
	private String codigoPlaza;
	/** Nombre de la plaza * */
	private String nombrePlaza;
	/** Codigo de estado * */
	private String codigoEntidad;
	/** Nombre de estado * */
	private String nombreEntidad;
	/** Codigo de segmento * */
	private String codigoSegmento;
	/** Descripcion de segmento * */
	private String descSegmento;
	/** Actividad generica * */
	private ActividadEconomicaDTO actividadGenerica;
	/** Actividad especifica * */
	private ActividadEconomicaDTO actividadEspecifica;
	/** Origen y destino de los recursos * */
	private RecursoDTO recursoDTO;
	/** Codigo de producto * */
	private String codigoProducto;
	/** Descripcion de producto * */
	private String descProducto;
	/** Codigo de sub producto * */
	private String codigoSubproducto;
	/** Descripcion de sub producto * */
	private String descSubproducto; /*FALTA EN EL ARCHIVO DE ENTRADA*/
	/**codigo de pais de residencia**/
	private String codigoPaisResidencia;
	/**nombre de pais de residencia**/
	private String nombrePaisResidencia;
	/**
	 * @return Obtiene el codigo de region
	 */
	public String getCodigoRegion() {
		return codigoRegion;
	}
	/**
	 * Define Obtiene el codigo de region a agregar
	 * @param codigoRegion string
	 */
	public void setCodigoRegion(String codigoRegion) {
		this.codigoRegion = codigoRegion;
	}
	/**
	 * @return Obtiene el nombre de la region
	 */
	public String getNombreRegion() {
		return nombreRegion;
	}
	/**
	 * Define el nombre de la region a agregar
	 * @param nombreRegion string
	 */
	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}
	/**
	 * @return Obtiene el codigo de la plaza
	 */
	public String getCodigoPlaza() {
		return codigoPlaza;
	}
	/**
	 * Define el codigo de la plaza a agregar
	 * @param codigoPlaza string
	 */
	public void setCodigoPlaza(String codigoPlaza) {
		this.codigoPlaza = codigoPlaza;
	}
	/**
	 * @return Obtiene el nombre de la plaza
	 */
	public String getNombrePlaza() {
		return nombrePlaza;
	}
	/**
	 * Define el nombre de la plaza a agregar
	 * @param nombrePlaza string
	 */
	public void setNombrePlaza(String nombrePlaza) {
		this.nombrePlaza = nombrePlaza;
	}
	/**
	 * @return Obtiene el codigo de la entidad
	 */
	public String getCodigoEntidad() {
		return codigoEntidad;
	}
	/**
	 * Define el codigo de la entidad a agregar
	 * @param codigoEntidad string
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	/**
	 * @return Obtiene el nombre de la entidad
	 */
	public String getNombreEntidad() {
		return nombreEntidad;
	}
	/**
	 * Define el nombre de la entidad a agregar
	 * @param nombreEntidad string
	 */
	public void setNombreEntidad(String nombreEntidad) {
		this.nombreEntidad = nombreEntidad;
	}
	/**
	 * @return Obtiene el codigo del segmento
	 */
	public String getCodigoSegmento() {
		return codigoSegmento;
	}
	/**
	 * Define el codigo del segmento a agregar
	 * @param codigoSegmento string 
	 */
	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}
	/**
	 * @return Obtiene la descripcion del segmento
	 */
	public String getDescSegmento() {
		return descSegmento;
	}
	/**
	 * Define la descripcion del segmento a agregar
	 * @param descSegmento string
	 */
	public void setDescSegmento(String descSegmento) {
		this.descSegmento = descSegmento;
	}
	/**
	 * @return Obtiene el codigo del producto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * Define el codigo del producto a agregar
	 * @param codigoProducto string
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return Obtiene la descripcion de producto
	 */
	public String getDescProducto() {
		return descProducto;
	}
	/**
	 * Define la descripcion de producto a agregar
	 * @param descProducto string
	 */
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	/**
	 * @return Obtiene el codigo de subproducto
	 */
	public String getCodigoSubproducto() {
		return codigoSubproducto;
	}
	/**
	 * Define el codigo de subproducto a agregar
	 * @param codigoSubproducto string
	 */
	public void setCodigoSubproducto(String codigoSubproducto) {
		this.codigoSubproducto = codigoSubproducto;
	}
	/**
	 * @return Obtiene la descripcion del subprodcuto
	 */
	public String getDescSubproducto() {
		return descSubproducto;
	}
	/**
	 * Define la descripcion del subprodcuto a agregar
	 * @param descSubproducto string
	 */
	public void setDescSubproducto(String descSubproducto) {
		this.descSubproducto = descSubproducto;
	}
	
	/**
	 * @return Obtiene la actividad generica
	 */
	public ActividadEconomicaDTO getActividadGenerica() {
		return actividadGenerica;
	}
	/**
	 * Define la actividad generica a agregar
	 * @param actividadGenerica dto
	 */
	public void setActividadGenerica(ActividadEconomicaDTO actividadGenerica) {
		this.actividadGenerica = actividadGenerica;
	}
	/**
	 * @return Obtiene el id de la actividad generica
	 */
	public int getIdActividadGenerica() {
		if(this.actividadGenerica == null){
			return 0;
		}
		
		return this.actividadGenerica.getIdActividad();
	}
	/**
	 * Define el id de la actividad generica a agregar
	 * @return string
	 */
	public String getCodigoActGenerica(){
		if(this.actividadGenerica == null){
			return StringUtils.EMPTY;
		}
		
		return this.actividadGenerica.getCodigo();
	}
	/**
	 * @return Obtiene la descripcion de la actividad generica
	 */
	public String getDescripcionActGenerica(){
		if(this.actividadGenerica == null){
			return StringUtils.EMPTY;
		}
		
		return this.actividadGenerica.getDescripcion();
	}
	
	/**
	 * @return Obtiene la actividad especifica
	 */
	public ActividadEconomicaDTO getActividadEspecifica() {
		return actividadEspecifica;
	}
	/**
	 * Define la actividad especifica a agregar
	 * @param actividadEspecifica dto
	 */
	public void setActividadEspecifica(ActividadEconomicaDTO actividadEspecifica) {
		this.actividadEspecifica = actividadEspecifica;
	}
	/**
	 * @return Obtiene el id de la actividad especifica
	 */
	public int getIdActividadEspecifica() {
		if(this.actividadEspecifica == null){
			return 0;
		}
		
		return this.actividadEspecifica.getIdActividad();
	}
	/**
	 * @return Obtiene el codigo de la actividad especifica
	 */
	public String getCodigoActEspecifica(){
		if(this.actividadEspecifica == null){
			return StringUtils.EMPTY;
		}
		
		return this.actividadEspecifica.getCodigo();
	}
	/**
	 * @return Obtiene la decripcion de la actividad especifica
	 */
	public String getDescripcionActEspecifica(){
		if(this.actividadEspecifica == null){
			return StringUtils.EMPTY;
		}
		
		return this.actividadEspecifica.getDescripcion();
	}
	
	/**
	 * @return Obtiene la entidad financiera
	 */
	public String getEntidadFinancieraActEspecifica(){
		if(this.actividadEspecifica == null){
			return StringUtils.EMPTY;
		}
		
		return this.actividadEspecifica.getEntidadFinanciera();
	}
	
	/**
	 * @return Obtiene el recurso
	 */
	public RecursoDTO getRecursoDTO() {
		return recursoDTO;
	}
	/**
	 * Define el recurso a agregar
	 * @param recursoDTO dto
	 */
	public void setRecursoDTO(RecursoDTO recursoDTO) {
		this.recursoDTO = recursoDTO;
	}
	/**
	 * @return the codigoPaisResidencia
	 */
	public String getCodigoPaisResidencia() {
		return codigoPaisResidencia;
	}
	/**
	 * @param codigoPaisResidencia 
	 * el codigoPaisResidencia a agregar
	 */
	public void setCodigoPaisResidencia(String codigoPaisResidencia) {
		this.codigoPaisResidencia = codigoPaisResidencia;
	}
	/**
	 * @return the nombrePaisResidencia
	 */
	public String getNombrePaisResidencia() {
		return nombrePaisResidencia;
	}
	/**
	 * @param nombrePaisResidencia 
	 * el nombrePaisResidencia a agregar
	 */
	public void setNombrePaisResidencia(String nombrePaisResidencia) {
		this.nombrePaisResidencia = nombrePaisResidencia;
	}

}
