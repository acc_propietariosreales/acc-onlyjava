/**
 * NorkomCEJBClient CuestionarioDTOHelper2.java
 */
package mx.isban.norkom.cuestionario.dto;


/**
 * @author Leopoldo F Espinosa R 27/02/2015
 *
 */
public class CuestionarioDTOHelper2 extends CuestionarioDTOHelper3{

	/**
	 * CuestionarioDTOHelper2.java  UID de tipo long
	 */
	private static final long serialVersionUID = 1L;
	/** Codigo de pais * */
	private String codigoPais;
	/** Nombre de pais * */
	private String nombrePais;
	/** Pais de bajo riesgo* */
	private String paisBajoRiesgo;
	/** Codigo de municipio * */
	private String codigoMunicipio;
	/** Nombre de municipio * */
	private String nombreMunicipio;
	/** Codigo de nacionalidad * */
	private String codigoNacionalidad;
	/** Nombre de nacionalidad * */
	private String nombreNacionalidad;
	/** Codigo de branch* */
	private String codigoBranch;
	/** Codigo de centro costos * */
	private String codigoCentroCostos;
	/** Tipo de formulario * */
	private String codigoTipoFormulario;
	/**Descripcion del cuestionario**/
	private String descripcion;
	/**Fecha de creacion del cuestionario**/
	private String fechaCreacion;
	/**nombre de cuestionario**/
	private String nombreCuestionario;
	/** Version de cuestionario * */
	private String versionCuestionario;
	/** Numero de contrato * */
	private String numeroContrato;
	

	/**
	 * Define la fecha a agregar
	 * @param fecha 
	 */
	public void setFechaCreacion(String fecha) {
		this.fechaCreacion = fecha;
	}
	/**
	 * @return Obtiene el nombre del cuestionario
	 */
	public String getNombreCuestionario() {
		return nombreCuestionario;
	}
	/**
	 * Define el nombre del cuestionario a agregar
	 * @param nombreCuestionario string
	 */
	public void setNombreCuestionario(String nombreCuestionario) {
		this.nombreCuestionario = nombreCuestionario;
	}
	/**
	 * @return Obtiene la fecha de creacion del cuestionario
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @return Obtiene la version del cuestionario
	 */
	public String getVersionCuestionario() {
		return versionCuestionario;
	}
	/**
	 * Define la version del cuestionario a agregar
	 * @param versionCuestionario string
	 */
	public void setVersionCuestionario(String versionCuestionario) {
		this.versionCuestionario = versionCuestionario;
	}
	/**
	 * @return Obtiene el numero de contrato del cliente
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * Define el numero de contrato del cliente a agregar
	 * @param numeroContrato string
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	
	/**
	 * Define la descripcion a agregar
	 * @param descripcion 
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return Obtiene la descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @return Obtiene el codigo del municipio
	 */
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}
	/**
	 * Define el codigo del municipio a agregar
	 * @param codigoMunicipio string
	 */
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	/**
	 * @return Obtiene el codigo de nacionalidad
	 */
	public String getCodigoNacionalidad() {
		return codigoNacionalidad;
	}
	/**
	 * Define el codigo de nacionalidad a agregar
	 * @param codigoNacionalidad string
	 */
	public void setCodigoNacionalidad(String codigoNacionalidad) {
		this.codigoNacionalidad = codigoNacionalidad;
	}
	/**
	 * @return Obtiene el codigo de Branch
	 */
	public String getCodigoBranch() {
		return codigoBranch;
	}
	/**
	 * Define el codigo de Branch a agregar
	 * @param codigoBranch string
	 */
	public void setCodigoBranch(String codigoBranch) {
		this.codigoBranch = codigoBranch;
	}
	/**
	 * @return Obtiene el codigo del centro de costos
	 */
	public String getCodigoCentroCostos() {
		return codigoCentroCostos;
	}
	/**
	 * Define el codigo del centro de costos a agregar
	 * @param codigoCentroCostos string
	 */
	public void setCodigoCentroCostos(String codigoCentroCostos) {
		this.codigoCentroCostos = codigoCentroCostos;
	}
	/**
	 * @return Obtiene el codigo de tipo de formulario
	 */
	public String getCodigoTipoFormulario() {
		return codigoTipoFormulario;
	}
	/**
	 * Define el codigo de tipo de formulario a agregar
	 * @param codigoTipoFormulario string
	 */
	public void setCodigoTipoFormulario(String codigoTipoFormulario) {
		this.codigoTipoFormulario = codigoTipoFormulario;
	}
	/**
	 * @return Obtiene el nombre del pais
	 */
	public String getNombrePais() {
		return nombrePais;
	}
	/**
	 * Define el nombre del pais a agregar
	 * @param nombrePais string
	 */
	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}
	/**
	 * @return Obtiene el pais de bajo riesgo
	 */
	public String getPaisBajoRiesgo() {
		return paisBajoRiesgo;
	}
	/**
	 * Define el pais de bajo riesgo a agregar
	 * @param paisBajoRiesgo string
	 */
	public void setPaisBajoRiesgo(String paisBajoRiesgo) {
		this.paisBajoRiesgo = paisBajoRiesgo;
	}
	/**
	 * @return Obtiene el nombre de municipio
	 */
	public String getNombreMunicipio() {
		return nombreMunicipio;
	}
	/**
	 * Define el nombre de municipio a agregar
	 * @param nombreMunicipio string
	 */
	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}
	/**
	 * @return Obtiene el nombre de nacionalidad
	 */
	public String getNombreNacionalidad() {
		return nombreNacionalidad;
	}
	/**
	 * Define el nombre de nacionalidad a agregar
	 * @param nombreNacionalidad string
	 */
	public void setNombreNacionalidad(String nombreNacionalidad) {
		this.nombreNacionalidad = nombreNacionalidad;
	}
	/**
	 * @return Obtiene el codigo de pais
	 */
	public String getCodigoPais() {
		return codigoPais;
	}
	/**
	 * Define el codigo de pais a agregar
	 * @param codigoPais string
	 */
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
}
