/**
 * NorkomCEJBClient CuestionarioDTOHelper3.java
 */
package mx.isban.norkom.cuestionario.dto;

import mx.isban.norkom.cuestionario.bean.ResponseBase;

/**
 * @author Leopoldo F Espinosa R 27/02/2015
 *
 */
public class CuestionarioDTOHelper3 extends ResponseBase{
	/**
	 * CuestionarioDTOHelper3.java  de tipo long
	 */
	private static final long serialVersionUID = -8984820177975945858L;
	/** Id aplicacion * */
	private String idAplicacion;
	/** Cuenta o contrato* */
	private String cuentaContrato;
	
	/** Define si se trata de un Familiar Politicamente Expuesto */
	private boolean familiarPep;
	
	/** Codigo de Zona */
	private String codigoZona;
	
	/** Nombre de la Zona */
	private String nombreZona;
	
	/** Nivel de Riesgo obtenidod de Norkom */
	private String nivelRiesgo;
	
	/** Indicador UPLD obtenido de Norkom */
	private String indicadorUpld;
	
	/** Bandera para indicar si la calificacion fue realizada mediante un dummy */
	private boolean dummy;
	/** peopiedad para Regimen Simplificado***/
	private boolean regSimp=false;
	/***
	 * numero de intentos	
	 * 
	 */
	private int intentos;
	
	/** Nombre del funcionario que esta realizando la contratacion */
	private String nombreFuncionario;
	
	/**
	 * @return Obtiene la cuenta y/o contrato
	 */
	public String getCuentaContrato() {
		return cuentaContrato;
	}
	/**
	 * Define la cuenta y/o contrato a agregar
	 * @param cuentaContrato string
	 */
	public void setCuentaContrato(String cuentaContrato) {
		this.cuentaContrato = cuentaContrato;
	}
	/**
	 * @return Obtiene el id de la aplicacion
	 */
	public String getIdAplicacion() {
		return idAplicacion;
	}
	/**
	 * Define el id de la aplicacion a agregar
	 * @param idAplicacion string
	 */
	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	/**
	 * @return Obtiene si el cliente tiene un familiar PEP
	 */
	public boolean hasFamiliarPep() {
		return familiarPep;
	}
	/**
	 * Define el familiar PEP del cliente a agregar
	 * @param familiarPep boolean
	 */
	public void setFamiliarPep(boolean familiarPep) {
		this.familiarPep = familiarPep;
	}
	/**
	 * @return Obtiene el codigo de zona
	 */
	public String getCodigoZona() {
		return codigoZona;
	}
	/**
	 * Define el codigo de zona a agregar
	 * @param codigoZona string
	 */
	public void setCodigoZona(String codigoZona) {
		this.codigoZona = codigoZona;
	}
	/**
	 * @return Obtiene el nombre de la zona
	 */
	public String getNombreZona() {
		return nombreZona;
	}
	/**
	 * Define el nombre de la zona a agregar
	 * @param nombreZona string
	 */
	public void setNombreZona(String nombreZona) {
		this.nombreZona = nombreZona;
	}
	/**
	 * @return Obtiene el nivel de riesgo
	 */
	public String getNivelRiesgo() {
		return nivelRiesgo;
	}
	/**
	 * Define el nivel de riesgo a agregar
	 * @param nivelRiesgo void
	 */
	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}
	/**
	 * @return Obtiene el indicador
	 */
	public String getIndicadorUpld() {
		return indicadorUpld;
	}
	/**
	 * Define el indicador a agregar
	 * @param indicadorUpld void
	 */
	public void setIndicadorUpld(String indicadorUpld) {
		this.indicadorUpld = indicadorUpld;
	}
	
	/**
	 * Define si la calificacion fue realizada por dummy
	 * @return True si se trata de un dummy, false en caso contrario
	 */
	public boolean isDummy() {
		return dummy;
	}
	
	/**
	 * Define si la calificacion fue realizada por dummy
	 * @param esDummy El nuevo valor para el campo de calificacion dummy
	 */
	public void setDummy(boolean esDummy) {
		this.dummy = esDummy;
	}
	/**
	 * @return the regSimp
	 */
	public boolean isRegSimp() {
		return regSimp;
	}
	/**
	 * @param regSimp 
	 * el regSimp a agregar
	 */
	public void setRegSimp(boolean regSimp) {
		this.regSimp = regSimp;
	}
	/**
	 * @return the familiarPep
	 */
	public boolean isFamiliarPep() {
		return familiarPep;
	}
	/**
	 * Obtiene el valor de nombreFuncionario
	 * @return El valor de nombreFuncionario
	 */
	public String getNombreFuncionario() {
		return nombreFuncionario;
	}
	/**
	 * Define el nuevo valor para nombreFuncionario
	 * @param nombreFuncionario El nuevo valor de nombreFuncionario
	 */
	public void setNombreFuncionario(String nombreFuncionario) {
		this.nombreFuncionario = nombreFuncionario;
	}
	/**
	 * Obtiene el valor de intentos
	 * @return El valor de intentos
	 */
	public int getIntentos() {
		return intentos;
	}
	/**
	 * Define el nuevo valor para intentos
	 * @param intentos El nuevo valor de intentos
	 */
	public void setIntentos(int intentos) {
		this.intentos = intentos;
	}
}
