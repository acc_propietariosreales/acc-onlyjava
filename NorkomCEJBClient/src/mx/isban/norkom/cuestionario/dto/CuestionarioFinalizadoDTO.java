package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

import mx.isban.norkom.ws.dto.RespuestaWebService;

public class CuestionarioFinalizadoDTO extends RespuestaWebService implements
		Serializable {

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -340233441375637977L;
	
	/**
	 * Variables utilizada para almacenar el IND_PLD
	 */
	private String indPld;

	/**
	 * Variables utilizada para almacenar el NIVEL_RGO
	 */
	private String nivelRgo;
	
	/**
	 * Variables utilizada para almacenar el IND_NOR
	 */
	private String indNor;

	/**
	 * Variables utilizada para almacenar el IND_UPLD
	 */
	private String indUpld;

	/**
	 * @return the indPld
	 */
	public String getIndPld() {
		return indPld;
	}

	/**
	 * @param indPld 
	 * el indPld a agregar
	 */
	public void setIndPld(String indPld) {
		this.indPld = indPld;
	}

	/**
	 * @return the nivelRgo
	 */
	public String getNivelRgo() {
		return nivelRgo;
	}

	/**
	 * @param nivelRgo 
	 * el nivelRgo a agregar
	 */
	public void setNivelRgo(String nivelRgo) {
		this.nivelRgo = nivelRgo;
	}

	/**
	 * @return the indNor
	 */
	public String getIndNor() {
		return indNor;
	}

	/**
	 * @param indNor 
	 * el indNor a agregar
	 */
	public void setIndNor(String indNor) {
		this.indNor = indNor;
	}

	/**
	 * @return the indUpld
	 */
	public String getIndUpld() {
		return indUpld;
	}

	/**
	 * @param indUpld 
	 * el indUpld a agregar
	 */
	public void setIndUpld(String indUpld) {
		this.indUpld = indUpld;
	}

	
}
