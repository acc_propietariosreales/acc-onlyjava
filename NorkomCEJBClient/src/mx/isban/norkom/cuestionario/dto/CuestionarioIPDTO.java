/**
 * Isban Mexico
 *   Clase: CuestionarioIPDTO.java
 *   Descripcion: DTO para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class CuestionarioIPDTO extends CuestionarioIPDTOHelper implements Serializable {
	/** Variable utilizada para declarar serialVersionUID **/
	private static final long serialVersionUID = -1894481478601726235L;
	/** Variable de idFormulario **/
	private String idFormulario;
	/** Variable de descAplicativo **/
	private String descAplicativo;
	/** Variable de fechaCreacion **/
	private String fechaCreacion;
	/** Variable de valBUC **/
	private String valBUC;
	/** Variable de codigoEntidad **/
	private String codigoEntidad;
	/** Variable de nombrePersona **/
	private String nombrePersona;
	/** Variable de fechaPersona **/
	private String fechaPersona;
	/** Variable de tipoPersona **/
	private String tipoPersona;
	/** Variable de subtipoPersona **/
	private String subtipoPersona;
	/** Variable de codigoSegmento **/
	private String codigoSegmento;
	
	/** @return Obtiene el id del Formulario **/
	public String getIdFormulario() {
		return idFormulario;
	}
	/**
	 * Define el id del Formulario a agregar
	 * @param idFormulario el idFormulario a establecer
	**/
	public void setIdFormulario(String idFormulario) {
		this.idFormulario = idFormulario;
	}
	/** @return Obtiene la descripcion del Aplicativo **/
	public String getDescAplicativo() {
		return descAplicativo;
	}
	/**
	 * Define la descripcion del Aplicativo a agregar
	 * @param descAplicativo el descAplicativo a establecer
	 * **/
	public void setDescAplicativo(String descAplicativo) {
		this.descAplicativo = descAplicativo;
	}
	/** @return Obtiene la fecha de Creacion **/
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * Define la fecha de Creacion a agregar
	 * @param fechaCreacion el fechaCreacion a establecer
	 * **/
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/** @return Obtiene el valor del BUC del cliente **/
	public String getValBUC() {
		return valBUC;
	}
	/**
	 * Define el valor del BUC del cliente a agregar
	 * @param valBUC el valBUC a establecer
	 **/
	public void setValBUC(String valBUC) {
		this.valBUC = valBUC;
	}
	/** @return Obtiene el codigo de Entidad **/
	public String getCodigoEntidad() {
		return codigoEntidad;
	}
	/**
	 * Define el codigo de Entidad a agregar
	 * @param codigoEntidad el codigoEntidad a establecer
	 * **/
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	/** @return Obtiene el nombre de la Persona **/
	public String getNombrePersona() {
		return nombrePersona;
	}
	/**
	 * Define el nombre de la Persona a agregar
	 * @param nombrePersona el nombrePersona a establecer
	 **/
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	/** @return Obtiene la fecha de nacimiento de la Persona **/
	public String getFechaPersona() {
		return fechaPersona;
	}
	/** 
	 * Define la fecha de nacimiento de la Persona a agregar
	 * @param fechaPersona el fechaPersona a establecer
	 **/
	public void setFechaPersona(String fechaPersona) {
		this.fechaPersona = fechaPersona;
	}
	/** @return Obtiene el tipo de Persona **/
	public String getTipoPersona() {
		return tipoPersona;
	}
	/** 
	 * Define el tipo de Persona a agregar
	 * @param tipoPersona el tipoPersona a establecer
	 **/
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/** @return Obtiene el subtipoPersona **/
	public String getSubtipoPersona() {
		return subtipoPersona;
	}
	/** 
	 * Define el subtipoPersona a agregar
	 * @param subtipoPersona el subtipoPersona a establecer
	 **/
	public void setSubtipoPersona(String subtipoPersona) {
		this.subtipoPersona = subtipoPersona;
	}
	/** @return Obtiene el codigo de Segmento **/
	public String getCodigoSegmento() {
		return codigoSegmento;
	}
	/**
	 * Define el codigo de Segmento a agregar
	 * @param codigoSegmento el codigoSegmento a establecer
	 **/
	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}
		
}