/**
 * Isban Mexico
 *   Clase: CuestionarioIPDTO.java
 *   Descripcion: DTO para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class CuestionarioIPDTOHelper implements Serializable {
	/** Variable utilizada para declarar serialVersionUID **/
	private static final long serialVersionUID = -1894481478601726235L;
	/** Variable de codigoActividadEcon **/
	private String codigoActividadEcon;
	/** Variable de codigoProducto **/
	private String codigoProducto;
	/** Variable de codigoPais **/
	private String codigoPais;
	/** Variable de codigoMunicipio **/
	private String codigoMunicipio;
	/** Variable de codigoNacionalidad **/
	private String codigoNacionalidad;
	/** Variable de divisa **/
	private String divisa;
	/** Variable de regionSucursal **/
	private String regionSucursal;
	/** Variable de zonaSucursal **/
	private String zonaSucursal;
	/** Variable de codigoCentroCostos **/
	private String codigoCentroCostos;
	
	/** @return Obtiene el codigo de la Actividad Economica **/
	public String getCodigoActividadEcon() {
		return codigoActividadEcon;
	}
	/** 
	 * Define el codigo de la Actividad Economica a agregar
	 * @param codigoActividadEcon el codigoActividadEcon
	 **/
	public void setCodigoActividadEcon(String codigoActividadEcon) {
		this.codigoActividadEcon = codigoActividadEcon;
	}
	/** @return Obtiene el codigo del Producto **/
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/** 
	 * Define el codigo del Producto a agregar
	 * @param codigoProducto el codigoProducto a establecer
	 **/
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/** @return Obtiene el codigo del Pais **/
	public String getCodigoPais() {
		return codigoPais;
	}
	/** 
	 * Define el codigo del Pais a agregar
	 * @param codigoPais el codigoPais a establecer 
	 **/
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	/** @return Obtiene el codigo del Municipio **/
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}
	/** 
	 * Define el codigo del Municipio a agregar
	 * @param codigoMunicipio el codigoMunicipio a establecer 
	 **/
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	/** @return Obtiene el codigo de Nacionalidad **/
	public String getCodigoNacionalidad() {
		return codigoNacionalidad;
	}
	/**
	 * Define el codigo de Nacionalidad a agregar
	 * @param codigoNacionalidad el codigoNacionalidad a establecer
	 **/
	public void setCodigoNacionalidad(String codigoNacionalidad) {
		this.codigoNacionalidad = codigoNacionalidad;
	}
	/** @return Obtiene la divisa **/
	public String getDivisa() {
		return divisa;
	}
	/**
	 * Define el valor de la divisa
	 * @param divisa el divisa a establecer 
	 **/
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}
	/** @return Obtiene la region de la Sucursal **/
	public String getRegionSucursal() {
		return regionSucursal;
	}
	/**
	 * Define la region de la Sucursal a agregar
	 * @param regionSucursal el regionSucursal a establecer
	 **/
	public void setRegionSucursal(String regionSucursal) {
		this.regionSucursal = regionSucursal;
	}
	/** @return Obtiene la zona de la Sucursal **/
	public String getZonaSucursal() {
		return zonaSucursal;
	}
	/**
	 * Define la zona de la Sucursal a agregar
	 * @param zonaSucursal el zonaSucursal a establecer 
	 **/
	public void setZonaSucursal(String zonaSucursal) {
		this.zonaSucursal = zonaSucursal;
	}
	/** @return Obtiene el codigo del Centro de Costos **/
	public String getCodigoCentroCostos() {
		return codigoCentroCostos;
	}
	/**
	 * Define el codigo del Centro de Costos a agregar
	 * @param codigoCentroCostos el codigoCentroCostos a establecer
	 **/
	public void setCodigoCentroCostos(String codigoCentroCostos) {
		this.codigoCentroCostos = codigoCentroCostos;
	}
		
}