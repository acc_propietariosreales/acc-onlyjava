/**
 * 
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 * Objetivo: Guardar la informacion de la peticion de Cuestionarios por parte de LightHouse
 * Justificacion: Es necesario que la informacion se haga llegar a traves de una cadena JSON por lo cual con la conversion a un objeto java facilitara el manejo al momento de recibirla.
 */
public class CuestionarioLightHouseRequest extends ClienteLightHouseRequest implements Serializable {
	/**
	 * UID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = 6755073922670479211L;
	/** Identificador del Cuestionario */
	private String idCuestionario;
	/** Divisa */
	private String divisa;
	/** Codigo de segmento */
	private String codigoSegmento;
	/** Descripcion de segmento */
	private String descSegmento;
	/** Codigo de actividad generica */
	private String codigoActGenerica;
	/** Codigo de actividad especifica */
	private String codigoActEspecifica;
	/** Codigo de producto */
	private String codigoProducto;
	/** Descripcion de producto */
	private String descProducto;
	/** Codigo de subproducto */
	private String codigoSubproducto;
	/** Codigo de pais de nacionalidad */
	private String codigoPaisNacionalidad;
	/** Codigo de entidad */
	private String codigoEntidad;
	/** Codigo de municipio */
	private String codigoMunicipio;
	/** Codigo de pais de residencia */
	private String codigoPaisResidencia;
	/** Canal de Comercializacion */
	private String canalComercializacion;
	/** Canal de operacion */
	private String canalOperacion;
	
	/**
	 * Obtiene el valor de idCuestionario
	 * @return El valor de idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * Define el nuevo valor para idCuestionario
	 * @param idCuestionario El nuevo valor de idCuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}
	/**
	 * Obtiene el valor de divisa
	 * @return El valor de divisa
	 */
	public String getDivisa() {
		return divisa;
	}
	/**
	 * Define el nuevo valor para divisa
	 * @param divisa El nuevo valor de divisa
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}
	/**
	 * Obtiene el valor de codigoSegmento
	 * @return El valor de codigoSegmento
	 */
	public String getCodigoSegmento() {
		return codigoSegmento;
	}
	/**
	 * Define el nuevo valor para codigoSegmento
	 * @param codigoSegmento El nuevo valor de codigoSegmento
	 */
	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}
	/**
	 * Obtiene el valor de descSegmento
	 * @return El valor de descSegmento
	 */
	public String getDescSegmento() {
		return descSegmento;
	}
	/**
	 * Define el nuevo valor para descSegmento
	 * @param descSegmento El nuevo valor de descSegmento
	 */
	public void setDescSegmento(String descSegmento) {
		this.descSegmento = descSegmento;
	}
	/**
	 * Obtiene el valor de codigoActGenerica
	 * @return El valor de codigoActGenerica
	 */
	public String getCodigoActGenerica() {
		return codigoActGenerica;
	}
	/**
	 * Define el nuevo valor para codigoActGenerica
	 * @param codigoActGenerica El nuevo valor de codigoActGenerica
	 */
	public void setCodigoActGenerica(String codigoActGenerica) {
		this.codigoActGenerica = codigoActGenerica;
	}
	/**
	 * Obtiene el valor de codigoActEspecifica
	 * @return El valor de codigoActEspecifica
	 */
	public String getCodigoActEspecifica() {
		return codigoActEspecifica;
	}
	/**
	 * Define el nuevo valor para codigoActEspecifica
	 * @param codigoActEspecifica El nuevo valor de codigoActEspecifica
	 */
	public void setCodigoActEspecifica(String codigoActEspecifica) {
		this.codigoActEspecifica = codigoActEspecifica;
	}
	/**
	 * Obtiene el valor de codigoProducto
	 * @return El valor de codigoProducto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * Define el nuevo valor para codigoProducto
	 * @param codigoProducto El nuevo valor de codigoProducto
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * Obtiene el valor de descProducto
	 * @return El valor de descProducto
	 */
	public String getDescProducto() {
		return descProducto;
	}
	/**
	 * Define el nuevo valor para descProducto
	 * @param descProducto El nuevo valor de descProducto
	 */
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	/**
	 * Obtiene el valor de codigoSubproducto
	 * @return El valor de codigoSubproducto
	 */
	public String getCodigoSubproducto() {
		return codigoSubproducto;
	}
	/**
	 * Define el nuevo valor para codigoSubproducto
	 * @param codigoSubproducto El nuevo valor de codigoSubproducto
	 */
	public void setCodigoSubproducto(String codigoSubproducto) {
		this.codigoSubproducto = codigoSubproducto;
	}
	/**
	 * Obtiene el valor de codigoPais
	 * @return El valor de codigoPais
	 */
	public String getCodigoPaisNacionalidad() {
		return codigoPaisNacionalidad;
	}
	/**
	 * Define el nuevo valor para codigoPais
	 * @param codigoPaisNacionalidad El nuevo valor de codigoPais
	 */
	public void setCodigoPaisNacionalidad(String codigoPaisNacionalidad) {
		this.codigoPaisNacionalidad = codigoPaisNacionalidad;
	}
	/**
	 * Obtiene el valor de codigoEntidad
	 * @return El valor de codigoEntidad
	 */
	public String getCodigoEntidad() {
		return codigoEntidad;
	}
	/**
	 * Define el nuevo valor para codigoEntidad
	 * @param codigoEntidad El nuevo valor de codigoEntidad
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	/**
	 * Obtiene el valor de codigoMunicipio
	 * @return El valor de codigoMunicipio
	 */
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}
	/**
	 * Define el nuevo valor para codigoMunicipio
	 * @param codigoMunicipio El nuevo valor de codigoMunicipio
	 */
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	/**
	 * Obtiene el valor de codigoNacionalidad
	 * @return El valor de codigoNacionalidad
	 */
	public String getCodigoPaisResidencia() {
		return codigoPaisResidencia;
	}
	/**
	 * Define el nuevo valor para codigoNacionalidad
	 * @param codigoPaisResidencia El nuevo valor de codigoNacionalidad
	 */
	public void setCodigoPaisResidencia(String codigoPaisResidencia) {
		this.codigoPaisResidencia = codigoPaisResidencia;
	}
	/**
	 * Obtiene el valor de canalComercializacion
	 * @return El valor de canalComercializacion
	 */
	public String getCanalComercializacion() {
		return canalComercializacion;
	}
	/**
	 * Define el nuevo valor para canalComercializacion
	 * @param canalComercializacion El nuevo valor de canalComercializacion
	 */
	public void setCanalComercializacion(String canalComercializacion) {
		this.canalComercializacion = canalComercializacion;
	}
	/**
	 * Obtiene el valor de canalOperacion
	 * @return El valor de canalOperacion
	 */
	public String getCanalOperacion() {
		return canalOperacion;
	}
	/**
	 * Define el nuevo valor para canalOperacion
	 * @param canalOperacion El nuevo valor de canalOperacion
	 */
	public void setCanalOperacion(String canalOperacion) {
		this.canalOperacion = canalOperacion;
	}
}
