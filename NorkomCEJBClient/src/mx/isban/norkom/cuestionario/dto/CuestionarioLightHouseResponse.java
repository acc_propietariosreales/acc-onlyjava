/**
 * 
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 * Objetivo: Guardar la respuesta a la peticion de Cuestionarios por parte de LightHouse
 * Justificacion: Es necesario el retorno de codigo de operacion, un mensaje de operacion, id de cuestionario, un nivel de riesgo y un indicador UPLD en una solo respuesta
 */
public class CuestionarioLightHouseResponse implements Serializable {
	/**
	 * Objeto para la serialzacion del objeto
	 */
	private static final long serialVersionUID = -8205268261829885971L;
	/** Codigo del resultado de la operacion */
	private String codigoOperacion;
	/** Mensaje del resultado de la operacion */
	private String mensajeOperacion;
	/** Identificador del cuestionario */
	private String idCuestionario;
	/** Nivel de riesgo obtenido */
	private String nivelRiesgo;
	/** Indicdor UPLD obtenido */
	private String indicadorUpld;
	
	/**
	 * Obtiene el valor de codigoOperacion
	 * @return El valor de codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	/**
	 * Define el nuevo valor para codigoOperacion
	 * @param codigoOperacion El nuevo valor de codigoOperacion
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	/**
	 * Obtiene el valor de mensajeOperacion
	 * @return El valor de mensajeOperacion
	 */
	public String getMensajeOperacion() {
		return mensajeOperacion;
	}
	/**
	 * Define el nuevo valor para mensajeOperacion
	 * @param mensajeOperacion El nuevo valor de mensajeOperacion
	 */
	public void setMensajeOperacion(String mensajeOperacion) {
		this.mensajeOperacion = mensajeOperacion;
	}
	/**
	 * Obtiene el valor de idCuestionario
	 * @return El valor de idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * Define el nuevo valor para idCuestionario
	 * @param idCuestionario El nuevo valor de idCuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}
	/**
	 * Obtiene el valor de nivelRiesgo
	 * @return El valor de nivelRiesgo
	 */
	public String getNivelRiesgo() {
		return nivelRiesgo;
	}
	/**
	 * Define el nuevo valor para nivelRiesgo
	 * @param nivelRiesgo El nuevo valor de nivelRiesgo
	 */
	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}
	/**
	 * Obtiene el valor de indicadorUpld
	 * @return El valor de indicadorUpld
	 */
	public String getIndicadorUpld() {
		return indicadorUpld;
	}
	/**
	 * Define el nuevo valor para indicadorUpld
	 * @param indicadorUpld El nuevo valor de indicadorUpld
	 */
	public void setIndicadorUpld(String indicadorUpld) {
		this.indicadorUpld = indicadorUpld;
	}
}
