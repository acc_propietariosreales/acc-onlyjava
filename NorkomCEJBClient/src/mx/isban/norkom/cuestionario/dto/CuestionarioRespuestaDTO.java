/**
 * Isban Mexico
 *   Clase: CuestionarioRespuestaDTO.java
 *   Descripcion: DTO para almacenar datos de Cuestionarios relcion pregunta-respuesta
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */


package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import mx.isban.norkom.cuestionario.bean.ResponseBase;

public class CuestionarioRespuestaDTO extends ResponseBase implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -784677863338520322L;
	/**
	 * claveCuestionarioResp
	 */
	private int claveCuestionarioResp;
	/**
	 * claveClienteCuestionario
	 */
	private int claveClienteCuestionario;
	/**
	 * claveCuestionario
	 */
	private int claveCuestionario;
	/**
	 * claveRespuestaPregunta
	 */
	private int claveRespuestaPregunta;
	/**
	 * clavePregunta
	 */
	private int clavePregunta;
	/**
	 * valorTexto
	 */
	private String valorTexto;
	/**
	 * valorOpcion
	 */
	private int valorOpcion;
	/**
	 * tipoRespuesta
	 */
	private String tipoRespuesta;
	/**
	 * int claveGrupo
	 */
	private int claveGrupo;
	
	/**
	 * @return Obtiene la clave del cuestionario con las respuestas
	 */
	public int getClaveCuestionarioResp() {
		return claveCuestionarioResp;
	}
	/**
	 * Define la clave del cuestionario con las respuestas a agregar
	 * @param claveCuestionarioResp int
	 */
	public void setClaveCuestionarioResp(int claveCuestionarioResp) {
		this.claveCuestionarioResp = claveCuestionarioResp;
	}
	/**
	 * @return Obtiene la clave del cuestionario del cliente
	 */
	public int getClaveClienteCuestionario() {
		return claveClienteCuestionario;
	}
	/**
	 * Define la clave del cuestionario del cliente a agregar
	 * @param claveClienteCuestionario int
	 */
	public void setClaveClienteCuestionario(int claveClienteCuestionario) {
		this.claveClienteCuestionario = claveClienteCuestionario;
	}
	/**
	 * @return Obtiene la clave del cuestionario
	 */
	public int getClaveCuestionario() {
		return claveCuestionario;
	}
	/**
	 * Define la clave del cuestionario a agregar
	 * @param claveCuestionario int
	 */
	public void setClaveCuestionario(int claveCuestionario) {
		this.claveCuestionario = claveCuestionario;
	}
	/**
	 * @return Obtiene la clave de la respuesta de la pregunta
	 */
	public int getClaveRespuestaPregunta() {
		return claveRespuestaPregunta;
	}
	/**
	 * Define la clave de la respuesta de la pregunta a agregar
	 * @param claveRespuestaPregunta int
	 */
	public void setClaveRespuestaPregunta(int claveRespuestaPregunta) {
		this.claveRespuestaPregunta = claveRespuestaPregunta;
	}
	/**
	 * @return Obtiene la clave de la pregunta
	 */
	public int getClavePregunta() {
		return clavePregunta;
	}
	/**
	 * Define la clave de la pregunta a agregar
	 * @param clavePregunta int
	 */
	public void setClavePregunta(int clavePregunta) {
		this.clavePregunta = clavePregunta;
	}
	/**
	 * @return Obtiene el valor del campo de texto
	 */
	public String getValorTexto() {
		if(valorTexto == null){
			return StringUtils.EMPTY;
		}
		
		return valorTexto;
	}
	/**
	 * Define el valor del campo de texto a agregar
	 * @param valorTexto string
	 */
	public void setValorTexto(String valorTexto) {
		this.valorTexto = valorTexto;
	}
	/**
	 * @return Obtiene el valor de la opcion
	 */
	public int getValorOpcion() {
		return valorOpcion;
	}
	/**
	 * Define el valor de la opcion a agregar
	 * @param valorOpcion int
	 */
	public void setValorOpcion(int valorOpcion) {
		this.valorOpcion = valorOpcion;
	}
	/**
	 * @return Obtiene el tipo de respuesta
	 */
	public String getTipoRespuesta() {
		return tipoRespuesta;
	}
	/**
	 * Define el tipo de respuesta a agregar
	 * @param tipoRespuesta string
	 */
	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
	
	/**
	 * @return Obtiene la clave del grupo
	 */
	public int getClaveGrupo() {
		return claveGrupo;
	}
	/**
	 * Define la clave del grupo a agregar
	 * @param claveGrupo void
	 */
	public void setClaveGrupo(int claveGrupo) {
		this.claveGrupo = claveGrupo;
	}
	/* (sin Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CuestionarioRespuestaDTO [claveCuestionarioResp="
				+ claveCuestionarioResp + ", claveClienteCuestionario="
				+ claveClienteCuestionario + ", claveCuestionario="
				+ claveCuestionario + ", clavePregunta=" + clavePregunta
				+ ", valorTexto=" + valorTexto + ", valorOpcion=" + valorOpcion
				+ ", tipoRespuesta=" + tipoRespuesta + "]";
	}
}
