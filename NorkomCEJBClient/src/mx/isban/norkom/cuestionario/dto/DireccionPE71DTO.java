/**
 * Isban Mexico
 *   Clase: DireccionPE71DTO.java
 *   Descripcion: DTO para almacenar los datos de direccion de la transaccion PE71
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class DireccionPE71DTO implements Serializable {
	/**
	 * Id para serializacion del objeto
	 */
	private static final long serialVersionUID = 3434027876962103282L;
	/**
	 * tipoVia de PE71
	 */
	private String tipoVia;
	/**
	 * calle 
	 */
	private String calle;
	/**
	 * bloque
	 */
	private String bloque;
	/**
	 * localidad
	 */
	private String localidad;
	/**
	 * comuna
	 */
	private String comuna;
	/**
	 * codigoPostal
	 */
	private String codigoPostal;
	/**
	 * rutaCartero
	 */
	private String rutaCartero;
	/**
	 * codigoProvincia
	 */
	private String codigoProvincia;
	/**
	 * codigoProvinciaDos
	 */
	private String codigoProvinciaDos;
	
	/**
	 * Obtiene el valor de tipoVia
	 * @return El valor de tipoVia
	 */
	public String getTipoVia() {
		return tipoVia;
	}
	/**
	 * Define el nuevo valor para tipoVia
	 * @param tipoVia El nuevo valor de tipoVia
	 */
	public void setTipoVia(String tipoVia) {
		this.tipoVia = tipoVia;
	}
	/**
	 * Obtiene el valor de calle
	 * @return El valor de calle
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * Define el nuevo valor para calle
	 * @param calle El nuevo valor de calle
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * Obtiene el valor de bloque
	 * @return El valor de bloque
	 */
	public String getBloque() {
		return bloque;
	}
	/**
	 * Define el nuevo valor para bloque
	 * @param bloque El nuevo valor de bloque
	 */
	public void setBloque(String bloque) {
		this.bloque = bloque;
	}
	/**
	 * Obtiene el valor de localidad
	 * @return El valor de localidad
	 */
	public String getLocalidad() {
		return localidad;
	}
	/**
	 * Define el nuevo valor para localidad
	 * @param localidad El nuevo valor de localidad
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	/**
	 * Obtiene el valor de comuna
	 * @return El valor de comuna
	 */
	public String getComuna() {
		return comuna;
	}
	/**
	 * Define el nuevo valor para comuna
	 * @param comuna El nuevo valor de comuna
	 */
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	/**
	 * Obtiene el valor de codigoPostal
	 * @return El valor de codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}
	/**
	 * Define el nuevo valor para codigoPostal
	 * @param codigoPostal El nuevo valor de codigoPostal
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	/**
	 * Obtiene el valor de rutaCartero
	 * @return El valor de rutaCartero
	 */
	public String getRutaCartero() {
		return rutaCartero;
	}
	/**
	 * Define el nuevo valor para rutaCartero
	 * @param rutaCartero El nuevo valor de rutaCartero
	 */
	public void setRutaCartero(String rutaCartero) {
		this.rutaCartero = rutaCartero;
	}
	/**
	 * Obtiene el valor de codigoProvincia
	 * @return El valor de codigoProvincia
	 */
	public String getCodigoProvincia() {
		return codigoProvincia;
	}
	/**
	 * Define el nuevo valor para codigoProvincia
	 * @param codigoProvincia El nuevo valor de codigoProvincia
	 */
	public void setCodigoProvincia(String codigoProvincia) {
		this.codigoProvincia = codigoProvincia;
	}
	/**
	 * Obtiene el valor de codigoProvinciaDos
	 * @return El valor de codigoProvinciaDos
	 */
	public String getCodigoProvinciaDos() {
		return codigoProvinciaDos;
	}
	/**
	 * Define el nuevo valor para codigoProvinciaDos
	 * @param codigoProvinciaDos El nuevo valor de codigoProvinciaDos
	 */
	public void setCodigoProvinciaDos(String codigoProvinciaDos) {
		this.codigoProvinciaDos = codigoProvinciaDos;
	}
}
