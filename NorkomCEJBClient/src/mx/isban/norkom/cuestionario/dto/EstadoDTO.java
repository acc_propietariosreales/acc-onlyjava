/**
 * Isban Mexico
 *   Clase: EstadoDTO.java
 *   Descripcion: DTO para almacenar datos de estados
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class EstadoDTO implements Serializable{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2839740061749176893L;
	
	/**
	 * int clave de Estado
	 */
	private int claveEstado;
	/** 
	 * String codigo del estado
	 */
	private String codigo;
	/** 
	 * String nombre del estado
	 */
	private String nombre;
	
	/**
	 * @return Obtiene la clave del estado
	 */
	public int getClaveEstado() {
		return claveEstado;
	}
	/**
	 * Define la clave del estado a agregar
	 * @param claveEstado int
	 */
	public void setClaveEstado(int claveEstado) {
		this.claveEstado = claveEstado;
	}
	/**
	 * @return Obtiene el codigo del estado
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * Define el codigo del estado a agregar
	 * @param codigo string
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return Obtiene el nombre del estado
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Define el nombre del estado a agregar
	 * @param nombre string
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
