/**
 * Isban Mexico
 *   Clase: FamiliarDTO.java
 *   Descripcion: DTO para almacenar datos de familiares
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class FamiliarDTO implements Serializable {
	/**
	 * String ABREV_FAM_LISTA_PARENTESCO
	 */
	public static final String ABREV_FAM_LISTA_PARENTESCO = "IAPR";
	/**
	 * String ABREV_FAM_PARENTESCO
	 */
	public static final String ABREV_FAM_PARENTESCO = "IAP";
	/**
	 * String ABREV_FAM_NOMBRE
	 */
	public static final String ABREV_FAM_NOMBRE = "IAN";
	/**
	 * String ABREV_FAM_DOMICILIO
	 */
	public static final String ABREV_FAM_DOMICILIO = "IAD";
	/**
	 * String ABREV_FAM_FECHA_NAC
	 */
	public static final String ABREV_FAM_FECHA_NAC = "IAF";
	/**
	 * String SEPARADOR_VALOR_CAMPO_ENTRADA
	 */
	public static final String SEPARADOR_VALOR_CAMPO_ENTRADA = ":::";
	/**
	 * int IND_ABREV_NUM_REG_CAMPO_ENTRADA
	 */
	private static final int IND_ABREV_NUM_REG_CAMPO_ENTRADA = 0;
	/**
	 * int IND_VALOR_CAMPO_ENTRADA
	 */
	private static final int IND_VALOR_CAMPO_ENTRADA = 1;
	/**
	 * Identificador para la serializacion del objeto
	 */
	private static final long serialVersionUID = 3663473729341259726L;
	/**
	 * Clave del familiar
	 */
	private int claveFamiliar;
	/**
	 * Parentesco con el titular
	 */
	private String parentesco;
	/**
	 * Nombre completo del familiar
	 */
	private String nombre;
	/**
	 * Domicilio del familiar
	 */
	private String domicilio;
	/**
	 * Fecha de nacimiento del familiar
	 */
	private String fechaNacimiento;
	
	/**
	 * @return Obtiene la clave del familiar del cliente
	 */
	public int getClaveFamiliar() {
		return claveFamiliar;
	}
	/**
	 * Define la clave del familiar del cliente a agregar
	 * @param claveFamiliar void
	 */
	public void setClaveFamiliar(int claveFamiliar) {
		this.claveFamiliar = claveFamiliar;
	}
	/**
	 * @return Obtiene el parentesco del familiar del cliente
	 */
	public String getParentesco() {
		return parentesco;
	}
	/**
	 * Define el parentesco del familiar del cliente a agregar
	 * @param parentesco void
	 */
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}
	/**
	 * @return Obtiene el nombre del familiar del cliente
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Define el nombre del familiar del cliente a agregar
	 * @param nombre void
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return Obtiene el domicilio del familiar del cliente
	 */
	public String getDomicilio() {
		return domicilio;
	}
	/**
	 * Define el domicilio del familiar del cliente a agregar
	 * @param domicilio void
	 */
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	/**
	 * @return Obtiene la fecha de nacimiento del familiar del cliente
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * Define la fecha de nacimiento del familiar del cliente a agregar
	 * @param fechaNacimiento void
	 */
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	/**
	 * Define el valor del campo de texto
	 * @param valorTexto void
	 */
	public void definirValorCampo(String valorTexto) {
		String[] valoresSeparados = valorTexto.split(SEPARADOR_VALOR_CAMPO_ENTRADA);
		String abrev = valoresSeparados[IND_ABREV_NUM_REG_CAMPO_ENTRADA].substring(0, 3);
		String valor = valoresSeparados[IND_VALOR_CAMPO_ENTRADA];
		
		if(ABREV_FAM_PARENTESCO.equals(abrev)) {
			this.parentesco = valor;
		} else if(ABREV_FAM_NOMBRE.equals(abrev)) {
			this.nombre = valor;
		} else if(ABREV_FAM_DOMICILIO.equals(abrev)) {
			this.domicilio = valor;
		} else if(ABREV_FAM_FECHA_NAC.equals(abrev)) {
			this.fechaNacimiento = valor;
		}
	}
}