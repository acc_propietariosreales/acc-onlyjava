/**
 * Isban Mexico
 *   Clase: IndicadorOPD2WS.java
 *   Descripcion: DTO para almacenar los datos de indicador de la transaccion OPD2
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 *
 */
public class IndicadorOPD2WS implements Serializable {
	/**
	 * Id de version para la serializacion del objeto
	 */
	private static final long serialVersionUID = -2676845148212780080L;
	/**
	 * Codigo del cliente
	 */
	private String buc;
	/**
	 * Nivel de riesgo que se desea asignar
	 */
	private String nivelRiesgo;
	/**
	 * Indicador UPLD que se desea asignar
	 */
	private String indicadorUpld;
	/**
	 * Identificador de Norkom
	 */
	private String idNorkom;
	/**
	 * Fecha de actualizacion en Norkom
	 */
	private String fechaActualizacion;
	/**
	 * Ip del cliente que esta realizando la peticion
	 */
	private String ipCliente;
	/**
	 * Usuario que esta realizando la peticion
	 */
	private String usuario;
	/**
	 * Identificador de la sesion
	 */
	private String idSesion;
	/**
	 * Identificador de la referencia de la aplicacion
	 */
	private String idRefApp;
	/**
	 * Nombre del servidor
	 */
	private String nombreServidor;
	
	/**
	 * Obtiene el valor de buc
	 * @return El valor de buc
	 */
	public String getBuc() {
		return buc;
	}
	/**
	 * Define el nuevo valor para buc
	 * @param buc El nuevo valor de buc
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	/**
	 * Obtiene el valor de Nivel de Riesgo
	 * @return El valor de Nivel de Riesgo
	 */
	public String getNivelRiesgo() {
		return nivelRiesgo;
	}
	/**
	 * Define el nuevo valor para Nivel de Riesgo
	 * @param nivelRiesgo El nuevo valor de Nivel de Riesgo
	 */
	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}
	/**
	 * Obtiene el valor de Indicador Upld
	 * @return El valor de Indicador Upld
	 */
	public String getIndicadorUpld() {
		return indicadorUpld;
	}
	/**
	 * Define el nuevo valor para Indicador Upld
	 * @param indicadorUpld El nuevo valor de Indicador Upld
	 */
	public void setIndicadorUpld(String indicadorUpld) {
		this.indicadorUpld = indicadorUpld;
	}
	/**
	 * Obtiene el valor de Id Norkom
	 * @return El valor de Id Norkom
	 */
	public String getIdNorkom() {
		return idNorkom;
	}
	/**
	 * Define el nuevo valor para Id Norkom
	 * @param idNorkom El nuevo valor de Id Norkom
	 */
	public void setIdNorkom(String idNorkom) {
		this.idNorkom = idNorkom;
	}
	/**
	 * Obtiene el valor de Fecha de Actualizacion
	 * @return El valor de Fecha de Actualizacion
	 */
	public String getFechaActualizacion() {
		return fechaActualizacion;
	}
	/**
	 * Define el nuevo valor para Fecha de Actualizacion
	 * @param fechaActualizacion El nuevo valor de Fecha de Actualizacion
	 */
	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	/**
	 * Obtiene el valor de la Ip del Cliente
	 * @return El valor de la Ip del Cliente
	 */
	public String getIpCliente() {
		return ipCliente;
	}
	/**
	 * Define el nuevo valor para la Ip del Cliente
	 * @param ipCliente El nuevo valor de la Ip del Cliente
	 */
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}
	/**
	 * Obtiene el valor de usuario
	 * @return El valor de usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * Define el nuevo valor para usuario
	 * @param usuario El nuevo valor de usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * Obtiene el valor de idSesion
	 * @return El valor de idSesion
	 */
	public String getIdSesion() {
		return idSesion;
	}
	/**
	 * Define el nuevo valor para idSesion
	 * @param idSesion El nuevo valor de idSesion
	 */
	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}
	/**
	 * Obtiene el valor del Id de Referencia de la Aplicacion
	 * @return El valor del Id de Referencia de la Aplicacion
	 */
	public String getIdRefApp() {
		return idRefApp;
	}
	/**
	 * Define el nuevo valor del Id de Referencia de la Aplicacion
	 * @param idRefApp El nuevo valor del Id de Referencia de la Aplicacion
	 */
	public void setIdRefApp(String idRefApp) {
		this.idRefApp = idRefApp;
	}
	/**
	 * Obtiene el valor de Nombre del Servidor
	 * @return El valor de Nombre del Servidor
	 */
	public String getNombreServidor() {
		return nombreServidor;
	}
	/**
	 * Define el nuevo valor para Nombre del Servidor
	 * @param nombreServidor El nuevo valor de Nombre del Servidor
	 */
	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}
}
