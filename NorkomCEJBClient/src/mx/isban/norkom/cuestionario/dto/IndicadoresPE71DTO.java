/**
 * Isban Mexico
 *   Clase: IndicadoresPE71DTO.java
 *   Descripcion: DTO para almacenar datos de indicadores de PE71
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

public class IndicadoresPE71DTO {
	/**
	 * indicadorRelaciones
	 */
	private String indicadorRelaciones;
	/**
	 * indicador de Avisos
	 */
	private String indicadorAvisos;
	/**
	 * indicador de UsoUno
	 */
	private String indicadorUsoUno;
	/**
	 * indicador de UsoDos
	 */
	private String indicadorUsoDos;
	/**
	 * indicador de UsoTres
	 */
	private String indicadorUsoTres;
	/**
	 * indicador de UsoCuatro
	 */
	private String indicadorUsoCuatro;
	/**
	 * indicador de UsoCinco
	 */
	private String indicadorUsoCinco;
	/**
	 * Obtiene el indicador de las relaciones
	 * @return String con el valor del indicador
	 */
	public String getIndicadorRelaciones() {
		return indicadorRelaciones;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorRelaciones El nuevo valor del indicador
	 */
	public void setIndicadorRelaciones(String indicadorRelaciones) {
		this.indicadorRelaciones = indicadorRelaciones;
	}
	/**
	 * Obtiene el indicador de avisos
	 * @return String con el valor del indicador
	 */
	public String getIndicadorAvisos() {
		return indicadorAvisos;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorAvisos El nuevo valor del indicador
	 */
	public void setIndicadorAvisos(String indicadorAvisos) {
		this.indicadorAvisos = indicadorAvisos;
	}
	/**
	 * Obtiene el indicador de Uso Uno
	 * @return String con el valor del indicador
	 */
	public String getIndicadorUsoUno() {
		return indicadorUsoUno;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorUsoUno El nuevo valor del indicador
	 */
	public void setIndicadorUsoUno(String indicadorUsoUno) {
		this.indicadorUsoUno = indicadorUsoUno;
	}
	/**
	 * Obtiene el indicador de Uso Dos
	 * @return String con el valor del indicador
	 */
	public String getIndicadorUsoDos() {
		return indicadorUsoDos;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorUsoDos El nuevo valor del indicador
	 */
	public void setIndicadorUsoDos(String indicadorUsoDos) {
		this.indicadorUsoDos = indicadorUsoDos;
	}
	/**
	 * Obtiene el indicador de Uso Tres
	 * @return String con el valor del indicador
	 */
	public String getIndicadorUsoTres() {
		return indicadorUsoTres;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorUsoTres El nuevo valor del indicador
	 */
	public void setIndicadorUsoTres(String indicadorUsoTres) {
		this.indicadorUsoTres = indicadorUsoTres;
	}
	/**
	 * Obtiene el indicador de Uso Cuatro
	 * @return String con el valor del indicador
	 */
	public String getIndicadorUsoCuatro() {
		return indicadorUsoCuatro;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorUsoCuatro El nuevo valor del indicador
	 */
	public void setIndicadorUsoCuatro(String indicadorUsoCuatro) {
		this.indicadorUsoCuatro = indicadorUsoCuatro;
	}
	/**
	 * Obtiene el indicador de Uso Cinco
	 * @return String con el valor del indicador
	 */
	public String getIndicadorUsoCinco() {
		return indicadorUsoCinco;
	}
	/**
	 * Define el valor del indicador
	 * @param indicadorUsoCinco El nuevo valor del indicador
	 */
	public void setIndicadorUsoCinco(String indicadorUsoCinco) {
		this.indicadorUsoCinco = indicadorUsoCinco;
	}
}
