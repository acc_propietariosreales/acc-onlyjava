/**
 * Isban Mexico
 *   Clase: MunicipioDTO.java
 *   Descripcion: DTO para almacenar datos de municipios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class MunicipioDTO implements Serializable{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6368597546841814872L;
	/** 
	 * String codigo de Municipio
	 */
	private int claveMunicipio;
	/** 
	 * String codigo de Municipio
	 */
	private String codigoMunicipio;
	
	/**
	 * codigo de Estado
	 */
	private String codigoEstado;
	
	/** 
	 * String nombre
	 */
	private String nombre;

	/**
	 * @return Obtiene la clave del municipio
	 */
	public int getClaveMunicipio() {
		return claveMunicipio;
	}

	/**
	 * Define la clave del municipio a agregar
	 * @param claveMunicipio int
	 */
	public void setClaveMunicipio(int claveMunicipio) {
		this.claveMunicipio = claveMunicipio;
	}

	/**
	 * @return Obtiene el codigo del municipio
	 */
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}

	/**
	 * Define el codigo del municipio a agregar
	 * @param codigoMunicipio string
	 */
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	/**
	 * @return Obtiene el codigo del estado
	 */
	public String getCodigoEstado() {
		return codigoEstado;
	}

	/**
	 * Defineel codigo del estado a agregar
	 * @param codigoEstado string
	 */
	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	/**
	 * @return Obtiene el nombre del municipio
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Define el nombre del municipio a agregar
	 * @param nombre string
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
