/**
 * NorkomCEJBClient NivelRiesgoDTO.java
 */
package mx.isban.norkom.cuestionario.dto;

/**
 * @author Leopoldo F Espinosa R 30/06/2015
 *
 */
public class NivelRiesgoDTO extends ActividadEconomicaDTO{

	/**
	 * NivelRiesgoDTO.java  de tipo long
	 */
	private static final long serialVersionUID = 5453140089691544310L;
	/**presencia fisica para la validacion de Nivel de riesgo*/
	private boolean presenciaFisica;
	/**nivel de riesgo**/
	private String nivelRiesgo;
	/**pais bajo riesgo**/
	private String paisBajoRiesgo;
	/**id de cuestionario**/
	private String idCuestionario;
	/**propiedad para saber si es regimen simplificado y calificacion A1*/
	private boolean simplificadoA1;
	/**propiedad para saber si es regimen simplificado y calificacion diferente de A1*/
	private boolean simplificadoAx;
	/**
	 * @return the presenciaFisica
	 */
	public boolean isPresenciaFisica() {
		return presenciaFisica;
	}
	/**
	 * @param presenciaFisica 
	 * el presenciaFisica a agregar
	 */
	public void setPresenciaFisica(boolean presenciaFisica) {
		this.presenciaFisica = presenciaFisica;
	}
	/**
	 * @return the nivelRiesgo
	 */
	public String getNivelRiesgo() {
		return nivelRiesgo;
	}
	/**
	 * @param nivelRiesgo 
	 * el nivelRiesgo a agregar
	 */
	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}
	/**
	 * @param paisBajoRiesgo 
	 * el paisBajoRiesgo a agregar
	 */
	public void setPaisBajoRiesgo(String paisBajoRiesgo) {
		this.paisBajoRiesgo = paisBajoRiesgo;
	}
	/**
	 * @return the paisBajoRiesgo
	 */
	public String getPaisBajoRiesgo() {
		return paisBajoRiesgo;
	}
	/**
	 * @return the idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * @param idCuestionario 
	 * el idCuestionario a agregar
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * @return true si es regimen sikplificado y A1
	 */
	public boolean isSimplificadoA1() {
		return simplificadoA1;
	}
	/**
	 * @param simplificadoA1 
	 * el simplificadoA1 a agregar
	 */
	public void setSimplificadoA1(boolean simplificadoA1) {
		this.simplificadoA1 = simplificadoA1;
	}
	/**
	 * @return true si es regimen sikplificado diferente de A1
	 */
	public boolean isSimplificadoAx() {
		return simplificadoAx;
	}
	/**
	 * @param simplificadoAx 
	 * el simplificadoAx a agregar
	 */
	public void setSimplificadoAx(boolean simplificadoAx) {
		this.simplificadoAx = simplificadoAx;
	}
	
}
