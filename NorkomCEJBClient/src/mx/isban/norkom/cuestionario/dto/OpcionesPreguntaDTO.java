/**
 * Isban Mexico
 *   Clase: OpcionesPreguntaDTO.java
 *   Descripcion: DTO para las opciones de las preguntas/respuestas
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author STEFANINI (Leopoldo F Espinosa R) 04/12/2014
 *
 */
public class OpcionesPreguntaDTO implements Serializable{

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 938778228932054660L;
	/**id de la opcion**/
	private String idOpcion;
	/**descripcion de la opcion**/
	private String descripcion;
	/**valor de la opcion**/
	private String valor;
	/**tipo de grupo al que pertenece la opcion**/
	private String grupoTipo;
	/**
	 * @return Obtiene el id de la Opcion
	 */
	public String getIdOpcion() {
		return idOpcion;
	}
	/**
	 * Define el id de la Opcion a agregar
	 * @param idOpcion 
	 */
	public void setIdOpcion(String idOpcion) {
		this.idOpcion = idOpcion;
	}
	/**
	 * @return Obtiene la descripcion de la opcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * Define la descripcion de la opcion a agregar
	 * @param descripcion 
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return Obtiene el valor de la opcion
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * Define el valor de la opcion
	 * @param valor 
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	/**
	 * @return Obtiene el Tipo del grupo perteneciente
	 */
	public String getGrupoTipo() {
		return grupoTipo;
	}
	/**
	 * Define el Tipo del grupo perteneciente a agregar
	 * @param grupoTipo 
	 */
	public void setGrupoTipo(String grupoTipo) {
		this.grupoTipo = grupoTipo;
	}

}
