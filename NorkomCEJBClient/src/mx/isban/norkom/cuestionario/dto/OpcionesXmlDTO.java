/**
 * Isban Mexico
 *   Clase: OpcionesXmlDTO.java
 *   Descripcion: DTO para generar estructura 
 *   			  XML de cuestionario IP
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class OpcionesXmlDTO implements Serializable{
	
	/** Variable utilizada para declarar serialVersionUID **/
	private static final long serialVersionUID = -1894481478601726235L;
	/** Variable para definir el valor de una opcion **/
	private String valor;
	/** Variabla para definir la descripcion de una opcion **/
	private String descripcion;
	/** Variable para definir la abreviatura de la pregunta **/
	private String abreviatura;
	
	/** @return Obtiene el valor **/
	public String getValor() {
		return valor;
	}
	/** 
	 * Define valor el valor a agregar
	 * @param valor el valor
	 **/
	public void setValor(String valor) {
		this.valor = valor;
	}
	/** @return Obtiene la abreviatura **/
	public String getAbreviatura() {
		return abreviatura;
	}
	/** 
	 * Define la abreviatura a agregar
	 * @param abreviatura la abreviatura
	 **/
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	/**
	 * Define la descripcion a agregar
	 * @param descripcion la descripcion 
	 **/
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/** @return Obtiene la descripcion **/
	public String getDescripcion() {
		return descripcion;
	}
		
}