/**
 * Isban Mexico
 *   Clase: PE58DTO.java
 *   Descripcion: DTO para almacenar los datos de la transaccion PE58
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class PE58DTO implements Serializable {
	/**
	 * Id para serializacion del objeto
	 */
	private static final long serialVersionUID = -5712436451014425023L;
	/**
	 * numero de Persona
	 */
	protected String numeroPersona;
	/**
	 * tipo de Documento
	 */
	protected String tipoDocumento;
	/**
	 * numero de Documento
	 */
	protected String numeroDocumento;
	/**
	 * primer Apellido
	 */
	protected String primerApellido;
	/**
	 * segundo Apellido
	 */
	protected String segundoApellido;
	/**
	 * nombre
	 */
	protected String nombre;
	/**
	 * tipo de Persona
	 */
	protected String tipoPersona;
	/**
	 * observaciones Uno
	 */
	protected String observacionesUno;
	/**
	 * observaciones Dos
	 */
	protected String observacionesDos;
	/**
	 * Obtiene el valor de Numero de Persona
	 * @return El valor de numero de Persona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * Define el nuevo valor para numero de Persona
	 * @param numeroPersona El nuevo valor de Numero de Persona
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	/**
	 * Obtiene el valor de Tipo de Documento
	 * @return El valor de Tipo de Documento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * Define el nuevo valor para Tipo de Documento
	 * @param tipoDocumento El nuevo valor de Tipo de Documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * Obtiene el valor de Numero de Documento
	 * @return El valor de Numero de Documento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * Define el nuevo valor para Numero de Documento
	 * @param numeroDocumento El nuevo valor de Numero de Documento
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * Obtiene el valor de Primer Apellido
	 * @return El valor de Primer Apellido
	 */
	public String getPrimerApellido() {
		if(primerApellido == null){
			return StringUtils.EMPTY;
		}
		
		return primerApellido;
	}
	/**
	 * Define el nuevo valor para Primer Apellido
	 * @param primerApellido El nuevo valor de Primer Apellido
	 */
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	/**
	 * Obtiene el valor de Segundo Apellido
	 * @return El valor de Segundo Apellido
	 */
	public String getSegundoApellido() {
		if(segundoApellido == null){
			return StringUtils.EMPTY;
		}
		
		return segundoApellido;
	}
	/**
	 * Define el nuevo valor para Segundo Apellido
	 * @param segundoApellido El nuevo valor de Segundo Apellido
	 */
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	/**
	 * Obtiene el valor de nombre
	 * @return El valor de nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Define el nuevo valor para nombre
	 * @param nombre El nuevo valor de nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Obtiene el nombre de la referencia
	 * @return Nombre Completo formado por Nombre, Primer apellido y Segundo apellido 
	 */
	public String getNombreCompleto() {
		StringBuffer nombreCompleto = new StringBuffer(StringUtils.trimToEmpty(getNombre()));
		String apellidoPaternoTmp = StringUtils.trimToEmpty(getPrimerApellido());
		String apellidoMaternoTmp = StringUtils.trimToEmpty(getSegundoApellido());
		
		if(StringUtils.isNotBlank(apellidoPaternoTmp)){
			nombreCompleto.append(" ").append(apellidoPaternoTmp);
		}
		
		if(StringUtils.isNotBlank(apellidoMaternoTmp)){
			nombreCompleto.append(" ").append(apellidoMaternoTmp);
		}
		
		return nombreCompleto.toString();
	}
	
	/**
	 * Obtiene el valor de Tipo de Persona
	 * @return El valor de Tipo de Persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * Define el nuevo valor para Tipo de Persona
	 * @param tipoPersona El nuevo valor de Tipo de Persona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * Obtiene el valor de Observaciones Uno
	 * @return El valor de Observaciones Uno
	 */
	public String getObservacionesUno() {
		return observacionesUno;
	}
	/**
	 * Define el nuevo valor para Observaciones Uno
	 * @param observacionesUno El nuevo valor de Observaciones Uno
	 */
	public void setObservacionesUno(String observacionesUno) {
		this.observacionesUno = observacionesUno;
	}
	/**
	 * Obtiene el valor de Observaciones Dos
	 * @return El valor de Observaciones Dos
	 */
	public String getObservacionesDos() {
		return observacionesDos;
	}
	/**
	 * Define el nuevo valor para Observaciones Dos
	 * @param observacionesDos El nuevo valor de Observaciones Dos
	 */
	public void setObservacionesDos(String observacionesDos) {
		this.observacionesDos = observacionesDos;
	}
}
