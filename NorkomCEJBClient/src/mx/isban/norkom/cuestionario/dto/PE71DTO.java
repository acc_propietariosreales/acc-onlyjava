/**
 * Isban Mexico
 *   Clase: PE71DTO.java
 *   Descripcion: DTO para almacenar los datos de la transaccion PE71
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 *
 */
public class PE71DTO implements Serializable {
	/**
	 * Id para serializacion del objeto
	 */
	private static final long serialVersionUID = 9142942046033858448L;
	/**
	 * numeroPersona
	 */
	private String numeroPersona;
	/**
	 * tipoDocumento
	 */
	private String tipoDocumento;
	/**
	 * numeroDocumento
	 */
	private String numeroDocumento;
	/**
	 * secuenciaContacto
	 */
	private String secuenciaContacto;
	/**
	 * primerApellido
	 */
	private String primerApellido;
	/**
	 * segundoApellido
	 */
	private String segundoApellido;
	/**
	 * nombre
	 */
	private String nombre;
	/**
	 * observacionesUno
	 */
	private String observacionesUno;
	/**
	 * observacionesDos
	 */
	private String observacionesDos;
	/**
	 * timeStamp
	 */
	private String timeStamp;
	
	/**
	 * Obtiene el Numero de Persona
	 * @return El Numero de Persona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * Define el Numero de Persona
	 * @param numeroPersona El nuevo valor
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	/**
	 * Obtiene el Tipo de Documento
	 * @return El Tipo de Documento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * Define el nuevo Tipo de Documento
	 * @param tipoDocumento El nuevo valor de Tipo de Documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * Obtiene el Numero de Documento
	 * @return El Numero de Documento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * Define el nuevo Numero de Documento
	 * @param numeroDocumento El Numero de Documento
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * Define el nuevo Secuencia de Contacto
	 * @return La Secuencia de Contacto
	 */
	public String getSecuenciaContacto() {
		return secuenciaContacto;
	}
	/**
	 * Define la nueva Secuencia de Contacto
	 * @param secuenciaContacto El nuevo valor de Secuencia de Contacto
	 */
	public void setSecuenciaContacto(String secuenciaContacto) {
		this.secuenciaContacto = secuenciaContacto;
	}
	/**
	 * Obtiene el PrimerApellido
	 * @return El Primer Apellido
	 */
	public String getPrimerApellido() {
		return primerApellido;
	}
	/**
	 * Define el nuevo Primer Apellido
	 * @param primerApellido El Primer Apellido
	 */
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	/**
	 * Obtiene el Segundo Apellido
	 * @return El Segundo Apellido
	 */
	public String getSegundoApellido() {
		return segundoApellido;
	}
	/**
	 * Define el nuevo valor del Segundo Apellido
	 * @param segundoApellido El nuevo valor de Segundo Apellido
	 */
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	/**
	 * Obtiene el Nombre
	 * @return El Nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Define el nuevo valor del Nombre
	 * @param nombre El nuevo valor del Nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Obtiene el nombre de la referencia
	 * @return Nombre Completo formado por Nombre, Primer apellido y Segundo apellido 
	 */
	public String getNombreCompleto() {
		return String.format("%s %s %s", getNombre(), getPrimerApellido(), getSegundoApellido()).trim();
	}
	
	/**
	 * Obtiene el valor de Observaciones de Uno
	 * @return El valor de Observaciones de Uno
	 */
	public String getObservacionesUno() {
		return observacionesUno;
	}
	/**
	 * Define el nuevo valor para Observaciones de Uno
	 * @param observacionesUno El nuevo valor de Observaciones de Uno
	 */
	public void setObservacionesUno(String observacionesUno) {
		this.observacionesUno = observacionesUno;
	}
	/**
	 * Obtiene el valor de observaciones de Dos
	 * @return El valor de observaciones de Dos
	 */
	public String getObservacionesDos() {
		return observacionesDos;
	}
	/**
	 * Define el nuevo valor para Observaciones de Dos
	 * @param observacionesDos El nuevo valor de Observaciones de Dos
	 */
	public void setObservacionesDos(String observacionesDos) {
		this.observacionesDos = observacionesDos;
	}
	/**
	 * Obtiene el valor de timeStamp
	 * @return El valor de timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * Define el nuevo valor para timeStamp
	 * @param timeStamp El nuevo valor de timeStamp
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
}
