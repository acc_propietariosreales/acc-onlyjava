/**
 * Isban Mexico
 *   Clase: PaisDTO.java
 *   Descripcion: DTO para almacenar datos de paises
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class PaisDTO implements Serializable{
	
	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = -4322907031662414542L;
	/** 
	 * int id de Pais
	 */
	private int idPais;
	/** 
	 * String codigo
	 */
	private String codigo;
	/** 
	 * String nombre
	 */
	private String nombre;
	/** 
	 * String indicador de Riesgo
	 */
	private String indicadorRiesgo;
	
	/**
	 * Define el id del pais a agregar
	 * @param idPais 
	 */
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	/**
	 * @return Obtiene el id del Pais
	 */
	public int getIdPais() {
		return idPais;
	}
	/**
	 * Define el codigo del pais a agregar
	 * @param codigo 
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return Obtiene el codigo del pais
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * Define el nombre del pais a agregar
	 * @param nombre 
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return Obtiene el nombre del pais
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @return Obtiene el indicador de riesgo del pais
	 */
	public String getIndicadorRiesgo() {
		return indicadorRiesgo;
	}
	/**
	 * Define el indicador de riesgo del pais
	 * @param indicadorRiesgo string
	 */
	public void setIndicadorRiesgo(String indicadorRiesgo) {
		this.indicadorRiesgo = indicadorRiesgo;
	}

}
