/**
 * Isban Mexico
 *   Clase: ParametroDTO.java
 *   Descripcion: DTO para almacenar parametro de cuestionarios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class ParametroDTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3050526576069522860L;
	/**
	 * nombre del parametro
	 */
	private String nombre;
	/**
	 * valor del parametro
	 */
	private String valor;
	/**
	 * estaus  del parametro activo
	 */
	private boolean activo;
	
	/**
	 * @return Obtiene el nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Define el nombre a agregar
	 * @param nombre string
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return Obtiene el valor
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * Define el valor a agregar
	 * @param valor string
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	/**
	 * @return Obtiene si es activo
	 */
	public boolean isActivo() {
		return activo;
	}
	/**
	 * Define si esta activo
	 * @param activo boolean
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
}
