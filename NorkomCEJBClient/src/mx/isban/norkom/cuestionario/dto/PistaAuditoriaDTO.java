/**
 * Isban Mexico
 *   Clase: BeanPista.java
 *   Descripcion:Contenedor de atributos necesarios para realizar el grabado de pistas de auditoria.
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;


/**
 * Contenedor de atributos necesarios para realizar
 * el grabado de pistas de auditoria.
 * 
 * @author h2h.
 *
 */
public class PistaAuditoriaDTO implements Serializable {
	/**
	 * Id de version utilizado para la serializacion del objeto
	 */
	private static final long serialVersionUID = 8118151327347316428L;
	/**
	 * Servicio que identifica a la pista
	 */
	private String servicio;
	/**
	 * Canal de la aplicacion
	 */
	private String canal;
	/**
	 * Codigo de operacion de la pista
	 */
	private int codigoOperacion;
	/**
	 * Datos que se van a ver afectados por la pista
	 */
	private String datoAfectado;
	/**
	 * Valor anterior, antes de ejecutar la operacion
	 */
	private String valorAnterior;
	/**
	 * Valor que fue asignado
	 */
	private String valorNuevo;
	/**
	 * Tabla donde se registraron los cambios
	 */
	private String tablaAfectada;
	/**
	 * Tipo de operacion que se esta realizando
	 */
	private String tipoOperacion;
	/**
	 * Dato fijo en la tabla
	 */
	private String datoFijo;
	
	/**
	 * Obtiene el valor de servicio
	 * @return El valor de servicio
	 */
	public String getServicio() {
		return servicio;
	}
	/**
	 * Define el nuevo valor para servicio
	 * @param servicio El nuevo valor de servicio
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	/**
	 * Obtiene el valor de canal
	 * @return El valor de canal
	 */
	public String getCanal() {
		return canal;
	}
	/**
	 * Define el nuevo valor para canal
	 * @param canal El nuevo valor de canal
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}
	/**
	 * Obtiene el valor de Codigo de Operacion
	 * @return El valor de Codigo de Operacion
	 */
	public int getCodigoOperacion() {
		return codigoOperacion;
	}
	/**
	 * Define el nuevo valor para Codigo de Operacion
	 * @param codigoOperacion El nuevo valor de Codigo de Operacion
	 */
	public void setCodigoOperacion(int codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	/**
	 * Obtiene el valor de Dato Afectado
	 * @return El valor de Dato Afectado
	 */
	public String getDatoAfectado() {
		return datoAfectado;
	}
	/**
	 * Define el nuevo valor para Dato Afectado
	 * @param datoAfectado El nuevo valor de Dato Afectado
	 */
	public void setDatoAfectado(String datoAfectado) {
		this.datoAfectado = datoAfectado;
	}
	/**
	 * Obtiene el valor de Valor Anterior
	 * @return El valor de Valor Anterior
	 */
	public String getValorAnterior() {
		return valorAnterior;
	}
	/**
	 * Define el nuevo valor para Valor Anterior
	 * @param valorAnterior El nuevo valor de Valor Anterior
	 */
	public void setValorAnterior(String valorAnterior) {
		this.valorAnterior = valorAnterior;
	}
	/**
	 * Obtiene el valor de Valor Nuevo
	 * @return El valor de Valor Nuevo
	 */
	public String getValorNuevo() {
		return valorNuevo;
	}
	/**
	 * Define el nuevo valor para Valor Nuevo
	 * @param valorNuevo El nuevo valor de Valor Nuevo
	 */
	public void setValorNuevo(String valorNuevo) {
		this.valorNuevo = valorNuevo;
	}
	/**
	 * Obtiene el valor de Tabla Afectada
	 * @return El valor de Tabla Afectada
	 */
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	/**
	 * Define el nuevo valor para Tabla Afectada
	 * @param tablaAfectada El nuevo valor de Tabla Afectada
	 */
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	/**
	 * Obtiene el valor de Tipo de Operacion
	 * @return El valor de Tipo de Operacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * Define el nuevo valor para Tipo de Operacion
	 * @param tipoOperacion El nuevo valor de Tipo de Operacion
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * Obtiene el valor de Dato Fijo
	 * @return El valor de Dato Fijo
	 */
	public String getDatoFijo() {
		return datoFijo;
	}
	/**
	 * Define el nuevo valor para Dato Fijo
	 * @param datoFijo El nuevo valor de Dato Fijo
	 */
	public void setDatoFijo(String datoFijo) {
		this.datoFijo = datoFijo;
	}
}