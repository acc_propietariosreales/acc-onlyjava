/**
 * Isban Mexico
 *   Clase: PreguntaDTO.java
 *   Descripcion: DTO para almacenar preguntas del cuestionario
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */


package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
public class PreguntaDTO extends RespuestaPreguntaDTO implements Serializable,Comparable<PreguntaDTO> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 6599128897927390023L;
	/**PK_PREGUNTA**/
	private int idPregunta;
	/**Identificador texto de pregunta**/
	private String idDescripcionPregunta;
	/**TITULO**/
	private String titulo;
	/**SECCION**/
	private String seccion;
	/**PREGUNTA**/
	private String pregunta;
	/**TIPO_PREGUNTA**/
	private String tipoPregunta;
	/**PK_TIPO_PREGUNTA**/
	private String idTipoPregunta;
	/**FK_GRUPO_PREGUNTA**/
	private String claveGrupoPregunta;
	/**Clave Mapeo**/
	private String claveMapeo;
	/**DESCRIPCION GRUPO**/
	private String descripcionGrupoPregunta;
	/** ABREVIATURA**/
	private String abreviatura;
	/** LON MAX**/
	private int longitudMaxima;
	/** CAMPO NUMERICO**/
	private boolean campoNumerico;
	/**
	 * boolean campoFecha
	 */
	private boolean campoFecha;
	/**
	 * PreguntaDTO
	 */
	public PreguntaDTO(){
	}

	/**
	 * PreguntaDTO inicializa la pregunta y su respuesta
	 * @param pregunta string
	 * @param respuesta string
	 * @param claveMapeo string
	 */
	public PreguntaDTO(String pregunta,String respuesta,String claveMapeo)
	{
		this.pregunta=pregunta;
		super.respuesta=respuesta;
		this.claveMapeo=claveMapeo;
	}
	/**
	 * @return Obtiene la clave para Mapear la pregunta
	 */
	public String getClaveMapeo() {
		return claveMapeo;
	}
	/**
	 * Define la clave para Mapear la pregunta
	 * @param claveMapeo el claveMapeo a establecer
	 */
	public void setClaveMapeo(String claveMapeo) {
		this.claveMapeo = claveMapeo;
	}
	/**
	 * @return Obtiene el id de la Pregunta
	 */
	public int getIdPregunta() {
		return idPregunta;
	}
	/**
	 * @param idPregunta
	 * Define el id de la Pregunta a agregar
	 */
	public void setIdPregunta(int idPregunta) {
		this.idPregunta = idPregunta;
	}
	/**
	 * @return Obtiene el id de la descripcion de la pregunta
	 */
	public String getIdDescripcionPregunta() {
		return idDescripcionPregunta;
	}
	/**
	 * Define el id de la descripcion de la pregunta a agregar
	 * @param idDescripcionPregunta STRING
	 */
	public void setIdDescripcionPregunta(String idDescripcionPregunta) {
		this.idDescripcionPregunta = idDescripcionPregunta;
	}
	/**
	 * @return Obtiene el titulo de la pregunta
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo
	 * Define el titulo de la pregunta a agregar
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return Obtiene la seccion
	 */
	public String getSeccion() {
		return seccion;
	}
	/**
	 * Define la seccion a agregar
	 * @param seccion STRING
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	/**
	 * @return Obtiene la pregunta
	 */
	public String getPregunta() {
		return pregunta;
	}
	/**
	 * @param pregunta
	 * Define la pregunta a agregar
	 */
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	/**
	 * @return Obtiene el tipo de pregunta
	 */
	public String getTipoPregunta() {
		return tipoPregunta;
	}
	/**
	 * @param tipoPregunta
	 * Define el tipo de Pregunta a agregar
	 */
	public void setTipoPregunta(String tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}
	/**
	 * @return Obtiene el id del Tipo de Pregunta
	 */
	public String getIdTipoPregunta() {
		return idTipoPregunta;
	}
	/**
	 * @param idTipoPregunta
	 * Define el id del Tipo de Pregunta a agregar
	 */
	public void setIdTipoPregunta(String idTipoPregunta) {
		this.idTipoPregunta = idTipoPregunta;
	}
	/**
	 * @return Obtiene el id del Tipo de Pregunta del Grupo
	 */
	public String getClaveGrupoPregunta() {
		return claveGrupoPregunta;
	}
	/**
	 * @param idTipoPreguntaGpo
	 * Define el id del Tipo de Pregunta del Grupo a agregar
	 */
	public void setClaveGrupoPregunta(String idTipoPreguntaGpo) {
		this.claveGrupoPregunta = idTipoPreguntaGpo;
	}
	/**
	 * @return Obtiene la descripcion del grupo de la pregunta
	 */
	public String getDescripcionGrupoPregunta() {
		return descripcionGrupoPregunta;
	}
	/**
	 * Define la descripcion del grupo de la pregunta a agregar
	 * @param descripcionGrupoPregunta STRING
	 */
	public void setDescripcionGrupoPregunta(String descripcionGrupoPregunta) {
		this.descripcionGrupoPregunta = descripcionGrupoPregunta;
	}
	/**
	 * @return Obtiene abreviatura de la pregunta
	 */
	public String getAbreviatura() {
		return abreviatura;
	}
	/**
	 * Define abreviatura de la pregunta a agregar
	 * @param abreviatura STRING
	 */
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	/**
	 * @return Obtiene longitud maxima
	 */
	public int getLongitudMaxima() {
		return longitudMaxima;
	}
	/**
	 * Define longitud maxima a agregar
	 * @param longitudMaxima INT
	 */
	public void setLongitudMaxima(int longitudMaxima) {
		this.longitudMaxima = longitudMaxima;
	}
	/**
	 * @return Obtiene si el campo es numerico
	 */
	public boolean isCampoNumerico() {
		return campoNumerico;
	}
	/**
	 * Define el campo numerico a agregar
	 * @param campoNumerico BOOLEAN
	 */
	public void setCampoNumerico(boolean campoNumerico) {
		this.campoNumerico = campoNumerico;
	}
	/**
	 * @return Obtiene el campo es date
	 */
	public boolean isCampoFecha() {
		return campoFecha;
	}
	/**
	 * Define el campo date a agregar
	 * @param campoFecha void
	 */
	public void setCampoFecha(boolean campoFecha) {
		this.campoFecha = campoFecha;
	}

	@Override
	public int compareTo(PreguntaDTO o) {
		return Integer.valueOf(this.getIdPregunta()).compareTo(Integer.valueOf(o.getIdPregunta()));
	}
}