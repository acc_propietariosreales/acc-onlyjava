/**
 * Isban Mexico
 *   Clase: RecursoDTO.java
 *   Descripcion: DTO para almacenar datos de los recursos del cliente
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */


package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class RecursoDTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4706638304266301552L;
	/**
	 * clave Recurso
	 */
	private int claveRecurso;
	/**
	 * codigo Recurso
	 */
	private String codigoRecurso;
	/**
	 * descripcion
	 */
	private String descripcion;
	/**
	 * tipo Recurso
	 */
	private String tipoRecurso;
	/**
	 * tipo Persona
	 */
	private String tipoPersona;
	
	/**
	 * @return Obtiene la clave del recurso
	 */
	public int getClaveRecurso() {
		return claveRecurso;
	}
	/**
	 * Define la clave del recurso a agregar
	 * @param claveRecurso INT
	 */
	public void setClaveRecurso(int claveRecurso) {
		this.claveRecurso = claveRecurso;
	}
	/**
	 * @return Obtiene el codigo del recurso
	 */
	public String getCodigoRecurso() {
		return codigoRecurso;
	}
	/**
	 * Define el codigo del recurso a agregar
	 * @param codigoRecurso STRING
	 */
	public void setCodigoRecurso(String codigoRecurso) {
		this.codigoRecurso = codigoRecurso;
	}
	/**
	 * @return Obtiene la descripcion del recurso
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * Define la descripcion del recurso a agregar
	 * @param descripcion STRING
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return Obtiene el tipo de recurso
	 */
	public String getTipoRecurso() {
		return tipoRecurso;
	}
	/**
	 * Define el tipo de recurso a agregar
	 * @param tipoRecurso STRING
	 */
	public void setTipoRecurso(String tipoRecurso) {
		this.tipoRecurso = tipoRecurso;
	}
	/**
	 * @return Obtiene el tipo de persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * Define el tipo de persona a agregar
	 * @param tipoPersona STRING
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
}
