/**
 * Isban Mexico
 *   Clase: RequestODF3DTO.java
 *   Descripcion: Componente de transporte de datos de peticion para la transaccion ODF3.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class RequestODF3DTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4721267650912872174L;
	/**
	 * codigo Cliente
	 */
	private String codigoCliente;
	/**
	 * secuencia Domicilio
	 */
	private String secuenciaDomicilio;
	/**
	 * correo Electronico
	 */
	private String correoElectronico;
	/**
	 * time Stamp
	 */
	private String timeStamp;
	
	/**
	 * @return Obtiene el codigo del cliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}
	/**
	 * Define el codigo del cliente a agregar
	 * @param codigoCliente STRING
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	/**
	 * @return Obtiene la secuencia del domicilio
	 */
	public String getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}
	/**
	 * Define la secuencia de domicilio a agregar
	 * @param secuenciaDomicilio STRING
	 */
	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}
	/**
	 * @return Obtiene el correo electronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	/**
	 * Define el correo electronico a agregar
	 * @param correoElectronico STRING
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	/**
	 * @return Obtiene el time stamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * Define el time stamp a agreagar
	 * @param timeStamp STRING
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
}
