/**
 * 
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.exception.BusinessException;

/**
 * @author jugarcia
 *
 */
public class RequestODP1DTO implements Serializable {
	/**
	 * UID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = -1851830108556197164L;

	/**
	 * Numero de persona de la que se desea obtener la informacion
	 */
	private String numeroPersona;
	
	/**
	 * Obtiene el valor de numeroPersona
	 * @return El valor de numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * Define el nuevo valor para numeroPersona
	 * @param numeroPersona El nuevo valor de numeroPersona
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	
	/**
	 * Genera la trama de peticion ODP1
	 * @param buc8 Codigo del cliente
	 * @return Trama OPD1 sin cabecera PS7
	 * @throws BusinessException En caso de que el codigo de cliente no sea correcto
	 */
	public static String generaTramaODP1(String buc8) throws BusinessException {
		StringBuffer trama = new StringBuffer();
		
		if(!StringUtils.isNotBlank(buc8)) {
			throw new BusinessException("ERRODP101");
		}
		
		trama.append(StringUtils.leftPad(buc8, 8, "0"));
		
		return trama.toString();
	}
}
