/**
 * Isban Mexico
 *   Clase: RequestODP2DTO.java
 *   Descripcion: Componente de transporte de datos de peticion para la transaccion ODP2.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class RequestODP2DTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3561999990502326503L;
	/**
	 * Parametro para enviar el Buc o Codigo de Clinte
	 */
	private String buc;
	/**
	 * indicador
	 */
	private String indicador;
	/**
	 * valor Indicador
	 */
	private String valorIndicador;

	/**
	 * @return Obtiene el buc del cliente
	 */
	public String getBuc() {
		return buc;
	}
	/**
	 * Define el buc del cliente a agregar
	 * @param buc STRING
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	/**
	 * @return Obtiene el indicador del cliente
	 */
	public String getIndicador() {
		return indicador;
	}
	/**
	 * Define el indicador del cliente a agregar
	 * @param indicador STRING
	 */
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	/**
	 * @return Obtiene el valor del indicador
	 */
	public String getValorIndicador() {
		return valorIndicador;
	}
	/**
	 * Define el valor del indicador a agregar
	 * @param valorIndicador STRING
	 */
	public void setValorIndicador(String valorIndicador) {
		this.valorIndicador = valorIndicador;
	}
}
