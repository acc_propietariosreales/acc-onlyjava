/**
 * Isban Mexico
 *   Clase: RequestODP3DTO.java
 *   Descripcion: Componente de transporte de datos de peticion para la transaccion ODP3.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class RequestODP3DTO implements Serializable {
	/**
	 * Codigo de Cliente Indeseable
	 */
	public static final String CLIENTE_INDESEABLE = "ACL";
	/**
	 * Codigo de Cliente Nuevo
	 */
	public static final String CLIENTE_NUEVO = "NCL";
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 787833178701676948L;
	/**
	 * Parametro para enviar el Buc o Codigo de Clinte
	 */
	private String buc;
	
	/**
	 * condicion Cliente
	 */
	private String condicionCliente;
	
	/**
	 * @return Obtiene el buc del cliente
	 */
	public String getBuc() {
		return buc;
	}
	/**
	 * Define el buc del cliente a agregar
	 * @param buc STRING
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	/**
	 * @return Obtiene la condicion del cliente
	 */
	public String getCondicionCliente() {
		return condicionCliente;
	}
	/**
	 * Define la condicion del cliente a agregar
	 * @param condicionCliente STRING
	 */
	public void setCondicionCliente(String condicionCliente) {
		this.condicionCliente = condicionCliente;
	}
}
