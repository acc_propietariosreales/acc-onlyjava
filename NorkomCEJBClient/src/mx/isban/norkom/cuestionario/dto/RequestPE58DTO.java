/**
 * Isban Mexico
 *   Clase: RequestPE58DTO.java
 *   Descripcion: Componente de transporte de datos de peticion para la transaccion PE58.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class RequestPE58DTO extends PE58DTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1620571419440306207L;
	/**
	 * secuencia Documento
	 */
	private String secuenciaDocumento;
	/**
	 * secuencia Referencia
	 */
	private String secuenciaReferencia;
	/**
	 * nombre Fantasia
	 */
	private String nombreFantasia;
	/**
	 * fecha Nacimiento
	 */
	private String fechaNacimiento;
	/**
	 * estado Persona
	 */
	private String estadoPersona;
	/**
	 * condicion
	 */
	private String condicion;
	/**
	 * nivelAcceso
	 */
	private String nivelAcceso;
	/**
	 * indicador Uso Uno
	 */
	private String indicadorUsoUno;
	/**
	 * indicador Uso Dos
	 */
	private String indicadorUsoDos;
	/**
	 * indicador Uso Tres
	 */
	private String indicadorUsoTres;
	/**
	 * indicador Uso Cuatro
	 */
	private String indicadorUsoCuatro;
	/**
	 * indicador Uso Cinco
	 */
	private String indicadorUsoCinco;
	/**
	 * marca Normalizacion
	 */
	private String marcaNormalizacion;
	
	/**
	 * Direccion de la persona relacionda
	 */
	private DireccionPE58DTO direccion;
	
	/**
	 * @return Obtiene la secuencia del documento
	 */
	public String getSecuenciaDocumento() {
		return secuenciaDocumento;
	}
	/**
	 * Define la secuencia del documento a agregar
	 * @param secuenciaDocumento STRING
	 */
	public void setSecuenciaDocumento(String secuenciaDocumento) {
		this.secuenciaDocumento = secuenciaDocumento;
	}
	/**
	 * @return Obtiene la secuencia de referencia
	 */
	public String getSecuenciaReferencia() {
		return secuenciaReferencia;
	}
	/**
	 * Define la secuencia de referencia a agregar
	 * @param secuenciaReferencia STRING
	 */
	public void setSecuenciaReferencia(String secuenciaReferencia) {
		this.secuenciaReferencia = secuenciaReferencia;
	}
	/**
	 * @return Obtiene el nombre de fantasia
	 */
	public String getNombreFantasia() {
		return nombreFantasia;
	}
	/**
	 * Define el nombre de fantasia a agregar
	 * @param nombreFantasia STRING
	 */
	public void setNombreFantasia(String nombreFantasia) {
		this.nombreFantasia = nombreFantasia;
	}
	/**
	 * @return Obtiene la fecha de nacimiento
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * Define la fecha de nacimiento a agregar
	 * @param fechaNacimiento STRING
	 */
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * @return Obtiene el estado de persona
	 */
	public String getEstadoPersona() {
		return estadoPersona;
	}
	/**
	 * Define el estado de persona a agregar
	 * @param estadoPersona STRING
	 */
	public void setEstadoPersona(String estadoPersona) {
		this.estadoPersona = estadoPersona;
	}
	/**
	 * @return Obtiene la condicion
	 */
	public String getCondicion() {
		return condicion;
	}
	/**
	 * Define la condicion a agregar
	 * @param condicion STRING
	 */
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	/**
	 * @return Obtiene el nivel de acceso
	 */
	public String getNivelAcceso() {
		return nivelAcceso;
	}
	/**
	 * Define el nivel de acceso a agregar
	 * @param nivelAcceso STRING
	 */
	public void setNivelAcceso(String nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}
	/**
	 * @return Obtiene el uso del indicador uno
	 */
	public String getIndicadorUsoUno() {
		return indicadorUsoUno;
	}
	/**
	 * Define el uso del indicador uno a agregar
	 * @param indicadorUsoUno STRING
	 */
	public void setIndicadorUsoUno(String indicadorUsoUno) {
		this.indicadorUsoUno = indicadorUsoUno;
	}
	/**
	 * @return Obtiene el uso del indicador dos
	 */
	public String getIndicadorUsoDos() {
		return indicadorUsoDos;
	}
	/**
	 * Define el uso del indicador dos a agregar
	 * @param indicadorUsoDos STRING
	 */
	public void setIndicadorUsoDos(String indicadorUsoDos) {
		this.indicadorUsoDos = indicadorUsoDos;
	}
	/**
	 * @return Obtiene el uso del indicador tres
	 */
	public String getIndicadorUsoTres() {
		return indicadorUsoTres;
	}
	/**
	 * Define el uso del indicador tres a agregar
	 * @param indicadorUsoTres STRING
	 */
	public void setIndicadorUsoTres(String indicadorUsoTres) {
		this.indicadorUsoTres = indicadorUsoTres;
	}
	/**
	 * @return Obtiene el uso del indicador cuatro
	 */
	public String getIndicadorUsoCuatro() {
		return indicadorUsoCuatro;
	}
	/**
	 * Define el uso del indicador cuatro a agregar
	 * @param indicadorUsoCuatro STRING
	 */
	public void setIndicadorUsoCuatro(String indicadorUsoCuatro) {
		this.indicadorUsoCuatro = indicadorUsoCuatro;
	}
	/**
	 * @return Obtiene el uso del indicador cinco
	 */
	public String getIndicadorUsoCinco() {
		return indicadorUsoCinco;
	}
	/**
	 * Define el uso del indicador cinco a agregar
	 * @param indicadorUsoCinco STRING
	 */
	public void setIndicadorUsoCinco(String indicadorUsoCinco) {
		this.indicadorUsoCinco = indicadorUsoCinco;
	}
	/**
	 * @return Obtiene la marca de normalizacion
	 */
	public String getMarcaNormalizacion() {
		return marcaNormalizacion;
	}
	/**
	 * Define la marca de normalizacion a agregar
	 * @param marcaNormalizacion STRING
	 */
	public void setMarcaNormalizacion(String marcaNormalizacion) {
		this.marcaNormalizacion = marcaNormalizacion;
	}
	/**
	 * Obtiene la direccion de la persona
	 * @return Objeto de tipo DireccionPE58DTO
	 */
	public DireccionPE58DTO getDireccion() {
		return direccion;
	}
	/**
	 * Define la direccion de la persona
	 * @param direccion Objeto de tipo DireccionPE58DTO
	 */
	public void setDireccion(DireccionPE58DTO direccion) {
		this.direccion = direccion;
	}
}
