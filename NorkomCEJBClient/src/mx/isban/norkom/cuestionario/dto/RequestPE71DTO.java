/**
 * Isban Mexico
 *   Clase: RequestPE71DTO.java
 *   Descripcion: Componente de transporte de datos de peticion para la transaccion PE71.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class RequestPE71DTO extends PE71DTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6985988118508534435L;
	/**
	 * secuencia Documento
	 */
	private String secuenciaDocumento;
	/**
	 * nombre Fantasia
	 */
	private String nombreFantasia;
	/**
	 * fecha Constitucion
	 */
	private String fechaConstitucion;
	/**
	 * tipo Persona
	 */
	private String tipoPersona;
	/**
	 * estado Persona
	 */
	private String estadoPersona;
	/**
	 * condicion
	 */
	private String condicion;
	/**
	 * nivel Acceso
	 */
	private String nivelAcceso;
	/**
	 * tener Contratos
	 */
	private String tenerContratos;
	/**
	 * pertenenciaGrupos
	 */
	private String pertenenciaGrupos;
	/**
	 * Indicadores asignados a la persona
	 */
	private IndicadoresPE71DTO indicadores;
	/**
	 * marca Normalizacion
	 */
	private String marcaNormalizacion;
	
	/**
	 * Direccion de la persona relacionda
	 */
	private DireccionPE71DTO direccion;
	
	/**
	 * @return Obtiene la secuencia del documento
	 */
	public String getSecuenciaDocumento() {
		return secuenciaDocumento;
	}
	/**
	 * Define la secuencia del documento a agregar
	 * @param secuenciaDocumento STRING
	 */
	public void setSecuenciaDocumento(String secuenciaDocumento) {
		this.secuenciaDocumento = secuenciaDocumento;
	}
	/**
	 * @return Obtiene el nombre de la fantasia
	 */
	public String getNombreFantasia() {
		return nombreFantasia;
	}
	/**
	 * Define la nombre de fantasia a agregar
	 * @param nombreFantasia STRING
	 */
	public void setNombreFantasia(String nombreFantasia) {
		this.nombreFantasia = nombreFantasia;
	}
	/**
	 * @return Obtiene la fecha de constitucion
	 */
	public String getFechaConstitucion() {
		return fechaConstitucion;
	}
	/**
	 * Define la fecha de constitucion a agregar
	 * @param fechaConstitucion STRING
	 */
	public void setFechaConstitucion(String fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}
	/**
	 * @return Obtiene el tipo de persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * Define el tipo de persona a agregar
	 * @param tipoPersona STRING
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return Obtiene el estado de persona
	 */
	public String getEstadoPersona() {
		return estadoPersona;
	}
	/**
	 * Define el estado de persona a agregar
	 * @param estadoPersona STRING
	 */
	public void setEstadoPersona(String estadoPersona) {
		this.estadoPersona = estadoPersona;
	}
	/**
	 * @return Obtiene la condicion
	 */
	public String getCondicion() {
		return condicion;
	}
	/**
	 * Define la condicion a agregar
	 * @param condicion STRING
	 */
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	/**
	 * @return Obtiene el nivel de acceso
	 */
	public String getNivelAcceso() {
		return nivelAcceso;
	}
	/**
	 * Define el nivel de acceso a agregar
	 * @param nivelAcceso STRING
	 */
	public void setNivelAcceso(String nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}
	/**
	 * @return Obtiene los contratos
	 */
	public String getTenerContratos() {
		return tenerContratos;
	}
	/**
	 * Define los contratos a agregar
	 * @param tenerContratos STRING
	 */
	public void setTenerContratos(String tenerContratos) {
		this.tenerContratos = tenerContratos;
	}
	/**
	 * @return Obtiene la pertenecia a grupos
	 */
	public String getPertenenciaGrupos() {
		return pertenenciaGrupos;
	}
	/**
	 * Define la pertenencia a grupos a agregar
	 * @param pertenenciaGrupos STRING
	 */
	public void setPertenenciaGrupos(String pertenenciaGrupos) {
		this.pertenenciaGrupos = pertenenciaGrupos;
	}
	/**
	 * Obtiene los indicadore de la persona relacionada
	 * @return Objeto de tipo IndicadoresPE71DTO
	 */
	public IndicadoresPE71DTO getIndicadores() {
		return indicadores;
	}
	/**
	 * Define los indicadore de la persona relacionada
	 * @param indicadores Indicadores que se quieren asignar a las personas
	 */
	public void setIndicadores(IndicadoresPE71DTO indicadores) {
		this.indicadores = indicadores;
	}
	/**
	 * @return Obtiene la marca de normalizacion
	 */
	public String getMarcaNormalizacion() {
		return marcaNormalizacion;
	}
	/**
	 * Define la marca de normalizacion a agregar
	 * @param marcaNormalizacion STRING
	 */
	public void setMarcaNormalizacion(String marcaNormalizacion) {
		this.marcaNormalizacion = marcaNormalizacion;
	}
	/**
	 * Obtiene la direccion de la persona
	 * @return Objeto de tipo DireccionPE58DTO
	 */
	public DireccionPE71DTO getDireccion() {
		return direccion;
	}
	/**
	 * Define la direccion de la persona
	 * @param direccion Objeto de tipo DireccionPE58DTO
	 */
	public void setDireccion(DireccionPE71DTO direccion) {
		this.direccion = direccion;
	}
}
