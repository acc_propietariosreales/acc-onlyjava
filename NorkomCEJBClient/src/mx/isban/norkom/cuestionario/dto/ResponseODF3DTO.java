/**
 * Isban Mexico
 *   Clase: ResponseODF3DTO.java
 *   Descripcion: Componente de transporte de datos de respuesta para la transaccion ODF3.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class ResponseODF3DTO implements Serializable {
	/**
	 * OPERACION_EXITOSA
	 */
	public static final String OPERACION_EXITOSA = "PEA0000";
	/**
	 * Copy de la transaccion ODF3
	 */
	public static final String COPY_TRANSACCION = "DCODSCFT3 P";
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3210715666258119225L;	
	/**
	 * codigoCliente
	 */
	private String codigoCliente;
	/**
	 * secuenciaDomicilio
	 */
	private String secuenciaDomicilio;
	/**
	 * correoElectronico
	 */
	private String correoElectronico;
	/**
	 * timeStamp
	 */
	private String timeStamp;
	
	/**
	 * @return Obtiene el codigo del cliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}
	/**
	 * Define el codigo del cliente a agregar
	 * @param codigoCliente STRING
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	/**
	 * @return Obtiene la secuencia del domicilio
	 */
	public String getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}
	/**
	 * Define la secuencia del domicilio a agregar
	 * @param secuenciaDomicilio STRING
	 */
	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}
	/**
	 * @return Obtiene el correo electronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	/**
	 * Define el correo electronico a agregar
	 * @param correoElectronico STRING
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	/**
	 * @return Obtiene el time stamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * Define el time stamp agregar
	 * @param timeStamp STRING
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
}
