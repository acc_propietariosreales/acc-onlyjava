/**
 * 
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 *
 */
public class ResponseODP1DTO implements Serializable {
	/**
	 * Codigo de operacion exitosa
	 */
	public static final String OPERACION_EXITOSA = "ODA0029";
	
	
	/**
	 * UID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = 8482187146246578752L;
	
	/**
	 * Codigo de Error
	 */
	private String codigoOperacion;
	
	/**
	 * Descripcion del Error
	 */
	private String msgOperacion;
	
	/**
	 * Numero de persona de la que se solicito informacion
	 */
	private String numeroPersona;
	
	/**
	 * Indicador del Nivel Riesgo asignado
	 */
	private String indicadorNivelRiesgo;
	
	/**
	 * Valor del Nivel Riesgo asignado
	 */
	private String valorNivelRiesgo;
	
	/**
	 * Indicador UPLD asignado
	 */
	private String indicadorUpld;
	
	/**
	 * Valor del Indicador UPLD asignado
	 */
	private String valorUpld;
	
	/**
	 * Obtiene el valor de codigoOperacion
	 * @return El valor de codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * Define el nuevo valor para codigoOperacion
	 * @param codigoOperacion El nuevo valor de codigoOperacion
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	/**
	 * Obtiene el valor de msgOperacion
	 * @return El valor de msgOperacion
	 */
	public String getMsgOperacion() {
		return msgOperacion;
	}

	/**
	 * Define el nuevo valor para msgOperacion
	 * @param msgOperacion El nuevo valor de msgOperacion
	 */
	public void setMsgOperacion(String msgOperacion) {
		this.msgOperacion = msgOperacion;
	}

	/**
	 * Obtiene el valor de numeroPersona
	 * @return El valor de numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * Define el nuevo valor para numeroPersona
	 * @param numeroPersona El nuevo valor de numeroPersona
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

	/**
	 * Obtiene el valor de indicadorNivelRiesgo
	 * @return El valor de indicadorNivelRiesgo
	 */
	public String getIndicadorNivelRiesgo() {
		return indicadorNivelRiesgo;
	}

	/**
	 * Define el nuevo valor para indicadorNivelRiesgo
	 * @param indicadorNivelRiesgo El nuevo valor de indicadorNivelRiesgo
	 */
	public void setIndicadorNivelRiesgo(String indicadorNivelRiesgo) {
		this.indicadorNivelRiesgo = indicadorNivelRiesgo;
	}

	/**
	 * Obtiene el valor de valorNivelRiesgo
	 * @return El valor de valorNivelRiesgo
	 */
	public String getValorNivelRiesgo() {
		return valorNivelRiesgo;
	}

	/**
	 * Define el nuevo valor para valorNivelRiesgo
	 * @param valorNivelRiesgo El nuevo valor de valorNivelRiesgo
	 */
	public void setValorNivelRiesgo(String valorNivelRiesgo) {
		this.valorNivelRiesgo = valorNivelRiesgo;
	}

	/**
	 * Obtiene el valor de indicadorUpld
	 * @return El valor de indicadorUpld
	 */
	public String getIndicadorUpld() {
		return indicadorUpld;
	}

	/**
	 * Define el nuevo valor para indicadorUpld
	 * @param indicadorUpld El nuevo valor de indicadorUpld
	 */
	public void setIndicadorUpld(String indicadorUpld) {
		this.indicadorUpld = indicadorUpld;
	}

	/**
	 * Obtiene el valor de valorUpld
	 * @return El valor de valorUpld
	 */
	public String getValorUpld() {
		return valorUpld;
	}

	/**
	 * Define el nuevo valor para valorUpld
	 * @param valorUpld El nuevo valor de valorUpld
	 */
	public void setValorUpld(String valorUpld) {
		this.valorUpld = valorUpld;
	}
}
