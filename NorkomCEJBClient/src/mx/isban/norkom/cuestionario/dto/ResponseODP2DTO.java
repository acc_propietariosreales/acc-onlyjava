/**
 * Isban Mexico
 *   Clase: ResponseODP2DTO.java
 *   Descripcion: Componente de transporte de datos de respuesta para la transaccion ODP2.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class ResponseODP2DTO implements Serializable {
	
	/**
	 * Codigo de operacion exitosa
	 */
	public static final String OPERACION_EXITOSA = "ODA0002";
	
	/**
	 * Codigo de operacion exitosa
	 */
	public static final String OPERACION_VALIDA = "ODA0049";
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 790497633523979834L;
	/**
	 * Codigo de Error
	 */
	private String codigoOperacion;
	
	/**
	 * Descripcion del Error
	 */
	private String msgOperacion;

	/**
	 * @return Obtiene el codigo de operacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * Define el codigo de la operacion a agregar
	 * @param codigoOperacion STRING
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	/**
	 * @return Obtiene el mensaje de la operacion
	 */
	public String getMsgOperacion() {
		return msgOperacion;
	}

	/**
	 * Define el mensaje de la operacion a agregar
	 * @param msgOperacion STRING
	 */
	public void setMsgOperacion(String msgOperacion) {
		this.msgOperacion = msgOperacion;
	}
}
