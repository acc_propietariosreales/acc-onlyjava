/**
 * Isban Mexico
 *   Clase: ResponsePE58DTO.java
 *   Descripcion: Componente de transporte de datos de respuesta para la transaccion PE58.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class ResponsePE58DTO extends PE58DTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6161988453494794892L;
	/**
	 * numero de Secuencia
	 */
	private String numeroSecuencia;
	/**
	 * tipo de Relacion
	 */
	private String tipoRelacion;
	/**
	 * prefijo de Telefono
	 */
	private String prefijoTelefono;
	/**
	 * numero de Telefono
	 */
	private String numeroTelefono;
	/**
	 * los anexos
	 */
	private String anexos;
	/**
	 * correo Electronico
	 */
	private String correoElectronico;
	/**
	 * secuencia Domicilio
	 */
	private String secuenciaDomicilio;
	/**
	 * time Stamp
	 */
	private String timeStamp;
	/**
	 * observaciones Tres
	 */
	private String observacionesTres;
	/**
	 * @return Obtiene el numero secuencial
	 */
	public String getNumeroSecuencia() {
		return numeroSecuencia;
	}
	/**
	 * Define el numero secuencial a agregar
	 * @param numeroSecuencia STRING
	 */
	public void setNumeroSecuencia(String numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}
	/**
	 * @return Obtiene el tipo de relacion
	 */
	public String getTipoRelacion() {
		return tipoRelacion;
	}
	/**
	 * Define el tipo de relacion a agregar
	 * @param tipoRelacion STRING
	 */
	public void setTipoRelacion(String tipoRelacion) {
		this.tipoRelacion = tipoRelacion;
	}
	/**
	 * @return Obtiene el telefono completo
	 */
	public String getTelefonoCompleto() {
		return String.format("%s%s", getPrefijoTelefono(), getNumeroTelefono());
	}
	
	/**
	 * @return Obtiene el prefijo del telefono
	 */
	public String getPrefijoTelefono() {
		if(prefijoTelefono == null) {
			return StringUtils.EMPTY;
		}
		
		return prefijoTelefono;
	}
	/**
	 * Define el prefijo del telefono a agregar
	 * @param prefijoTelefono STRING
	 */
	public void setPrefijoTelefono(String prefijoTelefono) {
		this.prefijoTelefono = prefijoTelefono;
	}
	/**
	 * @return Obtiene el numero de telefono
	 */
	public String getNumeroTelefono() {
		if(numeroTelefono == null) {
			return StringUtils.EMPTY;
		}
		
		return numeroTelefono;
	}
	/**
	 * Define el numero de telefono a agregar
	 * @param numeroTelefono STRING
	 */
	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	/**
	 * @return Obtiene los anexos
	 */
	public String getAnexos() {
		return anexos;
	}
	/**
	 * Define los anexos a agregar
	 * @param anexos STRING
	 */
	public void setAnexos(String anexos) {
		this.anexos = anexos;
	}
	/**
	 * @return Obtiene el correo electronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	/**
	 * Define el correo electronico a agregar
	 * @param correoElectronico STRING
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	/**
	 * @return Obtiene la secuencia del domicilio
	 */
	public String getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}
	/**
	 * Define la secuencia del domicilio a agregar
	 * @param secuenciaDomicilio STRING
	 */
	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}
	/**
	 * @return Obtiene el time stamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * Define el time stamp a agregar
	 * @param timeStamp STRING
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return Obtiene las observaciones
	 */
	public String getObservacionesTres() {
		return observacionesTres;
	}
	/**
	 * Define las observaciones a agregar
	 * @param observacionesTres STRING
	 */
	public void setObservacionesTres(String observacionesTres) {
		this.observacionesTres = observacionesTres;
	}	
}