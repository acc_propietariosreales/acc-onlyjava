/**
 * Isban Mexico
 *   Clase: ResponsePE71DTO.java
 *   Descripcion: Componente de transporte de datos de respuesta para la transaccion PE71.
 *
 *   Control de Cambios:
 *   1.0 Ene 05, 2015 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

public class ResponsePE71DTO extends PE71DTO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1179268036643048109L;
	
	/**
	 * tipo Contacto
	 */
	private String tipoContacto;
	/**
	 * accesibilidad de Contacto
	 */
	private String accesibilidadContacto;
	/**
	 * marca Principal
	 */
	private String marcaPpal;
	/**
	 * nombre de Secretaria
	 */
	private String nombreSecretaria;
	/**
	 * cargo de Contacto
	 */
	private String cargoContacto;
	/**
	 * departamento de Contacto
	 */
	private String departamentoContacto;
	/**
	 * referencia de Domicilio
	 */
	private String referenciaDomicilio;
	/**
	 * prefijo de Telefono
	 */
	private String prefijoTelefono;
	/**
	 * numero de Telefono
	 */
	private String numeroTelefono;
	/**
	 * numero Interno Externo
	 */
	private String numeroInternoExterno;
	/**
	 * correo de Contacto
	 */
	private String correoContacto;
	/**
	 * marca del Cliente
	 */
	private String marcaCliente;
	/**
	 * observaciones Tres
	 */
	private String observacionesTres;
	
	/**
	 * @return Obtiene el tipo de contrato
	 */
	public String getTipoContacto() {
		return tipoContacto;
	}
	/**
	 * Define el tipo de contrato a agregar
	 * @param tipoContacto STRING
	 */
	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}
	/**
	 * @return Obtiene la accesibilidad del contacto
	 */
	public String getAccesibilidadContacto() {
		return accesibilidadContacto;
	}
	/**
	 * Define la accesibilidad del contacto a agregar
	 * @param accesibilidadContacto STRING
	 */
	public void setAccesibilidadContacto(String accesibilidadContacto) {
		this.accesibilidadContacto = accesibilidadContacto;
	}
	/**
	 * @return Obtiene la marcaPpal
	 */
	public String getMarcaPpal() {
		return marcaPpal;
	}
	/**
	 * Define la marcaPpal a agregar
	 * @param marcaPpal STRING
	 */
	public void setMarcaPpal(String marcaPpal) {
		this.marcaPpal = marcaPpal;
	}
	/**
	 * @return Obtiene el nombre de la secretaria
	 */
	public String getNombreSecretaria() {
		return nombreSecretaria;
	}
	/**
	 * Define el nombre de la secretaria a agregar
	 * @param nombreSecretaria STRING
	 */
	public void setNombreSecretaria(String nombreSecretaria) {
		this.nombreSecretaria = nombreSecretaria;
	}
	/**
	 * @return Obtiene el cargo del contacto
	 */
	public String getCargoContacto() {
		return cargoContacto;
	}
	/**
	 * Define el cargo del contacto a agregar
	 * @param cargoContacto STRING
	 */
	public void setCargoContacto(String cargoContacto) {
		this.cargoContacto = cargoContacto;
	}
	/**
	 * @return Obtiene el departamento del contacto
	 */
	public String getDepartamentoContacto() {
		return departamentoContacto;
	}
	/**
	 * Define el departamento del contacto a agregar
	 * @param departamentoContacto STRING
	 */
	public void setDepartamentoContacto(String departamentoContacto) {
		this.departamentoContacto = departamentoContacto;
	}
	/**
	 * @return Obtiene la referencia del domicilio
	 */
	public String getReferenciaDomicilio() {
		return referenciaDomicilio;
	}
	/**
	 * Define el domicilio a agregar
	 * @param referenciaDomicilio STRING
	 */
	public void setReferenciaDomicilio(String referenciaDomicilio) {
		this.referenciaDomicilio = referenciaDomicilio;
	}
	/**
	 * @return Obtiene el prefijo del telefono
	 */
	public String getPrefijoTelefono() {
		return prefijoTelefono;
	}
	/**
	 * Define el prefijo del telefono a agregar
	 * @param prefijoTelefono STRING
	 */
	public void setPrefijoTelefono(String prefijoTelefono) {
		this.prefijoTelefono = prefijoTelefono;
	}
	/**
	 * @return Obtiene el numero de telefonico
	 */
	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	/**
	 * Define el numero telefonico a agregar
	 * @param numeroTelefono STRING
	 */
	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	/**
	 * @return Obtiene el numero de telefono interno o externo
	 */
	public String getNumeroInternoExterno() {
		return numeroInternoExterno;
	}
	/**
	 * Define el numero interno o externo a agregar
	 * @param numeroInternoExterno STRING
	 */
	public void setNumeroInternoExterno(String numeroInternoExterno) {
		this.numeroInternoExterno = numeroInternoExterno;
	}
	/**
	 * @return Obtiene el correo del contacto
	 */
	public String getCorreoContacto() {
		return correoContacto;
	}
	/**
	 * Define el correo del contacto a agregar
	 * @param correoContacto STRING
	 */
	public void setCorreoContacto(String correoContacto) {
		this.correoContacto = correoContacto;
	}
	/**
	 * @return Obtiene la marca del cliente
	 */
	public String getMarcaCliente() {
		return marcaCliente;
	}
	/**
	 * Define la marca del cliente a agregar
	 * @param marcaCliente STRING
	 */
	public void setMarcaCliente(String marcaCliente) {
		this.marcaCliente = marcaCliente;
	}
	/**
	 * @return Obtiene las observaciones
	 */
	public String getObservacionesTres() {
		return observacionesTres;
	}
	/** Define las observaciones a agregar
	 * @param observacionesTres STRING
	 */
	public void setObservacionesTres(String observacionesTres) {
		this.observacionesTres = observacionesTres;
	}
}