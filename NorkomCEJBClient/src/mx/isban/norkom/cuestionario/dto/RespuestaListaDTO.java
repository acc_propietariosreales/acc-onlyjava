/**
 * NorkomCEJBClient RespuestaListaDTO.java
 *   Clase: RespuestaListaDTO.java
 *   Descripcion: Componente para contener las listas de respuestas.
 *
 *   Control de Cambios:
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Leopoldo F Espinosa R 01/04/2015
 *
 */
public class RespuestaListaDTO implements Serializable{

	/**
	 * RespuestaListaDTO.java  de tipo long
	 */
	private static final long serialVersionUID = -7624448895086234826L;
	/***lista de datos de respuestas**/
	private List<String> datos;



	/**
	 * @param datos 
	 * Define los datos a agregar
	 */
	public void setDatos(List<String> datos) {
		this.datos = datos;
	}



	/**
	 * @return Obtiene los datos
	 */
	public List<String> getDatos() {
		return datos;
	}
	
	
}
