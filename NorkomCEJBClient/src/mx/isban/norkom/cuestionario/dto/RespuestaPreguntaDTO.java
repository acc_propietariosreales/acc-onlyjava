/**
 * Isban Mexico
 *   Clase: PreguntaDTO.java
 *   Descripcion: DTO para almacenar preguntas del cuestionario
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */


package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author Leopoldo F Espinosa R 26/11/2014
 *
 */
public class RespuestaPreguntaDTO implements Serializable {
	/**
	 * Id de version utilizado para la serializacion del objeto
	 */
	private static final long serialVersionUID = -7938503586476652575L;

	/**
	 * Llave primaria de la tabla NKM_MX_PRC_RESP_PREG_DRO
	 */
	protected int claveRespuestaPregunta;
	
	/**REspuesta*/
	protected String respuesta;
	/**
	 * Opcion de la respuesta seleccionada cuando se trata de un RADIO o un SELECT
	 */
	private int opcionSeleccionada;
	/**
	 * @return Obtiene la respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}
	/**
	 * Define la respuesta a agregar
	 * @param respuesta la respuesta
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	/**
	 * Obtiene la opcion respuesta seleccionada de una pregunta de tipo Radio o Select 
	 * @return La opcion seleccionda
	 */
	public int getOpcionSeleccionada() {
		return opcionSeleccionada;
	}

	/**
	 * Define la opcion de respuesta seleccionada de una pregunta de tipo Radio o Select
	 * @param opcionSeleccionada El nuevo valor de la respuesta
	 */
	public void setOpcionSeleccionada(int opcionSeleccionada) {
		this.opcionSeleccionada = opcionSeleccionada;
	}
	/**
	 * Obtiene el valor de claveRespuestaPregunta
	 * @return El valor de claveRespuestaPregunta
	 */
	public int getClaveRespuestaPregunta() {
		return claveRespuestaPregunta;
	}
	/**
	 * Define el nuevo valor para claveRespuestaPregunta
	 * @param claveRespuestaPregunta El nuevo valor de claveRespuestaPregunta
	 */
	public void setClaveRespuestaPregunta(int claveRespuestaPregunta) {
		this.claveRespuestaPregunta = claveRespuestaPregunta;
	}
	
	
}
