/**
 * Isban Mexico
 *   Clase: RespuestasRes.java
 *   Descripcion: Response de AdministradorCuestionarioService, almacena cuestionarios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;
import java.util.List;

import mx.isban.norkom.ws.dto.RelacionadoDTO;

/**
 * @author Miguel Angel Ayala Franco
 *
 */
public class RespuestasRes implements Serializable {

	/**
	 * ID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = 1872222791155639625L;

	/**
	 * Codigo de operacion
	 */
	private String codOperacion;
	
	/**
	 * Descripcion de operacion
	 */
	private String descOperacion;
	
	/**
	 * Preguntas correspondientes al encabezado IP
	 */
	private List<PreguntaDTO> encabezadoIP;
	
	/**
	 * lista de preguntasIP
	 */
	private List<PreguntaDTO> preguntasIP;

	/**
	 * Preguntas correspondientes al encabezado IC
	 */
	private List<PreguntaDTO> encabezadoIC;
	
	/**
	 * lista de preguntasIC
	 */
	private List<PreguntaDTO> preguntasIC;

	/**
	 * lista de preguntasIP
	 */
	private List<PreguntaDTO> familiares;

	/**
	 * lista de preguntasIP
	 */
	private List<RelacionadoDTO> relacionados;

	
	/**
	 * @return Obtiene el codigo de Operacion
	 */
	public String getCodOperacion() {
		return codOperacion;
	}

	/**
	 * Define el codigo de Operacion a egregar
	 * @param codOperacion el codOperacion
	 */
	public void setCodOperacion(String codOperacion) {
		this.codOperacion = codOperacion;
	}

	/**
	 * @return Obtiene la descripcion de la Operacion
	 */
	public String getDescOperacion() {
		return descOperacion;
	}

	/**
	 * Define la descripcion de la Operacion a agregar
	 * @param descOperacion la descripcion de Operacion a establecer
	 */
	public void setDescOperacion(String descOperacion) {
		this.descOperacion = descOperacion;
	}

	/**
	 * @return Obtiene las preguntas de cuestionario IP
	 */
	public List<PreguntaDTO> getPreguntasIP() {
		return preguntasIP;
	}

	/**
	 * Define las preguntas de cuestionario IP
	 * @param preguntasIP preguntas de cuestionario IP a establecer
	 */
	public void setPreguntasIP(List<PreguntaDTO> preguntasIP) {
		this.preguntasIP = preguntasIP;
	}

	/**
	 * @return Obtiene las preguntas del cuestionariio IC
	 */
	public List<PreguntaDTO> getPreguntasIC() {
		return preguntasIC;
	}

	/**
	 * Define las preguntas de cuestionario IC
	 * @param preguntasIC las preguntas del cuestionaroio IC a establecer
	 */
	public void setPreguntasIC(List<PreguntaDTO> preguntasIC) {
		this.preguntasIC = preguntasIC;
	}


	/**
	 * Obtiene los Relacionados asignados
	 * @return Lista de relacionados
	 */
	public List<RelacionadoDTO> getRelacionados() {
		return relacionados;
	}

	/**
	 * Define los Relacionados al cuestionario
	 * @param relacionados Los relacionados a establecer
	 */
	public void setRelacionados(List<RelacionadoDTO> relacionados) {
		this.relacionados = relacionados;
	}

	/**
	 * @return Obtiene los familiares asignados
	 */
	public List<PreguntaDTO> getFamiliares() {
		return familiares;
	}

	/**
	 * Define los familiares a agregar
	 * @param familiares los familiares a establecer
	 */
	public void setFamiliares(List<PreguntaDTO> familiares) {
		this.familiares = familiares;
	}

	/**
	 * @return Obtiene el encabezado del cuestionario IP
	 */
	public List<PreguntaDTO> getEncabezadoIP() {
		return encabezadoIP;
	}

	/**
	 * Define el encabezadoIP a agregar
	 * @param encabezadoIP 
	 */
	public void setEncabezadoIP(List<PreguntaDTO> encabezadoIP) {
		this.encabezadoIP = encabezadoIP;
	}

	/**
	 * @return Obtiene el encabezado del cuestionario IC
	 */
	public List<PreguntaDTO> getEncabezadoIC() {
		return encabezadoIC;
	}

	/**
	 * Define el encabezadoIC a agregar
	 * @param encabezadoIC 
	 */
	public void setEncabezadoIC(List<PreguntaDTO> encabezadoIC) {
		this.encabezadoIC = encabezadoIC;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
