/**
 * Isban Mexico
 *   Clase: SegmentoDTO.java
 *   Descripcion: DTO para almacenar datos del segmento
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/** 
 * 
 * @author Stefanini (Jose Manuel Gonzalez Quillo) 10/11/2014
 *
 */

public class SegmentoDTO implements Serializable {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 202425082089631380L;

	/**
	 * int id de Segmento
	 */
	private int idSegmento;
	/**
	 * String codigo de segmento;
	 */
	private String codigo;
	/**
	 * String descripcion de segmento;
	 */
	private String descripcion;
	/**
	 * Define el id del Segmento a agregar
	 * @param idSegmento 
	 */
	public void setIdSegmento(int idSegmento) {
		this.idSegmento = idSegmento;
	}
	/**
	 * @return Obtiene el id del Segmento
	 */
	public int getIdSegmento() {
		return idSegmento;
	}
	/**
	 * Define el codigo a agregar
	 * @param codigo 
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return Obtiene el codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * Define la descripcion a agregar
	 * @param descripcion 
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return Obtiene la descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}	
}
