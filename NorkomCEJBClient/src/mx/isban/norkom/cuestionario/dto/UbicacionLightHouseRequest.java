/**
 * 
 */
package mx.isban.norkom.cuestionario.dto;

import java.io.Serializable;

/**
 * @author jugarcia
 *
 */
public class UbicacionLightHouseRequest implements Serializable {
	/**
	 * UID utilizado en la serializacion del objeto
	 */
	private static final long serialVersionUID = 6755073922670479211L;
	/** Codigo de sucursal */
	private String codigoSucursal;
	/** Descripcion de sucursal */
	private String descripcionSucursal;
	/** Codigo de plaza */
	private String codigoPlaza;
	/** Descripcion de plaza */
	private String descripcionPlaza;
	/** Codigo de zona */
	private String codigoZona;
	/** Descripcion de zona */
	private String descripcionZona;
	/** Codigo de region */
	private String codigoRegion;
	/** Descripcion de region */
	private String descripcionRegion;
	/** Codigo de segmento */
	private String codigoSegmento;
	/** Descripcion de segmento */
	private String descSegmento;
	
	/**
	 * Obtiene el valor de codigoSucursal
	 * @return El valor de codigoSucursal
	 */
	public String getCodigoSucursal() {
		return codigoSucursal;
	}
	/**
	 * Define el nuevo valor para codigoSucursal
	 * @param codigoSucursal El nuevo valor de codigoSucursal
	 */
	public void setCodigoSucursal(String codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}
	/**
	 * Obtiene el valor de nombreSucursal
	 * @return El valor de nombreSucursal
	 */
	public String getDescripcionSucursal() {
		return descripcionSucursal;
	}
	/**
	 * Define el nuevo valor para nombreSucursal
	 * @param descripcionSucursal El nuevo valor de nombreSucursal
	 */
	public void setDescripcionSucursal(String descripcionSucursal) {
		this.descripcionSucursal = descripcionSucursal;
	}
	/**
	 * Obtiene el valor de codigoPlaza
	 * @return El valor de codigoPlaza
	 */
	public String getCodigoPlaza() {
		return codigoPlaza;
	}
	/**
	 * Define el nuevo valor para codigoPlaza
	 * @param codigoPlaza El nuevo valor de codigoPlaza
	 */
	public void setCodigoPlaza(String codigoPlaza) {
		this.codigoPlaza = codigoPlaza;
	}
	/**
	 * Obtiene el valor de nombrePlaza
	 * @return El valor de nombrePlaza
	 */
	public String getDescripcionPlaza() {
		return descripcionPlaza;
	}
	/**
	 * Define el nuevo valor para nombrePlaza
	 * @param descripcionPlaza El nuevo valor de nombrePlaza
	 */
	public void setDescripcionPlaza(String descripcionPlaza) {
		this.descripcionPlaza = descripcionPlaza;
	}
	/**
	 * Obtiene el valor de codigoZona
	 * @return El valor de codigoZona
	 */
	public String getCodigoZona() {
		return codigoZona;
	}
	/**
	 * Define el nuevo valor para codigoZona
	 * @param codigoZona El nuevo valor de codigoZona
	 */
	public void setCodigoZona(String codigoZona) {
		this.codigoZona = codigoZona;
	}
	/**
	 * Obtiene el valor de nombreZona
	 * @return El valor de nombreZona
	 */
	public String getDescripcionZona() {
		return descripcionZona;
	}
	/**
	 * Define el nuevo valor para nombreZona
	 * @param descripcionZona El nuevo valor de nombreZona
	 */
	public void setDescripcionZona(String descripcionZona) {
		this.descripcionZona = descripcionZona;
	}
	/**
	 * Obtiene el valor de codigoRegion
	 * @return El valor de codigoRegion
	 */
	public String getCodigoRegion() {
		return codigoRegion;
	}
	/**
	 * Define el nuevo valor para codigoRegion
	 * @param codigoRegion El nuevo valor de codigoRegion
	 */
	public void setCodigoRegion(String codigoRegion) {
		this.codigoRegion = codigoRegion;
	}
	/**
	 * Obtiene el valor de nombreRegion
	 * @return El valor de nombreRegion
	 */
	public String getDescripcionRegion() {
		return descripcionRegion;
	}
	/**
	 * Define el nuevo valor para nombreRegion
	 * @param descripcionRegion El nuevo valor de nombreRegion
	 */
	public void setDescripcionRegion(String descripcionRegion) {
		this.descripcionRegion = descripcionRegion;
	}
	/**
	 * Obtiene el valor de codigoSegmento
	 * @return El valor de codigoSegmento
	 */
	public String getCodigoSegmento() {
		return codigoSegmento;
	}
	/**
	 * Define el nuevo valor para codigoSegmento
	 * @param codigoSegmento El nuevo valor de codigoSegmento
	 */
	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}
	/**
	 * Obtiene el valor de descSegmento
	 * @return El valor de descSegmento
	 */
	public String getDescSegmento() {
		return descSegmento;
	}
	/**
	 * Define el nuevo valor para descSegmento
	 * @param descSegmento El nuevo valor de descSegmento
	 */
	public void setDescSegmento(String descSegmento) {
		this.descSegmento = descSegmento;
	}
}
