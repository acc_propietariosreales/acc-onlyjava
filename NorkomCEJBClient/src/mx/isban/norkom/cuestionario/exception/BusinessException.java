/**
 * NorkomCEJBClient BusinessException.java
 * Isban Mexico
 *   Clase: BusinessException.java
 *   Descripcion: Componente para el manejo de errores que reciba el Exception para no perderse
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.exception;

/**
 * @author Leopoldo F Espinosa R 17/03/2015
 *
 */
public class BusinessException extends	mx.isban.agave.commons.exception.BusinessException {

	  /**
	 * BusinessException.java  de tipo long
	 */
	private static final long serialVersionUID = 2910856411286123225L;

	/**
	 * 
	 * @param code mensaje de error
	 * @param e exception para que nos e pierda
	 */
	public BusinessException(String code,Exception e)
	  {
	    super("", code, e!=null?e.getMessage():"default");
	  }
	/**
	 * @param code codigo de error
	 * @param message mensaje de error
	 * @param e exception para que nos e pierda
	 */
	  public BusinessException(String code, String message,Exception e) {
	    super("", code, message!=null?message:e.getMessage());
	  }
		/**
		 * @param nameClass nombre de la clase
		 * @param code codigo de error
		 * @param message mensaje de error
		 * @param e exception para que nos e pierda
		 */
	  public BusinessException(String nameClass, String code, String message,Exception e) {
	    super(nameClass, code,message+ e!=null?"-"+e.getMessage():"-default");
	  }
}
