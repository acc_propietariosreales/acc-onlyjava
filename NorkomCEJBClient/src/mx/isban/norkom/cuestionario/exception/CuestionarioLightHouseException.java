/**
 * 
 */
package mx.isban.norkom.cuestionario.exception;

import mx.isban.agave.commons.exception.BaseException;

/**
 * @author jugarcia
 * Objetivo: Manejar los errores durante el flujo de la generacion del cuestionario y obtencion de calificacion.
 * Justificacion: Acotar los errores que tengan que ver con la generacion del cuestionario y obtencion de calificacion para de esta manera darles un tratamiento especifico.
 */
public class CuestionarioLightHouseException extends BaseException {
	/**
	 * UID generado automaticamente para la serializacion del objeto 
	 */
	private static final long serialVersionUID = 7795508598262695839L;
	/**
	 * 
	 * @param code mensaje de error
	 * @param e exception para que nos e pierda
	 */
	public CuestionarioLightHouseException(String code,Exception e) {
	    super("", code, e!=null?e.getMessage():"default");
	}
	
	/**
	 * @param code codigo de error
	 * @param message mensaje de error
	 * @param e exception para que nos e pierda
	 */
	  public CuestionarioLightHouseException(String code, String message,Exception e) {
		  super("", code, message!=null?message:e.getMessage());
	  }
	/**
	 * @param nameClass nombre de la clase
	 * @param code codigo de error
	 * @param message mensaje de error
	 * @param e exception para que nos e pierda
	 */
	  public CuestionarioLightHouseException(String nameClass, String code, String message,Exception e) {
		  super(nameClass, code,message+ e!=null?"-"+e.getMessage():"-default");
	  }
}
