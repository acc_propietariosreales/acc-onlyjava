/**
 * 
 */
package mx.isban.norkom.cuestionario.exception;


/**
 * @author J Ulises Garcia Mtz
 * Objetivo: Manejar los errores durante el flujo de la obtencion de la llaves privada y publica
 * Justificacion: Acotar los errores que tengan que ver con el manejo de las llaves privadas y publicas
 */
public class GestorClavesException extends Exception {

	/** Variable para serializar el objeto */
	private static final long serialVersionUID = -8233446924260527682L;
	
	/** Variable para definir un codigo de error */
	private String codigo;

	/**
	 * Constructor por defecto
	 * @param e Error original
	 * @param message Mensaje de error
	 */
	public GestorClavesException(String message, Throwable e) {
		super(message, e);
	}

	/**
	 * Excepcion con mensaje y codigo de error
	 * @param mensaje Mensaje de error
	 * @param codigo Codigo de error
	 */
	public GestorClavesException(String mensaje, String codigo) {
		super(mensaje);
		this.codigo = codigo;
	}

	/**
	 * Obtiene el codigo de error
	 * @return El codigo de error
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Define el nuevo valor para codigo
	 * @param codigo El nuevo valor de codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
