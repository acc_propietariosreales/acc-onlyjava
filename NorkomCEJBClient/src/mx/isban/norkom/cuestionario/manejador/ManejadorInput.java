/**
 * Isban Mexico
 *   Clase: ManejadorInput.java
 *   Descripcion: Manejador de preguntas tipo texto/libres
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.manejador;

import org.apache.commons.lang.StringUtils;

import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;

public final class ManejadorInput {
	/**
	 * String ID_INPUT_TEXT
	 */
	public static final String ID_INPUT_TEXT = "txtPreg";
	/**
	 * String INPUT_TEXT
	 */
	private static final String INPUT_TEXT = "<input type='text' maxlength='%s' size='%s' id='%s' name='%s' %s %s class='%s' value='%s'>";
	/**
	 * String FUNC_VALIDA_ALFANUMERICO
	 */
	private static final String FUNC_VALIDA_ALFANUMERICO = " onkeypress = 'return validaLetrasNumerosEspEvent(event)' ";
	/**
	 * String FUNC_VALIDA_CORREO
	 */
	private static final String FUNC_VALIDA_CORREO = " onblur = 'return validaFormatoCorreo(this)' ";
	/**
	 * String FUNC_VALIDA_DECIMALES_NUMERICO
	 */
	private static final String FUNC_VALIDA_DECIMALES_NUMERICO = " onkeypress = 'return validaNumerosDecimalesEvent(event)' onBlur='validaPuntos(this.id)' ";
	/**
	 * String FUNC_VALIDA_ENTEROS_NUMERICO
	 */
	private static final String FUNC_VALIDA_ENTEROS_NUMERICO = " onkeypress = 'return validaNumerosEnterosEvent(event)' onBlur='validaPuntos(this.id)' ";
	/**
	 * String PROPIEDAD_READ_ONLY
	 */
	private static final String PROPIEDAD_READ_ONLY = " readonly='readonly' ";
	/**
	 * String PROPIEDAD_MAX_LENGTH
	 */
	private static final int MAX_LENGTH_DEFAULT = 80;
	/**
	 * String LONG_MAXIMO_FECHA
	 */
	private static final int LONG_MAXIMO_FECHA = 10;
	
	/**
	 * Porcentaje de incremento para el tamaño de los INPUT con base a su tamaño maximo permitido
	 */
	private static final float INCREMENTO_SIZE = 1.1f;
	/***
	 * ManejadorInput constructor
	 */
	private ManejadorInput() {

	}
	/**
	 * @param preguntaDTO objeto de preguntas
	 * @param opciones atributos de la opcion
	 * @param valorTexto valor de la pregunta
	 * @return StringBuilder cadena con la pregunta libre
	 */
	public static StringBuilder generarCampoLibre(PreguntaDTO preguntaDTO, AtributosTagHtmlDTO opciones, String valorTexto){
		StringBuilder inputHtml = new StringBuilder();
		String idInput = String.format("%s%s", ID_INPUT_TEXT, preguntaDTO.getAbreviatura());
		int longitudMaxima = preguntaDTO.getLongitudMaxima() == 0 ? MAX_LENGTH_DEFAULT : preguntaDTO.getLongitudMaxima();
		
		if(valorTexto == null) {
			valorTexto = StringUtils.EMPTY;
		}
		
		if(preguntaDTO.isCampoFecha()) {
			inputHtml.append(String.format(INPUT_TEXT, LONG_MAXIMO_FECHA, LONG_MAXIMO_FECHA * INCREMENTO_SIZE, idInput, idInput, 
											StringUtils.EMPTY, PROPIEDAD_READ_ONLY, StringUtils.EMPTY, valorTexto));
			inputHtml.append(String.format(
					"<input type='button' id='calendario%s' value='Calendario'>", 
					preguntaDTO.getAbreviatura()));
		} else if("MAIL".equals(preguntaDTO.getAbreviatura())){
			inputHtml.append(String.format(INPUT_TEXT, longitudMaxima, longitudMaxima * INCREMENTO_SIZE, idInput, idInput, 
											FUNC_VALIDA_CORREO, opciones.getHabilitadoHtml(), StringUtils.EMPTY, valorTexto));
		} else if("TLR1".equals(preguntaDTO.getAbreviatura()) || "TLR2".equals(preguntaDTO.getAbreviatura())){
			inputHtml.append(String.format(INPUT_TEXT, longitudMaxima, longitudMaxima * INCREMENTO_SIZE, idInput, idInput, 
											FUNC_VALIDA_ENTEROS_NUMERICO, opciones.getHabilitadoHtml(), "CampoTelefonico", valorTexto));
		} else {
			String validacionCaptura = preguntaDTO.isCampoNumerico() ? FUNC_VALIDA_DECIMALES_NUMERICO : FUNC_VALIDA_ALFANUMERICO;
			
			int tamanioCampo = longitudMaxima > MAX_LENGTH_DEFAULT ? MAX_LENGTH_DEFAULT : longitudMaxima;
			inputHtml.append(String.format(INPUT_TEXT, longitudMaxima, tamanioCampo, idInput, idInput, 
											validacionCaptura, opciones.getHabilitadoHtml(), StringUtils.EMPTY, valorTexto));
		}
		
		return inputHtml;
	}
	
	/**
	 * @param clavePregunta clave de la pregunta
	 * @param valorParametro valor del parametro
	 * @param claveClienteCuestionario clave del cliente cuestionario
	 * @param claveCuestionario CUESTIONARIO clave del cuestionario
	 * @return CuestionarioRespuestaDTO dto de la respuesta
	 */
	public static CuestionarioRespuestaDTO obtenerValorCampoLibre(
			int clavePregunta, String valorParametro, int claveClienteCuestionario, int claveCuestionario)
	{
		CuestionarioRespuestaDTO cuestRespDTO = new CuestionarioRespuestaDTO();
		
		cuestRespDTO.setClavePregunta(clavePregunta);
		cuestRespDTO.setValorTexto(valorParametro);
		cuestRespDTO.setClaveClienteCuestionario(claveClienteCuestionario);
		cuestRespDTO.setClaveCuestionario(claveCuestionario);
		
		return cuestRespDTO;
	}
}
