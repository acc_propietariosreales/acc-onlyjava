/**
 * Isban Mexico
 *   Clase: ManejadorRadio.java
 *   Descripcion: Manejador de preguntas tipo RadioButton
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.manejador;

import org.apache.commons.lang.StringUtils;

import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.Utilerias;

public final class ManejadorRadio {
	/**
	 * String identificador de radiobutton
	 */
	public static final String ID_INPUT_RADIO = "radioPreg";
	/**
	 * String Definel el codigo HTML para un radiobutton
	 */
	private static final String INPUT_RADIO = "<input type='radio' value='%s' id='%s' name='%s' %s validacionEspecial%s%s %s %s texto='%s'>%s ";
	/***
	 * constructor
	 */
	private ManejadorRadio() {
	
	}
	/**
	 * Genera el codigo HTML para los Radio Buttons de una pregunta, no lleva una opcion predeterminada
	 * @param preguntaDTO Pregunta a la que se le van a agregar las opciones
	 * @param opcionesReponse Opciones con las cuales se van a generar los Radiobuttons
	 * @param opciones Atributos para los Radiobuttons
	 * @return StringBuilder Codigo HTML generado de los radiobuttons
	 */
	public static StringBuilder generarRadios(PreguntaDTO preguntaDTO, ResponseOpcionesPregunta opcionesReponse, AtributosTagHtmlDTO opciones) {
		return generarRadios(preguntaDTO, opcionesReponse, StringUtils.EMPTY, opciones);
	}
	
	/**
	 * Genera el codigo HTML para los Radio Buttons de una pregunta, con una opcion predeterminada de Radiobutton
	 * @param preguntaDTO Pregunta a la que se le van a agregar las opciones
	 * @param opcionesReponse Opciones con las cuales se van a generar los Radiobuttons
	 * @param opcionSeleccionada Clave del Radiobutton que se debe seleccionar
	 * @param opciones Atributos para los Radiobuttons
	 * @return StringBuilder Codigo HTML generado de los radiobuttons
	 */
	public static StringBuilder generarRadios(
			PreguntaDTO preguntaDTO, ResponseOpcionesPregunta opcionesReponse, String opcionSeleccionada, AtributosTagHtmlDTO opciones) 
	{
		String nombreRadio = String.format("%s%s", ID_INPUT_RADIO, preguntaDTO.getAbreviatura());
		StringBuilder radiosHtml = new StringBuilder();
		
		for (OpcionesPreguntaDTO opcion : opcionesReponse.getLstOpcionesPreguntas()) {
			String idRadio = String.format("%s%s%s", ID_INPUT_RADIO, preguntaDTO.getAbreviatura(), opcion.getIdOpcion());
			String checked = StringUtils.EMPTY;
			if(opcion.getValor().equals(opcionSeleccionada)) {
				checked = " checked ";
			}
			
			radiosHtml.append(
					String.format(INPUT_RADIO, opcion.getIdOpcion(), idRadio, nombreRadio, checked,
										preguntaDTO.getAbreviatura(), opcion.getDescripcion(), 
										opciones.getHabilitadoHtml(), opciones.getSoloLecturaHtml(), opcion.getDescripcion(), opcion.getDescripcion()));
		}
		
		int indicePrimerRadio = radiosHtml.indexOf("type='radio'");
		int indiceChecked = radiosHtml.indexOf("checked");
		//Si existen Radiobuttons y ninguno tienen 'checked' entonces se le pone al primero
		if(indicePrimerRadio > 0 && indiceChecked == -1 && !esPeriodicidadProcedenciaRecursos(preguntaDTO)){
			radiosHtml.insert(indicePrimerRadio, " checked ");
		}
		
		return radiosHtml;
	}
	
	/**
	 * Generar un objeto de tipo CuestionarioRespuestaDTO el cual representa una opcion de Radiobutton
	 * @param clavePregunta Clave de la pregunta a la que pertenece el Radiobutton
	 * @param strValorParametro Valor de la opcion
	 * @param claveClienteCuestionario Clave del Cliente-Cuestionario
	 * @param claveCuestionario Clave del Cuestionario
	 * @return CuestionarioRespuestaDTO Respuesta de un Radiobutton
	 */
	public static CuestionarioRespuestaDTO obtenerValorRadio(
			int clavePregunta, String strValorParametro, int claveClienteCuestionario, int claveCuestionario)
	{
		CuestionarioRespuestaDTO cuestRespDTO = new CuestionarioRespuestaDTO();
		int valorParametro =Utilerias.defaultInteger(strValorParametro);
		
		cuestRespDTO.setClavePregunta(clavePregunta);
		cuestRespDTO.setValorOpcion(valorParametro);
		cuestRespDTO.setClaveClienteCuestionario(claveClienteCuestionario);
		cuestRespDTO.setClaveCuestionario(claveCuestionario);
		
		return cuestRespDTO;
	}
	
	private static boolean esPeriodicidadProcedenciaRecursos(PreguntaDTO preguntaDTO) {
		return "IAPR".equals(preguntaDTO.getSeccion()) && ("EBPR".equals(preguntaDTO.getAbreviatura()) || "FBPR".equals(preguntaDTO.getAbreviatura()));
	}
}
