/**
 * Isban Mexico
 *   Clase: ManejadorSelect.java
 *   Descripcion: Manejador de preguntas tipo Select
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.manejador;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import mx.isban.norkom.cuestionario.bean.ResponseOpcionesPregunta;
import mx.isban.norkom.cuestionario.dto.AtributosTagHtmlDTO;
import mx.isban.norkom.cuestionario.dto.CuestionarioRespuestaDTO;
import mx.isban.norkom.cuestionario.dto.OpcionesPreguntaDTO;
import mx.isban.norkom.cuestionario.dto.PreguntaDTO;
import mx.isban.norkom.cuestionario.util.Utilerias;

public final class ManejadorSelect {
	/**
	 * String ID_SELECT_UNICO
	 */
	public static final String ID_SELECT_UNICO = "selectPregUnico";
	/**
	 * String ID_SELECT_AUTOCOMPLETAR
	 */
	public static final String ID_SELECT_AUTOCOMPLETAR = "selectPregAutocompletar";
	/**
	 * String ID_SELECT_MULTIPLE
	 */
	public static final String ID_SELECT_MULTIPLE = "selectPregMultiple";
	/**
	 * String SELECT_INI
	 */
	private static final String SELECT_INI="<select id='%s' name='%s' %s %s %s %s class='%s'>";
	/**
	 * String SELECT_INI_MULTIPLE
	 */
	private static final String SELECT_INI_MULTIPLE="<select id='%s' name='%s' %s %s %s %s class='%s' style='width: 350px;'>";
	/**
	 * String OPTION
	 */
	private static final String OPTION="<option value='%s' %s>%s</option>";
	/**
	 * String SELECT_FIN
	 */
	private static final String SELECT_FIN="</select>";
	/**Constructor**/
	private ManejadorSelect(){
		
	}
	
	/**
	 * Genera el codigo HTML para un SELECT y sus OPTION con alguna opcion seleccionada por default
	 * @param preguntaDTO Pregunta con la cual se va a generar el SELECT
	 * @param opcionesReponse Opciones que tendra el SELECT
	 * @param opcionSeleccionada Opcion seleccionada
	 * @param atributos Atributos HTML del SELECT
	 * @return StringBuilder con el codigo HTML con el SELECT
	 */
	public static StringBuilder generarSelect(
			PreguntaDTO preguntaDTO, ResponseOpcionesPregunta opcionesReponse, String opcionSeleccionada, AtributosTagHtmlDTO atributos)
	{
		StringBuilder selectHtml = new StringBuilder();
		
		if("SELECT".equals(preguntaDTO.getTipoPregunta())) {
			atributos.setClassCss(ID_SELECT_UNICO);
			selectHtml.append(generarSelectUnico(preguntaDTO, opcionesReponse, opcionSeleccionada, atributos));
		} else if("SELECT_AUT".equals(preguntaDTO.getTipoPregunta())) {
			atributos.setClassCss(ID_SELECT_AUTOCOMPLETAR);
			selectHtml.append(generarSelectUnico(preguntaDTO, opcionesReponse, opcionSeleccionada, atributos));
		} else if("SELECT_MUL".equals(preguntaDTO.getTipoPregunta())) {
			atributos.setClassCss(ID_SELECT_MULTIPLE);
			atributos.setRespuestaMultiple(true);
			selectHtml.append(generarSelectMultiple(preguntaDTO, opcionesReponse, opcionSeleccionada, atributos));
		}
		
		return selectHtml;
	}
	
	/**
	 * Genera el codigo HTML para un SELECT y sus OPTION con seleccion multiple
	 * @param preguntaDTO Pregunta con la cual se va a generar el SELECT
	 * @param opcionesReponse Opciones que tendra el SELECT
	 * @param opcionSeleccionada Opcion seleccionada
	 * @param atributos Atributos HTML del SELECT
	 * @return StringBuilder con el codigo HTML con el SELECT
	 */
	public static StringBuilder generarSelectMultiple(
			PreguntaDTO preguntaDTO, ResponseOpcionesPregunta opcionesReponse, String opcionSeleccionada, AtributosTagHtmlDTO atributos)
	{
		StringBuilder selectHtml = new StringBuilder();
		String idSelectOrigen = String.format("%sOrigen%s", ID_SELECT_MULTIPLE, preguntaDTO.getAbreviatura());
		String idSelectDestino = String.format("%s%s", ID_SELECT_MULTIPLE, preguntaDTO.getAbreviatura());
		
		atributos.setClassCss(ID_SELECT_MULTIPLE);
		
		selectHtml.append("<TABLE>");
		selectHtml.append("<TR>");
		
		selectHtml.append("<TD>");
		selectHtml.append(agregarTagInicialSelectOrigen(atributos, idSelectOrigen));
		selectHtml.append(agregarOpciones(opcionesReponse, opcionSeleccionada));
		selectHtml.append(SELECT_FIN);
		
		selectHtml.append("&nbsp;&nbsp;&nbsp;");
		String htmlInputAgregar = String.format(
				"<input type='button' id='button%s' name='button%s' value='Agregar' onclick=\"moverOpcionesSelect('%s', '%s');\">", 
				idSelectOrigen, idSelectOrigen, idSelectOrigen, idSelectDestino);
		selectHtml.append(htmlInputAgregar);
		
		selectHtml.append("<br><br>");
		
		selectHtml.append(agregarTagInicialSelectMultiple(atributos, idSelectDestino));
		selectHtml.append(SELECT_FIN);
		
		selectHtml.append("&nbsp;&nbsp;&nbsp;");
		String htmlInputEliminar = String.format(
				"<input type='button' id='button%s' name='button%s' value='Eliminar' onclick=\"moverOpcionesSelect('%s', '%s');\">", 
				idSelectDestino, idSelectDestino, idSelectDestino, idSelectOrigen);
		selectHtml.append(htmlInputEliminar);
		
		selectHtml.append("</TD>");
		selectHtml.append("</TR>");
		selectHtml.append("</TABLE>");
		
		
		return selectHtml;
	}
	
	/**
	 * Genera el codigo HTML para un SELECT y sus OPTION con seleccion unica
	 * @param preguntaDTO Pregunta con la cual se va a generar el SELECT
	 * @param opcionesReponse Opciones que tendra el SELECT
	 * @param opcionSeleccionada Opcion seleccionada
	 * @param atributos Atributos HTML del SELECT
	 * @return StringBuilder con el codigo HTML con el SELECT
	 */
	public static StringBuilder generarSelectUnico(
			PreguntaDTO preguntaDTO, ResponseOpcionesPregunta opcionesReponse, String opcionSeleccionada, AtributosTagHtmlDTO atributos)
	{
		StringBuilder selectHtml = new StringBuilder();
		
		String idSelect = String.format("%s%s", ID_SELECT_UNICO, preguntaDTO.getAbreviatura());
		selectHtml.append(agregarTagInicialSelect(atributos, idSelect));
		selectHtml.append(agregarOpciones(opcionesReponse, opcionSeleccionada));
		selectHtml.append(SELECT_FIN);
		
		return selectHtml;
	}
	
	/**
	 * Genera codigo HTML para un SELECT
	 * @param atributos objeto con los datos de atributos html
	 * @param idSelect Identificador HTML que se le asignar al SELECT creado
	 * @return String sql a ejecutar
	 */
	private static String agregarTagInicialSelect(AtributosTagHtmlDTO atributos, String idSelect){
		return String.format(SELECT_INI, idSelect, idSelect,
				atributos.getHabilitadoHtml(), atributos.getSoloLecturaHtml(), atributos.getRespuestaMultipleHtml(), "", atributos.getClassCss());
	}
	
	/**
	 * Genera codigo HTML para un SELECT MULTIPLE
	 * @param atributos objeto con los datos de atributos html
	 * @param idSelect Identificador HTML que se le asignar al SELECT creado
	 * @return String sql a ejecutar
	 */
	private static String agregarTagInicialSelectMultiple(AtributosTagHtmlDTO atributos, String idSelect){
		return String.format(SELECT_INI_MULTIPLE, idSelect, idSelect,
				atributos.getHabilitadoHtml(), atributos.getSoloLecturaHtml(), atributos.getRespuestaMultipleHtml(), "", atributos.getClassCss());
	}
	
	/**
	 * Genera codigo HTML para un SELECT MULTIPLE
	 * @param atributos objeto con los datos de atributos html
	 * @param idSelectOrigen Identificador HTML que se le asignar al SELECT creado
	 * @return String sql a ejecutar
	 */
	private static String agregarTagInicialSelectOrigen(AtributosTagHtmlDTO atributos, String idSelectOrigen){
		return String.format(SELECT_INI_MULTIPLE, idSelectOrigen, idSelectOrigen,
				atributos.getHabilitadoHtml(), atributos.getSoloLecturaHtml(), atributos.getRespuestaMultipleHtml(), "", atributos.getClassCss());
	}
	
	/**
	 * 
	 * @param opcionesReponse opciones de pregunta
	 * @param opcionSeleccionada opcion seleccionada
	 * @return StringBuilder string con resultado
	 * 
	 */
	private static StringBuilder agregarOpciones(ResponseOpcionesPregunta opcionesReponse, String opcionSeleccionada) {
		StringBuilder optionsHtml = new StringBuilder("");
		
		for (OpcionesPreguntaDTO opcion : opcionesReponse.getLstOpcionesPreguntas()) {
			String selected = opcion.getValor().equals(opcionSeleccionada) ? " selected " : StringUtils.EMPTY;
			optionsHtml.append(String.format(OPTION, opcion.getIdOpcion(), selected, opcion.getDescripcion()));
		}
		
		return  optionsHtml;
	}
	
	/**
	 * @param clavePregunta clave de la pregunta
	 * @param strValorParametro valor del parametro
	 * @param claveClienteCuestionario clave de clientecuestionario
	 * @param claveCuestionario clave del cuestionario
	 * @return CuestionarioRespuestaDTO ojeto con la informacion se gun parametros
	 */
	public static CuestionarioRespuestaDTO obtenerValorSelectUnico(
			int clavePregunta, String strValorParametro, int claveClienteCuestionario, int claveCuestionario)
	{
		CuestionarioRespuestaDTO cuestRespDTO = new CuestionarioRespuestaDTO();
		int valorParametro = Utilerias.defaultInteger(strValorParametro);
		
		cuestRespDTO.setClavePregunta(clavePregunta);
		cuestRespDTO.setValorOpcion(valorParametro);
		cuestRespDTO.setClaveClienteCuestionario(claveClienteCuestionario);
		cuestRespDTO.setClaveCuestionario(claveCuestionario);
		
		return cuestRespDTO;
	}
	
	/**
	 * @param clavePregunta clave de la pregunta
	 * @param strValorParametros  valor del parametro
	 * @param claveClienteCuestionario clave de clientecuestionario
	 * @param claveCuestionario clave del cuestionario
	 * @return List<CuestionarioRespuestaDTO> lista de valores para le pregunta
	 */
	public static List<CuestionarioRespuestaDTO> obtenerValorSelectMultiple(
			int clavePregunta, String[] strValorParametros, int claveClienteCuestionario, int claveCuestionario)
	{
		List<CuestionarioRespuestaDTO> respuestas = new ArrayList<CuestionarioRespuestaDTO>();
		int valorParametro = 0;
		
		for(String strValorParametro : strValorParametros){
			CuestionarioRespuestaDTO cuestRespDTO = new CuestionarioRespuestaDTO();
			
			valorParametro = Utilerias.defaultInteger(strValorParametro);
			
			cuestRespDTO.setClavePregunta(clavePregunta);
			cuestRespDTO.setValorOpcion(valorParametro);
			cuestRespDTO.setClaveClienteCuestionario(claveClienteCuestionario);
			cuestRespDTO.setClaveCuestionario(claveCuestionario);
			
			respuestas.add(cuestRespDTO);
		}
		
		return respuestas;
	}
}
