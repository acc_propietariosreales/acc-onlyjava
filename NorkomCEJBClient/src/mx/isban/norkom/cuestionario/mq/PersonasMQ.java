/******************************************************************** 
 * 	Isban Mexico
 *   Clase: PersonasMQ 
 *   Descripcion: DAO para el consumo de la transacciones en Personas
 *   Control de Cambios: 
 *   1.0 Oct 24, 2012 Stefanini - Creacion
 *********************************************************************/
package mx.isban.norkom.cuestionario.mq;

import java.util.List;

import javax.ejb.Local;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.norkom.cuestionario.dto.RequestODF3DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP1DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP2DTO;
import mx.isban.norkom.cuestionario.dto.RequestODP3DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE58DTO;
import mx.isban.norkom.cuestionario.dto.RequestPE71DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODF3DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP1DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP2DTO;
import mx.isban.norkom.cuestionario.dto.ResponseODP3DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE58DTO;
import mx.isban.norkom.cuestionario.dto.ResponsePE71DTO;

@Local
public interface PersonasMQ {
	/**
	 * Identificador de la trama ODP1
	 */
	String ID_TRAMA_ODP1 = "ODP1";
	
	/**
	 * Identificador de la trama ODP2
	 */
	String ID_TRAMA_ODP2 = "ODP2";
	
	/**
	 * Identificador de la trama ODP3
	 */
	String ID_TRAMA_ODP3 = "ODP3";
	
	/**
	 * Identificador de la trama ODF3
	 */
	String ID_TRAMA_ODF3 = "ODF3";
	
	/**
	 * Identificador de la trama PE71
	 */
	String ID_TRAMA_PE71 = "PE71";
	
	/**
	 * Identificador de la trama PE58
	 */
	String ID_TRAMA_PE58 = "PE58";
	
	/**
	 * Indice de la trama de salida ODP1: Resultado de la operacion
	 */
	int ODP1_CODIGO_RESPUESTA = 2;
	
	/**
	 * Indice de la trama de salida ODP2: Resultado de la operacion
	 */
	int ODP2_CODIGO_RESPUESTA = 2;
	
	/**
	 * Indice de la trama de salida ODP3: Resultado de la operacion
	 */
	int ODP3_CODIGO_RESPUESTA = 2;
	
	/**
	 * Indice de la trama de salida ODF3: Resultado de la operacion
	 */
	int ODF3_CODIGO_RESPUESTA = 2;

	/**
	 * String ERROR INESPERADO EN CICS
	 */
	String ERROR_CICS="ERROR INESPERADO EN CICS";
	
	/**
	 * Codigo para referencias personales en la tx PE58
	 */
	String CODIGO_REFERENCIAS_PERSONALES = "10";
	
	/**
	 * Codigo para referencias bancarias en la tx PE58
	 */
	String CODIGO_REFERENCIAS_BANCARIAS = "30";
	
	/**
	 * Obtiene el Nivel de Riesgo y el Indicador UPLD de Personas a traves de la transaccion ODP1
	 * @param input Datos de entrada de la transaccion
	 * @param sesion Objeto de sesion de Agave
	 * @return Nivel de Riesgo y el Indicador UPLD
	 * @throws BusinessException En caso de un error en la ejecucion de la trama
	 */
	public ResponseODP1DTO obtenerCalificacionesODP1(RequestODP1DTO input, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Actualiza el nivel de riesgo del cliente a traves de la trama ODP2
	 * @param input Objeto con la informacion de la peticion
	 * @param sesion Objeto de sesion de Agave
	 * @return ResponseODP2DTO Objeto con el codigo de la operacion
	 * @throws BusinessException En caso de un error durante la operacion 
	 */
	public ResponseODP2DTO actualizarRiesgoODP2(RequestODP2DTO input, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Actualiza el estado del cliente a traves de la trama ODP3
	 * @param input Objeto con la informacion de la peticion
	 * @param sesion Objeto de sesion de Agave
	 * @return ResponseODP3DTO Objeto con el codigo de la operacion
	 * @throws BusinessException En caso de un error durante la operacion
	 */
	public ResponseODP3DTO actualizarEstadoClienteNuevoODP3(RequestODP3DTO input, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Obtiene el correo electronico de un cliente
	 * @param input Objeto con la informacion de la peticion
	 * @param sesion Objeto de sesion de Agave
	 * @return ResponseODF3DTO Objeto con informacion del cliente, incuido el correo electronico
	 * @throws BusinessException En caso de un error durante la operacion
	 */
	public ResponseODF3DTO obtenerCorreoElectronicoODF3(RequestODF3DTO input, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Obtiene las referencias personales para una persona fisica
	 * @param input Objeto con la informacion de la peticion
	 * @param conActividadEmpresarial Define si es persona fisica con actividad empresarial
	 * @param sesion Objeto de sesion de Agave
	 * @return Lista de referencias
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public List<ResponsePE58DTO> obtenerReferenciasPersonasFisicasPE58(RequestPE58DTO input, boolean conActividadEmpresarial, ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Obtiene las referencias bancarias de una persona moral
	 * @param input Objeto con la informacion de la peticion
	 * @param sesion Objeto de sesion de Agave
	 * @return Lista de referencias bancarias
	 * @throws BusinessException En caso de un error con la informacion
	 */
	public List<ResponsePE71DTO> obtenerReferenciasBancariasPE71(RequestPE71DTO input, ArchitechSessionBean sesion) throws BusinessException;

}
