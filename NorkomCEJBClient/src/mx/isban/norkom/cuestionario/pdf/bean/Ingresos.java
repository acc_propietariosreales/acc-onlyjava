/**
 * Isban Mexico
 *   Clase: Ingresos.java
 *   Descripcion: Componente que almacena tipos de ingresos
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.pdf.bean;

/**
 * @author Leopoldo F Espinosa R 10/02/2015
 *
 */
public enum Ingresos {
	/**clave para honorarios*/
	HONORARIOS("HONORARIOS","HON"),
	/**clave para sueldo*/
	SUELDO("SUELDO","SUE"),
	/**clave para inversiones*/
	INVERSIONES("INVERSIONES","INV"),
	/**clave para comisiones*/
	COMISIONES("COMISIONES","COM"),
	/**clave para venta de vienes*/
	VENTADEBIENES("VENTA DE BIENES","VEN"),
	/**clave para Arrendamiento*/
	ARRENDAMIENTO("ARRENDAMIENTO","ARR");
	
	/**nombre del tipo de Ingreso*/
	private String descripcion;
	/**clave para los reporte**/
	private String clave;
	/**
	 * Constructor de enum
	 * @param descripcion DESCRIPCION
	 * @param clave CLAVE
	 */
	private Ingresos(String descripcion,String clave){
		this.descripcion=descripcion;
		this.clave=clave;
	}
	
	@Override
	public String toString() {
		return this.clave;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}
}
