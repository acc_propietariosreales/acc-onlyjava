/**
 * Isban Mexico
 *   Clase: ConstantesEncabezado.java
 *   Descripcion: Clase de constantes para la informacion que se recibe en la peticion del Cuestionario de Informacion Preliminar
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.cuestionario.util;

/**
 * @author Leopoldo F Espinosa R
 * Clase de constantes para la informacion que se recibe en la peticion del Cuestionario de Informacion Preliminar
 */
public class ConstantesEncabezado {
	/**
	 * Identificador de Cuestionario AP000000YYYYMMDD 
	 * AP=Identificador aplicacion, 000000=Consecutivo de 6 digitos, YYYY=Anio a cuatro digitos, MM=Mes dos digitos, DD=Dia dos digitos
	 */
	public static final String ENC_ID_CUESTIONARIO = "idCuestionario";
	/**
	 * BUC del cliente
	 */
	public static final String ENC_BUC = "bucCliente";
	/**
	 * Indicador para saber si es un cliente nuevo o ya existente (N = Nuevo, E = Existente)
	 */
	public static final String ENC_IND_CLIENTE_NVO = "indClienteNuevo";
	/**
	 * Nombre del cliente
	 */
	public static final String ENC_NOMBRE_CLI = "nombreCliente";
	/**
	 * Apellido paterno del cliente
	 */
	public static final String ENC_AP_PATERNO = "apPaternoCliente";
	/**
	 * Apellido materno del cliente
	 */
	public static final String ENC_AP_MATERNO = "apMaternoCliente";
	/**
	 * Fecha de nacimiento del cliente
	 */
	public static final String ENC_FECHA_NAC = "fechaNacCliente";
	/**
	 * Tipo de persona del cliente
	 */
	public static final String ENC_TIPO_PERS = "tipoPersonaCliente";
	/**
	 * Subtipo de persona del cliente
	 */
	public static final String ENC_SUBTIPO_PERS = "subtipoPersonaCliente";
	/**
	 * Divisa del producto contratado
	 */
	public static final String ENC_DIVISA_PROD = "divisaProductoCntr";
	/**
	 * Codigo de la sucursal de apertura del contrato
	 */
	public static final String ENC_CODIGO_SUC = "codSucAperturaCntr";
	/**
	 * Nombre de la sucursal de apertura del contrato
	 */
	public static final String ENC_NOMBRE_SUC = "nombreSucAperturaCntr";
	/**
	 * Codigo de la Zona
	 */
	public static final String ENC_CODIGO_ZONA = "codZona";
	/** 
	 * Nombre de la Zona
	 */
	public static final String ENC_NOMBRE_ZONA = "nombreZona";
	/**
	 * Codigo de la plaza de la sucursal
	 */
	public static final String ENC_CODIGO_PLAZA = "codPlaza";
	/** 
	 * Nombre de la plaza de la sucursal
	 */
	public static final String ENC_NOMBRE_PLAZA = "nombrePlaza";
	/**
	 * Codigo de la region de la sucursal
	 */
	public static final String ENC_COD_REGION = "codigoRegion";
	/**
	 * Nombre de la region de la sucursal
	 */
	public static final String ENC_NOM_REGION = "nombreRegion";
	/**
	 * Codigo de Entidad del domicilio del cliente
	 */
	public static final String ENC_COD_ENTIDAD_DOM = "codEntidad";
	/**
	 * Codigo del segmento al que pertenece el cliente
	 */
	public static final String ENC_COD_SEGMENTO = "codSegmentoCliente";
	/**
	 * Descripcion del segmento al que pertenece el cliente
	 */
	public static final String ENC_DESC_SEGMENTO = "descSegmentoCliente";
	/**
	 * Codigo de actividad generica
	 */
	public static final String ENC_COD_ACT_GENERICA = "codActGenericaCliente";
	/**
	 * Descripcion de la actividad generica
	 */
	public static final String ENC_DESC_ACT_GENERICA = "descActGenericaCliente";
	/**
	 * Codigo de actividad especifica
	 */
	public static final String ENC_COD_ACT_ESPECIFICA = "codActEspecificaCliente";
	/**
	 * Descripcion de la actividad generica
	 */
	public static final String ENC_DESC_ACT_ESPECIFICA = "descActEspecificaCliente";
	/**
	 * Codigo de producto
	 */
	public static final String ENC_COD_PROD = "codProducto";
	/**
	 * Descripcion de producto
	 */
	public static final String ENC_DESC_PROD = "descProducto";
	/**
	 * Codigo de subproducto
	 */
	public static final String ENC_COD_SUB_PROD = "codSubproducto";
	/**
	 * Descripcion de subproducto
	 */
	public static final String ENC_DESC_SUB_PROD = "descSubproducto";
	/**
	 * Codigo de pais de residencia
	 */
	public static final String ENC_COD_PAIS = "codPaisResidencia";
	/**
	 * Codigo del municipio de residencia del cliente  (COD_COMUNA + COD_POBLACION)
	 */
	public static final String ENC_COD_MUNICIPIO = "codMunicipioResidencia";
	/**
	 * Codigo del pais de nacimiento del cliente
	 */
	public static final String ENC_COD_NACIONALIDAD = "codNacionalidadCliente";
	/**
	 * Branch al que pertenece el contrato
	 */
	public static final String ENC_COD_BRANCH = "codBranch";
	/**
	 * Codigo de centro de costos
	 */
	public static final String ENC_COD_CENTRO_COSTO = "codCentroCosto";
	/**
	 * Tipo de formulario a presentar D = Estatico y Dinamico,  E = Estatico
	 */
	public static final String ENC_COD_TIPO_FORMULARIO = "codTipoFormulario";
	/**
	 * Nombre del Funcionario
	 */
	public static final String ENC_NOMBRE_FUNC = "nombreFuncionario";
}
