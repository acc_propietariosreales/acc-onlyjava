/**
 * Isban Mexico
 *   Clase: Cuestionarios.java
 *   Descripcion: Componente que almacena tipos y datos de cuestionarios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

/**
 * @author Leopoldo F Espinosa R 05/02/2015
 *
 */
public enum Cuestionarios {
	/**Informacion Preliminar*/
	TIPO_IP("Informacion Preliminar"),
	/**Informacion complementaria A1*/
	TIPO_IC_A1("Informacion Complementaria A1"),
	/**Informacion complementaria A2*/
	TIPO_IC_A2("Informacion Complementaria A2"),
	/**Informacion complementaria A3*/
	TIPO_IC_A3("Informacion Complementaria A3"),
	/**personas fisicas*/
	PERSONA_FISICA("Fisica"),
	/**personas fisicas con actividad Empresarial*/
	PERSONA_FISICA_AE("Fisica Actividad Empresarial"),
	/**personas morales*/
	PERSONA_MORAL("Moral"),
	/**tipo de moneda en Dolares*/
	TIPO_MONEDA_DOLARES("Dolares"),
	/**tipo de moneda en Pesos*/
	TIPO_MONEDA_PESOS("Pesos");
	
	/**
	 * Valor de la constante
	 */
	private String valor; 
	
	/**
	 * 
	 * @param valor a agregar
	 */
	private Cuestionarios(String valor){
		this.valor=valor;
	}
	/**obtencion de la variable valor
	 * @return String
	 */
	public String getValor(){
		return this.valor;
	}
	/***
	 * metodo para comparar los enums
	 * @param comparacion enum
	 * @return boolean con resultado
	 */
	public boolean equalsObject(Cuestionarios comparacion){
		if(comparacion!=null && this.getValor().equals(comparacion.getValor())){
			return true;
		}
		return false;
	}
}
