/**
 * Isban Mexico
 *   Clase: Constantes.java
 *   Descripcion: Clase estatus relacionados
 *
 *   Control de Cambios:
 *   1.0 Feb 09 2018 -everis- Creacion
 *
 */
package mx.isban.norkom.cuestionario.util;

/**
 * Valores Constantes para el campo de estatus de relacionados
 * @author everis
 */
public class EstatusRelacionados {
	
	/**Estatus de Relaciondo con el valor de Recibido*/
	public static final String RECIBIDO="REC";
	
	
	/**Estatus de Relaciondo con el valor de OK*/
	public static final String OK="OK";
	
	
	/**Estatus de Relaciondo con el valor de KO*/
	public static final String NO_OK="KO";
	
	
	/**Estatus de Relaciondo con el valor de Rechazado*/
	public static final String RECHAZADO="RZD";
	
	/**Estatus de Relaciondo con el valor de Enviado*/
	public static final String ENVIADO="ENV";

}
