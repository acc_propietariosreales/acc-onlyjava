/**
 * Isban Mexico
 *   Clase: IDACanal.java
 *   Descripcion: Clase para obtener los canales disponibles en el aplicativo
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

/**
 * @author STEFANINI (Leopoldo F Espinosa R) 06/11/2014
 * Clase para obtener los canales disponibles en el aplicativo
 *
 */
public enum IDACanal {
	
	/**
	 * IDACanal CANAL_DB_NORKOM
	 */
	CANAL_DB_NORKOM("DB_NORKOM"),
	/**
	 * IDACanal CANAL_MQ_NORKOM
	 */
	CANAL_MQ_NORKOM("CICS_CC");

	/**
	 * Nombre
	 */
	private String nombre;

	/**
	 * Crea canal con el nombre indicado
	 * 
	 * @param nombre
	 *            variable a almacenar
	 */
	private IDACanal(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene el nombre del canal
	 * 
	 * @return regresa el nombre del canal
	 */
	public String getNombre() {
		return nombre;
	}
}
