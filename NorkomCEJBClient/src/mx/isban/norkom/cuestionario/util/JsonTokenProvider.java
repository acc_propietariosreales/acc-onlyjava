package mx.isban.norkom.cuestionario.util;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import mx.isban.norkom.cuestionario.dto.CuestionarioLightHouseRequest;

/**
 * @author jugarcia
 * Objetivo: Convierte objetos java a json
 * Justificacion: Permite convertir las peticiones y las respuestas a los servicios web JAX-RS de java a json y viceversa.
 */

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JsonTokenProvider implements MessageBodyWriter<Object> {

	@Override
	public long getSize(Object arg0, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4) {
		return -1;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		boolean isWritable = false;
		if (List.class.isAssignableFrom(type) && genericType instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) genericType;
			Type[] actualTypeArgs = (parameterizedType.getActualTypeArguments());
			isWritable = (actualTypeArgs.length == 1 && actualTypeArgs[0]
					.equals(CuestionarioLightHouseRequest.class));
		} else if (type == CuestionarioLightHouseRequest.class) {
			isWritable = true;
		}
		
		return isWritable;
	}

	@Override
	public void writeTo(Object object, Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders,
			OutputStream entityStream) throws IOException,
			WebApplicationException {
		// Explicitly use the Jackson ObjectMapper to write dates in ISO8601
		// format
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.writeValue(entityStream, object);
	}

}
