/**
 * Isban Mexico
 *   Clase: Mensajes.java
 *   Descripcion: Componente que almacena los errores y descripcion de los mismos
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Leopoldo Espinosa - Creacion
 */
package mx.isban.norkom.cuestionario.util;

/**
 * @author STEFANINI (Leopoldo F Espinosa R) 06/11/2014
 *
 */
public enum Mensajes {

	/**
	 * Mensaje de error de comunicacion.
	 **/
	MSJ_ERROR("MSJ_ERR"),
	
	/**
	 * Mensaje de error de comunicacion.
	 **/
	COD_ERROR("COD_ERR"),
	/**
	 * Mensajes ERROR_COMUNICACION
	 */
	ERROR_COMUNICACION("ERCMN00","Error en Base de Datos Favor de Informar al Administrador"),
	/**
	 * Mensajes OPERACION_EXITOSA
	 */
	OPERACION_EXITOSA ("OK0001","Operaci\u00F3n exitosa "),
	/**
	 * Mensajes ERROR_INSERT
	 */
	ERROR_INSERT      ("ERRIN01","Error al agregar el registro"),
	/**
	 * Mensajes ERROR_NO_DATOS
	 */
	ERROR_NO_DATOS    ("BER0002","No existen datos para la consulta"),
	/**Error al guardar relacionados**/
	ERRLBD01("ERRLBD01","No fue posible guardar al Relacionado en la base de datos"),
	/**
	 * Mensajes ERROR_DATOS_INICIO
	 */
	ERROR_DATOS_INICIO("ERRDT01","Faltan datos necesarios para el Cuestionario IP"),
	/**
	 * Mensajes ERR_EXP_PDF_IP
	 */
	ERR_EXP_PDF_IP("EREXP01","Ocurrio un error al exportar el cuestionario IP"),
	/**
	 * Mensajes ERR_EXP_PDF_IC
	 */
	ERR_EXP_PDF_IC("EREXP02","Ocurrio un error al exportar el cuestionario IC"),
	/**
	 * Mensajes ERR_EXP_PDF_NO_IN_DATA EREXP03-No hay datos para realizar la busqueda
	 */
	ERR_EXP_PDF_NO_IN_DATA("EREXP03","No hay datos para realizar la b\u00FAsqueda"),
	/**
	 * Mensajes ERR_EXP_PDF_NO_IN_ID_CUEST EREXP04-No se ingreso el Id del Cuestionario
	 */
	ERR_EXP_PDF_NO_IN_ID_CUEST("EREXP04","No se ingreso el Id del Cuestionario"),
	/**
	 * Mensajes ERR_EXP_PDF_NO_IN_ID_CUEST EREXP05-Faltan datos para poder realizar la busqueda del cuestionario
	 */
	ERR_EXP_PDF_NO_IN_OBLIG("EREXP05","Faltan datos para poder realizar la b\u00FAsqueda del cuestionario"),
	//Mensajes de WS CC
	/**operacion exitosa**/
	EXOBID00("EXOBID00","Operaci\u00F3n  exitosa"),
	/**operacion exitosa de agregar relacionados WS**/
	EXRLGD00("EXRLGD00","Operaci\u00F3n exitosa"),
	/**registros con errores**/
	WNRLGD00("WNRLGD00","Existen registros con errores"),
	/***operacion exitosa de consulta relacionados**/
	EXGDRL00("EXGDRL00","Operacion exitosa"),
	/**error en relacionados**/
	ERGDRL01("ERGDRL01","No fue posible obtener los relacionados al cliente ERR[%s]"),
	/***operacion exitosa**/
	EXASCT00("EXASCT00","Operaci\u00F3n Exitosa"),
	/**Operacion exitosa en obtencion de datos generales de cuestionario**/
	EXOBCU00("EXOBCU00","Operacion exitosa"),
	/**Advertencia para operacion exitosa pero no existen registros con informacion**/
	WNOBCU00("WNOBCU00","No existen cuestionarios para la informaci\u00F3n proporcionada"),
	/**Error en bcu*/
	EROBCU01("EROBCU01"),
	/**Error EROBCU02 de formtao de fechas */
	EROBCU02("EROBCU02","Formato de fechas incorrecto, el formato es yyyyMMdd")
	/**NKM0001 Error de Conexion**/
	,NKM0001("NKM0001","Error de Conexi\u00F3n")
	/**ERRLGD01 **/
	,ERRLGD01("ERRLGD01","Es necesario proporcionar un Identificador de Cuestionario, sin este dato no es posible realizar la operaci\u00F3n")
	/**EROBCL01*/
	,EROBCL01("EROBCL01","No se ha proporcionado el C\u00F3digo de Cliente")
	/***ERCCIP13**/
	,ERCCIP13("ERCCIP13")
	/**ERCCIP08 no hay tipo de persona**/
	,ERCCIP08("ERCCIP08")
	/**ERCCIP09 no hay subtipo  persona**/
	,ERCCIP09("ERCCIP09")
	/**ERCCIP10 no hay divisa**/
	,ERCCIP10("ERCCIP10")
	/**ERCCIP11 datoscuestionario vacio**/
	,ERCCIP11("ERCCIP11")
	/**ERCCIP12 no hay preguntas por seccion**/
	,ERCCIP12("ERCCIP12")
	/**ERAGCT01 ya existe cuestionario**/
	,ERAGCT01("ERAGCT01","Ya existe un Cuestionario con el Identificador proporcionado")
	/**"ERAGCT02"**/
	,ERAGCT02("ERAGCT02", "Ya existe un Cuestionario con informaci\u00F3n capturada")
	/**ERAGCT06 act especifica no valida**/
	,ERAGCT06("ERAGCT06", "No fue posible identificar la Actividad Especifica")
	/**ERAGCT07 no hay informacion en bd**/
	,ERAGCT07("ERAGCT07", "No fue posible obtener la informaci\u00F3n del cuestionario en BD")
	/**No fue posible identificar el pais de Residencia**/
	,ERAGCT08("ERAGCT08", "No fue posible identificar el pa\u00EDs de Residencia")
	/**"No existe parametro de busqueda"**/
	,ERRXML01("ERRXML01","No existe parametro de busqueda")
	/**ERCLCT02 no hay dato para conexion con norkom**/
	,ERCLCT02("ERCLCT02","No fue posible obtener la informaci\u00F3n de conexi\u00F3n a Norkom")
	/**ERCLCT03 no hay conexion a norkom **/
	,ERCLCT03("ERCLCT03", "No fue posible conectarse a Norkom")
	/**ERCLCT01 no fue posible calificar cliente**/
	,ERCLCT01("ERCLCT01", "No fue posible calificar al Cliente en Norkom")
	/**ERCLCT04 no se pudo obtener la calificacion*/
	,ERCLCT04("ERCLCT04", "No fue posible obtener la calificaci\u00F3n de Norkom")
	/**JMSE00X(JMSE000) No fue posible Conectarse a Personas*/
	,JMSE00X("JMSE000","No fue posible Conectarse a Personas")
	;
	
	/**
	 * El codigo del mensaje.
	 **/
	private String codigo;

	/**
	 * Descripcion del mensaje
	 */
	private String descripcion;

	/**
	 * Constructor de mensajes que asigna valor solamente al codigo.
	 * 
	 * @param codigo
	 *            el codigo del mensaje.
	 **/
	private Mensajes(String codigo) {
		this.codigo = codigo;
		this.descripcion = "";
	}

	/**
	 * Constructor de mensajes que asigna valor tanto al codigo como a la
	 * descripcion.
	 * 
	 * @param codigo
	 *            El codigo que va a ser asignado
	 * @param descripcion
	 *            La descripcion que va a ser asignada
	 */
	private Mensajes(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el codigo del mensaje.
	 * 
	 * @return el codigo del mensaje.
	 **/
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Obtiene la descripcion del mensaje
	 * 
	 * @return La descripcion correspondiente al codigo
	 */
	public String getDescripcion() {
		return descripcion;
	}

}
