/**
 * Isban Mexico
 *   Clase: Utilerias.java
 *   Descripcion: Componente modificado para reutilizarse en diferentes proyectos
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.cuestionario.util;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
/**
 * 
 * @author Stefanini FSW, modificado para reutilizarse en diferentes proyectos
 *
 */
public final class Utilerias extends UtileriasHelper{
	/**
	 * Constructor creado para evitar que se pueda crear una instancia de esta
	 * clase
	 */
	private Utilerias() {
	}

	/**
	 * Obtiene el entero indicado, si el valor indicado es nulo entonces se
	 * regresa el entero por defecto indicado.
	 * 
	 * @param integer
	 *            el entero.
	 * @param defaultValue
	 *            el valor por defecto.
	 * @return el entero indicado, si el valor indicado es nulo entonces se
	 *         regresa el entero por defecto indicado.
	 **/
	public static int defaultInteger(Integer integer, int defaultValue) {
		if (integer == null) {
			return defaultValue;
		}
		return integer;
	}

	/**
	 * Obtiene el entero indicado, si el valor indicado es nulo entonces se
	 * regresa 0.
	 * 
	 * @param integer
	 *            el entero.
	 * @return el entero indicado, si el valor indicado es nulo entonces se
	 *         regresa 0.
	 **/
	public static int defaultInteger(Integer integer) {
		return defaultInteger(integer, 0);
	}

	/**
	 * Obtiene el valor entero indicado, si el valor indicado no puede ser
	 * convertido a entero entonces se regresa el valor por defecto indicado.
	 * 
	 * @param integer
	 *            el valor entero que se ha de obtener.
	 * @param defaultValue
	 *            el valor por defecto.
	 * @return el valor entero indicado, si el valor indicado no puede ser
	 *         convertido a entero entonces se regresa el valor por defecto
	 *         indicado.
	 **/
	public static Integer defaultInteger(Object integer, Integer defaultValue) {
		String intStr = null;
		if (integer == null) {
			return defaultValue;
		}
		intStr = integer.toString().trim();
		if (!NumberUtils.isNumber(intStr)) {
			return defaultValue;
		}

		return Integer.valueOf(intStr);
	}

	/**
	 * Obtiene el valor entero indicado, si el valor indicado no puede ser
	 * convertido a entero entonces se regresa 0.
	 * 
	 * @param integer
	 *            el valor entero que se ha de obtener.
	 * @return el valor entero indicado, si el valor indicado no puede ser
	 *         convertido a entero entonces se regresa 0.
	 **/
	public static Integer defaultInteger(Object integer) {
		return defaultInteger(integer, NumberUtils.INTEGER_ZERO);
	}


	/**
	 * Crea un String con los elementos del array separados por la cadena
	 * indicada.
	 * 
	 * @param <T>
	 *            Variable para indicar el tipo de objetos que es valido recibir
	 * @param array
	 *            el array.
	 * @param separador
	 *            el separador de elementos de la cadena.
	 * @return un String con los elementos del array separados por la cadena
	 *         indicada.
	 **/
	public static <T> String arrayToString(T[] array, String separador) {
		if ((array == null) || (array.length <= 0)) {
			return StringUtils.EMPTY;
		}
		return collectionToString(Arrays.asList(array), separador);
	}

	/**
	 * Crea un String con los elementos de la coleccion separados por la cadena
	 * indicada.
	 * 
	 * @param <T>
	 *            Variable para indicar el tipo de objetos que es valido recibir
	 * @param collection
	 *            la coleccion.
	 * @param separador
	 *            el separador de elementos de la coleccion.
	 * @return un String con los elementos de la coleccion separados por la
	 *         cadena indicada.
	 **/
	public static <T> String collectionToString(Collection<T> collection,
			String separador) {
		StringBuilder buildStr = null;
		if ((collection == null) || collection.isEmpty()) {
			return StringUtils.EMPTY;
		}
		if (separador == null) {
			separador = StringUtils.EMPTY;
		}
		buildStr = new StringBuilder();
		for (T element : collection) {
			if (element != null) {
				buildStr.append(String
						.format("%s%s", separador, element.toString()));
			}
		}
		if (buildStr.length() > 0) {
			buildStr.delete(0, separador.length());
		}
		return buildStr.toString().trim();
	}

	/**
	 * Metodo que verifica de manera segura (sin arrojar NPE) si una coleccion
	 * contiene o no elementos. 
	 * @param <E> Tipo de objetos para las colecciones
	 * @param collection la coleccion a verificar.
	 * @return verifica de manera segura (sin arrojar NPE) si una coleccion
	 *         contiene o no elementos.
	 **/
	public static <E> boolean isCollectionEmpty(Collection<E> collection) {
		return (collection == null) || collection.isEmpty();
	}

	/**
	 * Metodo que verifica de manera segura (sin arrojar NPE) si un arreglo
	 * contiene o no elementos. 
	 * @param <E> Tipo de objetos para los arreglos
	 * @param array el arreglo a verificar.
	 * @return verifica de manera segura (sin arrojar NPE) si un arreglo
	 * contiene o no elementos.
	 **/
	public static <E> boolean isArrayEmpty(E[] array) {
		return (array == null) || (array.length <= 0);
	}
	
}
