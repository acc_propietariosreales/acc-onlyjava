/**
 * Isban Mexico
 *   Clase: UtileriasHelper.java
 *   Descripcion: Componente  de ayuda modificado para reutilizarse en diferentes proyectos
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 * NorkomCEJBClient UtileriasHelper.java
 */
package mx.isban.norkom.cuestionario.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

/**
 * @author Leopoldo F Espinosa R 24/02/2015
 *
 */
public class UtileriasHelper extends UtileriasString{

	/**
	 * El tamanyo en bytes de un Megabyte.
	 **/
	public static final long MEGABYTE = 1024L * 1024L;

	/**
	 * El formato de fecha por defecto.
	 **/
	public static final String FORMATO_FECHA_DEFAULT = "dd/MM/yyyy";

	/**
	 * Caracter de espacio en blanco.
	 **/
	public static final char ESPACIO_BLANCO = ' ';
	/**singleton utileriasHelp***/
	private static UtileriasHelper utileriasHelp;
	/**Constructor*/
	protected UtileriasHelper(){
	}
	/**getInstance
	 * @return UtileriasHelper  Singleton**/
	public UtileriasHelper getInstance(){
		if(utileriasHelp==null){
			utileriasHelp= new UtileriasHelper();
		}
		return utileriasHelp;
	}
	/**
	 * Obtiene el numero indicado en formato de Moneda para la localidad
	 * indicada.
	 * 
	 * @param currency
	 *            el numero que ha de ser formateado.
	 * @param locale
	 *            la localidad para el formato.
	 * @return el numero indicado en formato de Moneda para la localidad
	 *         indicada.
	 **/
	public static String getFormattedCurrency(BigDecimal currency, Locale locale) {
		Locale lclLocale = locale;
		NumberFormat format = null;
		if (currency == null) {
			return StringUtils.EMPTY;
		}
		if (lclLocale == null) {
			lclLocale = Locale.getDefault();
		}
		format = NumberFormat.getCurrencyInstance(lclLocale);
		format.setMaximumFractionDigits(2);
		format.setMinimumFractionDigits(2);
		format.setGroupingUsed(true);

		return format.format(currency.setScale(2, RoundingMode.HALF_EVEN)
				.doubleValue());
	}

	/**
	 * Obtiene la representacion en {@link BigDecimal} del objeto indicado.
	 * 
	 * @param bigDecimal
	 *            el objeto que se ha de convertir.
	 * @param defValue
	 *            el valor por defecto que se asigna en caso de que el valor a
	 *            convertir sea nulo.
	 * @return la representacion en {@link BigDecimal} del objeto indicado.
	 **/
	public static BigDecimal defaultBigDecimal(Object bigDecimal,
			BigDecimal defValue) {
		String strBigDecimal = null;
		if (bigDecimal == null) {
			return defValue;
		}
		if (bigDecimal instanceof BigDecimal) {
			return (BigDecimal) bigDecimal;
		}
		strBigDecimal = bigDecimal.toString();
		if (StringUtils.isBlank(strBigDecimal)) {
			return defValue;
		}
		if (!NumberUtils.isNumber(strBigDecimal)) {
			throw new IllegalArgumentException("Indicar un numero valido");
		}
		return new BigDecimal(strBigDecimal);
	}

	/**
	 * Obtiene la representacion en {@link BigDecimal} del objeto indicado. Si
	 * el objeto a converir es nulo se regresa el valor de
	 * {@link BigDecimal#ZERO}
	 * 
	 * @param bigDecimal
	 *            el objeto que se ha de convertir.
	 * @return la representacion en {@link BigDecimal} del objeto indicado.
	 **/
	public static BigDecimal defaultBigDecimal(Object bigDecimal) {
		return defaultBigDecimal(bigDecimal, BigDecimal.ZERO);
	}

	/**
	 * Obtiene el numero indicado en formato de Moneda para la localidad
	 * configurada en la JVM {@linkplain Locale#getDefault()}.
	 * 
	 * @param currency
	 *            el numero que ha de ser formateado.
	 * @return el numero indicado en formato de Moneda para la localidad
	 *         indicada.
	 **/
	public static String getFormattedCurrency(BigDecimal currency) {
		return getFormattedCurrency(currency, Locale.getDefault());
	}

	/**
	 * Obtiene el numero indicado en formato de Moneda para la localidad
	 * configurada en la JVM {@linkplain Locale#getDefault()}.
	 * 
	 * @param currency
	 *            el numero que ha de ser formateado.
	 * @return el numero indicado en formato de Moneda para la localidad
	 *         indicada.
	 **/
	public static String getFormattedCurrencyFromString(String currency) {
		return getFormattedCurrency(defaultBigDecimal(currency));
	}

	/**
	 * Obtiene la fecha indicada con el formato indicado.
	 * 
	 * @param fecha
	 *            la fecha que se ha de formatear.
	 * @param dateFormat
	 *            el formato de fecha por usar.
	 * @return la fecha indicada con el formato indicado.
	 **/
	public static String getFormattedDate(Date fecha, DateFormat dateFormat) {
		DateFormat lclFormat = dateFormat;
		if (fecha == null) {
			return StringUtils.EMPTY;
		}
		if (lclFormat == null) {
			lclFormat = DateFormat.getInstance();
		}
		return lclFormat.format(fecha);
	}

	/**
	 * Obtiene la fecha indicada con el formato configurado en la JVM obtenido
	 * mediante la llamada a {@link DateFormat#getInstance()}.
	 * 
	 * @param fecha
	 *            la fecha que se ha de formatear.
	 * @return la fecha indicada con el formato indicado.
	 **/
	public static String getFormattedDate(Date fecha) {
		return getFormattedDate(fecha, DateFormat.getInstance());
	}

	/**
	 * Separa la lista indicada por el numero de elementos.
	 * 
	 * @param <T>
	 *            Variable para indicar el tipo de objetos que es valido recibir
	 * @param lista
	 *            la lista que se va a separar.
	 * @param numeroElementos
	 *            el numero de elementos por el que se ha de separar la lista.
	 * @return una lista con las secciones creadas a partir de los argumentos
	 *         indicados.
	 **/
	public static <T> List<List<T>> separaListaPorNumeroElementos(
			List<T> lista, int numeroElementos) {
		List<List<T>> resultado = null;
		int iteraciones = 0;
		if (numeroElementos <= 0) {
			throw new IllegalArgumentException(
					"Indicar un numero de elementos mayor a cero");
		}
		resultado = new ArrayList<List<T>>();
		if ((lista == null) || lista.isEmpty()) {
			resultado.add(lista);
			return resultado;
		}
		if (lista.size() <= numeroElementos) {
			resultado.add(lista);
			return resultado;
		}
		iteraciones = (int) Math.ceil(lista.size() / (float) numeroElementos);
		for (int i = 0; i < iteraciones; i++) {
			final int count = i + 1;
			final int start = Math.max(((count - 1) * numeroElementos), 0);
			final int end = Math.min((count * numeroElementos), lista.size());
			resultado.add(lista.subList(start, end));
		}

		return resultado;
	}

	/**
	 * Obtiene el codigo del nombre de mes que se encuentra definido en el
	 * archivo de {@code general.properties} de la forma:</br> {@code
	 * general-calendario.mes.1=Enero}</br> {@code
	 * general-calendario.mes.2=Febrero}</br> {@code ...}
	 * 
	 * @param claveMes
	 *            la clave del mes.
	 * @return el codigo del nombre del mes indicado.
	 **/
	public static String getCodigoNombreMes(int claveMes) {
		final String fmt = "general-calendario.mes.%d";
		if ((claveMes <= 0) || (claveMes > 12)) {
			throw new IllegalArgumentException("Clave de mes incorrecta");
		}
		return String.format(fmt, claveMes);
	}

	/**
	 * Obtiene la fecha actual en el formato por defecto del sistema.
	 * 
	 * @see #FORMATO_FECHA_DEFAULT
	 * @return la fecha actual en el formato por defecto del sistema.
	 **/
	public static String getFechaActual() {
		return getFormattedDate(new Date(), new SimpleDateFormat(
				FORMATO_FECHA_DEFAULT,Locale.getDefault()));
	}

	/**
	 * Compara las fechas indicadas como cadenas.
	 * 
	 * @param fecha1
	 *            la fecha 1 a comparar.
	 * @param fecha2
	 *            la fecha 2 a comparar.
	 * @param formato
	 *            el formato de las fechas.
	 * @return el caracter indicador de la comparacion de fechas
	 *         {@link Date#compareTo(Date)}.
	 * @throws ParseException
	 *             si alguna de las fechas no se encuentra en el formato
	 *             indicado.
	 * @throws ParseException
	 *             si no se indica el formato.
	 **/
	public static int compareFecha(String fecha1, String fecha2,
			DateFormat formato) throws ParseException {

		if (formato == null) {
			throw new IllegalArgumentException("Indicar el formato de la fecha");
		}
		if (StringUtils.isBlank(fecha1)) {
			if (StringUtils.isBlank(fecha2)) {
				return 0;
			}
			return -1;
		}
		if (StringUtils.isBlank(fecha2)) {
			return 1;
		}

		return formato.parse(fecha1).compareTo(formato.parse(fecha2));
	}

	/**
	 * Compara las fechas indicadas como cadenas con el formato por defecto del
	 * sistrma {@link #FORMATO_FECHA_DEFAULT}.
	 * 
	 * @param fecha1
	 *            la fecha 1 a comparar.
	 * @param fecha2
	 *            la fecha 2 a comparar.
	 * @return el caracter indicador de la comparacion de fechas
	 *         {@link Date#compareTo(Date)}.
	 * @throws ParseException
	 *             si alguna de las fechas no se encuentra en el formato
	 *             indicado.
	 **/
	public static int compareFecha(String fecha1, String fecha2)
			throws ParseException {
		return compareFecha(fecha1, fecha2, new SimpleDateFormat(
				FORMATO_FECHA_DEFAULT,Locale.getDefault()));
	}

	/**
	 * Obtiene la representacion en megabytes de los bytes indicados.
	 * 
	 * @param bytes
	 *            Los bytes
	 * @return bytesToMegabytes la representacion en megabytes de los bytes
	 *         indicados.
	 **/
	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	
	
public static long diasDiferenciaAldia(String fechayyyymmdd){
	final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000; //Milisegundos al dia 
	java.util.Date hoy = new Date(); //Fecha de hoy 
	     
	int año = Integer.parseInt(fechayyyymmdd.substring(0, 4));
	int mes = Integer.parseInt(fechayyyymmdd.substring(4, 6));
	int dia = Integer.parseInt(fechayyyymmdd.substring(6, 8));; //Fecha anterior 
	
	Calendar calendar = new GregorianCalendar(año, mes-1, dia); 
	java.sql.Date fecha = new java.sql.Date(calendar.getTimeInMillis());

	long diferencia = ( hoy.getTime() - fecha.getTime() )/MILLSECS_PER_DAY; 
	
	return diferencia;
}
	
/***
 * metodo para validar la fecha del año 1900 a 2200, mes del 01 al 12
 * @param input fecha a 
 * @return true si es o no valido el formato
 */
public static boolean isValidDate(String input) {
	if(input.length()!=8){
		return false;
	}
     try {
    	  if(Integer.parseInt(input.substring(0, 4)) > 1900 && Integer.parseInt(input.substring(0, 4)) < 2100){
    		  if(Integer.parseInt(input.substring(4, 6)) >0 && Integer.parseInt(input.substring(4, 6)) <=12 ){
    			if(Integer.parseInt(input.substring(6,8)) >0 && Integer.parseInt(input.substring(6,8)) <=31){
    				return true;
    			} else {
    				return false;
    			} 
    		  }else{
    			  return false;  
    		  }
    	  }else{
    		  return false;
    	  }
     }catch(NumberFormatException e){
          return false;
     }
}

}
