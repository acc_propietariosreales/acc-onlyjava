/**
 * Isban Mexico
 *   Clase: UtileriasHelper2.java
 *   Descripcion: Componente  de ayuda modificado para reutilizarse en diferentes proyectos
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 * NorkomCEJBClient UtileriasHelper2.java
 */
package mx.isban.norkom.cuestionario.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author Leopoldo F Espinosa R 24/02/2015
 *
 */
public class UtileriasString {
	/**singleton**/
	private transient UtileriasString utileriasStr;
	
	/**Constructor**/
	protected UtileriasString(){
		
	}
	/***
	 * singleton
	 * @return utileriasString instance
	 */
	public UtileriasString getInstance(){
		if(utileriasStr==null){
			utileriasStr= new UtileriasString();
		}
		return utileriasStr;
	}
	/**
	 * Rellena con los espacios a la derecha indicados la cadena indicada.
	 * 
	 * @param str
	 *            la cadena que se ha de rellenar.
	 * @param size
	 *            los espacios de relleno a la derecha.
	 * @return la cadena indicada con los espacios de relleno.
	 **/
	public static String defaultRightPad(Object str, int size) {
		final String defString = defaultStringIfBlank(str);
		if (defString.length() > size) {
			return defString.substring(0, size);
		}
		return StringUtils.rightPad(defString, size);
	}

	/**
	 * Rellena con los espacios a la izquierda indicados la cadena indicada.
	 * 
	 * @param str
	 *            la cadena que se ha de rellenar.
	 * @param size
	 *            los espacios de relleno a la izquierda.
	 * @param relleno
	 *            el caracter de relleno.
	 * @return la cadena indicada con los espacios de relleno.
	 **/
	public static String defaultLeftPad(Object str, int size, char relleno) {
		final String defString = defaultStringIfBlank(str);
		if (defString.length() > size) {
			return defString.substring(defString.length() - size);
		}
		return StringUtils.leftPad(defString, size, relleno);
	}

	/**
	 * Rellena con los espacios a la izquierda indicados la cadena indicada,
	 * dependiendo si la cadena se encuentra vacia o no.
	 * 
	 * @param str
	 *            la cadena que se ha de rellenar.
	 * @param size
	 *            los espacios de relleno a la izquierda.
	 * @param rellenoVacio
	 *            el caracter de relleno si la cadena es vacia.
	 * @param rellenoNoVacio
	 *            el caracter de relleno si la cadena no es vacia.
	 * @return la cadena indicada con los espacios de relleno.
	 **/
	public static String defaultLeftPad(Object str, int size,
			char rellenoVacio, char rellenoNoVacio) {
		final String string = defaultStringIfBlank(str);
		if (StringUtils.isBlank(string)) {
			return defaultLeftPad(defaultStringIfBlank(str), size, rellenoVacio);
		}
		return defaultLeftPad(defaultStringIfBlank(str), size, rellenoNoVacio);
	}
	/**
	 * Obtiene el valor {@code String} por defecto en caso de que el objeto
	 * {@code String} indicado sea vacio de acuerdo a
	 * {@link StringUtils#isBlank(String)}.
	 * 
	 * @param string
	 *            el valor {@code String} a evaluar.
	 * @param defaultValue
	 *            el valor por defecto.
	 * @return el valor {@code String} por defecto en caso de que el objeto
	 *         {@code String} indicado sea vacio de acuerdo a
	 *         {@link StringUtils#isBlank(String)}.
	 **/
	public static String defaultStringIfBlank(Object string, String defaultValue) {
		if (string == null) {
			return defaultValue;
		}
		if (StringUtils.isBlank(string.toString())) {
			return defaultValue;
		}
		return string.toString();
	}

	/**
	 * Obtiene el valor de cadena vacia, {@link StringUtils#EMPTY}, en caso de
	 * que el objeto {@code String} indicado sea vacio de acuerdo a
	 * {@link StringUtils#isBlank(String)}.
	 * 
	 * @param string
	 *            el valor {@code String} a evaluar.
	 * @return el valor de cadena vacia, {@link StringUtils#EMPTY}, en caso de
	 *         que el objeto {@code String} indicado sea vacio de acuerdo a
	 *         {@link StringUtils#isBlank(String)}.
	 **/
	public static String defaultStringIfBlank(Object string) {
		return defaultStringIfBlank(string, StringUtils.EMPTY);
	}
	
	/**
	 * Abrevia una cadena usando los tres puntos. Esto cambiaria "Ahora es el
	 * tiempo de los hombres buenos" a "Ahora es el tiempo...".<br/>
	 * Si la cadena es nula entonces se regresa una cadena vacia.<br/>
	 * Si maxWidth es menor a 4 se lanza un {@code IllegalArgumentException}.
	 * 
	 * @param str
	 *            la cadena a checar.
	 * @param maxWidth
	 *            el tamanio maximo de la cadena, debe ser al menos 4.
	 * @return la cadena abreviada, si la cadena es nula entonces se regresa una
	 *         cadena vacia.
	 **/
	public static final String defaultAbbreviate(String str, int maxWidth) {
		return StringUtils.abbreviate(defaultStringIfBlank(str), maxWidth - 1);
	}

	/**
	 * Abrevia una cadena usando los tres puntos. Esto cambiaria "Ahora es el
	 * tiempo de los hombres buenos" a "Ahora es el tiempo...".<br/>
	 * Si la cadena es nula entonces se regresa el valor por defecto indicado.<br/>
	 * Si maxWidth es menor a 4 se lanza un {@code IllegalArgumentException}.
	 * 
	 * @param str
	 *            la cadena a checar.
	 * @param maxWidth
	 *            el tamanio maximo de la cadena, debe ser al menos 4.
	 * @param defaultValue
	 *            el valor por defecto.
	 * @return la cadena abreviada, si la cadena es nula entonces se regresa el
	 *         valor por defecto.
	 **/
	public static final String defaultAbbreviate(String str, int maxWidth, String defaultValue) {
		return StringUtils.abbreviate(defaultStringIfBlank(str, defaultValue),
				maxWidth);
	}
}
