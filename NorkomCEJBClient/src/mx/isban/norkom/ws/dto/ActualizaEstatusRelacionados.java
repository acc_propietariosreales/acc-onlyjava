/**
 * ActualizaEstatusRelacionados
 * clase para recibir los datos 
 * requeridos para actualizar los
 * estatus de los relacionados por
 * bloque segun Norkom
 */
package mx.isban.norkom.ws.dto;

import java.io.Serializable;

/**
 * Clase para recibir los datos que se van a actualizar
 * con el estatus de norkom
 * @author lespinosa
 * 
 */
public class ActualizaEstatusRelacionados implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1996340468684552240L;
	/**
	 * id del formulario
	 */
	private String idformulario;
	/**
	 * numero de bloque a actualizar
	 * por formulario
	 */
	private String numeroBloque;
	/**
	 * Obtiene el valor de idformulario
	 * @return El valor de idformulario
	 */
	public String getIdformulario() {
		return idformulario;
	}
	/**
	 * Define el nuevo valor para idformulario
	 * @param idformulario El nuevo valor de idformulario
	 */
	public void setIdformulario(String idformulario) {
		this.idformulario = idformulario;
	}
	/**
	 * Obtiene el valor de numeroBloque
	 * @return El valor de numeroBloque
	 */
	public String getNumeroBloque() {
		return numeroBloque;
	}
	/**
	 * Define el nuevo valor para numeroBloque
	 * @param numeroBloque El nuevo valor de numeroBloque
	 */
	public void setNumeroBloque(String numeroBloque) {
		this.numeroBloque = numeroBloque;
	}
	
}
