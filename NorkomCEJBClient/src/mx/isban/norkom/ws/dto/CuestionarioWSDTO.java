/**
 * Isban Mexico
 *   Clase: CuestionarioWSDTO.java
 *   Descripcion: DTO de datos de los cuestionarios utilizado por un WS
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;


import mx.isban.norkom.cuestionario.dto.ClienteDTO;

/**
 * @author Leopoldo F Espinosa R
 * 
 *
 */
public class CuestionarioWSDTO extends CuestionarioWSDTOHelper  {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 6955369892885661424L;
	
	/** 
	 * Variable utilizada para declarar codigoSubproducto
	 */
	private String codigoSubproducto;
	/** 
	 * Variable utilizada para declarar descSubproducto
	 */
	private String descSubproducto;
	/** 
	 * Variable utilizada para declarar codigoPais
	 */
	private String codigoPais;
	/** 
	 * Variable utilizada para declarar codigoMunicipio
	 */
	private String codigoMunicipio;
	/** 
	 * Variable utilizada para declarar codigoNacionalidad
	 */
	private String codigoNacionalidad;
	/** 
	 * Variable utilizada para declarar codigoBranch
	 */
	private String codigoBranch;
	/** 
	 * Variable utilizada para declarar codigoCentroCostos
	 */
	private String codigoCentroCostos;
	/** 
	 * Variable utilizada para declarar codigoTipoFormulario
	 */
	private String codigoTipoFormulario;
	/** 
	 * Variable utilizada para declarar calificacion
	 */
	/**tipo de custionario preliminar o subtipo A1-A2-A3 **/
	private String calificacion;
	/** 
	 * Variable utilizada para declarar descripcion
	 */
	private String descripcion;
	/** 
	 * Variable utilizada para declarar fechaCreacion
	 */
	private String fechaCreacion;
	/** 
	 * Variable utilizada para declarar claveClienteCuestionario
	 */
	private int claveClienteCuestionario;
	/** 
	 * Variable utilizada para declarar claveCuestionario
	 */
	private int claveCuestionario;
	/** 
	 * Variable utilizada para declarar idCuestionario
	 */
	private String idCuestionario;
	/** 
	 * Variable utilizada para declarar cliente
	 */
	private ClienteDTO cliente;


	/**
	 * @return String calificacion
	 */
	public String getCalificacion() {
		return calificacion;
	}
	/**
	 * @param calificacion para Calificacion
	 */
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	/**
	 * @param descripcion para Descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return String descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param fecha para Fecha de Creacion
	 */
	public void setFechaCreacion(String fecha) {
		this.fechaCreacion = fecha;
	}
	/**
	 * @return ClienteDTO con datos del cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}
	/**
	 * @param cliente a agregar
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	/**
	 * @return int id cliente asignado
	 */
	public int getIdClienteAsignado(){
		if(cliente == null){
			return 0;
		}
		
		return cliente.getIdCliente();
	}
	/**
	 * @return String nombre completo del cliente
	 */
	public String getNombreCompletoCliente(){
		if(cliente == null){
			return "No hay datos";
		}
		
		return cliente.getNombreCompleto();
	}
	/**
	 * @return String codigo de cliente
	 */
	public String getCodigoCliente(){
		if(cliente == null){
			return "No hay datos";
		}
		
		return cliente.getCodigoCliente();
	}

	/**
	 * @return String fecha de creacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	
	/**
	 * @return String codigo de subproducto
	 */
	public String getCodigoSubproducto() {
		return codigoSubproducto;
	}
	/**
	 * @param codigoSubproducto para Codigo de Subproducto
	 */
	public void setCodigoSubproducto(String codigoSubproducto) {
		this.codigoSubproducto = codigoSubproducto;
	}
	/**
	 * @return String descripcion de subproducto
	 */
	public String getDescSubproducto() {
		return descSubproducto;
	}
	/**
	 * @param descSubproducto para Descripcion de Subproducto
	 */
	public void setDescSubproducto(String descSubproducto) {
		this.descSubproducto = descSubproducto;
	}
	/**
	 * @return String codigo pais
	 */
	public String getCodigoPais() {
		return codigoPais;
	}
	/**
	 * @param codigoPais para Codigo de Pais
	 */
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	/**
	 * @return String Codigo de municipio
	 */
	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}
	/**
	 * @param codigoMunicipio para Codigo de Municipio
	 */
	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}
	/**
	 * @return String codigo de nacionalidad
	 */
	public String getCodigoNacionalidad() {
		return codigoNacionalidad;
	}
	/**
	 * @param codigoNacionalidad para Codigo de Nacionalidad
	 */
	public void setCodigoNacionalidad(String codigoNacionalidad) {
		this.codigoNacionalidad = codigoNacionalidad;
	}
	/**
	 * @return String codigo de branch
	 */
	public String getCodigoBranch() {
		return codigoBranch;
	}
	/**
	 * @param codigoBranch para Codigo Branch
	 */
	public void setCodigoBranch(String codigoBranch) {
		this.codigoBranch = codigoBranch;
	}
	/**
	 * @return String codigo centro de costos
	 */
	public String getCodigoCentroCostos() {
		return codigoCentroCostos;
	}
	/**
	 * @param codigoCentroCostos para Codigo de Centro de Costos
	 */
	public void setCodigoCentroCostos(String codigoCentroCostos) {
		this.codigoCentroCostos = codigoCentroCostos;
	}
	/**
	 * @return String codigo tipo formulario
	 */
	public String getCodigoTipoFormulario() {
		return codigoTipoFormulario;
	}
	/**
	 * @param codigoTipoFormulario para Codigo de Tipo de Formulario
	 */
	public void setCodigoTipoFormulario(String codigoTipoFormulario) {
		this.codigoTipoFormulario = codigoTipoFormulario;
	}
	
	/**
	 * @return int clave cliente cuestionario
	 */
	public int getClaveClienteCuestionario() {
		return claveClienteCuestionario;
	}
	/**
	 * @param claveClienteCuestionario para Clave de Cliente-Cuestionario
	 */
	public void setClaveClienteCuestionario(int claveClienteCuestionario) {
		this.claveClienteCuestionario = claveClienteCuestionario;
	}
	/**
	 * @return int clave cuestionario
	 */
	public int getClaveCuestionario() {
		return claveCuestionario;
	}
	/**
	 * @param claveCuestionario para Clave de Cuestionario
	 */
	public void setClaveCuestionario(int claveCuestionario) {
		this.claveCuestionario = claveCuestionario;
	}
	/**
	 * @return String id cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * @param idCuestionario para Id de Cuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}
	

}