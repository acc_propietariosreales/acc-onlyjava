/**
 * Isban Mexico
 *   Clase: CuestionarioWSDTOHelper.java
 *   Descripcion: DTO de datos de los cuestionarios utilizado por un WS generado por observaciones de sonar
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import mx.isban.norkom.cuestionario.dto.ActividadEconomicaDTO;

public class CuestionarioWSDTOHelper extends CuestionarioWSDTOHelper2{

	
	/**
	 * CuestionarioWSDTOHelper.java  de tipo long
	 */
	private static final long serialVersionUID = 4656331348311987328L;
	/** 
	 * Variable utilizada para declarar codigoSegmento
	 */
	private String codigoSegmento;
	/** 
	 * Variable utilizada para declarar descSegmento
	 */
	private String descSegmento;

	/** 
	 * Variable utilizada para declarar codigoProducto
	 */
	private String codigoProducto;
	/** 
	 * Variable utilizada para declarar nombreCuestionario
	 */
	private String nombreCuestionario;
	/** 
	 * Variable utilizada para declarar versionCuestionario
	 */
	private String versionCuestionario;

	/** 
	 * Variable utilizada para declarar codigoSucursal
	 */
	private String codigoSucursal;
	/** 
	 * Variable utilizada para declarar nombreSucursal
	 */ 
	private String nombreSucursal;
	/** 
	 * Variable utilizada para declarar descProducto
	 */
	private String descProducto;
	/** 
	 * Variable utilizada para declarar numeroContrato
	 */
	private String numeroContrato;
	/** 
	 * Variable utilizada para declarar idAplicacion
	 */
	private String idAplicacion;

	/** 
	 * Variable utilizada para declarar actividadGenerica
	 */
	private ActividadEconomicaDTO actividadGenerica;
	/** 
	 * Variable utilizada para declarar actividadEspecifica
	 */
	private ActividadEconomicaDTO actividadEspecifica;
	
	/** 
	 * Variable utilizada para declarar tipoPersona
	 */
	private String tipoPersona;
	/** 
	 * Variable utilizada para declarar subtipoPersona
	 */
	private String subtipoPersona;
	/** 
	 * Variable utilizada para declarar divisa
	 */
	private String divisa;

	/**
	 * @return String codigo de sucursal
	 */
	public String getCodigoSucursal() {
		return codigoSucursal;
	}
	/**
	 * @param codigoSucursal para Codigo de Sucursal
	 */
	public void setCodigoSucursal(String codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}
	/**
	 * @return String nombre de sucursal
	 */
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	/**
	 * @param nombreSucursal para Nombre de Sucursal
	 */
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	/**
	 * @return String nombre cuestionario
	 */
	public String getNombreCuestionario() {
		return nombreCuestionario;
	}
	/**
	 * @param nombreCuestionario para Nombre de Cuestionario
	 */
	public void setNombreCuestionario(String nombreCuestionario) {
		this.nombreCuestionario = nombreCuestionario;
	}
	
	/**
	 * @return String codigo de segmento
	 */
	public String getCodigoSegmento() {
		return codigoSegmento;
	}
	/**
	 * @param codigoSegmento para Codigo de Segmento
	 */
	public void setCodigoSegmento(String codigoSegmento) {
		this.codigoSegmento = codigoSegmento;
	}
	/**
	 * @return String descripcion de segmento
	 */
	public String getDescSegmento() {
		return descSegmento;
	}
	/**
	 * @param descSegmento para Descripcion de Segmento
	 */
	public void setDescSegmento(String descSegmento) {
		this.descSegmento = descSegmento;
	}
	/**
	 * @return String codigo de producto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto para Codigo de Producto
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
		/**
	 * @return String version cuestionario
	 */
	public String getVersionCuestionario() {
		return versionCuestionario;
	}
	/**
	 * @param versionCuestionario para Version de Cuestionario
	 */
	public void setVersionCuestionario(String versionCuestionario) {
		this.versionCuestionario = versionCuestionario;
	}
	
	/**
	 * @return int id actividad generica
	 */
	public int getIdActividadGenerica() {
		if(this.actividadGenerica == null){
			return 0;
		}
		
		return this.actividadGenerica.getIdActividad();
	}
	/**
	 * @return String codigo act generica
	 */
	public String getCodigoActGenerica(){
		if(this.actividadGenerica == null){
			return "";
		}
		
		return this.actividadGenerica.getCodigo();
	}

	/**
	 * @return int actividad especifica
	 */
	public int getIdActividadEspecifica() {
		if(this.actividadEspecifica == null){
			return 0;
		}
		
		return this.actividadEspecifica.getIdActividad();
	}
	/**
	 * @return String codigo act especifica
	 */
	public String getCodigoActEspecifica(){
		if(this.actividadEspecifica == null){
			return "";
		}
		
		return this.actividadEspecifica.getCodigo();
	}

		
	/**
	 * 
	 * @return String descripcion de producto
	 */
	public String getDescProducto() {
		return descProducto;
	}
	/**
	 * @param descProducto para Descripcion de Producto
	 */
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	/**
	 * @return String numero de contrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato para Numero de Contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return String id de aplicacion
	 */
	public String getIdAplicacion() {
		return idAplicacion;
	}
		/**
	 * @param idAplicacion para Id de Aplicacion
	 */
	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	
	/**
	 * @return String tipo persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoPersona para Tipo de Persona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return String subtipo de persona
	 */
	public String getSubtipoPersona() {
		return subtipoPersona;
	}
	/**
	 * @param subtipoPersona para Subtipo Persona
	 */
	public void setSubtipoPersona(String subtipoPersona) {
		this.subtipoPersona = subtipoPersona;
	}
	/**
	 * @return String divisa
	 */
	public String getDivisa() {
		return divisa;
	}
	/**
	 * @param divisa para Divisa
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}
	/**
	 * @return ActividadEconomicaDTO con datos de la actividad generica
	 */
	public ActividadEconomicaDTO getActividadGenerica() {
		return actividadGenerica;
	}
	/**
	 * @param actividadGenerica para Actividad Generica
	 */
	public void setActividadGenerica(ActividadEconomicaDTO actividadGenerica) {
		this.actividadGenerica = actividadGenerica;
	}
	/**
	 * @return ActividadeEconomicaDTO con datos de la actividad
	 */
	public ActividadEconomicaDTO getActividadEspecifica() {
		return actividadEspecifica;
	}
	/**
	 * @param actividadEspecifica para Actividad Especifica
	 */
	public void setActividadEspecifica(ActividadEconomicaDTO actividadEspecifica) {
		this.actividadEspecifica = actividadEspecifica;
	}
}
