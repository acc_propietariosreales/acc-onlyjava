/**
 * Isban Mexico
 *   Clase: CuestionarioWSDTOHelper2.java
 *   Descripcion: DTO de datos de los cuestionarios utilizado por un WS generado por observaciones de sonar
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.io.Serializable;

public class CuestionarioWSDTOHelper2 implements Serializable{

	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 8648672297654202491L;
	/**
	 * Clave del cuestionario IP asignado
	 */
	private int claveCuestionarioIP;
	
	/**
	 * Nivel de Riesgo  de Norkom
	 */
	private String nivelRiesgo;
	
	/**
	 * Indicador UPLD de Norkom
	 */
	private String indicadorUpld;
	
	/** 
	 * Variable utilizada para declarar codigoRegion
	 */
	private String codigoRegion;
	/** 
	 * Variable utilizada para declarar nombreRegion
	 */
	private String nombreRegion;
	/** 
	 * Variable utilizada para declarar codigoPlaza
	 */
	private String codigoPlaza;
	/** 
	 * Variable utilizada para declarar nombrePlaza
	 */
	private String nombrePlaza;
	/** 
	 * Variable utilizada para declarar codigoEntidad
	 */
	private String codigoEntidad;
	
	/**
	 * @return String codigo de region
	 */
	public String getCodigoRegion() {
		return codigoRegion;
	}
	/**
	 * @param codigoRegion para Codigo de Region
	 */
	public void setCodigoRegion(String codigoRegion) {
		this.codigoRegion = codigoRegion;
	}
	/**
	 * @return String nombre de region
	 */
	public String getNombreRegion() {
		return nombreRegion;
	}
	/**
	 * @param nombreRegion para Nombre de Region
	 */
	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}
	/**
	 * @return String codigo de plaza
	 */
	public String getCodigoPlaza() {
		return codigoPlaza;
	}
	/**
	 * @param codigoPlaza para Codigo de Plaza
	 */
	public void setCodigoPlaza(String codigoPlaza) {
		this.codigoPlaza = codigoPlaza;
	}
	/**
	 * @return String nombre de plaza
	 */
	public String getNombrePlaza() {
		return nombrePlaza;
	}
	/**
	 * @param nombrePlaza para Nombre de Plaza
	 */
	public void setNombrePlaza(String nombrePlaza) {
		this.nombrePlaza = nombrePlaza;
	}
	/**
	 * @return Codigo de Entidad
	 */
	public String getCodigoEntidad() {
		return codigoEntidad;
	}
	/**
	 * @param codigoEntidad para Codigo de Entidad
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	
	/**
	 * Obtiene la Clave del Cuestionario IP asignado
	 * @return Clave del cuestionario IP
	 */
	public int getClaveCuestionarioIP() {
		return claveCuestionarioIP;
	}

	/**
	 * Define el valor para Clave del cuestionario IP
	 * @param claveCuestionarioIP La nueva Clave del cuestionario IP
	 */
	public void setClaveCuestionarioIP(int claveCuestionarioIP) {
		this.claveCuestionarioIP = claveCuestionarioIP;
	}
	/**
	 * Obtiene el valor de nivelRiesgo
	 * @return El valor de nivelRiesgo
	 */
	public String getNivelRiesgo() {
		return nivelRiesgo;
	}
	/**
	 * Define el nuevo valor para nivelRiesgo
	 * @param nivelRiesgo El nuevo valor de nivelRiesgo
	 */
	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}
	/**
	 * Obtiene el valor de indicadorUpld
	 * @return El valor de indicadorUpld
	 */
	public String getIndicadorUpld() {
		return indicadorUpld;
	}
	/**
	 * Define el nuevo valor para indicadorUpld
	 * @param indicadorUpld El nuevo valor de indicadorUpld
	 */
	public void setIndicadorUpld(String indicadorUpld) {
		this.indicadorUpld = indicadorUpld;
	}
}
