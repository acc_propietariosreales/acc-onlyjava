/**
 * Isban Mexico
 *   Clase: CuestionariosWebServiceDTO.java
 *   Descripcion: DTO de cuestionarios
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.ws.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CuestionariosWebServiceDTO extends RespuestaWebService implements Serializable {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -8430233449105637896L;
	/**
	 * Variable utilizada para declarar idCuestionario
	 */
	private String idCuestionario;
	/**
	 * Variable utilizada para declarar cuestionarios
	 */
	private List<CuestionarioWSDTO> cuestionarios;
	
	/**
	 * 
	 * @return String id cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * 
	 * @param idCuestionario de setIdCuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
		
	}
	
	/**
	 * 
	 * @return ArrayList<CuestionarioWSDTO> lista de cuestionarios
	 */
	public List<CuestionarioWSDTO> getCuestionarios() {
		return cuestionarios;
	}
	/**
	 * 
	 * @param cuestionarios de setCuestionarios
	 */
	public void setCuestionarios(List<CuestionarioWSDTO> cuestionarios) {
		this.cuestionarios = cuestionarios;
	}
	/**
	 * 
	 * @param cuestionario de tipo CuestionarioWSDTO a agregar
	 */
	public void addCuestionario(CuestionarioWSDTO cuestionario){
		if(this.cuestionarios == null) {
			this.cuestionarios = new ArrayList<CuestionarioWSDTO>();
		}
		
		this.cuestionarios.add(cuestionario);
	}
}
