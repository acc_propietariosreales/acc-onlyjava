/**
 * Clase de ayuda por revision de
 * sonar para los WS de Relacionados
 * 17/07/2017
 * Stefanini
 */
package mx.isban.norkom.ws.dto;


/**
 * @author lespinosa
 *
 */
public class DatosRelacionadoGeneral extends
		DatosRelacionadosParteDos {

	/**
	 * serialVersionUID
	 * long
	 */
	private static final long serialVersionUID = -5626002334133007730L;
	/**
	 * Variable utilizada para declarar idFormulario
	 */
	protected String idFormulario = "";
	/**
	 * Variable utilizada para declarar descAplicativo
	 */
	protected String descAplicativo = "";
	/**
	 * Variable utilizada para declarar fechaFormulario
	 */
	protected String fechaFormulario = "";
	/**
	 * Variable utilizada para declarar buc
	 */
	protected String buc = "";
	/**
	 * Variable utilizada para declarar entidad
	 */
	protected String entidad = "";
	/**
	 * Obtiene el valor de idFormulario
	 * @return El valor de idFormulario
	 */
	public String getIdFormulario() {
		return idFormulario;
	}

	/**
	 * Define el nuevo valor para idFormulario
	 * @param idFormulario El nuevo valor de idFormulario
	 */
	public void setIdFormulario(String idFormulario) {
		this.idFormulario = idFormulario;
	}
	
	/**
	 * Obtiene el valor de fechaFormulario
	 * @return El valor de fechaFormulario
	 */
	public String getFechaFormulario() {
		return fechaFormulario;
	}

	/**
	 * Define el nuevo valor para fechaFormulario
	 * @param fechaFormulario El nuevo valor de fechaFormulario
	 */
	public void setFechaFormulario(String fechaFormulario) {
		this.fechaFormulario = fechaFormulario;
	}
	

	/**
	 * Obtiene el valor de buc
	 * @return El valor de buc
	 */
	public String getBuc() {
		return buc;
	}

	/**
	 * Define el nuevo valor para buc
	 * @param buc El nuevo valor de buc
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	
	/**
	 * Obtiene el valor de descAplicativo
	 * @return El valor de descAplicativo
	 */
	public String getDescAplicativo() {
		return descAplicativo;
	}

	/**
	 * Define el nuevo valor para descAplicativo
	 * @param descAplicativo El nuevo valor de descAplicativo
	 */
	public void setDescAplicativo(String descAplicativo) {
		this.descAplicativo = descAplicativo;
	}




	/**
	 * Obtiene el valor de entidad
	 * @return El valor de entidad
	 */
	public String getEntidad() {
		return entidad;
	}

	/**
	 * Define el nuevo valor para entidad
	 * @param entidad El nuevo valor de entidad
	 */
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}


}