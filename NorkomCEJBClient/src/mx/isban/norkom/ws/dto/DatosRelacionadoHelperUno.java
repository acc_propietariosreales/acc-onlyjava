/**
 * 
 */
package mx.isban.norkom.ws.dto;



/**
 * @author lespinosa
 *
 */
public class DatosRelacionadoHelperUno extends DatosRelacionadosHelperEstatus {

	/**
	 *  implements Serializable
	 *  serialVersionUID
	 */
	private static final long serialVersionUID = -8677481708159860349L;
	/**
	 * Variable utilizada para declarar divisa
	 */
	private String divisa = "";
	/**
	 * Variable utilizada para declarar codigoCentroCostos
	 */
	private String codigoCentroCostos = "";
	/**
	 * Variable utilizada para declarar pep
	 */
	private String pep = "";
	/**
	 * Variable utilizada para declarar cvDiv
	 */
	private String cvDiv = "";
	
	/**
	 * Obtiene el valor de divisa
	 * @return El valor de divisa
	 */
	public String getDivisa() {
		return divisa;
	}

	/**
	 * Define el nuevo valor para divisa
	 * @param divisa El nuevo valor de divisa
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * Obtiene el valor de codigoCentroCostos
	 * @return El valor de codigoCentroCostos
	 */
	public String getCodigoCentroCostos() {
		return codigoCentroCostos;
	}

	/**
	 * Define el nuevo valor para codigoCentroCostos
	 * @param codigoCentroCostos El nuevo valor de codigoCentroCostos
	 */
	public void setCodigoCentroCostos(String codigoCentroCostos) {
		this.codigoCentroCostos = codigoCentroCostos;
	}

	/**
	 * Obtiene el valor de pep
	 * @return El valor de pep
	 */
	public String getPep() {
		return pep;
	}

	/**
	 * Define el nuevo valor para pep
	 * @param pep El nuevo valor de pep
	 */
	public void setPep(String pep) {
		this.pep = pep;
	}

	/**
	 * Obtiene el valor de cvDiv
	 * @return El valor de cvDiv
	 */
	public String getCvDiv() {
		return cvDiv;
	}

	/**
	 * Define el nuevo valor para cvDiv
	 * @param cvDiv El nuevo valor de cvDiv
	 */
	public void setCvDiv(String cvDiv) {
		this.cvDiv = cvDiv;
	}

	
}