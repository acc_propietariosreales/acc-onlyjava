/**
 * 
 */
package mx.isban.norkom.ws.dto;

import java.io.Serializable;



/**
 * @author lespinosa
 *
 */
public class DatosRelacionadosHelperEstatus implements Serializable{

	/**
	 * Srial UIDA
	 * long
	 */
	private static final long serialVersionUID = 7746062594377134478L;
	/**
	 * Variable utilizada para declarar paisTransferenciasInternacionales
	 */
	private String paisTransferenciasInternacionales = "";
	/**
	 * Variable utilizada para declarar estatusRelacionWS
	 */
	private String estatusRelacionWS = "";

	/**
	 * Obtiene el valor de estatusRelacionWS
	 * @return El valor de estatusRelacionWS
	 */
	public String getEstatusRelacionWS() {
		return estatusRelacionWS;
	}

	/**
	 * Define el nuevo valor para estatusRelacionWS
	 * @param estatusRelacionWS El nuevo valor de estatusRelacionWS
	 */
	public void setEstatusRelacionWS(String estatusRelacionWS) {
		this.estatusRelacionWS = estatusRelacionWS;
	}

	/**
	 * Obtiene el valor de paisTransferenciasInternacionales
	 * @return El valor de paisTransferenciasInternacionales
	 */
	public String getPaisTransferenciasInternacionales() {
		return paisTransferenciasInternacionales;
	}

	/**
	 * Define el nuevo valor para paisTransferenciasInternacionales
	 * @param paisTransferenciasInternacionales El nuevo valor de paisTransferenciasInternacionales
	 */
	public void setPaisTransferenciasInternacionales(String paisTransferenciasInternacionales) {
		this.paisTransferenciasInternacionales = paisTransferenciasInternacionales;
	}

}