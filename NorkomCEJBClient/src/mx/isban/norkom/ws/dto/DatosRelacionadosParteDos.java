/**
 * 
 */
package mx.isban.norkom.ws.dto;

/**
 * clase xtra por reglas de sonar 
 * en numero demetodos
 * @author lespinosa
 *
 */
public class DatosRelacionadosParteDos extends
		ResponseDatosRelacionadosWSDTOHelper {

	/**
	 * serialVersionUID
	 * de parte dos  para sonar
	 */
	private static final long serialVersionUID = 4205046562475519271L;
	/**
	 * Variable utilizada para declarar nombrePersonaEmpresa
	 */
	protected String nombrePersonaEmpresa = "";
	/**
	 * Variable utilizada para declarar tipoPersona
	 */
	protected String tipoPersona = "";
	/**
	 * Variable utilizada para declarar subtipoPersona
	 */
	protected String subtipoPersona = "";
	/**
	 * Variable utilizada para declarar actividadEconomica
	 */
	protected String actividadEconomica = "";
	/**
	 * Variable utilizada para declarar codProducto
	 */
	protected String codProducto = "";

	/**
	 * Obtiene el valor de nombrePersonaEmpresa
	 * @return El valor de nombrePersonaEmpresa
	 */
	public String getNombrePersonaEmpresa() {
		return nombrePersonaEmpresa;
	}

	/**
	 * Define el nuevo valor para nombrePersonaEmpresa
	 * @param nombrePersonaEmpresa El nuevo valor de nombrePersonaEmpresa
	 */
	public void setNombrePersonaEmpresa(String nombrePersonaEmpresa) {
		this.nombrePersonaEmpresa = nombrePersonaEmpresa;
	}

	/**
	 * Obtiene el valor de tipoPersona
	 * @return El valor de tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * Define el nuevo valor para tipoPersona
	 * @param tipoPersona El nuevo valor de tipoPersona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * Obtiene el valor de subtipoPersona
	 * @return El valor de subtipoPersona
	 */
	public String getSubtipoPersona() {
		return subtipoPersona;
	}

	/**
	 * Define el nuevo valor para subtipoPersona
	 * @param subtipoPersona El nuevo valor de subtipoPersona
	 */
	public void setSubtipoPersona(String subtipoPersona) {
		this.subtipoPersona = subtipoPersona;
	}

	/**
	 * Obtiene el valor de actividadEconomica
	 * @return El valor de actividadEconomica
	 */
	public String getActividadEconomica() {
		return actividadEconomica;
	}

	/**
	 * Define el nuevo valor para actividadEconomica
	 * @param actividadEconomica El nuevo valor de actividadEconomica
	 */
	public void setActividadEconomica(String actividadEconomica) {
		this.actividadEconomica = actividadEconomica;
	}

	/**
	 * Obtiene el valor de codProducto
	 * @return El valor de codProducto
	 */
	public String getCodProducto() {
		return codProducto;
	}

	/**
	 * Define el nuevo valor para codProducto
	 * @param codProducto El nuevo valor de codProducto
	 */
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

}