/**
 * Isban Mexico
 *   Clase: RelacionadoDTO.java
 *   Descripcion: DTO para las relaciones de las personas
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */

package mx.isban.norkom.ws.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class RelacionadoDTO extends RespuestaWebService implements Serializable {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 8492912233099771327L;
	/**
	 * Variable utilizada para declarar claveRelacionado
	 */
	private int claveRelacionado;
	/**
	 * Variable utilizada para declarar tipoRelacion
	 */
	private String tipoRelacion;
	/**
	 * Variable utilizada para declarar nombre
	 */
	private String nombre;
	/**
	 * Variable utilizada para declarar apellidoPaterno
	 */
	private String apellidoPaterno;
	/**
	 * Variable utilizada para declarar apellidoMaterno
	 */
	private String apellidoMaterno;
	/**
	 * Variable utilizada para declarar fechaNacimiento
	 */
	private String fechaNacimiento;
	/**
	 * Variable utilizada para declarar tipoPersona
	 */
	private String tipoPersona;
	/**
	 * Variable utilizada para declarar codPaisNacionalidad
	 */
	private String codPaisNacionalidad;
	/**
	 * Variable utilizada para declarar codPaisResidencia
	 */
	private String codPaisResidencia;
	/**
	 * Variable utilizada para declarar porcentajeParticipacion
	 */
	private double porcentajeParticipacion;
	/**
	 * 
	 * @return int clave relacionado
	 */
	public int getClaveRelacionado() {
		return claveRelacionado;
	}
	/**
	 * 
	 * @param claveRelacionado para Clave de Relacinado
	 */
	public void setClaveRelacionado(int claveRelacionado) {
		this.claveRelacionado = claveRelacionado;
	}
	/**
	 * 
	 * @return String tipo de relacion
	 */
	public String getTipoRelacion() {
		return tipoRelacion;
	}
	/**
	 * 
	 * @param tipoRelacion para Tipo de Relacion
	 */
	public void setTipoRelacion(String tipoRelacion) {
		this.tipoRelacion = tipoRelacion;
	}
	
	/**
	 * Obtiene el nombre completo de la referencia
	 * @return Cadena formada por Nombre, Apellido paterno y Apellido materno
	 */
	public String getNombreCompleto() {
		StringBuffer nombreCompleto = new StringBuffer(StringUtils.trimToEmpty(getNombre()));
		String apellidoPaternoTmp = StringUtils.trimToEmpty(getApellidoPaterno());
		String apellidoMaternoTmp = StringUtils.trimToEmpty(getApellidoMaterno());
		
		if(StringUtils.isNotBlank(apellidoPaternoTmp)){
			nombreCompleto.append(" ").append(apellidoPaternoTmp);
		}
		
		if(StringUtils.isNotBlank(apellidoMaternoTmp)){
			nombreCompleto.append(" ").append(apellidoMaternoTmp);
		}
		
		return nombreCompleto.toString();
	}
	
	public String getNombreCompletoNorkom() {
		StringBuffer nombreCompleto = new StringBuffer(StringUtils.trimToEmpty(getNombre()));
		String apellidoPaternoTmp = StringUtils.trimToEmpty(getApellidoPaterno());
		String apellidoMaternoTmp = StringUtils.trimToEmpty(getApellidoMaterno());
		
		if(StringUtils.isNotBlank(apellidoPaternoTmp) && !"J".equalsIgnoreCase(getTipoPersona()) && !"null".equalsIgnoreCase(apellidoPaternoTmp)){
			nombreCompleto.append(" ").append(apellidoPaternoTmp);
		}
		
		if(StringUtils.isNotBlank(apellidoMaternoTmp) && !"J".equalsIgnoreCase(getTipoPersona()) && !"null".equalsIgnoreCase(apellidoMaternoTmp)){
			nombreCompleto.append(" ").append(apellidoMaternoTmp);
		}
		
		return nombreCompleto.toString();
	}
	
	/**
	 * 
	 * @return String nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * 
	 * @param nombre para Nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * 
	 * @return String apellido paterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * 
	 * @param apellidoPaterno para Apellido Paterno
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * 
	 * @return String apellido materno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * 
	 * @param apellidoMaterno para Apellido Materno
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * 
	 * @return String fecha de nacimiento
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * 
	 * @param fechaNacimiento para Fecha de Nacimiento
	 */
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * 
	 * @return String tipo de persona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * 
	 * @param tipoPersona para Tipo de Persona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * 
	 * @return String codigo de pais y nacionalidad
	 */
	public String getCodPaisNacionalidad() {
		return codPaisNacionalidad;
	}
	/**
	 * 
	 * @param codPaisNacionalidad para Codigo de Pais de Nacionalidad
	 */
	public void setCodPaisNacionalidad(String codPaisNacionalidad) {
		this.codPaisNacionalidad = codPaisNacionalidad;
	}
	/**
	 * 
	 * @return String codigo del pais de recidencia
	 */
	public String getCodPaisResidencia() {
		return codPaisResidencia;
	}
	/**
	 * 
	 * @param codPaisResidencia para Codigo de Pais de Residencia
	 */
	public void setCodPaisResidencia(String codPaisResidencia) {
		this.codPaisResidencia = codPaisResidencia;
	}
	/**
	 * 
	 * @return double porcentaje de participacion
	 */
	public double getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	/**
	 * 
	 * @param porcentajeParticipacion para Porcentaje de Participacion
	 */
	public void setPorcentajeParticipacion(double porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	
}
