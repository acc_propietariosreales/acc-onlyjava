/**
 * Isban Mexico
 *   Clase: RelacionadosRequestDTO.java
 *   Descripcion: Componente para las personas 
 *   relacionadas de los cuestionarios con numero de 
 *   Bloque
 *
 *   Control de Cambios:
 *   1.0 Jun 13, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.util.ArrayList;
import java.util.List;
/**
 * Calse DTO para 
 * el proceso de relacionados por bloque
 * @author lespinosa
 *
 */
public class RelacionadosRequestDTO extends RespuestaWebService {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 6870555686213721810L;
	/**
	 * Variable utilizada para declarar 
	 * idCuestionario
	 */
	private String idCuestionario;
	/**
	 * Numero de Buc
	 */
	private String buc;
	/**
	 * Numero de Bloque a ingresar
	 */
	private String bloque;
	/**
	 * Variable utilizada para declarar 
	 * relacionados
	 */
	private List<RelacionadoDTO> relacionados;
	

	/**
	 * Obtiene el valor de bloque
	 * @return El valor de bloque
	 */
	public String getBloque() {
		return bloque;
	}
	/**
	 * Define el nuevo valor para bloque
	 * @param bloque El nuevo valor de bloque
	 */
	public void setBloque(String bloque) {
		this.bloque = bloque;
	}
	/**
	 * Obtiene el valor de buc
	 * @return El valor de buc
	 */
	public String getBuc() {
		return buc;
	}
	/**
	 * 
	 * @return String id de cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * 
	 * @param idCuestionario para Id de Cuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * 
	 * @param relacionados para definir Relacionados
	 */
	public void setRelacionados(List<RelacionadoDTO> relacionados) {
		List<RelacionadoDTO> relacionin= new ArrayList<RelacionadoDTO>();
		relacionin.addAll(relacionados);
		this.relacionados = relacionin;
	}
	/**
	 * 
	 * @param relacionado para agregar Relacionados
	 */
	public void addRelacionado(RelacionadoDTO relacionado){
		if(this.relacionados == null){
			this.relacionados = new ArrayList<RelacionadoDTO>();
		}
		
		this.relacionados.add(relacionado);
	}
	/**
	 * Define el nuevo valor para buc
	 * @param buc El nuevo valor de buc
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	/**
	 * 
	 * @return List<RelacionadoBloqueDTO> lista de relacionados
	 */
	public List<RelacionadoDTO> getRelacionados() {
		List<RelacionadoDTO> relacionin= new ArrayList<RelacionadoDTO>();
		relacionin.addAll(this.relacionados);
		return relacionin;
	}
}