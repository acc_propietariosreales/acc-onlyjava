/**
 * Isban Mexico
 *   Clase: RelacionadosResponseDTO.java
 *   Descripcion: Componente para las respuestas de los 
 *   relacionadas de los cuestionarios con numero de 
 *   Bloque
 *
 *   Control de Cambios:
 *   1.0 Jun 13, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.util.ArrayList;
import java.util.List;
/**
 * DTO para la respuesta de 
 * relacionados
 * @author lespinosa
 *
 */
public class RelacionadosResponseDTO extends RespuestaWebService {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 6870555686213721810L;
	/**
	 * Variable utilizada para declarar 
	 * idCuestionario
	 */
	private String idCuestionario;
	/**
	 * Numero de Bloque a ingresar
	 */
	private String bloque;
	/**
	 * Variable utilizada para declarar 
	 * relacionados
	 */
	private List<RelacionadoDTO> relacionados;
	
	/**
	 * 
	 * @return String id de cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * 
	 * @return List<RelacionadoBloqueDTO> lista de relacionados
	 */
	public List<RelacionadoDTO> getRelacionados() {
		List<RelacionadoDTO> relacIn= new ArrayList<RelacionadoDTO>();
		relacIn.addAll(this.relacionados);
		return relacIn;
	}
	/**
	 * Obtiene el valor de bloque
	 * @return El valor de bloque
	 */
	public String getBloque() {
		return bloque;
	}
	/**
	 * 
	 * @param idCuestionario para Id de Cuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * 
	 * @param relacionados para definir Relacionados
	 */
	public void setRelacionados(List<RelacionadoDTO> relacionados) {
		List<RelacionadoDTO> inrelas= new ArrayList<RelacionadoDTO>();
		inrelas.addAll(relacionados);
		this.relacionados = inrelas;

	}

	/**
	 * Define el nuevo valor para bloque
	 * @param bloque El nuevo valor de bloque
	 */
	public void setBloque(String bloque) {
		this.bloque = bloque;
	}
	/**
	 * 
	 * @param relacionado para agregar Relacionados
	 */
	public void addRelacionado(RelacionadoDTO relacionado){
		if(this.relacionados == null){
			this.relacionados = new ArrayList<RelacionadoDTO>();
		}
		
		this.relacionados.add(relacionado);
	}
}