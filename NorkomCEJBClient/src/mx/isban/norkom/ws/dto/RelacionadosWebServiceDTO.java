/**
 *   Isban Mexico
 *   Clase: RelacionadosWebServiceDTO.java
 *   Descripcion: Componente para las personas relacionadas de los cuestionarios
 *
 *   Control de Cambios:
 *   1.0 Jul 03, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.util.ArrayList;
import java.util.List;
/**
 * Clase para almacenar los datos
 * de los relacionados
 * @author lespinosa
 *
 */
public class RelacionadosWebServiceDTO extends RespuestaWebService {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 * de tipo long
	 */
	private static final long serialVersionUID = 6870555686213721810L;
	/**
	 * Variable utilizada para declarar idCuestionario
	 * de tipo String
	 */
	private String idCuestionario;
	/**
	 * Variable utilizada para declarar relacionados
	 * de tipos RelacionadoDTO
	 */
	private List<RelacionadoDTO> relacionados;
	
	/**
	 * 
	 * @return String id de cuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}
	/**
	 * 
	 * @param idCuestionario para Id de Cuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}
	/**
	 * 
	 * @return List<RelacionadoDTO> lista de relacionados
	 */
	public List<RelacionadoDTO> getRelacionados() {
		List<RelacionadoDTO> copia= new ArrayList<RelacionadoDTO>();
		copia.addAll(relacionados);
		return copia;
	}
	/**
	 * 
	 * @param relacionados para definir Relacionados
	 */
	public void setRelacionados(List<RelacionadoDTO> relacionados) {
		List<RelacionadoDTO> copia= new ArrayList<RelacionadoDTO>();
		copia.addAll(relacionados);
		this.relacionados = copia;
	}
	/**
	 * metodo para agregar los relacionados
	 * @param relacionado para agregar Relacionados
	 */
	public void addRelacionado(RelacionadoDTO relacionado){
		if(this.relacionados == null){
			this.relacionados = new ArrayList<RelacionadoDTO>();
		}
		
		this.relacionados.add(relacionado);
	}
}