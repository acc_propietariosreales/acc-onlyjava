/**
 * Isban Mexico
 *   Clase: ReporteBase64WebServiceDTO.java
 *   Descripcion: Componente que almacena pdf en base64
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.io.Serializable;


public class ReporteBase64WebServiceDTO extends RespuestaWebService implements Serializable {

	/**
	 * Codigo de exito en la obtencion del cuestionario
	 */
	public static final String COD_EXITO = "EXOBCT00";
	/**
	 * Mensaje de exito en la obtencion del cuestionario
	 */
	public static final String MSJ_EXITO = "Generaci\u00F3n exitosa";
	
	/**
	 * Codigo de error inesperado
	 */
	public static final String COD_ER_GENERACION = "EROBCT99";
	/**
	 * Codigo de error inesperado
	 */
	public static final String MSJ_ER_GENERACION = "No se pudo obtener el PDF";
	
	/**
	 * Codigo de error inesperado
	 */
	public static final String COD_ER_DATOS = "EROBCT98";
	/**
	 * Codigo de error inesperado
	 */
	public static final String MSJ_ER_DATOS = "No se ha asignado un Número de Contrato";
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6691170196950215879L;
	/**
	 * String pdfBase64
	 */
	private String pdfBase64;
	
	/**
	 * @return String base62
	 */
	public String getPdfBase64() {
		return pdfBase64;
	}

	/**
	 * @param pdfBase64 to set
	 */
	public void setPdfBase64(String pdfBase64) {
		this.pdfBase64 = pdfBase64;
	}
}
