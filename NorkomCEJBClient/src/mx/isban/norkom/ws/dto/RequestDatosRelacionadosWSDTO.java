/**
 * Isban Mexico
 *   Clase: RequestDatosRelacionadosWSDTO.java
 *   Descripcion: Response para obtener estatus de relacionados
 *   
 *   Control de Cambios:
 *   1.0 Jun 26, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.io.Serializable;
/**
 * RequestDatosRelacionadosWSDTO
 * @author lespinosa
 *
 */
public class RequestDatosRelacionadosWSDTO implements Serializable{

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -1675771191241448858L;
	
	/**
	 * Variable utilizada para declarar idCuestionario
	 */
	private String idCuestionario = "";
	
	/**
	 * Variable utilizada para declarar numeroBloque
	 */
	private String numeroBloque;

	/**
	 * Obtiene el valor de idCuestionario
	 * @return El valor de idCuestionario
	 */
	public String getIdCuestionario() {
		return idCuestionario;
	}

	/**
	 * Define el nuevo valor para idCuestionario
	 * @param idCuestionario El nuevo valor de idCuestionario
	 */
	public void setIdCuestionario(String idCuestionario) {
		this.idCuestionario = idCuestionario;
	}

	/**
	 * Obtiene el valor de numeroBloque
	 * @return El valor de numeroBloque
	 */
	public String getNumeroBloque() {
		return numeroBloque;
	}

	/**
	 * Define el nuevo valor para numeroBloque
	 * @param numeroBloque El nuevo valor de numeroBloque
	 */
	public void setNumeroBloque(String numeroBloque) {
		this.numeroBloque = numeroBloque;
	}
	
	

}
