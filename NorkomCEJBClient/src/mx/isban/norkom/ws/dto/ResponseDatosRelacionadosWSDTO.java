/**
 * Isban Mexico
 *   Clase: ResponseDatosRelacionadosWSDTO.java
 *   Descripcion: Response para obtener estatus de relacionados
 *   
 *   Control de Cambios:
 *   1.0 Jun 26, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

/**
 * ResponseDatosRelacionadosWSDTO
 * ResponseDatosRelacionadosWSDTOHelper
 * @author lespinosa
 *
 */
public class ResponseDatosRelacionadosWSDTO extends DatosRelacionadoGeneral{

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = -6325675338107392815L;
	
	/**
	 * Variable utilizada para declarar fechaNacimientoConstitucion
	 */
	private String fechaNacimientoConstitucion = "";
	
	/**
	 * Variable utilizada para declarar segmentoNegCliente
	 */
	private String segmentoNegCliente = "";
	
	/**
	 * Variable utilizada para declarar paisResidencia
	 */
	private String paisResidencia = "";
	
	/**
	 * Variable utilizada para declarar municipioResidencia
	 */
	private String municipioResidencia = "";
	
	/**
	 * Variable utilizada para declarar nacionalidad
	 */
	private String nacionalidad = "";


	/**
	 * Obtiene el valor de fechaNacimientoConstitucion
	 * @return El valor de fechaNacimientoConstitucion
	 */
	public String getFechaNacimientoConstitucion() {
		return fechaNacimientoConstitucion;
	}

	/**
	 * Define el nuevo valor para fechaNacimientoConstitucion
	 * @param fechaNacimientoConstitucion El nuevo valor de fechaNacimientoConstitucion
	 */
	public void setFechaNacimientoConstitucion(String fechaNacimientoConstitucion) {
		this.fechaNacimientoConstitucion = fechaNacimientoConstitucion;
	}



	/**
	 * Obtiene el valor de segmentoNegCliente
	 * @return El valor de segmentoNegCliente
	 */
	public String getSegmentoNegCliente() {
		return segmentoNegCliente;
	}

	/**
	 * Define el nuevo valor para segmentoNegCliente
	 * @param segmentoNegCliente El nuevo valor de segmentoNegCliente
	 */
	public void setSegmentoNegCliente(String segmentoNegCliente) {
		this.segmentoNegCliente = segmentoNegCliente;
	}


	/**
	 * Obtiene el valor de paisResidencia
	 * @return El valor de paisResidencia
	 */
	public String getPaisResidencia() {
		return paisResidencia;
	}

	/**
	 * Define el nuevo valor para paisResidencia
	 * @param paisResidencia El nuevo valor de paisResidencia
	 */
	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	/**
	 * Obtiene el valor de municipioResidencia
	 * @return El valor de municipioResidencia
	 */
	public String getMunicipioResidencia() {
		return municipioResidencia;
	}

	/**
	 * Define el nuevo valor para municipioResidencia
	 * @param municipioResidencia El nuevo valor de municipioResidencia
	 */
	public void setMunicipioResidencia(String municipioResidencia) {
		this.municipioResidencia = municipioResidencia;
	}

	/**
	 * Obtiene el valor de nacionalidad
	 * @return El valor de nacionalidad
	 */
	public String getNacionalidad() {
		return nacionalidad;
	}

	/**
	 * Define el nuevo valor para nacionalidad
	 * @param nacionalidad El nuevo valor de nacionalidad
	 */
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	

}
