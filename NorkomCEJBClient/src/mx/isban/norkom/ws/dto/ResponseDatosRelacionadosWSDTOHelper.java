/**
 * Isban Mexico
 *   Clase: ResponseDatosRelacionadosWSDTOHelper.java
 *   Descripcion: Response para obtener estatus de relacionados
 *   
 *   Control de Cambios:
 *   1.0 Jun 26, 2017 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

/**
 * ResponseDatosRelacionadosWSDTOHelper
 * clase de ayuda para WS
 * @author lespinosa
 *
 */
public class ResponseDatosRelacionadosWSDTOHelper extends DatosRelacionadoHelperUno{

	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 4648134575888665333L;
	
	/**
	 * Variable utilizada para declarar codigoRegionSucursal
	 */
	private String codigoRegionSucursal = "";
	
	/**
	 * Variable utilizada para declarar codigoZonaSucursal
	 */
	private String codigoZonaSucursal = "";
	
	/**
	 * Variable utilizada para declarar presenciaFisica
	 */
	private String presenciaFisica = "";
	
	/**
	 * Variable utilizada para declarar origenRecursos
	 */
	private String origenRecursos = "";
	
	/**
	 * Variable utilizada para declarar destinoRecursos
	 */
	private String destinoRecursos = "";
	


	/**
	 * Obtiene el valor de codigoRegionSucursal
	 * @return El valor de codigoRegionSucursal
	 */
	public String getCodigoRegionSucursal() {
		return codigoRegionSucursal;
	}

	/**
	 * Define el nuevo valor para codigoRegionSucursal
	 * @param codigoRegionSucursal El nuevo valor de codigoRegionSucursal
	 */
	public void setCodigoRegionSucursal(String codigoRegionSucursal) {
		this.codigoRegionSucursal = codigoRegionSucursal;
	}

	/**
	 * Obtiene el valor de codigoZonaSucursal
	 * @return El valor de codigoZonaSucursal
	 */
	public String getCodigoZonaSucursal() {
		return codigoZonaSucursal;
	}

	/**
	 * Define el nuevo valor para codigoZonaSucursal
	 * @param codigoZonaSucursal El nuevo valor de codigoZonaSucursal
	 */
	public void setCodigoZonaSucursal(String codigoZonaSucursal) {
		this.codigoZonaSucursal = codigoZonaSucursal;
	}

	/**
	 * Obtiene el valor de presenciaFisica
	 * @return El valor de presenciaFisica
	 */
	public String getPresenciaFisica() {
		return presenciaFisica;
	}

	/**
	 * Define el nuevo valor para presenciaFisica
	 * @param presenciaFisica El nuevo valor de presenciaFisica
	 */
	public void setPresenciaFisica(String presenciaFisica) {
		this.presenciaFisica = presenciaFisica;
	}

	/**
	 * Obtiene el valor de origenRecursos
	 * @return El valor de origenRecursos
	 */
	public String getOrigenRecursos() {
		return origenRecursos;
	}

	/**
	 * Define el nuevo valor para origenRecursos
	 * @param origenRecursos El nuevo valor de origenRecursos
	 */
	public void setOrigenRecursos(String origenRecursos) {
		this.origenRecursos = origenRecursos;
	}

	/**
	 * Obtiene el valor de destinoRecursos
	 * @return El valor de destinoRecursos
	 */
	public String getDestinoRecursos() {
		return destinoRecursos;
	}

	/**
	 * Define el nuevo valor para destinoRecursos
	 * @param destinoRecursos El nuevo valor de destinoRecursos
	 */
	public void setDestinoRecursos(String destinoRecursos) {
		this.destinoRecursos = destinoRecursos;
	}

	
}
