/**
 * Isban Mexico
 *   Clase: RespuestaWebService.java
 *   Descripcion: Response que regresa un WS
 *
 *   Control de Cambios:
 *   1.0 Oct 23, 2012 Stefanini - Creacion
 */
package mx.isban.norkom.ws.dto;

import java.io.Serializable;

public class RespuestaWebService implements Serializable {
	/**
	 * Variable utilizada para declarar serialVersionUID
	 */
	private static final long serialVersionUID = 747503605920645097L;
	/**
	 * Variable utilizada para declarar codigoOperacion
	 */
	private String codigoOperacion;
	/**
	 * Variable utilizada para declarar descOperacion
	 */ 
	private String descOperacion;
	
	/**
	 * @return String codigo de operacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}
	/**
	 * @param codigoOperacion para Codigo de Operacion
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	/**
	 * @return String descripcion de operacion
	 */
	public String getDescOperacion() {
		return descOperacion;
	}
	/**
	 * @param descOperacion para Descripcion de Operacion
	 */
	public void setDescOperacion(String descOperacion) {
		this.descOperacion = descOperacion;
	}
}